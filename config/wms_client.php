<?php

return [
    'client_id' => env("WMS_CLIENT_ID",'kuku_stf'),
    'client_secret' => env("WMS_CLIENT_SECRET",'kuku_stf553264'),
    'url'=>env('WMS_URL', 'https://devkukuruyuk.com/wms_test/'), //wms hosting kukuruyuk
    'proxy'=>env('WMS_PROXY')
];
