<?php

return [
    'bcava_code' => env('SPRINTASIA_BCAVA_CODE', '57111'),
    'permatava_code' => env('SPRINTASIA_PERMATA_CODE', '873563') ,
    'secret_key' => env('SPRINTASIA_SECRET_KEY', '58a85394b0fe867ade8cdbe5a787dfae'),
    'bcaklikpay_channelid' => env('SPRINTASIA_BCAKLIKPAY_CHANNELID', 'KUKURYKKP01') ,
    'bcava_channelid' => env('SPRINTASIA_BCAVA_CHANNELID', 'KUKURBVA01'),
    'permatava_channelid' => env('SPRINTASIA_PERMATAVA_CHANNELID', 'KUKURPVA01'),
    'kredivo_channelid' => env('SPRINTASIA_KREDIVO_CHANNELID', 'KUKURKRDV01'),
    'url_insert' => env('SPRINTASIA_URL_INSERT', 'https://simpg.sprintasia.net/PaymentRegister'),
];
?>
