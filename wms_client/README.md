# About

WMSClient.php adalah wrapper untuk memanggil api yang disediakan oleh aplikasi WMS pada framework laravel.

# Installation 

1. Instal guzzle "guzzlehttp/guzzle": "~6.0". Lihat [https://github.com/guzzle/guzzle](https://github.com/guzzle/guzzle)
2. Copy wms_client.php ke folder config
3. Copy WMSClient.php ke folder app
4. Selesai

# Usage 

Berikut adalah fungsi-fungsi yang disediakan di WMSClient.php:

## 1. Cara Buat SO (Sales Order)

### Request
Dalam membuat SO, panggil fungsi `createSO`, masukan parameter-parameter sesuai data dibawah ini di dalam function nya

| Nama Parameter        | Type              | Contoh Nilai          | Keterangan                                                                |
| -----                 | :--:              | :--:                  | ----------:                                                               |
| invoice_number        | String            | SO1610170025          | **[Required]** Nomor Invoice                                                  | 
| buyer_name            | String            | Budi Mulia            | **[Required]** Nama Pembeli                                                   |
| buyer_email           | String            | Budi@gmail.com        | **[Required]** Alamat Email Pembeli                                           |
| address               | String            | Jl. Kuring 2          | **[Required]** Alamat lengkap seperti nama jalannya                           |
| city_code             | String            | 1504                  | **[Required]** Kota                                                           |
| postalservice_code    | String            | REGULER               | **[Required]** Kode Postal Service nya, disini menggunakan JNE [REGULER]      |
| zip_code              | String            | 15159                 | **[Required]** Kode Pos                                                       |                 
| order_date            | String            | 2016-11-03            | Tanggal Pemesanan                                                         |
| currency_code         | String            | IDR                   | Kode Mata Uang                                                            |
| warehouse_id          | int               | 1                     | Id Warehouse untuk yang melayani SO. Jika tidak diisi, maka SO akan dilayani oleh warehouse default. |
| items                 | Array             |                       | **[Required]** Daftar barang yang dibeli                                      |
| - items[sku]          | String            | LGX470103B11          | **[Required]** No Punggung Barang                                             |
| - items[qty]          | int               | 10                    | **[Required]** Jumlah Stok Barang                                             |
| - items[unit_price]   | int               | 500000                | **[Required]** Harga Barang                                                   |
| - items[tax_rate]     | float             | 0                     | **[Required]** Presentase pajak barang relatif terhadap harga jual, dimana jika persentase nya 10% maka di aplikasi akan menjadi 0.1 nilai persentase nya, jika 25% maka akan menjadi 0.25 |

### Response

//TODO id:Id sale order yang dibuat

## 2. Mengambil atau mengecek jumlah Stok (Inventory)

Untuk mengetahui jumlah stok, panggil fungsi `getStock` pada WMSClient.php. Paramater yang dibutuhkan:

| Nama Parameter        | Type              | Contoh Nilai          | Keterangan                                            |
| -----                 | :--:              | :--:                  | ----------:                                           |
| sku                   | String            | LGX470103B11          | **[Required]** Nomor Sku                                  | 
| location_id           | int               | 1                     | Lokasi penyimpanan Stok item nya                      |

