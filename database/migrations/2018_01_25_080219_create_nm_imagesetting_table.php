<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmImagesettingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_imagesetting', function(Blueprint $table)
		{
			$table->smallInteger('imgs_id', true);
			$table->string('imgs_name', 150);
			$table->boolean('imgs_type')->comment('1- logo,2 -Favicon,3-noimage');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_imagesetting');
	}

}
