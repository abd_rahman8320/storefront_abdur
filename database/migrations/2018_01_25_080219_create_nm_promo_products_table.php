<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPromoProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_promo_products', function(Blueprint $table)
		{
			$table->integer('promop_id', true);
			$table->integer('promop_schedp_id');
			$table->integer('promop_merchant_id');
			$table->integer('promop_shop_id');
			$table->integer('promop_pro_id');
			$table->integer('promop_status');
			$table->integer('promop_original_price');
			$table->integer('promop_original_disprice');
			$table->integer('promop_discount_price');
			$table->integer('promop_discount_percentage');
			$table->integer('promop_saving_price');
			$table->dateTime('created_date')->default(DB::raw('NOW()'));
			$table->integer('promop_qty');
			$table->integer('stock');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_promo_products');
	}

}
