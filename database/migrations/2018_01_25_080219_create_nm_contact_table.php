<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmContactTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_contact', function(Blueprint $table)
		{
			$table->increments('cont_id');
			$table->string('cont_name', 100);
			$table->string('cont_email', 150);
			$table->string('cont_no', 50);
			$table->text('cont_message', 65535);
			$table->boolean('cont_restatus');
			$table->dateTime('cont_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_contact');
	}

}
