<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_user', function(Blueprint $table)
		{
			$table->integer('user_id', true);
			$table->string('user_name', 150)->unique('user_name');
			$table->string('user_profile_full_name', 150);
			$table->string('user_profile_email', 150);
			$table->string('user_pwd_method', 50);
			$table->string('user_pwd_salt', 50)->nullable();
			$table->string('user_pwd_secret', 150)->nullable();
			$table->boolean('user_pwd_expire');
			$table->boolean('user_active');
			$table->string('created_by', 150);
			$table->dateTime('created_ts');
			$table->string('lastupdated_by', 150);
			$table->timestamp('lastupdated_ts')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('deleted_by', 150);
			$table->dateTime('deleted_ts');
			$table->boolean('is_deleted');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_user');
	}

}
