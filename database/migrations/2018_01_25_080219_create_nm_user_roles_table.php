<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmUserRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_user_roles', function(Blueprint $table)
		{
			$table->integer('ur_id', true);
			$table->string('ur_user_name', 150);
			$table->string('ur_roles_name', 150);
			$table->string('created_by', 150);
			$table->dateTime('created_ts');
			$table->string('lastupdated_by', 150);
			$table->timestamp('lastupdated_ts')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('deleted_by', 150);
			$table->dateTime('deleted_ts');
			$table->boolean('is_deleted');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_user_roles');
	}

}
