<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmModulesettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_modulesettings', function(Blueprint $table)
		{
			$table->integer('ms_id', true);
			$table->integer('ms_dealmodule');
			$table->integer('ms_productmodule');
			$table->integer('ms_auctionmodule');
			$table->integer('ms_blogmodule');
			$table->integer('ms_nearmemapmodule');
			$table->integer('ms_storelistmodule');
			$table->integer('ms_pastdealmodule');
			$table->integer('ms_faqmodule');
			$table->integer('ms_cod');
			$table->integer('ms_paypal');
			$table->integer('ms_creditcard');
			$table->integer('ms_googlecheckout');
			$table->integer('ms_shipping')->comment('1=>Free shipping, 2=> Flat shipping, 3=> Product per shippin, 4=> Per Item shipping');
			$table->integer('ms_newsletter_template')->comment('1=> Temp 1, 2=>Temp 2, 3=> Temp 3, 4=> Temp 4');
			$table->integer('ms_citysettings')->comment('1=> With city, 0=> Without city');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_modulesettings');
	}

}
