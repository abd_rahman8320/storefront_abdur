<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthSessionScopesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_session_scopes', function(Blueprint $table)
		{
			$table->foreign('scope_id')->references('id')->on('oauth_scopes')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('session_id')->references('id')->on('oauth_sessions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_session_scopes', function(Blueprint $table)
		{
			$table->dropForeign('oauth_session_scopes_scope_id_foreign');
			$table->dropForeign('oauth_session_scopes_session_id_foreign');
		});
	}

}
