<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProcartTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_procart', function(Blueprint $table)
		{
			$table->increments('pc_id');
			$table->dateTime('pc_date');
			$table->integer('pc_pro_id');
			$table->boolean('pc_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_procart');
	}

}
