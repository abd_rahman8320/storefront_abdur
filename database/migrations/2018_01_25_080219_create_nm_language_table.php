<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmLanguageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_language', function(Blueprint $table)
		{
			$table->smallInteger('la_id', true)->unsigned();
			$table->string('la_code', 7);
			$table->string('la_name', 30);
			$table->boolean('la_active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_language');
	}

}
