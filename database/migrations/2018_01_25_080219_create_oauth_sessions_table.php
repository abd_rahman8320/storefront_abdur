<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOauthSessionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oauth_sessions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('client_id', 40);
			$table->enum('owner_type', array('client','user'))->default('user');
			$table->string('owner_id');
			$table->string('client_redirect_uri')->nullable();
			$table->timestamps();
			$table->index(['client_id','owner_type','owner_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oauth_sessions');
	}

}
