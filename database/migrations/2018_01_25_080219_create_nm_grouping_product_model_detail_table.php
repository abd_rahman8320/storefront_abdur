<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmGroupingProductModelDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_grouping_product_model_detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_grouping_product_model_header');
			$table->integer('id_product');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_grouping_product_model_detail');
	}

}
