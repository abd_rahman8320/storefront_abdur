<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthGrantScopesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_grant_scopes', function(Blueprint $table)
		{
			$table->foreign('grant_id')->references('id')->on('oauth_grants')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('scope_id')->references('id')->on('oauth_scopes')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_grant_scopes', function(Blueprint $table)
		{
			$table->dropForeign('oauth_grant_scopes_grant_id_foreign');
			$table->dropForeign('oauth_grant_scopes_scope_id_foreign');
		});
	}

}
