<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthClientScopesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_client_scopes', function(Blueprint $table)
		{
			$table->foreign('client_id')->references('id')->on('oauth_clients')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('scope_id')->references('id')->on('oauth_scopes')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_client_scopes', function(Blueprint $table)
		{
			$table->dropForeign('oauth_client_scopes_client_id_foreign');
			$table->dropForeign('oauth_client_scopes_scope_id_foreign');
		});
	}

}
