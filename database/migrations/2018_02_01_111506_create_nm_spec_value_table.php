<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNmSpecValueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_spec_value', function(Blueprint $table)
		{
			$table->increments('spec_val_id')->primary();
			 
			 $table->integer('nm_specification_id');
			// $table->foreign('nm_specification_id')->references('sp_id')->on('nm_specification')->onUpdate('CASCADE')->onDelete('CASCADE');
			 $table->string('code_value', 50);
			$table->string('short_desc', 50);
			$table->string('long_desc', 200);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_spec_value');
	
	}

}
