<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmAudittrailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_audittrail', function(Blueprint $table)
		{
			$table->integer('aud_id', true);
			$table->string('aud_table');
			$table->string('aud_action');
			$table->string('aud_detail', 10000);
			$table->dateTime('aud_date');
			$table->string('aud_user_name');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_audittrail');
	}

}
