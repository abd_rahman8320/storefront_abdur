<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmBlogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_blog', function(Blueprint $table)
		{
			$table->integer('blog_id', true);
			$table->string('blog_title', 50);
			$table->text('blog_desc', 65535);
			$table->integer('blog_catid');
			$table->string('blog_image', 100);
			$table->string('blog_metatitle', 100);
			$table->text('blog_metadesc', 65535);
			$table->string('blog_metakey', 100);
			$table->string('blog_tags', 100);
			$table->integer('blog_comments')->comment('0-not allow,1-allow');
			$table->integer('blog_type')->unsigned()->comment('1-publish,2-drafts');
			$table->boolean('blog_status')->comment('1-block,0-unblock');
			$table->timestamp('blog_created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_blog');
	}

}
