<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmInquiriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_inquiries', function(Blueprint $table)
		{
			$table->increments('iq_id');
			$table->string('iq_name', 100);
			$table->string('iq_emailid', 150);
			$table->string('iq_phonenumber', 20);
			$table->string('iq_message', 300);
			$table->integer('inq_readstatus')->default(0)->comment('0-not read 1 read');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_inquiries');
	}

}
