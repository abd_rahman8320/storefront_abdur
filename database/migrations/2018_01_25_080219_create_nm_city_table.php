<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmCityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_city', function(Blueprint $table)
		{
			$table->increments('ci_id');
			$table->string('ci_name', 100);
			$table->smallInteger('ci_prov_id');
			$table->decimal('ci_lati', 18, 14);
			$table->decimal('ci_long', 18, 14);
			$table->boolean('ci_default');
			$table->boolean('ci_status');
			$table->string('ci_code', 199)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_city');
	}

}
