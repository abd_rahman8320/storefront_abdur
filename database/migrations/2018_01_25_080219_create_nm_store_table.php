<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmStoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_store', function(Blueprint $table)
		{
			$table->increments('stor_id');
			$table->integer('stor_merchant_id');
			$table->string('stor_name', 100);
			$table->string('stor_phone', 20);
			$table->string('stor_address1', 150);
			$table->string('stor_address2', 150);
			$table->smallInteger('stor_country')->unsigned();
			$table->integer('stor_city')->unsigned();
			$table->integer('stor_prov_id');
			$table->integer('stor_dis_id');
			$table->integer('stor_sdis_id');
			$table->string('stor_zipcode', 20);
			$table->text('stor_metakeywords', 65535);
			$table->text('stor_metadesc', 65535);
			$table->text('stor_website', 65535);
			$table->decimal('stor_latitude', 18, 14);
			$table->decimal('stor_longitude', 18, 14);
			$table->string('stor_img', 150);
			$table->boolean('stor_status')->default(1);
			$table->string('created_date', 20);
			$table->integer('stor_addedby')->default(1)->comment('1-admin,2 -merchant');
			$table->string('warehouse_code', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_store');
	}

}
