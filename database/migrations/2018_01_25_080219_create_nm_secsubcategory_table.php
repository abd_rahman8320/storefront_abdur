<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSecsubcategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_secsubcategory', function(Blueprint $table)
		{
			$table->smallInteger('ssb_id', true)->unsigned();
			$table->string('ssb_name', 100);
			$table->smallInteger('ssb_sb_id')->unsigned();
			$table->smallInteger('ssb_smc_id')->unsigned();
			$table->smallInteger('mc_id')->unsigned();
			$table->boolean('ssb_status');
			$table->string('ssb_code', 2000);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_secsubcategory');
	}

}
