<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmWishlistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_wishlist', function(Blueprint $table)
		{
			$table->bigInteger('ws_id', true)->unsigned();
			$table->bigInteger('ws_pro_id')->unsigned();
			$table->bigInteger('ws_cus_id')->unsigned();
			$table->timestamp('ws_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_wishlist');
	}

}
