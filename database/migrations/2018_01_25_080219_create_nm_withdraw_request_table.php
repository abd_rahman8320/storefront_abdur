<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmWithdrawRequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_withdraw_request', function(Blueprint $table)
		{
			$table->integer('wd_id', true);
			$table->integer('wd_mer_id');
			$table->integer('wd_total_wd_amt');
			$table->integer('wd_submited_wd_amt');
			$table->integer('wd_status')->comment('2 => confirmed, 1 => paid, 0=> Hold');
			$table->dateTime('wd_date');
			$table->string('wd_image');
			$table->string('wd_faktur_pajak', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_withdraw_request');
	}

}
