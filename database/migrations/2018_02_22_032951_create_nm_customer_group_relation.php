<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNmCustomerGroupRelation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_customer_group_relation', function(Blueprint $table)
		{
			$table->increments('cgr_id');
			$table->integer('cgr_cus_id');
			$table->integer('cgr_cus_group_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_customer_group_relation');
	}

}
