<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmShippingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_shipping', function(Blueprint $table)
		{
			$table->increments('ship_id');
			$table->string('ship_name', 100);
			$table->string('ship_address1', 200);
			$table->string('ship_address2', 200);
			$table->integer('ship_ci_id');
			$table->string('ship_state', 100);
			$table->smallInteger('ship_country')->unsigned();
			$table->string('ship_postalcode', 20);
			$table->string('ship_phone', 20);
			$table->string('ship_email');
			$table->integer('ship_order_id')->unsigned();
			$table->bigInteger('ship_cus_id');
			$table->string('shipping_ref_number', 100)->nullable();
			$table->integer('ship_dis_id');
			$table->integer('ship_sdis_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_shipping');
	}

}
