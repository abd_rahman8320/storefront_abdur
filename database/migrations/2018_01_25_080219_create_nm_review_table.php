<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmReviewTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_review', function(Blueprint $table)
		{
			$table->integer('comment_id', true);
			$table->string('product_id')->nullable();
			$table->string('deal_id')->nullable();
			$table->string('store_id')->nullable();
			$table->string('customer_id');
			$table->string('title');
			$table->text('comments', 65535);
			$table->string('ratings');
			$table->integer('status');
			$table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('sam1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_review');
	}

}
