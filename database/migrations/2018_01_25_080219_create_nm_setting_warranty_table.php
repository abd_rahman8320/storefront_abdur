<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSettingWarrantyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_setting_warranty', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('besaran_warranty');
			$table->integer('lama_garansi_dasar');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_setting_warranty');
	}

}
