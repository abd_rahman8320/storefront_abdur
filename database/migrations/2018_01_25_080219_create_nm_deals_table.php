<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmDealsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_deals', function(Blueprint $table)
		{
			$table->integer('deal_id', true);
			$table->string('deal_title', 500);
			$table->integer('deal_category');
			$table->integer('deal_main_category');
			$table->integer('deal_sub_category');
			$table->integer('deal_second_sub_category');
			$table->integer('deal_original_price');
			$table->integer('deal_discount_price');
			$table->integer('deal_discount_percentage');
			$table->integer('deal_saving_price');
			$table->dateTime('deal_start_date');
			$table->dateTime('deal_end_date');
			$table->date('deal_expiry_date');
			$table->text('deal_description', 65535);
			$table->integer('deal_merchant_id');
			$table->integer('deal_shop_id');
			$table->string('deal_meta_keyword', 250);
			$table->string('deal_meta_description', 500);
			$table->integer('deal_min_limit');
			$table->integer('deal_max_limit');
			$table->integer('deal_purchase_limit');
			$table->integer('deal_image_count');
			$table->string('deal_image', 500);
			$table->integer('deal_no_of_purchase');
			$table->string('created_date', 20);
			$table->integer('deal_status')->default(1)->comment('1-active, 0-block');
			$table->dateTime('deal_posted_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_deals');
	}

}
