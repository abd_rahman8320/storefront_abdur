<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthAccessTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_access_tokens', function(Blueprint $table)
		{
			$table->foreign('session_id')->references('id')->on('oauth_sessions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_access_tokens', function(Blueprint $table)
		{
			$table->dropForeign('oauth_access_tokens_session_id_foreign');
		});
	}

}
