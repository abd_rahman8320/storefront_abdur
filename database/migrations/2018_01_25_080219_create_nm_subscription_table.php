<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSubscriptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_subscription', function(Blueprint $table)
		{
			$table->increments('sub_id');
			$table->integer('sub_cus_id')->unsigned()->comment('customer id');
			$table->smallInteger('sub_mc_id')->unsigned();
			$table->boolean('sub_status');
			$table->integer('sub_readstatus')->default(0)->comment('-not read 1 read');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_subscription');
	}

}
