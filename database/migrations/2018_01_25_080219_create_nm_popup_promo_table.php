<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPopupPromoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_popup_promo', function(Blueprint $table)
		{
			$table->integer('pop_id', true);
			$table->string('pop_title', 150);
			$table->string('pop_image', 150);
			$table->integer('pop_status')->comment('1-block, 0-unblock');
			$table->integer('pop_default')->comment('1-no, 0-default');
			$table->text('pop_redirect', 65535);
			$table->dateTime('start_date');
			$table->dateTime('end_date');
			$table->integer('jumlah_klik');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_popup_promo');
	}

}
