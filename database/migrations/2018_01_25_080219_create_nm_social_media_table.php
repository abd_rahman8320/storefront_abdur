<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSocialMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_social_media', function(Blueprint $table)
		{
			$table->integer('sm_id', true);
			$table->string('sm_fb_app_id', 100);
			$table->string('sm_fb_sec_key', 100);
			$table->string('sm_fb_page_url', 250);
			$table->string('sm_fb_like_page_url', 250);
			$table->string('sm_twitter_url', 250);
			$table->string('sm_twitter_app_id', 250);
			$table->string('sm_twitter_sec_key', 250);
			$table->string('sm_linkedin_url', 250);
			$table->string('sm_youtube_url', 250);
			$table->string('sm_gmap_app_key', 250);
			$table->string('sm_android_page_url', 250);
			$table->string('sm_iphone_url', 250);
			$table->text('sm_analytics_code', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_social_media');
	}

}
