<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmTransaksiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_transaksi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('transaction_id');
			$table->string('bukti_transfer');
			$table->integer('total_belanja');
			$table->integer('diskon');
			$table->integer('diskon_voucher');
			$table->integer('shipping');
			$table->string('metode_pembayaran');
			$table->string('status_pembayaran');
			$table->float('pajak', 10, 0);
			$table->integer('order_status')->default(3);
			$table->integer('status_outbound_transaksi')->default(1)->comment('1 -> blm dilihat, 2->pick, 3 ->packing, 4->shipping, 5 -> receiving');
			$table->integer('kupon_id');
			$table->integer('total_poin');
			$table->integer('transaksi_dp');
			$table->integer('lama_cicilan');
			$table->integer('adminfee');
			$table->integer('cicilan_perbulan');
			$table->integer('approval_columbia');
			$table->integer('poin_digunakan');
			$table->integer('trans_lama_ship');
			$table->dateTime('trans_estimasi_ship');
			$table->boolean('is_cicilan');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_transaksi');
	}

}
