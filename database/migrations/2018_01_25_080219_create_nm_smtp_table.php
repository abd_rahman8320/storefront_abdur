<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSmtpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_smtp', function(Blueprint $table)
		{
			$table->boolean('sm_id')->primary();
			$table->string('sm_host', 150);
			$table->string('sm_port', 20);
			$table->string('sm_uname', 30);
			$table->string('sm_pwd', 30);
			$table->boolean('sm_isactive');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_smtp');
	}

}
