<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmBannerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_banner', function(Blueprint $table)
		{
			$table->smallInteger('bn_id', true)->unsigned();
			$table->string('bn_title', 150);
			$table->string('bn_type', 10)->comment('1-home,2-product,3-deal,4-auction');
			$table->string('bn_img', 150);
			$table->integer('bn_status')->comment('1-block,0-unblock');
			$table->text('bn_redirecturl', 65535);
			$table->dateTime('start_date');
			$table->dateTime('end_date');
			$table->integer('jumlah_klik');
			$table->integer('bn_sort');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_banner');
	}

}
