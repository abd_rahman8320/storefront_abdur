<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSubdistrictsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_subdistricts', function(Blueprint $table)
		{
			$table->integer('sdis_id', true);
			$table->string('sdis_code', 100);
			$table->string('sdis_name', 1000);
			$table->integer('sdis_dis_id');
			$table->string('sdis_zip_code', 1000);
			$table->boolean('sdis_status')->default(1)->comment('0-deactive, 1-active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_subdistricts');
	}

}
