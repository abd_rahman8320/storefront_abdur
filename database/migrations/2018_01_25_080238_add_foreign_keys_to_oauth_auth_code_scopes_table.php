<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthAuthCodeScopesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_auth_code_scopes', function(Blueprint $table)
		{
			$table->foreign('auth_code_id')->references('id')->on('oauth_auth_codes')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('scope_id')->references('id')->on('oauth_scopes')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_auth_code_scopes', function(Blueprint $table)
		{
			$table->dropForeign('oauth_auth_code_scopes_auth_code_id_foreign');
			$table->dropForeign('oauth_auth_code_scopes_scope_id_foreign');
		});
	}

}
