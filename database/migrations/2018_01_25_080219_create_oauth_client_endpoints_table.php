<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOauthClientEndpointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oauth_client_endpoints', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('client_id', 40);
			$table->string('redirect_uri');
			$table->timestamps();
			$table->unique(['client_id','redirect_uri']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oauth_client_endpoints');
	}

}
