<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmMaincategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_maincategory', function(Blueprint $table)
		{
			$table->smallInteger('mc_id', true)->unsigned();
			$table->string('mc_name', 100);
			$table->string('mc_type', 10);
			$table->string('mc_img', 150);
			$table->boolean('mc_status');
			$table->string('mc_code', 10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_maincategory');
	}

}
