<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPasswordResetRequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_password_reset_request', function(Blueprint $table)
		{
			$table->integer('prr_id', true);
			$table->string('prr_user_name', 150);
			$table->string('prr_request_uuid', 64);
			$table->boolean('prr_is_new');
			$table->dateTime('prr_expired_ts');
			$table->string('created_by', 150);
			$table->dateTime('created_ts');
			$table->string('lastupdated_by', 150);
			$table->timestamp('lastupdated_ts')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('deleted_by', 150);
			$table->dateTime('deleted_ts');
			$table->boolean('is_deleted');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_password_reset_request');
	}

}
