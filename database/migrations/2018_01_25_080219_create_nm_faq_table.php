<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmFaqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_faq', function(Blueprint $table)
		{
			$table->smallInteger('faq_id', true)->unsigned();
			$table->string('faq_name', 100);
			$table->text('faq_ans', 65535);
			$table->boolean('faq_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_faq');
	}

}
