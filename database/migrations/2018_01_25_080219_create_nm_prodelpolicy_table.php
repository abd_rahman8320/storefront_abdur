<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProdelpolicyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_prodelpolicy', function(Blueprint $table)
		{
			$table->bigInteger('pdp_id')->unsigned();
			$table->smallInteger('pdp_pro_id')->unsigned();
			$table->text('pdp_desc', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_prodelpolicy');
	}

}
