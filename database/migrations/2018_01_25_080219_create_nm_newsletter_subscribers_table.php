<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmNewsletterSubscribersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_newsletter_subscribers', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('email');
			$table->bigInteger('city_id');
			$table->boolean('status')->default(1);
			$table->dateTime('post_dt');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_newsletter_subscribers');
	}

}
