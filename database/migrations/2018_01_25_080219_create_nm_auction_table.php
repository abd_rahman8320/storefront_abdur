<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmAuctionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_auction', function(Blueprint $table)
		{
			$table->increments('auc_id');
			$table->string('auc_title', 500);
			$table->integer('auc_category');
			$table->integer('auc_main_category');
			$table->integer('auc_sub_category');
			$table->integer('auc_second_sub_category');
			$table->integer('auc_original_price');
			$table->integer('auc_auction_price');
			$table->smallInteger('auc_bitinc')->unsigned();
			$table->integer('auc_saving_price');
			$table->dateTime('auc_start_date');
			$table->dateTime('auc_end_date');
			$table->decimal('auc_shippingfee', 10);
			$table->text('auc_shippinginfo', 65535);
			$table->text('auc_description', 65535);
			$table->integer('auc_merchant_id');
			$table->integer('auc_shop_id');
			$table->string('auc_meta_keyword', 250);
			$table->string('auc_meta_description', 500);
			$table->integer('auc_image_count');
			$table->string('auc_image', 500);
			$table->integer('auc_status')->default(1)->comment('1-active, 0-block');
			$table->dateTime('auc_posted_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_auction');
	}

}
