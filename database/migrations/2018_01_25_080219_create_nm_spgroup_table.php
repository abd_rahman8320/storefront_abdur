<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSpgroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_spgroup', function(Blueprint $table)
		{
			$table->smallInteger('spg_id', true)->unsigned();
			$table->string('spg_name', 150);
			$table->string('spg_keterangan', 100);
			$table->integer('spg_smc_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_spgroup');
	}

}
