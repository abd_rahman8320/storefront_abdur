<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProvinceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_province', function(Blueprint $table)
		{
			$table->integer('prov_id', true);
			$table->string('prov_code', 100);
			$table->string('prov_name', 100);
			$table->integer('prov_co_id');
			$table->boolean('prov_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_province');
	}

}
