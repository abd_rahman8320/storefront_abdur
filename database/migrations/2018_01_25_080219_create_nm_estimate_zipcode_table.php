<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmEstimateZipcodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_estimate_zipcode', function(Blueprint $table)
		{
			$table->integer('ez_id', true);
			$table->integer('ez_code_series');
			$table->integer('ez_code_series_end');
			$table->integer('ez_code_days');
			$table->boolean('ez_status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_estimate_zipcode');
	}

}
