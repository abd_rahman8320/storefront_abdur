<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmWithdrawRequestDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_withdraw_request_detail', function(Blueprint $table)
		{
			$table->integer('wdd_id', true);
			$table->integer('wdd_wd_id');
			$table->string('wdd_transaction_id');
			$table->integer('wdd_pro_id');
			$table->integer('wdd_order_qty');
			$table->integer('wdd_order_amt');
			$table->integer('wdd_order_shipping_price');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_withdraw_request_detail');
	}

}
