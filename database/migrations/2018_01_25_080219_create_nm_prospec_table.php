<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProspecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_prospec', function(Blueprint $table)
		{
			$table->bigInteger('spc_id', true)->unsigned();
			$table->integer('spc_pro_id')->unsigned();
			$table->smallInteger('spc_sp_id')->unsigned();
			$table->text('spc_value', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_prospec');
	}

}
