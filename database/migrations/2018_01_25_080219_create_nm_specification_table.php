<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSpecificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_specification', function(Blueprint $table)
		{
			//$table->primary('sp_id');
			$table->smallInteger('sp_id', true)->unsigned();
			$table->text('sp_name', 65535);
			$table->smallInteger('sp_spg_id')->unsigned();
			$table->smallInteger('sp_order')->unsigned();
			$table->string('sp_type', 50)->default('Text');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_specification');
	}

}
