<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmEmailblastTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_emailblast', function(Blueprint $table)
		{
			$table->integer('id_emb', true);
			$table->string('judul', 150);
			$table->string('subject', 150);
			$table->text('isi', 65535);
			$table->dateTime('tanggal_kirim')->nullable();
			$table->integer('status_email')->nullable()->comment('1-send, 0-notsend');
			$table->integer('status')->nullable()->comment('1-block,0-unblock');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_emailblast');
	}

}
