<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmReferaluserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_referaluser', function(Blueprint $table)
		{
			$table->increments('ruse_id');
			$table->dateTime('ruse_date');
			$table->string('ruse_name', 100);
			$table->string('ruse_emailid', 150);
			$table->integer('ruse_userid')->unsigned();
			$table->boolean('ruse_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_referaluser');
	}

}
