<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmDistrictsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_districts', function(Blueprint $table)
		{
			$table->integer('dis_id', true);
			$table->string('dis_code', 100);
			$table->string('dis_name', 1000);
			$table->integer('dis_city_id');
			$table->boolean('dis_status')->default(1)->comment('0-deactive, 1-active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_districts');
	}

}
