<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmOrdercodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_ordercod', function(Blueprint $table)
		{
			$table->increments('cod_id');
			$table->string('cod_transaction_id', 60);
			$table->integer('cod_cus_id')->unsigned();
			$table->integer('cod_pro_id')->unsigned();
			$table->boolean('cod_order_type')->comment('1-product,2-deals');
			$table->integer('cod_qty');
			$table->decimal('cod_amt', 10);
			$table->decimal('cod_tax', 10);
			$table->timestamp('cod_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('cod_status')->comment('1-sucess,2-complete,3-hold,4-failed');
			$table->string('created_date', 20);
			$table->smallInteger('cod_paytype');
			$table->boolean('cod_pro_color');
			$table->boolean('cod_pro_size');
			$table->text('cod_ship_addr', 65535);
			$table->integer('cod_merchant_id');
			$table->string('postalservice_code', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_ordercod');
	}

}
