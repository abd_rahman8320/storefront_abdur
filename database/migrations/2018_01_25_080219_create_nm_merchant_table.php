<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_merchant', function(Blueprint $table)
		{
			$table->increments('mer_id');
			$table->string('addedby', 20);
			$table->string('mer_fname', 150);
			$table->string('mer_lname', 150);
			$table->string('mer_password', 150);
			$table->string('mer_email', 150);
			$table->string('mer_phone', 20);
			$table->string('mer_address1', 150);
			$table->string('mer_address2', 150);
			$table->integer('mer_ci_id')->unsigned()->comment('city id');
			$table->smallInteger('mer_co_id')->unsigned()->comment('country id');
			$table->integer('mer_prov_id');
			$table->integer('mer_dis_id');
			$table->integer('mer_sdis_id');
			$table->string('mer_payment', 100)->nullable();
			$table->boolean('mer_commission');
			$table->dateTime('created_date');
			$table->boolean('mer_staus')->default(1)->comment('1-unblock,0-block');
			$table->string('nama_bank');
			$table->string('nomor_rekening');
			$table->string('nama_pemilik_rekening');
			$table->integer('status_approval')->default(1)->comment('1 - registered, 2 - approved');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_merchant');
	}

}
