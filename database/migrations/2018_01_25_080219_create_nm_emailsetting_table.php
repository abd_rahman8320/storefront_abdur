<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmEmailsettingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_emailsetting', function(Blueprint $table)
		{
			$table->boolean('es_id')->primary();
			$table->string('es_contactname', 150);
			$table->string('es_contactemail', 150);
			$table->string('es_webmasteremail', 150);
			$table->string('es_noreplyemail', 150);
			$table->string('es_phone1', 20);
			$table->string('es_phone2', 20);
			$table->decimal('es_latitude', 18, 14);
			$table->decimal('es_longitude', 18, 14);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_emailsetting');
	}

}
