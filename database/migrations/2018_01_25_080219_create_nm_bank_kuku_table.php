<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmBankKukuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_bank_kuku', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama_bank');
			$table->string('nomor_rekening');
			$table->string('nama_pemilik_rekening');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_bank_kuku');
	}

}
