<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmOrderAuctionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_order_auction', function(Blueprint $table)
		{
			$table->integer('oa_id', true);
			$table->integer('oa_pro_id');
			$table->integer('oa_cus_id');
			$table->string('oa_cus_name', 150);
			$table->string('oa_cus_email', 250);
			$table->text('oa_cus_address', 65535);
			$table->integer('oa_bid_amt');
			$table->integer('oa_bid_shipping_amt');
			$table->integer('oa_original_bit_amt');
			$table->timestamp('oa_bid_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('oa_bid_winner')->comment('1=> Winner, 0=> Bidders');
			$table->integer('oa_bid_item_status')->comment('0=> Onprocess, 1=> Send,  3=>Cancelled');
			$table->dateTime('oa_delivery_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_order_auction');
	}

}
