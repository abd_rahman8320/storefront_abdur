<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmKuponTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_kupon', function(Blueprint $table)
		{
			$table->integer('id_kupon');
			$table->string('nomor_kupon', 599);
			$table->integer('status');
			$table->integer('schedp_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_kupon');
	}

}
