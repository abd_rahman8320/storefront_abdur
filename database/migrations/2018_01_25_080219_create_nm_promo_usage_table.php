<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPromoUsageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_promo_usage', function(Blueprint $table)
		{
			$table->integer('promou_id', true);
			$table->integer('promou_cus_id');
			$table->integer('promou_promoc_id');
			$table->integer('promou_usage');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_promo_usage');
	}

}
