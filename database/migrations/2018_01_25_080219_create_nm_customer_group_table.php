<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmCustomerGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_customer_group', function(Blueprint $table)
		{
			$table->increments('cus_group_id');
			$table->string('cus_group_code', 32)->comment('Customer Group Code')->nullable();
			$table->string('cus_group_desc', 100);
			$table->integer('tax_class_id')->unsigned()->default(0)->comment('Tax Class Id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_customer_group');
	}

}
