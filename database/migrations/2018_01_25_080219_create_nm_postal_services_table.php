<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPostalServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_postal_services', function(Blueprint $table)
		{
			$table->integer('ps_id', true);
			$table->string('ps_code');
			$table->string('ps_vendor');
			$table->string('ps_service_name');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_postal_services');
	}

}
