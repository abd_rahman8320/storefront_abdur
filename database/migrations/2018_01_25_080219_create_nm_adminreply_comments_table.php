<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmAdminreplyCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_adminreply_comments', function(Blueprint $table)
		{
			$table->integer('reply_id', true);
			$table->integer('reply_blog_id');
			$table->integer('reply_cmt_id');
			$table->text('reply_msg', 65535);
			$table->timestamp('reply_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_adminreply_comments');
	}

}
