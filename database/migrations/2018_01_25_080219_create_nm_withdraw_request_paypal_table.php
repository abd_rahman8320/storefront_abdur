<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmWithdrawRequestPaypalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_withdraw_request_paypal', function(Blueprint $table)
		{
			$table->integer('wr_id', true);
			$table->integer('wr_mer_id');
			$table->string('wr_mer_name', 250);
			$table->string('wr_mer_payment_email', 250);
			$table->string('wr_paid_amount', 250);
			$table->string('wr_txn_id', 250);
			$table->string('wr_status', 100);
			$table->timestamp('wr_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_withdraw_request_paypal');
	}

}
