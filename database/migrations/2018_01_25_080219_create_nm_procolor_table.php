<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProcolorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_procolor', function(Blueprint $table)
		{
			$table->bigInteger('pc_id', true)->unsigned();
			$table->integer('pc_pro_id')->unsigned();
			$table->smallInteger('pc_co_id')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_procolor');
	}

}
