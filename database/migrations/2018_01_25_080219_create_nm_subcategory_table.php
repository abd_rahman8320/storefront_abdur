<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSubcategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_subcategory', function(Blueprint $table)
		{
			$table->smallInteger('sb_id', true)->unsigned();
			$table->string('sb_name', 100);
			$table->smallInteger('sb_smc_id')->unsigned();
			$table->smallInteger('mc_id')->unsigned();
			$table->boolean('sb_status');
			$table->string('sb_code', 10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_subcategory');
	}

}
