<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmAdminTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_admin', function(Blueprint $table)
		{
			$table->increments('adm_id');
			$table->string('adm_fname', 150);
			$table->string('adm_lname', 150);
			$table->string('adm_password', 150);
			$table->string('adm_email', 150);
			$table->string('adm_phone', 20);
			$table->string('adm_address1', 150);
			$table->string('adm_address2', 150);
			$table->integer('adm_ci_id')->unsigned()->comment('city id');
			$table->smallInteger('adm_co_id')->unsigned()->comment('country id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_admin');
	}

}
