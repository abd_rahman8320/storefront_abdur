<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmBlogsettingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_blogsetting', function(Blueprint $table)
		{
			$table->boolean('bs_id')->primary();
			$table->boolean('bs_allowcommt');
			$table->boolean('bs_radminapproval')->comment('Require Admin Approval ');
			$table->boolean('bs_postsppage');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_blogsetting');
	}

}
