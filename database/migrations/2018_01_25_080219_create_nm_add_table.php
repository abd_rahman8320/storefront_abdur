<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmAddTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_add', function(Blueprint $table)
		{
			$table->smallInteger('ad_id', true)->unsigned();
			$table->string('ad_name', 100);
			$table->boolean('ad_position')->comment('1-Header right,2-Left side bar,3-Bottom footer');
			$table->boolean('ad_pages')->comment('1-home,2-product,3-Deal,4-Auction');
			$table->string('ad_redirecturl', 150);
			$table->string('ad_img', 150);
			$table->integer('ad_type')->default(1)->comment('1-admin 2 customer');
			$table->boolean('ad_status');
			$table->integer('ad_read_status')->default(0)->comment('0-not read 1 read');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_add');
	}

}
