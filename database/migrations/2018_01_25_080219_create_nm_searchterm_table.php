<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSearchtermTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_searchterm', function(Blueprint $table)
		{
			$table->string('term', 50);
			$table->integer('search_term_uses_count');
			$table->integer('search_result');
			$table->timestamp('search_term_uses_time')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_searchterm');
	}

}
