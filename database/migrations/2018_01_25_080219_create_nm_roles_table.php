<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_roles', function(Blueprint $table)
		{
			$table->integer('roles_id', true);
			$table->string('roles_name', 150)->unique('roles_name');
			$table->string('roles_description', 500);
			$table->string('roles_label', 200);
			$table->string('created_by', 150);
			$table->dateTime('created_ts');
			$table->string('lastupdated_by', 150);
			$table->timestamp('lastupdated_ts')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('deleted_by', 150);
			$table->dateTime('deleted_ts');
			$table->boolean('is_deleted');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_roles');
	}

}
