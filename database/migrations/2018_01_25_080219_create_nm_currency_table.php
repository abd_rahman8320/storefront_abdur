<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_currency', function(Blueprint $table)
		{
			$table->increments('cur_id');
			$table->string('cur_name', 100);
			$table->string('cur_code', 5);
			$table->string('cur_symbol', 10);
			$table->boolean('cur_status')->default(1);
			$table->boolean('cur_default');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_currency');
	}

}
