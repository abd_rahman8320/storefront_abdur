<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_country', function(Blueprint $table)
		{
			$table->smallInteger('co_id', true)->unsigned();
			$table->string('co_code', 10);
			$table->string('co_name', 30);
			$table->string('co_cursymbol', 5);
			$table->string('co_curcode', 10);
			$table->boolean('co_status')->comment('1-block,0-unblock');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_country');
	}

}
