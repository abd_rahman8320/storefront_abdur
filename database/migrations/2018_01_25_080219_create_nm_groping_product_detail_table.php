<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmGropingProductDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_groping_product_detail', function(Blueprint $table)
		{
			$table->integer('group_detail_id', true);
			$table->integer('group_detail_id_grouping');
			$table->integer('group_detail_id_product');
			$table->integer('group_detail_sort');
			$table->string('status');
			$table->date('created_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_groping_product_detail');
	}

}
