<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthAccessTokenScopesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_access_token_scopes', function(Blueprint $table)
		{
			$table->foreign('access_token_id')->references('id')->on('oauth_access_tokens')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('scope_id')->references('id')->on('oauth_scopes')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_access_token_scopes', function(Blueprint $table)
		{
			$table->dropForeign('oauth_access_token_scopes_access_token_id_foreign');
			$table->dropForeign('oauth_access_token_scopes_scope_id_foreign');
		});
	}

}
