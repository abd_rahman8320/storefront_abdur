<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPartnersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_partners', function(Blueprint $table)
		{
			$table->smallInteger('par_id', true)->unsigned();
			$table->string('par_name', 100);
			$table->boolean('par_position')->comment('1-Header right,2-Left side bar,3-Bottom footer ');
			$table->boolean('par_pages')->comment('1-home,2-product,3-Deal,4-Auction ');
			$table->string('par_redirecturl', 150);
			$table->string('par_img', 150);
			$table->integer('par_type')->default(1)->comment('1-admin 2 customer');
			$table->boolean('par_status');
			$table->integer('par_read_status')->default(0)->comment(' 0-not read 1 read');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_partners');
	}

}
