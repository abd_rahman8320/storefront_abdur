<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmEnquiryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_enquiry', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->text('email', 65535);
			$table->string('phone');
			$table->text('message', 65535);
			$table->integer('status');
			$table->dateTime('created_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_enquiry');
	}

}
