<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPromoCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_promo_coupons', function(Blueprint $table)
		{
			$table->integer('promoc_id', true);
			$table->integer('promoc_schedp_id');
			$table->string('promoc_voucher_code')->nullable();
			$table->integer('promoc_usage_limit')->unsigned();
			$table->integer('promoc_usage_per_customer')->unsigned();
			$table->integer('promoc_times_used')->unsigned()->default(0);
			$table->smallInteger('promoc_is_primary')->unsigned()->nullable();
			$table->smallInteger('promoc_type')->nullable()->default(0)->comment('Coupon Code Type');
			$table->integer('promoc_minimal_transaction')->default(0);
			$table->dateTime('created_date');
			$table->dateTime('modified_date');
			$table->string('status_coupons');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_promo_coupons');
	}

}
