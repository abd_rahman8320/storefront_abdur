<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProsizeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_prosize', function(Blueprint $table)
		{
			$table->bigInteger('ps_id', true)->unsigned();
			$table->integer('ps_pro_id')->unsigned();
			$table->smallInteger('ps_si_id')->unsigned();
			$table->integer('ps_volume')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_prosize');
	}

}
