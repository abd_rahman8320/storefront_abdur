<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPaymentsettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_paymentsettings', function(Blueprint $table)
		{
			$table->boolean('ps_id')->primary();
			$table->decimal('ps_flatshipping', 10)->comment('shipping Tax Percentage');
			$table->boolean('ps_taxpercentage');
			$table->smallInteger('ps_extenddays')->unsigned()->comment('Auction Extend Days');
			$table->integer('ps_alertdays');
			$table->integer('ps_minfundrequest')->unsigned();
			$table->integer('ps_maxfundrequest')->unsigned();
			$table->integer('ps_referralamount');
			$table->integer('ps_countryid');
			$table->string('ps_countrycode', 10);
			$table->string('ps_cursymbol', 10);
			$table->string('ps_curcode', 10);
			$table->string('ps_paypalaccount', 150);
			$table->string('ps_paypal_api_pw', 250);
			$table->string('ps_paypal_api_signature', 250);
			$table->string('ps_authorize_trans_key', 250);
			$table->string('ps_authorize_api_id', 250);
			$table->boolean('ps_paypal_pay_mode')->comment('0->Test Mode, 1-> Live Mode');
			$table->string('ps_bankbranchname');
			$table->string('ps_bankaccountname');
			$table->string('ps_bankaccountno');
			$table->integer('ps_lama_cicilan_columbia');
			$table->integer('ps_biaya_columbia');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_paymentsettings');
	}

}
