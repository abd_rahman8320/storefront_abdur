<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmSecmaincategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_secmaincategory', function(Blueprint $table)
		{
			$table->smallInteger('smc_id', true)->unsigned();
			$table->string('smc_name', 100);
			$table->smallInteger('smc_mc_id')->unsigned();
			$table->boolean('smc_status');
			$table->string('smc_code', 10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_secmaincategory');
	}

}
