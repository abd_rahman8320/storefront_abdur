<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmScheduledPromoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_scheduled_promo', function(Blueprint $table)
		{
			$table->integer('schedp_id', true);
			$table->string('schedp_title', 500);
			$table->dateTime('schedp_start_date');
			$table->dateTime('schedp_end_date');
			$table->text('schedp_description', 65535);
			$table->integer('schedp_merchant_id');
			$table->integer('schedp_shop_id');
			$table->integer('schedp_status')->default(1)->comment('1-active, 0-block');
			$table->integer('schedp_NumOccurrences')->nullable();
			$table->integer('schedp_RecurrenceType')->nullable();
			$table->integer('schedp_Int1')->nullable();
			$table->string('schedp_Int2', 100)->nullable();
			$table->integer('schedp_Int3')->nullable();
			$table->string('schedp_String1', 50)->nullable();
			$table->string('schedp_simple_action', 32)->nullable()->comment('Simple Action : cart_fixed, by_fixed, by_percent, buy_x_get_y');
			$table->integer('schedp_discount_amount')->default(0)->comment('Discount Amount');
			$table->decimal('schedp_discount_qty', 12, 4)->nullable()->comment('Discount Qty');
			$table->integer('schedp_discount_step')->unsigned()->comment('Discount Step for buy_x_get_y : set the step x');
			$table->smallInteger('schedp_simple_free_shipping')->unsigned()->default(0)->comment('Simple Free Shipping : set the quantity');
			$table->smallInteger('schedp_apply_to_shipping')->unsigned()->default(0)->comment('Apply To Shipping : 1 - True, 0 - False');
			$table->integer('schedp_times_used')->unsigned()->default(0)->comment('Times Used : counter');
			$table->smallInteger('schedp_is_rss')->default(0)->comment('Is Rss');
			$table->smallInteger('schedp_coupon_type')->unsigned()->default(1)->comment('Coupon Type : 1 - All Product, 2 - Specific Product');
			$table->smallInteger('schedp_use_auto_generation')->default(0)->comment('Use Auto Generation : 1 - True, 0 - False');
			$table->integer('schedp_uses_per_coupon')->default(0)->comment('Uses Per Coupon : set the quantity');
			$table->dateTime('created_date');
			$table->dateTime('modified_date');
			$table->integer('schedp_ps_id')->default(0);
			$table->integer('schedp_x')->default(0);
			$table->integer('schedp_y')->default(0);
			$table->integer('maximum_free_shipping');
			$table->string('jenis_voucher');
			$table->integer('jumlah_voucher');
			$table->string('kode_depan_voucher');
			$table->time('schedp_recurr_start_time');
			$table->time('schedp_recurr_end_time');
			$table->string('schedp_picture', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_scheduled_promo');
	}

}
