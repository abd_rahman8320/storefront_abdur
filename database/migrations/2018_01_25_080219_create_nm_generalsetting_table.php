<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmGeneralsettingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_generalsetting', function(Blueprint $table)
		{
			$table->boolean('gs_id')->primary();
			$table->string('gs_sitename', 100);
			$table->string('gs_metatitle', 150);
			$table->text('gs_metakeywords', 65535);
			$table->text('gs_metadesc', 65535);
			$table->boolean('gs_defaulttheme');
			$table->boolean('gs_defaultlanguage');
			$table->string('gs_payment_status', 50);
			$table->string('gs_themes', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_generalsetting');
	}

}
