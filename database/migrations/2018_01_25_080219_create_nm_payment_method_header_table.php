<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPaymentMethodHeaderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_payment_method_header', function(Blueprint $table)
		{
			$table->integer('id_pay_header', true);
			$table->string('nama_pay_header');
			$table->integer('id_pay_detail')->nullable();
			$table->string('channel_pay_header', 10)->nullable();
			$table->integer('cus_group_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_payment_method_header');
	}

}
