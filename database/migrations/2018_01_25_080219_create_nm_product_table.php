<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_product', function(Blueprint $table)
		{
			$table->integer('pro_no_of_purchase');
			$table->increments('pro_id');
			$table->string('pro_sku', 16);
			$table->string('pro_sku_merchant', 16);
			$table->string('pro_title', 150);
			$table->smallInteger('pro_mc_id')->unsigned()->nullable();
			$table->smallInteger('pro_smc_id')->unsigned();
			$table->smallInteger('pro_sb_id')->unsigned();
			$table->smallInteger('pro_ssb_id')->unsigned();
			$table->decimal('pro_price', 10);
			$table->decimal('pro_disprice', 10);
			$table->boolean('pro_inctax')->nullable();
			$table->decimal('pro_shippamt', 10);
			$table->text('pro_desc', 65535);
			$table->boolean('pro_isspec')->comment('1-yes 2-no');
			$table->smallInteger('pro_delivery')->unsigned()->nullable()->comment('in days');
			$table->integer('pro_mr_id')->unsigned();
			$table->smallInteger('pro_sh_id')->unsigned();
			$table->string('pro_mtitle', 100)->nullable();
			$table->text('pro_mkeywords', 65535)->nullable();
			$table->text('pro_mdesc', 65535)->nullable()->comment('metadescription');
			$table->boolean('pro_postfacebook');
			$table->string('pro_Img', 500);
			$table->date('created_date');
			$table->boolean('pro_status')->comment('1=> Active, 0 => Block');
			$table->integer('pro_image_count');
			$table->integer('pro_qty');
			$table->integer('sold_status')->default(1);
			$table->float('pro_weight', 10);
			$table->float('pro_length', 10);
			$table->float('pro_width', 10);
			$table->float('pro_height', 10);
			$table->boolean('pro_isapproved')->default(0);
			$table->boolean('pro_isresponse')->default(0);
			$table->string('pro_message', 1000);
			$table->integer('pro_grade_id');
			$table->integer('pro_color_id');
			$table->integer('pro_poin');
			$table->integer('wholesale_level1_min')->nullable();
			$table->integer('wholesale_level1_max')->nullable();
			$table->decimal('wholesale_level1_price', 15)->nullable();
			$table->integer('wholesale_level2_min')->nullable();
			$table->integer('wholesale_level2_max')->nullable();
			$table->decimal('wholesale_level2_price', 15)->nullable();
			$table->integer('wholesale_level3_min')->nullable();
			$table->integer('wholesale_level3_max')->nullable();
			$table->decimal('wholesale_level3_price', 15)->nullable();
			$table->integer('wholesale_level4_min')->nullable();
			$table->integer('wholesale_level4_max')->nullable();
			$table->decimal('wholesale_level4_price', 15)->nullable();
			$table->integer('wholesale_level5_min')->nullable();
			$table->integer('wholesale_level5_max')->nullable();
			$table->decimal('wholesale_level5_price', 15)->nullable();
			$table->dateTime('activated_date');
			$table->integer('have_warranty');
			$table->integer('jumlah_cicilan_3bulan');
			$table->integer('jumlah_cicilan_6bulan');
			$table->integer('jumlah_cicilan_12bulan');
			$table->boolean('pro_issepulsa')->default(0);
			$table->integer('pro_sepulsa_id');
			$table->string('pro_sepulsa_type', 100);
			$table->string('pro_sepulsa_operator', 100);
			$table->integer('pro_sepulsa_nominal');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_product');
	}

}
