<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmHistoryOutboundTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_history_outbound', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('id_transaksi');
			$table->dateTime('tgl_update');
			$table->integer('status')->default(1)->comment('1 -> blm dilihat, 2->pick, 3 ->packing, 4->shipping, 5 -> receiving');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_history_outbound');
	}

}
