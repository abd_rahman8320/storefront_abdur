<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmGradeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_grade', function(Blueprint $table)
		{
			$table->integer('grade_id', true);
			$table->string('grade_name', 100);
			$table->string('grade_code', 100);
			$table->string('grade_keterangan', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_grade');
	}

}
