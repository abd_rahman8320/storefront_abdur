<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOauthClientGrantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oauth_client_grants', function(Blueprint $table)
		{
			$table->foreign('client_id')->references('id')->on('oauth_clients')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('grant_id')->references('id')->on('oauth_grants')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_client_grants', function(Blueprint $table)
		{
			$table->dropForeign('oauth_client_grants_client_id_foreign');
			$table->dropForeign('oauth_client_grants_grant_id_foreign');
		});
	}

}
