<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmLoginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_login', function(Blueprint $table)
		{
			$table->integer('log_id', true);
			$table->integer('cus_id');
			$table->timestamp('log_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('log_type')->default(1)->comment('1-wesite,2 facebook');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_login');
	}

}
