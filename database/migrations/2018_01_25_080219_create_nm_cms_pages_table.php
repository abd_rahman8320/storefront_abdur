<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmCmsPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_cms_pages', function(Blueprint $table)
		{
			$table->smallInteger('cp_id', true)->unsigned();
			$table->string('cp_title', 250);
			$table->text('cp_description');
			$table->boolean('cp_status')->default(1);
			$table->dateTime('cp_created_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_cms_pages');
	}

}
