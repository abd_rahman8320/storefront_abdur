<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmColorfixedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_colorfixed', function(Blueprint $table)
		{
			$table->smallInteger('cf_id', true)->unsigned();
			$table->string('cf_code', 10);
			$table->string('cf_name', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_colorfixed');
	}

}
