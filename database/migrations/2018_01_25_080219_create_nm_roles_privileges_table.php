<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmRolesPrivilegesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_roles_privileges', function(Blueprint $table)
		{
			$table->integer('rp_id', true);
			$table->string('rp_roles_name', 150);
			$table->string('rp_priv_name', 150);
			$table->string('created_by', 150);
			$table->dateTime('created_ts');
			$table->string('lastupdated_by', 150);
			$table->timestamp('lastupdated_ts')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('deleted_by', 150);
			$table->dateTime('deleted_ts');
			$table->boolean('is_deleted');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_roles_privileges');
	}

}
