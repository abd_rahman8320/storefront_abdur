<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmBlogCusCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_blog_cus_comments', function(Blueprint $table)
		{
			$table->integer('cmt_id', true);
			$table->integer('cmt_blog_id');
			$table->string('cmt_name', 250);
			$table->string('cmt_email', 250);
			$table->string('cmt_website', 250);
			$table->text('cmt_msg', 65535);
			$table->integer('cmt_admin_approve')->default(0)->comment('1 => Approved, 2 => Unapproved');
			$table->timestamp('cmt_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('cmt_msg_status')->default(0)->comment('0-not read ,1 Read ');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_blog_cus_comments');
	}

}
