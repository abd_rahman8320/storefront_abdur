<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_customer', function(Blueprint $table)
		{
			$table->bigInteger('cus_id', true)->unsigned();
			$table->string('cus_name', 100);
			$table->string('facebook_id', 150);
			$table->string('cus_email', 150);
			$table->string('cus_pwd', 40);
			$table->string('cus_phone', 20);
			$table->string('cus_address1', 150);
			$table->string('cus_address2', 150);
			$table->string('cus_country', 50);
			$table->string('cus_city', 50);
			$table->timestamp('cus_joindate')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('cus_logintype')->default(1)->comment('1=>Admin user, 2=> Website User, 3=> Facebook User');
			$table->integer('cus_status')->comment('0 unblock 1 block');
			$table->string('cus_pic', 150);
			$table->date('created_date');
			$table->integer('total_poin');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_customer');
	}

}
