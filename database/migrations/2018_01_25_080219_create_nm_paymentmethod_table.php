<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPaymentmethodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_paymentmethod', function(Blueprint $table)
		{
			$table->integer('pm_id', true);
			$table->string('pm_title', 100);
			$table->string('pm_type', 10)->comment('1-home');
			$table->string('pm_image', 150);
			$table->integer('pm_status')->comment('1-block,0-unblock');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_paymentmethod');
	}

}
