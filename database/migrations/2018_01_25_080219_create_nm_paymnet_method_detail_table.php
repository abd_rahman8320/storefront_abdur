<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmPaymnetMethodDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_paymnet_method_detail', function(Blueprint $table)
		{
			$table->integer('id_pay_detail', true);
			$table->string('nama_pay_detail');
			$table->integer('kode_pay_detail');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_paymnet_method_detail');
	}

}
