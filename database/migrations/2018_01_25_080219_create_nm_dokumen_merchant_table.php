<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmDokumenMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_dokumen_merchant', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_merchant');
			$table->string('jenis_dokumen');
			$table->binary('base64_file', 16777215);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_dokumen_merchant');
	}

}
