<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_order', function(Blueprint $table)
		{
			$table->increments('order_id');
			$table->integer('order_cus_id')->unsigned();
			$table->integer('order_pro_id')->unsigned();
			$table->boolean('order_type')->comment('1-product,2-deals');
			$table->string('transaction_id', 50);
			$table->string('payer_email', 50);
			$table->string('payer_id', 50);
			$table->string('payer_name', 100);
			$table->integer('order_qty');
			$table->decimal('order_amt', 10);
			$table->decimal('order_tax', 10);
			$table->string('currency_code', 10);
			$table->string('token_id', 30);
			$table->string('payment_ack', 10);
			$table->timestamp('order_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('created_date', 20);
			$table->string('payer_status', 50);
			$table->boolean('order_status')->comment('1-sucess,2-complete,3-hold,4-failed');
			$table->smallInteger('order_paytype')->comment('1-paypal, 2-doku');
			$table->integer('order_pro_color');
			$table->integer('order_pro_size');
			$table->text('order_shipping_add', 65535);
			$table->integer('order_merchant_id');
			$table->string('ip_address', 16)->default('');
			$table->string('process_type', 15)->default('');
			$table->dateTime('process_datetime')->nullable();
			$table->dateTime('doku_payment_datetime')->nullable();
			$table->string('transidmerchant', 30)->default('');
			$table->string('notify_type', 1)->default('');
			$table->string('response_code', 4)->default('');
			$table->string('status_code', 4)->default('');
			$table->string('result_msg', 20)->default('');
			$table->integer('reversal')->default(0);
			$table->char('approval_code', 20)->default('');
			$table->string('payment_channel', 2)->default('');
			$table->string('payment_code', 20)->default('');
			$table->string('bank_issuer', 100)->default('');
			$table->string('creditcard', 16)->default('');
			$table->string('words', 200)->default('');
			$table->string('session_id', 48)->default('');
			$table->string('verify_id', 30)->default('');
			$table->integer('verify_score')->default(0);
			$table->string('verify_status', 10)->default('');
			$table->integer('check_status')->default(0);
			$table->integer('count_check_status')->default(0);
			$table->text('message', 65535)->nullable();
			$table->integer('id')->nullable();
			$table->string('postalservice_code', 120);
			$table->decimal('order_dp', 10);
			$table->decimal('order_adminfee', 10);
			$table->integer('order_termin');
			$table->decimal('order_installment', 10);
			$table->boolean('is_transfer_bank')->default(0);
			$table->dateTime('order_expire');
			$table->string('insert_id', 50);
			$table->string('insert_status', 50);
			$table->string('redirect_url', 50);
			$table->integer('acc_shipped_qty')->default(0);
			$table->integer('shipped_qty');
			$table->string('basket');
			$table->string('address');
			$table->string('phone');
			$table->string('booking_code');
			$table->integer('status_outbound')->default(1);
			$table->string('payment_status', 10)->nullable();
			$table->dateTime('order_tgl_pesanan_dibayarkan');
			$table->dateTime('order_tgl_pesanan_diproses');
			$table->dateTime('order_tgl_pesanan_dikemas');
			$table->dateTime('order_tgl_pesanan_dikirim');
			$table->dateTime('order_tgl_konfirmasi_penerimaan_barang');
			$table->dateTime('order_garansi_expired');
			$table->string('order_nomor_resi', 100);
			$table->boolean('is_funded')->default(0);
			$table->boolean('is_fund_request')->default(0);
			$table->integer('order_shipping_price');
			$table->integer('order_use_warranty')->comment('1 -> yes 0 -> no ');
			$table->integer('order_jumlah_warranty');
			$table->boolean('order_is_cicilan');
			$table->string('order_sepulsa_customer_number', 100);
			$table->string('order_sepulsa_operator_code', 100);
			$table->string('order_sepulsa_meter_number', 100);
			$table->string('order_sepulsa_payment_period', 100);
			$table->string('order_sepulsa_response_code', 16);
			$table->string('order_sepulsa_transaction_id', 100);
			$table->string('order_customer_account', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_order');
	}

}
