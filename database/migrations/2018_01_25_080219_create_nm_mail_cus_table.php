<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNmMailCusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nm_mail_cus', function(Blueprint $table)
		{
			$table->integer('mail_id', true);
			$table->integer('first_name');
			$table->integer('cus_name');
			$table->integer('password');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nm_mail_cus');
	}

}
