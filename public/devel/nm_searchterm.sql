-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2017 at 09:08 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefro_test1`
--

-- --------------------------------------------------------

--
-- Table structure for table `nm_searchterm`
--

CREATE TABLE `nm_searchterm` (
  `term` varchar(50) NOT NULL,
  `search_term_uses_count` int(50) NOT NULL,
  `search_result` int(10) NOT NULL,
  `search_term_uses_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_searchterm`
--

INSERT INTO `nm_searchterm` (`term`, `search_term_uses_count`, `search_result`, `search_term_uses_time`) VALUES
('sendals', 10, 0, '2017-02-28 08:42:06'),
('nokia', 20, 0, '2017-02-28 08:42:06'),
('celana', 1, 0, '2017-02-28 08:43:02'),
('gelas', 1, 0, '2017-02-28 08:43:02'),
('kipas', 3, 0, '2017-02-28 08:44:35'),
('piring', 5, 0, '2017-02-28 08:44:35');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
