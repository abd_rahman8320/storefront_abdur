-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Apr 2017 pada 04.41
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_promo_coupons`
--

CREATE TABLE `nm_promo_coupons` (
  `promoc_id` int(11) NOT NULL,
  `promoc_schedp_id` int(11) NOT NULL,
  `promoc_voucher_code` varchar(255) DEFAULT NULL,
  `promoc_usage_limit` int(10) UNSIGNED DEFAULT NULL,
  `promoc_usage_per_customer` int(10) UNSIGNED DEFAULT NULL,
  `promoc_times_used` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `promoc_is_primary` smallint(5) UNSIGNED DEFAULT NULL,
  `promoc_type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_promo_coupons`
--

INSERT INTO `nm_promo_coupons` (`promoc_id`, `promoc_schedp_id`, `promoc_voucher_code`, `promoc_usage_limit`, `promoc_usage_per_customer`, `promoc_times_used`, `promoc_is_primary`, `promoc_type`, `created_date`, `modified_date`) VALUES
(1, 1, 'LBR2017', NULL, NULL, 0, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 'RMD2017', NULL, NULL, 0, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 'LBR20', NULL, NULL, 0, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_promo_coupons`
--
ALTER TABLE `nm_promo_coupons`
  ADD PRIMARY KEY (`promoc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_promo_coupons`
--
ALTER TABLE `nm_promo_coupons`
  MODIFY `promoc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
