-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2017 at 09:21 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Table structure for table `nm_subdistricts`
--

CREATE TABLE `nm_subdistricts` (
  `sdis_id` int(255) NOT NULL,
  `sdis_code` varchar(100) NOT NULL,
  `sdis_name` varchar(1000) NOT NULL,
  `sdis_dis_id` int(255) NOT NULL,
  `sdis_zip_code` varchar(1000) NOT NULL,
  `sdis_status` tinyint(2) NOT NULL COMMENT '0-deactive, 1-active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_subdistricts`
--
ALTER TABLE `nm_subdistricts`
  ADD PRIMARY KEY (`sdis_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_subdistricts`
--
ALTER TABLE `nm_subdistricts`
  MODIFY `sdis_id` int(255) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
