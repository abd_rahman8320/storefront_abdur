ALTER TABLE `nm_product` ADD `pro_weight` FLOAT(10,2) NOT NULL AFTER `sold_status`,
ADD `pro_length` FLOAT(10,2) NOT NULL AFTER `pro_weight`, 
ADD `pro_width` FLOAT(10,2) NOT NULL AFTER `pro_length`, 
ADD `pro_height` FLOAT(10,2) NOT NULL AFTER `pro_width`;