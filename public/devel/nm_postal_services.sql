-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Mar 2017 pada 07.53
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_postal_services`
--

CREATE TABLE `nm_postal_services` (
  `ps_id` int(11) NOT NULL,
  `ps_code` varchar(255) NOT NULL,
  `ps_vendor` varchar(255) NOT NULL,
  `ps_service_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_postal_services`
--

INSERT INTO `nm_postal_services` (`ps_id`, `ps_code`, `ps_vendor`, `ps_service_name`) VALUES
(1, 'JNE_REG', 'JNE', 'REGULER'),
(2, 'JNE_OK', 'JNE', 'OK'),
(3, 'JNE_YES', 'JNE', 'YES'),
(4, 'Columbia', 'Columbia', 'Columbia'),
(5, 'PopBox', 'PopBox', 'PopBox');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_postal_services`
--
ALTER TABLE `nm_postal_services`
  ADD PRIMARY KEY (`ps_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_postal_services`
--
ALTER TABLE `nm_postal_services`
  MODIFY `ps_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
