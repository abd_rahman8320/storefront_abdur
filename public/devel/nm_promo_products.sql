-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Apr 2017 pada 04.41
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_promo_products`
--

CREATE TABLE `nm_promo_products` (
  `promop_id` int(50) NOT NULL,
  `promop_schedp_id` int(50) NOT NULL,
  `promop_merchant_id` int(50) NOT NULL,
  `promop_shop_id` int(50) NOT NULL,
  `promop_pro_id` int(50) NOT NULL,
  `promop_status` int(50) NOT NULL,
  `promop_original_price` int(50) NOT NULL,
  `promop_discount_price` int(11) NOT NULL,
  `promop_discount_percentage` int(11) NOT NULL,
  `promop_saving_price` int(11) NOT NULL,
  `created_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP
) ;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_promo_products`
--
ALTER TABLE `nm_promo_products`
  MODIFY `promop_id` int(50) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
