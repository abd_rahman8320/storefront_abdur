-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Apr 2017 pada 04.41
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_scheduled_promo`
--

CREATE TABLE `nm_scheduled_promo` (
  `schedp_id` int(11) NOT NULL,
  `schedp_title` varchar(500) NOT NULL,
  `schedp_start_date` datetime NOT NULL,
  `schedp_end_date` datetime NOT NULL,
  `schedp_description` text NOT NULL,
  `schedp_merchant_id` int(11) NOT NULL,
  `schedp_shop_id` int(11) NOT NULL,
  `schedp_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `schedp_NumOccurrences` int(11) DEFAULT NULL,
  `schedp_RecurrenceType` int(11) DEFAULT NULL,
  `schedp_Int1` int(11) DEFAULT NULL,
  `schedp_Int2` int(11) DEFAULT NULL,
  `schedp_Int3` int(11) DEFAULT NULL,
  `schedp_String1` varchar(50) DEFAULT NULL,
  `schedp_simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action : cart_fixed, by_fixed, by_percent, buy_x_get_y',
  `schedp_discount_amount` decimal(12,0) NOT NULL DEFAULT '0' COMMENT 'Discount Amount',
  `schedp_discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `schedp_discount_step` int(10) UNSIGNED DEFAULT NULL COMMENT 'Discount Step for buy_x_get_y : set the step x',
  `schedp_simple_free_shipping` smallint(5) UNSIGNED DEFAULT '0' COMMENT 'Simple Free Shipping : set the quantity',
  `schedp_apply_to_shipping` smallint(5) UNSIGNED DEFAULT '0' COMMENT 'Apply To Shipping : 1 - True, 0 - False',
  `schedp_times_used` int(10) UNSIGNED DEFAULT '0' COMMENT 'Times Used : counter',
  `schedp_is_rss` smallint(6) DEFAULT '0' COMMENT 'Is Rss',
  `schedp_coupon_type` smallint(5) UNSIGNED DEFAULT '1' COMMENT 'Coupon Type : 1 - All Product, 2 - Specific Product',
  `schedp_use_auto_generation` smallint(6) DEFAULT '0' COMMENT 'Use Auto Generation : 1 - True, 0 - False',
  `schedp_uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Coupon : set the quantity',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `schedp_ps_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_scheduled_promo`
--

INSERT INTO `nm_scheduled_promo` (`schedp_id`, `schedp_title`, `schedp_start_date`, `schedp_end_date`, `schedp_description`, `schedp_merchant_id`, `schedp_shop_id`, `schedp_status`, `schedp_NumOccurrences`, `schedp_RecurrenceType`, `schedp_Int1`, `schedp_Int2`, `schedp_Int3`, `schedp_String1`, `schedp_simple_action`, `schedp_discount_amount`, `schedp_discount_qty`, `schedp_discount_step`, `schedp_simple_free_shipping`, `schedp_apply_to_shipping`, `schedp_times_used`, `schedp_is_rss`, `schedp_coupon_type`, `schedp_use_auto_generation`, `schedp_uses_per_coupon`, `created_date`, `modified_date`, `schedp_ps_id`) VALUES
(1, 'Diskon Lebaran', '2017-04-01 13:49:27', '2017-04-03 13:49:27', '', 0, 0, 1, NULL, 2, 0, 0, NULL, NULL, NULL, '0', NULL, NULL, 1, 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Free Shipping Ramadhan', '2017-04-01 15:09:30', '2017-04-30 15:09:30', '', 0, 0, 1, NULL, 2, 0, 0, NULL, NULL, NULL, '0', NULL, NULL, 1, 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
(3, 'Diskon Liburan 20%', '2017-04-01 17:48:58', '2017-05-31 17:48:58', '', 0, 0, 1, NULL, 2, 0, 0, NULL, NULL, 'by_percent', '20', NULL, NULL, 0, 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_scheduled_promo`
--
ALTER TABLE `nm_scheduled_promo`
  ADD PRIMARY KEY (`schedp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_scheduled_promo`
--
ALTER TABLE `nm_scheduled_promo`
  MODIFY `schedp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
