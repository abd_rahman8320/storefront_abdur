ALTER TABLE `nm_city` ADD `ci_code` VARCHAR(5) NOT NULL AFTER `ci_id`;

ALTER TABLE `nm_merchant` ADD `mer_code` CHAR(21) NOT NULL AFTER `mer_id`;

ALTER TABLE `nm_product` ADD `pro_sku` VARCHAR(255) NOT NULL AFTER `pro_id`;