/**
 * Harga Grosir
 **/
$(document).ready(function() {
    init_wholesale();
    check_format_price(1);
    check_range_price(1, true);
});
var switch_grosir = $("#wholesale-exist").val();

function init_wholesale() {
    if (switch_grosir == 1) {
        $(".switch_round_grosir").animate({
            left: "20px"
        }, 500);
        $(".switch_grosir").css("background", "#008000");
        $("#grosirbox").find('span').html('- Remove wholesale price');
        $(".custom-view-grosir").slideDown();
    } else {
        $(".switch_round_grosir").animate({
            left: "0px"
        }, 500);
        $(".switch_grosir").css("background", "#eee");
        $("#grosirbox").find('span').html('+ Add wholesale price');
        $(".custom-view-grosir").slideUp();
    }
}

$("#grosirbox").click(function() {
    if (switch_grosir == 0) {
        $(".switch_round_grosir").animate({
            left: "20px"
        }, 500);
        $(".switch_grosir").css("background", "#008000");
        $(this).find('span').html('- Remove wholesale price');
        switch_grosir++;
        $(".custom-view-grosir").slideDown();
    } else {
        $(".switch_round_grosir").animate({
            left: "0px"
        }, 500);
        $(".switch_grosir").css("background", "#eee");
        $(this).find('span').html('+ Add wholesale price');
        switch_grosir = 0;
        $(".custom-view-grosir").slideUp();
    }
});

$(document).on('keyup', ".price-grosir-new", function(e) {
    var row = $(this).attr('id').split('-').pop();
    check_wh_price(row);
    check_range_price(row, true);
});
$(document).on('keyup', ".qty-1, .qty-2", function(e) {
    var row = $(this).attr('id').split('-').pop();
    check_format_price(row);
    check_range_price(row, true);
});
$(document).on('focus', ".qty-1, .qty-2", function(e) {
    var row = $(this).attr('id').split('-').pop();
    check_format_price(row);
});

function check_format_price(row) {
    if ($('#qty-min-' + row).val() <= 0 && $('#qty-max-' + row).val() <= 0 && $('#prd-prc-' + row).val() <= 0) {
        $('#prd-prc-' + row).val('');
    }
}

$('.grosir-list input').on('keyup change', function() {
    var input_id = $(this).attr('id');
    var tmp_input_id = input_id.split('-');
    var row = tmp_input_id.pop();
    check_range_price(row, true);
});
$('#Original_Price').on('keyup change', function() {
    if ($('#qty-min-1').val() && $('#qty-max-1').val()) {
        check_range_price(1, true);
    }
});
$('body').on({
    click: function() {
        reset_selected($(this));
        return false;
    }
}, 'a.reset-wholesale');
$('body').on({
    click: function() {
        if ($(this).closest('ul').attr('class') != 'add_product_image') {
            reset_selected($(this).closest('span'));
            return false;
        }
    }
}, 'i.icon-remove');

function check_wh_price(row) {
    if ($('#qty-min-' + row).val() <= 0 && $('#qty-max-' + row).val() <= 0 && $('#prd-prc-' + row).val() <= 0) {
        $('#qty-min-' + row).focus();
    }
}

function check_range_price(row, check_others) {
    row = parseInt(row);
    var normal_price = $("#Original_Price");
    var qmin = $("#qty-min-" + row);
    var qmax = $("#qty-max-" + row);
    var prc = $("#prd-prc-" + row);
    var er = $("#error-grosir-" + row);
    var prev_qmin = $("#qty-min-" + parseInt(row - 1));
    var prev_qmax = $("#qty-max-" + parseInt(row - 1));
    var prev_prc = $("#prd-prc-" + parseInt(row - 1));
    var prev_er = $("#error-grosir-" + parseInt(row - 1));
    var next_qmin = $("#qty-min-" + (parseInt(row) + 1));
    var next_qmax = $("#qty-max-" + (parseInt(row) + 1));
    var next_prc = $("#prd-prc-" + (parseInt(row) + 1));
    var next_er = $("#error-grosir-" + (parseInt(row) + 1));
    var curr_price = prc.val();
    if (curr_price) {
        curr_price = curr_price.replace(/\,/g, "");
    }
    var norm_price = normal_price.val();
    if (curr_price) {
        norm_price = norm_price.replace(/\,/g, "");
    }
    var prev_price = prev_prc.val();
    if (prev_price) {
        prev_price = prev_price.replace(/\,/g, "");
    }
    if (prev_er.find('i').hasClass('icon-remove') && prev_qmin.val() && prev_qmax.val() && prev_prc.val()) {
        disabled_rest(row + 1);
        return false;
    }
    if (qmin.val() && parseInt(qmin.val()) <= parseInt(prev_qmax.val())) {
        err_msg = "Quantity range is not valid";
        er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
        er.find('.msg').html(err_msg);
        disabled_rest(row + 1);
        return false;
    }
    if ((qmin.val() && !qmax.val()) || (qmax.val() && !qmin.val()) || (prc.val() && !qmin.val()) || (prc.val() && !qmax.val())) {
        err_msg = "Quantity range is not complete";
        er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
        er.find('.msg').html(err_msg);
        disabled_rest(row + 1);
        return false;
    }
    if (qmin.val() && (parseInt(qmin.val()) > parseInt(qmax.val()) || qmin.val() <= 1 || isNaN(qmin.val()) || isNaN(qmax.val()))) {
        err_msg = "Quantity range is not valid";
        er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
        er.find('.msg').html(err_msg);
        disabled_rest(row + 1);
        return false;
    }
    if (qmin.val() == 1 && !normal_price.val()) {
        err_msg = "Please complete the product fixed price";
        normal_price.focus();
        er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
        er.find('.msg').html(err_msg);
        disabled_rest(row + 1);
        return false;
    }
    if (qmin.val() == 1) {
        if (curr_price != norm_price) {
            prc.val(norm_price);
            init_price_format();
        }
    } else {
        if (qmin.val() && qmax.val()) {}
        if (parseFloat(curr_price) >= parseFloat(norm_price)) {
            err_msg = "Price should be cheaper than the fixed price";
            er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
            er.find('.msg').html(err_msg);
            disabled_rest(row + 1);
            return;
        }
    }
    if (qmin.val() && qmax.val() && parseFloat(curr_price) >= parseFloat(prev_price)) {
        err_msg = "Price should be cheaper than the previous wholesale price";
        er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
        er.find('.msg').html(err_msg);
        disabled_rest(row + 1);
        return false;
    }
    er.find('.msg').html('');
    if (qmin.val() && qmax.val() && curr_price) {
        var check = '<a id="reset-' + row + '" class="reset-wholesale" href="#">Clear</a>';
        er.find('.msg').html(check);
        er.find('i').removeClass('icon-checked icon-remove');
    } else {
        er.find('i').removeClass('icon-checked icon-remove');
    }
    if (qmin.val() && qmin.val() && qmax.val() && curr_price && !er.find('i').hasClass('icon-remove')) {
        next_qmin.removeAttr("disabled");
        next_qmax.removeAttr("disabled");
        next_prc.removeAttr("disabled");
    }
    if (check_others) {
        for (var i = 1; i <= 5; i++) {
            if (i == row) {
                continue;
            }
            check_range_price(i, false);
        }
        warn_wholesale_normal_price_diff(30.0);
    }
    return true;
}

function disabled_rest(row) {
    for (var i = row; i <= 5; i++) {
        var loop_qmin = $("#qty-min-" + i);
        var loop_qmax = $("#qty-max-" + i);
        var loop_prc = $("#prd-prc-" + i);
        var loop_er = $("#error-grosir-" + i);
        var prev_qmin = $("#qty-min-" + parseInt(i - 1));
        var prev_qmax = $("#qty-max-" + parseInt(i - 1));
        var prev_prc = $("#prd-prc-" + parseInt(i - 1));
        var prev_er = $("#error-grosir-" + parseInt(i - 1));
        if (prev_er.find('i').hasClass('icon-remove') && prev_qmin.val() && prev_qmax.val() && prev_prc.val() || (prev_qmin.val() && prev_qmax.val())) {
            err_msg = "Please fix the previous error first";
            loop_qmin.attr('disabled', true);
            loop_qmax.attr('disabled', true);
            loop_prc.attr('disabled', true);
            if (loop_qmin.val() && loop_qmax.val() && loop_prc.val()) {
                loop_er.find('i').removeClass('icon-checked icon-remove').addClass('icon-remove').addClass('green');
                loop_er.find('.msg').html(err_msg);
            }
        }
    }
}

function reset_selected(this_row, row_no) {
    var row;
    if (this_row) {
        var selected = this_row.attr('id');
        row = selected.split('-').pop();
    } else {
        row = row_no;
    }
    for (var i = row; i < 6; i++) {
        if (i == row) {
            $("#qty-min-" + i).val('');
            $("#qty-max-" + i).val('');
            $("#prd-prc-" + i).val('');
        } else {
            $("#qty-min-" + i).val('').attr("disabled", true);
            $("#qty-max-" + i).val('').attr("disabled", true);
            $("#prd-prc-" + i).val('').attr("disabled", true);
        }
        $("#img-check-prc-" + i).val('');
        $("#hint-range-" + i).val('');
        $("#error-grosir-" + i).find('i').removeClass('icon-checked icon-remove');
        $("#error-grosir-" + i).find('.msg').html('');
    }
    check_range_price(row, true);
    if (this_row) {
        check_wh_price(row);
    }
}

function warn_wholesale_normal_price_diff(priceDiffPercentage) {
    var warnBox = $("#wholesale-price-alert");
    for (var i = 1; i <= 5; i++) {
        var diff = wholesale_normal_price_diff(i);
        if (isNaN(diff)) {
            continue;
        }
        if (diff > priceDiffPercentage) {
            warnBox.attr("hidden", false);
            return;
        }
    }
    warnBox.attr("hidden", true);
}

function wholesale_normal_price_diff(row) {
    row = parseInt(row);
    var normal_price = parsePrice("#Original_Price");
    var price = parsePrice("#prd-prc-" + row);
    if (normal_price === NaN || price === NaN) {
        return NaN;
    }
    return ((normal_price - price) / normal_price) * 100;
}

function parsePrice(queryElement) {
    var prc = $(queryElement).val();
    if (prc == undefined || prc == "") {
        return NaN;
    }
    prc = prc.replace(/\,/g, "");
    return parseFloat(prc);
}
