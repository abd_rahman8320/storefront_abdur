<?php

namespace App;
use Cache;
use Carbon\Carbon;

//Version 0.7.2f
//Helpers
class STFClient
{
    const CONFIG_FILENAME = "stf_client";

    const INVOICE_STATUS_PACKED = "PACKED";
    const INVOICE_STATUS_PICKED = "PICKED";
    const INVOICE_STATUS_SHIPPED = "SHIPPED";
    const INVOICE_STATUS_CANCELED = "CANCELED";

    /**
     * Update invoice status
     *
     * @return void
     */
    static function update_invoice($data)
    {
        $param = [
         "transaction_id" => $data["transaction_id"],
         "status" => $data["status"],
         "tracking_number" => self::getParam($data, "tracking_number"),
         "items" => self::getParam($data, "items"),
        ];

        $res = self::makeRequest(
            "api/update_invoice", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    // API STORE
    static function createstore($data)
    {
        $param = [
         "merchant_code"=>$data["merchant_code"],
         "stor_name"=>$data["stor_name"],
         "stor_phone"=>$data["stor_phone"],
        ];

        $res = self::makeRequest(
            "api/create_store", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        $resBody = json_decode($res->getBody(), false);
        //parse response
        if($res->getStatusCode() == 200) {
            //success
            $result["stor_merchant_id"] = $resBody->id;
        }else{
            //error
            $result["error"] = $resBody->error;
            $result["stor_merchant_id"] = null;
        }

        return $result;
    }

    // API PRODUCT
    static function createproduct($data)
    {
        $param = [
            "pro_sku"=>$data["pro_sku"],
            "pro_title"=>$data["pro_title"],
            "mc_code"=>$data["mc_code"],
            "smc_code" => self::getParam($data, "smc_code"),
            "sb_code" => self::getParam($data, "sb_code"),
            "ssb_code" => self::getParam($data, "ssb_code"),
            "pro_status" => self::getParam($data, "pro_status"),
            "pro_weight" => self::getParam($data, "pro_weight"),
            "pro_length" => self::getParam($data, "pro_length"),
            "pro_width" => self::getParam($data, "pro_width"),
            "pro_height" => self::getParam($data, "pro_height"),
            "pro_grade" => self::getParam($data, "pro_grade"),
            "pro_color" => self::getParam($data, "pro_color")
        ];

        $res = self::makeRequest(
            "api/create_product", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    /**
     * Mendapatkan url image product
     *
     * @param  type $data
     * @return type
     */
    static function getProductImage($sku)
    {
        return config(self::CONFIG_FILENAME.".public_url")."api/get_product_img?sku=".$sku;
    }

    static function updateStock($items)
    {
        $param = [
        "items" =>$items,
        ];

        $res = self::makeRequest(
            "api/update_stock", "PUT", [
            "form_params" => $param
            ]
        );

        $errors = json_decode($res->getBody())->errors;
        //parse response
        //        if(!empty($errors)){
        //            //success
        //			//throw new \Exception("Gagal sinkronisasi stok");
        //            $result = $errors;
        //        }

        return $errors;
    }

    static function insert_top_category($data)
    {
        $param = [
         "mc_name"=>$data["mc_name"],
         "mc_code"=>$data["mc_code"],
         "mc_status" => self::getParam($data, "mc_status"),
        ];

        $res = self::makeRequest(
            "api/insert_top_category", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "SUCCESS") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function insert_main_category($data)
    {
        $param = [
         "smc_name" => $data["smc_name"],
         "smc_code" => $data["smc_code"],
         "mc_code" => $data["mc_code"],
         "smc_status" => self::getParam($data, "smc_status"),
        ];

        $res = self::makeRequest(
            "api/insert_main_category", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "SUCCESS") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function insert_sub_category($data)
    {
        $param = [
         "sb_name" => $data["sb_name"],
         "sb_code" => $data["sb_code"],
         "smc_code" => $data["smc_code"],
         "sb_status" => self::getParam($data, "sb_status"),
        ];

        $res = self::makeRequest(
            "api/insert_sub_category", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "SUCCESS") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function insert_secsub_category($data)
    {
        $param = [
         "ssb_name" => $data["ssb_name"],
         "ssb_code" => $data["ssb_code"],
         "sb_code" => $data["sb_code"],
         "ssb_status" => self::getParam($data, "ssb_status"),
        ];

        $res = self::makeRequest(
            "api/insert_secsub_category", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "SUCCESS") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function createCountry($data)
    {
        $param = [
         "co_code" => $data["co_code"],
         "co_name" => $data["co_name"],
         "co_status" => self::getParam($data, "co_status"),
        ];

        $res = self::makeRequest(
            "api/create_country", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function updateCountry($data)
    {
        $param = [
         "co_code" => $data["co_code"],
            "co_name" => self::getParam($data, "co_name"),
            "cur_code" => self::getParam($data, "cur_code"),
            "cur_symbol" => self::getParam($data, "cur_symbol"),
         "co_status" => self::getParam($data, "co_status"),
        ];

        $res = self::makeRequest(
            "api/update_country", "PUT", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function deleteCountry($data)
    {
        $param = [
         "co_code" => $data["co_code"],
        ];

        $res = self::makeRequest(
            "api/delete_country", "DELETE", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    //API put Province
    static function putProvince($data)
    {
        $param = [
            'prov_code' => $data['code'],
            'prov_name' => self::getParam($data, 'name'),
            'prov_status' => self::getParam($data, 'status'),
            'prov_co_code' => self::getParam($data, 'country_code'),
        ];

        $res = self::makeRequest(
            'api/put_province', 'PUT', ['form_params' => $param]
        );

        $result = [];

        if ($res->getBody() != 'Sukses') {
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function createCity($data)
    {
        $param = [
         "ci_code" => $data["ci_code"],
         "ci_name" => $data["ci_name"],
         "ci_con_code" => $data["ci_con_code"],
         "ci_lati" => self::getParam($data, "ci_lati"),
         "ci_long" => self::getParam($data, "ci_long"),
         "ci_status" => self::getParam($data, "ci_status"),
        ];

        $res = self::makeRequest(
            "api/create_city", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function updateCity($data)
    {
        $param = [
            "ci_code" => $data["ci_code"],
            "ci_name" => self::getParam($data, "ci_name"),
            "ci_con_code" => self::getParam($data, "ci_con_code"),
            "ci_lati" => self::getParam($data, "ci_lati"),
            "ci_long" => self::getParam($data, "ci_long"),
            "ci_status" => self::getParam($data, "ci_status"),
        ];

        $res = self::makeRequest(
            "api/update_city", "PUT", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function deleteCity($data)
    {
        $param = [
         "ci_code" => $data["ci_code"]
        ];

        $res = self::makeRequest(
            "api/delete_city", "DELETE", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function putDistrict($data)
    {
        $param = [
            "dis_code" => $data["code"],
            "dis_name" => self::getParam($data, "name"),
            "dis_status" => self::getParam($data, "status"),
            "dis_city_code" => self::getParam($data, "city_code"),
        ];

        $res = self::makeRequest(
            "api/put_district", "PUT", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function putSubDistrict($data)
    {
        $param = [
        "sdis_code" => $data["code"],
        "sdis_name" => self::getParam($data, "name"),
        "sdis_district_code" => self::getParam($data, "district_code"),
        "sdis_status" => self::getParam($data, "status"),
        "sdis_zip_code" => self::getParam($data, "zip_code"),
        ];

        $res = self::makeRequest(
            "api/put_subdistrict", "PUT", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    static function createCurrency($data)
    {
        $param = [
         "cur_name" => $data["cur_name"],
         "cur_code" => $data["cur_code"],
         "cur_symbol" => $data["cur_symbol"],
         "co_code" =>  $data["co_code"],
         "cur_status" => self::getParam($data, "cur_status"),

        ];

        $res = self::makeRequest(
            "api/create_currency", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception("Sinkronisasi gagal. ".$res->getBody());
        }
    }

    static function updateCurrency($data)
    {
        $param = [
         "cur_code" => $data["cur_code"],
         "co_code" => $data["co_code"],
         "cur_name" => $data["cur_name"],
         "cur_symbol" => $data["cur_symbol"],
         "cur_status" => self::getParam($data, "cur_status"),
         "cur_default" => self::getParam($data, "cur_default"),
        ];

        $res = self::makeRequest(
            "api/update_currency", "PUT", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception("Sinkronisasi gagal. ".$res->getBody());
        }
    }

    static function deleteCurrency($data)
    {
        $param = [
         "cur_code" => $data["cur_code"],
        ];

        $res = self::makeRequest(
            "api/delete_currency", "DELETE", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }
    }

    static function createColor($data)
    {
        $param = [
         "co_code" =>  $data["co_code"],
         "co_name" =>  $data["co_name"],
        ];

        $res = self::makeRequest(
            "api/create_color", "POST", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }
    }

    static function updateColor($data)
    {
        $param = [
         "co_code" =>  $data["co_code"],
         "co_name" =>  $data["co_name"],
        ];

        $res = self::makeRequest(
            "api/update_color", "PUT", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }
    }

    static function deleteColor($data)
    {
        $param = [
         "co_code" =>  $data["co_code"],
        ];

        $res = self::makeRequest(
            "api/delete_color", "delete", [
            "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }
    }

    static function putGrade($data)
    {
        $param = [
            "code" => $data["code"],
            "name" => self::getParam($data, "name"),
        ];

        $res = self::makeRequest(
            "api/grade",
            "PUT", 
            [
                "form_params" => $param
            ]
        );

        $result = [];
        //parse response
        if($res->getBody() != "Sukses") {
            //success
            throw new \Exception($res->getBody());
        }

        return $result;
    }

    /**
     * Mendapatkan daftar order yang diambil oleh merchant
     *
     * @param [type] $data
     * @return void
     */
    static function getMerchantOrders($param)
    {
        $res = self::makeRequest(
            "api/merchant_orders", "GET", [
            "query" => $param
            ]
        );

        //parse response
        if($res->getStatusCode() == 200){
            //success
            return json_decode($res->getBody(), true);
        }else{
            //error
            throw new \Exception($res->getBody());
        }
    }

    /**
     * Mendapatkan data dashboard penjualan SF
     *
     * @param [type] $data
     * @return void
     */
    static function getSalesDashboardData($param)
    {
        $res = self::makeRequest(
            "goa", "GET", [
            "query" => $param
            ]
        );

        //parse response
        if($res->getStatusCode() == 200){
            //success
            return json_decode($res->getBody(), true);
        }else{
            //error
            throw new \Exception($res->getBody());
        }
    }

    //------------------- UTILS ------------------
    static function makeRequest($url, $method, $data)
    {
        $client = new \GuzzleHttp\Client(["proxy"=>config(self::CONFIG_FILENAME.".proxy")]);

        $accessToken = Cache::get(
            "stf_access_token", function () use ($client) {
                return self::getAccessToken($client);
            }
        );

        //jalankan request
        $response = self::sendRequest($url, $method, $data, $accessToken, $client);

        if(!$response){
            throw new \Exception("Cannot send request to Storefront");
        }

        //jika unaouthorize
        $responseStatus = $response->getStatusCode();
        if($responseStatus == 401) //mungkin token expired
        {
            //coba minta lagi
            $accessToken = self::getAccessToken($client);

            //jalankan request dengan token yang baru
            $response = self::sendRequest($url, $method, $data, $accessToken, $client);
        }

        //kembalikan jawaban
        return $response;
    }

    static function sendRequest($url, $method, $data, $token, $client)
    {
        //pakai token untuk autentikasikasi
        $data["headers"] = [
         "Authorization" => "Bearer ".$token,
        ];

        try {
            //jalankan request
            return $client->request($method, config(self::CONFIG_FILENAME.".url").$url, $data);
        } catch (\GuzzleHttp\Exception\RequestException $exc) {
            return $exc->getResponse();
        }
    }

    static function getAccessToken($client)
    {
        $res = $client->request(
            "POST",
            config(self::CONFIG_FILENAME.".url")."oauth/access_token", //config ke wms kukuruyuk
            [
            "form_params" =>
            [
            "grant_type"    => "client_credentials",
            "client_id"     => config(self::CONFIG_FILENAME.".client_id"),
            "client_secret"    => config(self::CONFIG_FILENAME.".client_secret"),
            ]
            ]
        );

        // mengambil akses token dari variabel $res
        $json_response = json_decode($res->getBody(), false);

        //simpan nilai accesstoken yang telah di dapat ke dalam cache
        Cache::put("stf_access_token", $json_response->access_token, Carbon::now()->addSeconds($json_response->expires_in));
        return $json_response->access_token;
    }

    static function getParam($array, $key, $default = null, $emptyUseDefault = true)
    {
        if(array_key_exists($key, $array)) {
            $key = $array[$key];
            if(!$emptyUseDefault
                || $key !== ""//cek if empty string allowed
            ) {
                $default = $key;
            }
        }
        return ($default instanceof \Closure)?$default($array):$default;
    }
}
