-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 13 Mar 2017 pada 06.00
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_01_01_000001_create_oauth_scopes_table', 1),
('2015_01_01_000002_create_oauth_grants_table', 1),
('2015_01_01_000003_create_oauth_grant_scopes_table', 1),
('2015_01_01_000004_create_oauth_clients_table', 1),
('2015_01_01_000005_create_oauth_client_endpoints_table', 1),
('2015_01_01_000006_create_oauth_client_scopes_table', 1),
('2015_01_01_000007_create_oauth_client_grants_table', 1),
('2015_01_01_000008_create_oauth_sessions_table', 1),
('2015_01_01_000009_create_oauth_session_scopes_table', 1),
('2015_01_01_000010_create_oauth_auth_codes_table', 1),
('2015_01_01_000011_create_oauth_auth_code_scopes_table', 1),
('2015_01_01_000012_create_oauth_access_tokens_table', 1),
('2015_01_01_000013_create_oauth_access_token_scopes_table', 1),
('2015_01_01_000014_create_oauth_refresh_tokens_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_aboutus`
--

CREATE TABLE `nm_aboutus` (
  `ap_id` int(11) NOT NULL,
  `ap_description` text NOT NULL,
  `ap_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_add`
--

CREATE TABLE `nm_add` (
  `ad_id` smallint(5) UNSIGNED NOT NULL,
  `ad_name` varchar(100) NOT NULL,
  `ad_position` tinyint(4) NOT NULL COMMENT '1-Header right,2-Left side bar,3-Bottom footer',
  `ad_pages` tinyint(4) NOT NULL COMMENT '1-home,2-product,3-Deal,4-Auction',
  `ad_redirecturl` varchar(150) NOT NULL,
  `ad_img` varchar(150) NOT NULL,
  `ad_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-admin 2 customer',
  `ad_status` tinyint(3) UNSIGNED NOT NULL,
  `ad_read_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_add`
--

INSERT INTO `nm_add` (`ad_id`, `ad_name`, `ad_position`, `ad_pages`, `ad_redirecturl`, `ad_img`, `ad_type`, `ad_status`, `ad_read_status`) VALUES
(1, 'Mens Fashion', 1, 1, 'http://localhost/laravel5.0/catproducts/viewcategorylist/MSwy', '1394024215326-BeSummerReady_980x459_980_459_mini-770x459PiSN9mMO.jpg', 1, 0, 1),
(2, 'Womens Fashion', 1, 1, 'http://localhost/laravel5.0/catproducts/viewcategorylist/MSwz', 'banner04ynoKA3qX.jpg', 1, 0, 1),
(3, 'Mens Winter', 1, 1, 'http://localhost/laravel5.0/catproducts/viewcategorylist/Miwy', 'banner_mens_SHOP_BY_STYLE_WINTER61P1mrNl.jpg', 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_admin`
--

CREATE TABLE `nm_admin` (
  `adm_id` int(10) UNSIGNED NOT NULL,
  `adm_fname` varchar(150) NOT NULL,
  `adm_lname` varchar(150) NOT NULL,
  `adm_password` varchar(150) NOT NULL,
  `adm_email` varchar(150) NOT NULL,
  `adm_phone` varchar(20) NOT NULL,
  `adm_address1` varchar(150) NOT NULL,
  `adm_address2` varchar(150) NOT NULL,
  `adm_ci_id` int(10) UNSIGNED NOT NULL COMMENT 'city id',
  `adm_co_id` smallint(5) UNSIGNED NOT NULL COMMENT 'country id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_admin`
--

INSERT INTO `nm_admin` (`adm_id`, `adm_fname`, `adm_lname`, `adm_password`, `adm_email`, `adm_phone`, `adm_address1`, `adm_address2`, `adm_ci_id`, `adm_co_id`) VALUES
(1, 'admin', 'admin', 'admin', 'harry.okta.maulana@gmail.com', '+6285717325510', 'aaa1', 'aa22', 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_adminreply_comments`
--

CREATE TABLE `nm_adminreply_comments` (
  `reply_id` int(11) NOT NULL,
  `reply_blog_id` int(11) NOT NULL,
  `reply_cmt_id` int(11) NOT NULL,
  `reply_msg` text NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_adminreply_comments`
--

INSERT INTO `nm_adminreply_comments` (`reply_id`, `reply_blog_id`, `reply_cmt_id`, `reply_msg`, `reply_date`) VALUES
(1, 1, 1, 'oke oke oke oke oke oke oke<br>', '2016-03-10 08:15:34'),
(2, 1, 1, 'dfgdfdfzhghdrdhghd', '2016-03-31 22:23:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_auction`
--

CREATE TABLE `nm_auction` (
  `auc_id` int(10) UNSIGNED NOT NULL,
  `auc_title` varchar(500) NOT NULL,
  `auc_category` int(11) NOT NULL,
  `auc_main_category` int(11) NOT NULL,
  `auc_sub_category` int(11) NOT NULL,
  `auc_second_sub_category` int(11) NOT NULL,
  `auc_original_price` int(11) NOT NULL,
  `auc_auction_price` int(11) NOT NULL,
  `auc_bitinc` smallint(5) UNSIGNED NOT NULL,
  `auc_saving_price` int(11) NOT NULL,
  `auc_start_date` datetime NOT NULL,
  `auc_end_date` datetime NOT NULL,
  `auc_shippingfee` decimal(10,2) NOT NULL,
  `auc_shippinginfo` text NOT NULL,
  `auc_description` text NOT NULL,
  `auc_merchant_id` int(11) NOT NULL,
  `auc_shop_id` int(11) NOT NULL,
  `auc_meta_keyword` varchar(250) NOT NULL,
  `auc_meta_description` varchar(500) NOT NULL,
  `auc_image_count` int(11) NOT NULL,
  `auc_image` varchar(500) NOT NULL,
  `auc_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `auc_posted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_auction`
--

INSERT INTO `nm_auction` (`auc_id`, `auc_title`, `auc_category`, `auc_main_category`, `auc_sub_category`, `auc_second_sub_category`, `auc_original_price`, `auc_auction_price`, `auc_bitinc`, `auc_saving_price`, `auc_start_date`, `auc_end_date`, `auc_shippingfee`, `auc_shippinginfo`, `auc_description`, `auc_merchant_id`, `auc_shop_id`, `auc_meta_keyword`, `auc_meta_description`, `auc_image_count`, `auc_image`, `auc_status`, `auc_posted_date`) VALUES
(1, 'Diamond necklace', 1, 1, 1, 1, 15000, 10500, 10, 4500, '2014-08-09 12:50:47', '2014-09-09 12:50:47', '10.00', 'Ships to india', 'Diamond necklace will take you to a completely different world with \r\nspectacular views. Jewelry diamond necklaces the most beautiful state. \r\nJewelry design is an art. The worldâ€™s most valuable asset is the people.\r\n Charms adorn people. So, Jewelry design is the art of human adornment. \r\nJewelry design is one of the oldest professions. Diamond necklaces are \r\ndesigned by expert designers. We chose the most beautiful diamond \r\nnecklace designs for you. There are two predominant objective in the \r\ndesign of diamond necklaces. Exquisite designs, modesty shows in the \r\nwealth. Large stone adorned with necklaces, are influenced environment. \r\nYou can choose the most special days of the diamond necklace.<br><br><br>', 6, 5, 'Diamond', 'Diamond', 2, 'jewelzc8BoIZr.jpg/**/new2jdMILdT7.jpg/**/new1PhpgKukL.jpg/**/', 1, '2014-08-13 04:11:08'),
(2, 'hand bags', 1, 1, 1, 1, 1500, 1000, 10, 500, '2014-08-13 16:49:34', '2014-08-06 16:49:34', '25.00', 'free', 'asds', 2, 1, 'asd', 'asss', 4, '2YmlkRqJC.png/**/5UCkEi0zv.png/**/4TZEEKtZ7.png/**/1Zfw5yHW8.png/**/6A9AOLRAx.png/**/', 1, '2014-08-11 11:20:54'),
(3, 'hand bags', 1, 1, 1, 1, 1500, 1000, 10, 500, '2014-08-09 12:50:47', '2014-08-12 16:51:17', '25.00', 'swds', 'sdsa', 2, 1, 'sads', 'asdsa', 4, '2w4QZLuE2.png/**/5iJNcTQKA.png/**/62wJiGqDm.png/**/1r11FrvLT.png/**/3hypyZDuW.png/**/', 1, '2014-08-11 11:22:15'),
(4, 'Merchant deal', 2, 2, 2, 2, 10000, 9000, 10, 1000, '2014-08-12 09:38:54', '2014-08-13 15:24:54', '10.00', 'coimbatore vadavalli', 'Meta keywords<br>', 4, 3, 'fdafds', 'dsfds', 2, 'flower3SIH0fbjz.jpg/**/flower1Fk1kQ6Cw.jpg/**/flower2jaBoQuEf.jpg/**/', 1, '2014-08-13 04:10:25'),
(5, 'sasa', 1, 1, 1, 1, 150, 100, 5, 50, '2014-11-21 19:41:48', '2014-11-28 19:41:48', '12.00', 'wewewe', 'weewe', 1, 1, 'wewe', 'wewe', 0, 'DMR_48xTLGZCgG.jpg/**/', 1, '2014-11-20 16:14:58'),
(6, 'BiG Bazar', 2, 2, 0, 0, 1000, 100, 10, 900, '2014-12-10 11:20:30', '2014-12-16 11:20:30', '3.00', 'shipp', 'auction', 5, 4, 'rterte', 'ertert', 0, 'IMG_1269_wUYY6ufcW.jpg/**/', 1, '2014-12-10 10:22:18'),
(7, 'Senbagam', 5, 9, 19, 49, 400, 300, 10, 100, '2015-05-11 08:15:34', '2015-05-12 08:04:34', '0.00', 'item shipping', 'This product which unique<br>', 3, 2, 'keywords', 'keywords', 0, 'ChrysanthemumZVXd9lBr.jpg/**/', 1, '2015-05-11 02:49:23'),
(8, 'shoe sport', 4, 7, 33, 0, 100, 50, 1, 50, '2016-02-17 18:34:37', '2016-02-25 18:34:37', '0.00', 'ship to you', 'is a sport shoe, nike', 1, 1, 'ss', 'ss', 0, 'slide1-2mwb33rdS.png/**/', 1, '2016-02-17 10:36:00'),
(9, 'Test Auc', 5, 8, 14, 35, 320, 250, 10, 70, '2016-03-09 17:40:47', '2016-03-25 17:40:47', '20.00', 'Test', 'Test', 1, 140, 'Test', 'Test', 0, 'imagesRT1JF5cz.jpeg/**/', 1, '2016-03-09 12:11:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_banner`
--

CREATE TABLE `nm_banner` (
  `bn_id` smallint(5) UNSIGNED NOT NULL,
  `bn_title` varchar(150) NOT NULL,
  `bn_type` varchar(10) NOT NULL COMMENT '1-home,2-product,3-deal,4-auction',
  `bn_img` varchar(150) NOT NULL,
  `bn_status` int(11) NOT NULL COMMENT '1-block,0-unblock',
  `bn_redirecturl` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_banner`
--

INSERT INTO `nm_banner` (`bn_id`, `bn_title`, `bn_type`, `bn_img`, `bn_status`, `bn_redirecturl`) VALUES
(44, 'Mens Wear', '1,1,1', 'banner1Rl0YCxMM.jpg', 1, 'http://www.storefront.co.id/'),
(45, 'Womens Wear', '1,1,1', 'banner2TGGsF3fd.jpg', 1, 'http://www.storefront.co.id/'),
(46, 'Kids wear', '1,1,1', 'banner3BkK5M1yz.jpg', 0, 'http://www.storefront.co.id/'),
(47, 'Cycles', '1,1,1', 'banner4dMOwCCMb.jpg', 1, 'http://www.storefront.co.id/'),
(48, 'Women Collection', '1,1,1', 'Vogue-Banner-870x350J2vBfniD.jpg', 1, 'http://www.storefront.co.id/'),
(49, 'Grade', '1,1,1', 'HOME-BANER-grade-rusbyG8a.png', 0, 'http://www.storefront.co.id/'),
(50, 'Turun Harga', '1,1,1', 'HOME-BANER-turun-hargaXVdCNd4Z.png', 0, 'http://www.storefront.co.id/'),
(51, 'Pengiriman', '1,1,1', 'HOME-BANER-pengirimanT4KPp4Tj.png', 0, 'http://www.storefront.co.id/'),
(52, 'Fruity wax', '1,1,1', 'kukuruyuk-fruitywax_home_bannerMxVwFOuU.png', 0, 'http://www.storefront.co.id/');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_blog`
--

CREATE TABLE `nm_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_title` varchar(50) NOT NULL,
  `blog_desc` text NOT NULL,
  `blog_catid` int(11) NOT NULL,
  `blog_image` varchar(100) NOT NULL,
  `blog_metatitle` varchar(100) NOT NULL,
  `blog_metadesc` text NOT NULL,
  `blog_metakey` varchar(100) NOT NULL,
  `blog_tags` varchar(100) NOT NULL,
  `blog_comments` int(5) NOT NULL COMMENT '0-not allow,1-allow',
  `blog_type` int(5) UNSIGNED NOT NULL COMMENT '1-publish,2-drafts',
  `blog_status` tinyint(3) UNSIGNED NOT NULL COMMENT '1-block,0-unblock',
  `blog_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_blog`
--

INSERT INTO `nm_blog` (`blog_id`, `blog_title`, `blog_desc`, `blog_catid`, `blog_image`, `blog_metatitle`, `blog_metadesc`, `blog_metakey`, `blog_tags`, `blog_comments`, `blog_type`, `blog_status`, `blog_created_date`) VALUES
(1, 'New product', 'Lorem ipsum dolor sit amet, lobortis mauris sed, mi fringilla enim nulla tincidunt nibh, mauris lectus ante rutrum at dui, mauris urna varius. Etiam amet vestibulum sodales augue, metus dapibus aspernatur in vel placerat, consectetuer mattis ornare non est convallis vitae, libero in non urna at. Tempor placerat sollicitudin consectetuer justo lacinia, pulvinar arcu arcu purus vel quisque ligula, felis vivamus curabitur nascetur purus elit, tempus mauris varius nulla faucibus sem, auctor mauris eget. Eu nunc ac nostra lectus, wisi dui ante sit sollicitudin aliquam, et nulla urna condimentum nisl cras, lobortis nisl primis libero id. Eu cum, dolor vitae turpis ut dui, neque quam vulputate ut ut. Sodales nostra sed suspendisse cras et, dictum vestibulum parturient amet non, ac vel ligula non id ultricies, per mauris interdum id laoreet, laoreet et sed auctor volutpat duis. Ullamcorper fermentum. Erat morbi nam nihil, ligula arcu mollis ac id tortor eros, neque malesuada elementum sed iaculis luctus vulputate, ac nostra, mauris nulla neque magna nibh eros tincidunt. Imperdiet sapien exercitationem sed. Maecenas dictum erat volutpat tempor, nullam senectus molestie, velit sed nullam maecenas vestibulum aliquam lorem, lectus ut nascetur eros sem mi.', 1, 'animasi-gambar-dp-bbm-tukang-parkir-300x300waJYdOpK.png', 'xxxxxxxxxxxxxx', 'xxxxxxxxxxxxx', 'xxxxxxxxxxxxx', 'Electronic Prd', 1, 1, 0, '2016-06-07 04:35:38'),
(2, 'Diwali Damaka', 'xcvxcvcxvxcvxcvxccccccccccccccc xcvxcvcxvxcvxcvcxvxcvxc xcvxcvxcvcxv xcvcxvcxvcxvxcvcxx', 3, 'TulipsjKKaczAb.jpg', 'cvxcxcv', 'xcvcxvcxvx', 'xcvcxvcx', 'vvcxcxc', 1, 1, 0, '2016-11-01 22:50:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_blogsetting`
--

CREATE TABLE `nm_blogsetting` (
  `bs_id` tinyint(3) UNSIGNED NOT NULL,
  `bs_allowcommt` tinyint(4) NOT NULL,
  `bs_radminapproval` tinyint(4) NOT NULL COMMENT 'Require Admin Approval ',
  `bs_postsppage` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_blogsetting`
--

INSERT INTO `nm_blogsetting` (`bs_id`, `bs_allowcommt`, `bs_radminapproval`, `bs_postsppage`) VALUES
(1, 1, 0, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_blog_cus_comments`
--

CREATE TABLE `nm_blog_cus_comments` (
  `cmt_id` int(11) NOT NULL,
  `cmt_blog_id` int(11) NOT NULL,
  `cmt_name` varchar(250) NOT NULL,
  `cmt_email` varchar(250) NOT NULL,
  `cmt_website` varchar(250) NOT NULL,
  `cmt_msg` text NOT NULL,
  `cmt_admin_approve` int(11) NOT NULL DEFAULT '0' COMMENT '1 => Approved, 2 => Unapproved',
  `cmt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cmt_msg_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read ,1 Read '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_blog_cus_comments`
--

INSERT INTO `nm_blog_cus_comments` (`cmt_id`, `cmt_blog_id`, `cmt_name`, `cmt_email`, `cmt_website`, `cmt_msg`, `cmt_admin_approve`, `cmt_date`, `cmt_msg_status`) VALUES
(1, 1, 'yamuna', 'yamua@nexplocindia.com', 'https://www.google.co.in/?gfe_rd=cr&ei=pwbrU7zeM6HV8gfkuYDABQ&gws_rd=ssl', 'description about the product', 1, '2014-08-13 01:03:36', 1),
(2, 1, 'charles', 'charlesvictor.j@pofitec.com', 'http://pofitec.com', 'Lorem ipsum dolor sit amet, lobortis mauris sed, mi fringilla enim nulla tincidunt nibh, mauris lectus ante rutrum at dui, mauris urna varius. Etiam amet vestibulum sodales augue, metus dapibus aspernatur in vel placerat, consectetuer mattis ornare non est convallis vitae, libero in non urna at. Tempor placerat sollicitudin consectetuer justo lacinia, pulvinar arcu arcu purus vel quisque ligula, felis vivamus curabitur nascetur purus elit, tempus mauris varius nulla faucibus sem, auctor mauris eget. Eu nunc ac nostra lectus, wisi dui ante sit sollicitudin aliquam, et nulla urna condimentum nisl cras, lobortis nisl primis libero id. Eu cum, dolor vitae turpis ut dui, neque quam vulputate ut ut. Sodales nostra sed suspendisse cras et, dictum  ', 1, '2016-06-07 04:37:37', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_city`
--

CREATE TABLE `nm_city` (
  `ci_id` int(10) UNSIGNED NOT NULL,
  `ci_name` varchar(100) NOT NULL,
  `ci_con_id` smallint(6) NOT NULL,
  `ci_lati` decimal(18,14) NOT NULL,
  `ci_long` decimal(18,14) NOT NULL,
  `ci_default` tinyint(4) NOT NULL,
  `ci_status` tinyint(4) NOT NULL,
  `ci_code` varchar(199) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_city`
--

INSERT INTO `nm_city` (`ci_id`, `ci_name`, `ci_con_id`, `ci_lati`, `ci_long`, `ci_default`, `ci_status`, `ci_code`) VALUES
(1, 'Coimbatore', 1, '11.00469725195311', '76.92911993710936', 0, 1, 'CBT'),
(2, 'chennai', 1, '12.76506834229134', '80.13712774960936', 0, 1, 'CHN'),
(3, 'Delhi', 1, '28.52674846734579', '77.13511358945311', 0, 1, 'DHL'),
(4, 'Bangaluru', 1, '12.91101510701221', '77.66657721249999', 0, 1, 'BGL'),
(5, 'Kolkata', 1, '23.05206669256911', '87.99372564999999', 0, 1, 'KLK'),
(6, 'Bangalore', 1, '9999.99999999999999', '9999.99999999999999', 0, 1, 'BGR'),
(7, 'Jakarta', 8, '-6.21150000000000', '106.84520000000000', 1, 1, 'JKT'),
(8, 'Bandung', 8, '-6.91746400000000', '107.61912300000000', 0, 1, '3204'),
(9, 'Denpasar', 8, '-8.67045800000000', '115.21262900000000', 0, 1, 'DNP');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_cms_pages`
--

CREATE TABLE `nm_cms_pages` (
  `cp_id` smallint(5) UNSIGNED NOT NULL,
  `cp_title` varchar(250) NOT NULL,
  `cp_description` longtext NOT NULL,
  `cp_status` tinyint(4) NOT NULL DEFAULT '1',
  `cp_created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_cms_pages`
--

INSERT INTO `nm_cms_pages` (`cp_id`, `cp_title`, `cp_description`, `cp_status`, `cp_created_date`) VALUES
(8, 'Help', '<b>What Information Do You We Collect<br></b>We collect information from you when register on our site or fill out our contact form.<br>\r\n\r\n            When ordering or raising enquiry on our \r\nwebsite, as appropriate, you may be asked to enter your Name, Email id, \r\nPhone number and skype ID. However you could visit Laravel ecommerce \r\nwebsite anonymously.<br><br><b>Special Notice</b> <br>If you are under 13 years old\r\n            Laravel Ecommerce website is not \r\nanticipated at children under 13 years old and we do not collect, use, \r\nprovide or process in any other form any personal information of \r\nchildren under the age of 13 consciously. We therefore also ask you, if \r\nyou are under 13 years old, please do not send us your personal \r\ninformation (for example, your name, address and email address).<br><br><b>Purposes of the collection of your data<br></b>&nbsp;Laravel Ecommerce is intent to inform \r\nyou of who we are and what we do. We collect and use personal \r\ninformation (including name, phone number and email ID) to better \r\nprovide you with the required services, or information. We would \r\ntherefore use your personal information in order to:\r\n\r\n            <ul><li>Acknowledge to your queries or requests</li><li>Govern our obligations in relation to any agreement you have with us</li><li>Anticipate and resolve problems with any goods or services supplied to you</li><li>Create products or services that may meet your needs</li></ul><b>Keeping our records accurate<br></b>&nbsp;We aim to keep our data confidential \r\nabout you as authentic as possible. If you would like to review, change \r\nor delete the details you have provided with us, please contact us via \r\nemail which is mentioned in our website.<br><br><b>Security of your personal data<br></b>&nbsp;As we value your personal information, \r\nwe will establish sufficient level of protection. We have therefore \r\nenforced technology and policies with the objective of protecting your \r\nprivacy from illegal access and erroneous use and will update these \r\nmeasures as new technology becomes available, as relevant.<br><b>Cookies policy</b>\r\n            <h4>Why do we use cookies?</h4>\r\n            We use browser cookies to learn more \r\nabout the way you interact with our content and help us to improve your \r\nexperience when visiting our website.<br>\r\n             Cookies remember the type of browser \r\nyou use and which additional browser software you have installed. They \r\nalso remember your preferences, such as language and region, which \r\nremain as your default settings when you revisit the website. Cookies \r\nalso allow you to rate pages and fill in comment forms.<br>\r\n            Some of the cookies we use are session \r\ncookies and only last until you close your browser, others are \r\npersistent cookies which are stored on your computer for longer.<br><b>Changes on privacy policy<br></b>&nbsp;We may make changes on our website’s \r\nprivacy policy at any time. If we make any consequential changes to this\r\n privacy policy and the way in which we use your personal data we will \r\npost these changes on this page and will do our best to notify you of \r\nany significant changes. Kindly often check our privacy policies.<br>', 1, '2016-06-10 04:36:27'),
(9, 'test', 'test', 0, '2016-08-21 00:56:58'),
(11, 'TestTK', '<b>CMS </b>content under construction<br><br><img alt="" src="http://hd-wall-papers.com/images/wallpapers/under-construction-image/under-construction-image-6.jpg" height="281" width="376"><br><a href="http://hd-" target="_self" rel=""></a><br>', 1, '2017-02-24 10:27:14'),
(12, 'promo2', 'promotion 2<br>', 1, '2017-02-24 10:28:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_color`
--

CREATE TABLE `nm_color` (
  `co_id` smallint(5) UNSIGNED NOT NULL,
  `co_code` varchar(10) NOT NULL,
  `co_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_color`
--

INSERT INTO `nm_color` (`co_id`, `co_code`, `co_name`) VALUES
(2, '#3D0C02', 'Bean  '),
(3, '#FFFFFF', 'White'),
(4, '#FFFF00', 'Yellow'),
(5, '#0141AD', 'Cobalt'),
(6, '#ED0A3F', 'Red Ribbon'),
(8, '#E7F8FF', 'Lily White'),
(9, '#163531', 'Gable Green'),
(10, '#B4C2DA', 'Pigeon Post'),
(11, '#000000', 'Black'),
(12, '#6195ED', 'Cornflower Blue'),
(13, '#66ED61', 'Pastel Green'),
(14, '#252F41', 'Ebony Clay'),
(15, '#3C485D', 'Oxford Blue'),
(16, '#9199AC', 'Manatee'),
(17, '#8F4B0E', 'Korma'),
(18, '#1D2A3E', 'Cloud Burst'),
(19, '#1C1E21', 'Shark'),
(20, '#0C1422', 'Ebony'),
(21, '#3E5884', 'East Bay'),
(23, '#070B12', 'Ebony'),
(24, '#07090D', 'Bunker'),
(25, '#4C4F56', 'Abbey'),
(26, '#F0F8FF', 'Alice Blue'),
(27, '#ED6461', 'Burnt Sienna'),
(28, '#ED6175', 'Mandy'),
(29, '#0D1116', 'Bunker'),
(30, '#6195ED', 'Cornflower Blue'),
(31, '#6195ED', 'Cornflower Blue'),
(34, '#273E72', 'Astronaut'),
(36, '#7CB0A1', 'Acapulco'),
(37, '#FF0000', 'Red'),
(38, '#7CB0A1', 'Acapulco'),
(39, '#C9FFE5', 'Aero Blue'),
(40, '#EB9373', 'Apricot'),
(41, '#4C80D6', 'Havelock Blue'),
(42, '#A7C219', 'La Rioja'),
(43, '#D92030', 'Alizarin Crimson');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_colorfixed`
--

CREATE TABLE `nm_colorfixed` (
  `cf_id` smallint(5) UNSIGNED NOT NULL,
  `cf_code` varchar(10) NOT NULL,
  `cf_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_colorfixed`
--

INSERT INTO `nm_colorfixed` (`cf_id`, `cf_code`, `cf_name`) VALUES
(1, '#FFFFFF', 'White'),
(2, '#808080', 'gray'),
(3, '#C0C0C0', 'silver'),
(4, '#000000', 'black'),
(5, '#800000', 'maroon'),
(6, '#FF0000', 'red'),
(7, '#800080', 'purple'),
(8, '#008000', 'green'),
(9, '#00FF00', 'lime'),
(10, '#808000', 'olive'),
(11, '#FFFF00', 'yellow'),
(12, '#00FFFF', 'aqua'),
(13, '#FFA500', 'orange'),
(14, '#000080', 'navy'),
(15, '#0000FF', 'blue'),
(16, '#008080', 'teal'),
(17, '#6195ED', 'Cornflower Blue'),
(18, '#7CB0A1', 'Acapulco'),
(19, '#000000', 'Black'),
(20, '#FFCB9E', 'Peach Orange');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_contact`
--

CREATE TABLE `nm_contact` (
  `cont_id` int(10) UNSIGNED NOT NULL,
  `cont_name` varchar(100) NOT NULL,
  `cont_email` varchar(150) NOT NULL,
  `cont_no` varchar(50) NOT NULL,
  `cont_message` text NOT NULL,
  `cont_restatus` tinyint(4) NOT NULL,
  `cont_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_country`
--

CREATE TABLE `nm_country` (
  `co_id` smallint(5) UNSIGNED NOT NULL,
  `co_code` varchar(10) NOT NULL,
  `co_name` varchar(30) NOT NULL,
  `co_cursymbol` varchar(5) NOT NULL,
  `co_curcode` varchar(10) NOT NULL,
  `co_status` tinyint(4) NOT NULL COMMENT '1-block,0-unblock'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_country`
--

INSERT INTO `nm_country` (`co_id`, `co_code`, `co_name`, `co_cursymbol`, `co_curcode`, `co_status`) VALUES
(1, 'IND', 'India', 'Rs', 'INR', 0),
(5, 'IR', 'Iran', 'Rls', 'IRR', 0),
(6, 'CZ', 'czech republic', 'K?', 'CZK', 0),
(7, 'USA', 'United States of America', '$', 'USD', 0),
(8, 'ID', 'Indonesia', 'Rp', 'IDR', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_currency`
--

CREATE TABLE `nm_currency` (
  `cur_id` int(10) UNSIGNED NOT NULL,
  `cur_name` varchar(100) NOT NULL,
  `cur_code` varchar(5) NOT NULL,
  `cur_symbol` varchar(10) NOT NULL,
  `cur_status` tinyint(11) NOT NULL DEFAULT '1',
  `cur_default` tinyint(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_customer`
--

CREATE TABLE `nm_customer` (
  `cus_id` bigint(10) UNSIGNED NOT NULL,
  `cus_name` varchar(100) NOT NULL,
  `facebook_id` varchar(150) NOT NULL,
  `cus_email` varchar(150) NOT NULL,
  `cus_pwd` varchar(40) NOT NULL,
  `cus_phone` varchar(20) NOT NULL,
  `cus_address1` varchar(150) NOT NULL,
  `cus_address2` varchar(150) NOT NULL,
  `cus_country` varchar(50) NOT NULL,
  `cus_city` varchar(50) NOT NULL,
  `cus_joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cus_logintype` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>Admin user, 2=> Website User, 3=> Facebook User',
  `cus_status` int(11) NOT NULL COMMENT '0 unblock 1 block',
  `cus_pic` varchar(150) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_customer`
--

INSERT INTO `nm_customer` (`cus_id`, `cus_name`, `facebook_id`, `cus_email`, `cus_pwd`, `cus_phone`, `cus_address1`, `cus_address2`, `cus_country`, `cus_city`, `cus_joindate`, `cus_logintype`, `cus_status`, `cus_pic`, `created_date`) VALUES
(28, 'Pavithran', '', 'pavithrandbpro@gmail.com', '202cb962ac59075b964b07152d234b70', '9787467575', '', '', '1', '1', '2016-06-10 16:14:29', 2, 0, '', '0000-00-00'),
(42, 'charles', '', 'charlesvictor.j@pofitec.com', '11d7fb8c3ed2d559a6575b2395a87537', '9498056637', '', '', '1', '1', '2016-06-17 13:03:30', 2, 0, '', '0000-00-00'),
(47, 'Pavithran', '', 'pavithran.g@pofitec.com', 'c781679cc2fd7a1f6c8ff947cd4abd5e', '9787467575', '', '', '1', '1', '2016-06-21 09:52:19', 2, 0, '', '0000-00-00'),
(74, 'rajesh', '', 'erkprajesh@gmail.com', '4297f44b13955235245b2497399d7a93', '9500818702', '', '', '1', '1', '2016-06-23 07:32:23', 2, 0, '', '0000-00-00'),
(75, 'kumar', '', 'rrpofi@gmail.com', '79cfac6387e0d582f83a29a04d0bcdc4', '1234567890', '', '', '1', '1', '2016-06-23 07:41:49', 2, 0, '', '0000-00-00'),
(76, 'Pavithran', '', 'pavithrang@rocketmail.com', '202cb962ac59075b964b07152d234b70', '9787467575', '', '', '1', '1', '2016-06-25 04:27:45', 2, 0, '', '0000-00-00'),
(78, 'Test', '', 'tester@testy.com', 'fcea920f7412b5da7be0cf42b8c93759', '4055551212', '', '', '1', '2', '2016-07-01 17:19:14', 2, 0, '', '0000-00-00'),
(79, 'reca', '', 'recabyte008@yahoo.com', 'c50303bee26c312d8d284b7abf372645', '56667788', '', '', '1', '1', '2016-07-02 04:28:51', 2, 0, '', '0000-00-00'),
(80, 'Renzo Fernandini', '10210016168465859', 'undefined', '', '', '', '', '', '', '2016-07-06 17:01:24', 3, 0, '', '0000-00-00'),
(81, 'TESTE 1', '', 'teste@teste.com', '827ccb0eea8a706c4c34a16891f84e7b', '1313131233', '', '', '1', '2', '2016-07-09 13:57:52', 2, 0, '', '0000-00-00'),
(82, 'vinod', '', 'vinodbabu@pofitec.com', 'e10adc3949ba59abbe56e057f20f883e', '1234567890', '', '', '1', '1', '2016-07-12 13:09:27', 2, 1, '', '0000-00-00'),
(83, 'JC Charles Victor', '1066743686710240', 'charlesjc496@gmail.com', '', '', '', '', '', '', '2016-07-16 09:43:27', 3, 0, '', '0000-00-00'),
(84, 'Kailash Rajendhar rr', '1192904664076815', 'kumarkailash075@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9092398789', 'bcvbcvbvb', 'vbcvbcvb', '', '', '2016-07-16 09:45:45', 3, 0, 'Koalaq3N6cYUG.jpg', '0000-00-00'),
(85, 'Vinod Vinod', '1765794540307590', 'test21689@gmail.com', '', '', '', '', '', '', '2016-07-16 09:45:48', 3, 0, '', '0000-00-00'),
(86, 'G Pavithran Cse', '561788407336067', 'pavithrang@rocketmail.com', '', '', '', '', '', '', '2016-07-16 09:45:59', 3, 0, '', '0000-00-00'),
(87, 'Lukas Gregor', '10205435746891755', 'lukas.gregis@gmail.com', '', '', '', '', '', '', '2016-07-17 17:12:34', 3, 0, '', '0000-00-00'),
(88, 'Faiz Arifin', '1219900048028708', 'paihz@live.com', '', '', '', '', '', '', '2016-07-21 16:27:15', 3, 0, '', '0000-00-00'),
(89, 'asd', '', 'sadasd@asdasd', '2a79542558166335a1203696038cd0ca', '4654654654', '', '', '5', '', '2016-07-21 17:29:48', 2, 0, '', '0000-00-00'),
(90, 'Al-Dosari Reema', '10155293974172619', 'ksu_sa@hotmail.com', '', '', '', '', '', '', '2016-07-23 08:34:28', 3, 0, '', '0000-00-00'),
(92, 'dfghdf', '', 'fdfdfgh@agsd.com', 'e10adc3949ba59abbe56e057f20f883e', '34563456', '', '', '1', '1', '2016-08-10 01:36:07', 2, 0, '', '0000-00-00'),
(93, 'Muhammad Eltayeb', '10153807869518342', 'moh_eltayeb2020@yahoo.com', '', '', '', '', '', '', '2016-08-15 17:30:25', 3, 0, '', '0000-00-00'),
(94, 'Justin McCombs', '10154148282473145', 'jtmccombs@gmail.com', '', '', '', '', '', '', '2016-08-16 18:47:14', 3, 0, '', '0000-00-00'),
(95, 'catella', '', 'smartwebfrance@gmail.com', '3ba7de1bbadfc2f8280f9019cfedfddf', '3367135517', '', '', '', '', '2016-08-17 06:42:29', 2, 0, '', '0000-00-00'),
(96, 'reza', '', 'abc@gmail.com', '815b4482c3bbf68e3ed3264bdebb22f4', '8221122341', '', '', '1', '3', '2016-08-17 09:24:01', 2, 0, '', '0000-00-00'),
(97, 'DiMas MuhammadFadillah', '672020079616159', 'dimas.triple@rocketmail.com', '', '', '', '', '', '', '2016-08-20 10:36:42', 3, 0, '', '0000-00-00'),
(98, 'a', '', 'a@a.com', '68830aef4dbfad181162f9251a1da51b', '1232132131', '', '', '6', '', '2016-08-22 08:09:08', 2, 0, '', '0000-00-00'),
(99, 'Pk', '', 'pk@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9687563245', '', '', '1', '3', '2016-08-23 09:45:28', 2, 0, '', '0000-00-00'),
(100, 'kiran', '', 'kiran@medsoft.com', '539d60e046342d8320727bbef8b098a1', '9898558741', '', '', '1', '4', '2016-08-25 06:31:22', 2, 0, '', '0000-00-00'),
(101, 'Rafael Sarrasqueiro', '1041105329300254', 'rafael114e@hotmail.com', '', '', '', '', '', '', '2016-08-28 01:34:14', 3, 0, '', '0000-00-00'),
(102, 'amitavaroy', '', 'amitava@mail.com', '095d611f2ced24942461da530e62b07d', '1587687', '', '', '1', '2', '2016-08-30 09:10:49', 2, 0, '', '0000-00-00'),
(103, 'Ismail Marzuqi', '10207343560412894', 'iesien22@yahoo.com', '', '', '', '', '', '', '2016-08-30 13:09:47', 3, 0, '', '0000-00-00'),
(104, 'Burham', '', 'rezka@gmail.com', '8c32b1f76c746d784f0c1fd005e2a220', '8213131233', '', '', '1', '3', '2016-08-30 15:09:18', 2, 0, '', '0000-00-00'),
(105, 'Multi Vendeur', '119809888472489', 'luxim67gg@gmail.com', '', '', '', '', '', '', '2016-08-31 13:08:39', 3, 0, '', '0000-00-00'),
(106, 'aaa', '', 'admin@example.com', '197b3add3c4657f8e9d6233b01b31183', '123456789', '', '', '1', '2', '2016-09-12 18:10:17', 2, 0, '', '0000-00-00'),
(108, 'khor', '', 'khoro918@hotmail.com', '6fe3ea8579a55615b47f17f7acfdbe13', '0168811709', '', '', '5', '', '2016-09-25 06:30:57', 2, 0, '', '0000-00-00'),
(109, 'Wedus', '', 'prifacom@yahoo.com', '21f303fb44ac7deae9527031551003ab', '123456', '', '', '1', '4', '2016-09-26 04:19:07', 2, 0, '', '0000-00-00'),
(110, 'mahmoud', '', 'mogahead@gmail.com', '7552747da92b1998eee7469a00daea71', '0111113015', '', '', '1', '3', '2016-09-26 21:27:35', 2, 0, '', '0000-00-00'),
(111, 'name', '', 'asd@asd.com', '078bbb4bf0f7117fb131ec45f15b5b87', '123456789', '', '', '7', '', '2016-10-03 03:09:27', 2, 0, '', '0000-00-00'),
(112, 'maheswaran', '', 'maheswaranphp@gmail.com', '49bb197bec17b7d20b2df6b1f3c3434a', '9543543543', '', '', '1', '1', '2016-10-14 11:57:28', 2, 0, '', '0000-00-00'),
(113, 'Jason', '', 'j.gerbes@me.com', 'e10adc3949ba59abbe56e057f20f883e', '0271111111', '', '', '6', '', '2016-10-16 02:50:53', 2, 0, '', '0000-00-00'),
(114, 'jubin', '', 'jubinmehta86@gmail.com', 'b99716c738bf817ab57eca6e2bd16b36', '9891117519', '', '', '1', '3', '2016-10-16 04:06:22', 2, 0, '', '0000-00-00'),
(115, 'trung', '', 'codertrung@gmail.com', 'c8dd80411c4d8e171dca372f000a65b8', '0986222512', '', '', '1', '4', '2016-10-17 16:17:25', 2, 0, '', '0000-00-00'),
(125, 'George Nammour', '', 'georgen@linteractif.com', 'f5fe3630496d417ef1beb821683238b6', '9613583114', '', '', '1', '1', '2016-11-15 13:21:04', 2, 0, '', '0000-00-00'),
(137, 'eseckailash', '', 'kailashkumar.r@esec.ac.in', 'e10adc3949ba59abbe56e057f20f883e', '1234567890', '', '', '1', '1', '2016-12-03 06:33:48', 2, 0, '', '0000-00-00'),
(138, 'kjjkjkjk', '', 'dfgfghgfh@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2123456666', '', '', '1', '1', '2016-12-03 06:35:03', 2, 0, '', '0000-00-00'),
(139, 'tester1.kuku', '', 'tester1.kuku@gmail.com', 'b3fe63bcbc4d61bc0a0089aaa9deaddf', '8788823771', 'address one', 'address two', '1', '2', '2016-12-03 07:30:42', 2, 0, '', '0000-00-00'),
(140, 'pofikailash', '', 'kailashkumar.r@pofitec.com', 'e10adc3949ba59abbe56e057f20f883e', '1234567890', '', '', '1', '1', '2016-12-22 05:42:31', 2, 0, '', '0000-00-00'),
(141, 'ramkumar', '', 'ramkumar@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7575757575757', '', '', '1', '1', '2016-12-30 06:58:46', 2, 0, '', '0000-00-00'),
(142, 'asd', '', 'tsenaltv12@gmail.com', '3440f0f817959b0cf74e7177e64ba121', '085463214785', '', '', '1', '4', '2017-01-12 20:50:28', 2, 0, '', '0000-00-00'),
(143, 'laxman', '', 'laxmankce@gmail.com', 'd516dc377f189b3ab734ec475bb8e70c', '8973899629', '', '', '1', '1', '2017-01-30 13:55:08', 2, 0, '', '0000-00-00'),
(144, 'Nur Hidayat', '', 'hidayat365@gmail.com', '57a93742dd6fe5c2e5d1465b827a3915', '8128156912', '', '', '8', '7', '2017-02-05 10:58:18', 2, 0, '', '0000-00-00'),
(145, 'laxman', '', 'lakshmanan@pofitec.com', 'e10adc3949ba59abbe56e057f20f883e', '1234567890', '', '', '1', '1', '2017-02-08 11:05:07', 2, 0, '', '0000-00-00'),
(147, 'Harry Okta Maulana', '', 'harry.okta.maulana2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '8571732559', '', '', '8', '7', '2017-02-27 10:33:38', 2, 0, '', '0000-00-00'),
(148, 'Rahmat Juniawan Junaidi', '', 'rahmatjs12@gmail.com', '2660f8a666c02b9b3275d4c5bea584cd', '0857666625524', 'JL Ciledug Indah', 'JL Ciledug Indah', '8', '7', '2017-02-28 03:10:41', 1, 0, '', '0000-00-00'),
(150, 'Harry Okta Maulana', '', 'harry.okta.maulana@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0857173255', 'JL. sndaskndksbakd', '', '8', '7', '2017-02-28 05:17:44', 2, 0, '', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_deals`
--

CREATE TABLE `nm_deals` (
  `deal_id` int(11) NOT NULL,
  `deal_title` varchar(500) NOT NULL,
  `deal_category` int(11) NOT NULL,
  `deal_main_category` int(11) NOT NULL,
  `deal_sub_category` int(11) NOT NULL,
  `deal_second_sub_category` int(11) NOT NULL,
  `deal_original_price` int(11) NOT NULL,
  `deal_discount_price` int(11) NOT NULL,
  `deal_discount_percentage` int(11) NOT NULL,
  `deal_saving_price` int(11) NOT NULL,
  `deal_start_date` datetime NOT NULL,
  `deal_end_date` datetime NOT NULL,
  `deal_expiry_date` date NOT NULL,
  `deal_description` text NOT NULL,
  `deal_merchant_id` int(11) NOT NULL,
  `deal_shop_id` int(11) NOT NULL,
  `deal_meta_keyword` varchar(250) NOT NULL,
  `deal_meta_description` varchar(500) NOT NULL,
  `deal_min_limit` int(11) NOT NULL,
  `deal_max_limit` int(11) NOT NULL,
  `deal_purchase_limit` int(11) NOT NULL,
  `deal_image_count` int(11) NOT NULL,
  `deal_image` varchar(500) NOT NULL,
  `deal_no_of_purchase` int(11) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `deal_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-active, 0-block',
  `deal_posted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_deals`
--

INSERT INTO `nm_deals` (`deal_id`, `deal_title`, `deal_category`, `deal_main_category`, `deal_sub_category`, `deal_second_sub_category`, `deal_original_price`, `deal_discount_price`, `deal_discount_percentage`, `deal_saving_price`, `deal_start_date`, `deal_end_date`, `deal_expiry_date`, `deal_description`, `deal_merchant_id`, `deal_shop_id`, `deal_meta_keyword`, `deal_meta_description`, `deal_min_limit`, `deal_max_limit`, `deal_purchase_limit`, `deal_image_count`, `deal_image`, `deal_no_of_purchase`, `created_date`, `deal_status`, `deal_posted_date`) VALUES
(1, 'Samsung Galaxy', 6, 26, 39, 59, 15999, 499, 97, 15500, '2016-06-08 16:42:05', '2017-03-08 16:42:05', '2017-03-08', 'Experience the new change with&nbsp;Samsung Galaxy S6&nbsp;which comprises of state-of-the-art functions and features. This smartphone has a big&nbsp;12.92 cm (5.1) Super AMOLED screen&nbsp;that offers colour rich picture performance with detailed textures. Enjoy capturing every moment with&nbsp;16 MP primary camera&nbsp;and take selfies with&nbsp;5 MP front camera.&nbsp;<div><br></div>', 9, 1, 'Samsung Galaxy s6 Best Deals', 'Crafted from glass and metal, the Samsung Galaxy S6 is a revolutionary smartphone that comes with a Quad HD Super AMOLED display, 16 MP camera and lightning-fast 64-bit, octa-core processor.', 1, 4, 0, 3, 's1Hs55vD5m.jpg/**/s1Hs55vD5m.jpg/**//**//**/', 0, '06/08/2016', 1, '2017-01-30 12:09:04'),
(2, 'Lenovo S90 Or Sisley S90', 1, 1, 2, 12, 12550, 499, 96, 12051, '2016-06-08 17:23:46', '2016-12-23 17:23:46', '2017-01-20', 'With a 127 mm screen size, the S90’s Super AMOLED screen packs an HD multimedia experience that’ll rock your socks off. You can watch movies while you commute to work, and share videos with friends and family.', 9, 1, 'Lenovo S90 ', 'With dual micro SIM slots, connect to 2G or 3G networks with ease. A-GPS allows you to broadcast your location and let your pals know where you are for a quick meet up.', 1, 4, 0, 3, 'l1SopvrOfv.jpeg/**/l1SopvrOfv.jpeg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 12:04:33'),
(3, 'Digital SLR Camera', 1, 9, 12, 13, 46737, 1999, 96, 44738, '2016-06-08 17:46:32', '2017-01-11 17:46:32', '2017-02-09', 'With the world’s fastest AF performance, you only need 0.06s to achieve perfect, timeless moments with the ?6000. Its compact body delivers superb image quality, making this a crowd pleaser among enthusiasts and professionals. World’s Fastest, AF (0.06 sec), 24.3MP Exmor APS HD CMOS + BIONZ X, 11 FPS Continuous Shooting, DSLR-style Operation, Enhanced OLED Tru-Finder, WiFi/NFC/PlayMemories Camera Apps.', 9, 1, 'Digital SLR Camera', 'With the world’s fastest AF performance, you only need 0.06s to achieve perfect, timeless moments with the ?6000. Its compact body delivers superb image quality, making this a crowd pleaser among enthusiasts and professionals.', 1, 4, 0, 3, 'd2Y6X5TAAr.jpg/**/d2Y6X5TAAr.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:00:51'),
(4, 'Athena Red & Black Layered Top', 3, 7, 10, 4, 600, 199, 67, 401, '2016-06-13 17:49:55', '2016-12-15 17:49:55', '2017-02-09', 'This stylish top from Athena is a chic must-have in your wardrobe. Team it with a pair of skinny jeans and a bracelet to look your best.', 9, 1, 'Western Wear', 'Red and black woven layered top, has a round neck, three-quarter sleeves with sleeveless on the left side, contrast asymmetric upper layer that extends to the right sleeve with keyhole detail', 1, 3, 0, 3, 'tN3JKuosC.jpg/**/tN3JKuosC.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:01:26'),
(5, 'Lifestyle Retail Pink Floral Print Top', 3, 7, 10, 4, 699, 100, 86, 599, '2016-06-08 18:09:26', '2016-12-16 18:09:26', '2017-02-15', 'Pink woven printed top, has a spread collar, a short button placket with pintucks on either side and pleats below, three-quarter sleeves with roll-up tabs, curved hem.', 9, 1, 'Lifestyle Retail Pink Floral Print Top', 'Lifestyle Retail brings to you a range of trendy tops to spruce up your wardrobe. Team this piece with a pair of jeans and heels to complete your look.', 1, 4, 0, 3, 'ttHEa6rOMB.jpg/**/ttHEa6rOMB.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:01:55'),
(6, 'Bitterlime Pack of 2 Leggings', 3, 7, 10, 15, 499, 99, 80, 400, '2016-06-08 18:29:59', '2017-01-05 18:29:59', '2017-02-15', 'Look trendy with this set of leggings from Bitterlime. Team it with a chic kurta or a long top and a pair of flats to complete your ensemble', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 1, 3, 0, 3, 'lwRCYSOvN.jpg/**/lwRCYSOvN.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:03:38'),
(7, ' Black-Gray Henley T Shirts', 2, 2, 3, 8, 399, 99, 75, 300, '2016-06-08 18:42:59', '2017-01-11 18:42:59', '2017-02-15', 'Free Spirit brings to you this Henley T-shirt that will be an excellent inclusion in your wardrobe. Made with premium quality material, this T-shirt brings out the best in your style. Its high-end material use makes it extremely comfortable to wear all throughout the day. It is also soft on the skin and, thus, does not cause any irritation or rashes. It also scores high on the aspect of style and is sure to win compliments for you. It is a must-have for people who prefer quality and reliability over everything.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'st1MQsGNe2y.JPG/**/st1MQsGNe2y.JPG/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:04:06'),
(8, 'Globalite Strike Navy Casual Shoes', 2, 10, 15, 17, 499, 99, 80, 400, '2016-06-24 19:04:56', '2017-01-12 19:04:56', '2017-02-15', 'Globalite continually creates what is aspired and not just what is necessary. All of its products are meant to deliver high performance, durability and great comfort.', 12, 5, 'Globalite Strike Navy Casual Shoes', 'Globalite continually creates what is aspired and not just what is necessary. All of its products are meant to deliver high performance, durability and great comfort.', 1, 4, 0, 3, 'shtNRf34ok.jpg/**/shtNRf34ok.jpg/**//**//**/', 0, '06/08/2016', 0, '2016-07-16 13:05:30'),
(9, ' Black Steel Wrist Watch', 2, 11, 17, 18, 1499, 199, 87, 1300, '2016-06-08 19:12:14', '2017-01-05 19:12:14', '2017-02-08', 'Japanese Movement :: Swiss Design :: Water Resistant :: Manufacturer Warranty :: Attractive Packing with Warranty Card :: Contemporary Modern Design :: Brass Steel Case :: Disclaimer: Product color may slightly vary due to photographic lighting sources or your monitor settings : Chronographs are Dummy and Do not Work.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'wYPovNF38.jpg/**/wYPovNF38.jpg/**//**//**/', 0, '06/08/2016', 0, '2016-07-16 13:05:57'),
(10, ' Babyhug Booties Bow Applique', 4, 6, 13, 14, 1500, 299, 80, 1201, '2016-06-08 19:45:56', '2016-12-15 19:45:56', '2017-01-20', 'Cute Walk Shoes by Babyhug meant for precious little feet. A range of super stylish &amp; comfortable footwear, fine crafted from top quality material. Materials and components are carefully chosen for their quality and lightness, ensuring that no shoes are heavier than the proportional age appropriate weight for the child''s body weight.', 12, 5, 'Baby Footwear', 'Cute Walk Shoes by Babyhug meant for precious little feet. A range of super stylish & comfortable footwear, fine crafted from top quality material. Materials and components are carefully chosen for their quality and lightness, ensuring that no shoes are heavier than the proportional age appropriate weight for the child''s body weight.', 1, 4, 0, 3, 'b4ZdiVx93F.jpg/**/b4ZdiVx93F.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:06:20'),
(11, 'Baby Full Sleeves Regular Tops', 4, 6, 18, 19, 799, 199, 75, 600, '2016-06-08 19:58:57', '2016-12-23 19:58:57', '2017-01-20', 'Product color may slightly vary due to photographic lighting sources or your monitor settings.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'btdP2Sy789.jpg/**/btdP2Sy789.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 14:38:03'),
(12, 'Samsung Galaxy A5', 1, 1, 1, 20, 19990, 599, 97, 19391, '2016-06-08 20:05:10', '2016-12-29 20:05:10', '2017-02-15', '4G&nbsp;Connectivity and Phone Features Depend on the Carrier and the Location of the User<br>', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 's1y3DywrGi.jpg/**/s1y3DywrGi.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 14:38:43'),
(13, 'Sony Xperia M4 Aqua', 1, 1, 1, 21, 18990, 699, 96, 18291, '2016-06-08 20:13:38', '2016-12-16 20:13:38', '2017-01-13', 'The waterproof camera phone designed for everyone. Get more with two powerful cameras, waterproofing and a 2-day battery. A 64-bit&nbsp;Octa-core&nbsp;phone with lightning-fast performance.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, 'sn5ntBRR0M.jpg/**/sn5ntBRR0M.jpg/**//**//**/', 0, '06/08/2016', 1, '2016-07-16 13:04:30'),
(14, 'Z Berries Women''s, Girl''s Shrug', 3, 7, 19, 22, 499, 99, 80, 400, '2016-06-09 10:19:33', '2016-07-07 10:19:33', '2016-07-29', 'Z Berries shrug is a must for your wardrobe any season. Pair it up with any outfit or a solid top &amp; footwear to look stylish and elegant for your outings. Made from Premium Viscose Lycra, These Regular-Fit Shrugs Are Lightweight And Comfortable To Wear All Day Long. Wear It Over A Sleeveless Top And Skinny-Fit Denims Or Shorts For An Ultimate Fashion.', 13, 6, 'Z Berries Women''s, Girl''s Shrug', 'Z Berries shrug is a must for your wardrobe any season. Pair it up with any outfit or a solid top & footwear to look stylish and elegant for your outings. Made from Premium Viscose Lycra, These Regular-Fit Shrugs Are Lightweight And Comfortable To Wear All Day Long. Wear It Over A Sleeveless Top And Skinny-Fit Denims Or Shorts For An Ultimate Fashion.', 1, 3, 0, 3, 'nTw35uEj3.jpg/**/sg5IAtBHbm.jpg/**/ggd3QlD5OL.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 13:17:58'),
(15, 'Garments Orange Gown For Girls', 4, 12, 20, 23, 600, 199, 67, 401, '2016-06-09 10:34:50', '2016-12-22 10:34:50', '2017-02-15', 'Dress your little darling in this beautiful Rani(Pink) coloured suit set from Aarika and watch as she fetches oodles of compliments and candies at the next wedding function you take her to.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, 'f1p9uvxNNc.jpg/**/f1p9uvxNNc.jpg/**//**//**/', 0, '06/09/2016', 1, '2016-07-16 13:02:45'),
(16, 'Noddy Clothing Full Sleeves Shirt', 4, 12, 20, 25, 799, 299, 63, 500, '2016-06-09 10:46:51', '2016-12-15 10:46:51', '2017-01-19', 'Dress up your son in this party wear shirt with Jacket by Noddy Original Clothing for the next evening out. Set contains a full sleeve check shirt and sleeveless Jacket with pockets. Pair this charming set with a classy pair of denims or chinos to give him a stylish party look.', 9, 1, 'Big Bazaar In Coimbatore', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'st1cjNNP6si.jpg/**/st1cjNNP6si.jpg/**//**//**/', 0, '06/09/2016', 1, '2016-11-01 23:40:11'),
(17, 'Pink Polka Dot Print A-Line Dress', 4, 12, 20, 23, 699, 99, 86, 600, '2016-06-09 10:53:15', '2016-07-07 10:53:15', '2016-07-29', 'Pink polka dot print&nbsp;A-line&nbsp;<a href="http://www.myntra.com/dress?src=pd" target="" rel="">dress</a>, has a round neck,&nbsp;<a href="http://www.myntra.com/cap?src=pd" target="" rel="">cap</a>&nbsp;sleeves, concealed zip closure on the back, flared hem with cut-out detail', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'd44o0jZ7HH.jpg/**/dZ5SzeaJY.jpg/**/d33eVJX6Cd.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 13:19:34'),
(18, 'Babyhug Full Sleeves Night Suit', 4, 12, 20, 24, 359, 99, 72, 260, '2016-11-01 10:57:38', '2016-12-31 10:57:38', '2016-12-31', 'Babyhug&nbsp;presents full sleeves night suit for your little boy. The set comprises of full sleeves top and Pyjama. Cute Dinosaur print on top and solid color Pyjama makes the set look attractive. Round neck ensures easy dressing.', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 4, 0, 3, 'bt3FFqSKfE.jpg/**/bt5TvUZQgCV.jpg/**/bt2A0ItJger.jpg/**/bt6QvyzpGru.jpg/**/', 0, '06/09/2016', 1, '2016-11-01 23:43:36'),
(19, 'Boys Ethnic Dhoti Kurta Set', 4, 12, 20, 26, 1299, 399, 69, 900, '2016-06-09 11:04:03', '2016-07-07 11:04:03', '2016-07-28', 'Kute Kids Boys Ethnic Dhoti Kurta Set.', 13, 6, 'Big Bazaar In Coimbatore', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 5, 0, 3, 'k1wYjjDmmO.jpg/**/k3Wh88S6Ra.jpg/**/k2votpiAH8.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 13:21:25'),
(20, 'Blue Net and geogertte Anarkali Suit', 4, 12, 20, 27, 1299, 299, 77, 1000, '2016-06-09 11:10:52', '2016-12-22 11:10:52', '2017-02-23', 'Blue shade Net and geogertte anarkali suit flaunts floral jacquard patterned and applique with fancy dangler enhanced yoke part. Gathered lower part beautifies the suit look. Comes with matching bottom and dupatta.&nbsp;<br>', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 4, 0, 3, 'auK6ix3De.jpg/**/auK6ix3De.jpg/**//**//**/', 0, '06/09/2016', 1, '2016-07-16 13:04:55'),
(21, 'Lee Slim Fit Fit Women''s Blue Jeans', 3, 7, 21, 28, 1235, 399, 68, 836, '2016-06-09 11:18:25', '2016-07-07 11:18:25', '2016-07-29', 'This model has a height of 5 feet 8 inches and is wearing a Jeans of Size 28.', 13, 6, 'Big Bazaar In Coimbatore', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, 'jevvRXU7R.jpg/**/j1oFRyU1qm.jpg/**/j46KYksBzd.jpg/**/j3Cm8gVTXP.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 13:22:07'),
(22, 'Javuli Cotton Solid Patiala', 3, 4, 6, 29, 799, 199, 75, 600, '2016-06-09 11:33:21', '2016-07-08 11:33:21', '2016-07-29', 'Look trendy with this set of Patiala&nbsp;from Bitterlime. Team it with a chic kurta or a long top and a pair of flats to complete your ensemble', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 4, 0, 3, 'p1QXYaqdIp.jpeg/**/p1QXYaqdIp.jpeg/**//**//**/', 0, '06/09/2016', 1, '2016-06-17 16:51:51'),
(24, 'Men''s Blue Jeans', 2, 2, 9, 2, 999, 199, 80, 800, '2016-07-13 11:48:43', '2016-12-07 11:48:43', '2017-01-19', 'This stylish Jeans from Athena is a chic must-have in your wardrobe. Team it with a pair of skinny jeans &nbsp;to look your best.', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 2, 5, 0, 3, 'j2JHhDI9ow.jpg/**/j2JHhDI9ow.jpg/**//**//**/', 0, '06/09/2016', 1, '2016-07-16 12:05:13'),
(25, 'Men''s Solid Casual Blue Shirt', 2, 2, 4, 1, 999, 99, 90, 900, '2016-06-09 11:55:27', '2016-07-07 11:55:27', '2016-07-28', 'This stylish Shitrs from Athena is a chic must-have in your wardrobe. Team it with a pair of skinny jeans to look your best.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'jZ4tcjVzB.jpg/**/htnq6jl2q.jpg/**/6mvzKoGeP.jpg/**/67kcjXMBHR.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:21:31'),
(26, 'Qpark White Linen Casual Shirt', 2, 2, 4, 1, 799, 99, 88, 700, '2016-06-09 11:58:25', '2016-07-07 11:58:25', '2016-07-29', 'This stylish Checked Shitrs from Athena is a chic must-have in your wardrobe. Team it with a pair of skinny jeans to look your best.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'lorJAUi6xOM.jpg/**/lo4wYZWxP16.jpg/**/lor2Lb8UsJAz.jpg/**/lo43pnnXuEM3.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:22:00'),
(27, 'Men''s Formal Navy Blue Cotton Shirt', 2, 2, 4, 1, 650, 99, 85, 551, '2016-06-09 12:09:42', '2016-07-08 12:09:42', '2016-07-29', 'Inspire the fashion freaks to try something out-of-the-box when you wear this casual shirt from the house of You Forever.Amplify your style quotient by wearing this button-down wonder that is fashioned using quality cotton for absolute comfort.So, keep partying like it''s your job in this comfy regular-fit casual shirt, pair of tapered jeans and suede loafers.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'c18gAMVO1b.jpg/**/c29J1ep6J7.jpg/**/c3y9ioRmPt.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 14:22:22'),
(28, 'Red T Shirt', 2, 2, 3, 9, 599, 99, 83, 500, '2016-06-09 12:15:23', '2016-07-07 12:15:23', '2016-07-30', 'This stylish T-Shitrs from Athena is a chic must-have in your wardrobe. Team it with a pair of skinny jeans to look your best.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, 'ts2IaGKAThz.jpg/**/ts3KLktwCqk.jpg/**/ts4WqTX1HxR.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 14:24:22'),
(29, 'Men''s Slim Fit Yellow TShirt ', 2, 2, 3, 8, 599, 99, 83, 500, '2016-06-09 12:19:26', '2016-07-07 12:19:26', '2016-07-28', 'Peter England Men''s Blended Shirt.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, '37RxrqPjr.jpg/**/aFkQGdeoX.jpg/**/2KnnhCsrC.jpg/**/aZ2Eraqvn.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:23:25'),
(30, 'Lenovo A6000 (8GB, Black)', 1, 1, 2, 6, 8000, 599, 93, 7401, '2016-06-09 12:25:29', '2016-07-07 12:25:29', '2016-07-29', 'From listening to music to watching videos, sharing data to editing documents on-the-go, you can do it all seamlessly on Lenovo A6000 8GB Black smartphone. An affordable smartphone with advanced features, it comes with a large 12.7 cm (5) HD display screen and quad-core 1.2 GHz processor. With 1 GB RAM and Android 4.4.4 KitKat operating system, it delivers hi-speed performance. You can take advantage of two distinct networks using its dual SIM feature.&nbsp;', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 5, 0, 3, 'l8kcmEDDS.jpg/**/l1LNASgW5t.jpg/**/l3FFFPdKOG.jpg/**/l4Xi4I4nLG.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:25:31'),
(31, 'Lenovo S580 8GB Black', 1, 1, 2, 6, 8775, 1599, 82, 7176, '2016-06-09 12:29:29', '2016-07-07 12:29:29', '2016-07-29', 'From clicking high-resolution images to recording HD videos, making video calls to fast-speed web browsing, Lenovo S580 Smartphone does it all seamlessly. An affordable smartphone with advanced features, it comes with a large 5 inch HD display screen and quad-core 1.2 GHz processor. With 1 GB RAM and Android 4.3 Jellybean, it delivers hi-speed performance. You can take advantage of two distinct networks using its Dual Sim feature.&nbsp;', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 2, 5, 0, 3, 'l9S7beRmYl.jpg/**/l8gw55oWz8.jpg/**/l71NVrP5l4.jpg/**/l54PGrQ3nkU.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:24:41'),
(32, 'Lenovo S850 (16GB, White)', 1, 1, 2, 6, 8290, 599, 93, 7691, '2016-06-09 12:34:59', '2016-07-07 12:34:59', '2016-07-28', 'Designed with advanced features along with a stylish look, the new Lenovo S850 smartphone is ready to amaze you with its excellent performance. Equipped with the latest MTK 6582 1.3 GHz Quad-core PowerVR SGX544 processor along with the new Android™ 4.4.2 KitKat Operating System (OS), this outstanding smartphone facilitates you with seamless multitasking and smooth operating experience. Watch your favourite videos and play graphic intensive games on a bright 12.7 cm (5) capacitive touchscreen of the Lenovo smartphone.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 2, 5, 0, 3, 'mkZTOJ2Y7.jpeg/**/m2ACZ3ggTI.jpg/**/m3OxdOfJQm.jpg/**/m4eYuOlPKI.jpg/**/', 0, '06/09/2016', 0, '2016-06-17 14:26:19'),
(33, 'Sony Cybershot WX220 Digital Camera', 1, 9, 12, 13, 9350, 799, 91, 8551, '2016-06-09 12:54:40', '2016-07-07 12:54:40', '2016-07-28', 'With Built-In Wi-Fi and full HD (High Definition) recording facility,&nbsp;Sony WX80&nbsp;weighs only 124 grams and can easily slip-on in your pockets. This 16.2 mega pixels Sony Cyber Shot digital camera consists of Exmor® R CMOS Image Sensor and 8x Optical Zoom, which clicks a high quality image with every minute detail.', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 3, 0, 3, 'caUYJkVR80.jpg/**/c3ZaVUa3La.jpg/**/c4JD1G7JA5.jpg/**/c1Hm9jfyzf.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:25:53'),
(34, 'Highlander Blue Slim Fit Shirt', 2, 2, 4, 1, 599, 99, 83, 500, '2016-06-09 13:26:26', '2016-07-07 13:26:26', '2016-07-29', 'Blue and red checked&nbsp;casual shirt, has a spread collar, a full button placket, long sleeves with roll-up tab features, a patch pocket,&nbsp;a curved hem', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'sQsvf4PxO.jpg/**/s2dlBvVg9Q.jpg/**/s3eGWHrSDL.jpg/**/s34yG5674ZE.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:57:28'),
(35, 'Red Sandals For Kids', 4, 6, 13, 14, 250, 59, 76, 191, '2016-06-09 13:37:35', '2016-07-08 13:37:35', '2016-07-29', 'Your little one will look all grown up with this baby sandal from Little''s. Neutral enough to wear with anything, the cushioning foot bed will mould to your child’s foot making this sandal super comfortable.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, '1hpR6EfeT.jpg/**/2v2T09d0d.jpg/**/5tcqxIu0b.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 14:58:27'),
(36, 'Yellow Solid Tops', 3, 7, 10, 4, 699, 99, 86, 600, '2016-06-13 13:41:33', '2016-07-07 13:41:33', '2016-07-29', '<div>Double your fashion flair as you wear this top, in white coloured, from the house of Ar2. Fashioned using cotton blend, this top will keep you at comfort all day long. This beautiful creation will give you a trendy look when clubbed with matching leggings and sandals.</div>', 9, 1, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 2, 5, 0, 3, 'yFbnmih72.jpg/**/y1nj18U3O0.jpg/**/y3aPSmqfBz.jpg/**/y4G1NkDTiq.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:25:04'),
(37, 'Red Solid Tops', 3, 7, 10, 4, 799, 199, 75, 600, '2016-06-09 13:50:40', '2016-07-07 13:50:40', '2016-07-29', '<div>Double your fashion flair wearing this red coloured top from the house of Lynda. Fashioned using georgette, this top will keep you at comfort all day long. Designed to perfection, this classy top will look great when clubbed with trousers and black pumps.</div>', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'ryZ8CWSJJ.jpg/**/3pPDMTGma.jpg/**/2Dq1y5Y5P.jpg/**/riRzYCTkr.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:58:47'),
(38, 'Yellow Striped Tops', 3, 7, 10, 4, 699, 99, 86, 600, '2016-06-09 13:57:02', '2016-07-07 13:57:02', '2016-07-29', '<div>Look like an enchanting beauty as you make your way for a dinner outing with your friends wearing this yellow top from Fabindia. Made from khadi cotton, this short top features a high neck with tiny cut out detail on the shoulders and has tiny pleats on the front yoke. It also has full sleeves and comes in regular fit. This chequered top &nbsp;can be teamed with a pair of jeans and beige wedges to complete your stylish look.&nbsp;</div>', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 4, 0, 3, 'kghdqgqq8.jpg/**/k2exbZ3unr.jpg/**/k3uq7gUp5x.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 14:59:15'),
(39, 'Samsung Galaxy Core 2 (White)', 1, 1, 1, 10, 6990, 599, 91, 6391, '2016-06-09 14:03:21', '2016-07-08 14:03:21', '2016-07-29', 'Power-packed with high end features along with an impressive design, the new Samsung Galaxy Core 2 4GB Phone is ready to dazzle you with its outstanding performance. Equipped with the latest Android KitKat 4.4.2 operating system along with Quad core 1.2GHz processor, this excellent smartphone thrills you with seamless multitasking and smooth operating experience. Watch your favourite videos and play graphic intensive games on a bright 11.43 cm (4.5) capacitive touchscreen of the Samsung smartphone.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'mtkAQ0cGI.jpg/**/m1z8bSLA8G.jpg/**/mokX5FPz2Y.jpg/**/m43eqbC899a.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 15:00:27'),
(40, 'Samsung Galaxy NFC Sticker', 1, 1, 1, 10, 8990, 599, 93, 8391, '2016-06-09 14:06:48', '2016-07-07 14:06:48', '2016-07-29', 'smartphone which is specially designed for bike riders? Samsung Galaxy J3 8GB 4G gives you an uninterrupted riding experience with its advanced S bike mode. Simply, activate the S bike mode and enjoy riding without worrying about answering your phone calls or checking notifications. This dual SIM smartphone packs all the latest features required by a professional go getter. It comes with a large 12.7 cm (5) super AMOLED display, Android Lollipop Operating system, 4G LTE connectivity, 1.5 GB of RAM plus a powerful 2600 mAh battery that makes it a must-buy. From playing games to working on your business projects, you can do it all with this advanced smartphone brought to you by Samsung. Additionally, it comes along with 1 year Samsung India warranty. &nbsp; &nbsp; &nbsp;', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'sZiSdcxPW.jpg/**/s1qhkjheYy.jpg/**/s2bJucShZJ.jpg/**/s3JrcF9keZ.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 15:01:07'),
(41, 'White Round Neck Top', 3, 7, 10, 4, 399, 59, 85, 340, '2016-06-09 14:13:51', '2016-07-07 14:13:51', '2016-07-29', 'Look trendy with this Tops &nbsp;from Bitterlime. Team it with a chic kurta or a long top and a pair of flats to complete your ensemble', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 3, 'wRDqSvmIf.jpg/**/w3f8XK1Atg.jpg/**/w2ywB53hRZ.jpg/**/w4J4qZzRyu.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 15:01:53'),
(44, 'Johnson''s Avocado Hair Oil', 4, 6, 22, 33, 155, 49, 68, 106, '2016-06-09 14:38:33', '2016-07-07 14:38:33', '2016-07-29', 'Grooming your baby and making him look good shows great care and love on your part. Keep his hair healthy and nourish it with this Johnson’s Baby Avocado Hair Oil.', 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 1, 3, 0, 0, 'hmp6GzHZd.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:58:07'),
(45, 'Johnson''s Baby Powder', 4, 6, 22, 34, 159, 49, 69, 110, '2016-06-09 22:19:28', '2016-07-08 22:19:28', '2016-07-29', 'Himalaya baby lotion has soothing and antimicrobial properties, which keeps your baby''s skin healthy and soft. 1. Protects skin 2. Adds moisture 3. Nourishes skin 4. Key Ingredients: Olive oil, enriched with vitamin E, nourishes, protects and softens skin and prevents chafing. Himalaya''s Baby Cream is specially formulated to protect your little one''s chapped cheeks, ''crawler''s knee'', tender nose and rubbed elbows. 1. It not only moisturize baby''s skin but also preserves its natural softness. 2. Country Mallow, an antioxidant, has nourishing properties and protects baby''s skin from germs. 3. Licorice protects and soothes baby''s skin.', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 4, 0, 2, 'byhnUey1L.jpg/**/byhnUey1L.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 16:43:34'),
(46, 'Chicco Concentrated Fabric Softener ', 4, 6, 22, 34, 341, 159, 53, 182, '2016-06-09 22:25:35', '2016-07-08 22:25:35', '2016-07-29', 'Chicco''s Softener for sensitive skin freshens and softens all baby''s garments without irritating its sensitive skin. The natural additives make the fabrics soft and gently perfumed, without damaging fibres of colours. Dermatological tested, nickel tested, antistatic and biodegradable', 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 1, 4, 0, 2, 'lo30IWAF9xq.jpg/**/lo2YBkYg020.jpg/**//**/', 0, '06/09/2016', 1, '2016-06-17 15:15:03'),
(47, 'Multi Purpose Diaper Shoulder Bag ', 4, 6, 7, 35, 949, 399, 58, 550, '2016-06-09 23:01:51', '2016-07-07 23:01:51', '2016-07-29', 'High Quality Durable Material.Easy to Clean.Adjustable/Removable Shoulder Strap.Beautiful design.Carry Handles.Lightweight and Compact.', 9, 1, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 2, 4, 0, 3, 'bZ2rIjqYE.jpg/**/61aEM8UjwuLWkArpojw._SL1000_/**/b3yjJZe04e.jpg/**/b4AXW8yRfZ.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 15:08:27'),
(48, 'Littly Baby Dungaree Set ', 4, 12, 20, 24, 799, 499, 38, 300, '2016-06-09 23:10:01', '2016-07-08 23:10:01', '2016-07-29', 'Littly presents a cute and adorable baby dungaree set for your little one. Made from soft and comfortable material, this dungaree set comes with a trendy round neck cotton t-shirt with shoulder loops for ease and comfort fit. Dungaree is made of soft corduroy fabric and has adjustable straps with two button closures. The embroidery at the front gives it a stylish look. This dungaree set is an ideal pick for this summer and is available in multiple summer cool colors. Note: Actual product may slightly vary in terms of color / design and will be shipped based on availability.', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 1, 5, 0, 2, 'bdA5Kr5OXk.jpg/**/bd1vWXy881I.jpg/**/bd3BhyjbtbA.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 15:13:58'),
(49, 'Baby Boys'' Denim Clothing Set', 4, 12, 20, 24, 599, 399, 33, 200, '2016-06-09 23:16:50', '2016-07-08 23:16:50', '2016-07-29', 'This is a regular fit baby boys top and dungaree is made of denim material and also has half sleeve and a round neck. It is best suitable for casual wear.', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 1, 4, 0, 3, 'dQeaErVRi.jpg/**/4TkxgVYTQ.jpg/**/2rhoSsaxj.jpg/**/d3ANGDMCZI.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 15:14:43'),
(50, 'Princess Flower Petal Tulle Dress ', 4, 12, 20, 23, 1999, 699, 65, 1300, '2016-06-09 23:27:36', '2016-07-07 23:27:36', '2016-07-29', 'Make your little girl look stylish and pretty in this comfortable rose petals style tulle dress with embedded pearl like beads for a rich look and lace dress. Sleeveless Lace tulle dress with pearl like beads adds to the beauty. Its a perfect summer party dress.', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 1, 4, 0, 3, 'drjc9TOuOwI.jpg/**/7soYpTKOi.jpg/**/hgcbkeAJA.jpg/**/55IwcjJHFg.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:23:57'),
(51, 'Pinehill Cuffed Booties & Mittens Set', 4, 6, 13, 14, 425, 229, 46, 196, '2016-06-09 23:46:43', '2016-07-07 23:46:43', '2016-07-29', 'Let your cute little one stay cozy in the cold anytime in this mitten and booties set form Pinehill. This cute set booties and mittens is perfect for babies on the go. The cute solid color dual shade makes it even more cute and stylish. This cuffed ribbed style adds an element of cuteness to this set. It is specially designed to keep your baby warm and stylish while you are in the park or just strolling. Made up of high quality fabric which is soft and gentle on your baby''s skin.&nbsp;<br>', 12, 5, 'Globalite Strike Navy Casual Shoes', 'Globalite continually creates what is aspired and not just what is necessary. All of its products are meant to deliver high performance, durability and great comfort.', 1, 3, 0, 3, 'bIr0144NC.jpg/**/b5dulzFqdA.jpg/**/b3N7oBLSQ9.jpg/**/b2O7dO0bmn.jpg/**/', 0, '06/09/2016', 1, '2016-06-17 14:59:39'),
(52, 'Women''s Viscose Dhoti', 3, 4, 6, 29, 1199, 699, 42, 500, '2016-06-09 23:53:51', '2016-07-08 23:53:51', '2016-07-29', '<div>Rajnandini Combo Of 4 Plain Women Leggings gives a trenty look.</div>', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 2, 6, 0, 2, '1x0QJmjT3.jpg/**/2KGEokUlH.jpg/**/3RcFF0Td1.jpg/**/', 0, '06/10/2016', 1, '2016-06-17 15:00:02'),
(53, 'Pannkh Women Pack of 3 Leggings', 3, 4, 6, 29, 699, 399, 43, 300, '2016-06-10 00:09:12', '2016-07-08 00:09:12', '2016-07-29', '<div><div><div><div><div>These versatile churidar leggings from Pannkh are a must-have in your ethnic wear staples. Team them with a kurta and flats to look your best.<br></div></div></div></div></div><div></div><div><div></div></div>', 12, 5, 'Trinity Place Department Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play. There is also a selection of signature funky neon leather handbags that are reasonably priced and make a statement.', 1, 4, 0, 3, 'l7qMGfTnz3.jpg/**/l1KjSysSqL.jpg/**/k8lvyOynnK.jpg/**/l7GvGaaA0Z.jpg/**/', 0, '06/10/2016', 0, '2016-06-17 15:14:20'),
(54, 'Johnsons  Baby Shampoo (400ml)', 4, 6, 22, 33, 150, 99, 34, 51, '2016-06-13 13:39:15', '2016-07-08 13:39:15', '2016-07-29', 'Baby Soap helps moisturise your baby''s delicate skin by reducing moisture loss while cleansing it ever so gently. Gentle care your baby''s delicate skin needs in the growing years.<br>', 9, 1, 'Rosan Bag Mall in Coimbtore', 'Rosan Bag Mall in Coimbtore', 2, 4, 0, 0, 'sKQni1Lqa.jpg/**/', 0, '06/13/2016', 1, '2016-06-17 15:13:33'),
(55, 'Johnson''s Baby Powder 700 g', 4, 6, 22, 33, 200, 159, 21, 41, '2016-06-14 10:28:37', '2016-07-08 10:28:37', '2016-07-29', 'Johnson &amp; Johnson is an American multinational medical devices, pharmaceutical and consumer packaged goods manufacturer founded in 1886. Johnson &amp; Johnson''s brands include numerous household names of medications and first aid supplies. Among its well-known consumer products are the Band-Aid Brand line of bandages, Tylenol medications, Johnson''s baby products, Neutrogena skin and beauty products, Clean &amp; Clear facial wash and Acuvue contact lenses.', 12, 5, 'Rosan Bag Mall in Coimbatore', 'Rosan Bag Mall in Coimbatore', 1, 4, 0, 0, 'gL7be9HJC.jpg/**/', 0, '06/14/2016', 1, '2016-06-17 15:12:56'),
(56, 'John Players Blue Cotton T-Shirt', 2, 2, 3, 7, 599, 399, 33, 200, '2016-06-14 11:20:54', '2016-12-15 11:20:54', '2017-02-15', 'John Players offers a complete fashion wardrobe to the male youth of today. The brand stands for style, charisma and attitude and brings them into your wardrobe with its vibrant yet relaxed collection. Incorporating the most contemporary trends with a splash of youthful energy, playful styling &amp; trendy collections, John Players knows the pulse of the youth and offers clothing for the discerning youth. John Players offers complete range in men''s apparel with the coolness quotient being an inseparable part of its genes.Offering vibrant wardrobe essentials spanning across Formal wear, Casual wear, Party wear, Jeans &amp; Accessories, John Players has everything that appeals to the new generation.', 12, 5, 'Rosan Bag Mall in Coimbatore', 'Rosan Bag Mall in Coimbatore', 1, 4, 0, 2, 'ttqoYMapiD.jpg/**/ttqoYMapiD.jpg/**//**/', 0, '06/14/2016', 1, '2016-07-16 13:02:23'),
(57, 'mob', 1, 8, 11, 5, 500, 20, 96, 480, '2016-09-07 17:16:20', '2016-09-10 17:16:20', '2016-09-15', 'dscription', 15, 11, 'test shop\r\nclock\r\n', 'Meta description*', 5, 10, 0, 0, '12994318_1120388151339038_3085805154032470004_ndMy1wig8.jpg/**/', 0, '09/07/2016', 0, '2016-09-07 20:56:03'),
(58, 'Apple iPad Mini 2 Retina Display Wifi + Cellular - 16GB - Silver ', 6, 26, 40, 60, 1000000, 700000, 30, 300000, '2017-01-19 15:47:09', '2017-01-28 15:47:09', '2017-01-29', 'Weekend promo iPad<br>', 33, 32, 'promo1', 'promo', 2, 20, 0, 0, 'apple-ipad-mini-2-retina-display-wifi-cellular-16gb-silver-3234-9243084-b1a841710c0d15dd433cf625db0af7e5-zoomeesLJaXL.jpg/**/', 1, '01/19/2017', 1, '2017-01-23 08:15:59'),
(59, 'Polo Milano Hard Case #1102 28" - Putih - Grade D', 5, 27, 41, 61, 12000, 900, 93, 11100, '2017-01-19 18:46:30', '2017-01-21 18:46:30', '2017-01-22', 'Handle Trolley ....<br>Security lock ...<br>', 33, 32, 'trolley', 'bag', 3, 10, 0, 0, 'polo_milano_putih__2OyZ7xBZP.jpg/**/', 0, '01/19/2017', 0, '2017-01-19 11:54:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_emailsetting`
--

CREATE TABLE `nm_emailsetting` (
  `es_id` tinyint(3) UNSIGNED NOT NULL,
  `es_contactname` varchar(150) NOT NULL,
  `es_contactemail` varchar(150) NOT NULL,
  `es_webmasteremail` varchar(150) NOT NULL,
  `es_noreplyemail` varchar(150) NOT NULL,
  `es_phone1` varchar(20) NOT NULL,
  `es_phone2` varchar(20) NOT NULL,
  `es_latitude` decimal(18,14) NOT NULL,
  `es_longitude` decimal(18,14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_emailsetting`
--

INSERT INTO `nm_emailsetting` (`es_id`, `es_contactname`, `es_contactemail`, `es_webmasteremail`, `es_noreplyemail`, `es_phone1`, `es_phone2`, `es_latitude`, `es_longitude`) VALUES
(1, 'StoreFront', 'untung@storefront.co.id', 'admin@storefront.co.id', 'sales@storefront.co.id', '+6287888237701', '+6287888237701', '-6.22790400000000', '106.98360800000000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_enquiry`
--

CREATE TABLE `nm_enquiry` (
  `id` int(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(50) NOT NULL,
  `created_date` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_enquiry`
--

INSERT INTO `nm_enquiry` (`id`, `name`, `email`, `phone`, `message`, `status`, `created_date`) VALUES
(1, 'charles', 'charlesvictor.j@pofitec.com', '9498056637', 'Test Demo', 0, ''),
(2, 'fgfdgdf', 'dfhdfhd@gmail.com', '1234567890', 'dfsgsfgfgfdh', 0, ''),
(5, 'yuyuy', 'pofidevelopmentyuyuy@gmail.com', '8883152529', 'teerdt', 1, '06/04/2016'),
(7, 'ilakkiya m', 'il@gm.com', '8883152529', 'rttrtr', 1, '06/03/2016'),
(8, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'hi', 0, ''),
(9, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'hi', 0, ''),
(10, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'hi', 0, ''),
(11, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'hi', 0, ''),
(12, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'hi', 0, ''),
(13, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'hi', 0, ''),
(14, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'twest', 0, ''),
(15, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'test', 0, ''),
(16, 'maheshwaran', 'maheswaran@pofitec.com', '1234567890', 'test', 0, ''),
(17, 'Amit', 'amit.srivastava@tradebooster.com', '9350352736', 'Hi,\r\n\r\nWe would link to purchase comment with star rating plugin.\r\n\r\nPlease confirm cost.', 0, ''),
(18, 'raj', 'kumar@pofitec.com', '12131232', 'test', 0, ''),
(19, 'Teresa', 'rlbojfanxv@meldram.com', 'http://v-doc.co/nm/txxrz', 'I was just looking at your eCommerce Software; Shopping Cart Software | Laravel E Commerce website and see that your website has the potential to become very popular. I just want to tell you, In case you don''t already know... There is a website service which already has more than 16 million users, and most of the users are interested in websites like yours. By getting your site on this service you have a chance to get your site more visitors than you can imagine. It is free to sign up and you can find out more about it here: http://hothor.se/1gj55 - Now, let me ask you... Do you need your website to be successful to maintain your business? Do you need targeted traffic who are interested in the services and products you offer? Are looking for exposure, to increase sales, and to quickly develop awareness for your site? If your answer is YES, you can achieve these things only if you get your website on the service I am describing. This traffic network advertises you to thousands, while also giving you a chance to test the network before paying anything at all. All the popular websites are using this service to boost their readership and ad revenue! Why aren’t you? And what is better than traffic? It’s recurring traffic! That''s how running a successful website works... Here''s to your success! Find out more here: http://www.v-diagram.com/2sv1p', 0, ''),
(20, 'kailash', 'kumarkailash075@gmail.com', '1234567890', 'fgdfgdfg', 0, ''),
(21, 'kailash', 'kumarkailash075@gmail.com', '1234567890', 'fgdfgdfgfdgfdgfd', 0, ''),
(22, 'kailash', 'kumarkailash075@gmail.com', '1234567890', 'ghjghjghjghj', 0, ''),
(23, 'kailash', 'kumarkailash075@gmail.com', '1234567890', 'ghjghjghjghj', 0, ''),
(24, 'kailash', 'kumarkailash075@gmail.com', '1234567890', 'ghjghjghjghj', 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_estimate_zipcode`
--

CREATE TABLE `nm_estimate_zipcode` (
  `ez_id` int(11) NOT NULL,
  `ez_code_series` int(11) NOT NULL,
  `ez_code_series_end` int(11) NOT NULL,
  `ez_code_days` int(11) NOT NULL,
  `ez_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_estimate_zipcode`
--

INSERT INTO `nm_estimate_zipcode` (`ez_id`, `ez_code_series`, `ez_code_series_end`, `ez_code_days`, `ez_status`) VALUES
(1, 641001, 641051, 2, 1),
(2, 642001, 642001, 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_faq`
--

CREATE TABLE `nm_faq` (
  `faq_id` smallint(5) UNSIGNED NOT NULL,
  `faq_name` varchar(100) NOT NULL,
  `faq_ans` text NOT NULL,
  `faq_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_faq`
--

INSERT INTO `nm_faq` (`faq_id`, `faq_name`, `faq_ans`, `faq_status`) VALUES
(1, 'Wondering where to sell online?', 'Dip Multivendor is the best place to sell online and make money. With over 100 million buyers, Dip Multivendoris one of the world''s largest online marketplaces. With so many buyers, Dip Multivendor gives you the opportunity to find more buyers to pay you more for your items.', 1),
(2, 'What is a product detail page?', 'A product detail page is the place where customers read details about a product for sale on Dip Multivendor. It includes the product image, price, description, customer reviews, ordering options, and the link to view offers from all sellers.', 1),
(3, 'Why do I need to create a product detail page?', 'Dip Multivendor has one of the largest online product catalogs. But we don''t have everything. If you can''t find a product detail page on Dip Multivendor for something you''d like to sell, then you need to create one. Once created, the product detail page will be available onDip Multivendor for you to sell your inventory, and other sellers can also use too.', 0),
(4, 'What will the product detail page I create look like?', 'The product detail page you create will look like any other product page on Dip Multivendor. By creating pages that use a standard format, customers can more easily evaluate the products they want to buy.', 0),
(5, 'How do I create a product detail page?', 'First, you confirm that your product isn''t already listed on Dip Multivendor. Second, you identify the product category and describe it. Third and last, you set the price and condition for each item. For more information, ', 0),
(6, 'What information can I include in my product description?', 'You are allowed 2,000 characters to describe your product. For some product categories, you also have five key product features (bullets) of 100 characters each for highlighting significant product attributes. ', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_generalsetting`
--

CREATE TABLE `nm_generalsetting` (
  `gs_id` tinyint(4) NOT NULL,
  `gs_sitename` varchar(100) NOT NULL,
  `gs_metatitle` varchar(150) NOT NULL,
  `gs_metakeywords` text NOT NULL,
  `gs_metadesc` text NOT NULL,
  `gs_defaulttheme` tinyint(3) UNSIGNED NOT NULL,
  `gs_defaultlanguage` tinyint(3) UNSIGNED NOT NULL,
  `gs_payment_status` varchar(50) NOT NULL,
  `gs_themes` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_generalsetting`
--

INSERT INTO `nm_generalsetting` (`gs_id`, `gs_sitename`, `gs_metatitle`, `gs_metakeywords`, `gs_metadesc`, `gs_defaulttheme`, `gs_defaultlanguage`, `gs_payment_status`, `gs_themes`) VALUES
(1, 'StoreFront Ecommerce', 'StoreFront Ecommerce', 'StoreFront Ecommerce Multivendor', 'StoreFront Ecommerce Multivendor', 1, 1, 'COD', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_imagesetting`
--

CREATE TABLE `nm_imagesetting` (
  `imgs_id` smallint(6) NOT NULL,
  `imgs_name` varchar(150) NOT NULL,
  `imgs_type` tinyint(4) NOT NULL COMMENT '1- logo,2 -Favicon,3-noimage'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_imagesetting`
--

INSERT INTO `nm_imagesetting` (`imgs_id`, `imgs_name`, `imgs_type`) VALUES
(1, 'logo-web-baru.png', 1),
(2, 'logo-store-front-favN8pIyoClXJ0Tj7mw.png', 2),
(3, 'EmptycLdH190y.png', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_inquiries`
--

CREATE TABLE `nm_inquiries` (
  `iq_id` int(10) UNSIGNED NOT NULL,
  `iq_name` varchar(100) NOT NULL,
  `iq_emailid` varchar(150) NOT NULL,
  `iq_phonenumber` varchar(20) NOT NULL,
  `iq_message` varchar(300) NOT NULL,
  `inq_readstatus` int(11) NOT NULL DEFAULT '0' COMMENT '0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_inquiries`
--

INSERT INTO `nm_inquiries` (`iq_id`, `iq_name`, `iq_emailid`, `iq_phonenumber`, `iq_message`, `inq_readstatus`) VALUES
(1, 'charles', 'charlesvictor.j@pofitec.com', '9498056637', 'Test File', 1),
(2, 'charles', 'charlesvictor.j@pofitec.com', '9498056637', 'dfhdhdf', 1),
(3, 'charles', 'charlesvictor.j@pofitec.com', '9498056637', 'sdfgfshdfh', 1),
(4, 'cffdh', 'dfhdfhfd@gmail.com', '56465456', 'dsgdgsdgsd', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_language`
--

CREATE TABLE `nm_language` (
  `la_id` smallint(5) UNSIGNED NOT NULL,
  `la_code` varchar(7) NOT NULL,
  `la_name` varchar(30) NOT NULL,
  `la_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_login`
--

CREATE TABLE `nm_login` (
  `log_id` int(5) NOT NULL,
  `cus_id` int(5) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-wesite,2 facebook'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_login`
--

INSERT INTO `nm_login` (`log_id`, `cus_id`, `log_date`, `log_type`) VALUES
(1, 2, '2014-08-09 05:46:12', 1),
(2, 4, '2014-08-11 23:07:20', 1),
(3, 1, '2014-08-11 23:08:36', 1),
(4, 1, '2014-08-11 23:38:03', 1),
(5, 1, '2014-08-11 23:40:12', 1),
(6, 1, '2014-08-11 23:52:03', 1),
(7, 1, '2014-08-12 00:03:43', 1),
(8, 1, '2014-08-12 00:25:54', 1),
(9, 5, '2014-08-12 01:25:16', 1),
(10, 1, '2014-08-12 22:41:56', 1),
(11, 1, '2014-08-12 23:12:51', 1),
(12, 1, '2014-08-13 00:13:15', 1),
(13, 1, '2014-08-17 23:45:10', 1),
(14, 1, '2014-08-18 04:15:54', 1),
(15, 1, '2014-08-19 00:09:47', 1),
(16, 21, '2014-08-20 22:21:20', 1),
(17, 21, '2014-08-22 22:33:16', 1),
(18, 7, '2014-08-27 22:41:28', 1),
(19, 7, '2014-08-27 22:42:08', 1),
(20, 1, '2014-09-03 04:46:15', 1),
(21, 1, '2014-09-03 04:57:41', 1),
(22, 1, '2014-09-03 04:58:26', 1),
(23, 26, '2014-09-03 05:01:06', 1),
(24, 42, '2014-11-24 12:04:30', 1),
(25, 40, '2014-11-24 13:00:14', 1),
(26, 40, '2014-11-24 17:20:46', 1),
(27, 46, '2015-04-25 13:14:12', 1),
(28, 48, '2015-04-25 13:18:06', 1),
(29, 46, '2015-04-25 13:19:02', 1),
(30, 53, '2015-07-05 05:45:14', 1),
(31, 64, '2015-11-18 10:22:11', 1),
(32, 76, '2015-11-18 12:32:25', 1),
(33, 76, '2015-11-18 12:36:21', 1),
(34, 76, '2015-11-18 12:46:03', 1),
(35, 76, '2015-11-18 12:48:44', 1),
(36, 76, '2015-11-18 13:35:30', 1),
(37, 76, '2015-11-18 14:02:46', 1),
(38, 76, '2015-11-19 05:11:05', 1),
(39, 76, '2015-11-19 09:55:47', 1),
(40, 76, '2015-11-19 10:24:13', 1),
(41, 77, '2015-11-19 11:18:29', 1),
(42, 92, '2015-12-17 07:00:24', 1),
(43, 94, '2015-12-19 22:06:05', 1),
(44, 96, '2015-12-29 07:35:59', 1),
(45, 99, '2016-01-06 04:07:19', 1),
(46, 96, '2016-01-08 09:53:52', 1),
(47, 96, '2016-01-09 08:07:52', 1),
(48, 96, '2016-01-09 08:08:38', 1),
(49, 96, '2016-01-09 08:13:43', 1),
(50, 96, '2016-01-09 08:13:43', 1),
(51, 106, '2016-01-21 07:24:55', 1),
(52, 106, '2016-01-22 06:42:28', 1),
(53, 107, '2016-01-27 07:59:32', 1),
(54, 109, '2016-01-29 07:11:26', 1),
(55, 119, '2016-02-21 03:29:24', 1),
(56, 122, '2016-02-23 11:48:39', 1),
(57, 122, '2016-02-23 12:29:45', 1),
(58, 124, '2016-02-26 05:08:21', 1),
(59, 125, '2016-02-26 12:33:57', 1),
(60, 131, '2016-03-09 12:05:48', 1),
(61, 124, '2016-03-11 09:49:45', 1),
(62, 124, '2016-03-11 09:50:42', 1),
(63, 134, '2016-03-15 05:04:54', 1),
(64, 134, '2016-03-15 10:18:01', 1),
(65, 137, '2016-03-15 11:53:12', 1),
(66, 137, '2016-03-15 12:58:45', 1),
(67, 141, '2016-03-16 15:57:43', 1),
(68, 142, '2016-03-17 11:54:40', 1),
(69, 146, '2016-03-17 12:48:03', 1),
(70, 146, '2016-03-17 12:51:18', 1),
(71, 160, '2016-03-21 08:11:22', 1),
(72, 160, '2016-03-21 12:21:38', 1),
(73, 163, '2016-03-21 12:49:15', 1),
(74, 164, '2016-03-23 03:05:04', 1),
(75, 3, '2016-04-20 10:24:36', 1),
(76, 1, '2016-04-21 07:25:49', 1),
(77, 4, '2016-04-27 04:33:34', 1),
(78, 4, '2016-04-27 04:34:36', 1),
(79, 4, '2016-04-27 04:37:47', 1),
(80, 4, '2016-04-27 04:57:53', 1),
(81, 7, '2016-05-05 12:22:36', 1),
(82, 7, '2016-05-05 12:22:37', 1),
(83, 8, '2016-05-20 06:36:22', 1),
(84, 14, '2016-05-27 06:31:05', 1),
(85, 15, '2016-05-28 04:35:28', 1),
(86, 15, '2016-05-28 05:26:00', 1),
(87, 15, '2016-05-28 05:36:54', 1),
(88, 15, '2016-05-28 06:37:27', 1),
(89, 15, '2016-05-28 07:43:24', 1),
(90, 15, '2016-05-28 07:55:02', 1),
(91, 15, '2016-05-28 09:42:15', 1),
(92, 15, '2016-05-28 10:10:43', 1),
(93, 16, '2016-05-28 10:17:49', 1),
(94, 16, '2016-05-28 10:20:12', 1),
(95, 15, '2016-05-28 10:25:38', 1),
(96, 16, '2016-05-28 12:38:58', 1),
(97, 15, '2016-05-28 12:41:21', 1),
(98, 15, '2016-05-28 12:41:25', 1),
(99, 15, '2016-05-28 12:41:32', 1),
(100, 15, '2016-05-28 12:41:33', 1),
(101, 16, '2016-05-28 12:41:53', 1),
(102, 16, '2016-05-28 12:41:55', 1),
(103, 16, '2016-05-28 12:41:56', 1),
(104, 16, '2016-05-28 12:42:18', 1),
(105, 16, '2016-05-28 12:43:16', 1),
(106, 16, '2016-05-28 13:07:44', 1),
(107, 16, '2016-05-30 04:41:05', 1),
(108, 15, '2016-05-30 04:50:46', 1),
(109, 16, '2016-05-30 04:54:36', 1),
(110, 16, '2016-05-30 06:09:35', 1),
(111, 16, '2016-05-30 06:41:30', 1),
(112, 16, '2016-05-30 07:15:56', 1),
(113, 16, '2016-05-30 07:23:25', 1),
(114, 18, '2016-05-30 07:33:09', 1),
(115, 19, '2016-05-31 02:30:57', 1),
(116, 19, '2016-05-31 02:32:37', 1),
(117, 19, '2016-05-31 02:42:13', 1),
(118, 19, '2016-05-31 16:43:23', 1),
(119, 19, '2016-05-31 22:35:24', 1),
(120, 20, '2016-05-31 23:03:09', 1),
(121, 19, '2016-06-01 00:41:29', 1),
(122, 20, '2016-06-01 02:03:31', 1),
(123, 19, '2016-06-02 18:58:47', 1),
(124, 19, '2016-06-03 10:30:02', 1),
(125, 19, '2016-06-03 10:59:57', 1),
(126, 19, '2016-06-03 12:07:19', 1),
(127, 19, '2016-06-03 12:19:57', 1),
(128, 19, '2016-06-03 12:19:59', 1),
(129, 19, '2016-06-03 12:20:00', 1),
(130, 19, '2016-06-03 12:20:02', 1),
(131, 19, '2016-06-03 12:20:05', 1),
(132, 19, '2016-06-03 12:20:05', 1),
(133, 19, '2016-06-03 12:20:06', 1),
(134, 19, '2016-06-02 16:44:03', 1),
(135, 19, '2016-06-04 01:45:46', 1),
(136, 19, '2016-06-03 13:40:06', 1),
(137, 19, '2016-06-03 15:52:48', 1),
(138, 19, '2016-06-03 16:20:24', 1),
(139, 19, '2016-06-03 16:24:11', 1),
(140, 19, '2016-06-04 06:41:09', 1),
(141, 15, '2016-06-04 06:43:20', 1),
(142, 15, '2016-06-04 07:13:27', 1),
(143, 14, '2016-06-04 08:25:04', 1),
(144, 15, '2016-06-06 12:34:12', 1),
(145, 19, '2016-06-06 12:36:48', 1),
(146, 19, '2016-06-06 17:55:41', 1),
(147, 21, '2016-06-07 07:55:19', 1),
(148, 21, '2016-06-07 08:10:58', 1),
(149, 14, '2016-06-07 08:15:28', 1),
(150, 14, '2016-06-07 09:38:39', 1),
(151, 21, '2016-06-07 09:39:57', 1),
(152, 14, '2016-06-07 13:40:32', 1),
(153, 14, '2016-06-07 14:33:56', 1),
(154, 14, '2016-06-08 05:02:02', 1),
(155, 23, '2016-06-08 11:38:39', 1),
(156, 23, '2016-06-08 12:07:08', 1),
(157, 24, '2016-06-09 06:01:43', 1),
(158, 24, '2016-06-09 07:52:18', 1),
(159, 24, '2016-06-09 11:39:10', 1),
(160, 23, '2016-06-10 04:41:24', 1),
(161, 23, '2016-06-10 05:23:15', 1),
(162, 24, '2016-06-10 05:58:56', 1),
(163, 23, '2016-06-10 06:53:43', 1),
(164, 23, '2016-06-10 07:07:01', 1),
(165, 23, '2016-06-10 08:12:56', 1),
(166, 23, '2016-06-10 08:19:09', 1),
(167, 23, '2016-06-10 08:35:14', 1),
(168, 24, '2016-06-10 08:36:41', 1),
(169, 23, '2016-06-10 09:21:46', 1),
(170, 23, '2016-06-10 09:46:05', 1),
(171, 24, '2016-06-10 09:47:20', 1),
(172, 23, '2016-06-10 09:51:29', 1),
(173, 24, '2016-06-10 10:04:33', 1),
(174, 25, '2016-06-10 10:07:42', 1),
(175, 25, '2016-06-10 10:16:33', 1),
(176, 25, '2016-06-10 10:18:53', 1),
(177, 24, '2016-06-10 10:22:12', 1),
(178, 25, '2016-06-10 10:34:17', 1),
(179, 25, '2016-06-10 10:46:44', 1),
(180, 27, '2016-06-10 11:23:42', 1),
(181, 25, '2016-06-10 13:12:12', 1),
(182, 25, '2016-06-10 14:46:00', 1),
(183, 28, '2016-06-10 16:14:44', 1),
(184, 28, '2016-06-11 04:26:29', 1),
(185, 23, '2016-06-11 04:59:56', 1),
(186, 28, '2016-06-11 05:16:21', 1),
(187, 30, '2016-06-11 05:44:53', 1),
(188, 30, '2016-06-11 05:55:08', 1),
(189, 28, '2016-06-11 06:37:04', 1),
(190, 28, '2016-06-11 07:03:38', 1),
(191, 25, '2016-06-11 07:25:51', 1),
(192, 23, '2016-06-11 07:33:36', 1),
(193, 31, '2016-06-11 07:49:01', 1),
(194, 25, '2016-06-11 07:51:09', 1),
(195, 31, '2016-06-11 07:56:32', 1),
(196, 23, '2016-06-15 06:34:20', 1),
(197, 25, '2016-06-15 12:11:13', 1),
(198, 34, '2016-06-16 09:39:44', 1),
(199, 25, '2016-06-17 06:03:58', 1),
(200, 23, '2016-06-18 06:53:31', 1),
(201, 23, '2016-06-18 06:54:27', 1),
(202, 43, '2016-06-21 09:07:04', 1),
(203, 23, '2016-06-21 09:16:31', 1),
(204, 23, '2016-06-21 09:17:34', 1),
(205, 23, '2016-06-21 09:53:21', 1),
(206, 43, '2016-06-21 10:00:28', 1),
(207, 48, '2016-06-21 10:06:34', 1),
(208, 51, '2016-06-21 10:23:29', 1),
(209, 25, '2016-06-21 10:38:51', 1),
(210, 25, '2016-06-22 10:27:25', 1),
(211, 25, '2016-06-22 10:33:57', 1),
(212, 25, '2016-06-22 13:43:25', 1),
(213, 42, '2016-06-22 13:51:40', 1),
(214, 42, '2016-06-22 13:57:59', 1),
(215, 42, '2016-06-22 14:36:06', 1),
(216, 25, '2016-06-22 14:42:19', 1),
(217, 60, '2016-06-22 20:35:47', 1),
(218, 61, '2016-06-23 02:45:02', 1),
(219, 25, '2016-06-23 04:54:24', 1),
(220, 74, '2016-06-23 07:32:56', 1),
(221, 75, '2016-06-23 07:42:05', 1),
(222, 42, '2016-06-23 08:20:57', 1),
(223, 25, '2016-06-23 09:11:39', 1),
(224, 76, '2016-06-25 04:28:38', 1),
(225, 76, '2016-06-25 04:30:06', 1),
(226, 76, '2016-06-25 05:13:12', 1),
(227, 76, '2016-06-25 05:18:35', 1),
(228, 76, '2016-06-28 10:46:31', 1),
(229, 76, '2016-06-28 10:48:42', 1),
(230, 76, '2016-06-28 11:15:11', 1),
(231, 76, '2016-06-28 13:30:44', 1),
(232, 76, '2016-06-28 13:37:29', 1),
(233, 76, '2016-07-01 05:24:42', 1),
(234, 76, '2016-07-01 05:29:23', 1),
(235, 76, '2016-07-01 05:29:23', 1),
(236, 25, '2016-07-01 05:31:15', 1),
(237, 76, '2016-07-01 05:39:01', 1),
(238, 76, '2016-07-01 06:24:45', 1),
(239, 79, '2016-07-02 04:29:23', 1),
(240, 76, '2016-07-02 05:37:59', 1),
(241, 76, '2016-07-02 11:16:28', 1),
(242, 42, '2016-07-04 06:52:20', 1),
(243, 81, '2016-07-09 13:58:50', 1),
(244, 42, '2016-07-12 13:02:07', 1),
(245, 42, '2016-07-12 13:02:08', 1),
(246, 42, '2016-07-12 13:02:09', 1),
(247, 42, '2016-07-12 13:02:09', 1),
(248, 42, '2016-07-12 13:02:09', 1),
(249, 42, '2016-07-12 13:02:09', 1),
(250, 82, '2016-07-12 13:10:55', 1),
(251, 25, '2016-07-15 05:42:18', 1),
(252, 25, '2016-07-15 07:45:11', 1),
(253, 28, '2016-07-26 05:46:48', 1),
(254, 25, '2016-07-29 08:04:38', 1),
(255, 25, '2016-07-29 08:05:36', 1),
(256, 25, '2016-08-01 07:46:00', 1),
(257, 25, '2016-08-01 08:11:49', 1),
(258, 28, '2016-08-11 10:53:02', 1),
(259, 95, '2016-08-17 06:43:30', 1),
(260, 96, '2016-08-17 09:24:35', 1),
(261, 96, '2016-08-17 09:41:05', 1),
(262, 96, '2016-08-19 13:51:57', 1),
(263, 99, '2016-08-23 09:45:48', 1),
(264, 99, '2016-08-23 09:53:01', 1),
(265, 100, '2016-08-25 06:32:10', 1),
(266, 100, '2016-08-25 12:02:07', 1),
(267, 84, '2016-08-25 12:50:16', 1),
(268, 84, '2016-08-29 10:52:40', 1),
(269, 104, '2016-08-30 15:10:02', 1),
(270, 104, '2016-08-30 16:07:39', 1),
(271, 25, '2016-09-07 13:10:42', 1),
(272, 106, '2016-09-12 18:10:52', 1),
(273, 108, '2016-09-25 06:31:20', 1),
(274, 109, '2016-09-26 04:19:29', 1),
(275, 109, '2016-09-26 09:49:27', 1),
(276, 110, '2016-09-26 21:28:45', 1),
(277, 110, '2016-09-28 00:02:11', 1),
(278, 42, '2016-09-30 07:49:16', 1),
(279, 42, '2016-09-30 07:51:33', 1),
(280, 111, '2016-10-03 03:10:35', 1),
(281, 113, '2016-10-16 02:51:19', 1),
(282, 114, '2016-10-16 04:06:48', 1),
(283, 115, '2016-10-17 16:17:54', 1),
(284, 84, '2016-10-19 13:14:54', 1),
(285, 84, '2016-11-01 01:48:56', 1),
(286, 84, '2016-11-01 04:50:44', 1),
(287, 84, '2016-11-01 04:52:15', 1),
(288, 84, '2016-11-02 02:17:03', 1),
(289, 84, '2016-11-02 04:36:09', 1),
(290, 84, '2016-11-02 06:10:05', 1),
(291, 84, '2016-11-02 06:10:23', 1),
(292, 84, '2016-11-03 04:48:18', 1),
(293, 84, '2016-11-09 22:22:39', 1),
(294, 84, '2016-11-09 22:53:51', 1),
(295, 84, '2016-11-09 22:55:10', 1),
(296, 84, '2016-11-10 02:10:29', 1),
(297, 84, '2016-11-10 02:24:41', 1),
(298, 84, '2016-11-10 02:34:11', 1),
(299, 84, '2016-11-10 02:34:38', 1),
(300, 84, '2016-11-10 02:35:23', 1),
(301, 124, '2016-11-15 06:33:26', 1),
(302, 124, '2016-11-15 13:23:13', 1),
(303, 124, '2016-11-15 13:24:01', 1),
(304, 125, '2016-11-15 13:25:01', 1),
(305, 125, '2016-11-15 13:26:01', 1),
(306, 125, '2016-11-15 13:47:44', 1),
(307, 125, '2016-11-15 13:56:47', 1),
(308, 125, '2016-11-15 13:57:21', 1),
(309, 125, '2016-11-15 14:01:59', 1),
(310, 84, '2016-11-15 14:07:20', 1),
(311, 84, '2016-11-16 04:25:46', 1),
(312, 84, '2016-11-16 04:37:59', 1),
(313, 112, '2016-12-02 08:11:23', 1),
(314, 112, '2016-12-02 08:11:23', 1),
(315, 84, '2016-12-02 14:36:57', 1),
(316, 139, '2016-12-03 07:31:12', 1),
(317, 84, '2016-12-03 08:16:45', 1),
(318, 139, '2016-12-19 10:55:38', 1),
(319, 139, '2016-12-19 11:00:04', 1),
(320, 139, '2016-12-21 20:07:28', 1),
(321, 139, '2016-12-21 20:25:09', 1),
(322, 139, '2016-12-21 20:39:25', 1),
(323, 84, '2016-12-22 04:45:39', 1),
(324, 139, '2016-12-22 04:58:35', 1),
(325, 84, '2016-12-22 05:16:47', 1),
(326, 84, '2016-12-22 05:38:07', 1),
(327, 140, '2016-12-22 05:43:48', 1),
(328, 140, '2016-12-22 05:44:51', 1),
(329, 140, '2016-12-22 06:04:13', 1),
(330, 139, '2016-12-22 22:51:39', 1),
(331, 139, '2016-12-23 18:44:21', 1),
(332, 139, '2016-12-30 02:43:14', 1),
(333, 141, '2016-12-30 06:59:14', 1),
(334, 141, '2016-12-30 10:12:19', 1),
(335, 139, '2017-01-03 05:44:42', 1),
(336, 139, '2017-01-03 06:59:12', 1),
(337, 141, '2017-01-04 12:02:35', 1),
(338, 139, '2017-01-05 07:30:41', 1),
(339, 139, '2017-01-05 07:57:47', 1),
(340, 139, '2017-01-05 08:17:17', 1),
(341, 139, '2017-01-05 08:20:33', 1),
(342, 141, '2017-01-05 11:31:48', 1),
(343, 139, '2017-01-06 07:08:05', 1),
(344, 139, '2017-01-06 07:37:26', 1),
(345, 141, '2017-01-06 13:01:35', 1),
(346, 141, '2017-01-10 09:55:39', 1),
(347, 142, '2017-01-12 20:50:54', 1),
(348, 139, '2017-01-25 04:38:58', 1),
(349, 139, '2017-01-25 08:22:41', 1),
(350, 139, '2017-01-27 06:52:28', 1),
(351, 144, '2017-02-05 10:58:48', 1),
(352, 139, '2017-02-07 06:44:08', 1),
(353, 139, '2017-02-08 02:47:48', 1),
(354, 139, '2017-02-08 03:50:25', 1),
(355, 139, '2017-02-08 06:50:13', 1),
(356, 139, '2017-02-08 07:50:57', 1),
(357, 139, '2017-02-08 09:56:17', 1),
(358, 139, '2017-02-08 09:56:17', 1),
(359, 139, '2017-02-08 09:56:17', 1),
(360, 139, '2017-02-08 09:56:17', 1),
(361, 139, '2017-02-08 09:56:17', 1),
(362, 139, '2017-02-08 09:56:18', 1),
(363, 139, '2017-02-08 09:56:18', 1),
(364, 139, '2017-02-08 09:56:18', 1),
(365, 139, '2017-02-08 09:56:18', 1),
(366, 139, '2017-02-08 09:56:19', 1),
(367, 139, '2017-02-08 09:56:19', 1),
(368, 145, '2017-02-08 11:05:37', 1),
(369, 139, '2017-02-09 06:55:47', 1),
(370, 139, '2017-02-10 03:17:54', 1),
(371, 139, '2017-02-10 03:19:10', 1),
(372, 139, '2017-02-10 04:22:43', 1),
(373, 139, '2017-02-14 06:19:22', 1),
(374, 139, '2017-02-16 09:42:36', 1),
(375, 139, '2017-02-16 16:02:36', 1),
(376, 139, '2017-02-16 23:26:43', 1),
(377, 139, '2017-02-17 08:05:45', 1),
(378, 139, '2017-02-20 09:35:05', 1),
(379, 139, '2017-02-20 23:38:00', 1),
(380, 139, '2017-02-21 04:53:09', 1),
(381, 139, '2017-02-22 03:36:25', 1),
(382, 139, '2017-02-22 09:25:04', 1),
(383, 139, '2017-02-22 12:13:49', 1),
(384, 139, '2017-02-23 09:19:56', 1),
(385, 139, '2017-02-23 11:22:14', 1),
(386, 146, '2017-02-24 08:32:29', 1),
(387, 139, '2017-02-24 10:10:50', 1),
(388, 150, '2017-02-28 05:18:25', 1),
(389, 150, '2017-03-01 03:39:05', 1),
(390, 150, '2017-03-02 07:53:05', 1),
(391, 150, '2017-03-02 09:26:42', 1),
(392, 150, '2017-03-03 03:45:19', 1),
(393, 150, '2017-03-03 04:29:49', 1),
(394, 150, '2017-03-03 07:29:47', 1),
(395, 150, '2017-03-03 07:53:07', 1),
(396, 150, '2017-03-03 08:00:01', 1),
(397, 150, '2017-03-03 10:14:04', 1),
(398, 150, '2017-03-03 10:14:04', 1),
(399, 150, '2017-03-06 02:27:09', 1),
(400, 150, '2017-03-06 08:10:36', 1),
(401, 150, '2017-03-07 03:04:40', 1),
(402, 150, '2017-03-07 04:05:18', 1),
(403, 150, '2017-03-07 04:15:43', 1),
(404, 150, '2017-03-07 11:27:52', 1),
(405, 150, '2017-03-08 02:54:47', 1),
(406, 150, '2017-03-08 05:40:27', 1),
(407, 150, '2017-03-09 06:10:01', 1),
(408, 150, '2017-03-09 06:29:30', 1),
(409, 150, '2017-03-10 02:24:35', 1),
(410, 150, '2017-03-10 07:56:37', 1),
(411, 150, '2017-03-10 08:14:28', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_maincategory`
--

CREATE TABLE `nm_maincategory` (
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_name` varchar(100) NOT NULL,
  `mc_type` varchar(10) NOT NULL,
  `mc_img` varchar(150) NOT NULL,
  `mc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_maincategory`
--

INSERT INTO `nm_maincategory` (`mc_id`, `mc_name`, `mc_type`, `mc_img`, `mc_status`) VALUES
(2, 'MEN', '1,1,1', 'men-categorymGFooaEh.jpeg', 1),
(3, 'WOMEN', '1,1,1', 'SAREESWEt8bBAo.jpeg', 1),
(4, 'BABY & KIDS', '1,1,1', 'BABYcBdJmJln.jpg', 1),
(5, 'HOME & KITCHEN', '1,1,1', 'dead-shopping-cartfVTnMIGJ.jpg', 1),
(6, 'ELECTRONICS', '1,1,1', 'shopping cart 2ltqacj52.jpg', 1),
(7, 'SPORTS', '1,1,1', 'profilexyUXsTa9.png', 1),
(8, 'AUTO MOBILES', '1,1,1', 'sedan-512xMcVlTeV.png', 1),
(9, 'BOOKS', '1,1,1', 'books-icon-512q6gJrb9I.png', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_merchant`
--

CREATE TABLE `nm_merchant` (
  `mer_id` int(10) UNSIGNED NOT NULL,
  `addedby` varchar(20) NOT NULL,
  `mer_fname` varchar(150) NOT NULL,
  `mer_lname` varchar(150) NOT NULL,
  `mer_password` varchar(150) NOT NULL,
  `mer_email` varchar(150) NOT NULL,
  `mer_phone` varchar(20) NOT NULL,
  `mer_address1` varchar(150) NOT NULL,
  `mer_address2` varchar(150) NOT NULL,
  `mer_ci_id` int(10) UNSIGNED NOT NULL COMMENT 'city id',
  `mer_co_id` smallint(5) UNSIGNED NOT NULL COMMENT 'country id',
  `mer_payment` varchar(100) NOT NULL,
  `mer_commission` tinyint(4) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `mer_staus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-unblock,0-block'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_merchant`
--

INSERT INTO `nm_merchant` (`mer_id`, `addedby`, `mer_fname`, `mer_lname`, `mer_password`, `mer_email`, `mer_phone`, `mer_address1`, `mer_address2`, `mer_ci_id`, `mer_co_id`, `mer_payment`, `mer_commission`, `created_date`, `mer_staus`) VALUES
(9, '0', 'Ganesh', 'kumar', '1234567890', 'charlesvictor.j@pofitec.com', '221657', 'City Main Road', 'Bangaluru', 4, 1, '1234567890', 10, '06/08/2016', 1),
(11, '0', 'mukunth', 'rajan', 'mLmWyiGj', 'rajan@gmail.com', '1234567890', 'j.k road', 'Delhi', 3, 1, '456789123', 10, '06/08/2016', 1),
(12, '0', 'Ravi', 'Kumar', '123456', 'pofidevelopment@gmail.com', '8883152529', 'Ramnagar', 'Gandhipuram', 1, 1, '15600', 2, '06/08/2016', 1),
(13, '12', 'Rajesh', 'Kumar', '0HSaEFbD', 'pofidevelopment1@gmail.com', '7894561230', 'Ganghipuram', 'Ganapathy', 1, 1, '45000', 2, '06/08/2016', 1),
(15, '', 'kumar', 'merchant', '123456', 'kumarkailash075@gmail.com', '5545454545', '75', 'anna street', 0, 1, '1212121212', 2, '', 1),
(16, '', 'charles', 'victor', '4Zdai1Px', 'charlesvictor.j@pofitec.com', '9952440467', 'ramnagar', 'gandipuram', 1, 1, 'charlesvictor.j@pofitec.com', 16, '', 1),
(17, '', 'nljdsnvj', 'jkxnvjsn', 'gGZM5gnB', 'erkprajesh@gmail.com', '1231231231', '12121', 'KJFNSF', 1, 1, 'erkprajesh@gmail.com', 10, '', 1),
(18, '0', 'Rajiv', 'Tandon', '8hUKOtpH', 'rajivlinkites@gmail.com', '8871665475', 'Indiore', 'IOndfeor', 4, 1, '', 10, '08/01/2016', 1),
(19, '', 'Kucau', 'Combro', 'K9wa9R13', 'persero.dana@gmail.com', '082312323123', 'cemput', 'barat', 3, 1, 'persero.dana@gmail.com', 5, '', 1),
(20, '0', 'Ashish', 'Zanwar', 'P1wz8RAR', 'ashish.tantram@gmail.com', '7738254158', 'ghfghfgh', 'fgfgfd', 5, 1, '', 10, '09/18/2016', 1),
(21, '15', 'testmerchantsli', 'testmerch', '8lmsNamc', 'testmerch@test.com', '9999999999', 'first address line', 'second address line', 4, 1, 'testpayment', 50, '10/05/2016', 1),
(22, '', 'knjn', 'njkn', 'xBSuOjCX', 'jkn@ddd.ddd', '878678687687', 'ddd', 'jknjknkn', 1, 1, 'knkjkjnjknkj', 10, '', 1),
(23, '', 'kjnjknj', 'kjnnknjk', 'BrglkSHh', 'j.gerbes@me.com', '878798789798', 'bjbjhhbjh', 'bhjhbjbhj', 2, 1, '', 10, '', 1),
(24, '0', 'sundhar', 'kumar', 'WpwQWyNb', 'sundhar@gmail.com', '1234567890', 'xcxcxccccxcxcx', 'cvvcvcvcvcvcvcvc', 1, 1, '45545545545455454545', 2, '11/01/2016', 1),
(25, '0', 'test', 'test', '9cNCLYOW', 'support@linteractif.com', '+9613583114', 'Beirut', 'Beirut', 2, 1, 'dfdf', 1, '11/11/2016', 0),
(26, '0', 'test', 'test', 'XUWUNPak', 'info@test.com', '3583114', 'Mar Takla - Street Elias Sarkis', 'Bldg. Georges Maalouf 2nd Floor', 1, 1, '', 5, '11/12/2016', 0),
(27, '0', 'test', 'k', 'djAOqi1O', 'test21689@gmail.com', '9876543210', 'test address1', 'test address2', 1, 1, 'vinodbabu@pofitec.com', 1, '11/12/2016', 1),
(28, '', 'bala', 'fgfgfgfg', 'gCqScTcZ', 'kailashkumar.r@esec.ac.in', '1234567890', 'fgfgfgfdg', 'ghgfhgfhgfh', 1, 1, 'kailashkumar.r@esec.ac.in', 5, '', 1),
(29, '', 'kailash', 'kumar', 'nBe0mm3F', 'kailashkumar.r@pofitec.com', '1234567890', 'vvxxcvxcvcvxcv', 'vxcxcvxcv', 1, 1, 'kailashkumar.r@pofitec.com', 5, '', 1),
(30, '0', 'test', 'test', 'y3mQYxab', 'test@test.com', '3583114', 'Mar Takla - Street Elias Sarkis', 'Bldg. Georges Maalouf 2nd Floor', 1, 1, 'georgen@linteractif.com', 5, '11/15/2016', 1),
(31, '', 'cvbcvbcvb', 'cvbcvb', '123456', 'hello@gmail.com', '1234567890', 'gdfgfgfdg', 'bvnbvnb', 1, 1, 'hello@gmail.com', 5, '', 1),
(32, '0', 'bala', 'krishna', 'SKKh0eWW', 'bala@gmail.com', '1234567890', 'xccxcxcxcxc', 'xcxcxcxcx', 1, 1, '12345678900000', 5, '12/30/2016', 1),
(33, '', 'Untung', 'Saptoto', '123456', 'untung27@gmail.com', '087888237701', 'a11', 'a22', 7, 8, 'untung27@gmail.com', 10, '', 1),
(34, '', 'kailashkumar', 'vccvc', 'iY8ifhGE', 'nnnn@gmail.com', '9092398789', 'ccccv', 'vbcvbvbc', 1, 1, 'nnnn@gmail.com', 5, '', 1),
(35, '', 'sssss', 'rrrr', '92ro7tnL', 'cvcvcvcv@gmail.com', '1234567890', 'cvcvxcvxcv', 'xcvcxvxcv', 1, 1, 'cvcvcvcv@gmail.com', 5, '', 1),
(36, '', 'Febrian', 'Ramadhan', '1ziPOXJB', 'febrian.wr@gmail.com', '08980452143', 'Bogor', 'Bogor', 7, 8, 'febrian.wr@gmail.com', 50, '', 1),
(37, '0', 'Johan', 'Ignatiu', 'aHCGAhBZ', 'harry.okta.maulana2@gmail.com', '0857173255109', 'Jl. Bekasi Barat 3', 'Jl. Bekasi Barat 4', 7, 8, 'BitCoin2', 12, '02/28/2017', 1),
(38, '0', 'Tirta', 'Fajar', '2dQ9lrXW', 'harry.okta.maulana@gmail.com', '6785678', 'JL. Pajak Raya', 'JL. Budi Jaya', 7, 8, 'BitCoin', 10, '02/28/2017', 1),
(39, '0', 'Fatchan', 'Abbas', '9TDEgQd5', 'fathan.abbas24@gmail.com', '123456789', 'JL. Juanda', 'JL. Onta', 7, 8, 'BitCoin', 9, '02/28/2017', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_modulesettings`
--

CREATE TABLE `nm_modulesettings` (
  `ms_id` int(11) NOT NULL,
  `ms_dealmodule` int(11) NOT NULL,
  `ms_productmodule` int(11) NOT NULL,
  `ms_auctionmodule` int(11) NOT NULL,
  `ms_blogmodule` int(11) NOT NULL,
  `ms_nearmemapmodule` int(11) NOT NULL,
  `ms_storelistmodule` int(11) NOT NULL,
  `ms_pastdealmodule` int(11) NOT NULL,
  `ms_faqmodule` int(11) NOT NULL,
  `ms_cod` int(11) NOT NULL,
  `ms_paypal` int(11) NOT NULL,
  `ms_creditcard` int(11) NOT NULL,
  `ms_googlecheckout` int(11) NOT NULL,
  `ms_shipping` int(11) NOT NULL COMMENT '1=>Free shipping, 2=> Flat shipping, 3=> Product per shippin, 4=> Per Item shipping',
  `ms_newsletter_template` int(11) NOT NULL COMMENT '1=> Temp 1, 2=>Temp 2, 3=> Temp 3, 4=> Temp 4',
  `ms_citysettings` int(11) NOT NULL COMMENT '1=> With city, 0=> Without city'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_newsletter_subscribers`
--

CREATE TABLE `nm_newsletter_subscribers` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `post_dt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `nm_newsletter_subscribers`
--

INSERT INTO `nm_newsletter_subscribers` (`id`, `email`, `city_id`, `status`, `post_dt`) VALUES
(5, 'sg@gmail.com', 0, 1, '0000-00-00 00:00:00'),
(8, 'chalesvictor.info@gmail.com', 0, 1, '0000-00-00 00:00:00'),
(7, 'erkprajesh@gmail.com', 0, 1, '0000-00-00 00:00:00'),
(9, 'charlesvictor.j@pofitec.com', 0, 1, '0000-00-00 00:00:00'),
(10, 'pavithrandbpro@gmail.com', 0, 1, '0000-00-00 00:00:00'),
(11, 'kumar@pofitec.com', 0, 1, '0000-00-00 00:00:00'),
(12, 'kailash.k@pofitec.com', 0, 1, '0000-00-00 00:00:00'),
(13, 'kumarkailash075@gmail.com', 0, 1, '0000-00-00 00:00:00'),
(14, 'kailashkumar.r@pofitec.com', 0, 1, '0000-00-00 00:00:00'),
(18, 'mogahead@gmail.com', 0, 1, '0000-00-00 00:00:00'),
(19, 'kailashkumar.r@esec.ac.in', 0, 1, '0000-00-00 00:00:00'),
(20, 'tester1.kuku@gmail.com', 0, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_order`
--

CREATE TABLE `nm_order` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_cus_id` int(10) UNSIGNED NOT NULL,
  `order_pro_id` int(11) UNSIGNED NOT NULL,
  `order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `transaction_id` varchar(50) NOT NULL,
  `payer_email` varchar(50) NOT NULL,
  `payer_id` varchar(50) NOT NULL,
  `payer_name` varchar(100) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_amt` decimal(10,2) NOT NULL,
  `order_tax` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `token_id` varchar(30) NOT NULL,
  `payment_ack` varchar(10) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` varchar(20) NOT NULL,
  `payer_status` varchar(50) NOT NULL,
  `order_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `order_paytype` smallint(6) NOT NULL COMMENT '1-paypal, 2-doku',
  `order_pro_color` int(11) NOT NULL,
  `order_pro_size` int(11) NOT NULL,
  `order_shipping_add` text NOT NULL,
  `order_merchant_id` int(11) NOT NULL,
  `ip_address` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `process_type` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `process_datetime` datetime DEFAULT NULL,
  `doku_payment_datetime` datetime DEFAULT NULL,
  `transidmerchant` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `notify_type` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `response_code` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status_code` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `result_msg` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reversal` int(1) NOT NULL DEFAULT '0',
  `approval_code` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_channel` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_issuer` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `creditcard` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `words` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `session_id` varchar(48) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `verify_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `verify_score` int(3) NOT NULL DEFAULT '0',
  `verify_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `check_status` int(1) NOT NULL DEFAULT '0',
  `count_check_status` int(1) NOT NULL DEFAULT '0',
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `id` int(11) DEFAULT NULL,
  `postalservice_code` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_order`
--

INSERT INTO `nm_order` (`order_id`, `order_cus_id`, `order_pro_id`, `order_type`, `transaction_id`, `payer_email`, `payer_id`, `payer_name`, `order_qty`, `order_amt`, `order_tax`, `currency_code`, `token_id`, `payment_ack`, `order_date`, `created_date`, `payer_status`, `order_status`, `order_paytype`, `order_pro_color`, `order_pro_size`, `order_shipping_add`, `order_merchant_id`, `ip_address`, `process_type`, `process_datetime`, `doku_payment_datetime`, `transidmerchant`, `notify_type`, `response_code`, `status_code`, `result_msg`, `reversal`, `approval_code`, `payment_channel`, `payment_code`, `bank_issuer`, `creditcard`, `words`, `session_id`, `verify_id`, `verify_score`, `verify_status`, `check_status`, `count_check_status`, `message`, `id`, `postalservice_code`) VALUES
(10, 30, 22, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-06-11 11:16:25', '', '', 3, 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(11, 30, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-06-11 11:16:25', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(12, 30, 45, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:16:25', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(13, 30, 44, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:16:25', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(14, 30, 35, 2, '', '', '', '', 1, '59.00', '0.00', '', '', '', '2016-06-11 11:16:25', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(15, 30, 22, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-06-11 11:18:08', '', '', 3, 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(16, 30, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-06-11 11:18:08', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(17, 30, 45, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:18:08', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(18, 30, 44, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:18:08', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(19, 30, 35, 2, '', '', '', '', 1, '59.00', '0.00', '', '', '', '2016-06-11 11:18:08', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(20, 30, 22, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-06-11 11:19:47', '', '', 3, 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(21, 30, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-06-11 11:19:47', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(22, 30, 9, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-06-11 11:19:47', '', '', 3, 0, 13, 13, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(23, 30, 45, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:19:47', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(24, 30, 44, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:19:47', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(25, 30, 35, 2, '', '', '', '', 1, '59.00', '0.00', '', '', '', '2016-06-11 11:19:47', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(26, 30, 22, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(27, 30, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(28, 30, 9, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 13, 13, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(29, 30, 45, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(30, 30, 44, 2, '', '', '', '', 1, '49.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(31, 30, 35, 2, '', '', '', '', 1, '59.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(32, 30, 13, 2, '', '', '', '', 1, '699.00', '0.00', '', '', '', '2016-06-11 11:25:22', '', '', 3, 0, 0, 0, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(33, 25, 9, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-06-15 05:00:00', '', '', 3, 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(34, 25, 9, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-06-15 05:00:00', '', '', 3, 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(35, 25, 9, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-06-15 05:00:00', '', '', 3, 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(36, 25, 20, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-06-17 05:00:00', '', '', 3, 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(37, 43, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-06-21 07:40:48', '', '', 3, 0, 2, 5, 'kumar,144, Ram nagar,sengupatha,TamilNadu,641009,1231231231', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(38, 23, 17, 1, '', '', '', '', 1, '479.00', '0.00', '', '', '', '2016-06-21 08:18:20', '', '', 3, 0, 16, 1, 'kailash,jansinagar,v.chatram,tn,638004,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(39, 23, 21, 1, '', '', '', '', 1, '50.00', '0.00', '', '', '', '2016-06-21 08:18:20', '', '', 3, 0, 8, 4, 'kailash,jansinagar,v.chatram,tn,638004,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(40, 48, 21, 1, '', '', '', '', 1, '50.00', '0.00', '', '', '', '2016-06-21 08:57:46', '', '', 3, 0, 8, 1, 'kailashkumar,anna street,chatram,tn,123456,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(41, 25, 20, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-06-21 12:01:34', '', '', 3, 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(42, 76, 13, 1, '', '', '', '', 1, '699.00', '0.00', '', '', '', '2016-07-01 15:59:51', '', '', 3, 0, 0, 0, 'Pavithran,144, Sengupta Street | Near Hotel City Towers,Ram Nagar | Coimbatore - 641009,Tamil Nadu,641009,9787467575', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(43, 79, 14, 1, '', '', '', '', 1, '8499.00', '0.00', '', '', '', '2016-07-02 15:01:20', '', '', 3, 0, 19, 0, 'reca,543654365,jytfjuyj,jhgfjhgfj,4324321,654365436', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(44, 25, 11, 1, '', '', '', '', 1, '1573.00', '0.00', '', '', '', '2016-07-29 06:36:10', '', '', 3, 0, 12, 7, ',,,,,', 9, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(45, 96, 13, 1, '', '', '', '', 1, '699.00', '0.00', '', '', '', '2016-08-17 07:57:17', '', '', 3, 0, 12, 4, ',,,,,', 9, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(46, 100, 37, 1, '', '', '', '', 31, '21669.00', '0.00', '', '', '', '2016-08-25 17:02:44', '', '', 3, 0, 1, 1, ',,,,,', 9, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(47, 84, 14, 1, '', '', '', '', 1, '8499.00', '0.00', '', '', '', '2016-08-25 11:21:18', '', '', 3, 0, 19, 10, ',,,,,', 9, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(48, 103, 13, 1, '', '', '', '', 1, '699.00', '0.00', '', '', '', '2016-08-30 11:42:41', '', '', 3, 0, 12, 6, ',,,,,', 9, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(49, 104, 38, 1, '', '', '', '', 1, '400.00', '0.00', '', '', '', '2016-08-30 14:56:42', '', '', 3, 0, 3, 1, ',,,,,', 19, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(50, 110, 25, 1, '', '', '', '', 5, '100.00', '0.00', '', '', '', '2016-09-27 08:00:31', '', '', 3, 0, 4, 1, ',,,,,', 9, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(51, 84, 35, 1, '', '', '', '', 1, '299.00', '0.00', '', '', '', '2016-11-02 11:55:22', '', '', 3, 0, 13, 3, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(52, 84, 35, 1, '', '', '', '', 1, '299.00', '0.00', '', '', '', '2016-11-02 11:55:44', '', '', 3, 0, 13, 3, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(53, 84, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 12:50:44', '', '', 3, 0, 4, 6, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(54, 84, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 12:52:06', '', '', 3, 0, 4, 6, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(55, 84, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 13:06:05', '', '', 3, 0, 4, 6, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(56, 84, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-11-02 13:11:45', '', '', 3, 0, 6, 3, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(57, 84, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-11-02 13:15:41', '', '', 3, 0, 6, 3, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(58, 84, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 13:17:07', '', '', 3, 0, 4, 6, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(59, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:23:50', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(60, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:24:12', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(61, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:32:47', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(62, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:33:20', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(63, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:33:36', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(64, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:34:48', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(65, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:35:49', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(66, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:48:50', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(67, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:49:10', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(68, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:52:47', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(69, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:53:43', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(70, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:54:27', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(71, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:54:59', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(72, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:56:58', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(73, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 13:58:36', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(74, 84, 24, 1, '', '', '', '', 1, '30.00', '0.00', '', '', '', '2016-11-02 14:02:28', '', '', 3, 0, 4, 2, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(75, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 14:11:02', '', '', 3, 0, 1, 4, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(76, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:30:38', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(77, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:32:16', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(78, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:34:14', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(79, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:44:09', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(80, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:48:03', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(81, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:52:27', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(82, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:52:45', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(83, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:53:24', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(84, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:57:13', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(85, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 14:58:30', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(86, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:01:26', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(87, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:02:44', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(88, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:16:02', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(89, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:17:04', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(90, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:20:46', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(91, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:21:45', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(92, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:30:16', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(93, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:31:17', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(94, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:36:10', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(95, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:36:31', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(96, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:36:35', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(97, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:36:53', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(98, 84, 30, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-02 15:37:42', '', '', 3, 0, 5, 5, ',,,,,', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(99, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 16:54:31', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(100, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 16:56:59', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(101, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 16:57:26', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(102, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 16:58:00', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(103, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 16:59:14', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(104, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:13:53', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(105, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:14:15', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(106, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:14:30', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(107, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:15:20', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(108, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:15:36', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(109, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:16:49', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(110, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:17:07', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(111, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:17:24', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(112, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:33:55', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(113, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:34:11', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(114, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:34:24', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(115, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:34:38', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(116, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:35:06', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(117, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:36:07', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(118, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:36:53', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(119, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:37:18', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(120, 84, 25, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-11-02 17:37:42', '', '', 3, 0, 4, 4, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(121, 125, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-11-15 02:03:45', '', '', 3, 0, 4, 6, 'George Nammour,test,test,test,test,test,georgen@linteractif.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(122, 139, 18, 1, '', '', '', '', 1, '9500.00', '0.00', '', '', '', '2016-12-03 00:35:45', '', '', 3, 0, 0, 0, 'tester1.kuku,address1,address2,state*,1234567890123456789,0878882377,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(123, 84, 26, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-12-03 01:17:21', '', '', 3, 0, 2, 7, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(124, 139, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-12-21 01:26:10', '', '', 3, 0, 4, 6, 'u,u,u,u,u,u,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(125, 139, 29, 1, '', '', '', '', 1, '10.00', '0.00', '', '', '', '2016-12-21 01:35:46', '', '', 3, 0, 4, 6, 'test,tes,tes,tes,tes,tes,tester1.kuku-buyer@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(126, 139, 17, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-12-21 01:40:21', '', '', 3, 0, 16, 0, '1,1,1,1,1,1,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(127, 84, 18, 1, '', '', '', '', 1, '9500.00', '0.00', '', '', '', '2016-12-21 21:46:05', '', '', 3, 0, 0, 0, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(128, 139, 28, 1, '', '', '', '', 1, '15.00', '0.00', '', '', '', '2016-12-21 22:00:05', '', '', 3, 0, 6, 3, '1,1,1,1,1,1,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(129, 84, 17, 1, '', '', '', '', 1, '599.00', '0.00', '', '', '', '2016-12-21 22:05:41', '', '', 3, 0, 0, 0, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(130, 84, 26, 1, '', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-12-21 22:18:12', '', '', 3, 0, 2, 7, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(131, 84, 29, 1, '20634141J62698159', 'vinodbabu.89-buyer@gmail.com', 'SN6X6BJGBPQYU', 'test', 1, '10.00', '0.00', 'USD', 'EC-2FF58650VR518922Y', 'Success', '2016-12-21 22:20:06', '', 'verified', 1, 1, 4, 6, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(132, 84, 28, 1, '89C4944661162853U', 'vinodbabu.89-buyer@gmail.com', 'SN6X6BJGBPQYU', 'test', 1, '15.00', '0.00', 'USD', 'EC-21C54843R2488712R', 'Success', '2016-12-21 22:29:31', '', 'verified', 1, 1, 6, 3, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(133, 84, 23, 1, '', '', '', '', 1, '60.00', '0.00', '', '', '', '2016-12-21 22:33:36', '', '', 3, 0, 0, 0, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(134, 84, 23, 1, '02069709RU0064111', 'vinodbabu.89-buyer@gmail.com', 'SN6X6BJGBPQYU', 'test', 1, '60.00', '0.00', 'USD', 'EC-5CT61077DK133241N', 'Success', '2016-12-21 22:38:31', '', 'verified', 1, 1, 0, 0, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(135, 139, 29, 1, '4JH527590T0105546', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '10.00', '0.00', 'USD', 'EC-2GJ921677W440622N', 'Success', '2016-12-22 03:52:24', '', 'verified', 1, 1, 4, 6, '1,1,1,1,1,1,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(136, 139, 27, 1, '00156403MD991244J', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '30.00', '0.00', 'USD', 'EC-63Y58799L9397822S', 'Success', '2016-12-22 23:45:03', '', 'verified', 1, 1, 18, 5, '1,1,1,1,1,1,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(137, 139, 27, 1, '8C5675103D803401U', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 4, '120.00', '0.00', 'USD', 'EC-3F747711ED6352147', 'Success', '2016-12-29 19:45:00', '', 'verified', 1, 1, 18, 5, '2,2,2,2,12300,0878882377,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(138, 139, 26, 1, '33B29665J5272570H', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '20.00', '0.00', 'USD', 'EC-7VY06011NY871451X', 'Success', '2016-12-29 21:35:19', '', 'verified', 1, 1, 2, 3, 'tester1.kuku,3,3,3,12300,0878882377,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(139, 141, 23, 1, '2EA05549YH758692D', 'kumarkailash075-buyer@gmail.com', 'SPMQ4UD2Y55WW', 'test', 1, '60.00', '0.00', 'USD', 'EC-5DV21171Y10016304', 'Success', '2016-12-30 03:25:56', '', 'verified', 1, 1, 0, 0, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(140, 141, 26, 1, '2EA05549YH758692D', 'kumarkailash075-buyer@gmail.com', 'SPMQ4UD2Y55WW', 'test', 1, '20.00', '0.00', 'USD', 'EC-5DV21171Y10016304', 'Success', '2016-12-30 03:25:56', '', 'verified', 1, 1, 0, 0, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(141, 141, 34, 1, 'unpay1811383516', '', '', '', 1, '50.00', '0.00', '', '', '', '2016-12-30 03:47:02', '', '', 3, 0, 9, 3, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(142, 141, 31, 1, 'unpay1685597361', '', '', '', 1, '20.00', '0.00', '', '', '', '2016-12-30 03:47:02', '', '', 3, 0, 0, 0, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(143, 141, 34, 1, '1EE03115HJ435732P', 'kumarkailash075-buyer@gmail.com', 'SPMQ4UD2Y55WW', 'test', 1, '50.00', '0.00', 'USD', 'EC-3D8894418B3155441', 'Success', '2016-12-30 03:50:13', '', 'verified', 1, 1, 9, 3, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(144, 141, 31, 1, '1EE03115HJ435732P', 'kumarkailash075-buyer@gmail.com', 'SPMQ4UD2Y55WW', 'test', 1, '20.00', '0.00', 'USD', 'EC-3D8894418B3155441', 'Success', '2016-12-30 03:50:13', '', 'verified', 1, 1, 0, 0, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(145, 139, 29, 1, '8XG78165PB9263838', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 2, '20.00', '0.00', 'USD', 'EC-42W12488F85447638', 'Success', '2017-01-02 22:45:36', '', 'verified', 1, 1, 4, 6, 'test,address1,address2,state*,12300,0878802377,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(146, 139, 31, 1, 'unpay1446155174', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-01-03 00:00:17', '', '', 3, 0, 18, 3, 'tester1.kuku,address1,address2,state*,12300,0878802377,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(147, 141, 36, 1, '0E048165E6548602S', 'kumarkailash075-buyer@gmail.com', 'SPMQ4UD2Y55WW', 'test', 1, '599.00', '0.00', 'USD', 'EC-5CA5598093447473A', 'Success', '2017-01-03 18:03:08', '', 'verified', 1, 1, 0, 0, 'ramkumar, aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(148, 141, 43, 1, 'unpay561809462', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-01-03 19:00:03', '', '', 3, 0, 11, 2, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,9546877878,ramkumar@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(149, 139, 37, 1, '9K978190GP1017646', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '699.00', '0.00', 'USD', 'EC-4SJ833019X635863X', 'Success', '2017-01-05 00:46:38', '', 'verified', 1, 1, 0, 0, 'tester1.kuku,a1,a2,s,121,0129312345,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(150, 139, 32, 1, '48C79152S49401333', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '199.00', '0.00', 'USD', 'EC-75F88663M0529415A', 'Success', '2017-01-05 01:25:31', '', 'verified', 0, 1, 13, 5, 'tester1.kuku,a1,a2,s,121,0129312345,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(151, 139, 18, 1, '65506551W68184344', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '9500.00', '0.00', 'USD', 'EC-21F93784RX820235L', 'Success', '2017-01-06 00:09:57', '', 'verified', 1, 1, 0, 0, 'tester1.kuku .....,address1 ....,address2 ....,state* ....,111111111,1231231231,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(152, 139, 38, 1, '9SA68896N63588535', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '400.00', '0.00', 'USD', 'EC-4SG447574N5538010', 'Success', '2017-01-25 03:47:32', '', 'verified', 1, 1, 0, 0, 'U,A1,A2,S4,12345,0878882377,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(153, 139, 58, 2, '7XD24410TU977121A', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '700000.00', '0.00', 'USD', 'EC-06P18230C2088043U', 'Success', '2017-01-25 03:52:29', '', 'verified', 1, 1, 0, 0, 'UU,AA1,AA2,SS4,121212,0989898898,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(154, 139, 26, 1, '6TD230289M4866144', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '20.00', '0.00', 'USD', 'EC-8U654307590837940', 'Success', '2017-02-06 23:45:01', '', 'verified', 1, 1, 0, 0, 'nama,alamat1,alamat2,provinsi,123,0123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(155, 139, 18, 1, '43C07788883290719', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '9500.00', '0.00', 'USD', 'EC-29V654004X1874255', 'Success', '2017-02-07 01:00:41', '', 'verified', 1, 1, 0, 0, 'aaadd,bbb,ccc,eee,1212,737283,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(156, 139, 29, 1, 'unpay925937685', '', '', '', 1, '10.00', '0.00', '', '', '', '2017-02-07 21:20:19', '', '', 3, 0, 0, 0, 'sadas,asdas,asdas,asdas,13414,3124,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(157, 139, 32, 1, '49G320860J878390Y', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '199.00', '0.00', 'USD', 'EC-3GJ51665FA883630B', 'Success', '2017-02-08 02:39:12', '', 'verified', 1, 1, 0, 0, 'qwqwq,wwew,qwqw,qwq,2323,2323,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(158, 139, 32, 1, 'unpay1162961019', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-02-08 02:57:00', '', '', 3, 0, 0, 0, 'ada,ada111,adada11,adad,da,ad,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(159, 139, 32, 1, 'unpay1391421453', '', '', '', 2, '398.00', '0.00', '', '', '', '2017-02-08 03:00:33', '', '', 3, 0, 0, 0, 'asas,as,asas,sas,asas,aa,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(160, 145, 18, 1, 'unpay2110147862', '', '', '', 1, '9500.00', '0.00', '', '', '', '2017-02-08 04:06:27', '', '', 0, 0, 0, 0, 'laxman,ccc,ccc,tn,123456,1234567890,kailashkumar.r@pofitec.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(161, 139, 43, 1, '4DG71849EM685150E', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '22000.00', '0.00', 'USD', 'EC-8JB61175PU7399541', 'Success', '2017-02-09 20:20:24', '', 'verified', 1, 1, 0, 0, 'notebook,ad1,ad2,ct4,1212,0123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(162, 139, 29, 1, '88N03962BV157344K', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 2, '20.00', '0.00', 'USD', 'EC-3HH711475H6909029', 'Success', '2017-02-13 23:46:42', '', 'verified', 0, 1, 4, 6, 'tester1.kuku,aaa,bbb,ddd,123,1212,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(163, 139, 31, 1, '88N03962BV157344K', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '20.00', '0.00', 'USD', 'EC-3HH711475H6909029', 'Success', '2017-02-13 23:46:42', '', 'verified', 0, 1, 18, 3, 'tester1.kuku,aaa,bbb,ddd,123,1212,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(164, 139, 32, 1, '7U642471W94949703', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '199.00', '0.00', 'USD', 'EC-6RN194416G8584748', 'Success', '2017-02-16 02:43:09', '', 'verified', 1, 1, 0, 0, 'qqq,www,eeee,tttt,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(165, 139, 17, 1, '1FA93906GB072925L', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '599.00', '0.00', 'USD', 'EC-5NB744616L536794S', 'Success', '2017-02-15 21:05:47', '', 'verified', 1, 1, 0, 0, '1212212,121212,12121212,121212,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(166, 139, 38, 1, 'unpay318454370', '', '', 'Toto', 1, '401.00', '0.00', '360', '', '', '2017-02-16 04:27:24', '', '', 0, 0, 0, 0, 'doku1,doku2,doku3,doku5,2121,112,tester1.kuku@gmail.com', 15, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(167, 139, 38, 1, '7NT52076D4693625U', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '400.00', '0.00', 'USD', 'EC-6SP65209MF6677905', 'Success', '2017-02-16 04:29:52', '', 'verified', 1, 1, 0, 0, 'qwqwqw,qwqwq,qwqwq,qwqw,3434,2434,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(168, 139, 26, 1, 'unpay196653315', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-20 02:35:37', '', '', 0, 0, 0, 0, 'popop,opopo,popop,opop,1212,121,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(169, 139, 26, 1, 'unpay1470397252', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-20 03:42:46', '', '', 3, 0, 0, 0, 'sdsd,sdsd,sdsd,sdsd,121,122,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(170, 139, 26, 1, 'unpay1729292191', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-20 03:45:53', '', '', 0, 0, 0, 0, '1221,122121,1212,1212,1212,1212,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(171, 139, 26, 1, 'unpay1953308928', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-20 03:56:58', '', '', 0, 0, 0, 0, '1212,1212,1212,121,121,1212,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(172, 139, 26, 1, 'unpay413622983', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-20 04:00:19', '', '', 0, 0, 0, 0, '1212,1212,1212,1212,1212,1212,tester1.kuku@gmail.com', 4, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(173, 139, 34, 1, 'unpay204920041', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-20 04:38:35', '', '', 0, 0, 0, 0, 'asas,sasas,asasas,asasas,1212,12121,tester1.kuku@gmail.com', 15, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(174, 139, 34, 1, 'unpay960551265', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-20 04:50:55', '', '', 0, 0, 0, 0, 'wqwqw,qwqw,wqwqw,qwqw,1212,2121,tester1.kuku@gmail.com', 15, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(175, 139, 17, 1, '1M944090MN939970M', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '599.00', '0.00', 'USD', 'EC-3HP22659XA504862S', 'Success', '2017-02-20 21:54:17', '', 'verified', 1, 1, 0, 0, 'Toto,Add1,Add2,Sta,121,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(176, 139, 18, 1, '1M944090MN939970M', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '9500.00', '0.00', 'USD', 'EC-3HP22659XA504862S', 'Success', '2017-02-20 21:54:17', '', 'verified', 1, 1, 0, 0, 'Toto,Add1,Add2,Sta,121,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(177, 139, 26, 1, '1M944090MN939970M', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '20.00', '0.00', 'USD', 'EC-3HP22659XA504862S', 'Success', '2017-02-20 21:54:17', '', 'verified', 1, 1, 0, 0, 'Toto,Add1,Add2,Sta,121,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(178, 139, 43, 1, 'unpay1314221473', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-20 21:57:50', '', '', 3, 0, 0, 0, 'Saptoto,aaa,bbb,sss,22,223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(179, 139, 44, 1, 'unpay1314221473', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-20 21:57:50', '', '', 3, 0, 0, 0, 'Saptoto,aaa,bbb,sss,22,223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(180, 139, 38, 1, 'unpay1314221473', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-20 21:57:50', '', '', 3, 0, 0, 0, 'Saptoto,aaa,bbb,sss,22,223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(181, 139, 34, 1, 'unpay1314221473', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-20 21:57:50', '', '', 0, 0, 0, 0, 'Saptoto,aaa,bbb,sss,22,223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '04', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(182, 139, 43, 1, 'unpay898064735', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 00:36:32', '', '', 3, 0, 0, 0, 'aaaa,bbbb,ccc,eee,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(183, 139, 44, 1, 'unpay898064735', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 00:36:32', '', '', 3, 0, 0, 0, 'aaaa,bbbb,ccc,eee,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(184, 139, 38, 1, 'unpay898064735', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 00:36:32', '', '', 3, 0, 0, 0, 'aaaa,bbbb,ccc,eee,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(185, 139, 34, 1, 'unpay898064735', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 00:36:32', '', '', 0, 0, 0, 0, 'aaaa,bbbb,ccc,eee,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '04', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(186, 139, 43, 1, 'unpay1527987080', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 00:52:28', '', '', 3, 0, 0, 0, '111,333,222,666,113,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(187, 139, 44, 1, 'unpay1527987080', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 00:52:28', '', '', 3, 0, 0, 0, '111,333,222,666,113,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(188, 139, 38, 1, 'unpay1527987080', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 00:52:28', '', '', 3, 0, 0, 0, '111,333,222,666,113,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(189, 139, 34, 1, 'unpay1527987080', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 00:52:28', '', '', 0, 0, 0, 0, '111,333,222,666,113,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '04', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(190, 139, 43, 1, 'unpay1162750153', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 01:28:54', '', '', 3, 0, 0, 0, 'qqqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, '');
INSERT INTO `nm_order` (`order_id`, `order_cus_id`, `order_pro_id`, `order_type`, `transaction_id`, `payer_email`, `payer_id`, `payer_name`, `order_qty`, `order_amt`, `order_tax`, `currency_code`, `token_id`, `payment_ack`, `order_date`, `created_date`, `payer_status`, `order_status`, `order_paytype`, `order_pro_color`, `order_pro_size`, `order_shipping_add`, `order_merchant_id`, `ip_address`, `process_type`, `process_datetime`, `doku_payment_datetime`, `transidmerchant`, `notify_type`, `response_code`, `status_code`, `result_msg`, `reversal`, `approval_code`, `payment_channel`, `payment_code`, `bank_issuer`, `creditcard`, `words`, `session_id`, `verify_id`, `verify_score`, `verify_status`, `check_status`, `count_check_status`, `message`, `id`, `postalservice_code`) VALUES
(191, 139, 44, 1, 'unpay1162750153', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 01:28:54', '', '', 3, 0, 0, 0, 'qqqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(192, 139, 38, 1, 'unpay1162750153', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 01:28:54', '', '', 3, 0, 0, 0, 'qqqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(193, 139, 34, 1, 'unpay1162750153', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 01:28:54', '', '', 0, 0, 0, 0, 'qqqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '04', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(194, 139, 43, 1, 'unpay525458064', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 01:51:26', '', '', 3, 0, 0, 0, 'qqq,www,eee,tt,12111,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(195, 139, 44, 1, 'unpay525458064', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 01:51:26', '', '', 3, 0, 0, 0, 'qqq,www,eee,tt,12111,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(196, 139, 38, 1, 'unpay525458064', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 01:51:26', '', '', 3, 0, 0, 0, 'qqq,www,eee,tt,12111,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(197, 139, 34, 1, 'unpay525458064', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 01:51:26', '', '', 3, 0, 0, 0, 'qqq,www,eee,tt,12111,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(198, 139, 43, 1, 'unpay2006720535', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:02:43', '', '', 3, 0, 0, 0, 'qqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(199, 139, 44, 1, 'unpay2006720535', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:02:43', '', '', 3, 0, 0, 0, 'qqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(200, 139, 38, 1, 'unpay2006720535', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:02:43', '', '', 3, 0, 0, 0, 'qqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(201, 139, 34, 1, 'unpay2006720535', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:02:43', '', '', 3, 0, 0, 0, 'qqq,www,eee,ttt,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(202, 139, 43, 1, 'unpay2105050589', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:05:28', '', '', 3, 0, 0, 0, 'rrrr,tttt,yyyy,iiii,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(203, 139, 44, 1, 'unpay2105050589', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:05:28', '', '', 3, 0, 0, 0, 'rrrr,tttt,yyyy,iiii,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(204, 139, 38, 1, 'unpay2105050589', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:05:28', '', '', 3, 0, 0, 0, 'rrrr,tttt,yyyy,iiii,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(205, 139, 34, 1, 'unpay2105050589', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:05:28', '', '', 3, 0, 0, 0, 'rrrr,tttt,yyyy,iiii,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(206, 139, 43, 1, 'unpay948629294', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:15:10', '', '', 3, 0, 0, 0, 'sss,dddd,ffff,hhhh,456,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(207, 139, 44, 1, 'unpay948629294', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:15:10', '', '', 3, 0, 0, 0, 'sss,dddd,ffff,hhhh,456,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(208, 139, 38, 1, 'unpay948629294', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:15:10', '', '', 3, 0, 0, 0, 'sss,dddd,ffff,hhhh,456,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(209, 139, 34, 1, 'unpay948629294', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:15:10', '', '', 3, 0, 0, 0, 'sss,dddd,ffff,hhhh,456,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(210, 139, 43, 1, 'unpay861609865', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:16:58', '', '', 3, 0, 0, 0, 'uuu,iiii,oooo,rrr,567,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(211, 139, 44, 1, 'unpay861609865', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:16:58', '', '', 3, 0, 0, 0, 'uuu,iiii,oooo,rrr,567,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(212, 139, 38, 1, 'unpay861609865', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:16:58', '', '', 3, 0, 0, 0, 'uuu,iiii,oooo,rrr,567,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(213, 139, 34, 1, 'unpay861609865', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:16:58', '', '', 3, 0, 0, 0, 'uuu,iiii,oooo,rrr,567,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(214, 139, 43, 1, 'unpay1526241523', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:24:40', '', '', 3, 0, 0, 0, 'vvvv,ffff,dddd,rrr,343,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(215, 139, 44, 1, 'unpay1526241523', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:24:40', '', '', 3, 0, 0, 0, 'vvvv,ffff,dddd,rrr,343,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(216, 139, 38, 1, 'unpay1526241523', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:24:40', '', '', 3, 0, 0, 0, 'vvvv,ffff,dddd,rrr,343,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(217, 139, 34, 1, 'unpay1526241523', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:24:40', '', '', 3, 0, 0, 0, 'vvvv,ffff,dddd,rrr,343,1234,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(218, 139, 43, 1, 'unpay1137274556', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:28:00', '', '', 3, 0, 0, 0, 'ggg,ggg,ggg,ggg,343,3434,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(219, 139, 44, 1, 'unpay1137274556', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:28:00', '', '', 3, 0, 0, 0, 'ggg,ggg,ggg,ggg,343,3434,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(220, 139, 38, 1, 'unpay1137274556', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:28:00', '', '', 3, 0, 0, 0, 'ggg,ggg,ggg,ggg,343,3434,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(221, 139, 34, 1, 'unpay1137274556', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:28:00', '', '', 3, 0, 0, 0, 'ggg,ggg,ggg,ggg,343,3434,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(222, 139, 43, 1, 'unpay1919778076', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:40:32', '', '', 3, 0, 0, 0, 'wwe,ewew,wewe,wewe,333,222,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(223, 139, 44, 1, 'unpay1919778076', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:40:32', '', '', 3, 0, 0, 0, 'wwe,ewew,wewe,wewe,333,222,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(224, 139, 38, 1, 'unpay1919778076', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:40:32', '', '', 3, 0, 0, 0, 'wwe,ewew,wewe,wewe,333,222,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(225, 139, 34, 1, 'unpay1919778076', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:40:32', '', '', 3, 0, 0, 0, 'wwe,ewew,wewe,wewe,333,222,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(226, 139, 43, 1, 'unpay408905665', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 02:44:03', '', '', 3, 0, 0, 0, '2,3,4,6,6,5,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(227, 139, 44, 1, 'unpay408905665', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 02:44:03', '', '', 3, 0, 0, 0, '2,3,4,6,6,5,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(228, 139, 38, 1, 'unpay408905665', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 02:44:03', '', '', 3, 0, 0, 0, '2,3,4,6,6,5,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(229, 139, 34, 1, 'unpay408905665', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 02:44:03', '', '', 3, 0, 0, 0, '2,3,4,6,6,5,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(230, 139, 43, 1, 'unpay830334542', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-21 03:53:38', '', '', 3, 0, 0, 0, '11,22,33,55,88,77,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(231, 139, 44, 1, 'unpay830334542', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-21 03:53:38', '', '', 3, 0, 0, 0, '11,22,33,55,88,77,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(232, 139, 38, 1, 'unpay830334542', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-21 03:53:38', '', '', 3, 0, 0, 0, '11,22,33,55,88,77,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(233, 139, 34, 1, 'unpay830334542', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-02-21 03:53:38', '', '', 3, 0, 0, 0, '11,22,33,55,88,77,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(234, 139, 32, 1, 'unpay418782755', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-02-21 20:37:02', '', '', 3, 0, 0, 0, 'qwqw,qwqw,qwqw,qwqw,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(235, 139, 44, 1, 'unpay172936325', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 02:25:56', '', '', 3, 0, 0, 0, 'ooo,ppp,lll,jjjj,456,123,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(236, 139, 44, 1, 'unpay156277077', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 02:29:24', '', '', 3, 0, 0, 0, 'qqq,qqq,qqq,qq,121,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(237, 139, 44, 1, 'unpay761335010', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:00:10', '', '', 3, 0, 0, 0, 'ggg,gg,gg,gg,23,3323,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(238, 139, 44, 1, 'unpay1180217682', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:05:29', '', '', 3, 0, 0, 0, 'ggg,ggg,ggg,ggg,ggg,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(239, 139, 44, 1, 'unpay853072715', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:07:44', '', '', 3, 0, 0, 0, 'dd,dd,dd,ddd,122,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(240, 139, 44, 1, 'unpay1074138555', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:10:15', '', '', 3, 0, 0, 0, 'ww,www,www,www,121,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(241, 139, 44, 1, 'unpay1669135043', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:21:02', '', '', 3, 0, 0, 0, 'fff,fff,fff,fff,2323,2232,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(242, 139, 44, 1, 'unpay1872889449', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:26:05', '', '', 3, 0, 0, 0, 'qwq,qwq,qwqw,qw,121,121,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(243, 139, 44, 1, 'unpay720388917', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:28:40', '', '', 3, 0, 0, 0, 'dfdf,dfdf,dfdf,dfdf,1212,1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(244, 139, 44, 1, 'unpay1210818714', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:30:14', '', '', 3, 0, 0, 0, 'wew,wew,wewe,wewe,2323,2323,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(245, 139, 44, 1, 'unpay522127322', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 03:35:37', '', '', 3, 0, 0, 0, 'qqq,qqq,qqq,qqqq,121,q1212,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(246, 139, 44, 1, 'unpay734051356', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 04:06:36', '', '', 3, 0, 0, 0, '232,232,323,2323,232,232,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(247, 139, 44, 1, 'unpay1311219529', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-02-22 04:51:19', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(248, 139, 44, 1, '7B793279XR1618213', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '37000.00', '0.00', 'USD', 'EC-6CC51718SK263274R', 'Success', '2017-02-22 05:15:35', '', 'verified', 1, 1, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(249, 139, 26, 1, '7B793279XR1618213', 'tester1.kuku-buyer@gmail.com', 'WZL6FXCHB7894', 'test', 1, '20.00', '0.00', 'USD', 'EC-6CC51718SK263274R', 'Success', '2017-02-22 05:15:35', '', 'verified', 1, 1, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(250, 139, 26, 1, 'unpay900669111', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-22 05:18:05', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(251, 139, 26, 1, 'unpay1952707003', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-22 05:20:30', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(252, 139, 26, 1, 'unpay1289566965', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-02-22 05:21:18', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(253, 139, 43, 1, 'unpay852643672', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 02:20:40', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(254, 139, 43, 1, 'unpay1745773559', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 02:24:59', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(255, 139, 43, 1, 'unpay442358439', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 02:26:32', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(256, 139, 43, 1, 'unpay1168085556', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 02:29:23', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(257, 139, 43, 1, 'unpay271034654', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 02:58:51', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(258, 139, 43, 1, 'unpay365482623', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 02:59:48', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(259, 139, 43, 1, 'unpay852585406', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 03:10:36', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(260, 139, 43, 1, 'unpay658670619', '', '', '', 1, '22000.00', '0.00', '', '', '', '2017-02-23 04:23:01', '', '', 3, 0, 0, 0, 'Toto,aaa,aaaaaa,sss,121,3223,tester1.kuku@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(261, 146, 18, 1, 'unpay254151333', '', '', '', 1, '9500.00', '0.00', '', '', '', '2017-02-24 02:16:05', '', '', 3, 0, 3, 2, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,0857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(262, 146, 38, 1, 'unpay254151333', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-02-24 02:16:05', '', '', 3, 0, 2, 7, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,0857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(263, 150, 26, 1, 'unpay23934', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:24:35', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(264, 150, 26, 1, 'unpay30120', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:26:21', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(265, 150, 26, 1, 'unpay7225', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:27:55', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(266, 150, 26, 1, 'unpay25311', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:28:40', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(267, 150, 26, 1, 'unpay22747', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:29:19', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(268, 150, 26, 1, 'unpay30083', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:29:56', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(269, 150, 26, 1, 'unpay8960', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:31:22', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(270, 150, 26, 1, 'unpay4734', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:31:58', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(271, 150, 26, 1, 'unpay30913', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:36:27', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(272, 150, 26, 1, 'unpay5945', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:36:28', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(273, 150, 26, 1, 'unpay26726', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:40:15', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(274, 150, 26, 1, 'unpay14247', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:41:29', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(275, 150, 26, 1, 'unpay29054', '', '', '', 1, '20.00', '0.00', '', '', '', '2017-03-03 02:44:44', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(276, 150, 34, 1, 'unpay32505', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-03 03:24:27', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(277, 150, 34, 1, 'unpay7742', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-03 03:24:57', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(278, 150, 34, 1, 'unpay4461', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-03 03:25:53', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(279, 150, 34, 1, 'unpay20898', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-03 03:29:13', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(280, 150, 34, 1, 'unpay20019', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-03 03:30:15', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(281, 150, 34, 1, 'unpay29855', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-03 03:31:27', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 0, ''),
(282, 150, 32, 1, 'unpay15547', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 02:11:32', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 95, ''),
(283, 150, 32, 1, 'unpay10116', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 02:47:38', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 96, ''),
(284, 150, 32, 1, 'unpay22277', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 02:47:39', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 97, ''),
(285, 150, 32, 1, 'unpay18827', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 02:53:51', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 98, ''),
(286, 150, 32, 1, 'unpay23516', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:07:02', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 99, ''),
(287, 150, 32, 1, 'unpay15261', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:13:59', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 100, ''),
(288, 150, 32, 1, 'unpay20283', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:19:31', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 101, ''),
(289, 150, 32, 1, 'unpay29839', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:30:12', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 102, ''),
(290, 150, 32, 1, 'unpay6624', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:30:13', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 103, ''),
(291, 150, 32, 1, 'unpay5041', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:36:41', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 24, ''),
(292, 150, 32, 1, 'unpay10352', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:57:46', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 104, ''),
(293, 150, 32, 1, 'unpay24323', '', '', '', 1, '199.00', '0.00', '', '', '', '2017-03-06 03:59:18', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 25, ''),
(294, 150, 38, 1, 'unpay12417', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-06 20:07:53', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 105, ''),
(295, 150, 38, 1, 'unpay5479', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-06 22:07:29', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 8, ''),
(296, 150, 37, 1, 'unpay5479', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-06 22:07:29', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 10, ''),
(297, 150, 38, 1, 'unpay22511', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-06 22:12:14', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 12, ''),
(298, 150, 37, 1, 'unpay22511', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-06 22:12:14', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 14, ''),
(299, 150, 38, 1, 'unpay10525', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 04:28:07', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 106, ''),
(300, 150, 37, 1, 'unpay10525', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 04:28:07', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 106, ''),
(301, 150, 38, 1, 'unpay2782', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 04:45:44', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 107, ''),
(302, 150, 37, 1, 'unpay2782', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 04:45:44', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 107, ''),
(303, 150, 38, 1, 'unpay6028', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 04:53:24', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 108, ''),
(304, 150, 37, 1, 'unpay6028', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 04:53:24', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 108, ''),
(305, 150, 38, 1, 'unpay31205', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 04:56:57', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 109, ''),
(306, 150, 37, 1, 'unpay31205', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 04:56:57', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 109, ''),
(307, 150, 38, 1, 'unpay2694', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 04:58:53', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 110, ''),
(308, 150, 37, 1, 'unpay2694', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 04:58:53', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 110, ''),
(309, 150, 38, 1, 'unpay8312', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:01:52', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 111, ''),
(310, 150, 37, 1, 'unpay8312', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:01:52', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 111, ''),
(311, 150, 38, 1, 'unpay157', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:09:27', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 112, ''),
(312, 150, 38, 1, 'unpay2457', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:11:27', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 75, ''),
(313, 150, 37, 1, 'unpay2457', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:11:27', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 94, ''),
(314, 150, 38, 1, 'unpay25694', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:14:18', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 114, ''),
(315, 150, 37, 1, 'unpay25694', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:14:18', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 114, ''),
(316, 150, 38, 1, 'unpay4159', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:14:52', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 116, ''),
(317, 150, 37, 1, 'unpay4159', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:14:52', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 116, ''),
(318, 150, 38, 1, 'unpay19724', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:16:13', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 118, ''),
(319, 150, 37, 1, 'unpay19724', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:16:13', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 118, ''),
(320, 150, 38, 1, 'unpay9813', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:17:10', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 120, ''),
(321, 150, 37, 1, 'unpay9813', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:17:10', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 120, ''),
(322, 150, 38, 1, 'unpay26730', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:18:19', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 122, ''),
(323, 150, 37, 1, 'unpay26730', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:18:19', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 122, ''),
(324, 150, 38, 1, 'unpay29019', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:19:05', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 124, ''),
(325, 150, 37, 1, 'unpay29019', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:19:05', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 124, ''),
(326, 150, 38, 1, 'unpay11768', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:20:33', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 126, ''),
(327, 150, 37, 1, 'unpay11768', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:20:33', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 126, ''),
(328, 150, 38, 1, 'unpay21201', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:23:21', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 128, ''),
(329, 150, 37, 1, 'unpay21201', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:23:21', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 128, ''),
(330, 150, 38, 1, 'unpay8502', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:24:26', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 130, ''),
(331, 150, 37, 1, 'unpay8502', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:24:26', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 130, ''),
(332, 150, 38, 1, 'unpay18487', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:26:40', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 131, ''),
(333, 150, 37, 1, 'unpay18487', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:26:40', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 131, ''),
(334, 150, 38, 1, 'unpay12708', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:27:32', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 132, ''),
(335, 150, 37, 1, 'unpay12708', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:27:32', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 132, ''),
(336, 150, 38, 1, 'unpay3161', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:29:49', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 133, ''),
(337, 150, 37, 1, 'unpay3161', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:29:49', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 133, ''),
(338, 150, 17, 1, 'unpay3161', '', '', '', 1, '599.00', '0.00', '', '', '', '2017-03-07 05:29:49', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 133, ''),
(339, 150, 38, 1, 'unpay4126', '', '', '', 1, '400.00', '0.00', '', '', '', '2017-03-07 05:30:29', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 134, ''),
(340, 150, 37, 1, 'unpay4126', '', '', '', 1, '699.00', '0.00', '', '', '', '2017-03-07 05:30:29', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 134, ''),
(341, 150, 17, 1, 'unpay4126', '', '', '', 1, '599.00', '0.00', '', '', '', '2017-03-07 05:30:29', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 134, ''),
(342, 150, 34, 1, 'unpay2617', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-07 20:00:26', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 135, 'JNE_YES'),
(343, 150, 44, 1, 'unpay2617', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-03-07 20:00:26', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 135, 'JNE_YES'),
(344, 150, 34, 1, 'unpay4341', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-07 20:13:00', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 136, 'JNE_YES'),
(345, 150, 44, 1, 'unpay4341', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-03-07 20:13:00', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 136, 'JNE_YES'),
(346, 150, 34, 1, 'unpay32730', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-07 20:20:08', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 137, 'JNE_OK'),
(347, 150, 44, 1, 'unpay32730', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-03-07 20:20:08', '', '', 3, 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 137, 'JNE_OK'),
(348, 150, 34, 1, 'unpay3083', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-07 20:23:38', '', '', 3, 0, 0, 0, 'Koko,Jl Kocak,JL Ngibul,Jawa,15333,0866777655,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 138, 'JNE_OK'),
(349, 150, 44, 1, 'unpay3083', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-03-07 20:23:38', '', '', 3, 0, 0, 0, 'Koko,Jl Kocak,JL Ngibul,Jawa,15333,0866777655,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 138, 'JNE_OK'),
(350, 150, 18, 1, 'unpay3083', '', '', '', 1, '9500.00', '0.00', '', '', '', '2017-03-07 20:23:38', '', '', 3, 0, 0, 0, 'Koko,Jl Kocak,JL Ngibul,Jawa,15333,0866777655,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, 138, 'JNE_OK'),
(351, 150, 34, 1, 'unpay10984', '', '', '', 1, '50.00', '0.00', '', '', '', '2017-03-07 20:24:48', '', '', 3, 0, 0, 0, 'Koko,Jl Kocak,JL Ngibul,Jawa,15333,866777655,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, NULL, ''),
(352, 150, 44, 1, 'unpay10984', '', '', '', 1, '37000.00', '0.00', '', '', '', '2017-03-07 20:24:48', '', '', 3, 0, 0, 0, 'Koko,Jl Kocak,JL Ngibul,Jawa,15333,866777655,harry.okta.maulana@gmail.com', 0, '', '', NULL, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, '', 0, 0, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_ordercod`
--

CREATE TABLE `nm_ordercod` (
  `cod_id` int(10) UNSIGNED NOT NULL,
  `cod_transaction_id` varchar(60) NOT NULL,
  `cod_cus_id` int(10) UNSIGNED NOT NULL,
  `cod_pro_id` int(11) UNSIGNED NOT NULL,
  `cod_order_type` tinyint(4) NOT NULL COMMENT '1-product,2-deals',
  `cod_qty` int(11) NOT NULL,
  `cod_amt` decimal(10,2) NOT NULL,
  `cod_tax` decimal(10,2) NOT NULL,
  `cod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cod_status` tinyint(4) NOT NULL COMMENT '1-sucess,2-complete,3-hold,4-failed',
  `created_date` varchar(20) NOT NULL,
  `cod_paytype` smallint(6) NOT NULL,
  `cod_pro_color` tinyint(4) NOT NULL,
  `cod_pro_size` tinyint(4) NOT NULL,
  `cod_ship_addr` text NOT NULL,
  `cod_merchant_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_ordercod`
--

INSERT INTO `nm_ordercod` (`cod_id`, `cod_transaction_id`, `cod_cus_id`, `cod_pro_id`, `cod_order_type`, `cod_qty`, `cod_amt`, `cod_tax`, `cod_date`, `cod_status`, `created_date`, `cod_paytype`, `cod_pro_color`, `cod_pro_size`, `cod_ship_addr`, `cod_merchant_id`) VALUES
(2, '9WvmAPSH', 23, 11, 1, 1, '1573.00', '0.00', '2016-06-10 03:23:00', 1, '', 0, 12, 6, 'kailashkumar,165,nehru street,tamilnadu,123456,1234567890', 0),
(3, 'OqiTjnRY', 25, 10, 1, 1, '349.00', '0.00', '2016-06-10 03:46:54', 3, '', 0, 12, 1, 'maheshwaran,155,,Anna street,tamilnadu,123456,1234567890', 0),
(4, '4eRFlF3I', 25, 11, 1, 1, '1573.00', '0.00', '2016-06-10 03:49:09', 3, '', 0, 12, 6, 'maheshwaran,155,,Anna street,tamilnadu,123456,1234567890', 0),
(5, 'Q0im3BDy', 24, 11, 1, 1, '1573.00', '0.00', '2016-06-10 03:52:49', 3, '', 0, 12, 6, 'dhanasekar,147,my street,tamilnadu,123456,1234567890', 0),
(6, 'Mm0bMdJK', 25, 14, 1, 1, '8499.00', '0.00', '2016-06-10 04:17:31', 3, '', 0, 19, 10, 'maheshwaran,155,,Anna street,tamilnadu,123456,1234567890', 0),
(7, 'Sj4oddUf', 25, 33, 1, 1, '199.00', '0.00', '2016-06-10 04:22:50', 3, '', 0, 7, 1, 'mahesh,12,gandhipuram,tamilnadu,123456,1234567890', 0),
(8, '5hKo9QmB', 25, 9, 1, 1, '599.00', '0.00', '2016-06-10 04:25:35', 3, '', 0, 13, 1, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890', 0),
(9, 'OYPw6feZ', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(10, 'OYPw6feZ', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 0, 0, ',,,,,', 0),
(11, 'OYPw6feZ', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 13, 13, ',,,,,', 0),
(12, 'OYPw6feZ', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 0, 0, ',,,,,', 0),
(13, 'OYPw6feZ', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 0, 0, ',,,,,', 0),
(14, 'OYPw6feZ', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 0, 0, ',,,,,', 0),
(15, 'OYPw6feZ', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:26:25', 3, '', 0, 0, 0, ',,,,,', 0),
(16, 'lb9GLDtR', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(17, 'lb9GLDtR', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 0, 0, ',,,,,', 0),
(18, 'lb9GLDtR', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 13, 13, ',,,,,', 0),
(19, 'lb9GLDtR', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 0, 0, ',,,,,', 0),
(20, 'lb9GLDtR', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 0, 0, ',,,,,', 0),
(21, 'lb9GLDtR', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 0, 0, ',,,,,', 0),
(22, 'lb9GLDtR', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:26:35', 3, '', 0, 0, 0, ',,,,,', 0),
(23, 'PZ1rgQHJ', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(24, 'PZ1rgQHJ', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 0, 0, ',,,,,', 0),
(25, 'PZ1rgQHJ', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 13, 13, ',,,,,', 0),
(26, 'PZ1rgQHJ', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 0, 0, ',,,,,', 0),
(27, 'PZ1rgQHJ', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 0, 0, ',,,,,', 0),
(28, 'PZ1rgQHJ', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 0, 0, ',,,,,', 0),
(29, 'PZ1rgQHJ', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:26:45', 3, '', 0, 0, 0, ',,,,,', 0),
(30, 'CbNDY0Es', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(31, 'CbNDY0Es', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 0, 0, ',,,,,', 0),
(32, 'CbNDY0Es', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 13, 13, ',,,,,', 0),
(33, 'CbNDY0Es', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 0, 0, ',,,,,', 0),
(34, 'CbNDY0Es', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 0, 0, ',,,,,', 0),
(35, 'CbNDY0Es', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 0, 0, ',,,,,', 0),
(36, 'CbNDY0Es', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:26:55', 3, '', 0, 0, 0, ',,,,,', 0),
(37, '59GkS84h', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(38, '59GkS84h', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 0, 0, ',,,,,', 0),
(39, '59GkS84h', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 13, 13, ',,,,,', 0),
(40, '59GkS84h', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 0, 0, ',,,,,', 0),
(41, '59GkS84h', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 0, 0, ',,,,,', 0),
(42, '59GkS84h', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 0, 0, ',,,,,', 0),
(43, '59GkS84h', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:27:05', 3, '', 0, 0, 0, ',,,,,', 0),
(44, 'avPATAKg', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(45, 'avPATAKg', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 0, 0, ',,,,,', 0),
(46, 'avPATAKg', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 13, 13, ',,,,,', 0),
(47, 'avPATAKg', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 0, 0, ',,,,,', 0),
(48, 'avPATAKg', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 0, 0, ',,,,,', 0),
(49, 'avPATAKg', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 0, 0, ',,,,,', 0),
(50, 'avPATAKg', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:27:15', 3, '', 0, 0, 0, ',,,,,', 0),
(51, 'p4bs1uOi', 30, 22, 1, 1, '30.00', '0.00', '2016-06-11 11:27:25', 3, '', 0, 0, 0, 'ilakkiya m,ganghipuram,ganghipuram,tamilnadu,641008,8883152529', 0),
(52, 'p4bs1uOi', 30, 28, 1, 1, '15.00', '0.00', '2016-06-11 11:27:25', 3, '', 0, 0, 0, ',,,,,', 0),
(53, 'p4bs1uOi', 30, 9, 1, 1, '599.00', '0.00', '2016-06-11 11:27:25', 3, '', 0, 13, 13, ',,,,,', 0),
(54, 'p4bs1uOi', 30, 45, 2, 1, '49.00', '0.00', '2016-06-11 11:27:25', 1, '', 0, 0, 0, ',,,,,', 0),
(55, 'p4bs1uOi', 30, 44, 2, 1, '49.00', '0.00', '2016-06-11 11:27:25', 1, '', 0, 0, 0, ',,,,,', 0),
(56, 'p4bs1uOi', 30, 35, 2, 1, '59.00', '0.00', '2016-06-11 11:27:25', 3, '', 0, 0, 0, ',,,,,', 0),
(57, 'p4bs1uOi', 30, 13, 2, 1, '699.00', '0.00', '2016-06-11 11:27:25', 1, '', 0, 0, 0, ',,,,,', 0),
(58, 'jgVSv9x2', 23, 21, 1, 1, '50.00', '0.00', '2016-06-18 17:25:12', 3, '', 0, 4, 4, 'kailashkumar,155,,Anna street,tamilnadu,123456,1234567890,', 0),
(59, 'l7m0ZvzV', 43, 29, 1, 1, '10.00', '0.00', '2016-06-21 07:42:03', 3, '', 0, 2, 5, 'kumar,144, Ram nagar,sengupatha,TamilNadu,641009,1231231231,', 0),
(60, 'tH4ngdDM', 23, 21, 1, 1, '50.00', '0.00', '2016-06-21 07:48:23', 3, '', 0, 8, 4, 'kailashkumar,155,,Anna street,tamilnadu,123456,1234567890,', 0),
(61, 'wevRSXhG', 23, 17, 1, 1, '479.00', '0.00', '2016-06-21 08:19:08', 3, '', 0, 16, 1, 'kailash,jansinagar,v.chatram,tn,638004,1234567890,kumarkailash075@gmail.com', 0),
(62, 'wevRSXhG', 23, 21, 1, 1, '50.00', '0.00', '2016-06-21 08:19:08', 3, '', 0, 8, 4, 'kailash,jansinagar,v.chatram,tn,638004,1234567890,kumarkailash075@gmail.com', 0),
(63, 'lgLhm2to', 43, 13, 1, 1, '699.00', '0.00', '2016-06-21 08:33:01', 3, '', 0, 0, 0, 'kumar,144, Ram nagar,sengupatha,TamilNadu,641009,1231231231,kumar@pofitec.com', 0),
(64, '0jtp89vX', 43, 11, 1, 1, '1573.00', '0.00', '2016-06-21 08:34:13', 3, '', 0, 0, 0, 'kumar,144, Ram nagar,sengupatha,TamilNadu,641009,1231231231,kumar@pofitec.com', 0),
(65, 'XkPGCA0r', 48, 17, 1, 1, '479.00', '0.00', '2016-06-21 08:36:54', 3, '', 0, 16, 1, 'kailashkumar,155,,Anna street,tamilnadu,123456,1234567890,kailashkumar.r@pofitec.com', 0),
(66, 'kP2OOfA0', 48, 18, 1, 1, '9500.00', '0.00', '2016-06-21 08:41:33', 3, '', 0, 3, 1, 'kailashkumar,anna street,chatram,tn,123456,1234567890,kailashkumar.r@gmail.com', 0),
(67, 'iGuSLB1T', 48, 13, 1, 1, '699.00', '0.00', '2016-06-21 08:50:40', 3, '', 0, 12, 6, 'kailashkumar,anna street,chatram,tn,123456,1234567890,kumarkailash075@gmail.com', 0),
(68, 'WvIjasje', 43, 15, 1, 1, '9999.00', '0.00', '2016-06-21 08:51:54', 3, '', 0, 0, 0, 'kumar,144, Ram nagar,sengupatha,TamilNadu,641009,1231231231,kumar@pofitec.com', 0),
(69, '0y1K8VYa', 51, 15, 1, 1, '9999.00', '0.00', '2016-06-21 08:54:15', 3, '', 0, 0, 0, 'rajesh,144, Ram nagar,sengupatha,TamilNadu,641009,1231231231,erkprajesh@gmail.com', 0),
(70, 'NFOlWGD3', 48, 9, 1, 1, '599.00', '0.00', '2016-06-21 08:55:18', 3, '', 0, 0, 0, 'kailashkumar,anna street,chatram,tn,123456,1234567890,kailashkumar.r@pofitec.com', 0),
(71, 'iXrxCcRg', 48, 21, 1, 1, '50.00', '0.00', '2016-06-21 09:02:29', 3, '', 0, 8, 1, 'kailashkumar,anna street,chatram,tn,123456,1234567890,kumarkailash075@gmail.com', 0),
(72, 'svpI47Pj', 25, 9, 1, 1, '599.00', '0.00', '2016-06-21 09:09:24', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(73, 'EOzBpRES', 48, 9, 1, 1, '599.00', '0.00', '2016-06-21 10:00:12', 3, '', 0, 13, 1, 'kailashkumar,anna street,chatram,tn,123456,1234567890,kumarkailash075@gmail.com', 0),
(74, 'UqgixZIE', 25, 37, 1, 1, '699.00', '0.00', '2016-06-21 12:00:21', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(75, 'bioWWIO5', 25, 20, 1, 1, '20.00', '0.00', '2016-06-21 12:01:47', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(76, 'qk7EpHI9', 58, 13, 1, 1, '699.00', '0.00', '2016-06-22 17:03:48', 3, '', 0, 12, 6, 'test,144,ramnagar,TamilNadu,641009,1231231231,sales@laravelecommerce.com', 0),
(77, 'Wj4MQ1km', 74, 11, 1, 3, '4719.00', '0.00', '2016-06-23 06:10:10', 3, '', 0, 12, 7, 'rajesh,144,122,tn,641010,1231231231,erkprajesh@gmail.com', 0),
(78, 'Wj4MQ1km', 74, 17, 1, 1, '479.00', '0.00', '2016-06-23 06:10:10', 3, '', 0, 6, 1, 'sddsfd,sdvsd,sdvsdv,sdvsdv,123123,1231231231,er@er.com', 0),
(79, 'IzrQJsE0', 75, 9, 1, 1, '599.00', '0.00', '2016-06-23 06:15:05', 3, '', 0, 0, 0, 'kumar,test1,test2,tn,641009,9745232341,maheswaran@pofitec.com', 0),
(80, 'IzrQJsE0', 75, 11, 1, 1, '1573.00', '0.00', '2016-06-23 06:15:05', 3, '', 0, 12, 7, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(81, 'ZT4lBMuL', 75, 9, 1, 1, '599.00', '0.00', '2016-06-23 06:18:30', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(82, 'RCzVnnIb', 75, 9, 1, 1, '599.00', '0.00', '2016-06-23 06:21:28', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(83, 'RCzVnnIb', 75, 11, 1, 1, '1573.00', '0.00', '2016-06-23 06:21:28', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(84, 'QBs1y0kg', 75, 13, 1, 1, '699.00', '0.00', '2016-06-23 06:25:28', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(85, 'QBs1y0kg', 75, 14, 1, 1, '8499.00', '0.00', '2016-06-23 06:25:28', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(86, 'z3mLALps', 75, 9, 1, 1, '599.00', '0.00', '2016-06-23 06:30:27', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(87, 'z3mLALps', 75, 11, 1, 1, '1573.00', '0.00', '2016-06-23 06:30:27', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,kumar@pofitec.com', 0),
(88, 'z3mLALps', 75, 13, 1, 1, '699.00', '0.00', '2016-06-23 06:30:27', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,charles.j@pofitec.com', 0),
(89, 'JUuJiL2f', 42, 9, 1, 1, '599.00', '0.00', '2016-06-23 06:55:12', 3, '', 0, 13, 13, 'charles,xxxxx,xxxxxx,tamilnadu,641009,9498056637,charlesvictor.j@pofitec.com', 0),
(90, 'JUuJiL2f', 42, 15, 1, 1, '9999.00', '0.00', '2016-06-23 06:55:12', 3, '', 0, 3, 1, 'victor,xxxx,xxxx,tamilnadu,641009,9498056637,chalesvictor.info@gmail.com', 0),
(91, 'j8gh2u4d', 76, 11, 1, 1, '1573.00', '0.00', '2016-07-01 15:56:46', 3, '', 0, 0, 0, 'Pavi,Ddf,Ff,Ggg,638452,1234567890,pavithrandbpro@gmail.com', 0),
(92, 'Kx2rQPjQ', 76, 13, 1, 1, '699.00', '0.00', '2016-07-01 16:00:06', 3, '', 0, 0, 0, 'Pavithran,144, Sengupta Street | Near Hotel City Towers,Ram Nagar | Coimbatore - 641009,Tamil Nadu,641009,9787467575,pavithrandbpro@gmail.com', 0),
(93, 'pHsuuWgj', 76, 21, 1, 1, '50.00', '0.00', '2016-07-01 16:01:22', 3, '', 0, 0, 0, 'test,test mail,tes,test,123456,1234567890,pavithran.g@pofitec.com', 0),
(94, '5Pml8btH', 25, 11, 1, 1, '1573.00', '0.00', '2016-07-01 16:02:55', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(95, '5Pml8btH', 25, 13, 1, 1, '699.00', '0.00', '2016-07-01 16:02:55', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(96, 'uTagFXCm', 25, 11, 1, 1, '1573.00', '0.00', '2016-07-01 16:04:07', 3, '', 0, 0, 0, 'maheshwaran,gandhipuram,ramnagar,tamilnadu,123456,1234567890,maheswaran@pofitec.com', 0),
(97, 'mMD3pBAC', 25, 13, 1, 1, '699.00', '0.00', '2016-07-01 16:05:14', 3, '', 0, 0, 0, 'test,test,test,test,123456,1234567890,charles.j@pofitec.com', 0),
(98, '5Zwmhnj4', 76, 13, 1, 1, '699.00', '0.00', '2016-07-01 16:06:43', 3, '', 0, 0, 0, 'test,test mail,tes,test,123456,1234567890,pavithran.g@pofitec.com', 0),
(99, 'YHLHgVKi', 25, 13, 1, 1, '699.00', '0.00', '2016-07-01 16:09:30', 3, '', 0, 0, 0, 'test,test,test,test,123456,1234567890,charles.j@pofitec.com', 0),
(100, '3jdOFHkf', 76, 13, 1, 1, '699.00', '0.00', '2016-07-01 16:23:45', 3, '', 0, 0, 0, 'test,test mail,tes,test,123456,1234567890,pavithran.g@pofitec.com', 0),
(101, '4VMOK411', 81, 24, 1, 1, '30.00', '0.00', '2016-07-09 12:29:56', 3, '', 0, 0, 0, 'TESTE 1,123,teste,sp,11600-000,123123123,teste@teste.com', 9),
(102, 'huwHgoS9', 84, 14, 1, 1, '8499.00', '0.00', '2016-08-25 12:40:01', 3, '', 0, 19, 10, 'kailashkumar,dfgfdgd,dfgfgdg,dggdfg,123456,1234567890,kumarkailash075@gmail.com', 9),
(103, '73jUIiyu', 84, 13, 1, 1, '699.00', '0.00', '2016-08-29 09:23:23', 3, '', 0, 12, 4, 'kailashkumar,dg,dfgfdg,dfgdfg,123456,1234567890,kumarkailash075@gmail.com', 9),
(104, 'MvdrPn46', 103, 13, 1, 1, '699.00', '0.00', '2016-08-30 11:42:26', 3, '', 0, 12, 6, 'Testing,testing address,address two,testing,32412,1231231231,iesien22@yahoo.com', 9),
(105, 'KwFJYVzW', 104, 17, 1, 1, '479.00', '0.00', '2016-08-30 13:41:30', 1, '', 0, 6, 1, 'Burham,cimahi depok,rt07,delhi,10222,rezka@gmai,rezka@gmail.com', 12),
(106, 'TnypbbGu', 104, 38, 1, 1, '400.00', '0.00', '2016-08-30 14:38:47', 1, '', 0, 3, 1, 'Burham,cimahi depok,rt07,delhi,10222,812122112,rezka@gmail.com', 19),
(107, 'LbPMcMu6', 110, 26, 1, 1, '20.00', '0.00', '2016-09-28 11:02:19', 1, '', 0, 2, 7, 'mahmoud,cairo,cairo,cairo,76878,+201111130,mogahead@gmail.com', 9),
(108, 'LbPMcMu6', 110, 11, 1, 2, '3146.00', '0.00', '2016-09-28 11:02:19', 1, '', 0, 12, 5, 'mahmoud,cairo,cairo,cairo,76878,+201111130,mogahead@gmail.com', 15),
(109, 'ShXub3Ge', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 11:53:20', 3, '', 0, 13, 3, ',,,,,,', 0),
(110, 'wCryOpkZ', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 11:55:06', 3, '', 0, 13, 3, ',,,,,,', 0),
(111, 'bibajVfl', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 11:59:48', 3, '', 0, 13, 3, ',,,,,,', 0),
(112, 'KlIyxHNL', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 12:05:14', 3, '', 0, 13, 3, ',,,,,,', 0),
(113, 'JQ57ouKA', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 12:05:37', 3, '', 0, 13, 3, ',,,,,,', 0),
(114, 'uewNxyPv', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 12:06:10', 3, '', 0, 13, 3, ',,,,,,', 0),
(115, '5ORhOEB9', 84, 35, 1, 1, '299.00', '0.00', '2016-11-02 12:09:29', 3, '', 0, 13, 3, ',,,,,,', 0),
(116, 'oNeEWkG1', 84, 31, 1, 1, '20.00', '0.00', '2016-11-02 12:13:17', 3, '', 0, 14, 3, ',,,,,,', 0),
(117, 'AvQEABpA', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:21:40', 3, '', 0, 3, 3, ',,,,,,', 0),
(118, 'SEywcJPh', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:22:49', 3, '', 0, 3, 3, ',,,,,,', 0),
(119, '4xFTjTRs', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:24:47', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(120, 'K33ATkxh', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:29:06', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(121, 'pj4q7kkY', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:30:10', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(122, 'xil8xroD', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:30:41', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(123, '73TywZtC', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:31:02', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(124, 'EkOm6dVn', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:31:39', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(125, 'pm5udNkC', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:33:44', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(126, 'tpioIaxP', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:34:49', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(127, 'CL9GK0VK', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:35:43', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(128, 'VQUucOlj', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:37:33', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(129, 'rDALZnbO', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:37:54', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(130, 'zy04wMzL', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:40:27', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(131, 'scqhCW1v', 84, 22, 1, 1, '30.00', '0.00', '2016-11-02 12:40:45', 3, '', 0, 3, 3, 'kannan,36,vaheni street,tn,121211,1234567890,kumarkailash075@gmail.com', 0),
(132, 'pr6cJt0j', 84, 25, 1, 1, '20.00', '0.00', '2016-11-02 17:39:22', 3, '', 0, 4, 4, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0),
(133, 'RLycuxJD', 84, 25, 1, 1, '20.00', '0.00', '2016-11-02 17:39:52', 3, '', 0, 4, 4, 'guna,75,hghdhghgs,tn,123456,1234567890,kumarkailash075@gmail.com', 0),
(134, 'SIlc0AWh', 124, 18, 1, 1, '9500.00', '0.00', '2016-11-15 06:36:04', 3, '', 0, 3, 2, 'pofi kailash,bcvbcvcvb,vcbvcbcvbcvb,tn,123456,1234567890,kailashkumar.r@esec.ac.in', 0),
(135, 'JOywjWvB', 141, 18, 1, 1, '9500.00', '0.00', '2016-12-30 00:08:37', 1, '', 0, 3, 2, 'ramkumar,aaaaaa,bbbbbbbbb,tn,123456,1234567890,ramkumar@gmail.com', 0),
(136, 'Zrk4RvGg', 139, 29, 1, 1, '10.00', '0.00', '2017-02-07 19:55:42', 3, '', 0, 4, 6, 'Toto,address1,address2,state4,1234566,2311212,tester1.kuku@gmail.com', 0),
(137, 'SwAOoYzJ', 139, 32, 1, 1, '199.00', '0.00', '2017-02-08 02:57:20', 3, '', 0, 0, 0, 'ada,ada111,adada11,adad,da,ad,tester1.kuku@gmail.com', 0),
(138, 'R1osCfB0', 139, 32, 1, 1, '199.00', '0.00', '2017-02-08 02:57:55', 3, '', 0, 0, 0, 'adad,dada,adadad,adad,da,ddad,tester1.kuku@gmail.com', 0),
(139, 'Xn0vhhkR', 139, 32, 1, 1, '199.00', '0.00', '2017-02-08 02:58:56', 1, '', 0, 0, 0, '121,212,qdadad,adad,dada,dada,tester1.kuku@gmail.com', 0),
(140, 'OisGnYcn', 150, 26, 1, 1, '20.00', '0.00', '2017-03-02 21:15:07', 3, '', 0, 0, 0, 'Harry,JL Pajak,Jl. Bunga,DKI,15222,0863535353,harry.okta.maulana@gmail.com', 0),
(141, 'OisGnYcn', 150, 34, 1, 1, '50.00', '0.00', '2017-03-02 21:15:07', 3, '', 0, 0, 0, 'Harry,JL Pajak,Jl. Bunga,DKI,15222,0863535353,harry.okta.maulana@gmail.com', 0),
(142, 'WKL5T3eS', 150, 26, 1, 1, '20.00', '0.00', '2017-03-02 21:21:04', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(143, 'xAlriGpU', 150, 32, 1, 1, '199.00', '0.00', '2017-03-02 21:30:52', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(144, 'xAlriGpU', 150, 34, 1, 1, '50.00', '0.00', '2017-03-02 21:30:52', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(145, 'somCqg33', 150, 34, 1, 1, '50.00', '0.00', '2017-03-02 21:32:16', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(146, 'aMk01IKp', 150, 32, 1, 1, '199.00', '0.00', '2017-03-03 00:30:05', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(147, 'c59BOR1L', 150, 34, 1, 1, '50.00', '0.00', '2017-03-03 00:31:12', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(148, 'hTcODx2H', 150, 34, 1, 1, '50.00', '0.00', '2017-03-03 00:34:24', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(149, 'tGqcfprf', 150, 34, 1, 1, '50.00', '0.00', '2017-03-03 00:39:36', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(150, '8AztcQnG', 150, 34, 1, 1, '50.00', '0.00', '2017-03-03 00:41:58', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(151, 'QojSq4Vy', 150, 34, 1, 1, '50.00', '0.00', '2017-03-03 00:43:07', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(152, 'zm0qhejt', 150, 26, 1, 1, '20.00', '0.00', '2017-03-03 00:43:45', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(153, 'qVuuk0SN', 150, 17, 1, 1, '599.00', '0.00', '2017-03-03 00:45:28', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(154, 'rkrQWCap', 150, 38, 1, 1, '400.00', '0.00', '2017-03-03 00:46:54', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(155, 'vUiGhYwD', 150, 17, 1, 1, '599.00', '0.00', '2017-03-03 00:48:21', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0),
(156, 'gNo7eEuw', 150, 38, 1, 1, '400.00', '0.00', '2017-03-03 00:53:30', 3, '', 0, 0, 0, 'Harry,JL. Pajak Raya,JL. Budi Jaya,Jawa Barat,15315,857173255,harry.okta.maulana@gmail.com', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_order_auction`
--

CREATE TABLE `nm_order_auction` (
  `oa_id` int(11) NOT NULL,
  `oa_pro_id` int(11) NOT NULL,
  `oa_cus_id` int(11) NOT NULL,
  `oa_cus_name` varchar(150) NOT NULL,
  `oa_cus_email` varchar(250) NOT NULL,
  `oa_cus_address` text NOT NULL,
  `oa_bid_amt` int(11) NOT NULL,
  `oa_bid_shipping_amt` int(11) NOT NULL,
  `oa_original_bit_amt` int(11) NOT NULL,
  `oa_bid_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `oa_bid_winner` int(11) NOT NULL COMMENT '1=> Winner, 0=> Bidders',
  `oa_bid_item_status` int(11) NOT NULL COMMENT '0=> Onprocess, 1=> Send,  3=>Cancelled',
  `oa_delivery_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_order_auction`
--

INSERT INTO `nm_order_auction` (`oa_id`, `oa_pro_id`, `oa_cus_id`, `oa_cus_name`, `oa_cus_email`, `oa_cus_address`, `oa_bid_amt`, `oa_bid_shipping_amt`, `oa_original_bit_amt`, `oa_bid_date`, `oa_bid_winner`, `oa_bid_item_status`, `oa_delivery_date`) VALUES
(1, 1, 2, 'kumar ', 'kumar@gmail.com ', 'cbe', 10501, 10, 10500, '2014-08-11 23:23:05', 1, 3, '2014-08-12 04:53:01'),
(2, 3, 4, 'testing ', 'testing@nexplocindia.com ', 'coimbatore ,vadavalli', 1010, 25, 1000, '2014-08-11 23:24:36', 1, 1, '2014-08-12 04:54:30'),
(3, 1, 4, 'testing ', 'testing@nexplocindia.com ', 'Coimbatore vadavalli', 10550, 10, 10500, '2014-08-11 23:21:40', 0, 0, '0000-00-00 00:00:00'),
(4, 4, 1, 'yamuna ', 'yamuna@nexplocindia.com ', 'coimbatore,vadavalli', 90010, 10, 9000, '2014-08-12 23:02:22', 0, 0, '0000-00-00 00:00:00'),
(5, 1, 7, 'DivyaNanjappan ', 'divya@gmail.com ', 'cbe', 10551, 10, 10500, '2014-08-27 22:43:06', 0, 0, '0000-00-00 00:00:00'),
(6, 5, 42, 'suresh1 ', 'suresh@claydip.com ', 'asda', 1123, 12, 100, '2014-11-24 12:13:02', 0, 0, '0000-00-00 00:00:00'),
(7, 9, 131, 'Vineet ', 'vineet19universal@gmail.com ', 'Test', 270, 20, 250, '2016-03-09 12:13:23', 0, 0, '0000-00-00 00:00:00'),
(8, 9, 131, 'Vineet ', 'vineet19universal@gmail.com ', 'test', 271, 20, 250, '2016-03-09 12:13:51', 0, 0, '0000-00-00 00:00:00'),
(9, 9, 124, 'testery ', 'yksin98@yahoo.com ', '21 jalajjjjj', 285, 20, 250, '2016-03-11 09:50:14', 0, 0, '0000-00-00 00:00:00'),
(10, 9, 124, 'testery ', 'yksin98@yahoo.com ', 'as', 288, 20, 250, '2016-03-11 09:51:15', 0, 0, '0000-00-00 00:00:00'),
(11, 9, 124, 'testery ', 'yksin98@yahoo.com ', 'nice', 289, 20, 250, '2016-03-11 09:51:47', 0, 0, '0000-00-00 00:00:00'),
(12, 9, 124, 'testery ', 'yksin98@yahoo.com ', 'as', 289, 20, 250, '2016-03-11 09:52:06', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_paymentsettings`
--

CREATE TABLE `nm_paymentsettings` (
  `ps_id` tinyint(3) UNSIGNED NOT NULL,
  `ps_flatshipping` decimal(10,2) NOT NULL COMMENT 'shipping Tax Percentage',
  `ps_taxpercentage` tinyint(3) UNSIGNED NOT NULL,
  `ps_extenddays` smallint(5) UNSIGNED NOT NULL COMMENT 'Auction Extend Days',
  `ps_alertdays` int(11) NOT NULL,
  `ps_minfundrequest` int(10) UNSIGNED NOT NULL,
  `ps_maxfundrequest` int(10) UNSIGNED NOT NULL,
  `ps_referralamount` int(11) NOT NULL,
  `ps_countryid` int(11) NOT NULL,
  `ps_countrycode` varchar(10) NOT NULL,
  `ps_cursymbol` varchar(10) NOT NULL,
  `ps_curcode` varchar(10) NOT NULL,
  `ps_paypalaccount` varchar(150) NOT NULL,
  `ps_paypal_api_pw` varchar(250) NOT NULL,
  `ps_paypal_api_signature` varchar(250) NOT NULL,
  `ps_authorize_trans_key` varchar(250) NOT NULL,
  `ps_authorize_api_id` varchar(250) NOT NULL,
  `ps_paypal_pay_mode` tinyint(4) NOT NULL COMMENT '0->Test Mode, 1-> Live Mode'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_paymentsettings`
--

INSERT INTO `nm_paymentsettings` (`ps_id`, `ps_flatshipping`, `ps_taxpercentage`, `ps_extenddays`, `ps_alertdays`, `ps_minfundrequest`, `ps_maxfundrequest`, `ps_referralamount`, `ps_countryid`, `ps_countrycode`, `ps_cursymbol`, `ps_curcode`, `ps_paypalaccount`, `ps_paypal_api_pw`, `ps_paypal_api_signature`, `ps_authorize_trans_key`, `ps_authorize_api_id`, `ps_paypal_pay_mode`) VALUES
(1, '0.00', 0, 0, 0, 0, 0, 0, 7, 'USA', '$', 'USD', 'tester1.kuku-facilitator_api1.gmail.com', 'GDQ9HPBD8XPBGHSU', 'AFcWxV21C7fd0v3bYYYRCpSSRl31Aq7DFLFO7FlU0dBxDTIe5YLjjJv.', '', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_procart`
--

CREATE TABLE `nm_procart` (
  `pc_id` int(10) UNSIGNED NOT NULL,
  `pc_date` datetime NOT NULL,
  `pc_pro_id` int(11) NOT NULL,
  `pc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_procolor`
--

CREATE TABLE `nm_procolor` (
  `pc_id` bigint(20) UNSIGNED NOT NULL,
  `pc_pro_id` int(10) UNSIGNED NOT NULL,
  `pc_co_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_procolor`
--

INSERT INTO `nm_procolor` (`pc_id`, `pc_pro_id`, `pc_co_id`) VALUES
(2, 2, 5),
(3, 2, 1),
(4, 2, 3),
(5, 2, 4),
(56, 37, 1),
(62, 40, 6),
(63, 40, 1),
(64, 41, 1),
(65, 42, 2),
(68, 45, 10),
(70, 46, 1),
(96, 48, 2),
(97, 48, 3),
(111, 3, 13),
(112, 3, 6),
(113, 3, 1),
(114, 3, 9),
(115, 3, 10),
(117, 4, 10),
(118, 50, 8),
(119, 50, 2),
(122, 52, 10),
(123, 52, 4),
(124, 53, 11),
(125, 53, 8),
(126, 53, 3),
(127, 57, 10),
(128, 57, 8),
(129, 57, 16),
(130, 58, 4),
(131, 58, 12),
(132, 58, 15),
(133, 59, 4),
(134, 59, 3),
(135, 59, 6),
(136, 59, 14),
(137, 59, 15),
(138, 60, 8),
(139, 60, 2),
(140, 62, 4),
(141, 62, 12),
(142, 63, 13),
(143, 63, 12),
(144, 63, 10),
(145, 63, 6),
(146, 63, 5),
(147, 64, 5),
(148, 65, 10),
(149, 65, 6),
(150, 66, 8),
(151, 66, 6),
(152, 66, 5),
(153, 66, 4),
(154, 66, 3),
(155, 66, 2),
(156, 66, 1),
(157, 67, 8),
(158, 67, 6),
(159, 67, 5),
(160, 67, 4),
(161, 67, 3),
(162, 67, 2),
(163, 67, 1),
(164, 68, 2),
(165, 68, 1),
(166, 69, 2),
(167, 69, 1),
(168, 70, 2),
(169, 70, 1),
(171, 72, 2),
(172, 72, 1),
(173, 73, 6),
(175, 71, 3),
(176, 74, 1),
(177, 75, 1),
(178, 76, 3),
(179, 76, 4),
(180, 76, 2),
(181, 76, 1),
(182, 77, 1),
(183, 78, 3),
(184, 79, 11),
(185, 79, 3),
(186, 80, 8),
(187, 81, 13),
(188, 82, 10),
(191, 85, 1),
(192, 86, 3),
(193, 86, 2),
(194, 86, 1),
(195, 87, 3),
(196, 87, 2),
(197, 87, 1),
(198, 88, 8),
(199, 89, 2),
(200, 89, 1),
(202, 91, 1),
(203, 91, 2),
(204, 92, 4),
(205, 92, 3),
(206, 92, 2),
(207, 92, 1),
(208, 93, 4),
(209, 93, 3),
(210, 93, 2),
(211, 93, 1),
(212, 94, 2),
(213, 94, 1),
(214, 95, 10),
(215, 95, 9),
(216, 95, 8),
(217, 95, 6),
(218, 95, 5),
(219, 95, 4),
(220, 95, 3),
(221, 95, 2),
(222, 95, 1),
(223, 96, 3),
(224, 96, 2),
(225, 96, 1),
(226, 97, 3),
(227, 97, 2),
(228, 97, 1),
(229, 98, 4),
(230, 98, 3),
(231, 99, 3),
(234, 101, 3),
(235, 101, 2),
(236, 101, 1),
(237, 102, 13),
(238, 103, 13),
(239, 104, 10),
(240, 104, 3),
(241, 105, 4),
(242, 105, 9),
(243, 106, 9),
(244, 106, 4),
(245, 106, 3),
(246, 107, 10),
(247, 107, 5),
(248, 108, 8),
(249, 108, 1),
(256, 109, 19),
(257, 109, 18),
(258, 109, 6),
(259, 110, 16),
(260, 111, 8),
(261, 111, 3),
(262, 111, 1),
(266, 47, 9),
(267, 47, 2),
(268, 47, 4),
(269, 100, 1),
(270, 100, 2),
(274, 83, 1),
(276, 84, 3),
(278, 112, 19),
(279, 90, 9),
(281, 2, 3),
(282, 2, 4),
(283, 3, 5),
(284, 3, 12),
(285, 4, 6),
(286, 4, 13),
(287, 4, 12),
(295, 1, 6),
(309, 6, 12),
(310, 5, 3),
(311, 5, 8),
(312, 5, 10),
(313, 5, 19),
(314, 5, 10),
(315, 7, 3),
(316, 8, 11),
(393, 13, 12),
(394, 14, 19),
(396, 15, 3),
(399, 16, 3),
(400, 16, 5),
(402, 19, 11),
(403, 20, 11),
(404, 20, 9),
(405, 25, 4),
(406, 25, 1),
(407, 25, 14),
(408, 26, 2),
(409, 26, 11),
(410, 27, 18),
(411, 27, 4),
(412, 27, 5),
(413, 27, 6),
(414, 24, 4),
(415, 24, 2),
(416, 24, 14),
(417, 21, 4),
(418, 21, 8),
(419, 21, 6),
(420, 21, 4),
(421, 21, 2),
(422, 21, 1),
(423, 23, 10),
(424, 23, 6),
(425, 23, 3),
(426, 23, 18),
(427, 35, 13),
(428, 35, 1),
(429, 36, 1),
(430, 36, 1),
(434, 30, 4),
(435, 30, 5),
(436, 30, 1),
(437, 30, 4),
(438, 31, 18),
(439, 31, 14),
(440, 31, 2),
(444, 32, 13),
(445, 32, 9),
(446, 32, 1),
(449, 9, 13),
(454, 33, 14),
(455, 33, 17),
(461, 10, 12),
(465, 11, 12),
(466, 40, 3),
(467, 17, 6),
(468, 17, 16),
(471, 41, 17),
(472, 42, 4),
(473, 22, 3),
(474, 22, 18),
(477, 29, 4),
(478, 29, 2),
(479, 39, 3),
(481, 43, 11),
(483, 44, 11),
(484, 45, 6),
(485, 46, 6),
(486, 47, 13),
(489, 34, 9),
(490, 34, 15),
(491, 34, 14),
(493, 28, 6),
(494, 28, 2),
(495, 48, 2),
(496, 48, 4),
(497, 48, 6),
(498, 48, 8),
(499, 48, 13),
(501, 18, 3),
(514, 38, 4),
(515, 38, 2),
(516, 38, 3),
(517, 49, 3),
(518, 50, 11),
(530, 51, 8),
(531, 51, 13),
(532, 51, 2),
(533, 51, 8),
(534, 52, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_prodelpolicy`
--

CREATE TABLE `nm_prodelpolicy` (
  `pdp_id` bigint(20) UNSIGNED NOT NULL,
  `pdp_pro_id` smallint(5) UNSIGNED NOT NULL,
  `pdp_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_product`
--

CREATE TABLE `nm_product` (
  `pro_no_of_purchase` int(11) NOT NULL,
  `pro_id` int(10) UNSIGNED NOT NULL,
  `pro_sku` varchar(16) NOT NULL,
  `pro_title` varchar(150) NOT NULL,
  `pro_mc_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pro_smc_id` smallint(5) UNSIGNED NOT NULL,
  `pro_sb_id` smallint(5) UNSIGNED NOT NULL,
  `pro_ssb_id` smallint(5) UNSIGNED NOT NULL,
  `pro_price` decimal(10,2) NOT NULL,
  `pro_disprice` decimal(10,2) NOT NULL,
  `pro_inctax` tinyint(4) NOT NULL,
  `pro_shippamt` decimal(10,2) NOT NULL,
  `pro_desc` text NOT NULL,
  `pro_isspec` tinyint(4) NOT NULL COMMENT '1-yes 2-no',
  `pro_delivery` smallint(5) UNSIGNED NOT NULL COMMENT 'in days',
  `pro_mr_id` int(10) UNSIGNED NOT NULL,
  `pro_sh_id` smallint(5) UNSIGNED NOT NULL,
  `pro_mkeywords` text NOT NULL,
  `pro_mdesc` text NOT NULL COMMENT 'metadescription',
  `pro_postfacebook` tinyint(4) NOT NULL,
  `pro_Img` varchar(500) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `pro_status` tinyint(4) NOT NULL COMMENT '1=> Active, 0 => Block',
  `pro_image_count` int(11) NOT NULL,
  `pro_qty` int(11) NOT NULL,
  `sold_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_product`
--

INSERT INTO `nm_product` (`pro_no_of_purchase`, `pro_id`, `pro_sku`, `pro_title`, `pro_mc_id`, `pro_smc_id`, `pro_sb_id`, `pro_ssb_id`, `pro_price`, `pro_disprice`, `pro_inctax`, `pro_shippamt`, `pro_desc`, `pro_isspec`, `pro_delivery`, `pro_mr_id`, `pro_sh_id`, `pro_mkeywords`, `pro_mdesc`, `pro_postfacebook`, `pro_Img`, `created_date`, `pro_status`, `pro_image_count`, `pro_qty`, `sold_status`) VALUES
(15, 17, 'NOK210220W12', 'Dora Canvas Shoes', 4, 6, 13, 16, '799.00', '599.00', 0, '0.00', '<h3>Dora DO1DGC1159 Canvas Shoes Price: Rs. 479</h3><div>Stylish, light in weight and soft from base, this Casual shoe pair is perfect for your li''l angel. Having classy eyelets and strong laces, the shoe will give ultimate comfort to your princess and enables her in Casual speedily.<br><h3>Key Features of Dora DO1DGC1159 Canvas Shoes</h3><ul><li>Dora Sequence Canvas</li><li>Canvas Shoes</li></ul><br></div>', 2, 2, 12, 18, 'Dora Canvas Shoes', 'Dora Canvas Shoes', 0, 'dora1x8bOmLY5.jpg/**/dora2MAwypKuu.jpg/**/dora3NENSeMIg.jpg/**//**/', '06/08/2016', 1, 3, 15, 0),
(14, 18, 'NOK210220W12', 'Lenovo A6000', 2, 2, 3, 7, '10000.00', '9500.00', 0, '0.00', '<h3>Key Features of Lenovo A6000</h3><ul><li>Android v5.1 (Lollipop) OS</li><li>13 MP Primary Camera</li><li>5 MP Secondary Camera</li><li>Dual Sim (LTE + LTE)</li><li>5.5 inch Capacitive Touchscreen</li><li>1.2 GHz Qualcomm Snapdragon 410 Quad Core Processor</li><li>Wi-Fi Enabled</li><li>Expandable Storage Capacity of 128 GB</li></ul>', 2, 5, 9, 23, 'Samsung Galaxy On7', 'Samsung Galaxy On7', 0, 'lenoa69pcx1UqA.jpg/**/lenoa62joycXNLk.jpg/**/lenoa63iRDHKKWo.jpg/**//**/', '06/08/2016', 1, 3, 15, 1),
(26, 22, 'NOK210220W12', 'LUCfashion Solid Men''s V-neck Maroon T-Shirt', 2, 2, 3, 9, '799.00', '30.00', 0, '0.00', '<div>Key Features of LUCfashion Solid Men''s V-neck Maroon T-Shirt Fit: Regular Fit Sleeve: Half Sleeve Fabric: 100% Cotton Color:Maroon Number Of Contents in Package:1</div>', 2, 8, 9, 23, 'xxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxx', 0, '13612bumz-lucfashion-m-400x400-imae6xv2k4h9g5pz.jpeg/**/13612bumz-lucfashion-xxl-400x400-imae6xv2myahydsk.jpeg/**/61Yg8h3JjML._UL1500_/**/asn305sko-12bum-lucfashion-400x400-imae5jzq8npa2vze.jpeg/**/', '06/08/2016', 1, 3, 20, 0),
(6, 23, 'NOK210220W12', 'LUCfashion Solid Men''s V-neck Green T-Shirt', 2, 2, 3, 9, '499.00', '60.00', 0, '0.00', 'LanosUC has designed this V-neck T-shirt in a versatile solid black shade to give you a cool look effortlessly. Crafted from&nbsp;pure cotton&nbsp;with half-sleeves, this trendy tee can be clubbed with a pair of slim worn effect pants and plimsolls.', 2, 6, 13, 6, 'xxxxxxxxxxxx', 'xxxxxxxxxxxxx', 0, 'Original-Penguin-Green-V-Neck-SDL556475312-1-b6f11yKmrU8HX.jpg/**/Original-Penguin-Green-V-Neck-SDL556475312-2-af117KhytXVQf.jpg/**/Original-Penguin-Green-V-Neck-SDL556475312-3-1be0b8iBQmwsa.jpg/**/', '06/08/2016', 1, 3, 5, 0),
(17, 24, 'NOK210220W12', 'Shoe Island Bab088-Tan-9 Casual Shoes', 2, 10, 15, 17, '499.00', '30.00', 0, '0.00', 'Hand-Made With Perfection. Class. Perfection. Relive. Experience The Exotic New Hand-Crafted Range Of #Shoeisland', 2, 8, 9, 4, 'xxxxxxxxx', 'xxxxxxxxx', 0, 'Shoe-Island-Tan-Synthetic-Leather-SDL530640140-1-48618gJt7ep3v.jpg/**/Shoe-Island-Tan-Synthetic-Leather-SDL530640140-2-7df84UCSyGWik.jpg/**/Shoe-Island-Tan-Synthetic-Leather-SDL530640140-3-a0773rGzJwjoG.jpg/**/', '06/08/2016', 1, 3, 10, 0),
(26, 25, 'NOK210220W12', 'Isole Beige Casual Shoes', 2, 10, 15, 17, '499.00', '20.00', 0, '0.00', 'Specifications of Isole Beige Casual Shoes Hand-Made With Perfection. Class. Perfection. Relive. Experience The Exotic New Hand-Crafted Range Of #Shoeisland', 2, 3, 9, 4, 'xxxxxxxxx', 'xxxxxxxxx', 0, 'Globalite-White-Canvas-Lace-Casual-SDL273015021-1-5d96bV8AuCQyk.jpg/**/Globalite-White-Canvas-Lace-Casual-SDL273015021-2-df6a73VF8LYOx.jpg/**/Globalite-White-Canvas-Lace-Casual-SDL273015021-3-690feE19lHGdm.jpg/**/', '06/08/2016', 1, 3, 5, 0),
(37, 26, 'NOK210220W12', 'CURREN New Fashion', 2, 11, 17, 18, '299.00', '20.00', 0, '0.00', 'Lorem ipsum dolor sit amet, mattis eros blandit, et consequat quis pede erat, per vestibulum nec proin lacus, arcu pede bibendum proin dui ipsum. Viverra quis eget accumsan tempor hendrerit, aut aenean ante ac posuere eget lorem, condimentum quam eu id. Ut in natoque vestibulum integer, odio ut a irure vulputate, et consectetuer eget, ut lacus eget pede aenean aliquip, vitae ligula id ut vitae mauris vestibulum. Sodales nullam vitae dolor posuere mollis nulla, cubilia sagittis, tortor commodo ac tempor fringilla mollis fermentum, consectetuer amet cillum ac amet habitant posuere. Tristique semper. Id sem non felis, rutrum sed magna pede convallis. Ac ut dolor lacus suscipit dolor, suspendisse erat.', 2, 6, 9, 4, 'xxxxx', 'xxxxxx', 0, 'Curren-Brown-Leather-Analog-Watch-SDL118668689-1-8816akQl5KTGb.jpg/**/Curren-Brown-Leather-Analog-Watch-SDL118668689-2-8188bGqv2hEoG.jpg/**/Curren-Brown-Leather-Analog-Watch-SDL118668689-3-338c2CE51pu0R.jpg/**/', '06/08/2016', 1, 3, 30, 0),
(7, 27, 'NOK210220W12', 'Vintex CR-01 Curren Analog Watch - For Boys, Men', 2, 11, 17, 18, '399.00', '30.00', 0, '0.00', 'Lorem ipsum dolor sit amet, mattis eros blandit, et consequat quis pede erat, per vestibulum nec proin lacus, arcu pede bibendum proin dui ipsum. Viverra quis eget accumsan tempor hendrerit, aut aenean ante ac posuere eget lorem, condimentum quam eu id. Ut in natoque vestibulum integer, odio ut a irure vulputate, et consectetuer eget, ut lacus eget pede aenean aliquip, vitae ligula id ut vitae mauris vestibulum. Sodales nullam vitae dolor posuere mollis nulla, cubilia sagittis, tortor commodo ac tempor fringilla mollis fermentum, consectetuer amet cillum ac amet habitant posuere. Tristique semper. Id sem non felis, rutrum sed magna pede convallis. Ac ut dolor lacus suscipit dolor, suspendisse erat.', 2, 6, 9, 4, 'xxx', 'xxx', 0, 'Curren-Beige-Analog-Watch-SDL611647344-1-d78e6e7t6x4OB.jpg/**/Curren-Beige-Analog-Watch-SDL611647344-2-0e2a9sxJZkpbU.jpg/**/Curren-Beige-Analog-Watch-SDL611647344-3-37535pC0lFFJO.jpg/**/', '06/08/2016', 1, 3, 6, 1),
(16, 28, 'NOK210220W12', 'Sai Fabrics Casual Self Design, Solid Women''s Kurti', 3, 4, 6, 3, '240.00', '15.00', 0, '0.00', 'Lorem ipsum dolor sit amet, mattis eros blandit, et consequat quis pede erat, per vestibulum nec proin lacus, arcu pede bibendum proin dui ipsum. Viverra quis eget accumsan tempor hendrerit, aut aenean ante ac posuere eget lorem, condimentum quam eu id. Ut in natoque vestibulum integer, odio ut a irure vulputate, et consectetuer eget, ut lacus eget pede aenean aliquip, vitae ligula id ut vitae mauris vestibulum. Sodales nullam vitae dolor posuere mollis nulla, cubilia sagittis, tortor commodo ac tempor fringilla mollis fermentum, consectetuer amet cillum ac amet habitant posuere. Tristique semper. Id sem non felis, rutrum sed magna pede convallis. Ac ut dolor lacus suscipit dolor, suspendisse erat.', 2, 9, 9, 23, 'xxxxx', 'xxxxxxthtgh', 0, 'kblack-tanvi-400x400-imaec2ehdafnsbbe.jpeg/**/kblack-tanvi-400x400-imaec2egm96sbdz9.jpeg/**/ptf038-jaytextile-m-400x400-imaec2egteha4gkx.jpeg/**/', '06/08/2016', 1, 2, 15, 0),
(31, 29, 'NOK210220W12', 'Sai Fabrics Casual Self Design Women''s Kurti', 3, 4, 6, 3, '240.00', '10.00', 0, '0.00', 'Lorem ipsum dolor sit amet, mattis eros blandit, et consequat quis pede erat, per vestibulum nec proin lacus, arcu pede bibendum proin dui ipsum. Viverra quis eget accumsan tempor hendrerit, aut aenean ante ac posuere eget lorem, condimentum quam eu id. Ut in natoque vestibulum integer, odio ut a irure vulputate, et consectetuer eget, ut lacus eget pede aenean aliquip, vitae ligula id ut vitae mauris vestibulum. Sodales nullam vitae dolor posuere mollis nulla, cubilia sagittis, tortor commodo ac tempor fringilla mollis fermentum, consectetuer amet cillum ac amet habitant posuere. Tristique semper. Id sem non felis, rutrum sed magna pede convallis. Ac ut dolor lacus suscipit dolor, suspendisse erat.', 2, 8, 9, 23, 'xxxx', 'xxxx', 0, 'Sai-Fabrics-Beige-Cotton-Kurti-SDL837839685-1-ab9b6LNsyL2Zs.jpg/**/Sai-Fabrics-Beige-Cotton-Kurti-SDL837839685-2-b0a57yRcN18Qx.jpg/**/Sai-Fabrics-Beige-Cotton-Kurti-SDL837839685-3-d5e18jaagwxYp.jpg/**//**/', '06/08/2016', 1, 3, 20, 1),
(23, 30, 'NOK210220W12', 'THE WOOL HOUSE Women''s Red, Pink, Yellow, Black, White, Light Blue Leggings', 3, 7, 10, 15, '625.00', '10.00', 0, '0.00', 'Lorem ipsum dolor sit amet, mattis eros blandit, et consequat quis pede erat, per vestibulum nec proin lacus, arcu pede bibendum proin dui ipsum. Viverra quis eget accumsan tempor hendrerit, aut aenean ante ac posuere eget lorem, condimentum quam eu id. Ut in natoque vestibulum integer, odio ut a irure vulputate, et consectetuer eget, ut lacus eget pede aenean aliquip, vitae ligula id ut vitae mauris vestibulum. Sodales nullam vitae dolor posuere mollis nulla, cubilia sagittis, tortor commodo ac tempor fringilla mollis fermentum, consectetuer amet cillum ac amet habitant posuere. Tristique semper. Id sem non felis, rutrum sed magna pede convallis. Ac ut dolor lacus suscipit dolor, suspendisse erat.', 2, 3, 12, 5, 'xxxxx', 'xxx', 0, 'GOPPS-Yellow-Woolen-Leggings-SDL635969879-1-51217TPduHVfu.jpg/**/GOPPS-Yellow-Woolen-Leggings-SDL635969879-3-13f9faRSdTgVC.jpg/**/GOPPS-Yellow-Woolen-Leggings-SDL635969879-2-22a6dnfckIZp1.jpg/**/', '06/08/2016', 1, 3, 10, 0),
(12, 31, 'NOK210220W12', 'Greyon Women''s Multicolor Leggings', 3, 7, 10, 15, '550.00', '20.00', 0, '0.00', 'Lorem ipsum dolor sit amet, mattis eros blandit, et consequat quis pede erat, per vestibulum nec proin lacus, arcu pede bibendum proin dui ipsum. Viverra quis eget accumsan tempor hendrerit, aut aenean ante ac posuere eget lorem, condimentum quam eu id. Ut in natoque vestibulum integer, odio ut a irure vulputate, et consectetuer eget, ut lacus eget pede aenean aliquip, vitae ligula id ut vitae mauris vestibulum. Sodales nullam vitae dolor posuere mollis nulla, cubilia sagittis, tortor commodo ac tempor fringilla mollis fermentum, consectetuer amet cillum ac amet habitant posuere. Tristique semper. Id sem non felis, rutrum sed magna pede convallis. Ac ut dolor lacus suscipit dolor, suspendisse erat.', 2, 8, 12, 5, 'xxxx', 'xxxx', 0, 'Greenwich-Multi-Color-Cotton-Women-SDL089603320-1-577b9FpUEB6DY.jpg/**/Greenwich-Multi-Color-Cotton-Women-SDL089603320-2-b9bb8MhfZJESl.jpg/**/Greenwich-Multi-Color-Cotton-Women-SDL089603320-3-81a01NQBKeR6u.jpg/**/', '06/08/2016', 1, 3, 6, 0),
(27, 32, 'NOK210220W12', 'Crocs Baby Boys, Baby Girls Sandals', 4, 6, 13, 14, '1795.00', '199.00', 0, '0.00', 'Weve updated a favorite with a new look, but kept the same Crocs comfort inside. Slip into the bold new Kids Crocband II.5 from Crocs. Kids Crocband II.5 Clog Details: Updated midsole band design. Odor-resistant, easy to clean and quick to dry. Advanced toe box ventilation for breathability. Lightweight, non-marking soles. Water-friendly and buoyant; weighs only ounces. Fully molded Croslite material for lightweight cushioning and comfort. Croslite material heel strap for a secure fit.<br>', 2, 5, 12, 5, 'xxxx', 'xxxxx', 0, 'Crocs-Red-Clogs-SDL758501417-2-803c0LUJpekaF.jpg/**/Crocs-Red-Clogs-SDL758501417-1-455baxipQws7t.jpg/**/Crocs-Red-Clogs-SDL758501417-3-07b2dxuI3Bess.jpg/**/', '06/08/2016', 1, 3, 50, 1),
(40, 34, 'NOK210220W12', 'Cutecumber Party Short Sleeve Printed Baby Girl''s Red, White Top', 4, 6, 18, 19, '307.00', '50.00', 0, '0.00', 'Short Sleeve,&nbsp;Pack of 1&nbsp;Knit&nbsp;Printed&nbsp;Baby Girl''s&nbsp;Party dresses', 2, 2, 9, 23, 'xxxxx', 'xxxx', 0, '612-League-Red-White-Printed-SDL234360569-1-e4d58AKg3nUYO.jpg/**/612-League-Red-White-Printed-SDL234360569-2-f44a5WCy9xvCT.jpg/**/612-League-Red-White-Printed-SDL234360569-3-f91d2Lnuvhbuj.jpg/**//**/', '06/08/2016', 1, 3, 50, 1),
(9, 35, 'NOK210220W12', 'Globalite Stumble Walking Shoes', 2, 10, 15, 17, '599.00', '299.00', 0, '0.00', 'Cute Walk Shoes by Babyhug meant for precious little feet. A range of super stylish &amp; comfortable footwear, fine crafted from top quality material. Materials and components are carefully chosen for their quality and lightness, ensuring that no shoes are heavier than the proportional age appropriate weight for the child''s body weight.', 2, 2, 9, 4, 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 0, 'Globalite-Stumble-Black-Grey-Men-SDL996600935-1-2a717ksgE6anF.jpg/**/Globalite-Stumble-Black-Grey-Men-SDL996600935-2-9b815LHDnHWfW.jpg/**/Globalite-Stumble-Black-Grey-Men-SDL996600935-3-f14b1qHOHUN3d.jpg/**/', '06/10/2016', 1, 3, 2, 0),
(2, 36, 'NOK210220W12', 'Reebok Court Canvas Shoes', 2, 10, 15, 17, '1722.00', '599.00', 0, '0.00', 'Cute Walk Shoes by Babyhug meant for precious little feet. A range of super stylish &amp; comfortable footwear, fine crafted from top quality material. Materials and components are carefully chosen for their quality and lightness, ensuring that no shoes are heavier than the proportional age appropriate weight for the child''s body weight.', 2, 2, 12, 5, 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 0, 'Reebok-On-Court-Iv-Navy-SDL696561302-1-6ca21IPQtSgRU.jpg/**/Reebok-On-Court-Iv-Navy-SDL696561302-2-c8f796Nytmj96.jpg/**/Reebok-On-Court-Iv-Navy-SDL696561302-4-a163eGykzfYYO.jpg/**/', '06/10/2016', 1, 3, 2, 1),
(26, 37, 'NOK210220W12', 'Lynda Casual 3/4 Sleeve Floral Print Women''s Black Top', 3, 7, 10, 4, '1299.00', '699.00', 0, '0.00', '<h4>Lynda Casual 3/4 Sleeve Floral Print Women''s Black Top Price: Rs. 699</h4><div>Lynda black floral print shirt<br><br><br></div>', 2, 4, 9, 4, 'Print Women''s Black Top', 'Print Women''s Black Top', 0, 'tops1VvjRdFfI.jpg/**/tops3xqetccZq.jpg/**/tops2zmOlK39u.jpg/**/', '06/08/2016', 1, 2, 50, 1),
(51, 38, 'NOK210220W12', 'Lenovo A6000 Special Price', 6, 26, 39, 59, '500.00', '400.00', 0, '0.00', '<h3>Key Features of Lenovo A6000</h3><ul><li>Android v5.1 (Lollipop) OS</li><li>13 MP Primary Camera</li><li>5 MP Secondary Camera</li><li>Dual Sim (LTE + LTE)</li><li>5.5 inch Capacitive Touchscreen</li><li>1.2 GHz Qualcomm Snapdragon 410 Quad Core Processor</li><li>Wi-Fi Enabled</li><li>Expandable Storage Capacity of 128 GB</li></ul><br>', 2, 3, 19, 12, 'Special Price Lenovo A6000', 'Special Price Lenovo A6000', 0, 'Lenovo-A6000-WHITE.jpg/**//**/', '08/30/2016', 1, 1, 500, 1),
(25, 43, 'NOK210220W12', 'HP 15-AC167TU Notebook', 6, 16, 24, 38, '22399.00', '22000.00', 0, '0.00', '<div><ul><li>The Best offer for you will be applied directly in the cart</li></ul></div><div><div><div><div><div></div><div><div><div><ul><li>Extra 10% Off on purchases made through All Credit/Debit Cards<a href="https://www.snapdeal.com/offers/bank-offer-nov" target="_blank" rel="">View T&amp;C</a></li></ul></div></div></div></div></div></div><div><div><div><div></div><div><div><div><ul><li>3X Reward Points, Redeemable on Snapdeal with Snapdeal HDFC Credit Card.<a href="https://www.snapdeal.com/offers/sd-hdfc-card" target="_blank" rel="">View T&amp;C</a></li></ul></div></div></div></div></div></div><div><ol></ol><div><ul><li>5% Savings with HDFC Bank Rewards Debit Card.<a href="http://www.hdfcbank.com/personal/products/cards/debit-cards/hdfc-bank-rewards-debit-card" target="_blank" rel="">View T&amp;C</a></li></ul><div><br></div></div></div></div>', 2, 3, 20, 13, 'HP 15-AC167TU Notebook (P4Y38PA) ', 'HP 15-AC167TU Notebook (P4Y38PA) ', 0, 'HP-15-AC167TU-Notebook-P4Y38PA-SDL793038580-1-a64b6.jpg/**/HP-15-AC167TU-Notebook-P4Y38PA-SDL793038580-2-f4768.jpg/**/', '11/11/2016', 1, 1, 50, 1),
(34, 44, 'NOK210220W12', 'Nikon D5200', 6, 24, 38, 58, '37000.00', '37000.00', 0, '0.00', '<ol><li><span class="wysiwyg-color-red">See the world with more clarity with Nikon D5200. Make memories with this amazing piece of technology, it enables you to shoot from about any position. This camera comes with a built in HDR (high-dynamic range) – which helps take shots in both dark as well as bright settings. It also offers a plethora of filters at your disposal. So go clicking like a pro.</span></li></ol>', 2, 3, 19, 12, 'Nikon D5200', 'Nikon D5200', 0, 'Nikon-D5200-with-18-55mm-SDL193116648-1-de08f.jpg/**/Nikon-D5200-with-18-55mm-SDL193116648-2-be8f3.jpg/**/', '11/11/2016', 1, 1, 50, 1),
(0, 45, 'NOK210220W12', 'Elite Sofa Cum Bed', 5, 14, 23, 36, '14000.00', '12000.00', 0, '0.00', 'Elite Sofa Bed is very comfortable and modern designed. It has been specially designed to provide the most comfort while sitting and sleeping. Its strong and long lasting teak wood frame construction makes it super strong and sturdy for long time use. Its beautiful upholstery adds a different dimension to the overall decor of any area. With a fine quality finish, this sofa cum bed is perfect for those extra guests or friends to sleep with comfort.', 2, 5, 13, 6, 'Elite Sofa Cum Bed', 'Elite Sofa Cum Bed', 0, 'Dolphins-Elite-Fabric-Sofa-cum-SDL566288795-2-19ec8.jpg/**/Dolphins-Elite-Fabric-Sofa-cum-SDL566288795-1-9a877.jpg/**/', '11/11/2016', 1, 1, 20, 1),
(0, 46, 'NOK210220W12', 'Spiderman Comic Book', 9, 20, 33, 48, '450.00', '400.00', 0, '0.00', 'The Complete Story of spider man''s Life.&nbsp;', 2, 3, 12, 5, 'Spiderman Comic Book', 'Spiderman Comic Book', 0, 'Free_Comic_Book_Day_Vol_2007_Spider-Man.jpg/**/', '11/11/2016', 1, 0, 100, 1),
(0, 47, 'NOK210220W12', 'test new test ', 2, 2, 3, 7, '5500.00', '5.00', 0, '0.00', 'test', 2, 2, 9, 23, 'test', 'test', 0, '9410135-Interior-design-of-modern-apartment-living-room-hall-kitchen-3d-render-Stock-PhotoVIWvhVEH.jpg/**/', '11/14/2016', 1, 0, 5, 1),
(0, 48, 'NOK210220W12', 'test new test ', 2, 10, 15, 17, '1500.00', '1.00', 0, '0.00', 'test', 2, 2, 9, 14, 't', 't', 0, '1.png/**/', '11/14/2016', 1, 0, 10, 1),
(0, 49, 'NOK210220W12', 'Apple iPad Mini 2 Retina Display Wifi + Cellular - 16GB - Silver', 6, 26, 40, 60, '500.00', '200.00', 0, '0.00', 'iPad Mini memiliki layar Retina 7,9 inci yang menakjubkan dengan\r\nlebih dari 3,1 juta piksel. Dilengkapi juga dengan chip A7 dengan\r\narsitektur 64-bit, ultrafast nirkabel, iSight dan FaceTime dengan\r\nkamera HD, aplikasi yang kuat, dan hingga 10 jam masa pakai\r\nbaterai. Setiap detail menakjubkan. <br>', 1, 2, 33, 32, 'apple', 'tablet', 0, 'apple-ipad-mini-2-retina-display-wifi-cellular-16gb-silver-3234-9243084-b1a841710c0d15dd433cf625db0af7e5-zoomUwwoVQFJ.jpg/**/', '01/17/2017', 1, 0, 27, 1),
(0, 50, 'NOK210220W12', 'Sendals', 2, 10, 15, 17, '300000.00', '299999.00', 127, '0.00', 'Sendals serba guna<br>', 2, 200, 36, 35, 'sendal', 'sendal', 0, 'sendal-jepitoqGRDmml.png/**/', '02/24/2017', 1, 0, 5, 1),
(0, 51, 'NOK210220W12', 'Baju Koko', 2, 2, 3, 7, '199900.00', '199900.00', 100, '6000.00', 'qwertyuiop asdfghjkl asdfghjkl zxcvbnm qwertyuiop asdfghjkl zxcvbnm qwertyuiop asdfghjkl zxcvbnm&nbsp;', 1, 10, 9, 1, 'qwertyuiop qwertyuiop qwertyuiop qwertyuiop qwertyuiop', 'qwertyuiop qwertyuiop', 0, 'index.jpg/**/', '02/28/2017', 1, 0, 10, 1),
(0, 52, 'NOK210220W12', 'Baju Daster', 9, 20, 33, 48, '90000.00', '1000.00', 127, '0.00', 'qwertyuiop', 2, 7, 39, 38, 'wertyuiop', 'qwertyuiop', 0, 'grosir-baju-daster-murah-140x230.jpg/**/', '02/28/2017', 1, 0, 100, 1),
(0, 53, 'NOK210807K12', 'Handphone Nokia qwerty Hitam B', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 54, 'NOK210679K12', 'Handphone Nokia asdfg Hitam B', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 55, 'NOK210890K13', 'Handphone Nokia 12345 Hitam C', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 56, 'NOK210890K13', 'Handphone Nokia 12345 Hitam C', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 57, 'NOK210890K13', 'Handphone Nokia 12345 Hitam C', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 58, 'NOK210890K13', 'Handphone Nokia 12345 Hitam C', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 59, 'NOK210890K13', 'Handphone Nokia 12345 Hitam C', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1),
(0, 60, 'NOK210201R12', 'Handphone Nokia HW aaa Merah Tua B', NULL, 0, 0, 26, '0.00', '0.00', 0, '0.00', '', 0, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_prosize`
--

CREATE TABLE `nm_prosize` (
  `ps_id` bigint(20) UNSIGNED NOT NULL,
  `ps_pro_id` int(10) UNSIGNED NOT NULL,
  `ps_si_id` smallint(5) UNSIGNED NOT NULL,
  `ps_volume` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_prosize`
--

INSERT INTO `nm_prosize` (`ps_id`, `ps_pro_id`, `ps_si_id`, `ps_volume`) VALUES
(9, 2, 1, 1),
(50, 37, 1, 1),
(53, 40, 1, 1),
(54, 41, 1, 1),
(55, 42, 1, 1),
(58, 45, 4, 1),
(61, 46, 2, 1),
(79, 48, 5, 1),
(89, 3, 4, 1),
(90, 3, 1, 1),
(91, 3, 3, 1),
(93, 4, 1, 1),
(94, 50, 6, 1),
(95, 50, 8, 1),
(98, 57, 4, 1),
(99, 57, 6, 1),
(100, 57, 1, 1),
(101, 58, 6, 1),
(102, 58, 3, 1),
(103, 58, 2, 1),
(104, 59, 3, 1),
(105, 59, 1, 1),
(106, 59, 9, 1),
(107, 59, 8, 1),
(108, 59, 7, 1),
(109, 59, 2, 1),
(110, 59, 6, 1),
(111, 60, 1, 1),
(112, 60, 3, 1),
(113, 62, 4, 1),
(114, 63, 9, 1),
(115, 63, 7, 1),
(116, 63, 1, 1),
(117, 63, 2, 1),
(118, 63, 4, 1),
(119, 63, 5, 1),
(120, 64, 1, 1),
(121, 64, 6, 1),
(122, 65, 5, 1),
(123, 65, 3, 1),
(124, 66, 4, 1),
(125, 66, 3, 1),
(126, 66, 2, 1),
(127, 66, 1, 1),
(128, 67, 6, 1),
(129, 67, 5, 1),
(130, 67, 4, 1),
(131, 67, 3, 1),
(132, 67, 2, 1),
(133, 67, 1, 1),
(134, 68, 2, 1),
(135, 68, 1, 1),
(136, 69, 2, 1),
(137, 69, 1, 1),
(138, 70, 2, 1),
(139, 70, 1, 1),
(142, 72, 2, 1),
(143, 72, 1, 1),
(144, 73, 4, 1),
(147, 71, 8, 1),
(148, 71, 4, 1),
(149, 74, 1, 1),
(150, 75, 1, 1),
(151, 76, 2, 1),
(152, 76, 1, 1),
(153, 77, 1, 1),
(154, 78, 8, 1),
(155, 78, 1, 1),
(156, 79, 4, 1),
(157, 80, 7, 1),
(158, 81, 2, 1),
(159, 82, 4, 1),
(162, 85, 1, 1),
(163, 86, 1, 1),
(164, 87, 2, 1),
(165, 87, 1, 1),
(166, 88, 2, 1),
(167, 89, 1, 1),
(169, 91, 3, 1),
(170, 91, 2, 1),
(171, 91, 1, 1),
(172, 92, 2, 1),
(173, 92, 1, 1),
(174, 93, 2, 1),
(175, 93, 1, 1),
(176, 94, 2, 1),
(177, 94, 1, 1),
(178, 95, 9, 1),
(179, 95, 8, 1),
(180, 95, 7, 1),
(181, 95, 6, 1),
(182, 95, 5, 1),
(183, 95, 4, 1),
(184, 95, 3, 1),
(185, 95, 2, 1),
(186, 95, 1, 1),
(187, 96, 5, 1),
(188, 96, 4, 1),
(189, 96, 3, 1),
(190, 96, 2, 1),
(191, 96, 1, 1),
(192, 97, 3, 1),
(193, 97, 2, 1),
(194, 97, 1, 1),
(195, 98, 9, 1),
(196, 98, 8, 1),
(197, 98, 6, 1),
(198, 98, 4, 1),
(199, 98, 7, 1),
(200, 98, 5, 1),
(201, 98, 2, 1),
(202, 98, 3, 1),
(203, 98, 1, 1),
(204, 99, 4, 1),
(209, 101, 4, 1),
(210, 101, 3, 1),
(211, 101, 2, 1),
(212, 101, 1, 1),
(213, 102, 3, 1),
(214, 102, 2, 1),
(215, 102, 1, 1),
(216, 103, 3, 1),
(217, 103, 2, 1),
(218, 103, 1, 1),
(219, 104, 5, 1),
(220, 104, 4, 1),
(221, 104, 3, 1),
(222, 104, 2, 1),
(223, 104, 1, 1),
(224, 105, 9, 1),
(225, 105, 8, 1),
(226, 105, 7, 1),
(227, 105, 6, 1),
(228, 105, 5, 1),
(229, 105, 2, 1),
(230, 105, 3, 1),
(231, 105, 4, 1),
(232, 105, 1, 1),
(233, 106, 9, 1),
(234, 106, 8, 1),
(235, 106, 7, 1),
(236, 106, 6, 1),
(237, 106, 5, 1),
(238, 106, 2, 1),
(239, 106, 3, 1),
(240, 106, 4, 1),
(241, 106, 1, 1),
(242, 107, 5, 1),
(243, 107, 8, 1),
(244, 107, 9, 1),
(245, 107, 7, 1),
(246, 107, 6, 1),
(247, 107, 4, 1),
(248, 107, 3, 1),
(249, 107, 2, 1),
(250, 107, 1, 1),
(251, 108, 3, 1),
(261, 109, 1, 1),
(262, 110, 3, 1),
(263, 111, 7, 1),
(264, 111, 5, 1),
(268, 47, 2, 1),
(269, 47, 4, 1),
(270, 47, 8, 1),
(272, 100, 1, 1),
(273, 100, 2, 1),
(274, 100, 3, 1),
(275, 100, 4, 1),
(279, 83, 1, 1),
(281, 84, 3, 1),
(284, 112, 3, 1),
(285, 112, 6, 1),
(286, 54, 3, 1),
(289, 90, 1, 1),
(293, 2, 6, 1),
(294, 2, 8, 1),
(295, 2, 7, 1),
(296, 3, 8, 1),
(297, 3, 7, 1),
(298, 4, 10, 1),
(299, 4, 9, 1),
(300, 4, 7, 1),
(301, 4, 6, 1),
(313, 1, 7, 1),
(314, 1, 8, 1),
(315, 1, 1, 1),
(329, 6, 8, 1),
(330, 5, 4, 1),
(331, 5, 5, 1),
(332, 5, 8, 1),
(333, 5, 7, 1),
(334, 5, 1, 1),
(335, 7, 10, 1),
(336, 8, 10, 1),
(463, 13, 6, 1),
(464, 13, 4, 1),
(465, 14, 10, 1),
(468, 15, 1, 1),
(469, 15, 10, 1),
(471, 16, 1, 1),
(472, 16, 1, 1),
(475, 19, 10, 1),
(476, 19, 1, 1),
(477, 20, 2, 1),
(478, 20, 3, 1),
(479, 25, 1, 1),
(480, 25, 4, 1),
(481, 26, 7, 1),
(482, 26, 3, 1),
(483, 27, 1, 1),
(484, 27, 5, 1),
(485, 27, 4, 1),
(486, 27, 6, 1),
(487, 24, 2, 1),
(488, 24, 5, 1),
(489, 21, 4, 1),
(490, 21, 1, 1),
(491, 21, 2, 1),
(492, 21, 3, 1),
(493, 23, 3, 1),
(494, 23, 2, 1),
(495, 23, 4, 1),
(496, 35, 3, 1),
(497, 35, 1, 1),
(498, 36, 2, 1),
(499, 36, 1, 1),
(502, 30, 5, 1),
(503, 30, 4, 1),
(504, 31, 3, 1),
(505, 31, 3, 1),
(506, 31, 7, 1),
(509, 32, 1, 1),
(510, 32, 5, 1),
(511, 32, 4, 1),
(514, 9, 13, 1),
(515, 9, 1, 1),
(516, 9, 10, 1),
(520, 12, 1, 1),
(521, 12, 10, 1),
(522, 12, 2, 1),
(527, 33, 5, 1),
(528, 33, 1, 1),
(534, 10, 10, 1),
(535, 10, 1, 1),
(546, 11, 7, 1),
(547, 11, 6, 1),
(548, 11, 5, 1),
(549, 40, 2, 1),
(550, 17, 1, 1),
(553, 41, 2, 1),
(554, 42, 3, 1),
(555, 22, 3, 1),
(556, 22, 2, 1),
(557, 22, 4, 1),
(561, 29, 6, 1),
(562, 29, 5, 1),
(563, 39, 3, 1),
(565, 43, 2, 1),
(567, 44, 5, 1),
(568, 45, 2, 1),
(569, 46, 2, 1),
(570, 47, 2, 1),
(574, 34, 3, 1),
(576, 28, 3, 1),
(577, 28, 3, 1),
(578, 28, 7, 1),
(579, 48, 2, 1),
(580, 48, 4, 1),
(581, 48, 12, 1),
(582, 48, 11, 1),
(583, 48, 9, 1),
(584, 48, 7, 1),
(585, 48, 5, 1),
(586, 48, 3, 1),
(588, 18, 2, 1),
(597, 38, 7, 1),
(598, 49, 2, 1),
(599, 50, 6, 1),
(600, 50, 5, 1),
(601, 50, 8, 1),
(613, 51, 2, 1),
(614, 51, 12, 1),
(615, 51, 7, 1),
(616, 51, 8, 1),
(617, 52, 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_prospec`
--

CREATE TABLE `nm_prospec` (
  `spc_id` bigint(20) UNSIGNED NOT NULL,
  `spc_pro_id` int(10) UNSIGNED NOT NULL,
  `spc_sp_id` smallint(5) UNSIGNED NOT NULL,
  `spc_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_prospec`
--

INSERT INTO `nm_prospec` (`spc_id`, `spc_pro_id`, `spc_sp_id`, `spc_value`) VALUES
(15, 3, 1, ''),
(18, 55, 1, ''),
(19, 56, 1, 'cool wear'),
(20, 57, 1, 'comfort'),
(21, 58, 1, 'comfort'),
(22, 59, 1, 'good'),
(23, 61, 1, 'sofa material'),
(24, 63, 1, 'sofa specification'),
(25, 68, 1, 'material'),
(26, 79, 1, 'SOFA'),
(27, 80, 1, 'comfort'),
(29, 54, 1, 'Good'),
(39, 12, 2, '3/4 Sleeve'),
(40, 45, 2, '2'),
(41, 45, 5, '10'),
(42, 28, 1, 'dd'),
(43, 28, 1, 'dd'),
(44, 28, 1, 'dd'),
(45, 48, 6, 't'),
(46, 48, 7, 't'),
(47, 48, 1, 't'),
(53, 49, 8, '7 inch'),
(56, 51, 8, '24'),
(57, 51, 5, '10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_referaluser`
--

CREATE TABLE `nm_referaluser` (
  `ruse_id` int(10) UNSIGNED NOT NULL,
  `ruse_date` datetime NOT NULL,
  `ruse_name` varchar(100) NOT NULL,
  `ruse_emailid` varchar(150) NOT NULL,
  `ruse_userid` int(10) UNSIGNED NOT NULL,
  `ruse_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_review`
--

CREATE TABLE `nm_review` (
  `comment_id` int(50) NOT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `deal_id` varchar(255) DEFAULT NULL,
  `store_id` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `ratings` varchar(255) NOT NULL,
  `status` int(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sam1` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_review`
--

INSERT INTO `nm_review` (`comment_id`, `product_id`, `deal_id`, `store_id`, `customer_id`, `title`, `comments`, `ratings`, `status`, `created_date`, `sam1`) VALUES
(1, '69', NULL, NULL, '19', 'Super', 'Nice Products kids denim trousers', '3', 0, '2016-10-19 11:48:34', 0),
(10, '62', NULL, NULL, '19', 'BEAUTIFUL PRODUCT.', 'kids denim trousers is nice product', '1', 0, '2016-06-02 18:07:17', 0),
(13, '74', NULL, NULL, '19', 'Cray Cloths', 'Nice Products', '1', 0, '2016-06-02 19:07:17', 0),
(14, '62', NULL, NULL, '19', 'Nice Products', 'Nice Products thursday', '2', 0, '2016-06-03 12:28:50', 0),
(17, NULL, '20', NULL, '19', 'Deal for week nice vv', 'Nice product when will launch cvcvcv.', '1', 1, '2016-10-19 14:02:09', 0),
(18, '75', NULL, NULL, '19', 'Nice Products', 'Good Product make it easy.', '1', 0, '2016-06-03 13:40:40', 0),
(19, '75', NULL, NULL, '19', 'Nice Prd', 'Prd Nice', '2', 0, '2016-06-02 15:53:04', 0),
(22, '5', NULL, NULL, '19', 'Short Sleeve Graphic Print', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '3', 0, '2016-06-04 06:41:49', 0),
(23, '5', NULL, NULL, '15', ' Women''s White Top', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo?', '1', 0, '2016-06-04 06:43:58', 0),
(24, NULL, '26', NULL, '15', 'Quality and cost', 'Worth product for this Cost', '4', 1, '2016-10-19 13:32:08', 0),
(25, NULL, '27', NULL, '15', 'Nice Deal', 'This is a very good deal', '3', 1, '2016-10-19 13:33:12', 0),
(26, NULL, '27', NULL, '15', 'fgfhfhfgh', 'hgfhgfhgfhgfhgfhfgh', '2', 1, '2016-10-19 13:34:57', 0),
(27, '1', NULL, NULL, '19', 'Nice Shirts', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', '1', 0, '2016-06-06 12:37:10', 0),
(28, NULL, '26', NULL, '19', 'Nice Deals', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris', '1', 0, '2016-06-06 12:57:43', 0),
(31, '9', NULL, NULL, '23', 'Nice Product', 'It is worthful pproduct for this cost', '2', 0, '2016-06-08 11:39:22', 0),
(32, '10', NULL, NULL, '23', 'I like this product', 'Attractive color and best materiel.', '4', 0, '2016-06-08 11:49:55', 0),
(33, NULL, '2', NULL, '23', 'Best for cost', 'looking nice quality', '3', 0, '2016-06-08 12:10:46', 0),
(34, '15', NULL, NULL, '25', 'Lenovo K3 Note', 'Nice Products', '1', 1, '2017-02-28 03:06:51', 0),
(35, NULL, '45', NULL, '23', 'Nice deal.', 'It is very good deal, i will recommend to my friends', '3', 0, '2016-06-11 05:00:53', 0),
(37, NULL, NULL, '1', '25', 'Today', 'Nice Products good sales', '2', 0, '2016-06-11 07:26:19', 0),
(41, '14', NULL, NULL, '25', 'Lenovo', 'Nice Products', '2', 0, '2016-06-11 07:55:24', 0),
(44, NULL, '3', NULL, '31', 'Deal product', 'Nice camera', '1', 0, '2016-06-11 08:25:36', 0),
(45, '13', NULL, NULL, '42', 'Nice Cloths', 'hdsghsdfhsd', '1', 0, '2016-07-12 13:03:44', 0),
(46, '13', NULL, NULL, '82', 'Legens Top', 'Nice Tops and Best price', '1', 0, '2016-07-12 13:12:45', 0),
(47, '23', NULL, NULL, '28', 'Best Product', 'This T- Shirt is best product. Its have good quality and design', '5', 0, '2016-08-11 10:53:58', 0),
(48, '23', NULL, NULL, '99', 'Good Product', 'This product''s good quality and design.', '5', 0, '2016-08-23 09:48:37', 0),
(49, '23', NULL, NULL, '99', 'sfdg', 'dfgfhgf', '5', 0, '2016-08-23 09:49:10', 0),
(50, '11', NULL, NULL, '101', 'Duvida', 'Muit bom', '4', 0, '2016-08-28 01:34:51', 0),
(51, NULL, '7', NULL, '84', 'Nice collection', 'very nice collect.cost effective', '4', 0, '2016-10-19 13:17:10', 0),
(52, '27', NULL, NULL, '84', 'Nice Design', 'xzxzxzxzxzxzxxzxzxzx', '3', 0, '2016-11-03 04:49:04', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_secmaincategory`
--

CREATE TABLE `nm_secmaincategory` (
  `smc_id` smallint(5) UNSIGNED NOT NULL,
  `smc_name` varchar(100) NOT NULL,
  `smc_mc_id` smallint(5) UNSIGNED NOT NULL,
  `smc_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_secmaincategory`
--

INSERT INTO `nm_secmaincategory` (`smc_id`, `smc_name`, `smc_mc_id`, `smc_status`) VALUES
(1, 'Mobiles', 1, 1),
(2, 'Cloths', 2, 1),
(4, 'Ethnic Wear', 3, 1),
(6, 'Baby Care', 4, 1),
(7, 'Western Wear', 3, 1),
(8, 'Clock', 1, 1),
(9, 'Digital Camera', 1, 1),
(10, 'Footwear', 2, 1),
(11, 'Watches', 2, 1),
(12, 'Cloths', 4, 1),
(13, 'ccccc', 1, 1),
(14, 'Furnitures', 5, 1),
(15, 'Kitchenware', 5, 1),
(16, 'Computers & Gaming', 6, 1),
(17, 'Cricket Kit', 7, 1),
(18, 'Cars', 8, 1),
(19, 'Bikes', 8, 1),
(20, 'Story Books', 9, 1),
(21, 'Text Books', 9, 1),
(22, 'Biography', 9, 1),
(23, 'Bikes', 7, 1),
(24, 'Cameras', 6, 1),
(25, 'ELECTRONIC', 10, 1),
(26, 'Handphones & Tablets', 6, 1),
(27, 'Bags', 5, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_secsubcategory`
--

CREATE TABLE `nm_secsubcategory` (
  `ssb_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_name` varchar(100) NOT NULL,
  `ssb_sb_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_smc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `ssb_status` tinyint(4) NOT NULL,
  `ssb_code` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_secsubcategory`
--

INSERT INTO `nm_secsubcategory` (`ssb_id`, `ssb_name`, `ssb_sb_id`, `ssb_smc_id`, `mc_id`, `ssb_status`, `ssb_code`) VALUES
(1, 'Checked Shirts', 4, 2, 2, 1, ''),
(2, 'Regular Fit', 9, 2, 2, 1, ''),
(3, 'Indigo', 6, 3, 4, 1, ''),
(4, 'Printed Tops', 10, 3, 7, 1, ''),
(5, 'Analog', 11, 1, 8, 1, ''),
(6, 'Android Mobiles', 2, 1, 1, 1, ''),
(7, 'Polo', 3, 2, 2, 1, ''),
(8, 'Rounded Neck', 3, 2, 2, 1, ''),
(9, 'V-Neck', 3, 2, 2, 1, ''),
(10, 'Samsung Galaxy ', 1, 1, 1, 1, ''),
(12, 'Lenovo S90 ', 2, 1, 1, 1, ''),
(13, 'Sony CyberShot', 12, 1, 9, 1, ''),
(14, 'Boys Footwear', 13, 4, 6, 1, ''),
(15, 'Leggings', 10, 3, 7, 1, ''),
(16, 'Girls Footwear', 13, 4, 6, 1, ''),
(17, 'Casual Shoes', 15, 2, 10, 1, ''),
(18, ' Black Steel Wrist Watch', 17, 2, 11, 1, ''),
(19, 'Baby League  Regular Tops', 18, 4, 6, 1, ''),
(20, 'Samsung Galaxy A5 bb', 1, 1, 1, 1, ''),
(21, 'Sony Xperia M4', 1, 1, 1, 0, ''),
(22, 'Z Berries Shrug', 19, 3, 7, 1, ''),
(23, 'Empire Waist Pink Dress', 20, 4, 12, 1, ''),
(24, 'Tshirts', 20, 4, 12, 1, ''),
(25, 'Shirts', 20, 4, 12, 1, ''),
(26, 'Ethnic Dhoti', 20, 4, 12, 1, '210'),
(27, 'Anarkali Suit', 20, 4, 12, 1, ''),
(28, 'Slim Fit', 21, 3, 7, 1, ''),
(29, 'Patiala', 6, 3, 4, 1, ''),
(33, 'Johnson ', 22, 4, 6, 1, ''),
(34, 'Lotion', 22, 4, 6, 1, ''),
(35, 'Diaper Bag', 7, 4, 6, 1, ''),
(36, 'Living Room', 23, 5, 14, 1, ''),
(37, 'Bed Room', 23, 5, 14, 1, ''),
(38, '2 in 1 Laptops', 24, 6, 16, 1, ''),
(39, 'Mac Books', 24, 6, 16, 1, ''),
(40, 'Note Books', 24, 6, 16, 1, ''),
(41, 'Helmets', 27, 7, 17, 1, ''),
(42, 'Maruthi', 28, 8, 18, 1, ''),
(43, 'Hundai', 29, 8, 18, 1, ''),
(44, 'Audi', 30, 8, 18, 1, ''),
(45, 'Ancient India', 31, 9, 20, 1, ''),
(46, 'Nagas', 32, 9, 20, 1, ''),
(47, 'Superman', 33, 9, 20, 1, ''),
(48, 'Spider Man', 33, 9, 20, 1, ''),
(49, 'Mercedes Benz', 30, 8, 18, 1, ''),
(50, 'Hero', 34, 8, 19, 1, ''),
(51, 'Honda', 34, 8, 19, 1, ''),
(52, 'Yamaha', 35, 8, 19, 1, ''),
(53, 'Suzuki', 35, 8, 19, 1, ''),
(54, 'Royal Enfield', 36, 8, 19, 1, ''),
(55, 'Harley Davidson', 36, 8, 19, 1, ''),
(56, 'Canon', 37, 6, 24, 1, ''),
(58, 'Nikon', 38, 6, 24, 1, ''),
(59, 'iPhone', 39, 6, 26, 1, ''),
(60, 'iPad', 40, 6, 26, 1, ''),
(61, 'Handle Trolley', 41, 5, 27, 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_shipping`
--

CREATE TABLE `nm_shipping` (
  `ship_id` int(10) UNSIGNED NOT NULL,
  `ship_name` varchar(100) NOT NULL,
  `ship_address1` varchar(200) NOT NULL,
  `ship_address2` varchar(200) NOT NULL,
  `ship_ci_id` int(11) NOT NULL,
  `ship_state` varchar(100) NOT NULL,
  `ship_country` smallint(5) UNSIGNED NOT NULL,
  `ship_postalcode` varchar(20) NOT NULL,
  `ship_phone` varchar(20) NOT NULL,
  `ship_email` varchar(255) NOT NULL,
  `ship_order_id` int(10) UNSIGNED NOT NULL,
  `ship_cus_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_shipping`
--

INSERT INTO `nm_shipping` (`ship_id`, `ship_name`, `ship_address1`, `ship_address2`, `ship_ci_id`, `ship_state`, `ship_country`, `ship_postalcode`, `ship_phone`, `ship_email`, `ship_order_id`, `ship_cus_id`) VALUES
(1, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 0, 25),
(2, 'yamuna', '', '', 1, '', 1, '', '', '', 0, 3),
(3, 'testing', 'coimbatore', 'vadavalli', 1, 'tmilnadu', 1, '641021', '7598400215', '', 0, 4),
(4, 'testing', 'coimbatore', 'vadavalli', 0, 'tmilnadu', 0, '641021', '--', '', 1, 4),
(5, 'sethu', 'coimbatore', 'vadavalli', 1, 'tamilnadu', 1, '641021', '9789453352', '', 0, 5),
(6, 'sethu', 'coimbatore', 'vadavalli', 0, 'tamilnadu', 0, '641021', '--', '', 2, 5),
(7, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 12, 25),
(8, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 19, 25),
(9, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 29, 25),
(10, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 15, 25),
(11, 'din', '', '', 1, '', 1, '', '', '', 0, 20),
(12, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 36, 25),
(13, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 38, 25),
(14, 'dinesh', '', '', 1, '', 1, '', '', '', 0, 21),
(15, 'Marimuthu', '4/156, North Garden, Palladam', 'Coimbatore(post)', 0, 'Coimbatore', 0, '641041', '9874563210', '', 39, 21),
(16, '', '', '', 0, '', 0, '', '', '', 40, 21),
(17, 'dfgdfg', '', '', 1, '', 1, '', '', '', 0, 22),
(18, 'shankar', '', '', 1, '', 1, '', '', '', 0, 26),
(19, 'viniu', '', '', 1, '', 1, '', '', '', 0, 27),
(20, 'viniu', '', '', 1, '', 1, '', '', '', 0, 28),
(21, 'viniu', '', '', 1, '', 1, '', '', '', 0, 29),
(22, 'viniu', '', '', 1, '', 1, '', '', '', 0, 30),
(23, 'Bharathi', '', '', 1, '', 1, '', '', '', 0, 32),
(24, 'Priya', '', '', 1, '', 1, '', '', '', 0, 33),
(25, 'Gowtham', '', '', 1, '', 1, '', '', '', 0, 34),
(26, 'Gowtham', '', '', 1, '', 1, '', '', '', 0, 35),
(27, 'suresh', '', '', 1, '', 1, '', '', '', 0, 36),
(28, 'suresh', '', '', 1, '', 1, '', '', '', 0, 37),
(29, 'suresh', '', '', 1, '', 1, '', '', '', 0, 38),
(30, 'Gowtham', '', '', 1, '', 1, '', '', '', 0, 39),
(31, 'mate', 'bfd', 'gfdgfdg', 1, 'hfhg', 1, '457353', '789525332', '', 0, 40),
(32, 'Gowtham', '', '', 1, '', 1, '', '', '', 0, 41),
(33, 'mate', 'bfd', 'gfdgfdg', 1, 'hfhg', 1, '457353', '789525332', '', 56, 40),
(34, 'mate', 'bfd', 'gfdgfdg', 1, 'hfhg', 1, '457353', '789525332', '', 57, 40),
(35, 'suresh1', '', '', 1, '', 1, '', '', '', 0, 42),
(36, 'mate', 'bfd', 'gfdgfdg', 1, 'hfhg', 1, '457353', '789525332', '', 118, 40),
(37, 'mate', 'bfd', 'gfdgfdg', 1, 'hfhg', 1, '457353', '789525332', '', 59, 40),
(38, 'mate', 'bfd', 'gfdgfdg', 1, 'hfhg', 1, '457353', '789525332', '', 60, 40),
(39, 'manu', '', '', 1, '', 1, '', '', '', 0, 45),
(40, 'shanmu', '', '', 1, '', 1, '', '', '', 0, 46),
(41, 'dsfaew', '', '', 1, '', 1, '', '', '', 0, 47),
(42, 'kailashkumar', 'anna street', 'chatram', 2, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 0, 48),
(43, 'test', '', '', 1, '', 1, '', '', '', 0, 49),
(44, 'suresh', '', '', 1, '', 1, '', '', '', 0, 50),
(45, 'est', '', '', 1, '', 1, '', '', '', 0, 51),
(46, 'test', 'tesste', 'teste', 0, 'teste', 0, '2732733', '657657687', '', 120, 51),
(47, '', '', '', 0, '', 0, '', '', '', 121, 51),
(48, 'test', '', '', 1, '', 1, '', '', '', 0, 52),
(49, 'tezt', 'rqwr', 'qeqe', 0, 'reads', 0, '300000', '23413423', '', 122, 52),
(50, '', '', '', 0, '', 0, '', '', '', 123, 52),
(51, 'Habib', '', '', 1, '', 1, '', '', '', 0, 53),
(52, 'Habib Ynusa', 'c17 Kaduna', 'c17 Kaduna', 0, 'Nigeria', 0, '600001', '0806211490', '', 63, 53),
(53, '', '', '', 0, '', 0, '', '', '', 64, 53),
(54, 'mahesh', '', '', 1, '', 1, '', '', '', 0, 54),
(55, 'horny', '', '', 0, '', 1, '', '', '', 0, 55),
(56, 'vince', '', '', 1, '', 1, '', '', '', 0, 56),
(57, 'mahesh', '', '', 1, '', 1, '', '', '', 0, 57),
(58, 'test', '', '', 1, '', 1, '', '', '', 0, 58),
(59, 'test', '', '', 1, '', 1, '', '', '', 0, 59),
(60, 'test', '20,test', 'tester ', 0, 'AP', 0, '456324', '8495559723', '', 65, 59),
(61, 'dave', '', '', 1, '', 1, '', '', '', 0, 60),
(62, 'kumar kumar', '144,ramnagar', 'sengupatha street', 1, 'tamilnadu', 1, '641009', '9500818702', 'sales@laravelecommerce.com', 0, 61),
(63, 'balaji', 'Dhamnunagar', 'Puliyakulam', 1, 'Tamilnadu', 1, '641045', '9952440467', '', 0, 62),
(64, 'charles', 'Redfields', 'Puliyakulam', 1, 'Tamilnadu', 1, '641045', '9498056637', '', 0, 63),
(65, 'charles', '', '', 1, '', 1, '', '', '', 0, 64),
(66, 'charles', '', '', 1, '', 1, '', '', '', 0, 65),
(67, 'charles', '', '', 1, '', 1, '', '', '', 0, 66),
(68, 'vivek', '', '', 1, '', 1, '', '', '', 0, 67),
(69, 'charles', '', '', 1, '', 1, '', '', '', 0, 68),
(70, 'charles', '', '', 1, '', 1, '', '', '', 0, 69),
(71, 'babu', '', '', 1, '', 1, '', '', '', 0, 70),
(72, 'charles', '', '', 1, '', 1, '', '', '', 0, 71),
(73, 'charlesjc', '', '', 1, '', 1, '', '', '', 0, 72),
(74, 'charlesjc', '', '', 1, '', 1, '', '', '', 0, 73),
(75, 'rajesh', '144', '122', 1, 'tn', 1, '641010', '1231231231', 'erkprajesh@gmail.com', 0, 74),
(76, 'kumar', 'test1', 'test2', 1, 'tn', 1, '641009', '9745232341', 'rrpofi@gmail.com', 0, 75),
(77, 'victorboss', 'Ramnagar', 'Gandipuram', 1, 'Tamilnadu', 1, '641045', '9498056637', '', 0, 76),
(78, 'rajesh', '', '', 1, '', 1, '', '', '', 0, 77),
(79, 'rajesh', '', '', 1, '', 1, '', '', '', 0, 78),
(80, '', '', '', 0, '', 0, '', '', '', 133, 77),
(81, 'balji', '', '', 1, '', 1, '', '', '', 0, 92),
(82, 'Jayson', '?12, Prince Edward Road, #04-06 Podium A,', 'Bestway Building', 0, 'N/A', 0, '079212', '96172158', '', 135, 93),
(83, 'Ali', '', '', 1, '', 1, '', '', '', 0, 94),
(84, 'pavi', '', '', 1, '', 1, '', '', '', 0, 96),
(85, 'huyhoang', '', '', 1, '', 1, '', '', '', 0, 99),
(86, 'Li', 'dd', 'dd', 0, 'Ifugao', 0, '996584', '0915127728', '', 136, 100),
(87, 'hfhfh', 'fhfh', 'fhfhf', 0, '100000', 0, '100000', '0988543131', '', 89, 102),
(88, 'unicon', '', '', 1, '', 1, '', '', '', 0, 106),
(89, 'kkkk', '', '', 1, '', 1, '', '', '', 0, 107),
(90, 'kkkk', 'kkkk', 'kkkk', 0, 'kkkk', 0, '124544', '123000000', '', 138, 107),
(91, '', '', '', 0, '', 0, '', '', '', 139, 107),
(92, 'bike', '', '', 1, '', 1, '', '', '', 0, 108),
(93, 'sh', '', '', 1, '', 1, '', '', '', 0, 109),
(94, 'sdfasdf', '', '', 1, '', 1, '', '', '', 0, 111),
(95, 'sdfasdf', '', '', 1, '', 1, '', '', '', 0, 112),
(96, 'Test', '', '', 1, '', 1, '', '', '', 0, 113),
(97, 'vinay', '', '', 1, '', 1, '', '', '', 0, 116),
(98, 'testuser', '', '', 1, '', 1, '', '', '', 0, 119),
(99, 'aa', 'aaa', 'aaa', 0, 'aaa', 0, '111111', '1111111111', '', 140, 119),
(100, '', '', '', 0, '', 0, '', '', '', 141, 119),
(101, '', '', '', 0, '', 0, '', '', '', 142, 119),
(102, 'a', 'a', '11', 0, '111', 0, '111122', '11118988', '', 143, 120),
(103, 'arun', '', '', 1, '', 1, '', '', '', 0, 122),
(104, 'testery', '', '', 1, '', 1, '', '', '', 0, 124),
(105, 'kumar', '', '', 1, '', 1, '', '', '', 0, 125),
(106, '432432', '', '', 1, '', 1, '', '', '', 0, 126),
(107, 'Vineet', '', '', 1, '', 1, '', '', '', 0, 131),
(108, '', '', '', 0, '', 0, '', '', '', 145, 131),
(109, 'zam', 'jogja', 'jogja', 0, 'jogja', 0, '123456', '0292920000', '', 92, 132),
(110, '', '', '', 0, '', 0, '', '', '', 147, 132),
(111, 'dfas', '', '', 1, '', 1, '', '', '', 0, 133),
(112, 'ilakkiya', '', '', 1, '', 1, '', '', '', 0, 134),
(113, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 93, 134),
(114, '', '', '', 0, '', 0, '', '', '', 94, 134),
(115, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 148, 134),
(116, 'ilakkiya', '', '', 1, '', 1, '', '', '', 0, 135),
(117, 'ilakkiya', 'gandhipuram', 'namnagar', 0, 'tamilnadu', 0, '641008', '8883152529', '', 149, 134),
(118, '', '', '', 0, '', 0, '', '', '', 150, 134),
(119, 'ilakkiya', '', '', 2, '', 1, '', '', '', 0, 136),
(120, 'surya', '', '', 1, '', 1, '', '', '', 0, 137),
(121, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 151, 137),
(122, '', '', '', 0, '', 0, '', '', '', 152, 137),
(123, '', '', '', 0, '', 0, '', '', '', 153, 137),
(124, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 95, 137),
(125, '', '', '', 0, '', 0, '', '', '', 96, 137),
(126, '', '', '', 0, '', 0, '', '', '', 97, 137),
(127, 'ilakkiya m', '', '', 1, '', 1, '', '', '', 0, 138),
(128, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 98, 137),
(129, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 154, 139),
(130, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 155, 139),
(131, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 99, 139),
(132, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 100, 139),
(133, 'Rajeshkumar', 'POFI TECHNOLOGIES PRIVATE LIMITED (OPC)', '#144, 1st Floor, Sengupta Street, Ram Nagar,', 0, 'Tamil Nadu', 0, '641009', '9500818702', '', 156, 81),
(134, 'Rajeshkumar', 'POFI TECHNOLOGIES PRIVATE LIMITED (OPC)', '#144, 1st Floor, Sengupta Street, Ram Nagar,', 0, 'Tamil Nadu', 0, '641009', '9500818702', '', 157, 81),
(135, 'Rajeshkumar', 'POFI TECHNOLOGIES PRIVATE LIMITED (OPC)', '#144, 1st Floor, Sengupta Street, Ram Nagar,', 0, 'Tamil Nadu', 0, '641009', '9500818702', '', 101, 81),
(136, 'Test', '', '', 2, '', 1, '', '', '', 0, 140),
(137, 'root-master', '', '', 2, '', 1, '', '', '', 0, 141),
(138, 'ilakkiya', '', '', 1, '', 1, '', '', '', 0, 142),
(139, 'ilakkiya', '', '', 1, '', 1, '', '', '', 0, 143),
(140, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 102, 142),
(141, 'ilakkiya m', '', '', 1, '', 1, '', '', '', 0, 145),
(142, 'ilakkiya', '', '', 2, '', 1, '', '', '', 0, 146),
(143, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 103, 146),
(144, 'charles', '', '', 1, '', 1, '', '', '', 0, 147),
(145, 'charles', '', '', 1, '', 1, '', '', '', 0, 148),
(146, 'charles', '', '', 1, '', 1, '', '', '', 0, 149),
(147, 'charles', '', '', 1, '', 1, '', '', '', 0, 150),
(148, 'charles', '', '', 1, '', 1, '', '', '', 0, 151),
(149, 'charles', '', '', 2, '', 1, '', '', '', 0, 152),
(150, 'charles', '', '', 1, '', 1, '', '', '', 0, 153),
(151, 'charles', '', '', 1, '', 1, '', '', '', 0, 154),
(152, 'charles', '', '', 2, '', 1, '', '', '', 0, 155),
(153, 'charles', '', '', 2, '', 1, '', '', '', 0, 156),
(154, 'charles', '', '', 2, '', 1, '', '', '', 0, 157),
(155, 'charles', '', '', 1, '', 1, '', '', '', 0, 158),
(156, 'charles', '', '', 2, '', 1, '', '', '', 0, 159),
(157, 'charles', '', '', 2, '', 1, '', '', '', 0, 160),
(158, 'sdgsdgs', '', '', 2, '', 1, '', '', '', 0, 161),
(159, 'sdgsdgs', '', '', 2, '', 1, '', '', '', 0, 162),
(160, 'Ahmey', '', '', 1, '', 1, '', '', '', 0, 163),
(161, 'shafiq', '', '', 2, '', 1, '', '', '', 0, 164),
(162, 'charles', '', '', 2, '', 1, '', '', '', 0, 165),
(163, 'charles fdddfh', '', '', 2, '', 1, '', '', '', 0, 166),
(164, 'dfsdfg', '', '', 2, '', 1, '', '', '', 0, 167),
(165, '', '', '', 0, '', 0, '', '', '', 108, 130),
(166, 'sdfasdsad', 'asdasd', 'asdasd', 0, 'asdasd', 0, '23434234234', '2342342342', '', 109, 168),
(167, 'dfhdfhfd', '', '', 1, '', 1, '', '', '', 0, 169),
(168, 'arun', '', '', 1, '', 1, '', '', '', 0, 170),
(169, 'arun', '', '', 1, '', 1, '', '', '', 0, 171),
(170, 'dfhdfhd', '', '', 2, '', 1, '', '', '', 0, 172),
(171, 'arun', '', '', 1, '', 1, '', '', '', 0, 173),
(172, 'arun', '', '', 1, '', 1, '', '', '', 0, 174),
(173, 'charles', '', '', 2, '', 1, '', '', '', 0, 175),
(174, 'dfhdfhd', '', '', 2, '', 1, '', '', '', 0, 176),
(175, 'fddfhdfh', '', '', 2, '', 1, '', '', '', 0, 177),
(176, 'shsfhsf', '', '', 2, '', 1, '', '', '', 0, 178),
(177, 'dfhdfhdf', '', '', 0, '', 0, '', '', '', 0, 179),
(178, 'fdhdhfd', '', '', 0, '', 0, '', '', '', 0, 180),
(179, 'ccc', '', '', 2, '', 1, '', '', '', 0, 181),
(180, 'fff', '', '', 1, '', 1, '', '', '', 0, 182),
(181, 'gh', '', '', 2, '', 1, '', '', '', 0, 183),
(182, 'dhdhddf', '', '', 2, '', 1, '', '', '', 0, 184),
(183, 'sfhhsffsh', '', '', 2, '', 1, '', '', '', 0, 185),
(184, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 0, 25),
(185, 'charles', '', '', 2, '', 1, '', '', '', 0, 2),
(186, 'pavithran', '', '', 2, '', 1, '', '', '', 0, 3),
(187, 'pavithran', '', '', 2, '', 1, '', '', '', 0, 4),
(188, 'sdgsdgs', '', '', 1, '', 1, '', '', '', 0, 6),
(189, 'charles', '', '', 1, '', 1, '', '', '', 0, 7),
(190, 'dadsagds', '', '', 1, '', 1, '', '', '', 0, 8),
(191, 'sdgsdgs', '', '', 1, '', 1, '', '', '', 0, 9),
(192, 'new', '', '', 1, '', 1, '', '', '', 0, 10),
(193, 'charles', '', '', 2, '', 1, '', '', '', 0, 11),
(194, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 0, 25),
(195, 'charles', '', '', 1, '', 1, '', '', '', 0, 2),
(196, 'charles', '', '', 1, '', 1, '', '', '', 0, 3),
(197, 'charles', '', '', 1, '', 1, '', '', '', 0, 4),
(198, 'pavithran', '', '', 1, '', 1, '', '', '', 0, 5),
(199, 'dfhdhdf', '', '', 1, '', 1, '', '', '', 0, 6),
(200, 'charles', '', '', 1, '', 1, '', '', '', 0, 7),
(201, 'demo', '', '', 1, '', 1, '', '', '', 0, 8),
(202, 'mahesh', 'eerwrwer', 'erewrwer', 1, 'tn', 1, '3423423', '365536565', '', 0, 14),
(203, 'kailash', '', '', 1, '', 1, '', '', '', 0, 15),
(204, 'kailash', 'jansi nagar', 'erode', 0, 'tamilnadu', 0, '638004', '1234567890', '', 111, 15),
(205, 'kailash', 'jansi nagar', 'erode', 0, 'tamilnadu', 0, '638004', '1234567890', '', 112, 15),
(206, 'kailash', 'jansi nagar', 'erode', 0, 'tamilnadu', 0, '638004', '1234567890', '', 113, 15),
(207, 'saravanan', 'anna street', 'v,chatram', 2, 'tamil nadu', 1, '123458', '9092398789', '', 0, 16),
(208, 'kannan', '', '', 1, '', 1, '', '', '', 0, 17),
(209, 'pavithran', 'nehru Street', 'velacheri', 2, 'Tamil nadu', 1, '123456', '1717171717', '', 0, 18),
(210, 'charles', '', '', 1, '', 1, '', '', '', 0, 19),
(211, 'test', '', '', 1, '', 1, '', '', '', 0, 20),
(212, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 2, 21),
(213, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 3, 21),
(214, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 4, 21),
(215, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 5, 21),
(216, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 6, 21),
(217, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 131, 21),
(218, 'ilakkiya', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 132, 21),
(219, 'kailash', 'jansinagar', 'v.chatram', 2, 'tn', 1, '638004', '1234567890', 'kumarkailash075@gmail.com', 0, 23),
(220, 'maheswaran', 'hello', '', 1, '', 1, '', '', '', 0, 24),
(221, 'maheswaran', 'test', 'test', 0, 'test', 0, '641041', '9324234322', '', 133, 24),
(222, '', '', '', 0, '', 0, '', '', '', 134, 24),
(223, 'maheshwaran', 'gandhipuram', 'ramnagar', 1, 'tamilnadu', 1, '123456', '1234567890', '', 0, 25),
(224, 'dhanasekar', '', '', 1, '', 1, '', '', '', 0, 26),
(225, 'ilakkiya m', '', '', 1, '', 1, '', '', '', 0, 27),
(226, 'Pavithran', '', '', 1, '', 1, '', '', '', 0, 28),
(227, 'ilakkiya m', '', '', 1, '', 1, '', '', '', 0, 29),
(228, 'ilakkiya m', '', '', 1, '', 1, '', '', '', 0, 30),
(229, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 10, 30),
(230, '', '', '', 0, '', 0, '', '', '', 11, 30),
(231, '', '', '', 0, '', 0, '', '', '', 12, 30),
(232, '', '', '', 0, '', 0, '', '', '', 13, 30),
(233, '', '', '', 0, '', 0, '', '', '', 14, 30),
(234, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 15, 30),
(235, '', '', '', 0, '', 0, '', '', '', 16, 30),
(236, '', '', '', 0, '', 0, '', '', '', 17, 30),
(237, '', '', '', 0, '', 0, '', '', '', 18, 30),
(238, '', '', '', 0, '', 0, '', '', '', 19, 30),
(239, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 20, 30),
(240, '', '', '', 0, '', 0, '', '', '', 21, 30),
(241, '', '', '', 0, '', 0, '', '', '', 22, 30),
(242, '', '', '', 0, '', 0, '', '', '', 23, 30),
(243, '', '', '', 0, '', 0, '', '', '', 24, 30),
(244, '', '', '', 0, '', 0, '', '', '', 25, 30),
(245, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 26, 30),
(246, '', '', '', 0, '', 0, '', '', '', 27, 30),
(247, '', '', '', 0, '', 0, '', '', '', 28, 30),
(248, '', '', '', 0, '', 0, '', '', '', 29, 30),
(249, '', '', '', 0, '', 0, '', '', '', 30, 30),
(250, '', '', '', 0, '', 0, '', '', '', 31, 30),
(251, '', '', '', 0, '', 0, '', '', '', 32, 30),
(252, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 9, 30),
(253, '', '', '', 0, '', 0, '', '', '', 10, 30),
(254, '', '', '', 0, '', 0, '', '', '', 11, 30),
(255, '', '', '', 0, '', 0, '', '', '', 12, 30),
(256, '', '', '', 0, '', 0, '', '', '', 13, 30),
(257, '', '', '', 0, '', 0, '', '', '', 14, 30),
(258, '', '', '', 0, '', 0, '', '', '', 15, 30),
(259, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 16, 30),
(260, '', '', '', 0, '', 0, '', '', '', 17, 30),
(261, '', '', '', 0, '', 0, '', '', '', 18, 30),
(262, '', '', '', 0, '', 0, '', '', '', 19, 30),
(263, '', '', '', 0, '', 0, '', '', '', 20, 30),
(264, '', '', '', 0, '', 0, '', '', '', 21, 30),
(265, '', '', '', 0, '', 0, '', '', '', 22, 30),
(266, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 23, 30),
(267, '', '', '', 0, '', 0, '', '', '', 24, 30),
(268, '', '', '', 0, '', 0, '', '', '', 25, 30),
(269, '', '', '', 0, '', 0, '', '', '', 26, 30),
(270, '', '', '', 0, '', 0, '', '', '', 27, 30),
(271, '', '', '', 0, '', 0, '', '', '', 28, 30),
(272, '', '', '', 0, '', 0, '', '', '', 29, 30),
(273, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 30, 30),
(274, '', '', '', 0, '', 0, '', '', '', 31, 30),
(275, '', '', '', 0, '', 0, '', '', '', 32, 30),
(276, '', '', '', 0, '', 0, '', '', '', 33, 30),
(277, '', '', '', 0, '', 0, '', '', '', 34, 30),
(278, '', '', '', 0, '', 0, '', '', '', 35, 30),
(279, '', '', '', 0, '', 0, '', '', '', 36, 30),
(280, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 37, 30),
(281, '', '', '', 0, '', 0, '', '', '', 38, 30),
(282, '', '', '', 0, '', 0, '', '', '', 39, 30),
(283, '', '', '', 0, '', 0, '', '', '', 40, 30),
(284, '', '', '', 0, '', 0, '', '', '', 41, 30),
(285, '', '', '', 0, '', 0, '', '', '', 42, 30),
(286, '', '', '', 0, '', 0, '', '', '', 43, 30),
(287, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 44, 30),
(288, '', '', '', 0, '', 0, '', '', '', 45, 30),
(289, '', '', '', 0, '', 0, '', '', '', 46, 30),
(290, '', '', '', 0, '', 0, '', '', '', 47, 30),
(291, '', '', '', 0, '', 0, '', '', '', 48, 30),
(292, '', '', '', 0, '', 0, '', '', '', 49, 30),
(293, '', '', '', 0, '', 0, '', '', '', 50, 30),
(294, 'ilakkiya m', 'ganghipuram', 'ganghipuram', 0, 'tamilnadu', 0, '641008', '8883152529', '', 51, 30),
(295, '', '', '', 0, '', 0, '', '', '', 52, 30),
(296, '', '', '', 0, '', 0, '', '', '', 53, 30),
(297, '', '', '', 0, '', 0, '', '', '', 54, 30),
(298, '', '', '', 0, '', 0, '', '', '', 55, 30),
(299, '', '', '', 0, '', 0, '', '', '', 56, 30),
(300, '', '', '', 0, '', 0, '', '', '', 57, 30),
(301, 'charles', '', '', 1, '', 1, '', '', '', 0, 31),
(302, 'kailashkumar', '', '', 1, '', 1, '', '', '', 0, 33),
(303, 'charles', '', '', 1, '', 1, '', '', '', 0, 34),
(304, 'charles', '', '', 1, '', 1, '', '', '', 0, 35),
(305, 'charles', '', '', 1, '', 1, '', '', '', 0, 36),
(306, 'charles', '', '', 1, '', 1, '', '', '', 0, 37),
(307, 'charles', '', '', 1, '', 1, '', '', '', 0, 38),
(308, 'charles', '', '', 1, '', 1, '', '', '', 0, 39),
(309, 'charles', '', '', 1, '', 1, '', '', '', 0, 40),
(310, 'charles', '', '', 1, '', 1, '', '', '', 0, 41),
(311, 'charles', '', '', 1, '', 1, '', '', '', 0, 42),
(312, 'kumar', '', '', 1, '', 1, '', '', '', 0, 43),
(313, 'Pavithran', '', '', 1, '', 1, '', '', '', 0, 47),
(314, 'kailashkumar', 'anna street', 'chatram', 2, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 0, 48),
(315, 'Mohsin Ahmed', '', '', 3, '', 1, '', '', '', 0, 49),
(316, 'rajesh', '', '', 1, '', 1, '', '', '', 0, 51),
(317, 'maheshwaran', '', '', 1, '', 1, '', '', '', 0, 59),
(318, 'emre', '', '', 5, '', 1, '', '', '', 0, 60),
(319, 'kumar kumar', '144,ramnagar', 'sengupatha street', 1, 'tamilnadu', 1, '641009', '9500818702', 'sales@laravelecommerce.com', 0, 61),
(320, 'kumar', '', '', 1, '', 1, '', '', '', 0, 62),
(321, 'kumar', '', '', 1, '', 1, '', '', '', 0, 63),
(322, 'kumar', '', '', 1, '', 1, '', '', '', 0, 64),
(323, 'kumar', '', '', 1, '', 1, '', '', '', 0, 65),
(324, 'kumar', '', '', 1, '', 1, '', '', '', 0, 66),
(325, 'kumar', '', '', 1, '', 1, '', '', '', 0, 67),
(326, 'kumar', '', '', 1, '', 1, '', '', '', 0, 68),
(327, 'kumar', '', '', 1, '', 1, '', '', '', 0, 69),
(328, 'kumar', '', '', 1, '', 1, '', '', '', 0, 70),
(329, 'kumar', '', '', 1, '', 1, '', '', '', 0, 71),
(330, 'kumar', '', '', 1, '', 1, '', '', '', 0, 72),
(331, 'kumar', '', '', 1, '', 1, '', '', '', 0, 73),
(332, 'rajesh', '144', '122', 1, 'tn', 1, '641010', '1231231231', 'erkprajesh@gmail.com', 0, 74),
(333, 'sddsfd', 'sdvsd', 'sdvsdv', 0, 'sdvsdv', 0, '123123', '1231231231', 'er@er.com', 78, 74),
(334, 'kumar', 'test1', 'test2', 1, 'tn', 1, '641009', '9745232341', 'rrpofi@gmail.com', 0, 75),
(335, 'maheshwaran', 'gandhipuram', 'ramnagar', 0, 'tamilnadu', 0, '123456', '1234567890', 'maheswaran@pofitec.com', 80, 75),
(336, 'victor', 'xxxx', 'xxxx', 0, 'tamilnadu', 0, '641009', '9498056637', 'chalesvictor.info@gmail.com', 90, 42),
(337, 'Pavithran', '', '', 1, '', 1, '', '', '', 0, 76),
(338, 'Pavi', 'Ddf', 'Ff', 0, 'Ggg', 0, '638452', '1234567890', 'pavithrandbpro@gmail.com', 91, 76),
(339, 'Pavithran', '144, Sengupta Street | Near Hotel City Towers', 'Ram Nagar | Coimbatore - 641009', 0, 'Tamil Nadu', 0, '641009', '9787467575', 'pavithrandbpro@gmail.com', 42, 76),
(340, 'Pavithran', '144, Sengupta Street | Near Hotel City Towers', 'Ram Nagar | Coimbatore - 641009', 0, 'Tamil Nadu', 0, '641009', '9787467575', 'pavithrandbpro@gmail.com', 92, 76),
(341, 'test', 'test mail', 'tes', 0, 'test', 0, '123456', '1234567890', 'pavithran.g@pofitec.com', 93, 76),
(342, 'test', 'test', 'test', 0, 'test', 0, '123456', '1234567890', 'charles.j@pofitec.com', 97, 25),
(343, 'test', 'test', 'test', 0, 'test', 0, '123456', '1234567890', 'charles.j@pofitec.com', 99, 25),
(344, 'Test', '', '', 2, '', 1, '', '', '', 0, 78),
(345, 'reca', '', '', 1, '', 1, '', '', '', 0, 79),
(346, 'TESTE 1', '', '', 2, '', 1, '', '', '', 0, 81),
(347, 'vinod', '', '', 1, '', 1, '', '', '', 0, 82),
(348, 'asd', '', '', 0, '', 5, '', '', '', 0, 89),
(349, 'dfghdf', '', '', 1, '', 1, '', '', '', 0, 92),
(350, 'catella', '', '', 0, '', 0, '', '', '', 0, 95),
(351, 'reza', '', '', 3, '', 1, '', '', '', 0, 96),
(352, 'a', '', '', 0, '', 6, '', '', '', 0, 98),
(353, 'Pk', '', '', 3, '', 1, '', '', '', 0, 99),
(354, 'kiran', '', '', 4, '', 1, '', '', '', 0, 100),
(355, 'hgjhgj', 'ghjhg', 'jghj', 0, 'hgj', 0, 'jhg', 'jhgjhg', 'jhgj', 46, 100),
(356, 'amitavaroy', '', '', 2, '', 1, '', '', '', 0, 102),
(357, 'Testing', 'testing address', 'address two', 0, 'testing', 0, '32412', '1231231231', 'iesien22@yahoo.com', 104, 103),
(358, 'Testing', 'testing address', 'address two', 0, 'testing', 0, '32412', '1231231231', 'iesien22@yahoo.com', 48, 103),
(359, 'Burham', '', '', 3, '', 1, '', '', '', 0, 104),
(360, 'Burham', 'cimahi depok', 'rt07', 0, 'delhi', 0, '10222', '812122112', 'rezka@gmail.com', 49, 104),
(361, 'aaa', '', '', 2, '', 1, '', '', '', 0, 106),
(362, 'wefsdfsdfs', '', '', 1, '', 1, '', '', '', 0, 107),
(363, 'khor', '', '', 0, '', 5, '', '', '', 0, 108),
(364, 'Wedus', '', '', 4, '', 1, '', '', '', 0, 109),
(365, 'mahmoud', '', '', 3, '', 1, '', '', '', 0, 110),
(366, 'name', '', '', 0, '', 7, '', '', '', 0, 111),
(367, 'maheswaran', '', '', 1, '', 1, '', '', '', 0, 112),
(368, 'Jason', '', '', 0, '', 6, '', '', '', 0, 113),
(369, 'jubin', '', '', 3, '', 1, '', '', '', 0, 114),
(370, 'trung', '', '', 4, '', 1, '', '', '', 0, 115),
(371, 'esec kailash', '', '', 2, '', 1, '', '', '', 0, 116),
(372, 'esec kailash', '', '', 2, '', 1, '', '', '', 0, 117),
(373, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 113, 84),
(374, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 114, 84),
(375, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 115, 84),
(376, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 116, 84),
(377, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 117, 84),
(378, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 118, 84),
(379, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 119, 84),
(380, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 120, 84),
(381, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 121, 84),
(382, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 122, 84),
(383, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 123, 84),
(384, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 124, 84),
(385, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 125, 84),
(386, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 126, 84),
(387, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 127, 84),
(388, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 128, 84),
(389, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 129, 84),
(390, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 130, 84),
(391, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 131, 84),
(392, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 53, 84),
(393, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 54, 84),
(394, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 55, 84),
(395, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 56, 84),
(396, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 57, 84),
(397, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 58, 84),
(398, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 59, 84),
(399, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 60, 84),
(400, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 61, 84),
(401, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 62, 84),
(402, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 63, 84),
(403, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 64, 84),
(404, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 65, 84),
(405, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 66, 84),
(406, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 67, 84),
(407, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 68, 84),
(408, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 69, 84),
(409, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 70, 84),
(410, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 71, 84),
(411, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 72, 84),
(412, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 73, 84),
(413, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 74, 84),
(414, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 75, 84),
(415, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 76, 84),
(416, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 77, 84),
(417, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 78, 84),
(418, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 79, 84),
(419, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 80, 84),
(420, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 81, 84),
(421, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 82, 84),
(422, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 83, 84),
(423, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 84, 84),
(424, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 85, 84),
(425, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 86, 84),
(426, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 87, 84),
(427, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 88, 84),
(428, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 89, 84),
(429, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 90, 84),
(430, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 91, 84),
(431, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 92, 84),
(432, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 93, 84),
(433, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 94, 84),
(434, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 95, 84),
(435, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 96, 84),
(436, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 97, 84),
(437, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 98, 84),
(438, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 99, 84),
(439, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 100, 84),
(440, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 101, 84),
(441, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 102, 84),
(442, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 103, 84),
(443, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 104, 84),
(444, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 105, 84),
(445, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 106, 84),
(446, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 107, 84),
(447, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 108, 84),
(448, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 109, 84),
(449, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 110, 84),
(450, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 111, 84),
(451, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 112, 84),
(452, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 113, 84),
(453, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 114, 84),
(454, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 115, 84),
(455, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 116, 84),
(456, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 117, 84),
(457, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 118, 84),
(458, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 119, 84),
(459, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 120, 84),
(460, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 132, 84),
(461, 'guna', '75', 'hghdhghgs', 5, 'tn', 1, '123456', '1234567890', 'kumarkailash075@gmail.com', 133, 84),
(462, 'pofikailash', '', '', 1, '', 1, '', '', '', 0, 123),
(463, 'pofi kailash', '', '', 1, '', 1, '', '', '', 0, 124),
(464, 'pofi kailash', 'bcvbcvcvb', 'vcbvcbcvbcvb', 0, 'tn', 0, '123456', '1234567890', 'kailashkumar.r@esec.ac.in', 134, 124),
(465, 'George Nammour', '', '', 1, '', 1, '', '', '', 0, 125),
(466, 'George Nammour', 'test', 'test', 0, 'test', 0, 'test', 'test', 'georgen@linteractif.com', 121, 125),
(467, 'kumarpofi', '', '', 4, '', 1, '', '', '', 0, 126),
(468, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 127),
(469, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 128),
(470, 'eseckailash', '', '', 2, '', 1, '', '', '', 0, 129),
(471, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 130),
(472, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 131),
(473, 'vvvvvvv', '', '', 1, '', 1, '', '', '', 0, 132),
(474, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 133),
(475, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 134),
(476, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 135),
(477, 'eseckailash', '', '', 2, '', 1, '', '', '', 0, 136),
(478, 'eseckailash', '', '', 1, '', 1, '', '', '', 0, 137),
(479, 'kjjkjkjk', '', '', 1, '', 1, '', '', '', 0, 138),
(480, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 0, 139),
(481, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 122, 139),
(482, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 123, 84),
(483, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 124, 139),
(484, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 125, 139),
(485, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 126, 139),
(486, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 127, 84),
(487, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 128, 139),
(488, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 129, 84),
(489, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 130, 84),
(490, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 131, 84),
(491, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 132, 84),
(492, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 133, 84),
(493, 'guna', '75', 'hghdhghgs', 0, 'tn', 0, '123456', '1234567890', 'kumarkailash075@gmail.com', 134, 84),
(494, 'pofikailash', '', '', 1, '', 1, '', '', '', 0, 140),
(495, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 135, 139),
(496, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 136, 139),
(497, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 137, 139),
(498, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 138, 139),
(499, 'ramkumar', '', '', 1, '', 1, '', '', '', 0, 141),
(500, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 135, 141),
(501, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 139, 141),
(502, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 140, 141),
(503, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 141, 141),
(504, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 142, 141),
(505, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 143, 141),
(506, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 144, 141),
(507, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 145, 139),
(508, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 146, 139),
(509, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '1234567890', 'ramkumar@gmail.com', 147, 141),
(510, 'ramkumar', 'aaaaaa', 'bbbbbbbbb', 0, 'tn', 0, '123456', '9546877878', 'ramkumar@gmail.com', 148, 141),
(511, 'tester1.kuku', 'a1', 'a2', 3, 's', 1, '121', '0129312345', 'tester1.kuku@gmail.com', 149, 139),
(512, 'tester1.kuku', 'a1', 'a2', 0, 's', 0, '121', '0129312345', 'tester1.kuku@gmail.com', 150, 139),
(513, 'tester1.kuku .....', 'address1 ....', 'address2 ....', 0, 'state* ....', 0, '111111111', '1231231231', 'tester1.kuku@gmail.com', 151, 139),
(514, 'asd', '', '', 4, '', 1, '', '', '', 0, 142),
(515, 'U', 'A1', 'A2', 0, 'S4', 0, '12345', '0878882377', 'tester1.kuku@gmail.com', 152, 139),
(516, 'UU', 'AA1', 'AA2', 0, 'SS4', 0, '121212', '0989898898', 'tester1.kuku@gmail.com', 153, 139),
(517, 'laxman', '', '', 1, '', 1, '', '', '', 0, 143),
(518, 'Nur Hidayat', '', '', 7, '', 8, '', '', '', 0, 144),
(519, 'nama', 'alamat1', 'alamat2', 0, 'provinsi', 0, '123', '0123', 'tester1.kuku@gmail.com', 154, 139),
(520, 'aaadd', 'bbb', 'ccc', 0, 'eee', 0, '1212', '737283', 'tester1.kuku@gmail.com', 155, 139),
(521, 'Toto', 'address1', 'address2', 0, 'state4', 0, '1234566', '2311212', 'tester1.kuku@gmail.com', 136, 139),
(522, 'sadas', 'asdas', 'asdas', 0, 'asdas', 0, '13414', '3124', 'tester1.kuku@gmail.com', 156, 139),
(523, 'qwqwq', 'wwew', 'qwqw', 0, 'qwq', 0, '2323', '2323', 'tester1.kuku@gmail.com', 157, 139),
(524, 'ada', 'ada111', 'adada11', 0, 'adad', 0, 'da', 'ad', 'tester1.kuku@gmail.com', 158, 139),
(525, 'ada', 'ada111', 'adada11', 0, 'adad', 0, 'da', 'ad', 'tester1.kuku@gmail.com', 137, 139),
(526, 'adad', 'dada', 'adadad', 0, 'adad', 0, 'da', 'ddad', 'tester1.kuku@gmail.com', 138, 139),
(527, '121', '212', 'qdadad', 0, 'adad', 0, 'dada', 'dada', 'tester1.kuku@gmail.com', 139, 139),
(528, 'asas', 'as', 'asas', 0, 'sas', 0, 'asas', 'aa', 'tester1.kuku@gmail.com', 159, 139),
(529, 'laxman', '', '', 1, '', 1, '', '', '', 0, 145),
(530, 'laxman', 'ccc', 'ccc', 0, 'tn', 0, '123456', '1234567890', 'kailashkumar.r@pofitec.com', 160, 145),
(531, 'notebook', 'ad1', 'ad2', 0, 'ct4', 0, '1212', '0123', 'tester1.kuku@gmail.com', 161, 139),
(532, 'tester1.kuku', 'aaa', 'bbb', 0, 'ddd', 0, '123', '1212', 'tester1.kuku@gmail.com', 162, 139),
(533, 'tester1.kuku', 'aaa', 'bbb', 0, 'ddd', 0, '123', '1212', 'tester1.kuku@gmail.com', 163, 139),
(534, 'qqq', 'www', 'eeee', 0, 'tttt', 0, '1212', '1212', 'tester1.kuku@gmail.com', 164, 139),
(535, '1212212', '121212', '12121212', 0, '121212', 0, '1212', '1212', 'tester1.kuku@gmail.com', 165, 139),
(536, 'doku1', 'doku2', 'doku3', 0, 'doku5', 0, '2121', '112', 'tester1.kuku@gmail.com', 166, 139),
(537, 'qwqwqw', 'qwqwq', 'qwqwq', 0, 'qwqw', 0, '3434', '2434', 'tester1.kuku@gmail.com', 167, 139),
(538, 'popop', 'opopo', 'popop', 0, 'opop', 0, '1212', '121', 'tester1.kuku@gmail.com', 168, 139),
(539, 'sdsd', 'sdsd', 'sdsd', 0, 'sdsd', 0, '121', '122', 'tester1.kuku@gmail.com', 169, 139),
(540, '1221', '122121', '1212', 0, '1212', 0, '1212', '1212', 'tester1.kuku@gmail.com', 170, 139),
(541, '1212', '1212', '1212', 0, '121', 0, '121', '1212', 'tester1.kuku@gmail.com', 171, 139),
(542, '1212', '1212', '1212', 0, '1212', 0, '1212', '1212', 'tester1.kuku@gmail.com', 172, 139),
(543, 'asas', 'sasas', 'asasas', 0, 'asasas', 0, '1212', '12121', 'tester1.kuku@gmail.com', 173, 139),
(544, 'wqwqw', 'qwqw', 'wqwqw', 0, 'qwqw', 0, '1212', '2121', 'tester1.kuku@gmail.com', 174, 139),
(545, 'Toto', 'Add1', 'Add2', 0, 'Sta', 0, '121', '123', 'tester1.kuku@gmail.com', 175, 139),
(546, 'Toto', 'Add1', 'Add2', 0, 'Sta', 0, '121', '123', 'tester1.kuku@gmail.com', 176, 139),
(547, 'Toto', 'Add1', 'Add2', 0, 'Sta', 0, '121', '123', 'tester1.kuku@gmail.com', 177, 139),
(548, 'Saptoto', 'aaa', 'bbb', 0, 'sss', 0, '22', '223', 'tester1.kuku@gmail.com', 178, 139),
(549, 'Saptoto', 'aaa', 'bbb', 0, 'sss', 0, '22', '223', 'tester1.kuku@gmail.com', 179, 139),
(550, 'Saptoto', 'aaa', 'bbb', 0, 'sss', 0, '22', '223', 'tester1.kuku@gmail.com', 180, 139),
(551, 'Saptoto', 'aaa', 'bbb', 0, 'sss', 0, '22', '223', 'tester1.kuku@gmail.com', 181, 139),
(552, 'aaaa', 'bbbb', 'ccc', 0, 'eee', 0, '1212', '1212', 'tester1.kuku@gmail.com', 182, 139),
(553, 'aaaa', 'bbbb', 'ccc', 0, 'eee', 0, '1212', '1212', 'tester1.kuku@gmail.com', 183, 139),
(554, 'aaaa', 'bbbb', 'ccc', 0, 'eee', 0, '1212', '1212', 'tester1.kuku@gmail.com', 184, 139),
(555, 'aaaa', 'bbbb', 'ccc', 0, 'eee', 0, '1212', '1212', 'tester1.kuku@gmail.com', 185, 139),
(556, '111', '333', '222', 0, '666', 0, '113', '121', 'tester1.kuku@gmail.com', 186, 139),
(557, '111', '333', '222', 0, '666', 0, '113', '121', 'tester1.kuku@gmail.com', 187, 139),
(558, '111', '333', '222', 0, '666', 0, '113', '121', 'tester1.kuku@gmail.com', 188, 139),
(559, '111', '333', '222', 0, '666', 0, '113', '121', 'tester1.kuku@gmail.com', 189, 139),
(560, 'qqqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 190, 139),
(561, 'qqqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 191, 139),
(562, 'qqqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 192, 139),
(563, 'qqqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 193, 139),
(564, 'qqq', 'www', 'eee', 0, 'tt', 0, '12111', '123', 'tester1.kuku@gmail.com', 194, 139),
(565, 'qqq', 'www', 'eee', 0, 'tt', 0, '12111', '123', 'tester1.kuku@gmail.com', 195, 139),
(566, 'qqq', 'www', 'eee', 0, 'tt', 0, '12111', '123', 'tester1.kuku@gmail.com', 196, 139),
(567, 'qqq', 'www', 'eee', 0, 'tt', 0, '12111', '123', 'tester1.kuku@gmail.com', 197, 139),
(568, 'qqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 198, 139),
(569, 'qqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 199, 139),
(570, 'qqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 200, 139),
(571, 'qqq', 'www', 'eee', 0, 'ttt', 0, '456', '123', 'tester1.kuku@gmail.com', 201, 139),
(572, 'rrrr', 'tttt', 'yyyy', 0, 'iiii', 0, '456', '123', 'tester1.kuku@gmail.com', 202, 139),
(573, 'rrrr', 'tttt', 'yyyy', 0, 'iiii', 0, '456', '123', 'tester1.kuku@gmail.com', 203, 139),
(574, 'rrrr', 'tttt', 'yyyy', 0, 'iiii', 0, '456', '123', 'tester1.kuku@gmail.com', 204, 139),
(575, 'rrrr', 'tttt', 'yyyy', 0, 'iiii', 0, '456', '123', 'tester1.kuku@gmail.com', 205, 139),
(576, 'sss', 'dddd', 'ffff', 0, 'hhhh', 0, '456', '1234', 'tester1.kuku@gmail.com', 206, 139),
(577, 'sss', 'dddd', 'ffff', 0, 'hhhh', 0, '456', '1234', 'tester1.kuku@gmail.com', 207, 139),
(578, 'sss', 'dddd', 'ffff', 0, 'hhhh', 0, '456', '1234', 'tester1.kuku@gmail.com', 208, 139),
(579, 'sss', 'dddd', 'ffff', 0, 'hhhh', 0, '456', '1234', 'tester1.kuku@gmail.com', 209, 139),
(580, 'uuu', 'iiii', 'oooo', 0, 'rrr', 0, '567', '1234', 'tester1.kuku@gmail.com', 210, 139),
(581, 'uuu', 'iiii', 'oooo', 0, 'rrr', 0, '567', '1234', 'tester1.kuku@gmail.com', 211, 139),
(582, 'uuu', 'iiii', 'oooo', 0, 'rrr', 0, '567', '1234', 'tester1.kuku@gmail.com', 212, 139),
(583, 'uuu', 'iiii', 'oooo', 0, 'rrr', 0, '567', '1234', 'tester1.kuku@gmail.com', 213, 139),
(584, 'vvvv', 'ffff', 'dddd', 0, 'rrr', 0, '343', '1234', 'tester1.kuku@gmail.com', 214, 139),
(585, 'vvvv', 'ffff', 'dddd', 0, 'rrr', 0, '343', '1234', 'tester1.kuku@gmail.com', 215, 139),
(586, 'vvvv', 'ffff', 'dddd', 0, 'rrr', 0, '343', '1234', 'tester1.kuku@gmail.com', 216, 139),
(587, 'vvvv', 'ffff', 'dddd', 0, 'rrr', 0, '343', '1234', 'tester1.kuku@gmail.com', 217, 139),
(588, 'ggg', 'ggg', 'ggg', 0, 'ggg', 0, '343', '3434', 'tester1.kuku@gmail.com', 218, 139),
(589, 'ggg', 'ggg', 'ggg', 0, 'ggg', 0, '343', '3434', 'tester1.kuku@gmail.com', 219, 139),
(590, 'ggg', 'ggg', 'ggg', 0, 'ggg', 0, '343', '3434', 'tester1.kuku@gmail.com', 220, 139),
(591, 'ggg', 'ggg', 'ggg', 0, 'ggg', 0, '343', '3434', 'tester1.kuku@gmail.com', 221, 139),
(592, 'wwe', 'ewew', 'wewe', 0, 'wewe', 0, '333', '222', 'tester1.kuku@gmail.com', 222, 139),
(593, 'wwe', 'ewew', 'wewe', 0, 'wewe', 0, '333', '222', 'tester1.kuku@gmail.com', 223, 139),
(594, 'wwe', 'ewew', 'wewe', 0, 'wewe', 0, '333', '222', 'tester1.kuku@gmail.com', 224, 139),
(595, 'wwe', 'ewew', 'wewe', 0, 'wewe', 0, '333', '222', 'tester1.kuku@gmail.com', 225, 139),
(596, '2', '3', '4', 0, '6', 0, '6', '5', 'tester1.kuku@gmail.com', 226, 139),
(597, '2', '3', '4', 0, '6', 0, '6', '5', 'tester1.kuku@gmail.com', 227, 139),
(598, '2', '3', '4', 0, '6', 0, '6', '5', 'tester1.kuku@gmail.com', 228, 139),
(599, '2', '3', '4', 0, '6', 0, '6', '5', 'tester1.kuku@gmail.com', 229, 139),
(600, '11', '22', '33', 0, '55', 0, '88', '77', 'tester1.kuku@gmail.com', 230, 139),
(601, '11', '22', '33', 0, '55', 0, '88', '77', 'tester1.kuku@gmail.com', 231, 139),
(602, '11', '22', '33', 0, '55', 0, '88', '77', 'tester1.kuku@gmail.com', 232, 139),
(603, '11', '22', '33', 0, '55', 0, '88', '77', 'tester1.kuku@gmail.com', 233, 139),
(604, 'qwqw', 'qwqw', 'qwqw', 0, 'qwqw', 0, '1212', '1212', 'tester1.kuku@gmail.com', 234, 139),
(605, 'ooo', 'ppp', 'lll', 0, 'jjjj', 0, '456', '123', 'tester1.kuku@gmail.com', 235, 139),
(606, 'qqq', 'qqq', 'qqq', 0, 'qq', 0, '121', '121', 'tester1.kuku@gmail.com', 236, 139),
(607, 'ggg', 'gg', 'gg', 0, 'gg', 0, '23', '3323', 'tester1.kuku@gmail.com', 237, 139),
(608, 'ggg', 'ggg', 'ggg', 0, 'ggg', 0, 'ggg', '121', 'tester1.kuku@gmail.com', 238, 139),
(609, 'dd', 'dd', 'dd', 0, 'ddd', 0, '122', '1212', 'tester1.kuku@gmail.com', 239, 139),
(610, 'ww', 'www', 'www', 0, 'www', 0, '121', '1212', 'tester1.kuku@gmail.com', 240, 139),
(611, 'fff', 'fff', 'fff', 0, 'fff', 0, '2323', '2232', 'tester1.kuku@gmail.com', 241, 139),
(612, 'qwq', 'qwq', 'qwqw', 0, 'qw', 0, '121', '121', 'tester1.kuku@gmail.com', 242, 139),
(613, 'dfdf', 'dfdf', 'dfdf', 0, 'dfdf', 0, '1212', '1212', 'tester1.kuku@gmail.com', 243, 139),
(614, 'wew', 'wew', 'wewe', 0, 'wewe', 0, '2323', '2323', 'tester1.kuku@gmail.com', 244, 139),
(615, 'qqq', 'qqq', 'qqq', 0, 'qqqq', 0, '121', 'q1212', 'tester1.kuku@gmail.com', 245, 139),
(616, '232', '232', '323', 0, '2323', 0, '232', '232', 'tester1.kuku@gmail.com', 246, 139),
(617, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 247, 139),
(618, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 248, 139),
(619, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 249, 139),
(620, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 250, 139),
(621, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 251, 139),
(622, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 252, 139),
(623, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 253, 139),
(624, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 254, 139),
(625, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 255, 139),
(626, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 256, 139),
(627, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 257, 139),
(628, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 258, 139),
(629, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 259, 139),
(630, 'Toto', 'aaa', 'aaaaaa', 0, 'sss', 0, '121', '3223', 'tester1.kuku@gmail.com', 260, 139),
(631, 'Harry Okta', '', '', 7, '', 8, '', '', '', 0, 146),
(632, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '0857173255', 'harry.okta.maulana@gmail.com', 261, 146),
(633, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '0857173255', 'harry.okta.maulana@gmail.com', 262, 146),
(634, 'Harry Okta Maulana', '', '', 7, '', 8, '', '', '', 0, 147),
(635, 'Harry Okta Maulana', '', '', 7, '', 8, '', '', '', 0, 149),
(636, 'Harry Okta Maulana', '', '', 7, '', 8, '', '', '', 0, 150),
(637, 'Harry', 'JL Pajak', 'Jl. Bunga', 0, 'DKI', 0, '15222', '0863535353', 'harry.okta.maulana@gmail.com', 140, 150),
(638, 'Harry', 'JL Pajak', 'Jl. Bunga', 0, 'DKI', 0, '15222', '0863535353', 'harry.okta.maulana@gmail.com', 141, 150),
(639, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 142, 150),
(640, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 143, 150);
INSERT INTO `nm_shipping` (`ship_id`, `ship_name`, `ship_address1`, `ship_address2`, `ship_ci_id`, `ship_state`, `ship_country`, `ship_postalcode`, `ship_phone`, `ship_email`, `ship_order_id`, `ship_cus_id`) VALUES
(641, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 144, 150),
(642, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 145, 150),
(643, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 146, 150),
(644, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 147, 150),
(645, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 148, 150),
(646, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 149, 150),
(647, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 150, 150),
(648, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 151, 150),
(649, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 152, 150),
(650, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 153, 150),
(651, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 154, 150),
(652, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 155, 150),
(653, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 156, 150),
(654, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 263, 150),
(655, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 264, 150),
(656, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 265, 150),
(657, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 266, 150),
(658, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 267, 150),
(659, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 268, 150),
(660, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 269, 150),
(661, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 270, 150),
(662, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 271, 150),
(663, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 272, 150),
(664, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 273, 150),
(665, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 274, 150),
(666, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 275, 150),
(667, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 276, 150),
(668, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 277, 150),
(669, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 278, 150),
(670, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 279, 150),
(671, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 280, 150),
(672, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 281, 150),
(673, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 282, 150),
(674, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 283, 150),
(675, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 284, 150),
(676, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 285, 150),
(677, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 286, 150),
(678, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 287, 150),
(679, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 288, 150),
(680, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 289, 150),
(681, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 290, 150),
(682, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 291, 150),
(683, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 292, 150),
(684, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(685, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(686, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(687, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(688, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(689, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(690, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(691, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(692, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(693, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(694, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(695, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(696, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(697, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(698, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(699, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(700, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(701, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(702, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(703, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(704, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(705, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(706, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(707, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(708, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(709, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(710, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(711, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(712, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(713, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(714, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(715, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(716, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(717, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(718, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(719, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(720, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 0, 150),
(721, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 332, 150),
(722, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 333, 150),
(723, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 334, 150),
(724, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 335, 150),
(725, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 336, 150),
(726, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 337, 150),
(727, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 338, 150),
(728, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 339, 150),
(729, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 340, 150),
(730, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 341, 150),
(731, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 342, 150),
(732, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 343, 150),
(733, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 344, 150),
(734, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 345, 150),
(735, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 346, 150),
(736, 'Harry', 'JL. Pajak Raya', 'JL. Budi Jaya', 0, 'Jawa Barat', 0, '15315', '857173255', 'harry.okta.maulana@gmail.com', 347, 150),
(737, 'Koko', 'Jl Kocak', 'JL Ngibul', 0, 'Jawa', 0, '15333', '0866777655', 'harry.okta.maulana@gmail.com', 348, 150),
(738, 'Koko', 'Jl Kocak', 'JL Ngibul', 0, 'Jawa', 0, '15333', '0866777655', 'harry.okta.maulana@gmail.com', 349, 150),
(739, 'Koko', 'Jl Kocak', 'JL Ngibul', 0, 'Jawa', 0, '15333', '0866777655', 'harry.okta.maulana@gmail.com', 350, 150),
(740, 'Koko', 'Jl Kocak', 'JL Ngibul', 0, 'Jawa', 0, '15333', '866777655', 'harry.okta.maulana@gmail.com', 351, 150),
(741, 'Koko', 'Jl Kocak', 'JL Ngibul', 0, 'Jawa', 0, '15333', '866777655', 'harry.okta.maulana@gmail.com', 352, 150);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_size`
--

CREATE TABLE `nm_size` (
  `si_id` smallint(5) UNSIGNED NOT NULL,
  `si_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_size`
--

INSERT INTO `nm_size` (`si_id`, `si_name`) VALUES
(2, '42'),
(3, '44'),
(4, '28'),
(5, '32'),
(6, '34'),
(7, '36'),
(8, '38'),
(9, '40'),
(11, '53'),
(12, '66'),
(13, '7.9');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_smtp`
--

CREATE TABLE `nm_smtp` (
  `sm_id` tinyint(4) NOT NULL,
  `sm_host` varchar(150) NOT NULL,
  `sm_port` varchar(20) NOT NULL,
  `sm_uname` varchar(30) NOT NULL,
  `sm_pwd` varchar(30) NOT NULL,
  `sm_isactive` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_smtp`
--

INSERT INTO `nm_smtp` (`sm_id`, `sm_host`, `sm_port`, `sm_uname`, `sm_pwd`, `sm_isactive`) VALUES
(1, 'smtp.gmail.com', '587', 'kailashkumar.r@pofitec.com', '1234567890', 1),
(2, 'Send Grid', '149', 'send grid', 'sendgrid', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_social_media`
--

CREATE TABLE `nm_social_media` (
  `sm_id` int(11) NOT NULL,
  `sm_fb_app_id` varchar(100) NOT NULL,
  `sm_fb_sec_key` varchar(100) NOT NULL,
  `sm_fb_page_url` varchar(250) NOT NULL,
  `sm_fb_like_page_url` varchar(250) NOT NULL,
  `sm_twitter_url` varchar(250) NOT NULL,
  `sm_twitter_app_id` varchar(250) NOT NULL,
  `sm_twitter_sec_key` varchar(250) NOT NULL,
  `sm_linkedin_url` varchar(250) NOT NULL,
  `sm_youtube_url` varchar(250) NOT NULL,
  `sm_gmap_app_key` varchar(250) NOT NULL,
  `sm_android_page_url` varchar(250) NOT NULL,
  `sm_iphone_url` varchar(250) NOT NULL,
  `sm_analytics_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_social_media`
--

INSERT INTO `nm_social_media` (`sm_id`, `sm_fb_app_id`, `sm_fb_sec_key`, `sm_fb_page_url`, `sm_fb_like_page_url`, `sm_twitter_url`, `sm_twitter_app_id`, `sm_twitter_sec_key`, `sm_linkedin_url`, `sm_youtube_url`, `sm_gmap_app_key`, `sm_android_page_url`, `sm_iphone_url`, `sm_analytics_code`) VALUES
(1, '1159985624035786', 'b515b2aa9c659647515b7c34964ae228', 'http://www.facebook.com/', 'http://www.facebook.com/', 'http://www.twitter.com/', 'dsf1dsfsd232d1f21dsf21ds2f1dsf', 'sd2f1sd2f13sfgsd543df3ds1fds1f', 'http://www.linkedin.com/', 'http://www.youtube.com', 'sdf1sd321f32dssadf1f3ds1f3', '', 'http://www.iphone.com', '<script>\r\n  (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,''script'',''https://www.google-analytics.com/analytics.js'',''ga'');\r\n\r\n  ga(''create'', ''UA-62671250-4'', ''auto'');\r\n  ga(''send'', ''pageview'');\r\n</script>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_specification`
--

CREATE TABLE `nm_specification` (
  `sp_id` smallint(5) UNSIGNED NOT NULL,
  `sp_name` text NOT NULL,
  `sp_spg_id` smallint(5) UNSIGNED NOT NULL,
  `sp_order` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_specification`
--

INSERT INTO `nm_specification` (`sp_id`, `sp_name`, `sp_spg_id`, `sp_order`) VALUES
(1, 'sofa feature', 1, 1),
(2, 'box 1', 2, 1),
(3, 'box 2', 2, 2),
(4, 'box 3', 2, 3),
(5, 'volume', 3, 5),
(6, 'test2', 1, 1),
(7, 'mangoolian', 4, 5),
(8, 'screen size', 6, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_spgroup`
--

CREATE TABLE `nm_spgroup` (
  `spg_id` smallint(5) UNSIGNED NOT NULL,
  `spg_name` varchar(150) NOT NULL,
  `spg_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_spgroup`
--

INSERT INTO `nm_spgroup` (`spg_id`, `spg_name`, `spg_order`) VALUES
(1, 'sofa', 1),
(2, 'boxes', 2),
(3, 'Memory', 4),
(4, 'mantest', 5),
(5, 'ABCDEFGH', 12),
(6, 'tablet details', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_store`
--

CREATE TABLE `nm_store` (
  `stor_id` int(10) UNSIGNED NOT NULL,
  `stor_merchant_id` int(11) NOT NULL,
  `stor_name` varchar(100) NOT NULL,
  `stor_phone` varchar(20) NOT NULL,
  `stor_address1` varchar(150) NOT NULL,
  `stor_address2` varchar(150) NOT NULL,
  `stor_country` smallint(5) UNSIGNED NOT NULL,
  `stor_city` int(10) UNSIGNED NOT NULL,
  `stor_zipcode` varchar(20) NOT NULL,
  `stor_metakeywords` text NOT NULL,
  `stor_metadesc` text NOT NULL,
  `stor_website` text NOT NULL,
  `stor_latitude` decimal(18,14) NOT NULL,
  `stor_longitude` decimal(18,14) NOT NULL,
  `stor_img` varchar(150) NOT NULL,
  `stor_status` tinyint(4) NOT NULL DEFAULT '1',
  `created_date` varchar(20) NOT NULL,
  `stor_addedby` int(5) NOT NULL DEFAULT '1' COMMENT '1-admin,2 -merchant'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_store`
--

INSERT INTO `nm_store` (`stor_id`, `stor_merchant_id`, `stor_name`, `stor_phone`, `stor_address1`, `stor_address2`, `stor_country`, `stor_city`, `stor_zipcode`, `stor_metakeywords`, `stor_metadesc`, `stor_website`, `stor_latitude`, `stor_longitude`, `stor_img`, `stor_status`, `created_date`, `stor_addedby`) VALUES
(1, 9, 'Design Hut', '226157', 'City Main Road', 'Bangaluru', 1, 4, '123456', 'nice store in bangalore', 'nice store in bangalore', 'www.hut.com', '12.96624565591343', '77.58632295390623', 'Bonmarche_1852845bS66erAZ7.jpg', 1, '06/08/2016', 1),
(4, 9, 'Maz Store', '1234567890', 'j.k road', 'Delhi', 1, 3, '123456', 'Maz store in delhi for online shopping', 'Maz store in delhi for online shopping', 'www.maz.com', '28.70552213640328', '77.24198018437505', 'storesffgnF9Dv.jpg', 1, '', 1),
(5, 12, 'Trinity Place', '8883152529', 'Ramnagar', 'Ganghipuram', 1, 1, '641008', 'Trinity  Place Deaprtment Store', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 'http://www.downtownmagazinenyc.com/theres-a-new-name-downtown-trinity-place-department-store/', '11.01006629675941', '76.94938781586916', 'sZEQNGpZB.jpg', 1, '06/08/2016', 1),
(6, 13, 'Big Bazaar', '7485963210', 'Ganghipuram', 'Town Hall', 1, 1, '641008', 'Big Bazaar In Coimbatore', 'This large, bright, minimalist three level (levels two and three will open shortly) space brings to Lower Manhattan the best of small European brands. For now, the first floor is women’s wear with vintage-inspired dresses, tops and skirts suitable for work or play.', 'http://www.allahabadonline.in/city-guide/big-bazar-allahabad', '10.98995993029173', '76.96492573652358', 'ko5BrnpVi.jpg', 1, '06/08/2016', 1),
(7, 16, 'Savithrai', '9498056637', 'ramnagar', 'gandipuram', 1, 1, '641009', 'xxxxxxxxxxx', 'xxxxxxxxxxx', 'www.demo.laravelecommerce.com', '11.01235010000000', '76.96286580000003', 'Desert3rk1dftU.jpg', 1, '', 1),
(8, 17, 'pofi', '1231231231', '1441', '123', 1, 1, '641009', 'NSBDV.SD', 'MNB KSJDFB', 'SKDJFNB.COM', '11.01684450000000', '76.95583209999995', 'logo2lrbuKBG.png', 0, '', 1),
(9, 18, 'rajivstore', '8871665475', 'address', 'address', 1, 4, '4503321', 'test', 'test', 'test', '10.25000000000000', '30.20000000000000', '2016-06-27-1800jirp7ox4.jpg', 1, '08/01/2016', 1),
(11, 15, 'Test Shop', '00000000000000000000', 'Mullai Nagar', 'test', 1, 1, '3456789', 'test', 'test', 'http://www.test.com', '40.35451960150120', '-3.68863972377926', 'sample swinder theme0Y5qz4Zf.jpg', 1, '', 2),
(12, 19, 'Palugada Store', '082312323123', 'Cemput', 'Barat', 1, 3, '10520', 'adasda', 'adasdad', 'http://domain.com', '12.29706829285382', '106.69921875000000', 'tampak ruangan 305bdtZ85.jpg', 1, '', 1),
(13, 20, 'tp', '7738254158', 'ghghg', 'hghgh', 1, 5, '400605', 'jkjkjj', 'ghhgh', 'baapprojects.com', '16.78703312221986', '72.71560756015629', 'html testnS811dWK.PNG', 1, '09/18/2016', 1),
(14, 9, 'test branch', '12312414412414', 'casdasdas', 'asdasdasd', 1, 1, '343434', 'sdasdas', 'asdasd', 'http://www.dsd.com', '11.14620713599789', '76.94422613828124', '300px-Usdollar100fronteXWGEDcW.jpg', 1, '', 1),
(15, 21, 'testShop', '7777777777', 'shopaddresslineone', 'shopaddresslinetwo', 1, 5, '877878', 'testshopmeta', 'testshopmetadesc', 'www.testshop.com', '28.63478125109309', '77.20816972578132', 'Hydrangeas987UGY1n.jpg', 0, '10/05/2016', 1),
(16, 22, 'jnjknjknjkn', '989890080', 'jkn', 'jkn', 1, 2, '88888', 'jknjknjkn', 'jk', 'njk', '44.91139700000000', '-116.11331489999998', 'tn-1470647813-s-l1600-2racpcR73.jpg', 1, '', 1),
(17, 23, 'kjnkjnkjnjkn', '8888888888', 'jknjknjk', 'nkjnjkn', 1, 2, '6757657', 'jhbjhb', 'hjb', 'jbhjbjb', '44.91139700000000', '-116.11331489999998', 'tn-1470647810-s-l1600-3CySrp4G7.jpg', 1, '', 1),
(18, 12, '21', '12', '12', '12', 1, 2, '12', '12', '12', '12', '13.07732955605433', '80.24050599765633', '_contoh1RFoqpumv.jpg', 1, '', 1),
(19, 9, 'bbb store', '1234567890', 'vxcxccxvc', 'fdggdf', 1, 1, '123456', 'dfdsfs', 'dfsdfsd', 'www.ssss.com', '11.03030886565141', '76.93873297421874', 'bbbb_shopOVk8M4lf.jpg', 1, '', 1),
(20, 9, 'vvv shop', '1234567890', 'sdssfddsf', 'dsfdfsd', 1, 1, '123456', 'sfdsdfsdf', 'dsfdf', 'www.mmmm.com', '11.05861384231795', '76.94559942929686', 'bbbb_shopreI0EGJh.jpg', 1, '', 1),
(21, 24, 'Magnum trendy shop', '2354780', 'bvbxcvxbvcbxbx', 'bvsbvbvdbvbdvsbv', 1, 1, '123456', 'bvxvbvbvxbvbcv', 'vbsvbvbvxbbcxbb', 'www.manum.com', '11.07614975186641', '76.96269855507808', 'bbbb_shopo0VH6YkM.jpg', 1, '11/01/2016', 1),
(22, 24, 'Hoston store', '4567891230', 'bvnbvnvbn', 'vnbvnbnv', 1, 4, '123456', 'cvcbvcbcvbcvb', 'cvbcvbcvbvbcvb', 'www.hoston.com', '12.99568597215706', '77.61378877421873', 'bbbb_shopR4xiRTvt.jpg', 1, '', 1),
(23, 9, 'Amazon shop', '123456', 'bnbbnbnbnbnb', 'jhsdjsjdhjdhjsh', 1, 1, '123456', 'nxnmxbcbnxcnxb', 'hsgshgshgd', 'http://www.amazon.in/', '11.03570050014765', '76.91676031796874', 'Koalai2hRpsY8.jpg', 1, '', 2),
(24, 25, 'test', '+9610000000', 'Beirut', 'test', 1, 2, '00000', 'test', 'test', 'http://www.test.com', '33.83107203432984', '35.51001644609369', '2TUcnr6nN.jpg', 1, '11/11/2016', 1),
(25, 26, 'test', '+9613583114', 'test', 'test', 1, 0, '000961', 'test', 'test', 'http://www.test.com', '33.87840083768415', '35.50109005449212', 'Screen Shot 2016-11-11 at 4F6l4ywOA.10', 1, '11/12/2016', 1),
(26, 27, 'test store', '09876543210', 'test ship addr1', 'test ship addr2', 1, 1, '543543543', 'dvfg', 'dfg d', 'www.google.com', '11.06267233831586', '76.98055133828120', '@mx_580wx0DdcKH.jpg', 1, '11/12/2016', 1),
(27, 28, 'biz store', '01234567890', '79', 'gandhi nagar', 1, 1, '123456', 'xcxcxcxcxcxcxxcxcxcxcc', 'xcxxxxcxccxxxxc', 'www.biz.com', '11.01684450000000', '76.95583209999995', 'Koalazc2q4GaR.jpg', 1, '', 1),
(28, 29, 'biz store', '01234567890', 'ghjghjghgj', 'fsfsdfsdfsdf', 1, 1, '123456', 'bxncbnbncxbvnxcbvnbnbxcnvb', 'nbnnbxnvbncbvnbxcnvb', 'www.biz.com', '11.01684450000000', '76.95583209999995', 'KoalaMA9fibRA.jpg', 1, '', 1),
(29, 30, 'teest', '+9613583114', 'test', 'test', 1, 1, '00000', 'test', 'test', 'http://www.test.com', '33.87384016751478', '35.51070309160150', '15095046_1356125864419876_5949159025492857915_nofXBjtQ1.jpg', 1, '11/15/2016', 1),
(30, 31, 'Helloo Store', '1234567890', 'cbvcvbvcbcvb', 'cvbcvbcv', 1, 1, '123456', 'xcvcvcvx', 'vbcbvbcvb', 'www.hellostore.com', '11.01684450000000', '76.95583209999995', 'Slides For You LogobJCfzXNZ.png', 1, '', 1),
(31, 32, 'Bala Store', '4567891223', 'xcxxcxx', 'cxcxcxc', 1, 1, '12345678', 'cvcvcxv', 'cvcxvxcv', 'www.bala.com', '11.04110718762900', '76.95308551796870', 'Slides For You LogoOwqIlqir.png', 1, '12/30/2016', 1),
(32, 33, 'untungstore1', '087888237701', 'a1', 'a2', 8, 7, '12345', 'mk', 'md', 'www.storefront.co.id', '-6.22483920000000', '106.85110869999994', 'logo-store-frontmHhIQQyy.png', 1, '', 1),
(33, 34, 'new store ', '1234567890', 'vvvv', 'yyyy', 1, 1, '641009', 'cccccccccc', 'xxxxxxxxxx', 'www.newstore.com', '11.34103640000000', '77.71716420000007', 'Koala4DiNZu2f.jpg', 1, '', 1),
(34, 35, 'new store 2', '01234567890', 'xdfdfsdf', 'bnnbnmn', 1, 1, '123456', 'bnbnn', 'nbnbnbnbn', 'www.newstore2.com', '11.34103640000000', '77.71716420000007', '', 1, '', 1),
(35, 36, 'Sendals', '089804521543', 'bogor', 'bogor', 8, 7, '16922', 'Jual apa aja', 'jual sendal', 'sendal.com', '-6.17446510000000', '106.82274499999994', '', 1, '', 1),
(36, 37, 'Coffe Break', '0857172536516', 'JL. Gunung Sahari 7', 'JL. Gunung Sahari 8', 8, 7, '15315', 'qwertyuiop', 'qwertyuiop', 'coffebreak1', '-6.27939353058377', '106.66296841656754', 'indexxupZBko0.jpg', 1, '02/28/2017', 1),
(37, 38, 'JonI', '3456789', 'sahfdja', 'sakjgdja', 8, 7, '5454', 'qwertyuiop', 'qwertyuiop', 'meta.com', '-6.87721509343191', '107.61734752698362', 'index0eGvN6CM.jpg', 1, '02/28/2017', 1),
(38, 39, 'Tarara', '34567890', 'JL. Batik', 'JL. Baju', 8, 8, '56789', 'qwertyuiop', '1234567890', 'bandung.com', '-6.55586874999263', '106.63050369101563', 'indexIa9z8ZQ7.jpg', 1, '02/28/2017', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_subcategory`
--

CREATE TABLE `nm_subcategory` (
  `sb_id` smallint(5) UNSIGNED NOT NULL,
  `sb_name` varchar(100) NOT NULL,
  `sb_smc_id` smallint(5) UNSIGNED NOT NULL,
  `mc_id` smallint(5) UNSIGNED NOT NULL,
  `sb_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_subcategory`
--

INSERT INTO `nm_subcategory` (`sb_id`, `sb_name`, `sb_smc_id`, `mc_id`, `sb_status`) VALUES
(1, 'Samsung', 1, 1, 1),
(2, 'Lenova', 1, 1, 1),
(3, 'T-Shirt', 2, 2, 1),
(4, 'Shirts', 2, 2, 1),
(5, 'Sarees', 4, 3, 1),
(6, 'Kurtas', 4, 3, 1),
(7, 'Diapers', 6, 4, 1),
(8, 'Wipes', 6, 4, 1),
(9, 'jeans', 2, 2, 1),
(10, 'Tops', 7, 3, 1),
(11, 'Wall Clock', 8, 1, 1),
(12, 'Sony', 9, 1, 1),
(13, 'Kids and Baby Footwear', 6, 4, 1),
(15, 'Casual', 10, 2, 1),
(17, 'Casual Watches', 11, 2, 1),
(18, 'Tops', 6, 4, 1),
(19, 'Shrug', 7, 3, 1),
(20, 'Kids Clothing', 12, 4, 1),
(21, 'Jeans', 7, 3, 1),
(22, 'Skin Care', 6, 4, 1),
(23, 'Wooden Furniture', 14, 5, 1),
(24, 'Laptops', 16, 6, 1),
(25, 'Desktops', 16, 6, 1),
(26, 'Full Kit', 17, 7, 1),
(27, 'Separate Equipments', 17, 7, 1),
(28, 'Basic Cars', 18, 8, 1),
(29, 'Sports Car', 18, 8, 1),
(30, 'Luxury Car', 18, 8, 1),
(31, 'Historical', 20, 9, 1),
(32, 'Thriller', 20, 9, 1),
(33, 'Comic', 20, 9, 1),
(34, 'Basic Bikes', 19, 8, 1),
(35, 'Sports Bikes', 19, 8, 1),
(36, 'Luxury Bikes', 19, 8, 1),
(37, 'Digital ', 24, 6, 1),
(38, 'DSLR', 24, 6, 1),
(39, 'Handphones', 26, 6, 1),
(40, 'Tablets', 26, 6, 1),
(41, 'Travel Bags', 27, 5, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_subscription`
--

CREATE TABLE `nm_subscription` (
  `sub_id` int(10) UNSIGNED NOT NULL,
  `sub_cus_id` int(10) UNSIGNED NOT NULL COMMENT 'customer id',
  `sub_mc_id` smallint(5) UNSIGNED NOT NULL,
  `sub_status` tinyint(4) NOT NULL,
  `sub_readstatus` int(11) NOT NULL DEFAULT '0' COMMENT '-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_terms`
--

CREATE TABLE `nm_terms` (
  `tr_id` int(11) NOT NULL,
  `tr_description` text NOT NULL,
  `tr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_terms`
--

INSERT INTO `nm_terms` (`tr_id`, `tr_description`, `tr_date`) VALUES
(1, '<b>Introduction</b><br>These terms and conditions govern \r\nyour use of <a rel="" target="" href="http://www.laravelecommerce.com">www.laravelecommerce.com</a>; by using this website, you accept \r\nthese terms and conditions in full. If you disagree with these terms and\r\n conditions or any part of these terms and conditions, you must not use \r\nthis website.<br>\r\n\r\n               You must be at least 18 years of age \r\nto use this website.  By using this website and by agreeing to these \r\nterms and conditions you warrant and represent that you are at least 18 \r\nyears of age.<br>\r\n\r\n               This website uses cookies.  By using \r\nthis website and agreeing to these terms and conditions, you consent to \r\nuse of cookies in accordance with the terms of <a rel="" target="" href="http://www.laravelecommerce.com">www.laravelecommerce.com</a><br>\r\n             <b>LICENSE TO USE WEBSITE</b><br>Unless otherwise stated, Laravel Ecommerce and/or \r\nits licensors own the intellectual property rights in the website and \r\nmaterial on the website.  Subject to the license below, all these \r\nintellectual property rights are reserved.<br>\r\n\r\n You may view, download for caching purposes only, \r\nand print pages [or [OTHER CONTENT]] from the website for your own \r\npersonal use, subject to the restrictions set out below and elsewhere in\r\n these terms and conditions.<br><br>YOU MUST NOT:&nbsp;&nbsp; <br><ul><li>Sell, rent or sub license material from the website;</li><li>Show any material from the website in public;</li><li>Reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;</li><li>Edit or otherwise modify any material on the website;</li><li>Redistribute material from this website.</li></ul><b>ACCEPTABLE USE\r\n</b><ul><li>You must not use this website in any way that causes, or may cause, \r\ndamage to the website or impairment of the availability or accessibility\r\n of the website; or in any way which is unlawful, illegal, fraudulent or\r\n harmful, or in connection with any unlawful, illegal, fraudulent or \r\nharmful purpose or activity.</li><li>You must not use this website to copy, store, host, transmit, send, \r\nuse, publish or distribute any material which consists of (or is linked \r\nto) any spyware, computer virus, Trojan horse, worm, keystroke logger, \r\nrootkit or other malicious computer software.</li><li>You must not conduct any systematic or automated data collection \r\nactivities (including without limitation scraping, data mining, data \r\nextraction and data harvesting) on or in relation to this website \r\nwithout Laravel Ecommerce’s express written consent.</li><li>You must not use this website to transmit or send unsolicited commercial communications.</li><li>You must not use this website for any purposes related to marketing without Laravel Ecommerce’s express written consent.</li></ul><b>RESTRICTED ACCESS\r\n\r\n </b><br>[Access to certain areas of this website is \r\nrestricted.]  Laravel Ecommerce’s reserves the right to restrict access \r\nto [other] areas of this website, or indeed this entire website, at \r\nLaravel Ecommerce discretion.<br>\r\n\r\n If Laravel Ecommerce’s provides you with a user ID \r\nand password to enable you to access restricted areas of this website or\r\n other content or services, you must ensure that the user ID and \r\npassword are kept confidential.<br>\r\n\r\n Laravel Ecommerce’s may disable your user ID and password in Laravel Ecommerce’s sole discretion without notice or explanation.]<br><br><b>USER CONTENT</b>\r\n\r\n <br>In these terms and conditions, “your user content” \r\nmeans material (including without limitation text, images, audio \r\nmaterial, video material and audio-visual material) that you submit to \r\nthis website, for whatever purpose.<br>\r\n\r\n You grant to Laravel Ecommerce a worldwide, \r\nirrevocable, non-exclusive, royalty-free license to use, reproduce, \r\nadapt, publish, translate and distribute your user content in any \r\nexisting or future media.  You also grant to the right to sub-license \r\nthese rights, and the right to bring an action for infringement of these\r\n rights.<br>\r\n\r\n Your user content must not be illegal or unlawful, \r\nmust not infringe any third party’s legal rights, and must not be \r\ncapable of giving rise to legal action whether against you or Laravel \r\nEcommerce or a third party.<br>\r\n\r\n You must not submit any user content to the website\r\n that is or has ever been the subject of any threatened or actual legal \r\nproceedings or other similar complaint.<br>\r\n\r\n Laravel Ecommerce reserves the right to edit or \r\nremove any material submitted to this website, or stored on Laravel \r\nEcommerce’s servers, or hosted or published upon this website.\r\n<br><br><b>LIMITATIONS OF LIABILITY</b>\r\n\r\n <br>Laravel Ecommerce will not be liable to you \r\n(whether under the law of contact, the law of torts or otherwise) in \r\nrelation to the contents of, or use of, or otherwise in connection with,\r\n this website:<br>\r\n\r\n \r\n\r\n For any indirect, special or consequential loss; or\r\nFor any business losses, loss of revenue, income, profits or anticipated\r\n savings, loss of contracts or business relationships, loss of \r\nreputation or goodwill, or loss or corruption of information or data.<br>\r\n \r\n\r\n These limitations of liability apply even if Laravel Ecommerce has been expressly advised of the potential loss.<br><br><b>REASONABLENESS</b>\r\n\r\n <br>By using this website, you agree that the \r\nexclusions and limitations of liability set out in this website \r\ndisclaimer are reasonable.<br>\r\n\r\n If you do not think they are reasonable, you must not use this website.<br><br><b>UNENFORCEABLE PROVISIONS<br></b>&nbsp;If any provision of this website disclaimer is, or \r\nis found to be, unenforceable under applicable law, that will not affect\r\n the enforceability of the other provisions of this website disclaimer.<br><br><b>BREACHES OF THESE TERMS AND CONDITIONS<br></b>&nbsp;Without prejudice to Laravel Ecommerce’s other \r\nrights under these terms and conditions, if you breach these terms and \r\nconditions in any way, Laravel Ecommerce may take such action as Laravel\r\n Ecommerce deems appropriate to deal with the breach, including \r\nsuspending your access to this website, prohibiting you from accessing \r\nthe website, blocking computers using your IP address from accessing the\r\n website, contacting your internet service provider to request that they\r\n block your access to the website and/or bringing court proceedings \r\nagainst you.<br><br><b>VARIATION<br></b>&nbsp;Laravel Ecommerce may revise these terms and \r\nconditions from time-to-time.  Revised terms and conditions will apply \r\nto the use of this website from the date of the publication of the \r\nrevised terms and conditions on this website.  Please check this page \r\nregularly to ensure you are familiar with the current version.<br><br><b>ASSIGNMENT<br></b>&nbsp;Laravel Ecommerce may transfer, sub-contract or \r\notherwise deal with Laravel Ecommerce rights and/or obligations under \r\nthese terms and conditions without notifying you or obtaining your \r\nconsent.<br>\r\n\r\n You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms and conditions.<br><br><b>SEVERABILITY<br></b>&nbsp;If a provision of these terms and conditions is \r\ndetermined by any court or other competent authority to be unlawful \r\nand/or unenforceable, the other provisions will continue in effect.  If \r\nany unlawful and/or unenforceable provision would be lawful or \r\nenforceable if part of it were deleted, that part will be deemed to be \r\ndeleted, and the rest of the provision will continue in effect.<br><br><b>LAW AND JURISDICTION<br></b>&nbsp;These terms and conditions will be governed by and \r\nconstrued in accordance with Indian law, and any disputes relating to \r\nthese terms and conditions will be subject to the exclusive jurisdiction\r\n of the courts of Coimbatore.<br><br><br><br>', '2016-06-09 22:39:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_theme`
--

CREATE TABLE `nm_theme` (
  `the_id` smallint(5) UNSIGNED NOT NULL,
  `the_Name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_wishlist`
--

CREATE TABLE `nm_wishlist` (
  `ws_id` bigint(20) UNSIGNED NOT NULL,
  `ws_pro_id` bigint(20) UNSIGNED NOT NULL,
  `ws_cus_id` bigint(20) UNSIGNED NOT NULL,
  `ws_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_wishlist`
--

INSERT INTO `nm_wishlist` (`ws_id`, `ws_pro_id`, `ws_cus_id`, `ws_date`) VALUES
(1, 55, 4, '2016-04-27 05:03:47'),
(2, 54, 4, '2016-04-27 05:04:21'),
(3, 55, 14, '2016-05-27 06:36:55'),
(4, 55, 15, '2016-05-28 06:38:33'),
(16, 69, 16, '2016-05-28 13:08:42'),
(17, 70, 15, '2016-05-30 04:51:10'),
(18, 62, 16, '2016-05-30 05:47:22'),
(19, 62, 16, '2016-05-30 05:49:34'),
(20, 62, 16, '2016-05-30 06:05:03'),
(21, 62, 16, '2016-05-30 06:06:27'),
(22, 62, 16, '2016-05-30 06:08:34'),
(23, 50, 16, '2016-05-30 06:08:47'),
(24, 55, 16, '2016-05-30 06:10:49'),
(25, 70, 16, '2016-05-30 06:41:49'),
(26, 69, 16, '2016-05-30 06:57:58'),
(27, 69, 16, '2016-05-30 06:59:21'),
(28, 69, 16, '2016-05-30 07:00:02'),
(29, 69, 16, '2016-05-30 07:00:48'),
(30, 69, 16, '2016-05-30 07:02:10'),
(31, 69, 16, '2016-05-30 07:02:51'),
(32, 69, 16, '2016-05-30 07:12:18'),
(33, 68, 16, '2016-05-30 07:13:50'),
(34, 68, 16, '2016-05-30 07:14:48'),
(35, 55, 16, '2016-05-30 07:16:23'),
(36, 71, 16, '2016-05-30 07:19:18'),
(37, 2, 14, '2016-06-07 08:15:47'),
(38, 9, 23, '2016-06-08 12:07:36'),
(39, 16, 48, '2016-06-21 10:09:19'),
(40, 9, 48, '2016-06-21 10:09:47'),
(41, 11, 58, '2016-06-22 06:34:46'),
(42, 25, 94, '2016-08-16 18:48:18'),
(43, 25, 125, '2016-11-15 14:02:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_withdraw_request`
--

CREATE TABLE `nm_withdraw_request` (
  `wd_id` int(11) NOT NULL,
  `wd_mer_id` int(11) NOT NULL,
  `wd_total_wd_amt` int(11) NOT NULL,
  `wd_submited_wd_amt` int(11) NOT NULL,
  `wd_status` int(11) NOT NULL COMMENT '1 => paid, 0=> Hold',
  `wd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_withdraw_request`
--

INSERT INTO `nm_withdraw_request` (`wd_id`, `wd_mer_id`, `wd_total_wd_amt`, `wd_submited_wd_amt`, `wd_status`, `wd_date`) VALUES
(1, 6, 6375, 3375, 0, '2014-08-13 01:34:05'),
(2, 1, 27360, 14700, 0, '2016-05-13 07:30:30'),
(3, 15, 13986, 11598, 0, '2016-10-05 07:29:32'),
(4, 9, 355786, 20, 0, '2016-11-02 00:27:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nm_withdraw_request_paypal`
--

CREATE TABLE `nm_withdraw_request_paypal` (
  `wr_id` int(11) NOT NULL,
  `wr_mer_id` int(11) NOT NULL,
  `wr_mer_name` varchar(250) NOT NULL,
  `wr_mer_payment_email` varchar(250) NOT NULL,
  `wr_paid_amount` varchar(250) NOT NULL,
  `wr_txn_id` varchar(250) NOT NULL,
  `wr_status` varchar(100) NOT NULL,
  `wr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nm_withdraw_request_paypal`
--

INSERT INTO `nm_withdraw_request_paypal` (`wr_id`, `wr_mer_id`, `wr_mer_name`, `wr_mer_payment_email`, `wr_paid_amount`, `wr_txn_id`, `wr_status`, `wr_date`) VALUES
(1, 6, 'shankar', 'marimuthu@ideazglobal.com', '3000.00', '7UM45757EY5785036', 'Pending', '2014-08-13 01:20:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `session_id`, `expire_time`, `created_at`, `updated_at`) VALUES
('0cgqFCMQ5T2oclDHHYvgy52snB0e6PUlwZi69z93', 12, 1489638218, '2017-03-08 21:23:38', '2017-03-08 21:23:38'),
('cA7ICCX7EcwtedobiLlpBkOJrVaEee9cIoLtd5hT', 20, 1489639469, '2017-03-08 21:44:29', '2017-03-08 21:44:29'),
('chLhhQ0yiNAOHPi5GYuDsDcjPj3iS3GlY90y0vfe', 4, 1489637176, '2017-03-08 21:06:16', '2017-03-08 21:06:16'),
('Eu9xW1EMBmh8TTN3ul5KpqfQb9o0b0GCZqVORSWB', 3, 1489035579, '2017-03-01 21:59:39', '2017-03-01 21:59:39'),
('FpLZ2yFRl7MJwIEh8WRjkhWoCwe6LFlBDDliK042', 2, 1489035291, '2017-03-01 21:54:51', '2017-03-01 21:54:51'),
('gBnzEQChJJDA8TIczFAqrUPGxiulB1oK1rZKjq5K', 19, 1489639265, '2017-03-08 21:41:05', '2017-03-08 21:41:05'),
('GSvwl3sE13JqCal6QMVLP8CrDHXheqmDxuBtajyu', 17, 1489638801, '2017-03-08 21:33:22', '2017-03-08 21:33:22'),
('HNufId0GwGzxTnZfsrsYkmAvxZ1GBrgEjzU0Lcb3', 9, 1489637739, '2017-03-08 21:15:39', '2017-03-08 21:15:39'),
('HYuHgoe2MAf4cWZb5cDcj3ZJOuyaMVS3Q7sbQdpd', 10, 1489637976, '2017-03-08 21:19:36', '2017-03-08 21:19:36'),
('iQA6lwSSdD5ww3yBDxYrPEQmlZkNHxp6ws1YYpWF', 16, 1489638757, '2017-03-08 21:32:37', '2017-03-08 21:32:37'),
('jeTf3mO6zl8hfclY7NAbB73HjYX8vxHOWPxDf54j', 15, 1489638660, '2017-03-08 21:31:00', '2017-03-08 21:31:00'),
('kNqq3BdiCIZp7UlJ96BSmX3T3NoPZBwdupG2Ewwn', 18, 1489639048, '2017-03-08 21:37:28', '2017-03-08 21:37:28'),
('kUUtICAgo8ge9r5vKghFHsCSMGfYc7ZHrug0UX61', 13, 1489638319, '2017-03-08 21:25:19', '2017-03-08 21:25:19'),
('og7c7lDHlRyLWdOatEvOtyqj9YgtyBtAGziVUGYk', 7, 1489637460, '2017-03-08 21:11:00', '2017-03-08 21:11:00'),
('pUidrstKZsvZBuq0Icpu7zOcfTLMZQdp24i8ix3D', 5, 1489637186, '2017-03-08 21:06:26', '2017-03-08 21:06:26'),
('Pzr7GYprS6KEEPCuHp8Wk5mGOkvsLCJqPRWVJNpB', 11, 1489638085, '2017-03-08 21:21:25', '2017-03-08 21:21:25'),
('uWWhLf3Xnndk4tMEQq79YhO8pO0asjSSUiyWvzfl', 6, 1489637347, '2017-03-08 21:09:07', '2017-03-08 21:09:07'),
('WW7sU7BVz0sU2M5AQEF8n6YPIj3APz5AGGMVJCAt', 1, 1489032684, '2017-03-01 21:11:25', '2017-03-01 21:11:25'),
('YgFOAzesnIXqKjlM4gMxmXUquJvUua3u2LsvSdP6', 14, 1489638480, '2017-03-08 21:28:01', '2017-03-08 21:28:01'),
('YPKREVRwurtqTGJodtZWnjJzut5gCydJZ13AT0Ud', 8, 1489637662, '2017-03-08 21:14:22', '2017-03-08 21:14:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_access_token_scopes`
--

CREATE TABLE `oauth_access_token_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_auth_code_scopes`
--

CREATE TABLE `oauth_auth_code_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `auth_code_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `created_at`, `updated_at`) VALUES
('wms', 'wms_secret987635', 'wms', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_client_endpoints`
--

CREATE TABLE `oauth_client_endpoints` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_client_grants`
--

CREATE TABLE `oauth_client_grants` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_client_scopes`
--

CREATE TABLE `oauth_client_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_grants`
--

CREATE TABLE `oauth_grants` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_grant_scopes`
--

CREATE TABLE `oauth_grant_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_sessions`
--

CREATE TABLE `oauth_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `oauth_sessions`
--

INSERT INTO `oauth_sessions` (`id`, `client_id`, `owner_type`, `owner_id`, `client_redirect_uri`, `created_at`, `updated_at`) VALUES
(1, 'wms', 'client', 'wms', NULL, '2017-03-01 21:11:25', '2017-03-01 21:11:25'),
(2, 'wms', 'client', 'wms', NULL, '2017-03-01 21:54:51', '2017-03-01 21:54:51'),
(3, 'wms', 'client', 'wms', NULL, '2017-03-01 21:59:39', '2017-03-01 21:59:39'),
(4, 'wms', 'client', 'wms', NULL, '2017-03-08 21:06:16', '2017-03-08 21:06:16'),
(5, 'wms', 'client', 'wms', NULL, '2017-03-08 21:06:26', '2017-03-08 21:06:26'),
(6, 'wms', 'client', 'wms', NULL, '2017-03-08 21:09:07', '2017-03-08 21:09:07'),
(7, 'wms', 'client', 'wms', NULL, '2017-03-08 21:11:00', '2017-03-08 21:11:00'),
(8, 'wms', 'client', 'wms', NULL, '2017-03-08 21:14:22', '2017-03-08 21:14:22'),
(9, 'wms', 'client', 'wms', NULL, '2017-03-08 21:15:39', '2017-03-08 21:15:39'),
(10, 'wms', 'client', 'wms', NULL, '2017-03-08 21:19:36', '2017-03-08 21:19:36'),
(11, 'wms', 'client', 'wms', NULL, '2017-03-08 21:21:25', '2017-03-08 21:21:25'),
(12, 'wms', 'client', 'wms', NULL, '2017-03-08 21:23:38', '2017-03-08 21:23:38'),
(13, 'wms', 'client', 'wms', NULL, '2017-03-08 21:25:19', '2017-03-08 21:25:19'),
(14, 'wms', 'client', 'wms', NULL, '2017-03-08 21:28:00', '2017-03-08 21:28:00'),
(15, 'wms', 'client', 'wms', NULL, '2017-03-08 21:31:00', '2017-03-08 21:31:00'),
(16, 'wms', 'client', 'wms', NULL, '2017-03-08 21:32:37', '2017-03-08 21:32:37'),
(17, 'wms', 'client', 'wms', NULL, '2017-03-08 21:33:21', '2017-03-08 21:33:21'),
(18, 'wms', 'client', 'wms', NULL, '2017-03-08 21:37:28', '2017-03-08 21:37:28'),
(19, 'wms', 'client', 'wms', NULL, '2017-03-08 21:41:05', '2017-03-08 21:41:05'),
(20, 'wms', 'client', 'wms', NULL, '2017-03-08 21:44:29', '2017-03-08 21:44:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_session_scopes`
--

CREATE TABLE `oauth_session_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_aboutus`
--
ALTER TABLE `nm_aboutus`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `nm_add`
--
ALTER TABLE `nm_add`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `nm_admin`
--
ALTER TABLE `nm_admin`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indexes for table `nm_adminreply_comments`
--
ALTER TABLE `nm_adminreply_comments`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `nm_auction`
--
ALTER TABLE `nm_auction`
  ADD PRIMARY KEY (`auc_id`);

--
-- Indexes for table `nm_banner`
--
ALTER TABLE `nm_banner`
  ADD PRIMARY KEY (`bn_id`);

--
-- Indexes for table `nm_blog`
--
ALTER TABLE `nm_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `nm_blogsetting`
--
ALTER TABLE `nm_blogsetting`
  ADD PRIMARY KEY (`bs_id`);

--
-- Indexes for table `nm_blog_cus_comments`
--
ALTER TABLE `nm_blog_cus_comments`
  ADD PRIMARY KEY (`cmt_id`);

--
-- Indexes for table `nm_city`
--
ALTER TABLE `nm_city`
  ADD PRIMARY KEY (`ci_id`);

--
-- Indexes for table `nm_cms_pages`
--
ALTER TABLE `nm_cms_pages`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `nm_color`
--
ALTER TABLE `nm_color`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `nm_colorfixed`
--
ALTER TABLE `nm_colorfixed`
  ADD PRIMARY KEY (`cf_id`);

--
-- Indexes for table `nm_contact`
--
ALTER TABLE `nm_contact`
  ADD PRIMARY KEY (`cont_id`);

--
-- Indexes for table `nm_country`
--
ALTER TABLE `nm_country`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `nm_currency`
--
ALTER TABLE `nm_currency`
  ADD PRIMARY KEY (`cur_id`);

--
-- Indexes for table `nm_customer`
--
ALTER TABLE `nm_customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `nm_deals`
--
ALTER TABLE `nm_deals`
  ADD PRIMARY KEY (`deal_id`);

--
-- Indexes for table `nm_emailsetting`
--
ALTER TABLE `nm_emailsetting`
  ADD PRIMARY KEY (`es_id`);

--
-- Indexes for table `nm_enquiry`
--
ALTER TABLE `nm_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_estimate_zipcode`
--
ALTER TABLE `nm_estimate_zipcode`
  ADD PRIMARY KEY (`ez_id`);

--
-- Indexes for table `nm_faq`
--
ALTER TABLE `nm_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `nm_generalsetting`
--
ALTER TABLE `nm_generalsetting`
  ADD PRIMARY KEY (`gs_id`);

--
-- Indexes for table `nm_imagesetting`
--
ALTER TABLE `nm_imagesetting`
  ADD PRIMARY KEY (`imgs_id`);

--
-- Indexes for table `nm_inquiries`
--
ALTER TABLE `nm_inquiries`
  ADD PRIMARY KEY (`iq_id`);

--
-- Indexes for table `nm_language`
--
ALTER TABLE `nm_language`
  ADD UNIQUE KEY `id` (`la_id`);

--
-- Indexes for table `nm_login`
--
ALTER TABLE `nm_login`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `nm_maincategory`
--
ALTER TABLE `nm_maincategory`
  ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `nm_merchant`
--
ALTER TABLE `nm_merchant`
  ADD PRIMARY KEY (`mer_id`);

--
-- Indexes for table `nm_modulesettings`
--
ALTER TABLE `nm_modulesettings`
  ADD PRIMARY KEY (`ms_id`);

--
-- Indexes for table `nm_newsletter_subscribers`
--
ALTER TABLE `nm_newsletter_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nm_order`
--
ALTER TABLE `nm_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `nm_ordercod`
--
ALTER TABLE `nm_ordercod`
  ADD PRIMARY KEY (`cod_id`);

--
-- Indexes for table `nm_order_auction`
--
ALTER TABLE `nm_order_auction`
  ADD PRIMARY KEY (`oa_id`);

--
-- Indexes for table `nm_paymentsettings`
--
ALTER TABLE `nm_paymentsettings`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `nm_procart`
--
ALTER TABLE `nm_procart`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `nm_procolor`
--
ALTER TABLE `nm_procolor`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `nm_product`
--
ALTER TABLE `nm_product`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `nm_prosize`
--
ALTER TABLE `nm_prosize`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `nm_prospec`
--
ALTER TABLE `nm_prospec`
  ADD PRIMARY KEY (`spc_id`);

--
-- Indexes for table `nm_referaluser`
--
ALTER TABLE `nm_referaluser`
  ADD PRIMARY KEY (`ruse_id`);

--
-- Indexes for table `nm_review`
--
ALTER TABLE `nm_review`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `nm_secmaincategory`
--
ALTER TABLE `nm_secmaincategory`
  ADD PRIMARY KEY (`smc_id`);

--
-- Indexes for table `nm_secsubcategory`
--
ALTER TABLE `nm_secsubcategory`
  ADD PRIMARY KEY (`ssb_id`);

--
-- Indexes for table `nm_shipping`
--
ALTER TABLE `nm_shipping`
  ADD PRIMARY KEY (`ship_id`);

--
-- Indexes for table `nm_size`
--
ALTER TABLE `nm_size`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `nm_smtp`
--
ALTER TABLE `nm_smtp`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `nm_social_media`
--
ALTER TABLE `nm_social_media`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `nm_specification`
--
ALTER TABLE `nm_specification`
  ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `nm_spgroup`
--
ALTER TABLE `nm_spgroup`
  ADD PRIMARY KEY (`spg_id`);

--
-- Indexes for table `nm_store`
--
ALTER TABLE `nm_store`
  ADD PRIMARY KEY (`stor_id`);

--
-- Indexes for table `nm_subcategory`
--
ALTER TABLE `nm_subcategory`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `nm_subscription`
--
ALTER TABLE `nm_subscription`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `nm_terms`
--
ALTER TABLE `nm_terms`
  ADD PRIMARY KEY (`tr_id`);

--
-- Indexes for table `nm_theme`
--
ALTER TABLE `nm_theme`
  ADD PRIMARY KEY (`the_id`);

--
-- Indexes for table `nm_wishlist`
--
ALTER TABLE `nm_wishlist`
  ADD PRIMARY KEY (`ws_id`);

--
-- Indexes for table `nm_withdraw_request`
--
ALTER TABLE `nm_withdraw_request`
  ADD PRIMARY KEY (`wd_id`);

--
-- Indexes for table `nm_withdraw_request_paypal`
--
ALTER TABLE `nm_withdraw_request_paypal`
  ADD PRIMARY KEY (`wr_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`),
  ADD KEY `oauth_access_tokens_session_id_index` (`session_id`);

--
-- Indexes for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`),
  ADD KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_session_id_index` (`session_id`);

--
-- Indexes for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`),
  ADD KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`);

--
-- Indexes for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`);

--
-- Indexes for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_client_grants_client_id_index` (`client_id`),
  ADD KEY `oauth_client_grants_grant_id_index` (`grant_id`);

--
-- Indexes for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_client_scopes_client_id_index` (`client_id`),
  ADD KEY `oauth_client_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_grants`
--
ALTER TABLE `oauth_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_grant_scopes_grant_id_index` (`grant_id`),
  ADD KEY `oauth_grant_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`access_token_id`),
  ADD UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`);

--
-- Indexes for table `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`);

--
-- Indexes for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_session_scopes_session_id_index` (`session_id`),
  ADD KEY `oauth_session_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_aboutus`
--
ALTER TABLE `nm_aboutus`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_add`
--
ALTER TABLE `nm_add`
  MODIFY `ad_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nm_admin`
--
ALTER TABLE `nm_admin`
  MODIFY `adm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_adminreply_comments`
--
ALTER TABLE `nm_adminreply_comments`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nm_auction`
--
ALTER TABLE `nm_auction`
  MODIFY `auc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nm_banner`
--
ALTER TABLE `nm_banner`
  MODIFY `bn_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `nm_blog`
--
ALTER TABLE `nm_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nm_blogsetting`
--
ALTER TABLE `nm_blogsetting`
  MODIFY `bs_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_blog_cus_comments`
--
ALTER TABLE `nm_blog_cus_comments`
  MODIFY `cmt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nm_city`
--
ALTER TABLE `nm_city`
  MODIFY `ci_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nm_cms_pages`
--
ALTER TABLE `nm_cms_pages`
  MODIFY `cp_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `nm_color`
--
ALTER TABLE `nm_color`
  MODIFY `co_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `nm_colorfixed`
--
ALTER TABLE `nm_colorfixed`
  MODIFY `cf_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `nm_contact`
--
ALTER TABLE `nm_contact`
  MODIFY `cont_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_country`
--
ALTER TABLE `nm_country`
  MODIFY `co_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `nm_currency`
--
ALTER TABLE `nm_currency`
  MODIFY `cur_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_customer`
--
ALTER TABLE `nm_customer`
  MODIFY `cus_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `nm_deals`
--
ALTER TABLE `nm_deals`
  MODIFY `deal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `nm_emailsetting`
--
ALTER TABLE `nm_emailsetting`
  MODIFY `es_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_enquiry`
--
ALTER TABLE `nm_enquiry`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `nm_estimate_zipcode`
--
ALTER TABLE `nm_estimate_zipcode`
  MODIFY `ez_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nm_faq`
--
ALTER TABLE `nm_faq`
  MODIFY `faq_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nm_generalsetting`
--
ALTER TABLE `nm_generalsetting`
  MODIFY `gs_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_imagesetting`
--
ALTER TABLE `nm_imagesetting`
  MODIFY `imgs_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nm_inquiries`
--
ALTER TABLE `nm_inquiries`
  MODIFY `iq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nm_language`
--
ALTER TABLE `nm_language`
  MODIFY `la_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_login`
--
ALTER TABLE `nm_login`
  MODIFY `log_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=412;
--
-- AUTO_INCREMENT for table `nm_maincategory`
--
ALTER TABLE `nm_maincategory`
  MODIFY `mc_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nm_merchant`
--
ALTER TABLE `nm_merchant`
  MODIFY `mer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `nm_modulesettings`
--
ALTER TABLE `nm_modulesettings`
  MODIFY `ms_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_newsletter_subscribers`
--
ALTER TABLE `nm_newsletter_subscribers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `nm_order`
--
ALTER TABLE `nm_order`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;
--
-- AUTO_INCREMENT for table `nm_ordercod`
--
ALTER TABLE `nm_ordercod`
  MODIFY `cod_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;
--
-- AUTO_INCREMENT for table `nm_order_auction`
--
ALTER TABLE `nm_order_auction`
  MODIFY `oa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `nm_paymentsettings`
--
ALTER TABLE `nm_paymentsettings`
  MODIFY `ps_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_procart`
--
ALTER TABLE `nm_procart`
  MODIFY `pc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_procolor`
--
ALTER TABLE `nm_procolor`
  MODIFY `pc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=535;
--
-- AUTO_INCREMENT for table `nm_product`
--
ALTER TABLE `nm_product`
  MODIFY `pro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `nm_prosize`
--
ALTER TABLE `nm_prosize`
  MODIFY `ps_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=618;
--
-- AUTO_INCREMENT for table `nm_prospec`
--
ALTER TABLE `nm_prospec`
  MODIFY `spc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `nm_referaluser`
--
ALTER TABLE `nm_referaluser`
  MODIFY `ruse_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_review`
--
ALTER TABLE `nm_review`
  MODIFY `comment_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `nm_secmaincategory`
--
ALTER TABLE `nm_secmaincategory`
  MODIFY `smc_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `nm_secsubcategory`
--
ALTER TABLE `nm_secsubcategory`
  MODIFY `ssb_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `nm_shipping`
--
ALTER TABLE `nm_shipping`
  MODIFY `ship_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=742;
--
-- AUTO_INCREMENT for table `nm_size`
--
ALTER TABLE `nm_size`
  MODIFY `si_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `nm_smtp`
--
ALTER TABLE `nm_smtp`
  MODIFY `sm_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nm_social_media`
--
ALTER TABLE `nm_social_media`
  MODIFY `sm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_specification`
--
ALTER TABLE `nm_specification`
  MODIFY `sp_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `nm_spgroup`
--
ALTER TABLE `nm_spgroup`
  MODIFY `spg_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nm_store`
--
ALTER TABLE `nm_store`
  MODIFY `stor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `nm_subcategory`
--
ALTER TABLE `nm_subcategory`
  MODIFY `sb_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `nm_subscription`
--
ALTER TABLE `nm_subscription`
  MODIFY `sub_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_terms`
--
ALTER TABLE `nm_terms`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nm_theme`
--
ALTER TABLE `nm_theme`
  MODIFY `the_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nm_wishlist`
--
ALTER TABLE `nm_wishlist`
  MODIFY `ws_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `nm_withdraw_request`
--
ALTER TABLE `nm_withdraw_request`
  MODIFY `wd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nm_withdraw_request_paypal`
--
ALTER TABLE `nm_withdraw_request_paypal`
  MODIFY `wr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD CONSTRAINT `oauth_access_tokens_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  ADD CONSTRAINT `oauth_access_token_scopes_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_access_token_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD CONSTRAINT `oauth_auth_codes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  ADD CONSTRAINT `oauth_auth_code_scopes_auth_code_id_foreign` FOREIGN KEY (`auth_code_id`) REFERENCES `oauth_auth_codes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_auth_code_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD CONSTRAINT `oauth_client_endpoints_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  ADD CONSTRAINT `oauth_client_grants_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `oauth_client_grants_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  ADD CONSTRAINT `oauth_client_scopes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_client_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  ADD CONSTRAINT `oauth_grant_scopes_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_grant_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD CONSTRAINT `oauth_refresh_tokens_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD CONSTRAINT `oauth_sessions_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  ADD CONSTRAINT `oauth_session_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_session_scopes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
