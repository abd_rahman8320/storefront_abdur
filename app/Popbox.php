<?php

namespace App;
use Cache;
use Carbon\Carbon;

//Helpers
class Popbox
{
    const TOKEN = 'ff0256b4496f35b54c61558a7c5bb3be8267cbf7';
    const URL = 'http://api-dev.popbox.asia/';

    static function getCountry()
    {
        $url = self::URL.'locker/country';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('POST', $url, [
                'json' =>
                [
                    'token' => self::TOKEN
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }
        return json_decode($res->getBody(), true);
    }

    static function getProvince($country)
    {
        $url = self::URL.'locker/province';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('POST', $url, [
                'json' =>
                [
                    'token' => self::TOKEN,
                    'country' => $country
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }
        return json_decode($res->getBody(), true);
    }

    static function getCity($country)
    {
        $url = self::URL.'locker/city';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('POST', $url, [
                'json' =>
                [
                    'token' => self::TOKEN,
                    'country' => $country
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }
        return json_decode($res->getBody(), true);
    }

    static function getLocation($country, $city)
    {
        $url = self::URL.'locker/location';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('POST', $url, [
                'json' =>
                [
                    'token' => self::TOKEN,
                    'country' => $country,
                    'city' => $city,
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }
        return json_decode($res->getBody(), true);
    }

    static function makeRequest($order_no, $phone, $popbox_location, $pickup_location, $customer_email, $detail_items, $price, $seller_phone, $weight)
    {
        $url = self::URL.'merchant/pickup';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' =>
            [
                'token' => self::TOKEN,
                'order_no' => $order_no,
                'phone' => $phone,
                'popbox_location' => $popbox_location,
                'pickup_location' => $pickup_location,
                'customer_email' => $customer_email,
                'detail_items' => $detail_items,
                'price' => $price,
                'seller_phone' => $seller_phone,
                'weight' => $weight,
            ]
        ]);
        return json_decode($res->getBody(), true);
    }

    static function parcelStatus($order_number)
    {
        $url = self::URL.'parcel/status';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' =>
            [
                'token' => self::TOKEN,
                'order_number' => $order_number
            ]
        ]);
        return json_decode($res->getBody(), true);
    }
}

?>
