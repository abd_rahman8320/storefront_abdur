<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Privileges extends Model
{
    use SoftDeletes;
    
    protected $table = 'nm_privileges';
    protected $primaryKey = 'priv_id';
    const CREATED_AT = 'created_ts';
    const UPDATED_AT = 'lastupdated_ts';
    const DELETED_AT = 'deleted_ts';

    protected $fillable = [
        'priv_name',
        'priv_label',
        'priv_description',
        'created_by',
        'lastupdated_by',
        'deleted_by',
        'is_deleted'
    ];

    protected $dates = ['deleted_ts'];

    public function roles_privileges()
    {
        return $this->hasMany('App\RolesPrivileges', 'priv_name');
    }
}
?>
