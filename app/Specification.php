<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Specification extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_specification';

    public static function viewjoin_specification_detail()
    {
        $get_first_spgroup = DB::table('nm_spgroup')->orderBy('spg_name', 'asc')->first();
        $spg_id = $get_first_spgroup->spg_id;
        return DB::table('nm_specification')->join('nm_spgroup', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')->orderBy('nm_spgroup.spg_name', 'asc')->orderBy('nm_specification.sp_order', 'asc')->where('sp_spg_id', $spg_id)->get();
    }

    public static function view_specification_detail()
    {
        return DB::table('nm_specification')->get();
    }

    public static function showindividual_specification_detail($id)
    {
        return DB::table('nm_specification')->where('sp_id', '=', $id)->get();
    }


    public static function delete_specification_detail($id)
    {
        return DB::table('nm_specification')->where('sp_id', '=', $id)->delete();
    }

    public static function update_specification_detail($id, $entry)
    {
        return DB::table('nm_specification')->where('sp_id', '=', $id)->update($entry);
    }

    public static function save_specification_detail($entry)
    {
        return DB::table('nm_specification')->insert($entry);
    }

    public static function get_specification2($entry)
    {
        return DB::table ('nm_specification')->where('sp_name', $entry['sp_name'])->where('sp_spg_id', $entry['sp_spg_id'])->first();
    }
    public static function save_specification_group($entry)
    {
        return DB::table('nm_spgroup')->insert($entry);
    }
   
    public static function check_exist_individual($name, $groupid)
    {
        return DB::table('nm_specification')->where('sp_name', '=', $name)->where('sp_spg_id', '=', $groupid)->get();
    }

    public static function check_exist_update($name, $groupid, $id)
    {
        return DB::table('nm_specification')->where('sp_name', '=', $name)->where('sp_spg_id', '=', $groupid)->where('sp_id', '!=', $id)->get();
    }

    public static function check_exist_individualorder($order, $groupid)
    {
        return DB::table('nm_specification')->where('sp_order', '=', $order)->where('sp_spg_id', '=', $groupid)->get();
    }

    public static function check_exist_individualorder_update($order, $groupid, $id)
    {
        return DB::table('nm_specification')->where('sp_order', '=', $order)->where('sp_spg_id', '=', $groupid)->where('sp_id', '!=', $id)->get();
    }

    public static function check_exist_group($name)
    {
        return DB::table('nm_spgroup')->where('spg_name', '=', $name)->get();
    }

    public static function check_exist_order($order)
    {
        return DB::table('nm_spgroup')->where('spg_order', '=', $order)->get();
    }

    public static function check_exist_group_update($name, $id)
    {
        return DB::table('nm_spgroup')->where('spg_name', '=', $name)->where('spg_id', '!=', $id)->get();
    }

    public static function check_exist_order_update($order, $id)
    {
        return DB::table('nm_spgroup')->where('spg_order', '=', $order)->where('spg_id', '!=', $id)->get();
    }

    public static function get_specification_group()
    {
        return DB::table('nm_spgroup')->orderBy('spg_name', 'asc')->get();
    }

    public static function edit_specification($spec_id)
    {
        return DB::table('nm_spec_value')->where('nm_specification_id','=',$spec_id)->orderBy('code_value', 'asc')->get();
    }

    public static function get_specification_value()
    {
        return DB::table('nm_spec_value')->leftJoin('nm_specification', 'nm_specification.sp_id', '=', 'nm_spec_value.nm_specification_id')->orderBy('short_desc', 'asc')->get();
    }

    public static function showindividual_specification_group_detail($id)
    {
        return DB::table('nm_spgroup')->where('spg_id', '=', $id)->get();
    }

    public static function update_specification_group($id, $entry)
    {
        return DB::table('nm_spgroup')->where('spg_id', '=', $id)->update($entry);
    }

    public static function delete_specification_group_detail($id)
    {
        return DB::table('nm_spgroup')->where('spg_id', '=', $id)->delete();
    }
    public static function get_specification_by_name($sp_name, $spg_id)
    {
        return DB::table('nm_specification')->where('sp_name', $sp_name)->where('sp_spg_id', $spg_id)->first();
    }
    public static function check_last_order_spec($spg_id)
    {
        return DB::table('nm_specification')->select('sp_order')->orderBy('sp_order', 'desc')->first();
    }
}
?>
