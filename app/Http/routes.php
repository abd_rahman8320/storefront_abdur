<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//cek queued
// Route::get('/', function(){
//     // some time to delay the job
//     $fiveSecs = \Carbon\Carbon::now()->addSeconds(5);
//     $tenSecs  = \Carbon\Carbon::now()->addSeconds(10);

//     // adds job to queue
//     Queue::later($fiveSecs, new App\Commands\SendCustomerMail('5 secs'));
//     Queue::later($tenSecs, new App\Commands\SendCustomerMail('10 secs'));

//     //$product_details              = Mail_Cus::get_mail_cus();

//     //Queue::push($fiveSecs, new App\Commands\SendCustomerMailSendEmail($message));

//     return 'All done';
// });

//todo remove
//indexing
Route::get('indexing', 'ProductController@indexing');
Route::get('bm25f', 'ProductController@bm25f');
Route::get('review_matrix', 'HomeController@review_matrix');
Route::get('neighbor', 'HomeController@neighbor');
Route::get('all_products_neighbour/{neighbours}', 'HomeController@all_products_neighbour');
Route::get('run_preprocessing', 'HomeController@run_preprocessing');
Route::get('run_preprocessing2', 'HomeController@run_preprocessing2');
Route::get('run_preprocessing3', 'HomeController@run_preprocessing3');

Route::get('slider', 'HomeController@slider');
Route::get('test', 'HomeController@test');
Route::get('carosel', 'HomeController@carosel');
Route::get('flashPromoView', 'HomeController@flashPromoView');
Route::get('loginto','HomeController@loginto');
Route::get('/', 'HomeController@index');
Route::get('get_promo_flash', 'PlanTimerController@get_promo_flash');
Route::get('get_paymentmethod_image', 'HomeController@get_paymentmethod_image');
Route::get('getting_count_klick/{id_banner}', 'HomeController@getting_count_klick');
Route::get('get_countpopup_click/{id_popup}', 'HomeController@get_countpopup_click');
Route::get('/search_product', 'PlanTimerController@search_product');
Route::get('/mer_search_product', 'PlanTimerController@mer_search_product');
Route::get('/search_filter', 'FilterController@search_filter');
Route::get('jplist_filter', 'FilterController@jplist_filter');
Route::get('/goa', 'HomeController@get_order_amount');
Route::get('index', 'HomeController@index');
Route::get('check_estimate_zipcode' , 'HomeController@check_estimate_zipcode');
Route::get('autosearch', 'HomeController@autosearch');
Route::get('my_wishlist','CustomerprofileController@get_userprofile');
Route::post('register_submit','HomeController@register_submit');
Route::get('account_verification/{email}', 'HomeController@account_verification');
Route::get('products', 'HomeController@products');
Route::get('productsflash', 'HomeController@products_flash');
Route::get('catproducts/{name}/{id}', 'HomeController@category_product_list');
Route::get('catdeals/{name}/{id}', 'HomeController@category_deal_list');
Route::get('catauction/{name}/{id}', 'HomeController@category_auction_list');
Route::get('productview/{mc}/{sc}/{sb}/{ssb}/{id}', 'HomeController@productview');
Route::get('productview_spesial/{mc}/{sc}/{sb}/{ssb}/{id}/{nama_product}', 'HomeController@productview_spesial'); // spesial
Route::get('product_flashPromo/{mc}/{sc}/{sb}/{ssb}/{id}/{promopFlashId}', 'HomeController@product_flashPromo');

Route::get('productcomment/{mc}/{sc}/{sb}/{ssb}/{id}', 'HomeController@productcomment');
Route::get('generate_voucher', 'nPembayaranController@generate_voucher');

Route::get('productview1/{mc}/{sc}/{sb}/{id}', 'HomeController@productview1');
Route::get('productview2/{mc}/{sc}/{id}', 'HomeController@productview2');

Route::get('pilih_produk_warna_cb', 'HomeController@pilih_produk_warna_cb');
Route::get('category_list/{id}', 'HomeController@category_list');
Route::get('/registers', 'HomeController@register');
Route::get('search', 'HomeController@search');
Route::post('subscription_submit','HomeController@subscription_submit');
Route::get('compare','HomeController@compare');
Route::post('addtowish','HomeController@addtowish');
Route::post('productcomments','HomeController@productcomments');
Route::post('dealcomments','HomeController@dealcomments');
Route::post('storecomments','HomeController@storecomments');
Route::get('/register',function(){
    $data = array('pageTitle'  =>  'Register');
    return View::make('index',$data);
});
Route::post('/register',function(){
    $inputData = Input::get('formData');
    parse_str($inputData, $formFields);
    $userData = array(
      'name'      => $formFields['name'],
      'email'     =>  $formFields['email'],
      'password'  =>  $formFields['password'],
      'password_confirmation' =>  $formFields[ 'password_confirmation'],
    );
    $rules = array(
        'name'      =>  'required',
        'email'     =>  'required|email|unique:users',
        'password'  =>  'required|min:6|confirmed',
    );
    $validator = Validator::make($userData,$rules);
    if($validator->fails())
        return Response::json(array(
            'fail' => true,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    else {
    //save password to show to user after registration
        $password = $userData['password'];
    //hash it now
        $userData['password'] =    Hash::make($userData['password']);
        unset($userData['password_confirmation']);
    //save to DB user details
      if(User::create($userData)) {
          //return success  message
        return Response::json(array(
          'success' => true,
          'email' => $userData['email'],
          'password'    =>  $password
        ));
      }
  }
});

Route::get('download_dokumen_merchant/{fname}', 'MerchantController@download_dokumen_merchant');
Route::get('clear_dokumen_merchant/{fname}', 'MerchantController@clear_dokumen_merchant');

Route::get('approval_merchant', 'MerchantController@approval_merchant');
Route::post('approval_merchant', 'MerchantController@approval_merchant');
Route::get('update_approve_merchant/{id_merchant}/{email}/{password}', 'MerchantController@update_approve_merchant');
Route::get('hapus_merchant/{id_merchant}', 'MerchantController@hapus_merchant');
Route::get('view_merchant_approval/{id_merchant}', 'MerchantController@view_merchant_approval');

Route::get('turun_harga', 'HomeController@turun_harga');
Route::get('kualitas_produk','HomeController@kualitas_produk');
Route::get('info_pembayaran/{id_transaksi}', 'CustomerprofileController@info_pembayaran');
Route::post('upload_bukti_transfer_bank_file', 'CustomerprofileController@upload_bukti_transfer_bank_file');
Route::get('download_file_bukti/{fname}', 'CustomerprofileController@download_file_bukti');
Route::get('hapus_file_bukti/{id_transaksi}', 'CustomerprofileController@hapus_file_bukti');
Route::post('konfirmasi_barang_cus', 'CustomerprofileController@konfirmasi_barang_cus');

Route::get('update_status_konfirmasi/{orderid}/{transaction_id}','CustomerprofileController@update_status_konfirmasi');
Route::get('download_invoice/{id_transaksi}','TransactionController@download_invoice_customer');

Route::get('deals', 'HomeController@deals');
Route::get('dealview/{id}', 'HomeController@dealview');
// Route::get('dealview/{mc}/{sc}/{sb}/{ssb}/{id}', 'HomeController@dealview');
Route::get('dealview2/{mc}/{sc}/{id}', 'HomeController@dealview2');
Route::get('dealview1/{mc}/{sc}/{sb}/{id}', 'HomeController@dealview1');
Route::get('auction', 'HomeController@auction');
Route::get('auctionview/{id}', 'HomeController@auctionview');
Route::get('stores', 'HomeController@stores');
Route::get('storeview/{id}', 'HomeController@storeview');
Route::get('sold', 'HomeController@sold');
Route::get('front_newsletter_submit', 'FooterController@front_newsletter_submit');
Route::get('check_title','FooterController@check_title');
Route::post('user_ad_ajax','FooterController@user_ad_ajax');
Route::get('register_getcountry','RegisterController@register_getcountry_ajax');
Route::get('register_emailcheck','RegisterController@register_emailcheck_ajax');
Route::post('user_login_submit','UserloginController@user_login_submit');
Route::get('user_profile','CustomerprofileController@get_userprofile');
Route::get('user_logout','CustomerprofileController@user_logout');
Route::get('facebook_logout','CustomerprofileController@facebook_logout');
Route::get('update_username_ajax','CustomerprofileController@update_username_ajax');
Route::post('update_password_ajax','CustomerprofileController@update_password_ajax');
Route::get('update_phonenumber_ajax','CustomerprofileController@update_phonenumber_ajax');
Route::get('update_address_ajax','CustomerprofileController@update_address_ajax');
Route::get('update_city_ajax','CustomerprofileController@update_city_ajax');
Route::get('update_shipping_ajax','CustomerprofileController@update_shipping_ajax');
Route::post('profile_image_submit','CustomerprofileController@profile_image_submit');
Route::get('contactus', 'FooterController@contactus');
Route::post('enquiry_submit', 'FooterController@enquiry_submit');
Route::get('insert_inquriy_ajax','FooterController@insert_inquriy_ajax');
Route::get('blog', 'FooterController@blog');
Route::get('blog_category/{id}', 'FooterController@blog_category');
Route::get('blog_view/{id}', 'FooterController@blog_view');
Route::get('blog_comment/{id}', 'FooterController@blog_comment');
Route::post('blog_comment_submit', 'FooterController@blog_comment_submit');
Route::get('pages/{id}', 'FooterController@get_front_cms_pages');
Route::get('faq', 'FooterController@get_faq');
Route::get('termsandconditons', 'FooterController@termsandconditons');
Route::post('user_subscription_submit','CustomerprofileController@user_subscription_submit');
Route::get('help', 'FooterController@help');
Route::get('nearbystore', 'HomeController@nearmemap');
Route::get('checkout', 'HomeController@checkout');
Route::post('checkout_auction', 'HomeController@checkout_auction');
Route::get('user_forgot_submit','UserloginController@password_emailcheck');
Route::post('payment_checkout_process', 'HomeController@payment_checkout_process');
Route::get('paypal_checkout_success', 'HomeController@paypal_checkout_success');
Route::get('paypal_checkout_cancel', 'HomeController@paypal_checkout_cancel');
//doku checkout_success : Toto 20170216
Route::get('doku_checkout_success/{transaction_id}', 'HomeController@doku_checkout_success');
Route::get('doku_checkout_cancel', 'HomeController@doku_checkout_cancel');
Route::post('notify/{code}', 'HomeController@dokuHostedNotify');
//sprintasia
Route::post('onepage', 'HomeController@sprintasiaCheckPaymentFlag');
Route::get('sprintasia_checkout_success/{id}', 'HomeController@sprintasia_checkout_success');
Route::get('sprintasia_checkout_cancel/{id}', 'HomeController@sprintasia_checkout_cancel');
Route::post('paymentflag/{channel}', 'HomeController@sprintasia_check_payment_flag');

Route::post('bid_payment', 'HomeController@bid_payment');
Route::get('bid_payment', 'HomeController@bid_payment_error');
Route::post('place_bid_payment', 'HomeController@place_bid_payment');
Route::get('place_bid_payment', 'HomeController@bid_payment_error');
//fb_login
Route::get('facebooklogin', 'HomeController@facebooklogin');
Route::get('merchant_signup', 'HomeController@merchant_signup');
Route::post('merchant_signup', 'HomeController@merchant_signup_submit');
Route::get('submission_merchant', 'HomeController@submission_merchant');


//fb_login


Route::get('register_getcity_shipping','CustomerprofileController@register_getcity_shipping');

//user_login_forgot_pwd
Route::get('user_forgot_pwd_email/{email_id}','UserloginController@user_forgot_pwd_email');
Route::get('user_reset_password_submit','UserloginController@user_reset_password_submit');

//payment result
Route::get('show_payment_result/{orderid}', 'HomeController@show_payment_result');
Route::get('show_payment_result_transfer_bank/{orderid}', 'HomeController@show_payment_result_transfer_bank');
Route::get('show_payment_result_columbia/{orderid}', 'HomeController@show_payment_result_columbia');

Route::get('show_payment_result_cod/{orderid}', 'HomeController@show_payment_result_cod');
//doku show payment result : Toto 20170216
Route::get('doku/show_payment_result/{orderid}', 'HomeController@doku_show_payment_result');

//add_to_cart

Route::get('cart', 'HomeController@cart');
Route::get('addtocart', 'HomeController@cart');
Route::get('addtocart_flash', 'HomeController@cart_flash');
Route::post('addtocart_flash', 'HomeController@addtocart_flash');
Route::post('addtocart', 'HomeController@add_to_cart');
Route::get('remove_session_cart_data', 'HomeController@remove_session_cart_data');
Route::get('set_quantity_session_cart', 'HomeController@set_quantity_session_cart');
Route::get('remove_session_dealcart_data', 'HomeController@remove_session_dealcart_data');
Route::get('set_quantity_session_dealcart', 'HomeController@set_quantity_session_dealcart');

Route::get('deal_cart', 'HomeController@deal_cart');
Route::get('addtocart_deal', 'HomeController@deal_cart');
Route::post('addtocart_deal', 'HomeController@add_to_cart_deal');
//add_to_cart

//admin_login
Route::get('chart', 'AdminController@chart');
Route::get('siteadmin', 'AdminController@siteadmin');
Route::get('siteadmin_login', 'AdminController@siteadmin_login');
Route::get('admin_settings', 'AdminController@admin_settings');
Route::post('admin_settings_submit', 'AdminController@admin_settings_submit');
Route::get('admin_profile', 'AdminController@admin_profile');
Route::post('login_check', 'AdminController@login_check');
Route::post('forgot_check', 'AdminController@forgot_check');
Route::get('admin_logout', 'AdminController@admin_logout');
Route::get('merchant_profile', 'MerchantSettingsController@merchant_profile');
//admin_login

//admin_dashboard
Route::get('siteadmin_dashboard', ['middleware' => 'auth', 'uses' => 'DashboardController@siteadmin_dashboard']);
Route::get('detail_transaksi_admin/{id}', ['middleware' => 'auth', 'uses' => 'DashboardController@detail_transaksi_admin']);

//admin_dashboard

// admin -> settings -> attribute managment
Route::get('add_size', ['middleware' => 'auth', 'uses' => 'AttributeController@add_size']);
Route::post('addsize_submit', ['middleware' => 'auth', 'uses' => 'AttributeController@addsizesubmit']);
Route::get('manage_size', ['middleware' => 'auth', 'uses' => 'AttributeController@manage_size']);
Route::get('delete_size/{id}', ['middleware' => 'auth', 'uses' => 'AttributeController@delete_size']);
Route::get('edit_size/{id}', ['middleware' => 'auth', 'uses' => 'AttributeController@edit_size']);
Route::post('editsize_submit', ['middleware' => 'auth', 'uses' => 'AttributeController@editsize_submit']);
Route::get('add_color', ['middleware' => 'auth', 'uses' => 'AttributeController@add_color']);
Route::post('add_color_submit', ['middleware' => 'auth', 'uses' => 'AttributeController@add_color_submit']);
Route::get('manage_color', ['middleware' => 'auth', 'uses' => 'AttributeController@manage_color']);
Route::get('edit_color/{id}', ['middleware' => 'auth', 'uses' => 'AttributeController@edit_color']);
Route::post('editcolor_submit', ['middleware' => 'auth', 'uses' => 'AttributeController@editcolor_submit']);
Route::get('delete_color/{id}', ['middleware' => 'auth', 'uses' => 'AttributeController@deletecolor_submit']);
Route::get('add_grade', ['middleware' => 'auth', 'uses' => 'AttributeController@add_grade']);
Route::post('add_grade_submit', ['middleware' => 'auth', 'uses' => 'AttributeController@add_grade_submit']);
Route::get('manage_grade', ['middleware' => 'auth', 'uses' => 'AttributeController@manage_grade']);
Route::get('edit_grade/{id}', ['middleware' => 'auth', 'uses' => 'AttributeController@edit_grade']);
Route::post('edit_grade_submit', ['middleware' => 'auth', 'uses' => 'AttributeController@edit_grade_submit']);
Route::get('delete_grade/{id}', ['middleware' => 'auth', 'uses' => 'AttributeController@delete_grade']);
//ajax color
Route::get('attribute_select_color', 'AttributeController@attribute_select_color');
//ajax color
// admin -> settings -> attribute managment

//admin -> popup image
Route::get('add_popup_image', ['middleware' => 'auth', 'uses' => 'BannerController@add_popup_image']);
Route::post('add_popup_submit', ['middleware' => 'auth', 'uses' => 'BannerController@add_popup_submit']);
Route::get('manage_popup_image', ['middleware' => 'auth', 'uses' => 'BannerController@manage_popup_image']);
Route::get('edit_popup_image/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@edit_popup_image']);
Route::post('edit_popup_submit', ['middleware' => 'auth', 'uses' => 'BannerController@edit_popup_submit']);
Route::get('status_popup_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'BannerController@status_popup_submit']);
Route::get('delete_popup_submit/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@delete_popup_submit']);
Route::get('status_popupdefault_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'BannerController@status_popupdefault_submit']);
// admin -> Banner image settings -> add banner image
Route::get('add_banner_image', ['middleware' => 'auth', 'uses' => 'BannerController@add_banner_image']);
Route::post('add_banner_submit', ['middleware' => 'auth', 'uses' => 'BannerController@add_banner_submit']);
Route::get('manage_banner_image', ['middleware' => 'auth', 'uses' => 'BannerController@manage_banner_image']);
Route::get('edit_banner_image/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@edit_banner_image']);
Route::post('edit_banner_submit', ['middleware' => 'auth', 'uses' => 'BannerController@edit_banner_submit']);
Route::get('delete_banner_submit/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@delete_banner_submit']);
Route::get('status_banner_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'BannerController@status_banner_submit']);
Route::post('sort_banner', 'BannerController@sort_banner');
// admin -> Banner image settings -> add banner image

//admin -> payment method image setting
Route::get('add_payment_method_image', ['middleware' => 'auth', 'uses' => 'BannerController@add_payment_method_image']);
Route::post('add_payment_method_image_submit', ['middleware' => 'auth', 'uses' => 'BannerController@add_payment_method_image_submit']);
Route::get('manage_payment_method_image', ['middleware' => 'auth', 'uses' => 'BannerController@manage_payment_method_image']);
Route::get('edit_payment_method_image/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@edit_payment_method_image']);
Route::post('edit_payment_method_image_submit', ['middleware' => 'auth', 'uses' => 'BannerController@edit_payment_method_image_submit']);
Route::get('delete_paymentmethod_image_submit/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@delete_paymentmethod_image_submit']);
Route::get('status_paymentmethod_image_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'BannerController@status_paymentmethod_image_submit']);
//admin -> payment method image setting

//admin -> Settings -> Specification management
Route::get('get_specification', 'SpecificationController@get_specification'); //bukan di admin
Route::get('add_specification', ['middleware' => 'auth', 'uses' => 'SpecificationController@add_specification']);
Route::post('add_specification_submit', ['middleware' => 'auth', 'uses' => 'SpecificationController@add_specification_submit']);
Route::get('manage_specification', ['middleware' => 'auth', 'uses' => 'SpecificationController@manage_specification']);
Route::get('edit_specification/{id}', ['middleware' => 'auth', 'uses' => 'SpecificationController@edit_specification']);
Route::get('delete_specification/{id}', ['middleware' => 'auth', 'uses' => 'SpecificationController@delete_specification']);
Route::post('update_specification_submit', ['middleware' => 'auth', 'uses' => 'SpecificationController@update_specification']);
Route::get('add_specification_group', ['middleware' => 'auth', 'uses' => 'SpecificationController@add_specification_group']);
Route::post('add_specification_group_submit', ['middleware' => 'auth', 'uses' => 'SpecificationController@add_specification_group_submit']);
Route::get('manage_specification_group', ['middleware' => 'auth', 'uses' => 'SpecificationController@manage_specification_group']);
Route::get('edit_specification_group/{id}', ['middleware' => 'auth', 'uses' => 'SpecificationController@edit_specification_group']);
Route::get('delete_specification_group/{id}', ['middleware' => 'auth', 'uses' => 'SpecificationController@delete_specification_group']);
Route::post('edit_specification_group_submit', ['middleware' => 'auth', 'uses' => 'SpecificationController@edit_specification_group_submit']);
//admin -> Settings -> Specification management

//admin country management->add country

Route::get('add_country', ['middleware' => 'auth', 'uses' => 'CountryController@add_country']);
Route::post('add_country_submit', ['middleware' => 'auth', 'uses' => 'CountryController@add_country_submit']);
Route::get('manage_country', ['middleware' => 'auth', 'uses' => 'CountryController@manage_country']);
Route::post('update_country_submit', ['middleware' => 'auth', 'uses' => 'CountryController@update_country_submit']);
Route::get('edit_country/{id}', ['middleware' => 'auth', 'uses' => 'CountryController@edit_country']);
Route::get('delete_country/{id}', ['middleware' => 'auth', 'uses' => 'CountryController@delete_country']);
Route::get('status_country_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'CountryController@update_status_country']);


// admin->settings->CMS management

Route::get('add_cms_page', ['middleware' => 'auth', 'uses' => 'CmsController@add_cms_page']);
Route::post('cms_add_page_submit', ['middleware' => 'auth', 'uses' => 'CmsController@cms_add_page_submit']);
Route::get('manage_cms_page', ['middleware' => 'auth', 'uses' => 'CmsController@manage_cms_page']);
Route::get('edit_cms_page/{id}', ['middleware' => 'auth', 'uses' => 'CmsController@edit_cms_page']);
Route::post('edit_cms_page_submit', ['middleware' => 'auth', 'uses' => 'CmsController@edit_cms_page_submit']);
Route::get('block_cms_page/{id}/{status}', ['middleware' => 'auth', 'uses' => 'CmsController@block_cms_page']);
Route::get('delete_cms_page/{id}', ['middleware' => 'auth', 'uses' => 'CmsController@delete_cms_page']);
Route::get('aboutus_page', ['middleware' => 'auth', 'uses' => 'CmsController@aboutus_page']);
Route::post('aboutus_page_update', ['middleware' => 'auth', 'uses' => 'CmsController@aboutus_page_update']);
Route::get('terms', ['middleware' => 'auth', 'uses' => 'CmsController@terms']);
Route::post('terms_update', ['middleware' => 'auth', 'uses' => 'CmsController@terms_update']);
Route::get('mer_terms', ['middleware' => 'auth', 'uses' => 'CmsController@mer_terms']);
Route::post('mer_terms_update', ['middleware' => 'auth', 'uses' => 'CmsController@mer_terms_update']);
//register success
Route::get('mer_thanks_reg', ['middleware' => 'auth', 'uses' => 'CmsController@mer_thanks_reg']);
Route::post('mer_thanks_reg_update', ['middleware' => 'auth', 'uses' => 'CmsController@mer_thanks_reg_update']);
// admin->settings->CMS management
//blade mer reg Sukses
Route::get('mer_register_success','MerchantController@mer_register_success');
//merchant terms blade
Route::get('terms_merchant','HomeController@terms_merchant');

//admin-> Deals
Route::get('deals_dashboard', ['middleware' => 'auth', 'uses' => 'DealsController@deals_dashboard']);
Route::get('add_deals', ['middleware' => 'auth', 'uses' => 'DealsController@add_deals']);
Route::post('add_deals_submit', ['middleware' => 'auth', 'uses' => 'DealsController@add_deals_submit']);
Route::get('/add_plan_timer', ['middleware' => 'auth', 'uses' => 'PlanTimerController@add_plan_timer']);
Route::post('add_plan_timer_submit', ['middleware' => 'auth', 'uses' => 'PlanTimerController@add_plan_timer_submit']);
Route::post('edit_plan_timer_submit', ['middleware' => 'auth', 'uses' => 'PlanTimerController@edit_plan_timer_submit']);
Route::get('edit_plan_timer/{id}', ['middleware' => 'auth', 'uses' => 'PlanTimerController@edit_plan_timer']);
Route::get('/manage_schedule', ['middleware' => 'auth', 'uses' => 'PlanTimerController@manage_schedule']);
Route::post('/manage_schedule', ['middleware' => 'auth', 'uses' => 'PlanTimerController@manage_schedule']);

Route::get('manage_coupons', ['middleware' => 'auth', 'uses' => 'PlanTimerController@manage_coupons']);


//for ajax
Route::get('deals_select_main_cat', 'DealsController@deals_select_main_cat');
Route::get('deals_select_sub_cat', 'DealsController@deals_select_sub_cat');
Route::get('deals_select_second_sub_cat', 'DealsController@deals_select_second_sub_cat');
Route::get('deals_edit_select_main_cat', 'DealsController@deals_edit_select_main_cat');
Route::get('deals_edit_select_sub_cat', 'DealsController@deals_edit_select_sub_cat' );
Route::get('deals_edit_second_sub_cat', 'DealsController@deals_edit_second_sub_cat');
Route::get('check_title_exist','DealsController@check_title_exist');
Route::get('check_title_exist_edit', 'DealsController@check_title_exist_edit');
//for ajax
Route::post('edit_deals_submit', ['middleware' => 'auth', 'uses' => 'DealsController@edit_deals_submit']);
Route::get('edit_deals/{id}', ['middleware' => 'auth', 'uses' => 'DealsController@edit_deals']);
Route::get('delete_deals/{id}', ['middleware' => 'auth', 'uses' => 'DealsController@delete_deals']);
Route::get('manage_deals', ['middleware' => 'auth', 'uses' => 'DealsController@manage_deals']);
Route::post('manage_deals', ['middleware' => 'auth', 'uses' => 'DealsController@manage_deals']);
Route::get('block_deals/{id}/{status}', ['middleware' => 'auth', 'uses' => 'DealsController@block_deals']);
Route::get('deal_details/{id}', ['middleware' => 'auth', 'uses' => 'DealsController@deal_details']);
Route::get('expired_deals', ['middleware' => 'auth', 'uses' => 'DealsController@expired_deals']);
Route::post('expired_deals', ['middleware' => 'auth', 'uses' => 'DealsController@expired_deals']);

Route::get('delete_schedule/{id}', ['middleware' => 'auth', 'uses' => 'PlanTimerController@delete_schedule']);
Route::get('schedule_details/{id}', ['middleware' => 'auth', 'uses' => 'PlanTimerController@schedule_details']);
Route::get('get_schedule', 'PlanTimerController@get_schedule');

Route::get('validate_coupon_code', ['middleware' => 'auth', 'uses' => 'DealsController@validate_coupon_code']);
Route::get('redeem_coupon_list', ['middleware' => 'auth', 'uses' => 'DealsController@redeem_coupon_list']);



//admin-> Deals
//admin -> cities management

Route::get('add_city', ['middleware' => 'auth', 'uses' => 'CityController@add_city']);
Route::post('add_city_submit', ['middleware' => 'auth', 'uses' => 'CityController@add_city_submit']);
Route::get('manage_city', ['middleware' => 'auth', 'uses' => 'CityController@manage_city']);
Route::post('edit_city_submit', ['middleware' => 'auth', 'uses' => 'CityController@edit_city_submit']);
Route::get('edit_city/{id}', ['middleware' => 'auth', 'uses' => 'CityController@edit_city']);
Route::get('delete_city/{id}', ['middleware' => 'auth', 'uses' => 'CityController@delete_city']);
Route::get('status_city_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'CityController@status_city_submit']);
Route::get('update_default_city_submit', ['middleware' => 'auth', 'uses' => 'CityController@update_default_city_submit']);

//admin-> categories management
Route::get('add_category', ['middleware' => 'auth', 'uses' => 'CategoryController@add_category']);
Route::post('add_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@add_category_submit']);
Route::get('manage_category', ['middleware' => 'auth', 'uses' => 'CategoryController@manage_category']);
Route::get('edit_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_category']);
Route::post('edit_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_category_submit']);
Route::get('status_category_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'CategoryController@status_category_submit']);
Route::get('delete_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@delete_category']);
Route::get('add_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@add_main_category']);
Route::post('add_main_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@add_main_category_submit']);
Route::get('manage_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@manage_main_category']);

Route::get('edit_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_main_category']);
Route::post('edit_main_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_main_category_submit']);
Route::get('status_main_category_submit/{id}/{mc_id}/{status}', ['middleware' => 'auth', 'uses' => 'CategoryController@status_main_category_submit']);
Route::get('delete_main_category/{id}/{mc_id}', ['middleware' => 'auth', 'uses' => 'CategoryController@delete_main_category']);

Route::get('add_sub_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@add_sub_main_category']);
Route::post('add_sub_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@add_sub_category_submit']);
Route::get('manage_sub_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@manage_sub_category']);
Route::get('add_secsub_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@add_secsub_main_category']);
Route::post('add_secsub_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@add_secsub_main_category_submit']);
Route::get('edit_secsub_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_secsub_main_category']);
Route::post('edit_secsub_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_secsub_category_submit']);
Route::get('status_sub_category_submit/{id}/{mc_id}/{status}', ['middleware' => 'auth', 'uses' => 'CategoryController@status_subsec_category_submit']);
Route::get('delete_sub_category/{id}/{mc_id}', ['middleware' => 'auth', 'uses' => 'CategoryController@delete_subsec_category']);
Route::get('manage_secsubmain_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@manage_secsubmain_category']);
Route::get('status_secsub_category_submit/{id}/{mc_id}/{status}', ['middleware' => 'auth', 'uses' => 'CategoryController@status_secsub_category_submit']);
Route::get('delete_secsub_category/{id}/{mc_id}', ['middleware' => 'auth', 'uses' => 'CategoryController@delete_secsub_category']);
Route::get('edit_sec1sub_main_category/{id}', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_sec1sub_main_category']);
Route::post('edit_sec1sub_category_submit', ['middleware' => 'auth', 'uses' => 'CategoryController@edit_sec1sub_category_submit']);

//admin-> settings->FAQ
Route::get('add_faq', ['middleware' => 'auth', 'uses' => 'FaqController@add_faq']);
Route::get('manage_faq', ['middleware' => 'auth', 'uses' => 'FaqController@manage_faq']);
Route::post('add_faq_submit', ['middleware' => 'auth', 'uses' => 'FaqController@add_faq_submit']);
Route::post('update_faq_submit', ['middleware' => 'auth', 'uses' => 'FaqController@update_faq_submit']);
Route::get('edit_faq/{id}', ['middleware' => 'auth', 'uses' => 'FaqController@edit_faq']);
Route::get('delete_faq/{id}', ['middleware' => 'auth', 'uses' => 'FaqController@delete_faq']);
Route::get('status_faq_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'FaqController@update_status_faq']);

//admin->settings->Ads Management
Route::get('add_ad', ['middleware' => 'auth', 'uses' => 'AdController@add_ad']);
Route::post('add_ad_submit', ['middleware' => 'auth', 'uses' => 'AdController@add_ad_submit']);
Route::get('manage_ad', ['middleware' => 'auth', 'uses' => 'AdController@manage_ad']);
Route::get('edit_ad/{id}', ['middleware' => 'auth', 'uses' => 'AdController@edit_ad']);
Route::post('edit_ad_submit', ['middleware' => 'auth', 'uses' => 'AdController@edit_ad_submit']);
Route::get('delete_ad/{id}', ['middleware' => 'auth', 'uses' => 'AdController@delete_ad']);
Route::get('status_ad_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'AdController@status_ad_submit']);

//admin -> Settings -> Newsletter
Route::get('send_newsletter', ['middleware' => 'auth', 'uses' => 'SettingsController@send_newsletter']);
Route::post('save_newsletter_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@save_newsletter_submit']);
Route::get('send_newsletter_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'SettingsController@send_newsletter_submit']);
Route::get('manage_newsletter', ['middleware' => 'auth', 'uses' => 'SettingsController@manage_newsletter']);
Route::get('edit_newsletter/{id}', ['middleware' => 'auth', 'uses' => 'SettingsController@edit_newsletter']);
Route::post('edit_newsletter_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@edit_newsletter_submit']);
// Route::get('status_newsletter_submit/{id}/{status}', 'SettingsController@status_popup_submit');
Route::get('delete_newsletter_submit/{id}', ['middleware' => 'auth', 'uses' => 'SettingsController@delete_newsletter_submit']);
Route::get('status_enable_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'SettingsController@status_enable_submit']);
Route::get('manage_newsletter_subscribers', ['middleware' => 'auth', 'uses' => 'SettingsController@manage_newsletter_subscribers']);
Route::get('edit_newsletter_subscriber_status/{id}/{status}', ['middleware' => 'auth', 'uses' => 'SettingsController@edit_newsletter_subscriber_status']);
Route::get('delete_newsletter_subscriber/{id}', ['middleware' => 'auth', 'uses' => 'SettingsController@delete_newsletter_subscriber']);
Route::get('subscribe_newsletter/{id}', 'SettingsController@subscribe_newsletter');
Route::get('unsubscribe/{email}','SettingsController@unsubscribe');
Route::post('submit_unsubscribe','SettingsController@submit_unsubscribe');
//Route::get('unsubscribe_success','SettingsController@unsubscribe_success');
Route::get('tampil_news','SettingsController@tampil_news');
Route::get('newsletter/', 'HomeController@newsletter');
//admin ->product->Add Product
Route::get('add_product', ['middleware' => 'auth', 'uses' => 'ProductController@add_product']);
Route::post('add_product_submit', ['middleware' => 'auth', 'uses' => 'ProductController@add_product_submit']);
Route::get('product_getmaincategory', 'ProductController@product_getmaincategory');
Route::get('product_getsubcategory', 'ProductController@product_getsubcategory');
Route::get('product_getsecondsubcategory', 'ProductController@product_getsecondsubcategory');
Route::get('product_getcolor', 'ProductController@product_getcolor');
Route::get('product_getsize', 'ProductController@product_getsize');
Route::get('product_getspecification', 'ProductController@product_getspecification');
Route::get('add_estimated_zipcode', 'ProductController@add_estimated_zipcode');
Route::post('add_estimated_zipcode_submit', 'ProductController@add_estimated_zipcode_submit');
Route::get('estimated_zipcode','ProductController@estimated_zipcode');
Route::get('edit_zipcode/{id}', 'ProductController@edit_zipcode');
Route::get('block_zipcode/{id}/{status}', 'ProductController@block_zipcode');
Route::post('edit_estimated_zipcode_submit', 'ProductController@edit_estimated_zipcode_submit');

Route::get('product_edit_getmaincategory', 'ProductController@product_edit_getmaincategory');
Route::get('product_edit_getsubcategory', 'ProductController@product_edit_getsubcategory' );
Route::get('product_edit_getsecondsubcategory', 'ProductController@product_edit_getsecondsubcategory');

Route::post('edit_product_submit', ['middleware' => 'auth', 'uses' => 'ProductController@edit_product_submit']);
Route::get('edit_product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@edit_product']);
Route::get('delete_product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@delete_product']);
Route::get('get_product', 'ProductController@get_product');
Route::get('manage_product', ['middleware' => 'auth', 'uses' => 'ProductController@manage_product']);
Route::post('manage_product', ['middleware' => 'auth', 'uses' => 'ProductController@manage_product']);
Route::get('sepulsa_product', ['uses' => 'ProductController@sepulsa_product']);
Route::post('add_sepulsa_product', ['uses' => 'ProductController@add_sepulsa_product']);
Route::post('update_sepulsa_product_process', ['uses' => 'ProductController@update_sepulsa_product_process']);
Route::get('update_sepulsa_product/{id}', ['uses' => 'ProductController@update_sepulsa_product']);
Route::get('approval_product', ['middleware' => 'auth', 'uses' => 'ProductController@approval_product']);
Route::post('approval_product', ['middleware' => 'auth', 'uses' => 'ProductController@approval_product']);
Route::get('approve_product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@approve_product']);
Route::get('reject_product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@reject_product']);
Route::post('reject_product_process', ['middleware' => 'auth', 'uses' => 'ProductController@reject_product_process']);
Route::get('block_product/{id}/{status}', ['middleware' => 'auth', 'uses' => 'ProductController@block_product']);
Route::get('product_details/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@product_details']);
Route::get('manage_product_shipping_details', ['middleware' => 'auth', 'uses' => 'ProductController@manage_product_shipping_details']);
Route::get('manage_cashondelivery_details', ['middleware' => 'auth', 'uses' => 'ProductController@manage_cashondelivery_details']);
Route::get('sold_product', ['middleware' => 'auth', 'uses' => 'ProductController@sold_product']);
Route::post('sold_product', ['middleware' => 'auth', 'uses' => 'ProductController@sold_product']);
Route::get('product_getmerchantshop', 'ProductController@product_getmerchantshop');
Route::get('product_getmerchantshop_ajax', 'ProductController@product_getmerchantshop');
Route::get('update_warranty', 'ProductController@update_warranty');
Route::get('upload_excel_product', ['middleware' => 'auth', 'uses' => 'ProductController@upload_excel_product']);
Route::get('download_template_product', 'ProductController@download_template_product');
Route::post('upload_excel_product_process', 'ProductController@upload_excel_product_process');
Route::get('get_secmaincategory_ajax', 'ProductController@get_secmaincategory_ajax');
Route::get('get_subcategory_ajax', 'ProductController@get_subcategory_ajax');
Route::post('download_product', 'ProductController@download_product');
Route::post('report_pricing', 'ProductController@report_pricing');
Route::get('report_promo', ['middleware' => 'auth', 'uses' => 'PlanTimerController@report_promo']);
Route::post('report_promo_download', ['middleware' => 'auth', 'uses' => 'PlanTimerController@report_promo_download']);

Route::get('product_dashboard', ['middleware' => 'auth', 'uses' => 'ProductController@product_dashboard']);


//admin->customer->add customer
Route::get('add_customer_group', ['middleware' => 'auth', 'uses' => 'CustomerController@add_customer_group']);
Route::post('add_customer_group_submit', ['middleware' => 'auth', 'uses' => 'CustomerController@add_customer_group_submit']);
Route::get('edit_customer_group/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@edit_customer_group']);
Route::post('edit_customer_group_submit', ['middleware' => 'auth', 'uses' => 'CustomerController@edit_customer_group_submit']);
Route::get('delete_customer_group/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@delete_customer_group']);
Route::get('manage_customer_group', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_customer_group']);
Route::post('add_customer_group_relation', ['middleware' => 'auth', 'uses' => 'CustomerController@add_customer_group_relation']);
Route::post('add_customer_group_relation_submit', ['middleware' => 'auth', 'uses' => 'CustomerController@add_customer_group_relation_submit']);
Route::get('delete_customer_group_relation/{cus_id}/{cus_group_id}', ['middleware' => 'auth', 'uses' => 'CustomerController@delete_customer_group_relation']);
Route::get('add_customer', ['middleware' => 'auth', 'uses' => 'CustomerController@add_customer']);
Route::get('load_getcity','CustomerController@getcity');
Route::post('add_customer_submit', ['middleware' => 'auth', 'uses' => 'CustomerController@add_customer_submit']);
Route::post('edit_customer_submit', ['middleware' => 'auth', 'uses' => 'CustomerController@edit_customer_submit']);
Route::get('edit_customer/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@edit_customer']);
Route::get('delete_customer/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@delete_customer']);
Route::get('status_customer_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'CustomerController@status_customer_submit']);
Route::get('customer_edit_getcity','CustomerController@customer_edit_getcity');
Route::get('manage_customer', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_customer']);
Route::post('manage_customer', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_customer']);
Route::get('manage_subscribers', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_subscribers']);
Route::get('edit_subscriber_status/{id}/{status}', ['middleware' => 'auth', 'uses' => 'CustomerController@edit_subscriber_status']);
Route::get('delete_subscriber/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@delete_subscriber']);
Route::get('manage_inquires', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_inquires']);
Route::post('manage_inquires', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_inquires']);
Route::get('send_inquiry_email/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@send_inquiry_email']);
Route::get('delete_inquires/{id}', ['middleware' => 'auth', 'uses' => 'CustomerController@delete_inquires']);
Route::get('manage_referral_users', ['middleware' => 'auth', 'uses' => 'CustomerController@manage_referral_users']);
Route::get('customer_dashboard', ['middleware' => 'auth', 'uses' => 'CustomerController@customer_dashboard']);
Route::get('download_customer', 'CustomerController@download_customer');

//admin ->general settings

Route::get('general_setting', ['middleware' => 'auth', 'uses' => 'SettingsController@general_setting']);
Route::post('general_setting_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@general_setting_submit']);
Route::get('email_setting', ['middleware' => 'auth', 'uses' => 'SettingsController@email_setting']);
Route::post('email_setting_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@email_setting_submit']);
Route::get('smtp_setting', ['middleware' => 'auth', 'uses' => 'SettingsController@smtp_setting']);
Route::post('smtp_setting_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@smtp_setting_submit']);
Route::post('send_setting_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@send_setting_submit']);
Route::get('extended_warranty', ['middleware' => 'auth', 'uses' => 'SettingsController@extended_warranty']);
Route::post('extended_warranty_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@extended_warranty_submit']);

//Admin -> Settings-> Social media settings

Route::get('social_media_settings', ['middleware' => 'auth', 'uses' => 'SettingsController@social_media_settings']);
Route::post('social_media_setting_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@social_media_setting_submit']);
Route::get('payment_settings', ['middleware' => 'auth', 'uses' => 'SettingsController@payment_settings']);
Route::get('select_currency_value_ajax', 'SettingsController@select_currency_value_ajax');
Route::post('payment_settings_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@payment_settings_submit']);
Route::get('module_settings', 'SettingsController@module_settings');
Route::post('module_settings_submit', 'SettingsController@module_settings_submit');

Route::get('bank_akun_kuku', ['middleware' => 'auth', 'uses' => 'SettingsController@bank_akun_kuku']);
Route::get('add_bank_akun', ['middleware' => 'auth', 'uses' => 'SettingsController@add_bank_akun']);
Route::post('add_bank_akun_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@add_bank_akun_submit']);
Route::get('edit_bank_akun/{id}', ['middleware' => 'auth', 'uses' => 'SettingsController@edit_bank_akun']);
Route::post('edit_bank_akun_submit', ['middleware' => 'auth', 'uses' => 'SettingsController@edit_bank_akun_submit']);
Route::get('delete_bank_akun/{id}', ['middleware' => 'auth', 'uses' => 'SettingsController@delete_bank_akun']);

//admin->merchants->add merchant

Route::get('add_merchant', ['middleware' => 'auth', 'uses' => 'MerchantController@add_merchant']);
Route::get('ajax_select_city','MerchantController@ajax_select_city');
Route::get('ajax_select_city_edit', 'MerchantController@ajax_select_city_edit');
Route::post('add_merchant_submit', ['middleware' => 'auth', 'uses' => 'MerchantController@add_merchant_submit']);
Route::post('add_merchant_submit_signup', 'MerchantController@add_merchant_submit_signup');

Route::get('edit_merchant/{id}', ['middleware' => 'auth', 'uses' => 'MerchantController@edit_merchant']);
Route::post('edit_merchant_submit', ['middleware' => 'auth', 'uses' => 'MerchantController@edit_merchant_submit']);
Route::get('manage_merchant', ['middleware' => 'auth', 'uses' => 'MerchantController@manage_merchant']);
Route::post('manage_merchant', ['middleware' => 'auth', 'uses' => 'MerchantController@manage_merchant']);
Route::get('manage_enquiry', 'MerchantController@manage_enquiry');
Route::get('block_merchant/{id}/{status}', ['middleware' => 'auth', 'uses' => 'MerchantController@block_merchant']);
Route::get('manage_store/{id}', 'MerchantController@manage_store');
Route::get('add_store/{id}', 'MerchantController@add_store');
Route::post('add_store_submit','MerchantController@add_store_submit');
Route::get('edit_store/{id}/{mer_id}','MerchantController@edit_store');
Route::post('edit_store_submit','MerchantController@edit_store_submit');
Route::get('block_store/{id}/{status}/{mer_id}', 'MerchantController@block_store');

Route::get('merchant_dashboards', ['middleware' => 'auth', 'uses' => 'MerchantController@merchant_dashboard']);
//admin-> Auction
Route::get('auction_dashboard', 'AuctionController@auction_dashboard');
Route::get('add_auction', 'AuctionController@add_auction');
Route::get('auction_winners', 'AuctionController@auction_winners');
Route::get('auction_cod', 'AuctionController@auction_cod');
Route::get('cancel_auction_cod/{id}', 'AuctionController@cancel_auction_cod');
Route::post('add_auction_submit', 'AuctionController@add_auction_submit');
//for ajax
Route::get('auction_select_main_cat', 'AuctionController@auction_select_main_cat');
Route::get('auction_select_sub_cat', 'AuctionController@auction_select_sub_cat');
Route::get('auction_select_second_sub_cat', 'AuctionController@auction_select_second_sub_cat');
Route::get('auction_edit_select_main_cat', 'AuctionController@auction_edit_select_main_cat');
Route::get('auction_edit_select_sub_cat', 'AuctionController@auction_edit_select_sub_cat' );
Route::get('auction_edit_second_sub_cat', 'AuctionController@auction_edit_second_sub_cat');
Route::get('check_auctiontitle_exist','AuctionController@check_auctiontitle_exist');
Route::get('check_auctiontitle_exist_edit', 'AuctionController@check_auctiontitle_exist_edit');
Route::get('auction_select_store_name', 'AuctionController@auction_select_store_name');
Route::get('auction_select_store_name_edit', 'AuctionController@auction_select_store_name_edit');
//for ajax
Route::post('edit_auction_submit', 'AuctionController@edit_auction_submit');
Route::get('edit_auction/{id}', 'AuctionController@edit_auction');
Route::get('manage_auction','AuctionController@manage_auction');
Route::get('block_auction/{id}/{status}','AuctionController@block_auction');
Route::get('auction_details/{id}', 'AuctionController@auction_details');
Route::get('expired_auction', 'AuctionController@expired_auction');

//merchant-> Auction
Route::get('merchant_add_auction', 'MerchantauctionController@add_auction');
Route::post('merchant_add_auction_submit', 'MerchantauctionController@add_auction_submit');
Route::get('merchant_auction_winners', 'MerchantauctionController@auction_winners');
Route::get('merchant_auction_cod', 'MerchantauctionController@auction_cod');



//for ajax
Route::get('check_auctiontitle_exist','MerchantauctionController@check_auctiontitle_exist');
Route::get('check_auctiontitle_exist_edit', 'MerchantauctionController@check_auctiontitle_exist_edit');
Route::get('auction_select_store_name', 'MerchantauctionController@auction_select_store_name');
Route::get('auction_select_store_name_edit', 'MerchantauctionController@auction_select_store_name_edit');
//for ajax
Route::post('merchant_edit_auction_submit', 'MerchantauctionController@edit_auction_submit');
Route::get('merchant_edit_auction/{id}', 'MerchantauctionController@edit_auction');
Route::get('merchant_manage_auction','MerchantauctionController@manage_auction');
Route::get('merchant_block_auction/{id}/{status}','MerchantauctionController@block_auction');
Route::get('merchant_auction_details/{id}', 'MerchantauctionController@auction_details');
Route::get('merchant_expired_auction', 'MerchantauctionController@expired_auction');
Route::get('merchant_block_auction/{id}/{status}/{merid}','MerchantauctionController@block_auction');





//admin-> Deals



//adming-Blogs
Route::get('add_blog', 'BlogController@add_blog');
Route::post('add_blog_submit', 'BlogController@add_blog_submit');
Route::get('manage_draft_blog', 'BlogController@manage_draft_blog');
Route::get('manage_publish_blog', 'BlogController@manage_publish_blog');
Route::get('block_blog/{id}/{status}/{blog_type}', 'BlogController@block_blog');
Route::get('delete_blog_submit/{id}/{blog_type}', 'BlogController@delete_blog_submit');
Route::get('edit_blog/{id}', 'BlogController@edit_blog');
Route::post('edit_blog_submit', 'BlogController@edit_blog_submit');
Route::get('blog_details/{id}', 'BlogController@blog_details');
Route::get('blog_settings', 'BlogController@blog_settings');
Route::post('edit_blog_settings', 'BlogController@edit_blog_settings');
Route::get('manage_blogcmts', 'BlogController@manage_blogcomments');
Route::get('status_blogcmt_submit/{cmtid}/{status}','BlogController@status_blogcmt_submit');
Route::get('reply_blogcmts/{cmtid}', 'BlogController@reply_blogcmts');
Route::post('admin_blogreply_submit', 'BlogController@admin_blogreply_submit');

//merchant login
Route::get('sitemerchant', 'MerchantLoginController@merchant_login');
Route::post('mer_login_check', 'MerchantLoginController@merchant_login_check');
Route::post('merchant_forgot_check', 'MerchantLoginController@merchant_forgot_check');
Route::get('forgot_pwd_email/{email}', 'MerchantLoginController@forgot_pwd_email');
Route::post('forgot_pwd_email_submit', 'MerchantLoginController@forgot_pwd_email_submit');
Route::get('merchant_logout', 'MerchantLoginController@merchant_logout');


//admin ->Image settings
Route::get('add_logo', ['middleware' => 'auth', 'uses' => 'ImagesettingsController@add_logo']);
Route::post('add_logo_submit', ['middleware' => 'auth', 'uses' => 'ImagesettingsController@add_logo_submit']);
Route::get('add_favicon', ['middleware' => 'auth', 'uses' => 'ImagesettingsController@add_favicon']);
Route::post('add_favicon_submit', ['middleware' => 'auth', 'uses' => 'ImagesettingsController@add_favicon_submit']);
Route::get('add_noimage', ['middleware' => 'auth', 'uses' => 'ImagesettingsController@add_noimage']);
Route::post('add_noimage_submit', ['middleware' => 'auth', 'uses' => 'ImagesettingsController@add_noimage_submit']);


//merchant_dashboard
Route::get('sitemerchant_dashboard', 'DashboardController@sitemerchant_dashboard');


//merchant settings

//merchant settings

Route::get('merchant_settings', 'MerchantSettingsController@show_settings');
Route::get('edit_merchant_info/{id}', 'MerchantSettingsController@edit_info');
Route::get('company_profile/{id}', 'MerchantSettingsController@profile');
Route::get('change_merchant_password/{id}', 'MerchantSettingsController@change_pwd');
Route::post('change_password_submit', 'MerchantSettingsController@change_password_submit');
Route::post('edit_merchant_account_submit', 'MerchantSettingsController@edit_merchant_account_submit');


//merchant shop
Route::get('merchant_manage_shop/{id}', 'MerchantshopController@manage_shop');
Route::post('merchant_manage_shop/{id}', 'MerchantshopController@manage_shop');
Route::get('merchant_add_shop/{id}', 'MerchantshopController@add_shop');
Route::get('merchant_edit_shop/{id}/{merid}', 'MerchantshopController@edit_shop');
Route::get('merchant_block_shop/{id}/{status}/{merid}', 'MerchantshopController@block_shop');

Route::post('merchant_add_shop_submit', 'MerchantshopController@add_shop_submit');
Route::post('merchant_edit_shop_submit', 'MerchantshopController@edit_shop_submit');



//merchant-> Deals
Route::get('mer_add_deals', 'MerchantdealsController@add_deals');
Route::post('mer_add_deals_submit', 'MerchantdealsController@add_deals_submit');
//merchant-> add schedule
Route::get('mer_add_plan_timer','MerchantdealsController@mer_add_plan_timer');
Route::post('mer_add_plan_timer_submit','MerchantdealsController@mer_add_plan_timer_submit');
Route::get('mer_manage_schedule','MerchantdealsController@mer_manage_schedule');
Route::get('mer_schedule_details/{id}','MerchantdealsController@mer_schedule_details');
Route::get('mer_edit_plan_timer/{id}','MerchantdealsController@mer_edit_plan_timer');
Route::post('mer_edit_plan_timer_submit','MerchantdealsController@mer_edit_plan_timer_submit');
Route::get('mer_block_deals/{id}/{status}','MerchantdealsController@mer_block_deals');
Route::get('mer_delete_schedule/{id}', 'MerchantdealsController@mer_delete_schedule');
Route::get('mer_expired_schedule_details/{id}','MerchantdealsController@mer_expired_schedule_details');
//for ajax
//for ajax
Route::post('mer_edit_deals_submit', 'MerchantdealsController@edit_deals_submit');
Route::get('mer_edit_deals/{id}', 'MerchantdealsController@edit_deals');
Route::get('mer_manage_deals','MerchantdealsController@manage_deals');
Route::post('mer_manage_deals','MerchantdealsController@manage_deals');
Route::get('mer_block_deals/{id}/{status}','MerchantdealsController@mer_block_deals');
Route::get('mer_deal_details/{id}', 'MerchantdealsController@deal_details');
Route::get('mer_expired_deals', 'MerchantdealsController@mer_expired_deals');
Route::post('mer_expired_deals', 'MerchantdealsController@expired_deals');


Route::get('mer_validate_coupon_code', 'MerchantdealsController@validate_coupon_code');
Route::get('mer_redeem_coupon_list', 'MerchantdealsController@redeem_coupon_list');

//merchant-> Deals

//merchant ->product->Add Product
Route::get('mer_add_product', 'MerchantproductController@add_product');
Route::post('mer_add_product_submit', 'MerchantproductController@mer_add_product_submit');
Route::get('mer_product_getcolor', 'MerchantproductController@product_getcolor');
Route::post('mer_edit_product_submit', 'MerchantproductController@mer_edit_product_submit');
Route::get('mer_delete_product/{id}', 'MerchantproductController@mer_delete_product');
Route::post('mer_edit_rejected_product_submit', 'MerchantproductController@mer_edit_rejected_product_submit');
Route::get('mer_edit_product/{id}', 'MerchantproductController@mer_edit_product');
Route::get('mer_edit_rejected_product/{id}', 'MerchantproductController@mer_edit_rejected_product');
Route::get('mer_manage_product','MerchantproductController@manage_product');
Route::get('mer_rejected_product', 'MerchantproductController@rejected_product');
Route::get('mer_block_product/{id}/{status}','MerchantproductController@block_product');
Route::get('mer_product_details/{id}', 'MerchantproductController@product_details');
Route::get('mer_manage_product_shipping_details','MerchantproductController@manage_product_shipping_details');
Route::get('mer_manage_cashondelivery_details','MerchantproductController@manage_cashondelivery_details');
Route::get('mer_sold_product', 'MerchantproductController@sold_product');
Route::post('mer_sold_product', 'MerchantproductController@sold_product');

//merchant ->product->Add Product
// merchant -> settings -> attribute managment
Route::get('mer_add_size', 'MerchantattributeController@mer_add_size');
Route::post('mer_addsize_submit', 'MerchantattributeController@mer_addsizesubmit');
Route::get('mer_manage_size', 'MerchantattributeController@mer_manage_size');
Route::get('mer_delete_size/{id}', 'MerchantattributeController@mer_delete_size');
Route::get('mer_edit_size/{id}', 'MerchantattributeController@mer_edit_size');
Route::post('mer_editsize_submit', 'MerchantattributeController@mer_editsize_submit');
Route::get('mer_add_color', 'MerchantattributeController@mer_add_color');
Route::post('mer_add_color_submit', 'MerchantattributeController@mer_add_color_submit');
Route::get('mer_manage_color', 'MerchantattributeController@mer_manage_color');
Route::get('mer_edit_color/{id}', 'MerchantattributeController@mer_edit_color');
Route::post('mer_editcolor_submit', 'MerchantattributeController@mer_editcolor_submit');
Route::get('mer_delete_color/{id}', 'MerchantattributeController@mer_deletecolor_submit');
//ajax color
Route::get('attribute_select_color', 'MerchantattributeController@mer_attribute_select_color');
//ajax color

/*merchant transaction */
Route::get('show_merchant_transactions', 'MerchantTransactionController@show_merchant_transactions');

/* merchant deals transaction */
Route::get('merchant_deals_all_orders/{merid}', 'MerchantTransactionController@merchant_deals_all_orders');
Route::get('merchant_deals_all_orders', 'MerchantTransactionController@deals_all_orders');
Route::post('merchant_deals_all_orders', 'MerchantTransactionController@deals_all_orders');
Route::get('merchant_deals_success_orders', 'MerchantTransactionController@deals_success_orders');
Route::get('merchant_deals_completed_orders', 'MerchantTransactionController@deals_completed_orders');
Route::get('merchant_deals_hold_orders', 'MerchantTransactionController@deals_hold_orders');
Route::get('merchant_deals_failed_orders', 'MerchantTransactionController@deals_failed_orders');


Route::post('merchant_deals_success_orders', 'MerchantTransactionController@deals_success_orders');
Route::post('merchant_deals_completed_orders', 'MerchantTransactionController@deals_completed_orders');
Route::post('merchant_deals_hold_orders', 'MerchantTransactionController@deals_hold_orders');
Route::post('merchant_deals_failed_orders', 'MerchantTransactionController@deals_failed_orders');
/* merchant dealscod transaction */
Route::get('merchant_dealscod_all_orders', 'MerchantTransactionController@dealscod_all_orders');
Route::get('merchant_dealscod_completed_orders', 'MerchantTransactionController@dealscod_completed_orders');
Route::get('merchant_dealscod_hold_orders', 'MerchantTransactionController@dealscod_hold_orders');
Route::get('merchant_dealscod_failed_orders', 'MerchantTransactionController@dealscod_failed_orders');

Route::post('merchant_dealscod_all_orders', 'MerchantTransactionController@dealscod_all_orders');
Route::post('merchant_dealscod_completed_orders', 'MerchantTransactionController@dealscod_completed_orders');
Route::post('merchant_dealscod_hold_orders', 'MerchantTransactionController@dealscod_hold_orders');
Route::post('merchant_dealscod_failed_orders', 'MerchantTransactionController@dealscod_failed_orders');

/* merchant products transaction */
Route::get('merchant_product_all_orders', 'MerchantTransactionController@product_all_orders_new_h');
Route::get('merchant_product_success_orders', 'MerchantTransactionController@product_success_orders');
Route::get('merchant_product_failed_orders', 'MerchantTransactionController@product_failed_orders');
Route::get('merchant_product_completed_orders', 'MerchantTransactionController@product_completed_orders');
Route::get('merchant_product_hold_orders', 'MerchantTransactionController@product_hold_orders');
Route::get('merchant_product_pick_orders', 'MerchantTransactionController@product_pick_orders');
Route::get('merchant_product_packing_orders', 'MerchantTransactionController@product_packing_orders');
Route::get('merchant_product_shipping_orders', 'MerchantTransactionController@product_shipping_orders');
Route::get('update_outbound_merchant_submit', 'MerchantTransactionController@update_outbound_merchant_submit');

Route::post('input_nomor_resi_merchant','MerchantTransactionController@input_nomor_resi_merchant');

Route::get('update_status_outbound_transaksi', 'TransactionController@update_status_outbound_transaksi');

Route::post('merchant_product_all_orders', 'MerchantTransactionController@product_all_orders');
Route::post('merchant_product_success_orders', 'MerchantTransactionController@product_success_orders');
Route::post('merchant_product_failed_orders', 'MerchantTransactionController@product_failed_orders');
Route::post('merchant_product_completed_orders', 'MerchantTransactionController@product_completed_orders');
Route::post('merchant_product_hold_orders', 'MerchantTransactionController@product_hold_orders');
Route::post('merchant_product_pick_orders', 'MerchantTransactionController@product_pick_orders');
Route::post('merchant_product_packing_orders', 'MerchantTransactionController@product_packing_orders');
Route::post('merchant_product_shipping_orders', 'MerchantTransactionController@product_shipping_orders');


/* merchant productscod transaction */
Route::get('merchant_product_cod_all_orders', 'MerchantTransactionController@cod_all_orders');
Route::get('merchant_product_cod_completed_orders', 'MerchantTransactionController@cod_completed_orders');
Route::get('merchant_product_cod_hold_orders', 'MerchantTransactionController@cod_hold_orders');
Route::get('merchant_product_cod_failed_orders', 'MerchantTransactionController@cod_failed_orders');

Route::post('merchant_product_cod_all_orders', 'MerchantTransactionController@cod_all_orders');
Route::post('merchant_product_cod_completed_orders', 'MerchantTransactionController@cod_completed_orders');
Route::post('merchant_product_cod_hold_orders', 'MerchantTransactionController@cod_hold_orders');
Route::post('merchant_product_cod_failed_orders', 'MerchantTransactionController@cod_failed_orders');


//admin Transactions
Route::get('show_transactions', 'TransactionController@show_transactions');
Route::get('show_all_deals_transactions', 'TransactionController@show_all_deals_transactions');
Route::get('auction_bidder', 'TransactionController@auction_bidder');
Route::get('manage_auction_bidder', 'TransactionController@manage_auction_bidder');
Route::get('auction_by_bidder', 'TransactionController@auction_by_bidder');
Route::get('list_auction_bidders/{id}', 'TransactionController@list_auction_bidders');
Route::get('auction_winner/{id}/{pageid}', 'TransactionController@auction_winner');
Route::get('send_auction_to_winner/{id}/{pageid}', 'TransactionController@send_auction_to_winner');
Route::post('send_auction_to_winner_submit', 'TransactionController@send_auction_to_winner_submit');
Route::get('all_fund_request', ['middleware' => 'auth', 'uses' => 'TransactionController@all_fund_request']);
Route::get('confirmed_fund_request', ['middleware' => 'auth', 'uses' => 'TransactionController@success_fund_request']);
Route::get('unpaid_fund_request', ['middleware' => 'auth', 'uses' => 'TransactionController@pending_fund_request']);
Route::get('paid_fund_request', ['middleware' => 'auth', 'uses' => 'TransactionController@failed_fund_request']);
Route::get('download_fund_request_invoice/{wd_id}', ['middleware' => 'auth', 'uses' => 'TransactionController@download_fund_request_invoice']);



Route::get('fund_paypal/{data}', 'TransactionController@fund_paypal');
Route::post('fund_pay/{id}', ['middleware' => 'auth', 'uses' => 'TransactionController@fund_pay']);
Route::get('upload_ulang_bukti/{id}', ['middleware' => 'auth', 'uses' => 'TransactionController@upload_ulang_bukti']);
Route::post('paypal_success', 'TransactionController@paypal_success');
Route::post('paypal_ipn', 'TransactionController@paypal_ipn');
Route::get('paypal_cancel', 'TransactionController@paypal_cancel');
Route::get('setujui_columbia/{id}', 'TransactionController@setujui_columbia');
Route::get('tolak_columbia/{id}', 'TransactionController@tolak_columbia');


Route::get('update_cod_order_status', 'MerchantTransactionController@update_order_cod');
Route::get('update_columbia_order_status', 'MerchantTransactionController@update_order_columbia');
Route::get('update_transfer_bank_order_status', 'MerchantTransactionController@update_transfer_bank_order_status');

Route::get('product_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@product_all_orders']);
Route::post('product_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@product_all_orders']);
Route::get('product_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@product_success_orders']);
Route::post('product_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@product_success_orders']);
Route::get('product_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@product_failed_orders']);
Route::post('product_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@product_failed_orders']);
Route::get('product_completed_orders', 'TransactionController@product_completed_orders');
Route::get('product_hold_orders', 'TransactionController@product_hold_orders');
Route::get('cod_all_orders', 'TransactionController@cod_all_orders');
Route::post('cod_all_orders', 'TransactionController@cod_all_orders');
Route::get('cod_completed_orders', 'TransactionController@cod_completed_orders');
Route::get('cod_hold_orders', 'TransactionController@cod_hold_orders');
Route::get('cod_failed_orders', 'TransactionController@cod_failed_orders');


Route::get('columbia_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@columbia_all_orders']);
Route::get('columbia_hold_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@columbia_hold_orders']);
Route::get('columbia_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@columbia_failed_orders']);
Route::get('columbia_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@columbia_success_orders']);

Route::get('tfr_bank_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_all_orders']);
Route::post('tfr_bank_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_all_orders']);
Route::get('tfr_bank_hold_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_hold_orders']);
Route::post('tfr_bank_hold_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_hold_orders']);
Route::get('tfr_bank_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_success_orders']);
Route::post('tfr_bank_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_success_orders']);
Route::get('tfr_bank_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_failed_orders']);
Route::post('tfr_bank_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@tfr_bank_failed_orders']);

Route::get('cash_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_all_orders']);
Route::post('cash_all_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_all_orders']);
Route::get('cash_hold_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_hold_orders']);
Route::post('cash_hold_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_hold_orders']);
Route::get('cash_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_success_orders']);
Route::post('cash_success_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_success_orders']);
Route::get('cash_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_failed_orders']);
Route::post('cash_failed_orders', ['middleware' => 'auth', 'uses' => 'TransactionController@cash_failed_orders']);

Route::get('merchant_all_transaction', ['middleware' => 'auth', 'uses' => 'TransactionController@merchant_all_transaction']);

Route::post('cod_completed_orders', 'TransactionController@cod_completed_orders');
Route::post('cod_hold_orders', 'TransactionController@cod_hold_orders');
Route::post('cod_failed_orders', 'TransactionController@cod_failed_orders');

Route::get('deals_all_orders', 'TransactionController@deals_all_orders');
Route::post('deals_all_orders', 'TransactionController@deals_all_orders');
Route::get('deals_success_orders', 'TransactionController@deals_success_orders');
Route::post('deals_success_orders', 'TransactionController@deals_success_orders');
Route::get('deals_failed_orders', 'TransactionController@deals_failed_orders');
Route::post('deals_failed_orders', 'TransactionController@deals_failed_orders');
Route::get('deals_completed_orders', 'TransactionController@deals_completed_orders');
Route::post('deals_completed_orders', 'TransactionController@deals_completed_orders');
Route::get('deals_hold_orders', 'TransactionController@deals_hold_orders');
Route::post('deals_hold_orders', 'TransactionController@deals_hold_orders');

Route::get('dealscod_all_orders', 'TransactionController@dealscod_all_orders');
Route::get('dealscod_completed_orders', 'TransactionController@dealscod_completed_orders');
Route::get('dealscod_hold_orders', 'TransactionController@dealscod_hold_orders');
Route::get('dealscod_failed_orders', 'TransactionController@dealscod_failed_orders');

Route::post('dealscod_all_orders', 'TransactionController@dealscod_all_orders');
Route::post('dealscod_completed_orders', 'TransactionController@dealscod_completed_orders');
Route::post('dealscod_hold_orders', 'TransactionController@dealscod_hold_orders');
Route::post('dealscod_failed_orders', 'TransactionController@dealscod_failed_orders');

Route::get('download_fund_request/{id}', 'FundController@download_fund_request');

Route::get('fund_request', 'FundController@fund_request');
Route::get('konfirmasi_fund_request/{id}', 'FundController@konfirmasi_fund_request');
Route::get('with_fund_request', 'FundController@with_fund_request');
Route::post('withdraw_submit', 	'FundController@withdraw_submit');








//local mobile json route

Route::any('front_end_banner_image', 'MobileController@home_page_banner_image');
Route::any('country_list', 'MobileController@country_list');
Route::any('city_list', 'MobileController@mobile_city_list');
Route::any('normal_signup/{name}/{email}/{password}/{country}/{city}', 'MobileController@user_signup');
Route::any('signin/{email}/{password}', 'MobileController@user_login');
Route::any('facebook_signup/{name}/{email}', 'MobileController@facebook_login');
Route::any('all_main_category_list', 'MobileController@all_main_category_list');
Route::any('mobile_products', 'MobileController@Products');
Route::any('product_detail_page/{id}', 'MobileController@product_mobile_category_list');
Route::any('product_detail_page_image_list/{id}', 'MobileController@product_detail_page_image_list');
Route::any('mobile_deals', 'MobileController@Deals');
Route::any('mobile_deals_detail_view/{id}', 'MobileController@deals_detail_view');
Route::any('deal_detail_page_image_list/{id}', 'MobileController@deal_detail_page_image_list');
Route::any('mobile_auctions', 'MobileController@Acutions');
Route::any('auction_detail_view/{id}', 'MobileController@auction_detail_view');
Route::any('auction_detail_view_image_list/{id}', 'MobileController@auction_detail_view_image_list');
Route::any('auction_bidder_detail/{id}', 'MobileController@auction_bidder_detail');
Route::any('auction_customer_bidder_detail/{id}', 'MobileController@auction_customer_bidder_detail');

Route::any('mobile_bid_payment/{bid_auc_id}/{oa_cus_id}/{oa_cus_name}/{oa_cus_email}/{oa_cus_address}/{oa_bid_amt}/{oa_bid_shipping_amt}/{oa_original_bit_amt}', 'MobileController@mobile_bid_payment');
Route::any('update_user_profile/{cus_id}/{name}/{email}/{phone}/{country_id}/{city_id}', 'MobileController@update_user_profile');
Route::any('my_buys/{id}', 'MobileController@product_my_buys');
Route::any('profile_image_submit/{cus_id}', 'MobileController@profile_image_submit');
Route::any('sub_category_list/{id}', 'MobileController@sub_category_list');
Route::any('country_selected_city/{id}', 'MobileController@country_selected_city');
Route::any('facebook_signup/{name}/{email}/{password}/{country}/{city}', 'MobileController@facebook_login');
Route::any('cash_and_delivery/{cus_id}/{cust_name}/{cust_address}/{cust_mobile}/{cust_city}/{cust_country}/{cust_zip}', 'MobileController@shipping_delivery');
Route::any('purchase_cod_order_list/{cus_id}/{pro_id}/{cod_qty}/{cod_amt}/{cod_pro_color}/{cod_pro_size}/{order_type}/{ship_addr}/{token_id}', 'MobileController@purchase_cod_order_list');
Route::any('paypal/{cus_id}/{pro_id}/{cod_qty}/{cod_amt}/{cod_pro_color}/{cod_pro_size}/{order_type}/{ship_addr}/{token_id}', 'MobileController@paypal');
Route::any('bidding_history/{cus_id}', 'MobileController@bidding_history');

Route::any('near_me_map_products', 'MobileController@near_me_map_products');
Route::any('near_me_map_deals', 'MobileController@near_me_map_deals');
Route::any('near_me_map_auction', 'MobileController@near_me_map_auction');
Route::any('stores_list', 'MobileController@stores_list');


Route::any('get_profile_image/{cus_id}', 'MobileController@get_profile_image');



Route::any('store_dealview_detail_by_id/{store_id}', 'MobileController@store_dealview_detail_by_id');
Route::any('store_productview_detail_by_id/{store_id}', 'MobileController@store_productview_detail_by_id');
Route::any('store_auctionview_detail_by_id/{store_id}', 'MobileController@store_auctionview_detail_by_id');
Route::any('terms_condition', 'MobileController@terms_condition');
Route::any('related_product_details/{pid}', 'MobileController@related_product_details');
Route::any('related_deal_details/{deal_id}', 'MobileController@related_deal_details');
Route::any('related_auction_details/{deal_id}', 'MobileController@related_auction_details');
Route::any('add_position', 'MobileController@add_position');
Route::any('add_pages', 'MobileController@add_pages');
Route::any('request_for_advertisment/{add_title}/{ads_position}/{ads_pages}/{url}', 'MobileController@request_for_advertisment');
Route::any('forgot_password_check/{email}', 'MobileController@forgot_password_check');

/* 07nov16  */
Route::get('cms/{id}', 'FooterController@cms');
Route::get('aboutus','FooterController@aboutus');

// Vinod mobile api

Route::any('home_page', 'MobileController@home_page_details');
// grouping payment
Route::get('manage_payment_aktif_setting', ['middleware' => 'auth', 'uses' => 'ProductController@manage_payment_aktif_setting']);
Route::post('add_header_payment', ['middleware' => 'auth', 'uses' => 'ProductController@add_header_payment']);
Route::get('hapus_payment_header/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@hapus_payment_header']);
Route::get('update_payment_aktif_x', ['middleware' => 'auth', 'uses' => 'ProductController@update_payment_aktif_x']);
Route::get('update_channel', ['middleware' => 'auth', 'uses' => 'ProductController@update_channel']);
Route::get('update_payment_customer_group', ['middleware' => 'auth', 'uses' => 'ProductController@update_payment_customer_group']);
//product review
Route::get('manage_review', ['middleware' => 'auth', 'uses' => 'ProductController@manage_review']);

//
Route::get('add_groping_page', ['middleware' => 'auth', 'uses' => 'ProductController@add_groping_page']);
Route::get('edit_grouping_page/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@edit_grouping_page']);
Route::post('edit_grouping_submit', ['middleware' => 'auth', 'uses' => 'ProductController@edit_grouping_submit']);
Route::get('manage_model', ['middleware' => 'auth', 'uses' => 'ProductController@manage_model']);
Route::get('manage_product_pic', ['middleware' => 'auth', 'uses' => 'ProductController@manage_product_pic']);
Route::post('add_grouping_model_header', ['middleware' => 'auth', 'uses' => 'ProductController@add_grouping_model_header']);
Route::get('delete_model_header/{id_model}', ['middleware' => 'auth', 'uses' => 'ProductController@delete_model_header']);
Route::get('keluarkan_model/{id_detail}/{id_header}', ['middleware' => 'auth', 'uses' => 'ProductController@keluarkan_model']);
Route::get('tambahkan_model/{id_pro}/{id_header}', ['middleware' => 'auth', 'uses' => 'ProductController@tambahkan_model']);

//
Route::get('halaman_product', ['middleware' => 'auth', 'uses' => 'ProductController@halaman_product']);
Route::get('tambah_grop/{id_pro}/{id_group}', ['middleware' => 'auth', 'uses' => 'ProductController@TambahakanProduct']);
Route::post('sort_product/{id_pro}/{id_group}', ['middleware' => 'auth', 'uses' => 'ProductController@sort_product']);
Route::get('keluarkan_group/{id_pro}/{id_group}', ['middleware' => 'auth', 'uses' => 'ProductController@KeluarkanProduct']);
Route::get('add_product_model/{id_header}', ['middleware' => 'auth', 'uses' => 'ProductController@add_manage_model']);

Route::post('add_grouping_master', ['middleware' => 'auth', 'uses' => 'ProductController@add_grouping_master']);
Route::get('delete_master_grouping/{id_master_group}', ['middleware' => 'auth', 'uses' => 'ProductController@delete_master_grouping']);


Route::get('edit_review/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@edit_review']);
Route::post('edit_review_submit', ['middleware' => 'auth', 'uses' => 'ProductController@edit_review_submit']);
Route::get('block_review/{id}/{status}', ['middleware' => 'auth', 'uses' => 'ProductController@block_review']);
Route::get('delete_review/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@delete_review']);

//deal review
Route::get('manage_deal_review','DealsController@manage_deal_review');
Route::get('edit_deal_review/{id}', 'DealsController@edit_deal_review');
Route::post('edit_deal_review_submit', 'DealsController@edit_deal_review_submit');
Route::get('block_deal_review/{id}/{status}','DealsController@block_deal_review');
Route::get('delete_deal_review/{id}', 'DealsController@delete_deal_review');

//for autocomplete
Route::get('auto_get_city', 'CityController@auto_get_city');
Route::get('auto_get_district', 'CityController@auto_get_district');
Route::get('auto_get_subdistrict', 'CityController@auto_get_subdistrict');

// Shipping
//Shipping
// Shipping
Route::get('request_shipping', 'nPembayaranController@kirim_request');
Route::get('cek_kupon', 'nPembayaranController@cek_kupon');

//Oauth2
Route::post('oauth/access_token', function() {
 return Response::json(Authorizer::issueAccessToken());
});

//Testing Oauth2
Route::get('harry',[ 'before' => 'oauth', function() {
 return "horey";
}]);

//Api STF Client Update Invoice
//Route::post('api/update_invoice', ['before' => 'oauth', 'uses' => 'HomeController@api_update_invoice']);

Route::post('api/update_invoice', ['before' => 'oauth', 'uses' => 'HomeController@api_update_invoice']);
Route::get('api/status_garansi', ['before' => 'oauth', 'uses' => 'HomeController@status_garansi']);

// Api STF Client Create Store
Route::post('api/create_store', ['before' => 'oauth', 'uses' => 'StoreController@apicreatestore']);

// Api STF Client Create Product
//Route::post('api/create_product', 'ProductController@apicreateproduct');

//Api STF Client Create Product with oauth
Route::post('api/create_product',['before' => 'oauth', 'uses' =>'ProductController@apicreateproduct']);
Route::get('api/get_product_img',['before' => 'oauth', 'uses' =>'ProductController@apiGetImage']);
Route::put('api/grade', ['before' => 'oauth', 'uses' =>'ProductController@api_grade']);
//API City
Route::get('api/get_city', 'CityController@getCity');
Route::post('api/create_city', ['before' => 'oauth', 'uses' => 'CityController@apiCreateCity']);
Route::put('api/update_city', ['before' => 'oauth', 'uses' => 'CityController@apiUpdateCity']);
Route::delete('api/delete_city', ['before' => 'oauth', 'uses' => 'CityController@apiDeleteCity']);

//API Country
Route::post('api/create_country', ['before' => 'oauth', 'uses' => 'CountryController@apiCreateCountry']);
Route::put('api/update_country', ['before' => 'oauth', 'uses' => 'CountryController@apiUpdateCountry']);
Route::delete('api/delete_country', ['before' => 'oauth', 'uses' => 'CountryController@apiDeleteCountry']);

//API Country
Route::post('api/create_currency', ['before' => 'oauth', 'uses' => 'CountryController@apiCreateCurrency']);
Route::put('api/update_currency', ['before' => 'oauth', 'uses' => 'CountryController@apiUpdateCurrency']);
Route::delete('api/delete_currency', ['before' => 'oauth', 'uses' => 'CountryController@apiDeleteCurrency']);

//API Merchant
Route::post('api/create_merchant', ['before' => 'oauth', 'uses'=> 'MerchantController@apiCreateMerchant']);
Route::put('api/update_merchant', ['before' => 'oauth', 'uses' => 'MerchantController@apiUpdateMerchant']);
Route::delete('api/delete_merchant', ['before' => 'oauth', 'uses' => 'MerchantController@apiDeleteMerchant']);

//API Update Stock
Route::put('api/update_stock', ['before' => 'oauth', 'uses' => 'ProductController@apiUpdateStock']);

//API Color
Route::post('api/create_color', ['before' => 'oauth', 'uses' => 'ProductController@apiCreateColor']);
Route::put('api/update_color', ['before' => 'oauth', 'uses' => 'ProductController@apiUpdateColor']);
Route::delete('api/delete_color', ['before' => 'oauth', 'uses' => 'ProductController@apiDeleteColor']);
//Testing Oauth2
// Route::get('harry', function() {
//  return "horey";
// });

//isnert
Route::post('api/insert_top_category', ['before'=>'oauth', 'uses' =>'CategoryController@api_insert_top_category']);
Route::post('api/insert_main_category', ['before'=> 'oauth', 'uses' =>'CategoryController@api_insert_main_category']);
Route::post('api/insert_sub_category', ['before'=>'oauth', 'uses' =>'CategoryController@api_insert_sub_category']);
Route::post('api/insert_secsub_category',['before'=>'oauth', 'uses' =>'CategoryController@api_insert_secsub_category']);

//update
Route::post('api/update_top_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_update_top_category']);
Route::post('api/update_main_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_update_main_category']);
Route::post('api/update_sub_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_update_sub_category']);
Route::post('api/update_secsub_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_update_secsub_category']);

//delete
Route::post('api/delete_top_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_delete_top_category']);
Route::post('api/delete_main_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_delete_main_category']);
Route::post('api/delete_sub_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_delete_sub_category']);
Route::post('api/delete_secsub_category', ['before' => 'oauth', 'uses' =>'CategoryController@api_delete_secsub_category']);

//Route::get('api/merchant_orders',['before' => 'oauth','uses' => 'HomeController@get_select_api_fathan_merchant_order']);

Route::get('api/merchant_orders/','HomeController@get_select_api_fathan_merchant_order');

//Get Location Popbox
Route::get('get_location_popbox', 'nPembayaranController@get_location');
Route::get('get_address_location', 'nPembayaranController@get_address_location');

//SiteAdmin -> Settings -> Partners Management
Route::get('add_partners', ['middleware' => 'auth', 'uses' => 'PartnersController@add_partners']);
Route::post('add_partners_submit', ['middleware' => 'auth', 'uses' => 'PartnersController@add_partners_submit']);
Route::get('manage_partners', ['middleware' => 'auth', 'uses' => 'PartnersController@manage_partners']);
Route::get('edit_partners/{id}', ['middleware' => 'auth', 'uses' => 'PartnersController@edit_partners']);
Route::post('edit_partners_submit', ['middleware' => 'auth', 'uses' => 'PartnersController@edit_partners_submit']);
Route::get('delete_partners/{id}', ['middleware' => 'auth', 'uses' => 'PartnersController@delete_partners']);
Route::get('status_partners_submit/{id}/{status}', ['middleware' => 'auth', 'uses' => 'PartnersController@status_partners_submit']);

//API Kecamatan dan Kelurahan
Route::get('api/get_district', 'CityController@getDistrict');
Route::get('api/get_subdistrict', 'CityController@getSubDistrict');
Route::put('api/put_district', ['before' => 'oauth', 'uses'=>'CityController@apiPutDistrict']);
Route::put('api/put_subdistrict', ['before' => 'oauth', 'uses' => 'CityController@apiPutSubdistrict']);

//API Provinsi
Route::put('api/put_province', ['before' => 'oauth', 'uses' => 'ProvinceController@apiPutProvince']);

//autocomplete Provinsi
Route::get('auto_provinsi', 'ProvinceController@auto_provinsi');

//Bayar ulang
Route::get('bayar_ulang/{jenis}', 'HomeController@bayar_ulang');

// User Management
Route::get('users', ['middleware' => 'auth', 'uses' => 'UserManagementController@users']);
Route::get('add_users', ['middleware' => 'auth', 'uses' => 'UserManagementController@add_users']);
Route::post('add_user_submit', ['middleware' => 'auth', 'uses' => 'UserManagementController@add_user_submit']);
Route::get('view_users/{id}', ['middleware' => 'auth', 'uses' => 'UserManagementController@view_users']);
Route::post('view_user_submit', ['middleware' => 'auth', 'uses' => 'UserManagementController@view_user_submit']);
Route::get('delete_user/{id}', ['middleware' => 'auth', 'uses' => 'UserManagementController@delete_user']);
Route::post('delete_users', ['middleware' => 'auth', 'uses' => 'UserManagementController@delete_users']);
Route::get('roles', ['middleware' => 'auth', 'uses' => 'UserManagementController@roles']);
Route::get('add_roles', ['middleware' => 'auth', 'uses' => 'UserManagementController@add_roles']);
Route::post('add_roles_submit', ['middleware' => 'auth', 'uses' => 'UserManagementController@add_roles_submit']);
Route::get('view_roles/{id}', ['middleware' => 'auth', 'uses' => 'UserManagementController@view_roles']);
Route::post('view_roles_submit', ['middleware' => 'auth', 'uses' => 'UserManagementController@view_roles_submit']);
Route::get('delete_role/{id}', ['middleware' => 'auth', 'uses' => 'UserManagementController@delete_role']);
Route::post('delete_roles', ['middleware' => 'auth', 'uses' => 'UserManagementController@delete_roles']);
Route::get('privileges', ['middleware' => 'auth', 'uses' => 'UserManagementController@privileges']);
Route::get('add_privileges', ['middleware' => 'auth', 'uses' => 'UserManagementController@add_privileges']);
Route::post('add_privileges_submit', ['middleware' => 'auth', 'uses' => 'UserManagementController@add_privileges_submit']);
Route::get('view_privileges/{id}', ['middleware' => 'auth', 'uses' => 'UserManagementController@view_privileges']);
Route::post('view_privileges_submit', ['middleware' => 'auth', 'uses' => 'UserManagementController@view_privileges_submit']);
Route::get('delete_privilege/{id}', ['middleware' => 'auth', 'uses' => 'UserManagementController@delete_privilege']);
Route::post('delete_privileges', ['middleware' => 'auth', 'uses' => 'UserManagementController@delete_privileges']);
Route::get('password_reset_request/{uuid}', 'UserManagementController@password_reset_request');
Route::post('create_password', 'UserManagementController@create_password');
Route::post('reset_password', 'UserManagementController@reset_password');

//audit trail
Route::get('audit_trail', ['middleware' => 'auth', 'uses' => 'UserManagementController@audit_trail']);
Route::post('audit_trail', ['middleware' => 'auth', 'uses' => 'UserManagementController@audit_trail']);

//API Sepulsa
Route::get('test_sepulsa', 'HomeController@test_sepulsa');
Route::get('sepulsa_product_details', 'ProductController@sepulsa_product_details');
Route::get('inquire_sepulsa', 'HomeController@inquire_sepulsa');
Route::get('auto_get_pdam_operators', 'HomeController@auto_get_pdam_operators');
Route::post('sepulsa_checkout_process', 'HomeController@sepulsa_checkout_process');
Route::get('run_auto_update_sepulsa', 'HomeController@run_auto_update_sepulsa');
Route::get('callback_sepulsa', 'HomeController@callback_sepulsa');
