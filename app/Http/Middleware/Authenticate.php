<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Users;
use App\UsersRoles;
use App\Roles;
use App\RolesPrivileges;
use App\Privileges;
use Illuminate\Support\Facades\Redirect;
use Session;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$path = explode('/', $request->path());
		if (Session::get('username')) {			
			$user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			if ($user_role) {
				foreach ($user_role as $role) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $role->ur_roles_name)->get();
					foreach ($role_privilege as $privilege) {
						$priv_access = explode(' ', $privilege->rp_priv_name);
						if ($path[0] == $priv_access[4]) {
							return $next($request);
						}
					}
				}
			}
			return response('Unauthorized', 401);
		}else {
			return Redirect::to('siteadmin');
		}
		// if ($this->auth->guest())
		// {
		// 	if ($request->ajax())
		// 	{
		// 		return response('Unauthorized.', 401);
		// 	}
		// 	else
		// 	{
		// 		return redirect()->guest('auth/login');
		// 	}
		// }
	}

}
