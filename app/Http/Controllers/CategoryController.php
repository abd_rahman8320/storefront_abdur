<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Country;
use App\Customer;
use App\City;
use App\Category;
use App\UsersRoles;
use App\RolesPrivileges;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
  const status = 1;
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function view_include()
	{
		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}

            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", "settings")->with('privileges', $privileges);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menus')->with('privileges', $privileges);
            $adminfooter      = view('siteadmin.includes.admin_footer');
            $return = [
                'adminheader' => $adminheader,
                'adminleftmenus' => $adminleftmenus,
                'adminfooter' => $adminfooter
            ];
            return $return;
        } else {
            return Redirect::to('siteadmin');
        }
	}

    public function add_category()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            return view('siteadmin.add_category')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_category()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $maincatg_list     = Category::maincatg_list();
            $maincatg_sub_list = Category::maincatg_sub_list($maincatg_list);
            return view('siteadmin.manage_categories')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('maincatg_list', $maincatg_list)->with('maincatg_sub_list', $maincatg_sub_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_category_submit()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $rule      = array(
                'catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_category')->withErrors($validator->messages())->withInput();
            } else {
                $inputs = Input::all();
                $file   = Input::file('file');
                if ($file != '') {
                    $filename        = $file->getClientOriginalName();
                    $move_img        = explode('.', $filename);
                    $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
                    $destinationPath = './assets/categoryimage/';
                    $uploadSuccess   = Input::file('file')->move($destinationPath, $filename);
                    $deal            = '1,1,1';
                    $entry           = array(
                        'mc_name' => Input::get('catg_name'),
                        'mc_type' => $deal,
                        'mc_img' => $filename,
                        'mc_status' => Input::get('catg_status')
                    );
                    $return          = Category::save_category($entry);
                    return Redirect::to('manage_category')->with('success', 'Record Inserted Successfully');
                } else {
                    return Redirect::to('add_category')->with('error', 'Image field Required');
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_category($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $catg_details   = Category::selectedcatg_list($id);
            return view('siteadmin.edit_category')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('catg_details', $catg_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_category_submit()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $id        = Input::get('catg_id');
            $rule      = array(
                'catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_category/' . $id)->withErrors($validator->messages());
            } else {
                $inputs = Input::all();
                $deal   = '1,1,1';
                $file   = Input::file('file');
                $id     = Input::get('catg_id');
                if ($file != '') {
                    $filename        = $file->getClientOriginalName();
                    $move_img        = explode('.', $filename);
                    $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
                    $destinationPath = './assets/categoryimage/';
                    $uploadSuccess   = Input::file('file')->move($destinationPath, $filename);
                    $entry           = array(
                        'mc_name' => Input::get('catg_name'),
                        'mc_type' => $deal,
                        'mc_img' => $filename,
                        'mc_status' => Input::get('catg_status')
                    );
                } else {
                    $entry = array(
                        'mc_name' => Input::get('catg_name'),
                        'mc_type' => $deal,
                        'mc_status' => Input::get('catg_status')
                    );
                }
                $return = DB::table('nm_maincategory')->where('mc_id', $id)->update($entry);
                return Redirect::to('manage_category')->with('success', 'Record Updated Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function status_category_submit($id, $status)
    {
        if (Session::has('userid')) {
            $return = Category::status_category_submit($id, $status);
            return Redirect::to('manage_category')->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function delete_category($id)
    {
        if (Session::has('userid')) {
            $return = Category::delete_category($id);
            return Redirect::to('manage_category')->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_main_category($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $add_main_catg_details = Category::selectedcatg_list($id);
            return view('siteadmin.add_maincategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('add_main_catg_details', $add_main_catg_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_main_category_submit()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $rule      = array(
                'main_catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_main_category/' . Input::get('catg_id'))->withErrors($validator->messages())->withInput();
            } else {
                $inputs = Input::all();
                $entry  = array(
                    'smc_name' => Input::get('main_catg_name'),
                    'smc_status' => Input::get('catg_status'),
                    'smc_mc_id' => Input::get('catg_id')
                );
                $return = Category::save_main_category($entry);
                return Redirect::to('manage_category')->with('success', 'Record Inserted Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_main_category($catg_id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $sub_maincatg_list  = Category::sub_maincatg_list($catg_id);
            $subcatg_count_list = Category::subcatg_count_list($sub_maincatg_list);
            return view('siteadmin.manage_maincategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('sub_maincatg_list', $sub_maincatg_list)->with('subcatg_count_list', $subcatg_count_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_main_category($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $edit_main_catg_details = Category::edit_main_catg_details($id);
            return view('siteadmin.edit_maincategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('edit_main_catg_details', $edit_main_catg_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_main_category_submit()
    {
        if (Session::has('userid')) {
            $data         = Input::except(array(
                '_token'
            ));
            $catg_id      = Input::get('catg_id');
            $main_catg_id = Input::get('main_catg_id');
            $rule         = array(
                'main_catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator    = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_main_category')->withErrors($validator->messages())->withInput();
            } else {
                $inputs = Input::all();
                $entry  = array(
                    'smc_name' => Input::get('main_catg_name'),
                    'smc_status' => Input::get('catg_status')
                );
                $return = Category::save_edit_main_category($entry, $main_catg_id);
                return Redirect::to('manage_main_category/' . $catg_id)->with('success', 'Record Updated Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_sub_main_category($sub_id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $add_sub_catg_details = Category::add_sub_catg_details($sub_id);
            return view('siteadmin.add_subcategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('add_sub_catg_details', $add_sub_catg_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_sub_category_submit()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $id        = Input::get('catg_id');
            $main_id   = Input::get('main_catg_id');
            $rule      = array(
                'main_catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_sub_main_category/' . $id)->withErrors($validator->messages())->withInput();
            } else {
                $inputs = Input::all();
                $entry  = array(
                    'sb_name' => Input::get('main_catg_name'),
                    'sb_status' => Input::get('catg_status'),
                    'mc_id' => Input::get('main_catg_id'),
                    'sb_smc_id' => Input::get('catg_id')
                );
                $return = Category::save_sub_category($entry);
                return Redirect::to('manage_main_category/' . $main_id)->with('success', 'Record Inserted Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function status_main_category_submit($id, $mc_id, $status)
    {
        if (Session::has('userid')) {
            $return = Category::status_main_category_submit($id, $status);
            return Redirect::to('manage_main_category/' . $mc_id)->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function delete_main_category($id, $mc_id)
    {
        if (Session::has('userid')) {
            $return = Category::delete_main_category($id);
            return Redirect::to('manage_main_category/' . $mc_id)->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_sub_category($catg_id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $sub_catg_list         = Category::sub_catg_list($catg_id);
            $secsubcatg_count_list = Category::secsubcatg_count_list($sub_catg_list);
            return view('siteadmin.manage_subcategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('sub_catg_list', $sub_catg_list)->with('secsubcatg_count_list', $secsubcatg_count_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_secsub_main_category($sub_id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $add_secsub_main_category = Category::add_secsub_main_category($sub_id);
            return view('siteadmin.add_secsubcategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('add_secsub_main_category', $add_secsub_main_category);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_secsub_main_category_submit()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $id        = Input::get('catg_id');
            $main_id   = Input::get('main_catg_id');
            $rule      = array(
                'main_catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_secsub_main_category/' . $id)->withErrors($validator->messages())->withInput();
            } else {
                $inputs = Input::all();
                $entry  = array(
                    'ssb_name' => Input::get('main_catg_name'),
                    'ssb_status' => Input::get('catg_status'),
                    'ssb_sb_id' => Input::get('catg_id'),
                    'ssb_smc_id' => Input::get('top_catg_id'),
                    'mc_id' => Input::get('main_catg_id')
                );
                $return = Category::save_secsub_category($entry);
                return Redirect::to('manage_sub_category/' . $main_id)->with('success', 'Record Inserted Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_secsub_main_category($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $edit_secsub_catg_details = Category::edit_secsub_catg_details($id);
            return view('siteadmin.edit_secsubcategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('edit_secsub_catg_details', $edit_secsub_catg_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_secsub_category_submit()
    {
        if (Session::has('userid')) {
            $data         = Input::except(array(
                '_token'
            ));
            $catg_id      = Input::get('catg_id');
            $main_catg_id = Input::get('main_catg_id');
            $rule         = array(
                'main_catg_name' => 'required',
                'catg_status' => 'required'
            );
            $validator    = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_secsub_main_category/' . $catg_id)->withErrors($validator->messages())->withInput();
            } else {
                $inputs = Input::all();
                $entry  = array(
                    'sb_name' => Input::get('main_catg_name'),
                    'sb_status' => Input::get('catg_status')
                );
                $return = Category::save_editsecsub_main_category($entry, $catg_id);
                return Redirect::to('manage_sub_category/' . $main_catg_id)->with('success', 'Record Updated Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function status_subsec_category_submit($id, $mc_id, $status)
    {
        if (Session::has('userid')) {
            $return = Category::status_subsec_category_submit($id, $status);
            return Redirect::to('manage_sub_category/' . $mc_id)->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function delete_subsec_category($id, $mc_id)
    {
        if (Session::has('userid')) {
            $return = Category::delete_subsec_category($id);
            return Redirect::to('manage_sub_category/' . $mc_id)->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_secsubmain_category($catg_id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $secsub_catg_list = Category::secsub_catg_list($catg_id);

            return view('siteadmin.manage_secsubcategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('secsub_catg_list', $secsub_catg_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function status_secsub_category_submit($id, $mc_id, $status)
    {
        if (Session::has('userid')) {
            $return = Category::status_secsub_category_submit($id, $status);
            return Redirect::to('manage_secsubmain_category/' . $mc_id)->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public static function delete_secsub_category($id, $mc_id)
    {
        if (Session::has('userid')) {
            $return = Category::delete_secsub_category($id);
            return Redirect::to('manage_secsubmain_category/' . $mc_id)->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_sec1sub_main_category($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $edit_sec1sub_catg_details = Category::edit_sec1sub_catg_details($id);

            return view('siteadmin.edit_sec1subcategory')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('edit_sec1sub_catg_details', $edit_sec1sub_catg_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_sec1sub_category_submit()
    {
        if (Session::has('userid')) {
            $data         = Input::except(array(
                '_token'
            ));
            $catg_id      = Input::get('catg_id');
            $main_catg_id = Input::get('main_catg_id');
            $rule         = array(
                'main_catg_name' => 'required',
                'catg_status' => 'required'

            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_sec1sub_main_category/' . $catg_id)->withErrors($validator->messages())->withInput();

            } else {
                $inputs = Input::all();

                $entry  = array(
                    'ssb_name' => Input::get('main_catg_name'),
                    'ssb_status' => Input::get('catg_status')
                );
                $return = Category::save_editsec1sub_main_category($entry, $catg_id);
                return Redirect::to('manage_secsubmain_category/' . $main_catg_id)->with('success', 'Record Updated Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }


    //api
    public function api_insert_top_category(Request $request){
        //nm_maincategory
        $check_exist_top_category = DB::table('nm_maincategory')->where('mc_code', $request->input('mc_code'))->first();

        if(!empty($check_exist_top_category))
        {
            $isi = [];
            $isi = [
              "mc_name" => $request->input('mc_name'),
              "mc_status" => $request->input('mc_status'),
              "mc_code" => $request->input('mc_code'),
            ];
            Category::update_category_detail($isi, $request->input('mc_code'));
            return response("SUCCESS");
        }
        $status = self::status;
        $isi = [];
        $isi = [
            "mc_name"   => $request->input('mc_name'),
            "mc_code"   => $request->input('mc_code'),
            "mc_status" => $status,
        ];
        Category::save_category($isi);
        return response("SUCCESS");
    }

    public function api_insert_main_category(Request $request){
      //nm_secmaincategory
      $check_mc_code = DB::table('nm_maincategory')->where('mc_code', $request->input('mc_code'))->first();

      $check_exist_main_category = DB::table('nm_secmaincategory')->where('smc_code', $request->input('smc_code'))->first();

      if ($check_mc_code == null) {
          return response("FAILED mc code not found");
      }

      if(!empty($check_exist_main_category))
      {
          $isi = [];
          $isi = [
            "smc_name" => $request->input('smc_name'),
            "smc_status" => $request->input('smc_status'),
            "smc_code" => $request->input('smc_code'),
          ];

          if (!empty($check_mc_code->mc_code)) {
            Category::update_main_category($isi, $request->input('smc_code'));
            return response("SUCCESS");
          }else {
            return response("Error");
          }
      }

      $mc_id = Category::select_id_main($request->input('mc_code'));

      $status = self::status;
      $isi = [];
      $isi = [
        "smc_name"   => $request->input('smc_name'),
        "smc_code"   => $request->input('smc_code'),
        "smc_mc_id"  => $mc_id->mc_id,
        "smc_status" => $status,
      ];

      if (Category::save_main_category($isi)) {
        return response("SUCCESS");
      }else{
        return response("Error");
      }

    }

    public function api_insert_sub_category(Request $request){
      //nm_subcategory
      $sb_code = $request->input('sb_code');
      $check_exist_sub_category = DB::table('nm_subcategory')->where('sb_code', $sb_code)->first();
      $smc_id = Category::select_id_secmain($request->input('smc_code'));

if (!empty($smc_id)) {
      if(!empty($check_exist_sub_category))
      {

            if($request->input('sb_name')!=null)
                $name = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['sb_name'=>$request->input('sb_name')]);
            if($smc_id->smc_id!=null)
                $smc_id = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['sb_smc_id'=>$smc_id->smc_id]);
            if($smc_id['smc_mc_id']!=null)
                $mc_id = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['mc_id'=>$smc_id->smc_mc_id]);
            if($request->input('sb_status')!=null)
                $status = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['sb_status'=>$request->input('sb_status')]);
            return response('SUCCESS');
          }
      }else {
        return response('FAILED smc code is not found');
      }

        $status = self::status;
        $isi = [];
        $isi = [
          "sb_name"   => $request->input('sb_name'),
          "sb_code"   => $request->input('sb_code'),

          "sb_smc_id" => $smc_id->smc_id,
          "mc_id"     => $smc_id->smc_mc_id,
          "sb_status" => $status,
        ];
        if (Category::save_sub_category($isi)) {
          return response('SUCCESS');
        }else {
          return response('FAILED');
        }

      }


    public function api_insert_secsub_category(Request $request){
      //nm_secsubcategory
      $ssb_code = $request->input('ssb_code');
      $check_exist_secsub_category = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->first();
      $sb_id = Category::select_id_sub($request->input('sb_code'));

      if (!empty($sb_id)) {
        if(!empty($check_exist_secsub_category))
        {
            if($request->input('ssb_name')!=null)
                $name = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_name'=>$request->input('ssb_name')]);
            if($sb_id->mc_id!=null)
                $smc_id = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_smc_id'=>$sb_id->mc_id]);
            if($sb_id->sb_smc_id!=null)
                $mc_id = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['mc_id'=>$sb_id->sb_smc_id]);
            if($sb_id->sb_id!=null)
                $mc_id = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_sb_id'=>$sb_id->sb_id]);
            if($request->input('ssb_status')!=null)
                $status = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_status'=>$request->input('ssb_status')]);

            return response('SUCCESS');
        }else {
              $status = self::status;
              $isi = [];
              $isi = [
                "ssb_name"    => $request->input('ssb_name'),
                "ssb_code"    => $request->input('ssb_code'),
                "ssb_sb_id"   => $sb_id->sb_id,
                //entah kenapa terbalik
                "ssb_smc_id"  => $sb_id->mc_id,
                "mc_id"       => $sb_id->sb_smc_id,
                "ssb_status"  => $status,
              ];

              if (Category::save_secsub_category($isi)) {
                return response("SUCCESS");
              }else {
                return response("FAILED");
              }
        }
      }else {
        return response('FAILED sb_code is not found');
      }
    }





    public function api_update_top_category(Request $request){
        $check_exist_top_category = DB::table('nm_maincategory')->where('mc_code', $request->input('mc_code'))->first();
        $mc_id = $check_exist_top_category->mc_id;
        $isi = [];
        $isi = [
            "mc_name" => $request->input('mc_name'),
            "mc_type" => $request->input('mc_type'),
            "mc_img" => $request->input('mc_img'),
            "mc_status" => $request->input('mc_status'),
            "mc_code" => $request->input('mc_code'),
        ];
        Category::update_category_detail($isi, $request->input('mc_code'));
        return response("SUCCESS");
    }

    public function api_update_main_category(Request $request){
        $check_exist_main_category = DB::table('nm_secmaincategory')->where('smc_code', $request->input('smc_code'))->first();
        $smc_id = $check_exist_main_category->smc_id;
        $isi = [];
        $isi = [
            "smc_name" => $request->input('smc_name'),
            "smc_type" => $request->input('smc_type'),
            "mc_img" => $request->input('mc_img'),
            "mc_status" => $request->input('mc_status'),
            "mc_code" => $request->input('mc_code'),
        ];
        $update = DB::table('nm_secmaincategory')->where('smc_code', $request->input('smc_code'))->update($isi);
        return response("SUCCESS");
    }

    public function api_update_sub_category(Request $request)
    {
        $sb_code = $request->input('sb_code');
        $smc_id = Category::select_id_secmain($request->input('smc_code'));

        if($request->input('sb_name')!=null)
            $name = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['sb_name'=>$request->input('sb_name')]);
        if($smc_id->smc_id!=null)
            $smc_id = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['sb_smc_id'=>$smc_id->smc_id]);
        if($smc_id->smc_mc_id!=null)
            $mc_id = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['mc_id'=>$smc_id->smc_mc_id]);
        if($request->input('sb_status')!=null)
            $status = DB::table('nm_subcategory')->where('sb_code', $sb_code)->update(['sb_status'=>$request->input('sb_status')]);

        return response('SUCCESS');
    }

    public function api_update_secsub_category(Request $request)
    {
        $ssb_code = $request->input('ssb_code');
        $sb_id = Category::select_id_sub($request->input('sb_code'));

        if($request->input('ssb_name')!=null)
            $name = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_name'=>$request->input('ssb_name')]);
        if($sb_id->mc_id!=null)
            $smc_id = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_smc_id'=>$sb_id->mc_id]);
        if($sb_id->sb_smc_id!=null)
            $mc_id = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['mc_id'=>$sb_id->sb_smc_id]);
        if($sb_id->sb_id!=null)
            $mc_id = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_sb_id'=>$sb_id->sb_id]);
        if($request->input('ssb_status')!=null)
            $status = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->update(['ssb_status'=>$request->input('ssb_status')]);

        return response('SUCCESS');
    }

    public function api_delete_top_category(Request $request)
    {
        $mc_code = $request->input('mc_code');
        $check_exist_top_category = DB::table('nm_maincategory')->where('mc_code', $request->input('mc_code'))->first();
        $check_exist_main_category = DB::table('nm_secmaincategory')->where('smc_mc_id', $check_exist_top_category->mc_id)->first();
        $check_exist_sub_category = DB::table('nm_subcategory')->where('mc_id', $check_exist_top_category->mc_id)->first();
        $check_exist_secsub_category = DB::table('nm_secsubcategory')->where('mc_id', $check_exist_top_category->mc_id)->first();
        if($check_exist_top_category==null)
        {
            return response('Main Category Code Not Found', 400);
            die();
        }
        if($check_exist_main_category!=null
        || $check_exist_sub_category!=null
        || $check_exist_secsub_category!=null)
        {
            return response('Main Category Have Descendant. Delete His Descendant First!', 400);
            die();
        }
        $row = DB::table('nm_maincategory')->where('mc_code', $mc_code)->delete();
    }

    public function api_delete_main_category(Request $request)
    {
        $smc_code = $request->input('smc_code');
        $check_exist_main_category = DB::table('nm_secmaincategory')->where('smc_code', $request->input('smc_code'))->first();
        $check_exist_sub_category = DB::table('nm_subcategory')->where('sb_smc_id', $check_exist_main_category->smc_id)->first();
        $check_exist_secsub_category = DB::table('nm_secsubcategory')->where('ssb_smc_id', $check_exist_main_category->smc_id)->first();
        if($check_exist_main_category==null)
        {
            return response('Second Main Category Code Not Found', 400);
            die();
        }
        if($check_exist_sub_category!=null
        || $check_exist_secsub_category!=null)
        {
            return response('Second Main Category Have Descendant. Delete His Descendant First!', 400);
            die();
        }
        $row = DB::table('nm_secmaincategory')->where('smc_code', $smc_code)->delete();
    }

    public function api_delete_sub_category(Request $request)
    {
        $sb_code = $request->input('sb_code');
        $check_exist_sub_category = DB::table('nm_subcategory')->where('sb_code', $sb_code)->first();
        $check_exist_secsub_category = DB::table('nm_secsubcategory')->where('ssb_sb_id', $check_exist_sub_category->sb_id)->first();
        if($check_exist_sub_category==null)
        {
            return response('Sub Category Code Not Found', 400);
            die();
        }
        if($check_exist_secsub_category!=null)
        {
            return response('Sub Category Have Descendant. Delete His Descendant First!', 400);
            die();
        }
        $row = DB::table('nm_subcategory')->where('sb_code', $sb_code)->delete();
    }

    public function api_delete_secsub_category(Request $request)
    {
        $ssb_code = $request->input('ssb_code');
        $check_exist_secsub_category = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->first();
        if($check_exist_secsub_category==null)
        {
            return response('Second Sub Category Code Not Found', 400);
            die();
        }
        $row = DB::table('nm_secsubcategory')->where('ssb_code', $ssb_code)->delete();
    }

    public function delete_MC(Request $request){
      $row = Category::find($request->input('mc_id'));
      $row->delete();
      return response("SUCCESS");
    }

    public function update_sub_MC(Request $request){
      $row = Category::find($request->input('mc_id'));
      $row->mc_name = $request->input('mc_name');
      $row->mc_type = $request->input('mc_type');
      $row->mc_img = $request->input('mc_img');
      $row->mc_status = $request->input('mc_status');
      $row->save();
      return response("SUCCESS");
    }

}
