<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Country;
use App\Customer;
use App\City;
use App\Category;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class PartnersController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function view_include($routemenu, $left_menu)
 	{
 		if (Session::has('userid')) {
             $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $routemenu)->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.'.$left_menu)->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}
	public function add_partners()
	{
		if(Session::has('userid'))
		{
			$include = self::view_include('settings', 'admin_left_menus');
   		 $adminheader 	= $include['adminheader'];
   		 $adminleftmenus = $include['adminleftmenus'];
   		 $adminfooter 	= $include['adminfooter'];
		 return view('siteadmin.add_partners')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
		}
		else
		{
		return Redirect::to('siteadmin');
		}
	}

	public function manage_partners()
	{
		if(Session::has('userid'))
		{
		Session::put('adrequestcnt',0);
        DB::table('nm_partners')->update(array(
            'par_read_status' => 1
        ));
		$include = self::view_include('settings', 'admin_left_menus');
		$adminheader 	= $include['adminheader'];
		$adminleftmenus = $include['adminleftmenus'];
		$adminfooter 	= $include['adminfooter'];
		$parresult = DB::table('nm_partners')->get();
		return view('siteadmin.manage_partners')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('parresult', $parresult);
		}
		else
		{
		return Redirect::to('siteadmin');
		}
	}

	public function edit_partners($id)
	{
		if(Session::has('userid'))
		{
			$include = self::view_include('settings', 'admin_left_menus');
			$adminheader 	= $include['adminheader'];
			$adminleftmenus = $include['adminleftmenus'];
			$adminfooter 	= $include['adminfooter'];
		$parresult = DB::table('nm_partners')->where('par_id', '=', $id)->get();
		return view('siteadmin.edit_partners')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('parresult', $parresult)->with('id',$id);
		}
		else
		{
		return Redirect::to('siteadmin');
		}
	}

	public function status_partners_submit($id,$status)
	{
		if(Session::has('userid'))
		{
		$return = DB::table('nm_partners')->where('par_id', '=', $id)->update(array(
            'par_status' => $status
        ));
  		return Redirect::to('manage_partners')->with('updated_result','Record Updated Successfully');
		}
		else
		{
		return Redirect::to('siteadmin');
		}
	}

	public function delete_partners($id)
	{
		if(Session::has('userid'))
		{
			$return = DB::table('nm_partners')->where('par_id', $id)->delete();
  			return Redirect::to('manage_partners')->with('delete_result','Record Deleted Successfully');
		}
		else
		{
		return Redirect::to('siteadmin');
		}
	}

	public function add_partners_submit()
	{
		if(Session::has('userid'))
		{
		$data =  Input::except(array('_token')) ;
		$rule  =  array(
		'partitle' => 'required',
		'parposition' =>  'required',
		'redirecturl' => 'required',
		);
		$partitle=Input::get('partitle');
		$parposition=Input::get('parposition');
		$parpage=1;
		$parredirecturl=Input::get('redirecturl');
		$validator = Validator::make($data,$rule);
		$check_exist_partner_title = DB::table('nm_partners')->where('par_name', '=', $partitle)->get();
		if ($validator->fails())
		{
		return Redirect::to('add_partners')->withErrors($validator->messages())->withInput();
		}
		else if($check_exist_partner_title)
		{
		return Redirect::to('add_partners')->withMessage("Partner Title Already Exists")->withInput();
		}
		else
		{
		$inputs = Input::all();
		$file = Input::file('file');
		if($file!='')
		{
		$filename = $file->getClientOriginalName();
		$move_img = explode('.',$filename);
		$filename = $move_img[0].str_random(8).".".$move_img[1];
		$destinationPath = './assets/parimage/';
		$uploadSuccess = Input::file('file')->move($destinationPath,$filename);
		$entry = array(
		'par_name' => $partitle,
		'par_position' => $parposition,
		'par_pages' => $parpage,
		'par_redirecturl' =>$parredirecturl,
		'par_img' =>$filename,
		);
		$return = DB::table('nm_partners')->insert($entry);
		return Redirect::to('manage_partners')->with('insert_result','Record Inserted Successfully');
		}
		else
		{
		return Redirect::to('add_partners')->withMessage("Image field required")->withInput();
		}
		}
		}
		else
		{
		return Redirect::to('siteadmin');
		}
	}

	public function edit_partners_submit()
	{
        	if(Session::has('userid'))
		{
		$data = Input::except(array('_token')) ;
		$id = Input::get('id');
		$partitle=Input::get('editpartitle');
		$parposition=Input::get('editparposition');
		$parpage=1;
		$parredirecturl = Input::get('editredirecturl');
		$rule  =  array(
		'editpartitle' => 'required' ,
		'editparposition' =>  'required',
		'editredirecturl' => 'required',
	 	) ;
		$check_exist_par_title = DB::table('nm_partners')->where('par_name', '=', $partitle)->where('par_id', '!=', $id)->get();
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
		{
		return Redirect::to('edit_partners/'.$id)->withErrors($validator->messages())->withInput();
		}
		else if($check_exist_par_title)
		{
		return Redirect::to('edit_partners/'.$id)->withMessage("Partner Title Already Exists")->withInput();
		}
		else
		{
		$inputs = Input::all();
		$file = Input::file('file');
		$id = Input::get('id');
		if($file!='')
		{
		$filename = $file->getClientOriginalName();
		$move_img = explode('.',$filename);
		$filename = $move_img[0].str_random(8).".".$move_img[1];
		$destinationPath = './assets/parimage/';
		$uploadSuccess = Input::file('file')->move($destinationPath,$filename);
		$entry = array(
		'par_name' => $partitle,
		'par_position' => $parposition,
		'par_pages' => $parpage,
		'par_redirecturl' =>$parredirecturl,
		'par_img' =>$filename,
		);
		}
		else
		{
		$entry = array(
		'par_name' => $partitle,
		'par_position' => $parposition,
		'par_pages' => $parpage,
		'par_redirecturl' =>$parredirecturl,
		);
		}
		$return = DB::table('nm_partners')->where('par_id', '=', $id)->update($entry);
		return Redirect::to('manage_partners')->with('updated_result','Record Updated Successfully');
		}
		}
		else
		{
		return Redirect::to('siteadmin');
		}
		}
	}
