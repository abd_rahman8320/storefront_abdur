<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Customer;
use App\Attributes;
use App\Specification;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class SpecificationController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function view_include($routemenu, $left_menu)
 	{
 		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $routemenu)->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.'.$left_menu)->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}

    public function get_specification()
    {
        $get_specification = DB::table('nm_specification')->where('sp_spg_id', $_GET['spg_id'])->orderBy('sp_order', 'asc')->get();

        return $get_specification;
    }

    public function add_specification()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $groupresult    = Specification::get_specification_group();
            $groupresult1    = Specification::get_specification_value();

            return view('siteadmin.add_specification')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('groupresult', $groupresult)
            ->with('groupresult1', $groupresult1)
            ;
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_specification()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $specificationresult = Specification::viewjoin_specification_detail();
            $spgroups = DB::table('nm_spgroup')->orderBy('spg_name', 'asc')->get();
            return view('siteadmin.manage_specification')
            ->with('spgroups', $spgroups)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('specificationresult', $specificationresult);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_specification($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $specificationresult = Specification::showindividual_specification_detail($id);
            $groupresult         = Specification::get_specification_group();
            $groupresult1         = Specification::get_specification_value();
            $values              = Specification::edit_specification($id);



            return view('siteadmin.edit_specification')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('specificationresult', $specificationresult)
            ->with('values',$values)
            ->with('groupresult', $groupresult)
            ->with('id', $id)

            ->with('groupresult1', $groupresult1)

            ;

        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_specification($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $affected            = Specification::delete_specification_detail($id);
            $specificationresult = Specification::view_specification_detail($id);

            return Redirect::to('manage_specification')->with('delete_result', 'Record Deleted');
        } else {
            return Redirect::to('siteadmin');
        }

    }

    public function update_specification()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $inputs = Input::all();

            $entry = array(
                'sp_name' => Input::get('spedit_name'),
                //'sp_spg_id' => Input::get('speditgroup_name'),
                'sp_order' => Input::get('spedit_order'),
                'sp_type'   => Input::get('type')
            );

            $id = Input::get('id');
            $values = Input::get('value');


            $check_name  = Input::get('spedit_name');
            $groupid     = Input::get('speditgroup_name');
            $check_exist = Specification::check_exist_update($check_name, $groupid, $id);

            if ($id != "") {
                if ($check_exist) {
                    return Redirect::to('edit_specification/' . $id)->withMessage('Specification Name Exist')->withInput();
                } else {
                    $check_order = Input::get('spedit_order');
                    $order_exist = Specification::check_exist_individualorder_update($check_order, $groupid, $id);
                    if ($order_exist) {
                        $get_old_order = DB::table('nm_specification')->where('sp_id', $id)->first();
                        $old_order = $get_old_order->sp_order;

                        $new_order2 = DB::table('nm_specification')
                        ->where('sp_spg_id', Input::get('speditgroup_name'))
                        ->where('sp_order', Input::get('spedit_order'))
                        ->update(['sp_order'=>$old_order]);

                        $new_order1 = DB::table('nm_specification')
                        ->where('sp_id', $id)
                        ->update(['sp_order'=>Input::get('spedit_order')]);

                        return Redirect::to('manage_specification')->with('updated_result', 'Record Updated : Sort Order Exchanged');
                    } else {
                         //dd($entry);
                        $affected = Specification::update_specification_detail($id, $entry);
                        $delete_spec_value = DB::table('nm_spec_value')->where('nm_specification_id', $id)->delete();

                        if ($entry['sp_type'] == 'Selection') {
                            $i = 1;
                            if ($values != null) {
                                foreach ($values as $value) {
                                    $id_spec = (string) $id;
                                    $id_spec_value = (string) $i;
                                    $code_value = $id_spec.$id_spec_value;
                                    $entry_spec_value = [
                                        'nm_specification_id' => $id,
                                        'code_value' => $code_value,
                                        'short_desc' => $value,
                                        'long_desc' => $value
                                    ];
                                    $insert = DB::table('nm_spec_value')->insert($entry_spec_value);
                                    $i++;
                                }
                            }
                        }
                        return Redirect::to('manage_specification')->with('updated_result', 'Record Updated');
                    }

                }
            }


        } else {
            return Redirect::to('siteadmin');
        }
    }


    public function add_specification_submit()
    {
        $last_sort_order = DB::table('nm_specification')->where('sp_spg_id', Input::get('spgroup_name'))->orderBy('sp_order', 'desc')->first();

        $values = Input::get('value');

        if($last_sort_order){
            $sort_order = $last_sort_order->sp_order + 1;
        }else {
            $sort_order = 1;
        }
        if (Session::has('userid')) {

            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'sp_name' => 'required',
                'spgroup_name' => 'required',
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_specification')->withErrors($validator->messages())->withInput();

            } else {
                $entry = array(
                    'sp_name' => Input::get('sp_name'),
                    'sp_spg_id' => Input::get('spgroup_name'),
                    'sp_order' => $sort_order,
                    'sp_type'   => Input::get('type')
                );

                $check_name  = Input::get('sp_name');
                $groupid     = Input::get('spgroup_name');
                $check_exist = Specification::check_exist_individual($check_name, $groupid);
                if ($check_exist) {
                    return Redirect::to('add_specification')->withMessage('Specification Name Exist')->withInput();
                } else {
                    $check_order = $sort_order;
                    $order_exist = Specification::check_exist_individualorder($check_order, $groupid);
                    if ($order_exist) {


                        return Redirect::to('add_specification')->withMessage('Sort Order Exist')->withInput();
                    } else {


                        $return = Specification::save_specification_detail($entry);
                        $get_spec_id = Specification::get_specification2($entry);

                        $delete_spec_value = DB::table('nm_spec_value')->where('nm_specification_id', $get_spec_id -> sp_id)->delete();
                       //dd($entry);
                        if ($entry['sp_type'] == 'Selection') {
                        $i = 1;
                        if ($values != null) {
                            foreach ($values as $value) {
                                $id_spec = (string) $get_spec_id ->sp_id ;
                                $id_spec_value = (string) $i;
                                $code_value = $id_spec.$id_spec_value;
                                $entry_spec_value = [
                                    'nm_specification_id' => $get_spec_id -> sp_id,
                                    'code_value' => $code_value,
                                    'short_desc' => $value,
                                    'long_desc' => $value
                                ];
                                $insert = DB::table('nm_spec_value')->insert($entry_spec_value);
                                $i++;
                        }

                            }
                        }
                        return Redirect::to('manage_specification')->with('insert_result', 'Record Inserted');
                    }
                }

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_specification_group()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $secmaincategory_list   = DB::table('nm_secmaincategory')->orderBy('smc_name', 'asc')->get();
            return view('siteadmin.add_specification_group')->with('secmaincategory_list', $secmaincategory_list)->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_specification_group_submit()
    {
        $smc = DB::table('nm_secmaincategory')->where('smc_id', Input::get('category'))->first();
        $smc_name = $smc->smc_name;
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'category' => 'required',
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_specification_group')->withErrors($validator->messages())->withInput();
            } else {
                $entry       = array(
                    'spg_name' => $smc_name,
                    'spg_keterangan' => Input::get('keterangan'),
                    'spg_smc_id' => Input::get('category')
                );
                $check_name  = $smc_name;
                $check_exist = Specification::check_exist_group($check_name);
                if ($check_exist) {
                    return Redirect::to('add_specification_group')->withMessage('Group Exist')->withInput();
                }
                else {
                    $return = Specification::save_specification_group($entry);
                    return Redirect::to('manage_specification_group')->with('insert_result', 'Record Inserted');
                    // $check_order = Input::get('sort_order');
                    // $order_exist = Specification::check_exist_order($check_order);
                    // if ($order_exist) {
                    //     return Redirect::to('add_specification_group')->withMessage('Sort Order Exist')->withInput();
                    // } else {
                    //     $return = Specification::save_specification_group($entry);
                    //     return Redirect::to('manage_specification_group')->with('insert_result', 'Record Inserted');
                    // }
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_specification_group()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $specificationresult = Specification::get_specification_group();
            return view('siteadmin.manage_specification_group')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('specificationresult', $specificationresult);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_specification_group($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $secmaincategory_list   = DB::table('nm_secmaincategory')->get();
            $specificationresult = Specification::showindividual_specification_group_detail($id);

            return view('siteadmin.edit_specification_group')
            ->with('secmaincategory_list', $secmaincategory_list)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('specificationresult', $specificationresult)
            ->with('id', $id);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_specification_group_submit()
    {
        $smc = DB::table('nm_secmaincategory')->where('smc_id', Input::get('category'))->first();
        $smc_name = $smc->smc_name;
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $rule      = array(
                'category' => 'required',
            );
            $id        = Input::get('spg_id');
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_specification_group/' . $id)->withErrors($validator->messages())->withInput();
            } else {
                $entry       = array(
                    'spg_name' => $smc_name,
                    'spg_keterangan' => Input::get('keterangan'),
                    'spg_smc_id' => Input::get('category')
                );
                $check_name  = $smc_name;
                $check_exist = Specification::check_exist_group_update($check_name, $id);
                if ($check_exist) {
                    return Redirect::to('edit_specification_group/' . $id)->withMessage('Group Exist')->withInput();
                } else {
                    $return = Specification::update_specification_group($id, $entry);
                    return Redirect::to('manage_specification_group')->with('updated_result', 'Record Updated');
                    // $check_order = Input::get('sort_order');
                    // $order_exist = Specification::check_exist_order_update($check_order, $id);
                    // if ($order_exist) {
                    //     return Redirect::to('edit_specification_group/' . $id)->withMessage('Sort Order Exist')->withInput();
                    // } else {
                    //     $return = Specification::update_specification_group($id, $entry);
                    //     return Redirect::to('manage_specification_group')->with('updated_result', 'Record Updated');
                    // }
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_specification_group($id)
    {
        if (Session::has('userid')) {
            $affected = Specification::delete_specification_group_detail($id);
            return Redirect::to('manage_specification_group')->with('delete_result', 'Record Deleted');
        } else {
            return Redirect::to('siteadmin');
        }
    }
}
