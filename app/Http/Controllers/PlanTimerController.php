<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Excel;
use URL;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\PlanTimer;
use App\Auction;
use App\Products;
use App\UsersRoles;
use App\RolesPrivileges;
use App\AuditTrail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PlanTimerController extends Controller{

    public static function view_include($routemenu, $left_menu)
 	{
 		if (Session::has('userid')) {
             $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $routemenu)->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.'.$left_menu)->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}

    public function get_schedule(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');
        $status = $request->input('status');
        if($status==1){
            $get = DB::table('nm_scheduled_promo')->where('schedp_status', $status)->where('schedp_end_date', '>', $date)->get();
        }elseif ($status==0) {
            $get = DB::table('nm_scheduled_promo')->where('schedp_status', $status)->orWhere('schedp_end_date', '<=', $date)->get();
        }elseif($status==2){
            $get = DB::table('nm_scheduled_promo')->get();
        }
        // dd($get);
        return $get;
    }

  public function search_product(Request $request){
      $paramTitle = $request->input('pro_title');
      $paramKategori = $request->input('kategori');
      $paramSkip = $request->input('skip');
      $paramCount = $request->input('count');
      $paramMerchant = $request->input('merchant');

     $result = Products::search($paramTitle, $paramKategori, $paramSkip, $paramCount, $paramMerchant);

     $data =  [
       "total" => $result['total'],
       "data" => $result['data']
     ];
     return response()->json($data);
  }

  public function get_promo_flash(Request $request){
        $paramTitle = $request->input('pro_title');
        $result = Products::get_promo_flash_product();

        $data =  [
            "data" => $result
        ];
     return response()->json($data);
    }

  public function mer_search_product(Request $request){
      $paramTitle = $request->input('pro_title');
      $paramKategori = $request->input('kategori');
      $paramSkip = $request->input('skip');
      $paramCount = $request->input('count');
      $id = Session::get('merchantid');
     $result = Products::mer_search($paramTitle, $paramKategori, $paramSkip, $paramCount,$id);

     $data =  [
       "total" => $result['total'],
       "data" => $result['data']
     ];
     return response()->json($data);
  }

  public function add_plan_timer(){
    if (Session::has('userid')) {
        $include = self::view_include('deals', 'admin_left_menu_deals');
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];
        $category         = Deals::get_category();
        $merchant_details = Deals::get_merchant_details();
        $get_products      = Products::get_promo_product();
        $list_postal_sevices = DB::table('nm_postal_services')->select('ps_id', 'ps_code')->get();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $productcategory = Products::get_product_category();
        $productmerchant = Products::get_product_merchant();

        return view('siteadmin.plan_timer')
        ->with('get_cur', $get_cur)
        ->with('list_postal_sevices', $list_postal_sevices)
        ->with('get_products', $get_products)
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('action', 'save')
        ->with('category', $category)
        ->with('merchant_details', $merchant_details)
        ->with('productcategory', $productcategory)
        ->with('productmerchant', $productmerchant);

    } else {
        return Redirect::to('siteadmin');
    }
  }

  public function add_plan_timer_submit()
  {
    if (Session::has('userid')) {
        $inputs     = Input::all();
        //dd($inputs);
        $all_product = 'off';
        $all_product_value = 2;
        if (!empty($inputs['all_products'])) {
          $all_product = $inputs['all_products'];
          $all_product_value = 1;
        }
        //dd($inputs);
        $voucher_limit = $inputs['voucher_limit'];
        $customer_voucher_limit = $inputs['customer_voucher_limit'];
        $simple_action = $inputs['simple_action'];
        $title      = $inputs['title'];
        $voucher_code = $inputs['voucher_code'];
        $startdate  = $inputs['startdate'];
        $enddate    = $inputs['enddate'];
        $opt_time   = $inputs['opt_time'];
        $minimal_transaction = $inputs['minimal_transaction'];
        $maximal_shipping = $inputs['maximal_shipping'];
        $jenis_voucher = $inputs['jenis_voucher'];
        $jumlah_voucher = $inputs['jml_voucher'];
        $kode_depan_voucher = $inputs['kode_depan_voucher'];
        $start_time = $inputs['start_time'];
        $end_time = $inputs['end_time'];

        $picture = Input::file('picture');
        if ($picture != '') {
            $filesize = filesize($picture);
            $imagesize = getimagesize($picture);
            $max_width = 200;// in pixels
            $max_height = 200;// in pixels
            $max_size = 2097152; //In bytes
            if ($filesize > $max_size) {
                return Redirect::to('add_plan_timer')->with('error', 'File Tidak Boleh Lebih dari 2 MB');
            }
            if ($imagesize[0] > $max_width || $imagesize[1] > $max_height) {
                return Redirect::to('add_plan_timer')->with('error', 'Dimensi File Harus 200 x 200');
            }
            $picture_name = $picture->getClientOriginalName();
            $path = './assets/promo';
            $explode_picture_name = explode('.', $picture_name);
            $picture_name_fix = $explode_picture_name[0].str_random(8).'.'.$explode_picture_name[1];
            $ekstensi = strtolower($explode_picture_name[1]);
            if ($ekstensi != 'jpg' && $ekstensi != 'jpeg' && $ekstensi != 'png' && $ekstensi != 'bmp') {
                echo "Ekstensi tidak didukung. <a href='".url('add_plan_timer')."'>Back</a>";
                die();
            }
        }

        if ($opt_time == 2) {
          $rec_week_every = implode(",", $inputs['rec_week_every']);
      }elseif($opt_time == 3){
          $rec_week_every = implode(",", $inputs['rec_month_every']);
          $start_time = 0;
          $end_time = 0;
      }else{
          $rec_week_every = null;
          $start_time = 0;
          $end_time = 0;
      }
        if($simple_action=='free_shipping'){
            $ps_id = $inputs['ps_id'];
            $entry_promo  = [
                'schedp_simple_free_shipping' => 1,
                'schedp_title' => $title,
                'schedp_start_date' => $startdate,
                'schedp_end_date' => $enddate,
                'schedp_RecurrenceType' => $opt_time,
                'schedp_Int2' => $rec_week_every,
                'schedp_ps_id' => $ps_id,
                'schedp_simple_action' => $simple_action,
                'maximum_free_shipping' => $maximal_shipping,
                'jenis_voucher' => $jenis_voucher,
                'jumlah_voucher' => $jumlah_voucher,
                'kode_depan_voucher' => $kode_depan_voucher,
                'schedp_recurr_start_time' => $start_time,
                'schedp_recurr_end_time' => $end_time,
                'schedp_picture' => $picture_name_fix
                // 'code_promo' => $code_promo,
            ];
        }elseif ($simple_action=='by_fixed') {
            $discounted_price = $inputs['discounted_price'];
            $discount = $discounted_price;
            $promop_saving_price = $discounted_price;
            $entry_promo  = [
                'schedp_title' => $title,
                'schedp_start_date' => $startdate,
                'schedp_end_date' => $enddate,
                'schedp_RecurrenceType' => $opt_time,
                'schedp_Int2' => $rec_week_every,
                'schedp_simple_action' => $simple_action,
                'schedp_discount_amount' => $discount,
                'schedp_coupon_type' => $all_product_value,
                'jenis_voucher' => $jenis_voucher,
                'jumlah_voucher' => $jumlah_voucher,
                'kode_depan_voucher' => $kode_depan_voucher,
                'schedp_recurr_start_time' => $start_time,
                'schedp_recurr_end_time' => $end_time,
                'schedp_picture' => $picture_name_fix
                // 'code_promo' => $code_promo,
              ];
        }elseif ($simple_action=='by_percent') {
            $discounted_percentage = $inputs['discounted_percentage'];
            $discount = $discounted_percentage;
            $entry_promo  = [
                'schedp_title' => $title,
                'schedp_start_date' => $startdate,
                'schedp_end_date' => $enddate,
                'schedp_RecurrenceType' => $opt_time,
                'schedp_Int2' => $rec_week_every,
                'schedp_simple_action' => $simple_action,
                'schedp_coupon_type' => $all_product_value,
                'schedp_discount_amount' => $discount,
                'jenis_voucher' => $jenis_voucher,
                'jumlah_voucher' => $jumlah_voucher,
                'kode_depan_voucher' => $kode_depan_voucher,
                'schedp_recurr_start_time' => $start_time,
                'schedp_recurr_end_time' => $end_time,
                'schedp_picture' => $picture_name_fix
                // 'code_promo' => $code_promo,
             ];
        }elseif ($simple_action=='buy_x_get_y') {
            $nilai_x = $inputs['nilai_x'];
            $nilai_y = $inputs['nilai_y'];
            $entry_promo  = [
                'schedp_title' => $title,
                'schedp_start_date' => $startdate,
                'schedp_end_date' => $enddate,
                'schedp_RecurrenceType' => $opt_time,
                'schedp_Int2' => $rec_week_every,
                'schedp_simple_action' => $simple_action,
                'schedp_coupon_type' => $all_product_value,
                'schedp_x' => $nilai_x,
                'schedp_y' => $nilai_y,
                'jenis_voucher' => $jenis_voucher,
                'jumlah_voucher' => $jumlah_voucher,
                'kode_depan_voucher' => $kode_depan_voucher,
                'schedp_recurr_start_time' => $start_time,
                'schedp_recurr_end_time' => $end_time,
                'schedp_picture' => $picture_name_fix
                // 'code_promo' => $code_promo,
             ];
        }elseif ($simple_action=='flash_promo') {
            $discounted_price = $inputs['discounted_price'];
            //dd($inputs);
            $discount = $discounted_price;

            //dd($promop_saving_price);
            $entry_promo  = [
                'schedp_title' => $title,
                'schedp_start_date' => $startdate,
                'schedp_end_date' => $enddate,
                'schedp_RecurrenceType' => $opt_time,
                // 'schedp_Int1' => $rec_time,
                'schedp_Int2' => $rec_week_every,
                'schedp_simple_action' => $simple_action,
                'schedp_discount_amount' => $discount,
                'schedp_coupon_type' => $all_product_value
                // 'code_promo' => $code_promo,
              ];
        }
        // $simple_free_shipping = $inputs['free_shipping'];
        if ($picture != '') {
            $upload_file = Input::file('picture')->move($path, $picture_name_fix);
        }
        $return = PlanTimer::save_scheduled_promo($entry_promo);

        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');
        $aud_detail1 = json_encode($entry_promo);
        $audit_trail = new AuditTrail;
        $audit_trail->aud_table = 'nm_scheduled_promo';
        $audit_trail->aud_action = 'create';
        $audit_trail->aud_detail = $aud_detail1;
        $audit_trail->aud_date = $now;
        $audit_trail->aud_user_name = Session::get('username');
        $audit_trail->save();

        $get_schedp_id = PlanTimer::get_last_schedule();

        //  dd($get_schedp_id);
        if($jenis_voucher == "statis")
        {
          $entry = [
              'promoc_schedp_id' => $get_schedp_id->schedp_id,
              'promoc_voucher_code' => $voucher_code,
              'promoc_minimal_transaction' => $minimal_transaction,
              'promoc_usage_limit' => $voucher_limit,
              'promoc_usage_per_customer' => $customer_voucher_limit
          ];

          $return_coupons = PlanTimer::save_promo_coupons($entry);

          $aud_detail2 = json_encode($entry);
          $audit_trail = new AuditTrail;
          $audit_trail->aud_table = 'nm_promo_coupons';
          $audit_trail->aud_action = 'create';
          $audit_trail->aud_detail = $aud_detail2;
          $audit_trail->aud_date = $now;
          $audit_trail->aud_user_name = Session::get('username');
          $audit_trail->save();
        }
        else if($jenis_voucher == "generate")
        {
          for($j = 0; $j < $jumlah_voucher; $j++)
          {
              $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
              $res = "";

              for ($i = 0; $i < 6; $i++) {
                  $res .= $chars[mt_rand(0, strlen($chars)-1)];
              }

              $entry = [
                  'promoc_schedp_id' => $get_schedp_id->schedp_id,
                  'promoc_voucher_code' => $kode_depan_voucher.$res,
                  'promoc_minimal_transaction' => $minimal_transaction,
                  'promoc_usage_limit' => 1,
                  'status_coupons' => 'belum dipakai'
              ];

              $return_coupons = PlanTimer::save_promo_coupons($entry);

              $aud_detail2 = json_encode($entry);
              $audit_trail = new AuditTrail;
              $audit_trail->aud_table = 'nm_promo_coupons';
              $audit_trail->aud_action = 'create';
              $audit_trail->aud_detail = $aud_detail2;
              $audit_trail->aud_date = $now;
              $audit_trail->aud_user_name = Session::get('username');
              $audit_trail->save();
          }
        }


        if($simple_action!='free_shipping')
        {
          if ($all_product == 'on') {
            $product = $inputs['product'];
          }else {
            $product = $inputs['products'];
          }

            $i = 0;
            // dd($inputs);
            foreach ($product as $value) {

              $prod_value[] = Products::get_product_by_id($value);
              //dd($prod_value);
            //   $promop_schedp_id = PlanTimer::get_promo_id($code_promo);
                //dd($prod_value);
              $pro_id = $prod_value[$i]->pro_id;
              $pro_mr_id = $prod_value[$i]->pro_mr_id;
              $pro_sh_id = $prod_value[$i]->pro_sh_id;
              $pro_status = $prod_value[$i]->pro_status;
              $pro_price = $prod_value[$i]->pro_price;
              $pro_original_disprice = $prod_value[$i]->pro_disprice;
              $promop_qty = 0;
              $stock = $prod_value[$i]->pro_qty;

              if($simple_action=='buy_x_get_y'){
                  $entry_product  = [
                              'promop_schedp_id' => $get_schedp_id->schedp_id,
                              'promop_merchant_id' => $pro_mr_id,
                              'promop_shop_id' => $pro_sh_id,
                              'promop_pro_id' => $pro_id,
                              'promop_status' => $pro_status,
                              'promop_original_price' => $pro_price,
                              'promop_original_disprice' => $pro_original_disprice,
                              'promop_discount_price' => 0,
                              'promop_discount_percentage' => 0,
                              'promop_saving_price' => 0,
                            ];
              }else{
                  if($simple_action=='by_percent'){
                        if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                            // dd('test1');
                            $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_price;
                            $pro_disprice = $pro_price-$promop_saving_price;
                        }
                        else{
                            // dd('test2');
                            $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_disprice;
                            $pro_disprice = $pro_original_disprice-$promop_saving_price;
                            // dd($promop_saving_price);
                        }

                  }elseif ($simple_action=='by_fixed') {
                      if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                          $pro_disprice = $pro_price - $promop_saving_price;
                          $discounted_percentage = ($promop_saving_price/$pro_price)*100;
                      }else {
                          $pro_disprice = $pro_original_disprice - $promop_saving_price;
                          $discounted_percentage = ($promop_saving_price/$pro_original_disprice)*100;
                      }

                  }elseif ($simple_action=='flash_promo') {
                        $promop_saving_price = $inputs['promoPrice'][$i];
                        $promop_qty = $inputs['qty'][$i];
                      if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                          $stock = $prod_value[$i]->pro_qty;
                          $pro_disprice = $pro_price - $promop_saving_price;
                          $discounted_percentage = ($promop_saving_price/$pro_price)*100;
                      }else {
                          $promop_qty = $inputs['qty'][$i];
                          $stock = $prod_value[$i]->pro_qty;
                          $pro_disprice = $pro_original_disprice - $promop_saving_price;
                          $discounted_percentage = ($promop_saving_price/$pro_original_disprice)*100;
                  }

                  }
                  $entry_product  = [
                              'promop_schedp_id' => $get_schedp_id->schedp_id,
                              'promop_merchant_id' => $pro_mr_id,
                              'promop_shop_id' => $pro_sh_id,
                              'promop_pro_id' => $pro_id,
                              'promop_status' => $pro_status,
                              'promop_original_price' => $pro_price,
                              'promop_original_disprice' => $pro_original_disprice,
                              'promop_discount_price' => $pro_disprice,
                              'promop_discount_percentage' => $discounted_percentage,
                              'promop_saving_price' => $promop_saving_price,
                              'promop_qty' => $promop_qty,
                              'stock' => $stock
                            ];
              }

              $return_pro = PlanTimer::save_promo_products($entry_product);

              $aud_detail3 = json_encode($entry_product);
              $audit_trail = new AuditTrail;
              $audit_trail->aud_table = 'nm_promo_products';
              $audit_trail->aud_action = 'create';
              $audit_trail->aud_detail = $aud_detail3;
              $audit_trail->aud_date = $now;
              $audit_trail->aud_user_name = Session::get('username');
              $audit_trail->save();

              $i++;
          }
        }
        return Redirect::to('manage_schedule');
      }
    }

    public static function manage_schedule(){

      if (Session::has('userid')) {
          $date      = date('Y-m-d H:i:s');
          $get_time_schedule = PlanTimer::get_schedule();
          $get_merchant_time_schedule = PlanTimer::get_schedule_merchant();
          //dd($get_time_schedule);
          //dd($get_time_schedule);
          $from_date = Input::get('from_date');
          if($from_date != ''){
              $from_date   = strtotime($from_date);
              $from_date   = date('Y-m-d H:i:s', $from_date);
          }
          $to_date   = Input::get('to_date');
          if ($to_date != '') {
              $to_date     = strtotime($to_date);
              $to_date     = strtotime('+1 day', $to_date);
              $to_date     = date('Y-m-d H:i:s', $to_date);
          }
          $dealrep   = Deals::get_dealreports($from_date, $to_date);
          // dd($dealrep);

          $include = self::view_include('deals', 'admin_left_menu_deals');
          $adminheader = $include['adminheader'];
          $adminleftmenus = $include['adminleftmenus'];
          $adminfooter 	= $include['adminfooter'];
          $delete_deals 	= Deals::get_order_deals_details();
          $return         = Deals::get_deal_details($date);

          return view('siteadmin.manage_schedule')
          ->with('get_time_schedule', $get_time_schedule)
          ->with('get_merchant_time_schedule', $get_merchant_time_schedule)
          ->with('adminheader', $adminheader)
          ->with('adminleftmenus', $adminleftmenus)
          ->with('adminfooter', $adminfooter)
          ->with('deal_details', $return)
          ->with('dealrep', $dealrep)
          ->with('delete_deals', $delete_deals);

      } else {
          return Redirect::to('siteadmin');
      }
    }

    public static function manage_coupons(){

      if (Session::has('userid')) {
          $date      = date('Y-m-d H:i:s');
          $get_coupons_h = PlanTimer::get_coupons_h();
          //dd($get_coupons_h);
          $from_date = Input::get('from_date');
          $to_date   = Input::get('to_date');
          $dealrep   = Deals::get_dealreports($from_date, $to_date);
          // dd($dealrep);

          $include = self::view_include('deals', 'admin_left_menu_deals');
          $adminheader 	= $include['adminheader'];
          $adminleftmenus = $include['adminleftmenus'];
          $adminfooter 	= $include['adminfooter'];
          $delete_deals   = Deals::get_order_deals_details();
          $return         = Deals::get_deal_details($date);

          return view('siteadmin.manage_coupons')
          ->with('get_coupons_h', $get_coupons_h)
          ->with('adminheader', $adminheader)
          ->with('adminleftmenus', $adminleftmenus)
          ->with('adminfooter', $adminfooter)
          ->with('deal_details', $return)
          ->with('dealrep', $dealrep)
          ->with('delete_deals', $delete_deals);

      } else {
          return Redirect::to('siteadmin');
      }
    }

    public function edit_plan_timer($id)
    {
        if (Session::has('userid')) {

            $include = self::view_include('deals', 'admin_left_menu_deals');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $get_schedule     = PlanTimer::get_schedule_by_id($id);
            // dd($get_schedule);
            $get_promo_product = PlanTimer::get_promo_by_id($get_schedule[0]->schedp_id);
             //dd($get_promo_product);
            $get_products_all   = Products::get_promo_product();
            // dd($get_products_all);
            $get_promo_coupons = PlanTimer::get_promo_coupons($id);
            //dd($get_promo_coupons);
            $list_postal_services = DB::table('nm_postal_services')->select('ps_id', 'ps_code')->get();
            $get_pay                      = Settings::get_pay_settings();
            $get_cur                      = $get_pay[0]->ps_cursymbol;
            $productcategory = Products::get_product_category();
              foreach ($get_promo_product as $value) {
                  $get_products[] = Products::get_promo_product_by_id($value->promop_pro_id);
              }
              //dd($get_products);
              if($get_promo_product == null){
                  $get_products = '';
              }
            //   dd($get_products);
            //   dd($get_promo_product);
            $productmerchant = Products::get_product_merchant();
            return view('siteadmin.edit_plan_timer')
            ->with('get_cur', $get_cur)
            ->with('id', $id)
            ->with('list_postal_services', $list_postal_services)
            ->with('get_promo_coupons', $get_promo_coupons)
            ->with('get_products_all', $get_products_all)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('action', 'update')
            ->with('get_schedules', $get_schedule)
            ->with('get_products', $get_products)
            ->with('get_promo_product', $get_promo_product)
            ->with('productmerchant', $productmerchant)
            ->with('productcategory', $productcategory)
            ->with('get_promo_product', $get_promo_product);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_plan_timer_submit(){
        if (Session::has('userid')) {
            $inputs     = Input::all();
            // dd($inputs);
            $all_product = 'off';
            $all_product_value = 2;
            // dd($inputs['all_products']);
            if (!empty($inputs['all_products'])) {
              $all_product = $inputs['all_products'];
              $all_product_value = 1;
            }
            //dd($inputs);
            $voucher_limit = $inputs['voucher_limit'];
            $customer_voucher_limit = $inputs['customer_voucher_limit'];
            $simple_action = $inputs['simple_action'];
            $title      = $inputs['title'];
            $voucher_code = $inputs['voucher_code'];
            $startdate  = $inputs['startdate'];
            $enddate    = $inputs['enddate'];
            $opt_time   = $inputs['opt_time'];
            $maximal_shipping = $inputs['maximum_shipping'];
            $start_time = $inputs['start_time'];
            $end_time = $inputs['end_time'];
            // dd(implode(",", $inputs['rec_week_every']));
            if ($opt_time == 2) {
              $rec_week_every = implode(",", $inputs['rec_week_every']);
          }elseif($opt_time==3){
              $rec_week_every = implode(",", $inputs['rec_month_every']);
              $start_time = 0;
              $end_time = 0;
          }else {
              $rec_week_every = null;
              $start_time = 0;
              $end_time = 0;
          }
            // dd($rec_week_every);
            if($simple_action=='free_shipping'){
                $ps_id = $inputs['ps_id'];
                $entry_promo  = [
                    'schedp_simple_free_shipping' => 1,
                    'schedp_title' => $title,
                    'schedp_start_date' => $startdate,
                    'schedp_end_date' => $enddate,
                    'schedp_RecurrenceType' => $opt_time,
                    'schedp_Int2' => $rec_week_every,
                    'schedp_ps_id' => $ps_id,
                    'schedp_simple_action' => $simple_action,
                    'maximum_free_shipping' => $maximal_shipping,
                    'schedp_recurr_start_time' => $start_time,
                    'schedp_recurr_end_time' => $end_time
                    // 'code_promo' => $code_promo,
                ];
            }elseif ($simple_action=='by_fixed') {
                $discounted_price = $inputs['discounted_price'];
                $discount = $discounted_price;
                $pro_disprice = $discounted_price;
                $entry_promo  = [
                    'schedp_title' => $title,
                    'schedp_start_date' => $startdate,
                    'schedp_end_date' => $enddate,
                    'schedp_RecurrenceType' => $opt_time,
                    'schedp_Int2' => $rec_week_every,
                    'schedp_simple_action' => $simple_action,
                    'schedp_coupon_type' => $all_product_value,
                    'schedp_discount_amount' => $discount,
                    'schedp_recurr_start_time' => $start_time,
                    'schedp_recurr_end_time' => $end_time
                    // 'code_promo' => $code_promo,
                  ];
            }elseif ($simple_action=='by_percent') {
                $discounted_percentage = $inputs['discounted_percentage'];
                $discount = $discounted_percentage;
                $entry_promo  = [
                    'schedp_title' => $title,
                    'schedp_start_date' => $startdate,
                    'schedp_end_date' => $enddate,
                    'schedp_RecurrenceType' => $opt_time,
                    'schedp_Int2' => $rec_week_every,
                    'schedp_simple_action' => $simple_action,
                    'schedp_coupon_type' => $all_product_value,
                    'schedp_discount_amount' => $discount,
                    'schedp_recurr_start_time' => $start_time,
                    'schedp_recurr_end_time' => $end_time
                    // 'code_promo' => $code_promo,
                 ];
            }elseif ($simple_action=='buy_x_get_y') {
                $nilai_x = $inputs['nilai_x'];
                $nilai_y = $inputs['nilai_y'];
                $entry_promo  = [
                    'schedp_title' => $title,
                    'schedp_start_date' => $startdate,
                    'schedp_end_date' => $enddate,
                    'schedp_RecurrenceType' => $opt_time,
                    'schedp_Int2' => $rec_week_every,
                    'schedp_simple_action' => $simple_action,
                    'schedp_coupon_type' => $all_product_value,
                    'schedp_x' => $nilai_x,
                    'schedp_y' => $nilai_y,
                    'schedp_recurr_start_time' => $start_time,
                    'schedp_recurr_end_time' => $end_time
                    // 'code_promo' => $code_promo,
                 ];
            }elseif ($simple_action=='flash_promo') {
                // $nilai_x = $inputs['nilai_x'];
                // $nilai_y = $inputs['nilai_y'];
                $entry_promo  = [
                    'schedp_title' => $title,
                    'schedp_start_date' => $startdate,
                    'schedp_end_date' => $enddate,
                    'schedp_RecurrenceType' => $opt_time,
                    // 'schedp_Int1' => $rec_time,
                    'schedp_Int2' => $rec_week_every,
                    'schedp_simple_action' => $simple_action,
                    'schedp_coupon_type' => $all_product_value,
                    // 'code_promo' => $code_promo,
                 ];
            }

            $picture = Input::file('picture');
            if ($picture != '') {
                $filesize = filesize($picture);
                $imagesize = getimagesize($picture);
                $max_width = 200;// in pixels
                $max_height = 200;// in pixels
                $max_size = 2097152; //In bytes
                if ($filesize > $max_size) {
                    return Redirect::to('edit_plan_timer/'.$inputs['id'])->with('error', 'File Tidak Boleh Lebih dari 2 MB');
                }
                if ($imagesize[0] > $max_width || $imagesize[1] > $max_height) {
                    return Redirect::to('edit_plan_timer/'.$inputs['id'])->with('error', 'Dimensi File Harus 200 x 200');
                }
                $picture_name = $picture->getClientOriginalName();
                $path = './assets/promo';
                $explode_picture_name = explode('.', $picture_name);
                $picture_name_fix = $explode_picture_name[0].str_random(8).'.'.$explode_picture_name[1];
                $ekstensi = strtolower($explode_picture_name[1]);
                if ($ekstensi != 'jpg' && $ekstensi != 'jpeg' && $ekstensi != 'png' && $ekstensi != 'bmp') {
                    echo "Ekstensi tidak didukung. <a href='".url('edit_plan_timer')."'>Back</a>";
                    die();
                }
                $entry_promo['schedp_picture'] = $picture_name_fix;
                $upload_file = $picture->move($path, $picture_name_fix);
            }
            // dd($inputs['voucher_code']);
            // $get_schedule_id = DB::table('nm_promo_coupons')->where('promoc_voucher_code', $inputs['voucher_code'])->first();
            // dd($get_schedule_id);
            $edit = PlanTimer::edit_schedule_promo($entry_promo, $inputs['id']);

            date_default_timezone_set('Asia/Jakarta');
            $now = date('Y-m-d H:i:s');

            $entry_promo_json = $entry_promo;
            $entry_promo_json['id'] = $inputs['id'];
            $aud_detail = json_encode($entry_promo_json);
            $audit_trail = new AuditTrail;
            $audit_trail->aud_table = 'nm_scheduled_promo';
            $audit_trail->aud_action = 'update';
            $audit_trail->aud_detail = $aud_detail;
            $audit_trail->aud_date = $now;
            $audit_trail->aud_user_name = Session::get('username');
            $audit_trail->save();
            //PlanTimer::delete_promo_product_by_id($inputs['id']);

            $entry = [
                'promoc_schedp_id' => $inputs['id'],
                'promoc_voucher_code' => $voucher_code,
                'promoc_usage_limit' => $voucher_limit,
                'promoc_usage_per_customer' => $customer_voucher_limit
            ];
            $return_coupons = PlanTimer::edit_promo_coupons($inputs['id'], $entry);

            $entry_json = $entry;
            $aud_detail = json_encode($entry_json);
            $audit_trail = new AuditTrail;
            $audit_trail->aud_table = 'nm_promo_coupons';
            $audit_trail->aud_action = 'update';
            $audit_trail->aud_detail = $aud_detail;
            $audit_trail->aud_date = $now;
            $audit_trail->aud_user_name = Session::get('username');
            $audit_trail->save();

            if($simple_action!='free_shipping')
            {
              if ($all_product == 'on') {
                $product = $inputs['product'];
              }else {
                $product = $inputs['product'];
              }

                $i = 0;
                // dd($product);
                // foreach ($product as $value) {
                //   $prod_value[] = Products::get_promo_product_by_id($value);
                // }


                //dd($prod_value);
                PlanTimer::delete_promo_product_by_id($inputs['id']);
                foreach ($product as $value) {

                  $prod_value[] = Products::get_product_by_id($value);
                //  var_dump($prod_value[$i]->promop_pro_id);
                //   $promop_schedp_id = PlanTimer::get_promo_id($code_promo);

//dd($prod_value);

                  //dd($prod_value);
                  $pro_id = $prod_value[$i]->pro_id;

                  $pro_mr_id = $prod_value[$i]->pro_mr_id;
                  //dd($pro_mr_id);
                  $pro_sh_id = $prod_value[$i]->pro_sh_id;

                  $pro_status = $prod_value[$i]->pro_status;
                    //dd($pro_status);
                  $pro_price = $prod_value[$i]->pro_price;
                  $pro_original_disprice = $prod_value[$i]->pro_disprice;
                //   $pro_disprice = $prod_value[$i]->promop_discount_price;
                //   $pro_discount_percentage = $prod_value[$i]->promop_discount_percentage;
                //   $pro_saving_price = $prod_value[$i]->promop_saving_price;
                //   $pro_qty = $prod_value[$i]->promop_qty;
                //   $pro_stock = $prod_value[$i]->pro_qty;
                //  if(!empty())
                  if($simple_action=='buy_x_get_y'){
                      $entry_product  = [
                                  'promop_schedp_id' => $inputs['id'],
                                  'promop_merchant_id' => $pro_mr_id,
                                  'promop_shop_id' => $pro_sh_id,
                                  'promop_pro_id' => $pro_id,
                                  'promop_status' => $pro_status,
                                  'promop_original_price' => $pro_price,
                                  'promop_original_disprice' => $pro_original_disprice,
                                  'promop_discount_price' => 0,
                                  'promop_discount_percentage' => 0,
                                  'promop_saving_price' => 0,
                                ];
                  }else{
                      if($simple_action=='by_percent'){
                            if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                                // dd('test1');
                                $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_price;
                                $pro_disprice = $pro_price-$promop_saving_price;
                            }
                            else{
                                // dd('test2');
                                $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_disprice;
                                $pro_disprice = $pro_original_disprice-$promop_saving_price;
                                // dd($promop_saving_price);
                            }

                      }elseif ($simple_action=='by_fixed') {
                          $promop_saving_price = $inputs['discounted_price'];
                          if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                              $pro_disprice = $pro_price - $promop_saving_price;
                              $discounted_percentage = ($promop_saving_price/$pro_price)*100;
                          }else {
                              $pro_disprice = $pro_original_disprice - $promop_saving_price;
                              $discounted_percentage = ($promop_saving_price/$pro_original_disprice)*100;
                          }

                      }elseif($simple_action == 'flash_promo'){
                          $discount = "";
                          $promop_saving_price = $inputs['promoPrice'][$i];
                          $promop_qty = $inputs['qty'][$i];
                        if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                            $stock = $prod_value[$i]->pro_qty;
                            $pro_disprice = $pro_price - $promop_saving_price;
                            $discounted_percentage = ($promop_saving_price/$pro_price)*100;
                        }else {
                            $promop_qty = $inputs['qty'][$i];
                            $stock = $prod_value[$i]->pro_qty;
                            $pro_disprice = $pro_original_disprice - $promop_saving_price;
                            $discounted_percentage = ($promop_saving_price/$pro_original_disprice)*100;
                        }
                            // $promop_saving_price = $discounted_price;
                            //dd($promop_saving_price);
                            $entry_promo  = [
                                'schedp_title' => $title,
                                'schedp_start_date' => $startdate,
                                'schedp_end_date' => $enddate,
                                //'schedp_simple_action' => $simple_action,
                                'schedp_RecurrenceType' => $opt_time,
                                //'schedp_Int1' => $rec_time,
                                'schedp_Int2' => $rec_week_every,
                                'schedp_simple_action' => $simple_action,
                                'schedp_discount_amount' => $discount,
                                'schedp_coupon_type' => $all_product_value
                                // 'code_promo' => $code_promo,
                            ];
                      }
                    //   dd($get_schedp_id);
                    if($simple_action == 'flash_promo'){
                      $entry_product  = [
                                  'promop_schedp_id' => $inputs['id'],
                                  'promop_merchant_id' => $pro_mr_id,
                                  'promop_shop_id' => $pro_sh_id,
                                  'promop_pro_id' => $pro_id,
                                  'promop_status' => $pro_status,
                                  'promop_original_price' => $pro_price,
                                  'promop_original_disprice' => $pro_original_disprice,
                                  'promop_discount_price' => $pro_disprice,
                                  'promop_discount_percentage' => $discounted_percentage,
                                  'promop_saving_price' => $promop_saving_price,
                                  'promop_qty' => $promop_qty,
                                  'stock' => $stock,
                                ];
                                //dd($entry_product);
                    }else {
                        $entry_product  = [
                            'promop_schedp_id' => $inputs['id'],
                            'promop_merchant_id' => $pro_mr_id,
                            'promop_shop_id' => $pro_sh_id,
                            'promop_pro_id' => $pro_id,
                            'promop_status' => $pro_status,
                            'promop_original_price' => $pro_price,
                            'promop_original_disprice' => $pro_original_disprice,
                            'promop_discount_price' => $pro_disprice,
                            'promop_discount_percentage' => $discounted_percentage,
                            'promop_saving_price' => $promop_saving_price,
                      ];
                    }
                  }
                  //dd($inputs['id']);

                  $return_pro = PlanTimer::save_promo_products($entry_product);

                  $entry_product_json = $entry_product;
                  $aud_detail = json_encode($entry_product_json);
                  $audit_trail = new AuditTrail;
                  $audit_trail->aud_table = 'nm_promo_products';
                  $audit_trail->aud_action = 'update';
                  $audit_trail->aud_detail = $aud_detail;
                  $audit_trail->aud_date = $now;
                  $audit_trail->aud_user_name = Session::get('username');
                  $audit_trail->save();

                  //dd($return_pro);
                  $i++;
              }
            }
            return Redirect::to(Input::get('url_redirect'));
        }
    }

    public function delete_schedule($id)
  	{

  		if(Session::has('userid'))
  		{
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

  		 $del_deals = PlanTimer::delete_schedule($id);

         date_default_timezone_set('Asia/Jakarta');
         $now = date('Y-m-d H:i:s');
         $audit_trail = new AuditTrail;
         $audit_trail->aud_table = 'nm_scheduled_promo';
         $audit_trail->aud_action = 'delete';
         $audit_trail->aud_detail = '{"schedp_id":"'.$id.'"}';
         $audit_trail->aud_date = $now;
         $audit_trail->aud_user_name = Session::get('username');
         $audit_trail->save();

         $audit_trail = new AuditTrail;
         $audit_trail->aud_table = 'nm_promo_coupons';
         $audit_trail->aud_action = 'delete';
         $audit_trail->aud_detail = '{"promoc_schedp_id":"'.$id.'"}';
         $audit_trail->aud_date = $now;
         $audit_trail->aud_user_name = Session::get('username');
         $audit_trail->save();

         $audit_trail = new AuditTrail;
         $audit_trail->aud_table = 'nm_promo_products';
         $audit_trail->aud_action = 'delete';
         $audit_trail->aud_detail = '{"promop_schedp_id":"'.$id.'"}';
         $audit_trail->aud_date = $now;
         $audit_trail->aud_user_name = Session::get('username');
         $audit_trail->save();

  		return Redirect::to(redirect()->back()->getTargetUrl())->with('Delete Schedule','Schedule Deleted Successfully');
  		}
  		else
          {
          return Redirect::to('siteadmin');
          }
  	}

    public function schedule_details($id)
     {
         if (Session::has('userid')) {
             $include = self::view_include('deals', 'admin_left_menu_deals');
             $adminheader 	= $include['adminheader'];
             $adminleftmenus = $include['adminleftmenus'];
             $adminfooter 	= $include['adminfooter'];
           $get_schedule     = PlanTimer::get_schedule_by_id($id);
        //    dd($get_schedule);
           $get_promo_product = PlanTimer::get_promo_by_id($get_schedule[0]->schedp_id);
        //    dd($get_promo_product);
           $get_products_all   = Products::get_all();
        //    dd($get_products_all);
            $get_promo_coupons = PlanTimer::get_promo_coupons($id);
            $get_pay                      = Settings::get_pay_settings();
            $get_cur                      = $get_pay[0]->ps_cursymbol;
            if($get_schedule[0]->schedp_ps_id == 0)
            {
                $get_postal_services = '';
            }else {
                $get_postal_services = DB::table('nm_postal_services')->where('ps_id', $get_schedule[0]->schedp_ps_id)->first();
            }

            // dd($get_postal_services);
             foreach ($get_promo_product as $value) {
                 $get_products[] = Products::get_product_by_id($value->promop_pro_id);
             }
             if($get_promo_product == null){
                 $get_products = '';
             }



           return view('siteadmin.schedule_details')->with('get_cur', $get_cur)->with('get_postal_services', $get_postal_services)->with('get_promo_coupons', $get_promo_coupons)->with('get_products_all', $get_products_all)->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'update')->with('get_schedules', $get_schedule)->with('get_products', $get_products)->with('get_promo_product', $get_promo_product);

         } else {
             return Redirect::to('siteadmin');
         }
     }

    public function report_promo()
    {
        $include = self::view_include('deals', 'admin_left_menu_deals');
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        return view('siteadmin.report_promo')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter);
    }

    public function report_promo_download(Request $request)
    {
        $from_date  = Input::get('from_date');
        if ($from_date != '') {
            $from_date = strtotime($from_date);
            $from_date_text = date('d F Y', $from_date);
            $from_date = date('Y-m-d 00:00:00', $from_date);
        }else {
            $from_date_text = 'Awal';
        }
        $to_date    = Input::get('to_date');
        if ($to_date != '') {
            $to_date = strtotime($to_date);
            $to_date = strtotime('+1 day', $to_date);
            $to_date_text = date('d F Y', $to_date);
            $to_date = date('Y-m-d 23:59:59', $to_date);
        }else {
            date_default_timezone_set('Asia/Jakarta');
            $to_date_text = date('d F Y');
        }

        $promo = DB::table('nm_order')
        ->leftJoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
        ->leftJoin('nm_promo_coupons', 'nm_promo_coupons.promoc_id', '=', 'nm_transaksi.kupon_id')
        ->where('nm_transaksi.kupon_id', '!=', 0);
        if ($from_date != '' && $from_date != null) {
            $promo = $promo->where('nm_order.order_date', '>=', $from_date);
        }
        if ($to_date != '' && $to_date != null) {
            $promo = $promo->where('nm_order.order_date', '<=', $to_date);
        }
        $promo = $promo->groupBy('nm_transaksi.transaction_id')->get();

        Excel::create('Report Promo', function($excel) use($promo, $from_date_text, $to_date_text){
            // Set the title
            $excel->setTitle('Report Promo');

            // Chain the setters
            $excel->setCreator('Kukuruyuk')->setCompany('Kukuruyuk');

            // Call them separately
            $excel->setDescription('Report Promo');

            $excel->sheet('Report Promo', function($sheet) use($promo, $from_date_text, $to_date_text){
                date_default_timezone_set('Asia/Jakarta');
                $now = date('Y-m-d H:i:s');
                $sheet->mergeCells('A4:E4');
                $sheet->cells('A6:E6', function($cells){
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thick', 'thick', 'thick', 'thick');
                });
                $sheet->row(1, array(
                    'Tanggal:',
                    $now
                ));
                $sheet->row(2, array(
                    'Oleh:',
                    Session::get('username')
                ));
                $sheet->row(4, array(
                    'Report promo dari '.$from_date_text.' s/d '.$to_date_text
                ));
                $sheet->row(6, array(
                    'No',
                    'Kode Voucher',
                    'Nilai Nominal',
                    'Kuantiti',
                    'Total'
                ));
                $i = 7;
                foreach ($promo as $each_promo) {
                    $sheet->cells('A'.$i.':E'.$i, function($cells){
                        $cells->setFontWeight('bold');
                        $cells->setBorder('thick', 'thick', 'thick', 'thick');
                    });
                    $sheet->row($i, array(
                        $i-6,
                        $each_promo->promoc_voucher_code,
                        $each_promo->diskon_voucher,
                        '1',
                        $each_promo->diskon_voucher
                    ));
                }
            });
        })->export('xlsx');
    }
}
?>
