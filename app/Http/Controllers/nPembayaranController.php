<?php

    namespace App\Http\Controllers;
    use DB;
    use Session;
    use App\Http\Models;
    use App\Register;
    use App\Home;
    use App\Footer;
    use App\Settings;
    use App\Merchant;
    use App\Popbox;
    use App\City;
    use MyPayPal;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Http\Request;

    class nPembayaranController extends Controller
    {
        public function generate_voucher(Request $request)
        {
            $jumlah_voucher = $request->input('aJumlahVoucher');
            $kode_depan = $request->input('aKodeDepan');

            for($j = 0; $j < $jumlah_voucher; $j++)
            {
                $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $res = "";

                for ($i = 0; $i < 6; $i++) {
                    $res .= $chars[mt_rand(0, strlen($chars)-1)];
                }

            }
        }

        public function get_location(Request $request)
        {
            $country = $request->input('aCountry');
            $city = $request->input('aCity');
            $getLocation = Popbox::getLocation($country, $city);
            return $getLocation;
        }

        public function get_address_location(Request $request)
        {
            $country = $request->input('aCountry');
            $city = $request->input('aCity');
            $location = $request->input('aLocation');
            $getLocation = Popbox::getLocation($country, '');
            // dd($getLocation);
            foreach ($getLocation['data'] as  $get) {
                if ($get['id'] == $location) {
                    $getAddress = $get;
                }
            }
            // $getAddress = $getLocation['data'][$location];
            // dd($getAddress);
            return $getAddress;
        }

        public function get_provincy_popbox(Request $request)
        {
            $data = ['token'=>'ff0256b4496f35b54c61558a7c5bb3be8267cbf7', 'country'=>'Indonesia'];

            $datajson = json_encode($data);

            $client = new \GuzzleHttp\client();
            $req = $client->request(
                'GET',
                'http://api-dev.popbox.asia',
                [
                    $datajson
                ]
                );

            return $req;
        }

        public function cek_kupon(Request $request)
        {
            // -- Deklarasi variable
            $customerid = $request->session()->get('customerid');
            $nomor_kupon = $_GET['nomor_kupon'];
            $total_belanja_awal = $_GET['total_belanja_awal'];
            $total_shipping = $_GET['harga_shipping'];
            if($total_shipping == "")
            {
                $total_shipping = floatval(0);
            }
            $total_belanja_akhir = 0;
            $total_diskon = 0;
            date_default_timezone_set('Asia/Jakarta');
            $today = date('Y-m-d H:i:s');//$_GET['today'];
            //dd($today);
            $postal_code = $_GET['postal_code'];
            // $total_pajak = $_GET['pajak'];
            $total_pajak = 0;

            $getKupon = City::getKupon($nomor_kupon);
            $get_customer_usage = DB::table("nm_promo_usage")
            ->where('promou_cus_id', $customerid)
            ->where('promou_promoc_id', $getKupon->promoc_id)
            ->first();
            //dd($getKupon);
            if($getKupon->schedp_end_date < $today || $getKupon->schedp_start_date > $today)
            {
                return response() -> json([
                    'error' => "Kupon Tidak Berlaku",
                ]);
            }
            if($getKupon->promoc_usage_per_customer!=0 || $getKupon->promoc_usage_per_customer!=null || $getKupon->promoc_usage_per_customer!=''){
                if($get_customer_usage!=null)
                {
                    if($get_customer_usage->promou_usage >= $getKupon->promoc_usage_per_customer)
                    {
                        return response() -> json([
                            'error' => 'Jumlah Pemakaian Kupon Telah Mencapai Batas Maksimal'
                        ]);
                    }
                }
            }
            if($getKupon->promoc_usage_limit!=0 || $getKupon->promoc_usage_limit!=null || $getKupon->promoc_usage_limit!=''){
                if($getKupon->promoc_times_used >= $getKupon->promoc_usage_limit){
                    return response() -> json([
                        'error' => 'Kupon Telah Habis'
                    ]);
                }
            }

            // Check Recurring
            if($getKupon->schedp_RecurrenceType==2){ // Recurring Hari
                $days = explode(",", $getKupon->schedp_Int2);
                $tanggal = date('Y-m-d');
                $status_hari = 0;
                foreach ($days as $day) {
                    if($day==0){
                        if($tanggal == date('Y-m-d', strtotime('Monday'))){
                            $status_hari = 1;
                        }
                    }
                    if($day==1){
                        if($tanggal == date('Y-m-d', strtotime('Tuesday'))){
                            $status_hari = 1;
                        }
                    }
                    if($day==2){
                        if($tanggal == date('Y-m-d', strtotime('Wednesday'))){
                            $status_hari = 1;
                        }
                    }
                    if($day==3){
                        if($tanggal == date('Y-m-d', strtotime('Thursday'))){
                            $status_hari = 1;
                        }
                    }
                    if($day==4){
                        if($tanggal == date('Y-m-d', strtotime('Friday'))){
                            $status_hari = 1;
                        }
                    }
                    if($day==5){
                        if($tanggal == date('Y-m-d', strtotime('Saturday'))){
                            $status_hari = 1;
                        }
                    }
                    if($day==6){
                        if($tanggal == date('Y-m-d', strtotime('Sunday'))){
                            $status_hari = 1;
                        }
                    }
                }
                if($status_hari!=1){
                    return response()->json([
                        'error' => 'Promo Tidak Berlaku Pada Saat Ini'
                    ]);
                }
                if($getKupon->schedp_recurr_start_time!=0 && $getKupon->schedp_recurr_end_time!=0)
                {
                    $time = date('H:i:s');
                    if($time<$getKupon->schedp_recurr_start_time || $time>$getKupon->schedp_recurr_end_time)
                    {
                        return response()->json([
                            'error' => 'Promo Tidak Berlaku Pada Saat Ini'
                        ]);
                    }
                }
            }
            elseif ($getKupon->schedp_RecurrenceType==3) { // Recurring Bulan
                $months = explode(",", $getKupon->schedp_Int2);
                $tanggal = date('Y-m-d');
                $status_bulan = 0;
                foreach ($months as $month) {
                    if($month==0){
                        if($tanggal == date('Y-m-d', strtotime('January'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==1){
                        if($tanggal == date('Y-m-d', strtotime('February'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==2){
                        if($tanggal == date('Y-m-d', strtotime('March'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==3){
                        if($tanggal == date('Y-m-d', strtotime('April'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==4){
                        if($tanggal == date('Y-m-d', strtotime('May'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==5){
                        if($tanggal == date('Y-m-d', strtotime('June'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==6){
                        if($tanggal == date('Y-m-d', strtotime('July'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==7){
                        if($tanggal == date('Y-m-d', strtotime('August'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==8){
                        if($tanggal == date('Y-m-d', strtotime('September'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==9){
                        if($tanggal == date('Y-m-d', strtotime('October'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==10){
                        if($tanggal == date('Y-m-d', strtotime('November'))){
                            $status_bulan = 1;
                        }
                    }
                    if($month==11){
                        if($tanggal == date('Y-m-d', strtotime('December'))){
                            $status_bulan = 1;
                        }
                    }
                }
                if($status_bulan!=1){
                    return response()->json([
                        'error' => 'Promo Tidak Berlaku Pada Saat Ini'
                    ]);
                }
            }
            else{
                if($getKupon->schedp_start_date !=0 && $getKupon->schedp_end_date !=0)
                {
                    $time = date('Y-m-d H:i:s');
                    if($time<$getKupon->schedp_start_date || $time>$getKupon->schedp_end_date)
                    {
                        return response()->json([
                            'error' => 'Promo Tidak Berlaku Pada Saat Ini'
                        ]);
                    }
                }
            }
            //End Check Recurring

            if($getKupon == null)
            {
                return response() -> json([
                    'error' => "Kupon Tidak Berlaku"
                ]);
            }
            else
            {
                if($getKupon->status_coupons == "sudah dipakai")
                {
                    return response() -> json([
                        'error' => "Kupon Sudah Dipakai"
                    ]);
                }
                else
                {
                    // ambil barang dalam promo
                    if($getKupon->schedp_simple_action != 'free_shipping'){
                        $getProductKupon = DB::table('nm_promo_products')
                        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_promo_products.promop_pro_id')
                        ->where('promop_schedp_id', $getKupon->schedp_id)->get();

                        // dd($getProductKupon);
                    }

                    // kondisi diskon kupon by percent
                    if($getKupon->schedp_simple_action == "by_percent")
                    {
                        if($getKupon->schedp_end_date > $today)
                        {
                            if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                                $carts = $request->input('cart');
                                $diskon = 0;
                                // dd($carts);
                                if($getKupon->schedp_coupon_type==1)
                                {
                                    $total_belanja_akhir = (((100 - $getKupon->schedp_discount_amount)* $total_belanja_awal) / 100);
                                    $total_diskon = (($getKupon->schedp_discount_amount/100) * $total_belanja_awal);
                                }
                                elseif ($getKupon->schedp_coupon_type==2)
                                {
                                    foreach($carts as $cart){
                                        foreach ($getProductKupon as $productKupon)
                                        {
                                            if($cart['sku'] == $productKupon->promop_pro_id)
                                            {
                                                $diskon += $productKupon->promop_saving_price;
                                            }
                                        }
                                    }
                                    $total_belanja_akhir = $total_belanja_awal - $diskon;
                                    $total_diskon = $diskon;
                                    if($total_diskon==0){
                                        return response() -> json([
                                            'error' => "Barang Tidak Sesuai Dengan Kupon"
                                        ]);
                                        die();
                                    }
                                }

                                // dd($diskon);
                                $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);

                                $total_belanja_plus_shipping = $total_belanja_setelah_pajak + $total_shipping;

                                return response() -> json([
                                    'schedp_title' => $getKupon->schedp_title, // judul
                                    'schedp_discount_amount' => $getKupon->schedp_discount_amount, // jumlah diskonnya
                                    'total_belanja_akhir' => $total_belanja_akhir,
                                    'total_diskon' => $total_diskon,
                                    'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                    'total_belanja_setelah_pajak' => $total_belanja_setelah_pajak,
                                    'total_belanja_plus_shipping' => $total_belanja_plus_shipping
                                ]);
                            }
                            else {
                                return response() -> json([
                                    'error' => "Total Belanja Kurang Dari Minimal Transaksi"
                                ]);
                            }
                        }
                        else
                        {
                            return response() -> json([
                                'error' => "Kupon Tidak Berlaku"
                            ]);
                        }
                    }

                    // kondisi diskon kupon by amount
                    else if($getKupon->schedp_simple_action == "by_fixed")
                    {
                        if($getKupon->schedp_end_date > $today)
                        {
                            if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                                $carts = $request->input('cart');
                                $diskon = 0;
                                // dd($carts);
                                if($getKupon->schedp_coupon_type==1)
                                {
                                    $total_belanja_akhir = ($total_belanja_awal - $getKupon->schedp_discount_amount);
                                    $total_diskon = $getKupon->schedp_discount_amount;
                                }
                                elseif ($getKupon->schedp_coupon_type==2) {
                                    foreach($carts as $cart)
                                    {
                                        foreach ($getProductKupon as $productKupon)
                                        {
                                            if($cart['sku'] == $productKupon->promop_pro_id)
                                            {
                                                $diskon += $productKupon->promop_saving_price;
                                            }
                                        }
                                    }
                                    $total_belanja_akhir = $total_belanja_awal - $diskon;
                                    $total_diskon = $diskon;
                                    if($total_diskon==0){
                                        return response() -> json([
                                            'error' => "Barang Tidak Sesuai Dengan Kupon"
                                        ]);
                                        die();
                                    }
                                }
                                $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);
                                $total_belanja_plus_shipping = $total_belanja_setelah_pajak + $total_shipping;
                                return response() -> json([
                                    'schedp_title' => $getKupon->schedp_title, // judul
                                    'schedp_discount_amount' => floatval($getKupon->schedp_discount_amount), // jumlah diskonnya
                                    'total_belanja_akhir' => floatval($total_belanja_akhir),
                                    'total_diskon' => floatval($total_diskon),
                                    'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                    'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                    'total_belanja_plus_shipping' => $total_belanja_plus_shipping
                                ]);
                            }
                            else {
                                return response() -> json([
                                    'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                ]);
                            }
                        }
                        else
                        {
                            return response() -> json([
                                'error' => "Kupon Tidak Berlaku",
                            ]);
                        }
                    }
                    // kondisi diskon kupon free shipping
                    else if($getKupon->schedp_simple_free_shipping == 1)
                    {
                        // echo("masuk kondisi free shipping -|- ");
                        // cek kondisi tanggal
                        if($getKupon->schedp_end_date > $today && $getKupon->schedp_start_date < $today)
                        {
                            // echo("masuk lolos cek tanggal -|- ");
                            // echo("today :" .$today ."-|- ");
                            // echo("start_date :".$getKupon->schedp_start_date." -|- ");
                            // echo("end date :".$getKupon->schedp_end_date." -|- ");
                            // kondisi minimal transaksi
                            if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                                // echo("lolos dari minimal transaksi -|- ");
                                // kondisi free shipping postal service tertentu dan kondisi untuk semua postal service
                                if($getKupon->ps_code == $postal_code || $getKupon->ps_code == 0)
                                {
                                    // kondisi maximum free shipping
                                    if($getKupon->maximum_free_shipping >= $total_shipping)
                                    {
                                        $total_belanja_akhir = $total_belanja_awal;
                                        $total_diskon = 0;

                                        $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);

                                        return response() -> json([
                                            'schedp_title' => $getKupon->schedp_title, // judul
                                            'schedp_discount_amount' => "Free Shipping", // jumlah diskonnya
                                            'total_belanja_akhir' => floatval($total_belanja_akhir),
                                            'total_diskon' => floatval($total_shipping),
                                            'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                            'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                            'shipping_cost' => $total_shipping,
                                            'total_belanja_plus_shipping' => floatval($total_belanja_setelah_pajak)
                                        ]);
                                    }
                                    else
                                    {
                                        $total_belanja_akhir = $total_belanja_awal + $total_shipping - floatval($getKupon->maximum_free_shipping);
                                        $total_diskon = 0;

                                        $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);

                                        return response() -> json([
                                            'schedp_title' => $getKupon->schedp_title, // judul
                                            'schedp_discount_amount' => "Free Shipping", // jumlah diskonnya
                                            'total_belanja_akhir' => floatval($total_belanja_akhir),
                                            'total_diskon' => floatval($getKupon->maximum_free_shipping),
                                            'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                            'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                            'shipping_cost' => $total_shipping,
                                            'total_belanja_plus_shipping' => floatval($total_belanja_setelah_pajak)
                                        ]);
                                    }
                                }
                                else
                                {
                                    return response() -> json([
                                        'error' => "Kupon tidak berlaku untuk pengiriman melalui "+ $postal_code,
                                    ]);
                                }
                            }
                            else {
                                return response() -> json([
                                    'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                ]);
                            }
                        }
                        else
                        {
                            return response() -> json([
                                'error' => "Kupon Expired",
                            ]);
                        }
                    }
                    // if kondisi untuk buy x get y
                    else if ($getKupon->schedp_simple_action == 'buy_x_get_y')
                    {
                        if($getKupon->schedp_end_date > $today)
                        {
                            if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                                $carts = $request->input('cart');
                                $quantity_diskon = 0;
                                //dd($carts);
                                foreach($carts as $cart){
                                    foreach ($getProductKupon as $productKupon)
                                    {
                                        if($cart['sku'] == $productKupon->promop_pro_id)
                                        {
                                            $quantity_diskon = floor(($cart['qty']/($getKupon->schedp_x + $getKupon->schedp_y))*$getKupon->schedp_y);
                                            $total_diskon = $quantity_diskon * $cart['price'];
                                            // if($productKupon->pro_disprice == 0 || $productKupon->pro_disprice == null || $productKupon->pro_disprice == ''){
                                            //     $total_diskon = $quantity_diskon * $productKupon->pro_price;
                                            // }else {
                                            //     $total_diskon = $quantity_diskon * $productKupon->pro_disprice;
                                            // }
                                        }
                                    }
                                }
                                if($quantity_diskon==0){
                                    return response() -> json([
                                        'error' => "Barang Tidak Sesuai Dengan Kupon"
                                    ]);
                                    die();
                                }
                                $total_belanja_akhir = $total_belanja_awal - $total_diskon;
                                $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);
                                $total_belanja_plus_shipping = $total_belanja_setelah_pajak + $total_shipping;
                                return response() -> json([
                                    'schedp_title' => $getKupon->schedp_title,
                                    'schedp_discount_amount' => "FREE ".$quantity_diskon,
                                    'total_belanja_akhir' => $total_belanja_akhir,
                                    'total_diskon' => $total_diskon,
                                    'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                    'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                    'total_belanja_plus_shipping' => floatval($total_belanja_plus_shipping)
                                ]);
                            }
                            else {
                                return response() -> json([
                                    'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                ]);
                            }
                        }
                        else
                        {
                            return response() -> json([
                                'error' => 'Kupon Tidak Berlaku',
                            ]);
                        }
                    }
                }
            }
        }

        public function kirim_request(Request $request)
        {
            // -- Deklarasi variable
            $kode   = $_GET['aKode'];
            $Items  = $request->input('aItems');
            $aToZip = $_GET['aToZip'];
            $aCity  = $_GET['aCity'];

            //dd($items);
            if ($kode == 'Columbia') {
                $biaya_columbia = DB::table('nm_paymentsettings')->sum('ps_biaya_columbia');
                $biaya_columbia = intval($biaya_columbia);
                for ($l=0; $l < count($Items); $l++) {
                    $Items[$l]['cost'] = $biaya_columbia;
                }
                return response() -> json([
                    'price'         => $biaya_columbia * count($Items),
                    'items'         => $Items,
                    'est_duration'  => '1',
                    'kode' => $kode
                ]);
            }
            if ($kode == "AL") {
                for ($i=0; $i < count($Items); $i++) {
                    $Items[$i]['cost'] = 0;
                }
                return response()->json([
                    'price' => 0,
                    'items' => $Items,
                    'est_duration' => '1',
                    'kode' => $kode
                ]);
            }

            $max = count($Items);

            for($i = 0;$i<$max;$i++)
            {
                $id = $Items[$i]['id'];
                //dd($id);
                $x = City::get_zipcode_shipping_item($id);
                // dd($x);
                //cara push array
                $item_with_shop[] =  $x;
            }
            //membuat menjadi koleksi
            $collection = collect($item_with_shop);
            //meng-group koleksi
            $colgrop = $collection->groupBy('stor_zipcode')->toArray();

            //dd($colgrop);
            foreach($colgrop as $zipcode => $ndata)
            {
                $dest = array(
                    //'to_city_code' => $aCity,
                    'to_zip_code' => $aToZip,
                    //'from_city_code' => $colgrop[$ndata][$i]['ci_code'],
                    'from_zip_code' => $zipcode,
                );

                // dd($dest);

                $result = \App\WMSClient::getPostalInfo($kode, $Items, $dest);

                if(isset($result['error']))
                {
                    return response() -> json([
                        'error' => $result['error'],
                        'price' => 0,
                        ]);
                }
                else
                {
                    return response() -> json([
                        'price'         => $result['total_cost'],
                        'items'         => $result['items'],
                        'est_duration'  => $result['est_duration'],
                        'kode' => $kode

                    ]);
                }

            }

        }

        public function get_data_unsync2(Request $request)
        {
            $result = Home::recall_unsync();

            return $result;
        }

        public function get_sku_by_id($id)
        {
            $sku = Home::getSkubyId_product($id);
            return $sku;
        }
    }
