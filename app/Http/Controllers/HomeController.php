<?php
namespace App\Http\Controllers;
use DB;
use Config;
use Session;
use App\Http\Models;
use App\Register;
use App\Products;
use App\Home;
use App\Footer;
use App\Merchant;
use App\Country;
use App\Settings;
use App\City;
use App\PlanTimer;
use App\Transactions;
use App\Popbox;
use App\AuditTrail;
use App\Sepulsa;
use MyPayPal;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Log;
use Carbon;
use App\Commands\Command;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class HomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function get_select_api_fathan_merchant_order()
    {
        $skip = $_GET['skip'];
        $take = $_GET['take'];
        $id_transaksi = $_GET['id_transaksi'];
        $from_date = $_GET['from_date'];
        $end_date = $_GET['end_date'];

        $data = Home::get_select_api_fathan_merchant_order_q($skip, $take, $id_transaksi, $from_date, $end_date);

        $total = DB::table('nm_order')
        ->leftJoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->leftJoin('nm_transaksi','nm_transaksi.transaction_id','=','nm_order.transaction_id')
        ->leftJoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftJoin('nm_merchant','nm_merchant.mer_id','=', 'nm_product.pro_mr_id')
        ->where('nm_product.pro_title','!=','Biaya Pengiriman')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->count();

        $data_2 = array(
            'total' => $total,
            'data' => $data
            );

        //return response($data_2, 200);

        return response()->json($data_2);
    }

    public function flashPromoView(){
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();

        $women_product                = Home::get_women_product();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();
        //$getfavicondetails            = Home::getfavicondetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $groping_main                 = Home::get_main_group_product();
        //dd($groping_main);
        $grouping_product             = Home::get_grouping_product();
        //dd($grouping_product);
        $get_schedule_flash                 = Products::get_promo_flash_product();

        //dd($get_schedule);

        // $laravel = app();
        // echo "Laravel Version : ".$laravel::VERSION;
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')
            ->with('country_details', $country_details)
            ->with('metadetails', $getmetadetails)
            ->with('general', $general)
            ->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')
            ->with('country_details', $country_details)
            ->with('metadetails', $getmetadetails)
            ->with('general', $general)
            ->with('getanl', $getanl);
        }

        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('index')
        ->with("get_cur", $get_cur)
        ->with('get_schedule_flash', $get_schedule_flash)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deals_details', $deals_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('addetails', $addetails)
        ->with('noimagedetails', $noimagedetails)
        ->with('bannerimagedetails', $getbannerimagedetails)
        ->with('metadetails', $getmetadetails)
        ->with('women_product', $women_product)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general)
        ->with('grouping_product', $grouping_product)
        ->with('groping_main', $groping_main);

    }
    public function loginto(){
      return view('loginto');
    }
    public function slider(){
        return view('slider');
    }
    public function carosel(){
        return view('carosel');
    }
    public function siteadmin()
    {
        return Redirect::to('siteadmin');
    }

    public function rating_prediction($limit, $type)
    {
        if (Session::has('customerid')) {
            $items_bought = DB::table('nm_review')
            ->where('customer_id', Session::get('customerid'))
            ->groupBy('product_id')
            ->get();
            $average_rating = DB::table('nm_review')
            ->where('customer_id', Session::get('customerid'))
            ->avg('ratings');

            $items_not_buy = DB::table('nm_product')
            ->where('pro_id', '!=', 1)
            ->where('pro_id', '!=', 2)
            ->where('pro_status', '=', 1)
            ->where('pro_isapproved', 1)
            ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
            ->where(function($q) use($items_bought){
                foreach ($items_bought as $item) {
                    $q = $q->where('pro_id', '!=', $item->product_id);
                }
            })
            ->get();

            $neighbors = DB::table('neighbor')
            ->where('customer_id', Session::get('customerid'));
            if ($limit != 0) {
                $neighbors = $neighbors->limit($limit);
            }else {
                $neighbors = $neighbors->where('value', '>', 0);
            }
            $neighbors = $neighbors->orderBy('value', 'desc')
            ->get();

            if ($neighbors == null) {
                return null;
            }
            if ($neighbors[0]->value == 0) {
                return null;
            }

            $delete_rating_prediction = DB::table("rating_prediction")
            ->where('customer_id', Session::get('customerid'))
            ->where('type_neighbor', $limit)
            ->delete();

            foreach ($items_not_buy as $item) {
                $process1 = 0;
                $process2 = 0;
                foreach ($neighbors as $neighbor) {
                    $rating_item = DB::table('nm_review')
                    ->where('customer_id', $neighbor->to_customer_id)
                    ->where('product_id', $item->pro_id)
                    ->first();
                    if ($rating_item == null) {
                        $ratings = 0;
                    }else {
                        $ratings = $rating_item->ratings;
                    }
                    // echo $ratings.' '.$neighbor->to_customer_id.' '.$item->pro_id.'<br>';
                    $user_rating_average = DB::table('nm_review')
                    ->where('customer_id', $neighbor->customer_id)
                    ->avg('ratings');
                    $process1 += $neighbor->value * ($ratings - $user_rating_average);
                    $process2 += $neighbor->value;
                    // echo $item->pro_id.' '.$rating_item.' - '.$user_rating_average.'<br>';
                }
                $rating_prediction = $average_rating + ($process1 / $process2);
                // echo $rating_prediction.' = '.$average_rating.' + ('.$process1.' / '.$process2.')<br>';
                $insert = DB::table('rating_prediction')
                ->insert([
                    'customer_id' => Session::get('customerid'),
                    'product_id' => $item->pro_id,
                    'rating_prediction' => $rating_prediction,
                    'type_neighbor' => $limit
                ]);
            }

            $get_recommended = DB::table('rating_prediction')
            ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'rating_prediction.product_id')
            ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
            ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
            ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
            ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
            ->where('customer_id', Session::get('customerid'))
            ->where('type_neighbor', $limit)
            ->where('rating_prediction', '>', 0)
            ->orderBy('rating_prediction', 'desc');
            if ($type == 'home') {
                $get_recommended = $get_recommended->limit(5);
            }
            $get_recommended = $get_recommended->get();

            return $get_recommended;
        }else {
            return null;
        }
    }

    public function index()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();

        $women_product                = Home::get_women_product();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();
        //$getfavicondetails            = Home::getfavicondetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_popup                    = Home::get_popup();
        //dd($get_popup);
        $groping_main                 = Home::get_main_group_product();
        //dd($groping_main);
        $grouping_product             = Home::get_grouping_product();
        //dd($grouping_product);
        $get_customer_group = DB::table('nm_customer_group_relation')
        ->where('cgr_cus_id', Session::get('customerid'))
        ->get();

        $get_payment_method = DB::table('nm_payment_method_header')
        ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
        ->where(function($q) use($get_customer_group){
            $q = $q->where('cus_group_id', 0);
            foreach ($get_customer_group as $cus_group) {
                $q = $q->orWhere('cus_group_id', $cus_group->cgr_cus_group_id);
            }
        })
        ->where('kode_pay_detail', '!=', 5)
        ->where('kode_pay_detail', '!=', 4)
        ->get();

        if(empty($product_details)){
            $product_spec = '';
        }
        foreach($product_details as $product_det){
            $product_spec[] = DB::table('nm_prospec')
            ->where('spc_pro_id', '=', $product_det->pro_id)
            ->LeftJoin('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
            ->LeftJoin('nm_spgroup', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
            ->get();
        }
        $get_schedule_flash                 = Products::get_promo_flash_product();
        $next_flash_display = 0;
        $now_flash_display = 0;
        $expired_flash_display = 0;
        if($get_schedule_flash){
            foreach ($get_schedule_flash as $product_flash) {
                $todays = date('Y-m-d H:i:s');

                $start_date = $product_flash->schedp_start_date;

                $end_date = $product_flash->schedp_end_date;

                if($todays < $start_date && $todays < $end_date){
                    $next_flash_display = 1;
                }
                if($todays > $start_date && $todays <  $end_date){
                    $now_flash_display = 1;
                }
                if($todays > $end_date){
                    $expired_flash_display = 1;
                }
            }
        }

        $neighbor_limit = [
            '3' => 3,
            '5' => 5,
            'All' => 0
        ];
        foreach ($neighbor_limit as $limit) {
            $get_recommended[] = self::rating_prediction($limit, 'home');
        }
        // dd($get_recommended);
        // dd($get_schedule_flash);
        //dd($get_schedule);

        // $laravel = app();
        // echo "Laravel Version : ".$laravel::VERSION;
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')
                ->with('country_details', $country_details)
                ->with('metadetails', $getmetadetails)
                ->with('general', $general)
                ->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')
                ->with('country_details', $country_details)
                ->with('metadetails', $getmetadetails)
                ->with('general', $general)
                ->with('getanl', $getanl);
        }

        $header = view('includes.header')
            ->with('header_category', $header_category)
            ->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')
    		->with('pm_image_details',$get_pm_img_details)
            ->with('cms_page_title', $cms_page_title)
            ->with('get_partners', $get_partners)
            ->with('get_social_media_url', $get_social_media_url)
            ->with('get_contact_det', $get_contact_det)
            ->with('getanl', $getanl);

        return view('index')
        ->with('neighbor_limit', $neighbor_limit)
        ->with('recommended_products', $get_recommended)
        ->with('customerid', Session::get('customerid'))
        ->with('get_payment_method', $get_payment_method)
        ->with('product_spec', $product_spec)
        ->with('get_popup',$get_popup)
        ->with("get_cur", $get_cur)
        ->with('navbar', $navbar)
        ->with('next_flash_display', $next_flash_display)
        ->with('now_flash_display', $now_flash_display)
        ->with('expired_flash_display', $expired_flash_display)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('bannerimagedetails', $getbannerimagedetails)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deals_details', $deals_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('addetails', $addetails)
        ->with('noimagedetails', $noimagedetails)
        ->with('metadetails', $getmetadetails)
        ->with('women_product', $women_product)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general)
        ->with('grouping_product', $grouping_product)
        ->with('groping_main', $groping_main)
		->with('get_schedule_flash', $get_schedule_flash);
    }

    public function all_products_neighbour($neighbours)
    {
        $mc_id = null;
        $smc_id = null;
        $sb_id = null;
        $ssb_id = null;

        $param['pro_title']         = Input::get('q');
        $param['mc_id']             = Input::get('mc_id');
        $param['smc_id']            = Input::get('smc_id');
        $param['sb_id']             = Input::get('sb_id');
        $param['ssb_id']            = Input::get('ssb_id');

        $param['spc_value']          = Input::get('spc_value');
        $param['grade']             = Input::get('grade');
        $param['sp_name']           = Input::get('sp_name');

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = self::rating_prediction($neighbours, 'all_products');
        // dd($product_details);
        $write_file = fopen('hasil.csv', 'w');
        $outputData = [
            'customer_id',
            'pro_id',
            'pro_title',
            'rating_prediction',
            'type_neighbor'
        ];
        fputcsv($write_file, $outputData);
        foreach ($product_details as $pro_det) {
            $outputData = [
                $pro_det->customer_id,
                $pro_det->pro_id,
                $pro_det->pro_title,
                $pro_det->rating_prediction,
                $pro_det->type_neighbor
            ];
            fputcsv($write_file, $outputData);
        }
        fclose($write_file);
        $search_max_min               = Products::search_max_min($param);
        $get_grade                    = Products::search_grade($param);
        $spc_result                   = Products::search_spc($param);
        // dd($spc_result);
        $deal_details                 = array();
        $auction_details              = array();
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_schedule_flash           = Products::get_promo_flash_product();
        $product_spec = DB::table('nm_product')
        ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
        ->LeftJoin('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', 1)
        ->where('mc_status', 1)
        ->where('smc_status', 1)
        ->where('sb_status', 1)
        ->where('ssb_status', 1)
        ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
        ->orderBy('pro_id', 'desc')
        ->groupBy([
            'pro_id',
            'sp_name',
            'spc_value'
        ])
        ->select('pro_id', 'sp_name', 'spc_value')->get();

        $product_spec = collect($product_spec)->groupBy('pro_id');
        // dd($product_spec);
        // dd($product_details);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('products')
        ->with('get_schedule_flash', $get_schedule_flash)
        ->with('get_cur', $get_cur)->with('navbar', $navbar)
        ->with('header', $header)->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deal_details', $deal_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general)
        ->with('search_max_min', $search_max_min)
        ->with('get_grade', $get_grade)
        ->with('spc_result', $spc_result)
        ->with('mc_id', $mc_id)
        ->with('smc_id', $smc_id)
        ->with('sb_id', $sb_id)
        ->with('ssb_id', $ssb_id)
        ->with('product_spec', $product_spec);
    }

    // insert number count
    public function getting_count_klick($id_banner)
    {
        $det_banner = Home::get_detail_banner($id_banner);
        //dd($det_banner);
        $jml_klik = floatval($det_banner->jumlah_klik);
        $jml_klik = $jml_klik +1;
        //dd($jml_klik);

        Home::update_klik_banner($id_banner, $jml_klik);

        return Redirect::to($det_banner->bn_redirecturl);
    }

    public function get_countpopup_click($id_pop)
    {
        $det_banner = Home::get_detail_popup($id_pop);
        //dd($det_banner);
        $jml_klik = floatval($det_banner->jumlah_klik);
        $jml_klik = $jml_klik +1;
        //dd($jml_klik);

        Home::update_klik_popup($id_pop, $jml_klik);

        return Redirect::to($det_banner->pop_redirect);
    }

    public function kualitas_produk()
    {
        $country_details              = Register::get_country_details();
        $getmetadetails               = Home::getmetadetails();
        $general                      = Home::get_general_settings();
        $getlogodetails               = Home::getlogodetails();
        $cms_page_title               = Home::get_cms_page_title();
        $main_category                = Home::get_header_category();
        $header_category              = Home::get_header_category();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $get_social_media_url         = Home::get_social_media_url();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)
        ->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)
        ->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)
        ->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)
        ->with('getanl', $getanl);

        return view('kualitas_produk')->with('navbar', $navbar)->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)->with('main_category', $main_category)
        ->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function merchant_signup()
    {

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header         = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer         = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
        $country_return = Merchant::get_country_detail();
        return view('merchant_signup')
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('country_details', $country_return)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);
    }

    public function merchant_signup_submit()
    {
               $data = Input::except(
                   array(
                   '_token'
                   )
               );

        // kodingan asli

        // $rule = array(
        //     'first_name' => 'required|alpha_dash',
        //     'last_name' => 'required|alpha_dash',
        //     'email_id' => 'required|email',
        //     'select_mer_country' => 'required',
        //     'select_mer_city' => 'required',
        //     'phone_no' => 'required|numeric',
        //     'addreess_one' => 'required',
        //     'address_two' => 'required',
        //     'store_name' => 'required',
        //     'store_pho' => 'required|numeric',
        //     'store_add_one' => 'required',
        //     'store_add_two' => 'required',
        //     'select_country' => 'required',
        //     'select_city' => 'required',
        //     'zip_code' => 'required|numeric',
        //     'meta_keyword' => 'required',
        //     'meta_description' => 'required',
        //     'website' => 'required',
        //     'latitude' => 'required',
        //     'longtitude' => 'required',
        //     'commission' => 'required|numeric'

        // );

        $rule = array(
            'first_name' => 'required|alpha_dash',
            'last_name' => 'required|alpha_dash',
            'email_id' => 'required|email',
            'select_mer_country' => 'required',
            'select_mer_city' => 'required',
            'phone_no' => 'required|numeric',
            'addreess_one' => 'required',
            'address_two' => 'required',
            'store_name' => 'required',
            'store_pho' => 'required|numeric',
            'store_add_one' => 'required',
            'store_add_two' => 'required',
            'select_country' => 'required',
            'select_city' => 'required',
            'zip_code' => 'required|numeric',
            'latitude' => 'required',
            'longtitude' => 'required',
        );

        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return Redirect::to('merchant_signup')->withErrors($validator->messages())->withInput();
        } else {
            date_default_timezone_set('Asia/Jakarta');
            $now = date('Y-m-d H:i:s');
            $mer_email         = Input::get('email_id');
            $check_merchant_id = Merchant::check_merchant_email($mer_email);
            if ($check_merchant_id) {
                return Redirect::to('merchant_signup')->with('mail_exist', 'Merchant Email Exist')->withInput();
            } else {
                $file             = Input::file('file');
                if($file != '') {
                     $filename         = $file->getClientOriginalName();
                    $move_img         = explode('.', $filename);
                    $filename         = $move_img[0] . str_random(8) . "." . $move_img[1];
                    $destinationPath  = './assets/storeimage/';

                    $uploadSuccess    = Input::file('file')->move($destinationPath, $filename);
                    $get_new_password = Merchant::randomPassword();
                    $merchant_entry   = array(
                    'mer_fname' => Input::get('first_name'),
                    'mer_lname' => Input::get('last_name'),
                    'mer_password' => $get_new_password,
                    'mer_email' => Input::get('email_id'),
                    'mer_phone' => Input::get('phone_no'),
                    'mer_address1' => Input::get('addreess_one'),
                    'mer_address2' => Input::get('address_two'),
                    'mer_co_id' => Input::get('select_mer_country'),
                    'mer_ci_id' => Input::get('select_mer_city'),
                    'mer_payment' => Input::get('payment_account'),
                    'mer_commission' => Input::get('commission'),
                    'created_date' => $now
                    );


                    $inserted_merchant_id = Merchant::insert_merchant($merchant_entry);



                    $store_entry = array(
                    'stor_name' => Input::get('store_name'),
                    'stor_merchant_id' => $inserted_merchant_id,
                    'stor_phone' => Input::get('store_pho'),
                    'stor_address1' => Input::get('store_add_one'),
                    'stor_address2' => Input::get('store_add_two'),
                    'stor_country' => Input::get('select_country'),
                    'stor_city' => Input::get('select_city'),
                    'stor_zipcode' => Input::get('zip_code'),
                    'stor_metakeywords' => Input::get('meta_keyword'),
                    'stor_metadesc' => Input::get('meta_description'),
                    'stor_website' => Input::get('website'),
                    'stor_latitude' => Input::get('latitude'),
                    'stor_longitude' => Input::get('longtitude'),
                    'stor_img' => $filename
                    );

                }
                else{
                     $get_new_password = Merchant::randomPassword();
                    $merchant_entry   = array(
                    'mer_fname' => Input::get('first_name'),
                    'mer_lname' => Input::get('last_name'),
                    'mer_password' => $get_new_password,
                    'mer_email' => Input::get('email_id'),
                    'mer_phone' => Input::get('phone_no'),
                    'mer_address1' => Input::get('addreess_one'),
                    'mer_address2' => Input::get('address_two'),
                    'mer_co_id' => Input::get('select_mer_country'),
                    'mer_ci_id' => Input::get('select_mer_city'),
                    'mer_payment' => Input::get('payment_account'),
                    'mer_commission' => Input::get('commission'),
                    'created_date' => $now
                    );


                    $inserted_merchant_id = Merchant::insert_merchant($merchant_entry);



                    $store_entry = array(
                    'stor_name' => Input::get('store_name'),
                    'stor_merchant_id' => $inserted_merchant_id,
                    'stor_phone' => Input::get('store_pho'),
                    'stor_address1' => Input::get('store_add_one'),
                    'stor_address2' => Input::get('store_add_two'),
                    'stor_country' => Input::get('select_country'),
                    'stor_city' => Input::get('select_city'),
                    'stor_zipcode' => Input::get('zip_code'),
                    'stor_metakeywords' => Input::get('meta_keyword'),
                    'stor_metadesc' => Input::get('meta_description'),
                    'stor_website' => Input::get('website'),
                    'stor_latitude' => Input::get('latitude'),
                    'stor_longitude' => Input::get('longtitude')

                    );
                }


                Merchant::insert_store($store_entry);

                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                Mail::send(
                    'emails.merchantmail', array(
                    'first_name' => Input::get('first_name'),
                    'password' => $get_new_password
                    ), function ($message) {
                        $message->to(Input::get('email_id'))->subject('Merchant Account Created Successfully');
                    }
                );

            }



            return Redirect::to('submission_merchant');
        }


    }


    public function submission_merchant()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header         = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer         = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
        $country_return = Merchant::get_country_detail();
        return view('submission')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('country_details', $country_return)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }



    public function nearmemap()
    {

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_all_deals_details();
        $auction_details              = Home::get_all_action_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();

        $get_store_details            = Home::get_store_list();
        $get_store_deal_count         = Home::get_store_deal_count($get_store_details);
        $get_store_auction_count      = Home::get_store_auction_count($get_store_details);
        $get_store_product_count      = Home::get_store_product_count($get_store_details);
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_storeall                 = Home::get_store_all();
        $get_store_main               = Home::get_store_setting();
        $get_store_all                = Home::get_store_all();
        $get_default_city             = Home::get_default_city();
        $get_location_popbox          = Popbox::getLocation('Indonesia', '');
        $get_city_popbox = Popbox::getCity('Indonesia');
        $get_contact_det = Footer::get_contact_details();
        $getanl          = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('nearmemap2')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('deals_details', $deals_details)->with('auction_details', $auction_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('get_store_details', $get_store_details)->with('get_store_deal_count', $get_store_deal_count)->with('get_store_auction_count', $get_store_auction_count)->with('get_store_product_count', $get_store_product_count)->with('metadetails', $getmetadetails)->with('get_storeall', $get_storeall)->with('get_store_main', $get_store_main)->with('get_store_all', $get_store_all)->with('get_contact_det', $get_contact_det)->with('get_default_city', $get_default_city)->with('general', $general)->with('get_location_popbox', $get_location_popbox['data'])->with('get_city_popbox', $get_city_popbox['data']);
    }


    public function products()
    {
        $mc_id = null;
        $smc_id = null;
        $sb_id = null;
        $ssb_id = null;

        $param['pro_title']         = Input::get('q');
        $param['mc_id']             = Input::get('mc_id');
        $param['smc_id']            = Input::get('smc_id');
        $param['sb_id']             = Input::get('sb_id');
        $param['ssb_id']            = Input::get('ssb_id');

        $param['spc_value']          = Input::get('spc_value');
        $param['grade']             = Input::get('grade');
        $param['sp_name']           = Input::get('sp_name');

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_products();

        $search_max_min               = Products::search_max_min($param);
        $get_grade                    = Products::search_grade($param);
        $spc_result                   = Products::search_spc($param);
        // dd($spc_result);
        $deal_details                 = array();
        $auction_details              = array();
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_schedule_flash           = Products::get_promo_flash_product();
        $product_spec = DB::table('nm_product')
        ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
        ->LeftJoin('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', 1)
        ->where('mc_status', 1)
        ->where('smc_status', 1)
        ->where('sb_status', 1)
        ->where('ssb_status', 1)
        ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
        ->orderBy('pro_id', 'desc')
        ->groupBy([
            'pro_id',
            'sp_name',
            'spc_value'
        ])
        ->select('pro_id', 'sp_name', 'spc_value')->get();

        $product_spec = collect($product_spec)->groupBy('pro_id');
        // dd($product_spec);
        // dd($product_details);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('products')
        ->with('get_schedule_flash', $get_schedule_flash)
        ->with('get_cur', $get_cur)->with('navbar', $navbar)
        ->with('header', $header)->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deal_details', $deal_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general)
        ->with('search_max_min', $search_max_min)
        ->with('get_grade', $get_grade)
        ->with('spc_result', $spc_result)
        ->with('mc_id', $mc_id)
        ->with('smc_id', $smc_id)
        ->with('sb_id', $sb_id)
        ->with('ssb_id', $ssb_id)
        ->with('product_spec', $product_spec);

    }

    public function products_flash()
    {
        $get_schedule_flash           = Products::get_promo_flash_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_products();
        $deal_details                 = array();
        $auction_details              = array();
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        //dd($product_details);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productflash')
        ->with('get_schedule_flash', $get_schedule_flash)
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deal_details', $deal_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);

    }


    public function category_product_list($name, $id)
    {
        $product_id      = base64_decode($id);
        $id              = base64_decode(base64_decode(base64_decode($id)));

        $mc_id = null;
        $smc_id = null;
        $sb_id = null;
        $ssb_id = null;

        $city_details    = Register::get_city_details();
        $header_category = Home::get_header_category();

        $country_details = Register::get_country_details();
        if ($name == "viewcategorylist") {
            $get_cat_name_listby = Home::get_catname_listby($product_id);

            $product_details     = Home::get_category_product_details_listby($product_id);

            $deal_details        = Home::get_category_deal_details_listby($product_id);
            $auction_details     = Home::get_category_auction_details_listby($product_id);
            $get_listby_id       = explode(",", $product_id);

            foreach ($get_cat_name_listby as $get_cat_name_listby_single) {
            }

            if ($get_listby_id[0] == 1) {
                $product_name_single = $get_cat_name_listby_single->mc_name;
                $mc_id = $get_cat_name_listby_single->mc_id;
            } else if ($get_listby_id[0] == 2) {
                $product_name_single = $get_cat_name_listby_single->smc_name;
                $smc_id = $get_cat_name_listby_single->smc_id;
            } else if ($get_listby_id[0] == 3) {
                $product_name_single = $get_cat_name_listby_single->sb_name;
                $sb_id = $get_cat_name_listby_single->sb_id;
            } else if ($get_listby_id[0] == 4) {
                $product_name_single = $get_cat_name_listby_single->ssb_name;
                $ssb_id = $get_cat_name_listby_single->ssb_id;
            }

        } else {
            $product_details     = Home::get_category_product_details($id);
            $product_name_single = "";
        }

        $param['pro_title']         = Input::get('q');
        $param['mc_id']             = $mc_id;
        $param['smc_id']            = $smc_id;
        $param['sb_id']             = $sb_id;
        $param['ssb_id']            = $ssb_id;

        $param['spc_value']         = Input::get('spc_value');
        $param['grade']             = Input::get('grade');
        $param['sp_name']           = Input::get('sp_name');

        $search_max_min               = Products::search_max_min($param);
        $get_grade                    = Products::search_grade($param);
        $spc_result                   = Products::search_spc($param);


        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();

        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('products')
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deal_details', $deal_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('product_name_single', $product_name_single)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general)
        ->with('search_max_min', $search_max_min)
        ->with('get_grade', $get_grade)
        ->with('spc_result', $spc_result)
        ->with('mc_id', $mc_id)
        ->with('smc_id', $smc_id)
        ->with('sb_id', $sb_id)
        ->with('ssb_id', $ssb_id);

    }

    public function productview($mcid, $scid, $sbid, $ssbid, $id)
    {
        $sid = base64_decode($id);
        //dd($sid);

        $breadcrumb                   = Home::get_breadcrumb_category($sid);
        $product_id                   = base64_decode($id);
        //dd($product_id);
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $product_details_by_id        = Home::get_product_details_by_id($product_id);
        // $promo_flash_time_by_id       = Home::promo_flash_time_by_id($product_id);
        //dd($promo_flash_time_by_id);
        //dd($product_details_by_id);
        $get_header_group             = Home::get_header_group($product_id);
        //dd($get_header_group);
        if($get_header_group != null || $get_header_group != "")
        {
            $get_all_same_group           = Home::get_same_group_model($get_header_group->id_grouping_product_model_header);
        }
        else
        {
            $get_all_same_group = "";
        }
        //dd($get_all_same_group);
        $product_color_details        = Home::get_selected_product_color_details($product_details_by_id);
        $product_size_details         = Home::get_selected_product_size_details($product_details_by_id);

        $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_product($product_id);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_countone($product_id);
        $two_count                    = Home::get_counttwo($product_id);
        $three_count                  = Home::get_countthree($product_id);
        $four_count                   = Home::get_countfour($product_id);
        $five_count                   = Home::get_countfive($product_id);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_prd_deatils($product_id);
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_grade                    = Home::get_grade_product($id);
        $get_metaproduct              = Home::get_metaproduct($sid);
        $get_store_name               = Home::get_store_name($product_id);
        //dd($product_details_by_id);

        // jumlah ratings
        $jumlah_ratings = ceil(Home::jumlah_ratings($product_id));

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('metaproduct', $get_metaproduct)->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productview')
        ->with('get_storename',$get_store_name)
        ->with('get_cur', $get_cur)
        ->with('get_grade',$get_grade)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('product_details_by_id', $product_details_by_id)
        ->with('get_related_product',$get_related_product)
        ->with('product_color_details',$product_color_details)
        ->with('product_size_details', $product_size_details)
        ->with('product_spec_details', $product_spec_details)
        ->with('metadetails', $getmetadetails)
        ->with('one_count', $one_count)
        ->with('two_count', $two_count)
        ->with('three_count', $three_count)
        ->with('four_count', $four_count)
        ->with('five_count', $five_count)
        ->with('customer_details', $customer_details)
        ->with('review_comments', $review_comments)
        ->with('get_store', $get_store)
        ->with('breadcrumb', $breadcrumb)
        ->with('get_contact_det', $get_contact_det)
        ->with('general',$general)
        ->with('mcid', $mcid)
        ->with('scid',$scid)
        ->with('sbid',$sbid)
        ->with('ssbid',$ssbid)
        ->with('jumlah_ratings', $jumlah_ratings)
        ->with('get_all_same_group', $get_all_same_group);
        //->with('promo_flash_time_by_id', $promo_flash_time_by_id);
    }

    public function productview_spesial($mcid, $scid, $sbid, $ssbid, $id, $nama_product)
    {
        $sid = base64_decode($id);

        $breadcrumb                   = Home::get_breadcrumb_category($sid);
        //dd($breadcrumb);
        $product_id                   = base64_decode($id);
        //dd($product_id);
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $product_details_by_id        = Home::get_product_details_by_id($product_id);
        // dd($product_details_by_id);
        $get_header_group             = Home::get_header_group($product_id);
        //dd($get_header_group);
        if($get_header_group != null || $get_header_group != "")
        {
            $get_all_same_group           = Home::get_same_group_model($get_header_group->id_grouping_product_model_header);
        }
        else
        {
            $get_all_same_group = "";
        }
        //dd($get_all_same_group);
        $product_color_details        = Home::get_selected_product_color_details($product_details_by_id);
        $product_size_details         = Home::get_selected_product_size_details($product_details_by_id);
        $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_product($product_id);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_countone($product_id);
        $two_count                    = Home::get_counttwo($product_id);
        $three_count                  = Home::get_countthree($product_id);
        $four_count                   = Home::get_countfour($product_id);
        $five_count                   = Home::get_countfive($product_id);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        //dd($customer_details);
        $get_store                    = Home::get_prd_deatils($product_id);
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_grade                    = Home::get_grade_product($id);
        $get_metaproduct              = Home::get_metaproduct($sid);
        $get_store_name               = Home::get_store_name($product_id);
        //dd($get_store_name);

        // jumlah ratings
        $jumlah_ratings = ceil(Home::jumlah_ratings($product_id));

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('metaproduct', $get_metaproduct)->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productview')
        ->with('get_storename',$get_store_name)
        ->with('get_cur', $get_cur)
        ->with('get_grade',$get_grade)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('product_details_by_id', $product_details_by_id)
        ->with('get_related_product',$get_related_product)
        ->with('product_color_details',$product_color_details)
        ->with('product_size_details', $product_size_details)
        ->with('product_spec_details', $product_spec_details)
        ->with('metadetails', $getmetadetails)
        ->with('one_count', $one_count)
        ->with('two_count', $two_count)
        ->with('three_count', $three_count)
        ->with('four_count', $four_count)
        ->with('five_count', $five_count)
        ->with('customer_details', $customer_details)
        ->with('review_comments', $review_comments)
        ->with('get_store', $get_store)
        ->with('breadcrumb', $breadcrumb)
        ->with('get_contact_det', $get_contact_det)
        ->with('general',$general)
        ->with('mcid', $mcid)
        ->with('scid',$scid)
        ->with('sbid',$sbid)
        ->with('ssbid',$ssbid)
        ->with('jumlah_ratings', $jumlah_ratings)
        ->with('get_all_same_group', $get_all_same_group);
    }

    public function product_flashPromo($mcid, $scid, $sbid, $ssbid, $id, $promopFlashId)
    {

        $sid = base64_decode($id);
        // dd($sid."|".$promopFlashId);
        // dd($promopFlashId);
        $breadcrumb                   = Home::get_breadcrumb_category($sid);
        $product_id                   = base64_decode($id);
        //dd($product_id);
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        //dd($product_details);
        $product_details_by_id        = Home::get_product_details_by_id($product_id);
        // dd($product_details_by_id);
        $promo_flash_time_by_id       = Home::promo_flash_time_by_id($product_id, $promopFlashId);
        // dd($promo_flash_time_by_id);
        // dd($promo_flash_time_by_id);
        $get_header_group             = Home::get_header_group($product_id);
        //dd($get_header_group);
        if($get_header_group != null || $get_header_group != "")
        {
            $get_all_same_group           = Home::get_same_group_model($get_header_group->id_grouping_product_model_header);
        }
        else
        {
            $get_all_same_group = "";
        }
        //dd($get_all_same_group);
        $product_color_details        = Home::get_selected_product_color_details($product_details_by_id);
        $product_size_details         = Home::get_selected_product_size_details($product_details_by_id);

        $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_product($product_id);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_countone($product_id);
        $two_count                    = Home::get_counttwo($product_id);
        $three_count                  = Home::get_countthree($product_id);
        $four_count                   = Home::get_countfour($product_id);
        $five_count                   = Home::get_countfive($product_id);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_prd_deatils($product_id);
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_grade                    = Home::get_grade_product($id);
        $get_metaproduct              = Home::get_metaproduct($sid);
        $get_store_name               = Home::get_store_name($product_id);
        //$promo_flash_time_by_id       = Home::promo_flash_time_by_id($product_id);
        //dd($promo_flash_time_by_id);
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productview')
        ->with('promopFlashId', $promopFlashId)
        ->with('product_id', $product_id)
        ->with('get_cur', $get_cur)
        ->with('get_grade',$get_grade)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('product_details_by_id', $product_details_by_id)
        ->with('get_related_product',$get_related_product)
        ->with('product_color_details',$product_color_details)
        ->with('product_size_details', $product_size_details)
        ->with('product_spec_details', $product_spec_details)
        ->with('metadetails', $getmetadetails)
        ->with('one_count', $one_count)
        ->with('two_count', $two_count)
        ->with('three_count', $three_count)
        ->with('four_count', $four_count)
        ->with('five_count', $five_count)
        ->with('customer_details', $customer_details)
        ->with('review_comments', $review_comments)
        ->with('get_store', $get_store)
        ->with('breadcrumb', $breadcrumb)
        ->with('get_contact_det', $get_contact_det)
        ->with('general',$general)
        ->with('mcid', $mcid)
        ->with('scid',$scid)
        ->with('sbid',$sbid)
        ->with('ssbid',$ssbid)
        ->with('get_all_same_group', $get_all_same_group)
        ->with('promo_flash_time_by_id', $promo_flash_time_by_id)
        ->with('get_storename', $get_store_name) ;
    }


    public function productcomment($mcid, $scid, $sbid, $ssbid, $id)
    {
        $sid = base64_decode($id);

        $breadcrumb                   = Home::get_breadcrumb_category($sid);
        $product_id                   = base64_decode($id);
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $product_details_by_id = DB::table('nm_product')
        ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->leftJoin('nm_color', 'nm_color.co_id', '=', 'nm_product.pro_color_id')
        ->leftJoin('nm_grade','nm_grade.grade_id','=','nm_product.pro_grade_id')
        ->where('pro_id', $sid)
        ->get();
        // $product_details_by_id        = Home::get_product_details_by_id($product_id);
        //dd($product_details_by_id);
        $get_header_group             = Home::get_header_group($product_id);
        //dd($get_header_group);
        if($get_header_group != null || $get_header_group != "")
        {
            $get_all_same_group           = Home::get_same_group_model($get_header_group->id_grouping_product_model_header);
        }
        else
        {
            $get_all_same_group = "";
        }
        //dd($get_all_same_group);
        $product_color_details        = Home::get_selected_product_color_details($product_details_by_id);
        $product_size_details         = Home::get_selected_product_size_details($product_details_by_id);
        $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_product($product_id);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_countone($product_id);
        $two_count                    = Home::get_counttwo($product_id);
        $three_count                  = Home::get_countthree($product_id);
        $four_count                   = Home::get_countfour($product_id);
        $five_count                   = Home::get_countfive($product_id);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_prd_deatils($product_id);
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        //dd($product_details_by_id);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productcomment')
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('product_details_by_id', $product_details_by_id)
        ->with('get_related_product',$get_related_product)
        ->with('product_color_details',$product_color_details)
        ->with('product_size_details', $product_size_details)
        ->with('product_spec_details', $product_spec_details)
        ->with('metadetails', $getmetadetails)
        ->with('one_count', $one_count)
        ->with('two_count', $two_count)
        ->with('three_count', $three_count)
        ->with('four_count', $four_count)
        ->with('five_count', $five_count)
        ->with('customer_details', $customer_details)
        ->with('review_comments', $review_comments)
        ->with('get_store', $get_store)
        ->with('breadcrumb', $breadcrumb)
        ->with('get_contact_det', $get_contact_det)
        ->with('general',$general)
        ->with('mcid', $mcid)
        ->with('scid',$scid)
        ->with('sbid',$sbid)
        ->with('ssbid',$ssbid)
        ->with('get_all_same_group', $get_all_same_group);
    }

    public function productview1($mcid, $scid, $sbid, $id)
    {

        $sid = base64_decode($id);

        $product_id                   = base64_decode($id);
        $breadcrumb                   = Home::get_breadcrumb_category($sid);
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details1();
        // dd($product_details);
        $product_details_by_id        = Home::get_product_details_by_id($product_id);

        $product_color_details        = Home::get_selected_product_color_details($product_details_by_id);
        $product_size_details         = Home::get_selected_product_size_details($product_details_by_id);
        $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_product($sid);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_countone($product_id);
        $two_count                    = Home::get_counttwo($product_id);
        $three_count                  = Home::get_countthree($product_id);
        $four_count                   = Home::get_countfour($product_id);
        $five_count                   = Home::get_countfive($product_id);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_prd_deatils($product_id);
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productview')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('product_details_by_id', $product_details_by_id)->with('get_related_product', $get_related_product)->with('product_color_details', $product_color_details)->with('product_size_details', $product_size_details)->with('product_spec_details', $product_spec_details)->with('metadetails', $getmetadetails)->with('one_count', $one_count)->with('two_count', $two_count)->with('three_count', $three_count)->with('four_count', $four_count)->with('five_count', $five_count)->with('customer_details', $customer_details)->with('review_comments', $review_comments)->with('get_store', $get_store)->with('breadcrumb', $breadcrumb)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }

    public function productview2($mcid, $scid, $id)
    {

        $sid                          = base64_decode($id);
        $product_id                   = base64_decode($id);
        $breadcrumb                   = Home::get_breadcrumb_category($sid);
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details2();

        $product_details_by_id        = Home::get_product_details_by_id($product_id);

        $product_color_details        = Home::get_selected_product_color_details($product_details_by_id);
        $product_size_details         = Home::get_selected_product_size_details($product_details_by_id);
        $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_product($sid);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_store                    = Home::get_prd_deatils($product_id);
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('productview')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('product_details_by_id', $product_details_by_id)->with('get_related_product', $get_related_product)->with('product_color_details', $product_color_details)->with('product_size_details', $product_size_details)->with('product_spec_details', $product_spec_details)->with('metadetails', $getmetadetails)->with('one_count', $one_count)->with('two_count', $two_count)->with('three_count', $three_count)->with('four_count', $four_count)->with('five_count', $five_count)->with('customer_details', $customer_details)->with('review_comments', $review_comments)->with('get_store', $get_store)->with('breadcrumb', $breadcrumb)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }


    public function deals()
    {
        date_default_timezone_set('Asia/Jakarta');
        $today = date('Y-m-d H:i:s');

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_all_deals_details();
        $country_details              = Register::get_country_details();
        $most_visited_product         = Home::get_deals_details();
        $get_special_product          = Home::get_left_side_special_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        // $get_promo                    = DB::table('nm_promo_products')
        // ->leftJoin('nm_scheduled_promo', 'nm_scheduled_promo.schedp_id', '=', 'nm_promo_products.promop_schedp_id')
        // ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_promo_products.promop_pro_id')
        // ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        // ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        // ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        // ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        // ->where('schedp_simple_action', '!=', 'flash_promo')
        // ->where('schedp_end_date', '>', $today)
        // ->where('schedp_status', 1)
        // ->where('schedp_simple_free_shipping', 0)
        // ->where('schedp_coupon_type', 2)
        // ->where('pro_status', '=', 1)
        // ->where('pro_isapproved', 1)
        // ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
        // ->where('mc_status', 1)
        // ->where('smc_status', 1)
        // ->where('sb_status', 1)
        // ->where('ssb_status', 1)
        // ->get();
        $get_promo = DB::table('nm_scheduled_promo')
        ->leftJoin('nm_promo_coupons', 'nm_promo_coupons.promoc_schedp_id', '=', 'nm_scheduled_promo.schedp_id')
        ->where('schedp_simple_action', '!=', 'flash_promo')
        ->where('schedp_end_date', '>', $today)
        ->where('schedp_start_date', '<=', $today)
        ->where('schedp_status', 1)
        ->where('jumlah_voucher', 0)
        ->get();
        // dd($get_promo);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('deals')->with('get_promo', $get_promo)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('get_cur', $get_cur)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('metadetails', $getmetadetails)->with('get_special_product', $get_special_product)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function turun_harga()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details_turun_harga();
        $deal_details                 = array();
        $auction_details              = array();
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        //dd($product_details);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')
            ->with('country_details', $country_details)
            ->with('metadetails', $getmetadetails)
            ->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('turun_harga')
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deal_details', $deal_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);
    }

    public function category_deal_list($name, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $today = date('Y-m-d H:i:s');
        $product_id          = base64_decode($id);
        $getmetadetails      = Home::getmetadetails();
        $city_details        = Register::get_city_details();
        $header_category     = Home::get_header_category();
        $get_special_product = Home::get_left_side_special_product();
        $country_details     = Register::get_country_details();
        if ($name == "viewcategorylist") {
            $get_cat_name_listby = Home::get_catname_listby($product_id);
            $product_details     = Home::get_category_deal_details_listby($product_id);

            $get_listby_id       = explode(",", $product_id);
            //dd($get_listby_id);
            foreach ($get_cat_name_listby as $get_cat_name_listby_single) {
            }
            if ($get_listby_id[0] == 1) {
                $product_name_single = $get_cat_name_listby_single->mc_name;
            } else if ($get_listby_id[0] == 2) {
                $product_name_single = $get_cat_name_listby_single->smc_name;
            } else if ($get_listby_id[0] == 3) {
                $product_name_single = $get_cat_name_listby_single->sb_name;
            } else if ($get_listby_id[0] == 4) {
                $product_name_single = $get_cat_name_listby_single->ssb_name;
            }

        } else {
            $product_details     = Home::get_all_deals_details($id);
            $product_name_single = "";
        }


        $most_visited_product         = Home::get_deals_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_promo                    = DB::table('nm_promo_products')
        ->leftJoin('nm_scheduled_promo', 'nm_scheduled_promo.schedp_id', '=', 'nm_promo_products.promop_schedp_id')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_promo_products.promop_pro_id')
        ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->where('pro_mc_id', '=', $get_listby_id[1])
        ->where('schedp_end_date', '>', $today)
        ->where('schedp_status', 1)
        ->where('schedp_simple_free_shipping', 0)
        ->where('schedp_coupon_type', 2)
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', 1)
        ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
        ->where('mc_status', 1)
        ->where('smc_status', 1)
        ->where('sb_status', 1)
        ->where('ssb_status', 1)
        ->get();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('deals')->with('get_promo', $get_promo)->with('get_cur', $get_cur)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('metadetails', $getmetadetails)->with('get_special_product', $get_special_product)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function dealview($id)
    {
        $sid                          = base64_decode($id);
        $city_details                 = Register::get_city_details();
        $breadcrumb                   = Home::get_breadcrumb_deal($sid);
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        // $product_details_by_id        = Home::get_deals_details_by_id($sid);
        // $product_spec_details         = Home::get_selected_product_spec_details($product_details_by_id);
        $get_related_product          = Home::get_related_deals($sid);
        $country_details              = Register::get_country_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_dealcountone($sid);
        $two_count                    = Home::get_dealcounttwo($sid);
        $three_count                  = Home::get_dealcountthree($sid);
        $four_count                   = Home::get_dealcountfour($sid);
        $five_count                   = Home::get_dealcountfive($sid);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_deal_deatils($sid);
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $promo_product = DB::table('nm_scheduled_promo')
        ->leftJoin('nm_promo_coupons', 'nm_promo_coupons.promoc_schedp_id', '=', 'nm_scheduled_promo.schedp_id')
        ->leftJoin('nm_promo_products', 'nm_promo_products.promop_schedp_id', '=', 'nm_scheduled_promo.schedp_id')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_promo_products.promop_pro_id')
        ->where('schedp_id', $sid)
        ->get();
        // dd($promo_product);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('dealview')
        ->with('promo_product', $promo_product)
        ->with('get_cur', $get_cur)
        // ->with('product_spec_details', $product_spec_details)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        // ->with('product_details_by_id', $product_details_by_id)
        ->with('get_related_product', $get_related_product)
        ->with('metadetails', $getmetadetails)
        ->with('breadcrumb', $breadcrumb)
        ->with('one_count', $one_count)
        ->with('two_count', $two_count)
        ->with('three_count', $three_count)
        ->with('four_count', $four_count)
        ->with('five_count', $five_count)
        ->with('customer_details', $customer_details)
        ->with('review_comments', $review_comments)
        ->with('get_store', $get_store)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);
    }

    public function dealview1($mcid, $scid, $sbid, $id)
    {
        $sid                          = base64_decode($id);
        $city_details                 = Register::get_city_details();
        $breadcrumb                   = Home::get_breadcrumb_deal($sid);
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $product_details_by_id        = Home::get_deals_details_by_id($sid);

        $get_related_product          = Home::get_related_deals($sid);
        $country_details              = Register::get_country_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_dealcountone($sid);
        $two_count                    = Home::get_dealcounttwo($sid);
        $three_count                  = Home::get_dealcountthree($sid);
        $four_count                   = Home::get_dealcountfour($sid);
        $five_count                   = Home::get_dealcountfive($sid);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_deal_deatils($sid);
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);


        return view('dealview')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('product_details_by_id', $product_details_by_id)->with('get_related_product', $get_related_product)->with('metadetails', $getmetadetails)->with('breadcrumb', $breadcrumb)->with('one_count', $one_count)->with('two_count', $two_count)->with('three_count', $three_count)->with('four_count', $four_count)->with('five_count', $five_count)->with('customer_details', $customer_details)->with('review_comments', $review_comments)->with('get_store', $get_store)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }

    public function dealview2($mcid, $scid, $id)
    {
        $sid                          = base64_decode($id);
        $city_details                 = Register::get_city_details();
        $breadcrumb                   = Home::get_breadcrumb_deal($sid);
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $product_details_by_id        = Home::get_deals_details_by_id($sid);

        $get_related_product          = Home::get_related_deals($sid);
        $country_details              = Register::get_country_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $one_count                    = Home::get_dealcountone($sid);
        $two_count                    = Home::get_dealcounttwo($sid);
        $three_count                  = Home::get_dealcountthree($sid);
        $four_count                   = Home::get_dealcountfour($sid);
        $five_count                   = Home::get_dealcountfive($sid);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_deal_deatils($sid);
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);


        return view('dealview')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('product_details_by_id', $product_details_by_id)->with('get_related_product', $get_related_product)->with('metadetails', $getmetadetails)->with('breadcrumb', $breadcrumb)->with('one_count', $one_count)->with('two_count', $two_count)->with('three_count', $three_count)->with('four_count', $four_count)->with('five_count', $five_count)->with('customer_details', $customer_details)->with('review_comments', $review_comments)->with('get_store', $get_store)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }

    public function auction()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_all_action_details();
        $most_visited_product         = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $get_auction_last_bidder      = Home::auction_last_bidder($product_details);
        $get_auction_last_bidder1     = Home::auction_last_bidder($most_visited_product);
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('getanl', $getanl);

        return view('auction')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('get_auction_last_bidder', $get_auction_last_bidder)->with('get_auction_last_bidder1', $get_auction_last_bidder1)->with('metadetails', $getmetadetails)->with('general', $general);


    }


    public function category_auction_list($name, $id)
    {

        $product_id = base64_decode($id);
        $id         = base64_decode(base64_decode(base64_decode($id)));

        $city_details    = Register::get_city_details();
        $header_category = Home::get_header_category();
        $country_details = Register::get_country_details();
        if ($name == "viewcategorylist") {
            $get_cat_name_listby = Home::get_catname_listby($product_id);
            $product_details     = Home::get_category_auction_details_listby($product_id);

            $get_listby_id = explode(",", $product_id);
            foreach ($get_cat_name_listby as $get_cat_name_listby_single) {
            }
            if ($get_listby_id[0] == 1) {
                $product_name_single = $get_cat_name_listby_single->mc_name;
            } else if ($get_listby_id[0] == 2) {
                $product_name_single = $get_cat_name_listby_single->smc_name;
            } else if ($get_listby_id[0] == 3) {
                $product_name_single = $get_cat_name_listby_single->sb_name;
            } else if ($get_listby_id[0] == 4) {
                $product_name_single = $get_cat_name_listby_single->ssb_name;
            }

        } else {
            $product_details     = Home::get_all_action_details($id);
            $product_name_single = "";
        }

        $get_auction_last_bidder      = Home::auction_last_bidder($product_details);
        $most_visited_product         = Home::get_auction_details();
        $get_auction_last_bidder1     = Home::auction_last_bidder($most_visited_product);
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('getanl', $getanl);

        return view('auction')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('get_auction_last_bidder', $get_auction_last_bidder)->with('metadetails', $getmetadetails)->with('get_auction_last_bidder1', $get_auction_last_bidder1)->with('general', $general);


    }


    public function auctionview($id)
    {

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $product_details_by_id        = Home::get_action_details_by_id($id);
        $country_details              = Register::get_country_details();
        $get_related_product          = Home::get_related_auction($id);
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $get_max_bid_amt              = Home::max_bid_amt($id);
        $get_bidder_by_id             = Home::get_bidder_by_id($id);
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('getanl', $getanl);

        return view('auctionview')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('product_details_by_id', $product_details_by_id)->with('get_related_product', $get_related_product)->with('get_max_bid_amt', $get_max_bid_amt)->with('get_bidder_by_id', $get_bidder_by_id)->with('metadetails', $getmetadetails)->with('general', $general);
    }

    public function stores()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $most_visited_product         = Home::get_most_visited_product();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        $get_store_details       = Home::get_store_list();
        $get_store_deal_count    = Home::get_store_deal_count($get_store_details);
        $get_store_auction_count = Home::get_store_auction_count($get_store_details);
        $get_store_product_count = Home::get_store_product_count($get_store_details);

        return view('stores')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('get_store_details', $get_store_details)->with('get_store_deal_count', $get_store_deal_count)->with('get_store_auction_count', $get_store_auction_count)->with('get_store_product_count', $get_store_product_count)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function storeview($id)
    {


        $id                   = base64_decode(base64_decode(base64_decode($id)));
        $city_details         = Register::get_city_details();
        $header_category      = Home::get_header_category();
        $product_name_single  = "";
        $most_visited_product = Home::get_auction_details();

        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $get_store_by_id              = Home::get_store_by_id($id);
        $get_store_deal_by_id         = Home::get_store_deal_by_id($id);
        $get_store_auction_by_id      = Home::get_store_auction_by_id($id);
        $get_auction_last_bidder      = Home::auction_last_bidder($get_store_auction_by_id);
        $get_store_product_by_id      = Home::get_store_product_by_id($id);
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_storebranch              = Home::get_store_sub_details($id);
        $one_count                    = Home::get_storecountone($id);
        $two_count                    = Home::get_storecounttwo($id);
        $three_count                  = Home::get_storecountthree($id);
        $four_count                   = Home::get_storecountfour($id);
        $five_count                   = Home::get_storecountfive($id);
        $customer_details             = Home::get_customer_details();
        $review_comments              = Home::get_review_details();
        $get_store                    = Home::get_store_deatils($id);
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('storeview')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('get_store_deal_by_id', $get_store_deal_by_id)->with('get_store_auction_by_id', $get_store_auction_by_id)->with('get_store_product_by_id', $get_store_product_by_id)->with('get_store_by_id', $get_store_by_id)->with('metadetails', $getmetadetails)->with('get_auction_last_bidder', $get_auction_last_bidder)->with('get_storebranch', $get_storebranch)->with('one_count', $one_count)->with('two_count', $two_count)->with('three_count', $three_count)->with('four_count', $four_count)->with('five_count', $five_count)->with('customer_details', $customer_details)->with('review_comments', $review_comments)->with('get_store', $get_store)->with('get_contact_det', $get_contact_det)->with('general', $general);


    }

    public function sold()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_name_single          = "";
        $most_visited_product         = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $get_store_deal_by_id         = Home::get_sold_deal_by_id();

        $get_store_auction_by_id      = Home::get_sold_auction_by_id();
        $get_store_product_by_id      = Home::get_sold_product_by_id();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        //        dd($get_store_product_by_id);

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('sold')->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('get_store_deal_by_id', $get_store_deal_by_id)
        ->with('get_store_auction_by_id', $get_store_auction_by_id)
        ->with('get_store_product_by_id', $get_store_product_by_id)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);


    }

    public function category_list($id)
    {
        $id                   = base64_decode($id);
        $city_details         = Register::get_city_details();
        $header_category      = Home::get_header_category();
        $product_details      = Home::get_product_details_use_catid($id);
        $most_visited_product = Home::get_most_visited_product();
        $deals_details        = Home::get_deals_details_use_catid($id);

        $auction_details              = Home::get_auction_details_use_catid($id);
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();

        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $get_auction_last_bidder      = Home::auction_last_bidder($auction_details);
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('category_list')->with('get_cur',$get_cur)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('deals_details', $deals_details)->with('auction_details', $auction_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('get_auction_last_bidder', $get_auction_last_bidder)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }


    public function facebooklogin()
    {

        $fb_id = Input::get('fid');

        $data  = array(
            'cus_name' => Input::get('name'),
            'cus_email' => Input::get('email'),
            'facebook_id' => Input::get('fid'),
            'cus_logintype' => 3
        );

        return Home::facebook_login_check($fb_id, $data);
    }

    public function autosearch(Request $request)
    {
        $get_product = DB::table('nm_product')
        ->where('pro_title', 'like', '%'.$request->input('term').'%')
        ->select('pro_title')
        ->get();

        $return_product = [];

        foreach ($get_product as $product) {
            $array = [
                'value' => $product->pro_title,
                'label' => $product->pro_title
            ];
            array_push($return_product, $array);
        }

        return $return_product;

        // $q = $_GET['searchword'];
        // if ($q != "") {
        //     $header_category              = Home::get_autosearch_category($q);
        //     $header_category_get          = Home::get_header_category();
        //     $category_count               = Home::get_category_count($header_category_get);
        //     $get_product_details_typeahed = Home::get_product_details_autosearch($q);
        //     $get_cat_out                  = "";
        //     $general                      = Home::get_general_settings();
        //     foreach ($header_category as $header_categ) {
        //         $count = $category_count[$header_categ->mc_id];
        //         $get_cat_out .= '<a href="' . url('category_list/' . base64_encode($header_categ->mc_id)) . '" style="cursor:pointer;" >' . $header_categ->mc_name . '</a>' . '(' . $count . ')' . '<br/>';
        //     }
        //     $final_typeahed_result_one = $get_cat_out;
        //
        //     if ($get_product_details_typeahed) {
        //         $final_typeahed_result_three = '=== Special Products ===';
        //     } else {
        //         $final_typeahed_result_three = '';
        //     }
        //     $get_product_out = "";
        //
        //     foreach ($get_product_details_typeahed as $product_typeahed) {
        //         if ($product_typeahed->pro_no_of_purchase < $product_typeahed->pro_qty) {
        //             if($product_typeahed->pro_disprice==0){
        //                 $typeahed_price = $product_typeahed->pro_price;
        //             }else {
        //                 $typeahed_price = $product_typeahed->pro_disprice;
        //             }
        //             $pro_type_img = explode('/**/', $product_typeahed->pro_Img);
        //             $href         = url('productview/' . $product_typeahed->pro_id);
        //             $get_product_out .= '<div class="display_box" align="left"><table><tr><td><img src="' . url('') . '/assets/product/' . $pro_type_img[0] . '" alt="" height="100" width="70" ></td><td width="5"> </td><td><table><tr> <td>' . substr($product_typeahed->pro_title, 0, 25) . '...<br> Rp' . number_format($typeahed_price, 0, '.', ',') . '<br><a href="' . $href . '" class="btn align_brn icon_me" style="width:60px; height:50px;" href="">Add To Cart</a> </td> </tr> </table> </td></tr></table> </div>.............................................';
        //         }
        //     }
        //
        //     $final_typeahed_result_two = $get_product_out;
        //     $final_result              = $final_typeahed_result_one . $final_typeahed_result_three . $final_typeahed_result_two;
        //     if ($final_result == "") {
        //         echo $final_typeahed_result = '<div class="display_box" align="left"> No Result Found.. </div>';
        //     } else {
        //         echo $final_typeahed_result = '<b><div class="display_box"  align="left">' . $final_typeahed_result_one . $final_typeahed_result_three . $final_typeahed_result_two . "</div></b>";
        //     }
        // } else {
        //     echo "";
        // }

    }

    public function cart()
    {
        $result_cart  = Home::get_add_to_cart_details();
        //dd($result_cart);
        $size_result  = Home::get_add_to_cart_size();
        $color_result = Home::get_add_to_cart_color();
        if (isset($_SESSION['deal_cart'])) {
            $result_cart_deal = Home::get_add_to_cart_deal_details();
        } else {
            $result_cart_deal = "";
        }
        $country_details              = Register::get_country_details();
        $most_visited_product         = Home::get_most_visited_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $session_result               = '';
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails)->with('country_details', $country_details);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);


        return view('cart')
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('most_visited_product', $most_visited_product)
        ->with('result_cart', $result_cart)
        ->with('size_result', $size_result)
        ->with('color_result', $color_result)
        ->with('session_result', $session_result)
        ->with('page', "")
        ->with('result_cart_deal', $result_cart_deal)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);
    }

    public function cart_flash()
    {
        $result_cart  = Home::get_add_to_cart_details();
        // dd($result_cart);
        $size_result  = Home::get_add_to_cart_size();
        $color_result = Home::get_add_to_cart_color();
        if (isset($_SESSION['deal_cart'])) {
            $result_cart_deal = Home::get_add_to_cart_deal_details();
        } else {
            $result_cart_deal = "";
        }
        $country_details              = Register::get_country_details();
        $most_visited_product         = Home::get_most_visited_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $session_result               = '';
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails)->with('country_details', $country_details);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);


        return view('cart')
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('most_visited_product', $most_visited_product)
        ->with('result_cart', $result_cart)
        ->with('size_result', $size_result)
        ->with('color_result', $color_result)
        ->with('session_result', $session_result)
        ->with('page', "")
        ->with('result_cart_deal', $result_cart_deal)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);
    }

    public function addtocart_flash(){

      $cart_id    = Input::get('addtocart_promop_pro_id');
        $cart_qty   = Input::get('addtocart_qty');
        $cart_color = Input::get('addtocart_color');
        $cart_size  = Input::get('addtocart_size');
        $cart_type  = Input::get('addtocart_type');
        $return_url = Input::get('return_url');
        $flashPromoId = Input::get('addtocart_flashPromoId');
        //dd($flashPromoId);
        //$promop_pro_id = Input::get('addtocart_promop_pro_id');

        //dd($flashPromoId);

        //--
        $cart_weight = Input::get('addtocart_weight');
        $cart_length = Input::get('addtocart_length');
        $cart_width = Input::get('addtocart_width');
        $cart_height = Input::get('addtocart_height');

        //--
        $cart_color = Input::get('addtocart_color');
        $cart_grade = Input::get('addtocart_grade');

        if ($cart_id < 1 or $cart_qty < 1) {
            return;
        }
        //dd($_SESSION['cart']);
        if (isset($_SESSION['cart'])) {
            //dd($_SESSION['cart']);
            $check_product = Home::get_added_product_details($cart_id, $cart_color, $cart_size);
            //dd($check_product);
            // dd($check_product);
            if ($check_product == 0) {
                //dd($check_product);
                $max                                 = count($_SESSION['cart']);
                $_SESSION['cart'][$max]['productid'] = $cart_id;
                $_SESSION['cart'][$max]['flashPromoId']      = $flashPromoId;
                 $_SESSION['cart'][$max]['flashPromoId_temp']      = $flashPromoId;
                $_SESSION['cart'][$max]['qty']       = $cart_qty;
                $_SESSION['cart'][$max]['color']     = $cart_color;
                $_SESSION['cart'][$max]['size']      = $cart_size;
                $_SESSION['cart'][$max]['type']      = $cart_type;

                $_SESSION['cart'][$max]['weight']      = $cart_weight;
                $_SESSION['cart'][$max]['length']      = $cart_length;
                $_SESSION['cart'][$max]['width']      = $cart_width;
                $_SESSION['cart'][$max]['height']      = $cart_height;

                $_SESSION['cart'][$max]['color']      = $cart_color;
                $_SESSION['cart'][$max]['grade']      = $cart_grade;
                // $_SESSION['cart'][$max]['flashPromoId']      = $flashPromoId;
                // $_SESSION['cart'][$max]['promop_pro_id']      = $promop_pro_id;
                // dd($_SESSION['cart'][$max]['promop_pro_id']);
                $session_result                      = '';
            } else {
                $session_result = Home::get_already_product_details($cart_id);
                // dd($session_result);
                //dd($_SESSION['cart']);
            }

        } else {

            //session_start();

            $_SESSION['cart']                 = array();
            $_SESSION['cart'][0]['productid'] = $cart_id;
            $_SESSION['cart'][0]['flashPromoId']      = $flashPromoId;
             $_SESSION['cart'][0]['flashPromoId_temp']      = $flashPromoId;
            $_SESSION['cart'][0]['qty']       = $cart_qty;
            $_SESSION['cart'][0]['color']     = $cart_color;
            $_SESSION['cart'][0]['size']      = $cart_size;
            $_SESSION['cart'][0]['type']      = $cart_type;

            $_SESSION['cart'][0]['weight']      = $cart_weight;
            $_SESSION['cart'][0]['length']      = $cart_length;
            $_SESSION['cart'][0]['width']      = $cart_width;
            $_SESSION['cart'][0]['height']      = $cart_height;

            $_SESSION['cart'][0]['color']      = $cart_color;
            $_SESSION['cart'][0]['grade']      = $cart_grade;

            //$_SESSION['cart'][0]['promop_pro_id']      = $promop_pro_id;
            //dd($_SESSION['cart']);
            $session_result                   = '';

        }


        if (isset($_SESSION['deal_cart'])) {
            //dd($_SESSION['deal_cart']);
            $result_cart_deal = Home::get_add_to_cart_deal_details();
        } else {
            $result_cart_deal = "";
        }
        //dd($result_cart_deal);

        $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        if ($cart_type == "product") {
            $size_result  = Home::get_add_to_cart_size();
            $color_result = Home::get_add_to_cart_color();
        } else {
            $size_result  = '';
            $color_result = '';
        }

        $result_cart = Home::get_add_to_cart_details();
        //dd($result_cart);

        //dd($result_cart);
       // $promo_flash_time_by_id       = Home::promo_flash_time_by_id($promop_pro_id, $flashPromoId);
        $country_details              = Register::get_country_details();
        $most_visited_product         = Home::get_most_visited_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
         $general                      = Home::get_general_settings();
         $get_pay                      = Settings::get_pay_settings();
         $get_cur                      = $get_pay[0]->ps_cursymbol;
         $get_pm_img_details           = Footer::get_paymentmethod_img_details();


        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('cart')
        ->with('get_cur', $get_cur)
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('most_visited_product', $most_visited_product)
        ->with('result_cart', $result_cart)
        ->with('size_result', $size_result)
        ->with('color_result', $color_result)
        ->with('session_result', $session_result)
        ->with('page', "")
        ->with('result_cart_deal', $result_cart_deal)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general)
        ->with('flashPromoId', $flashPromoId);
        //->with('promo_flash_time_by_id', $promo_flash_time_by_id);

    }

    public function add_to_cart()
    {

        $cart_id    = Input::get('addtocart_pro_id');
        $cart_qty   = Input::get('addtocart_qty');
        $cart_color = Input::get('addtocart_color');
        $cart_size  = Input::get('addtocart_size');
        $cart_type  = Input::get('addtocart_type');
        $return_url = Input::get('return_url');

        //--
        $cart_weight = Input::get('addtocart_weight');
        $cart_length = Input::get('addtocart_length');
        $cart_width = Input::get('addtocart_width');
        $cart_height = Input::get('addtocart_height');

        //--
        $cart_color = Input::get('addtocart_color');
        $cart_grade = Input::get('addtocart_grade');

        if ($cart_id < 1 or $cart_qty < 1) {
            return;
        }

        if (isset($_SESSION['cart'])) {

            $check_product = Home::get_added_product_details($cart_id, $cart_color, $cart_size);

            if ($check_product == 0) {
                $max                                 = count($_SESSION['cart']);
                $_SESSION['cart'][$max]['productid'] = $cart_id;
                $_SESSION['cart'][$max]['qty']       = $cart_qty;
                $_SESSION['cart'][$max]['color']     = $cart_color;
                $_SESSION['cart'][$max]['size']      = $cart_size;
                $_SESSION['cart'][$max]['type']      = $cart_type;

                $_SESSION['cart'][$max]['weight']      = $cart_weight;
                $_SESSION['cart'][$max]['length']      = $cart_length;
                $_SESSION['cart'][$max]['width']      = $cart_width;
                $_SESSION['cart'][$max]['height']      = $cart_height;

                $_SESSION['cart'][$max]['color']      = $cart_color;
                $_SESSION['cart'][$max]['grade']      = $cart_grade;

                $session_result                      = '';
            } else {
                $session_result = Home::get_already_product_details($cart_id);
            }

        } else {
            //session_start();
            $_SESSION['cart']                 = array();
            $_SESSION['cart'][0]['productid'] = $cart_id;
            $_SESSION['cart'][0]['qty']       = $cart_qty;
            $_SESSION['cart'][0]['color']     = $cart_color;
            $_SESSION['cart'][0]['size']      = $cart_size;
            $_SESSION['cart'][0]['type']      = $cart_type;

            $_SESSION['cart'][0]['weight']      = $cart_weight;
            $_SESSION['cart'][0]['length']      = $cart_length;
            $_SESSION['cart'][0]['width']      = $cart_width;
            $_SESSION['cart'][0]['height']      = $cart_height;

            $_SESSION['cart'][0]['color']      = $cart_color;
            $_SESSION['cart'][0]['grade']      = $cart_grade;

            $session_result                   = '';

        }


        if (isset($_SESSION['deal_cart'])) {
            $result_cart_deal = Home::get_add_to_cart_deal_details();
        } else {
            $result_cart_deal = "";
        }
        $result_cart = Home::get_add_to_cart_details();
        //dd($result_cart);
        if ($cart_type == "product") {
            $size_result  = Home::get_add_to_cart_size();
            $color_result = Home::get_add_to_cart_color();
        } else {
            $size_result  = '';
            $color_result = '';
        }
        $country_details              = Register::get_country_details();
        $most_visited_product         = Home::get_most_visited_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();


        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('cart')->with('get_cur', $get_cur)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('most_visited_product', $most_visited_product)->with('result_cart', $result_cart)->with('size_result', $size_result)->with('color_result', $color_result)->with('session_result', $session_result)->with('page', "")->with('result_cart_deal', $result_cart_deal)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }

    public function remove_session_cart_data()
    {
        //session_start();
        $pid = intval($_GET['id']);

        $max = count($_SESSION['cart']);
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['cart'][$i]['productid']) {
                unset($_SESSION['cart'][$i]);
                break;
            }
        }
        $_SESSION['cart'] = array_values($_SESSION['cart']);
    }

    public function set_quantity_session_cart()
    {

        $pid = intval($_GET['pid']);
        $qty = intval($_GET['id']);
        //session_start();
        $max = count($_SESSION['cart']);
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['cart'][$i]['productid']) {
                $_SESSION['cart'][$i]['qty'] = $qty;
                break;
            }
        }
        $_SESSION['cart'] = array_values($_SESSION['cart']);
    }

    public function remove_session_dealcart_data()
    {

        $pid = intval($_GET['id']);
        $max = count($_SESSION['deal_cart']);
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['deal_cart'][$i]['productid']) {
                unset($_SESSION['deal_cart'][$i]);
                break;
            }
        }
        $_SESSION['deal_cart'] = array_values($_SESSION['deal_cart']);
    }

    public function set_quantity_session_dealcart()
    {

        $pid = intval($_GET['pid']);
        $qty = intval($_GET['id']);
        $max = count($_SESSION['deal_cart']);
        for ($i = 0; $i < $max; $i++) {
            if ($pid == $_SESSION['deal_cart'][$i]['productid']) {
                $_SESSION['deal_cart'][$i]['qty'] = $qty;
                break;
            }
        }
        $_SESSION['deal_cart'] = array_values($_SESSION['deal_cart']);
    }

    public function deal_cart()
    {
        $result_cart  = Home::get_add_to_cart_details();
        $size_result  = Home::get_add_to_cart_size();
        $color_result = Home::get_add_to_cart_color();
        if (isset($_SESSION['deal_cart'])) {
            $result_cart_deal = Home::get_add_to_cart_deal_details();
        } else {
            $result_cart_deal = "";
        }
        $country_details              = Register::get_country_details();
        $size_result                  = Home::get_add_to_cart_size();
        $color_result                 = Home::get_add_to_cart_color();
        $most_visited_product         = Home::get_most_visited_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $session_result               = '';
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);


        return view('cart')->with('get_cur', $get_cur)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('most_visited_product', $most_visited_product)->with('result_cart', $result_cart)->with('size_result', $size_result)->with('color_result', $color_result)->with('session_result', $session_result)->with('page', "deals")->with('result_cart_deal', $result_cart_deal)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function add_to_cart_deal()
    {
        $cart_id    = Input::get('addtocart_deal_id');
        $cart_qty   = Input::get('addtocart_qty');
        $cart_type  = Input::get('addtocart_type');
        $return_url = Input::get('return_url');
        // dd($cart_id.','.$cart_qty.','.$cart_type.','.$return_url);
        if ($cart_id < 1 or $cart_qty < 1) {
            return;
        }

        if (isset($_SESSION['deal_cart'])) {
            $check_product = Home::get_added_deal_details($cart_id);
            // dd($check_product);
            if ($check_product == 0) {
                $max                                      = count($_SESSION['deal_cart']);
                $_SESSION['deal_cart'][$max]['productid'] = $cart_id;
                $_SESSION['deal_cart'][$max]['qty']       = $cart_qty;
                $_SESSION['deal_cart'][$max]['type']      = $cart_type;

                $session_result = '';
            } else {
                $session_result = Home::get_already_deals_details($cart_id);
                // dd($session_result);
            }

        } else {
            $_SESSION['deal_cart']                 = array();
            $_SESSION['deal_cart'][0]['productid'] = $cart_id;
            $_SESSION['deal_cart'][0]['qty']       = $cart_qty;
            $_SESSION['deal_cart'][0]['type']      = $cart_type;
            $session_result                        = '';

        }


        $result_cart_deal = Home::get_add_to_cart_deal_details();
        // dd($result_cart_deal);
        if (isset($_SESSION['cart'])) {
            $result_cart = Home::get_add_to_cart_details();
            // dd($result_cart);
        } else {
            $result_cart = "";
        }
        $country_details              = Register::get_country_details();
        $size_result                  = Home::get_add_to_cart_size();
        $color_result                 = Home::get_add_to_cart_color();
        $most_visited_product         = Home::get_most_visited_product();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('cart')->with('get_cur', $get_cur)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('most_visited_product', $most_visited_product)->with('result_cart_deal', $result_cart_deal)->with('session_result', $session_result)->with('page', "deals")->with('result_cart', $result_cart)->with('size_result', $size_result)->with('color_result', $color_result)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }

    public function checkout_auction()
    {
        $country_details              = Register::get_country_details();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_all_deals_details();
        $auction_details              = Home::get_all_action_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $get_image_favicons_details   = Home::get_image_favicons_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('checkout_auction')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('product_details', $product_details)->with('deals_details', $deals_details)->with('auction_details', $auction_details)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function get_paymentmethod_image(Request $request)
    {
        $return = DB::table('nm_paymentmethod')->where('pm_title', $request->input('name'))->first();
        $image = $return->pm_image;
        return $image;
    }

    public function checkout()
    {
        //dd($_SESSION['cart']);
        $country_details              = Register::get_country_details();
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $general                      = Home::get_general_settings();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_all_deals_details();
        $auction_details              = Home::get_all_action_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $get_meta_details             = Home::get_meta_details();
        $get_image_favicons_details   = Home::get_image_favicons_details();
        $get_image_logoicons_details  = Home::get_image_logoicons_details();
        $getlogodetails               = Home::getlogodetails();
        $getmetadetails               = Home::getmetadetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $cust_id                      = Session::get('customerid');

        $list_country                 = Country::view_country_detail();
        $list_city                    = City::view_city_detail();
        $list_postal_sevices          = City::view_postal_service_detail();
        $get_default_city             = Home:: get_default_city();
        $get_pay                      = Settings::get_pay_settings();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_data_cus                 = Home::get_data_detail_cus(Session::get('customerid'));

        //session_start();
        if (isset($_SESSION['cart'])) {
            $result_cart = Home::get_add_to_cart_details();
        } else {
            $result_cart = '';
        }

        //dd($result_cart);

        $size_result           = Home::get_add_to_cart_size();
        $color_result          = Home::get_add_to_cart_color();
        $shipping_addr_details = Home::get_shipping_addr_details($cust_id);
        //dd($cust_id);
        //dd($shipping_addr_details);

        if (isset($_SESSION['deal_cart'])) {
            $result_cart_deal = Home::get_add_to_cart_deal_details();
        } else {
            $result_cart_deal = "";
        }

        $country = 'Indonesia';
        // $get_city_popbox = Popbox::getCity($country);
        // $get_location_popbox = Popbox::getLocation('Indonesia', '');
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl)->with('general', $general);

        $getmetadetails               = Home::getmetadetails();
        $get_metode_pembayaran        = Home::get_payment_method();
        $get_setting_extended         = Settings::get_setting_extended_db();
        $get_start_pm_img             = DB::table('nm_paymentmethod')->where('pm_title', $get_metode_pembayaran[0]->nama_pay_header)->first();
        //dd($result_cart);
        return view('checkout')
            ->with('navbar', $navbar)
            ->with('get_cur', $get_cur)
            ->with('header', $header)
            ->with('footer', $footer)
            ->with('get_start_pm_img', $get_start_pm_img)
            ->with('header_category', $header_category)
            ->with('product_details', $product_details)
            ->with('deals_details', $deals_details)
            ->with('auction_details', $auction_details)
            ->with('get_product_details_by_cat', $get_product_details_by_cat)
            ->with('most_visited_product', $most_visited_product)
            ->with('category_count', $category_count)
            ->with('get_product_details_typeahed', $get_product_details_typeahed)
            ->with('main_category', $main_category)
            ->with('sub_main_category', $sub_main_category)
            ->with('second_main_category', $second_main_category)
            ->with('second_sub_main_category', $second_sub_main_category)
            ->with('get_meta_details', $get_meta_details)
            ->with('get_image_favicons_details', $get_image_favicons_details)
            ->with('get_image_logoicons_details', $get_image_logoicons_details)
            ->with('shipping_addr_details', $shipping_addr_details)
            ->with('result_cart', $result_cart)
            ->with('size_result', $size_result)
            ->with('color_result', $color_result)
            ->with('result_cart_deal', $result_cart_deal)
            ->with('metadetails', $getmetadetails)
            ->with('get_contact_det', $get_contact_det)
            ->with('general', $general)
            ->with('list_country', $list_country)
            ->with('list_city', $list_city)
            ->with('list_postal_sevices', $list_postal_sevices)
            ->with('get_default_city', $get_default_city)
            // ->with('get_city_popbox', $get_city_popbox['data'])
            // ->with('get_location_popbox', $get_location_popbox['data'])
            ->with('get_metode_pembayaran', $get_metode_pembayaran)
            ->with('get_data_cus', $get_data_cus)
            ->with('get_setting_extended', $get_setting_extended);
    }

    public function check_estimate_zipcode()
    {
        $result = Home::get_estimate_zipcode_range($_GET['estimate_check_val']);
        if ($result) {
            foreach ($result as $estimate_result) {
            }
            echo $estimate_result->ez_code_days;
        } else {
            echo 0;
        }
    }

    public function recall_unsync()
    {
        $data_unsync = Home::unsyncedOrders();

        foreach($data_unsync as $transID => $ndata)
        {
            $promo_coupons = DB::table('nm_promo_coupons')->where('promoc_id', $ndata[0]->kupon_id)->first();
            if ($promo_coupons != null) {
                $voucher_code = $promo_coupons->promoc_voucher_code;
            }else {
                $voucher_code = '';
            }
            $totalCharge = 0;
            $items = [];
            $dest_data = [];
            foreach($ndata as $item){
                if($item->mer_fname == "Kukuruyuk")
                {
                    $items[] = [
                        'sku' => $item->pro_sku,
                        'unit_price' => $item->order_amt,
                        'tax_rate' => $item->order_tax,
                        'qty' => $item->order_qty
                    ];
                    $totalCharge += $item->order_qty*$item->order_amt - $item->order_tax;
                }
            }

            if ($ndata[0]->postalservice_code == 'AL') {
                $postalservice_code = 'NONE';
            }else {
                $postalservice_code = $ndata[0]->postalservice_code;
            }

            if($ndata[0]->postalservice_code == 'PopBox') {
                $dest_data[] =[
                    'location_id' => $ndata[0]->shipping_ref_number,
                    'address' => $ndata[0]->ship_address1,
                    'address2' => $ndata[0]->ship_address2,
                    'country' => $ndata[0]->co_name,
                    'city' => $ndata[0]->ci_name,
                    'state' => $ndata[0]->ship_state
                ];

                $data_kirim = array(
                    'invoice_number'=> $transID,
                    'buyer_name'=> $ndata[0]->cus_name,
                    'buyer_email'=>$ndata[0]->cus_email,
                    'postalservice_code'=>$postalservice_code,

                    'order_date'=>$ndata[0]->order_date,
                    'currency_code'=>'IDR',//todo ambil data
                    //--

                    'allow_duplicate_invoice' => false,

                    'address'=>$ndata[0]->ship_address1,
                    //'city_code'=>'1103',//todo ambil data
                    'zip_code'=>$ndata[0]->ship_postalcode,
                    'total_charge'=> $totalCharge,

                    'destination_type'=> 'PopBox',
                    'dest_data' => $dest_data,

                    'dest_code' => $ndata[0]->ci_code,

                    'items'=> $items,
                    'discount' => $ndata[0]->diskon + $ndata[0]->poin_digunakan,
                    'other_data' => [
                            'voucher_code' => $voucher_code,
                            'payment_method' => $ndata[0]->metode_pembayaran,
                            'receiver_name' => $ndata[0]->ship_name
                        ],
                    'buyer_data' => [
                            'phone' => $ndata[0]->ship_phone
                        ]
                );
            }
            else
            {
                $dest_data =[
                    'city' => $ndata[0]->ci_name,
                    'address' => $ndata[0]->ship_address1,
                    'address2' => $ndata[0]->ship_address2,
                    'country' => $ndata[0]->co_name,
                    'state' => $ndata[0]->ship_state,
                    'zip_code' => $ndata[0]->ship_postalcode
                ];

                //dd($dest_data);

                $data_kirim = array(
                    'invoice_number'=> $transID,
                    'buyer_name'=> $ndata[0]->cus_name,
                    'buyer_email'=>$ndata[0]->cus_email,
                    'postalservice_code'=>$postalservice_code,

                    'order_date'=>$ndata[0]->order_date,
                    'currency_code'=>'IDR',//todo ambil data
                    //--

                    'allow_duplicate_invoice' => false,

                    'address'=>$ndata[0]->ship_address1,
                    //'city_code'=>'1103',//todo ambil data
                    'zip_code'=>$ndata[0]->ship_postalcode,
                    'total_charge'=> $totalCharge,

                    'destination_type'=> 'ZIP_CODE',
                    'dest_data' => $dest_data,

                    'dest_code' => $ndata[0]->ci_code,

                    'items'=> $items,
                    'discount' => $ndata[0]->diskon + $ndata[0]->poin_digunakan,
                    'other_data' => [
                            'voucher_code' => $voucher_code,
                            'payment_method' => $ndata[0]->metode_pembayaran,
                            'receiver_name' => $ndata[0]->ship_name
                        ],
                    'buyer_data' => [
                            'phone' => $ndata[0]->ship_phone
                        ]
                );
            }

            if($ndata[0]->mer_fname == 'Kukuruyuk') {
                // echo "Mmasuk kukuruyuk";
                // $result = \App\WMSClient::createSO($data_kirim);
                $result = '';

                if(is_array($result)){
                    if($result['created_id'] != null){
                        DB::table('nm_order')
                        ->where('transaction_id', $transID)
                        ->update(['id' => $result['created_id']]);

                        // dd("sukses");
                    }
                }else {
                    // echo "failed integration to WMS";
                    // dd($result);
                }
                // if($result['created_id'] != null) {
                //     DB::table('nm_order')
                //     ->where('transaction_id', $transID)
                //     ->update(['id' => $result['created_id']]);
                // }else{
                //     return $result;
                // }
            }
            else
            {
                // echo "ga masuk kukuruyu";
            }
        }
        //die();
    }

    public function kirim_request($aKode, $aItems, $aToZip, $aCity)
    {
        // -- Deklarasi variable
        $kode   = $aKode;
        $Items  = $aItems;
        $aToZip = $aToZip;
        $aCity  = $aCity;

        // dd($Items);
        if ($kode == 'Columbia') {
            $biaya_columbia = DB::table('nm_paymentsettings')->sum('ps_biaya_columbia');
            $biaya_columbia = intval($biaya_columbia);
            for ($l=0; $l < count($Items); $l++) {
                $Items[$l]['cost'] = $biaya_columbia;
            }
            $return = array(
                'price'         => $biaya_columbia * count($Items),
                'items'         => $Items,
                'est_duration'  => '1',
            );
            return $return;
        }

        $max = count($Items);

        for($i = 0;$i<$max;$i++)
        {
            $id = $Items[$i]['id'];
            //dd($id);
            $x = City::get_zipcode_shipping_item($id);
            // dd($x);
            //cara push array
            $item_with_shop[] =  $x;
        }
        //membuat menjadi koleksi
        $collection = collect($item_with_shop);
        //meng-group koleksi
        $colgrop = $collection->groupBy('stor_zipcode')->toArray();

        //dd($colgrop);
        foreach($colgrop as $zipcode => $ndata)
        {
            $dest = array(
                //'to_city_code' => $aCity,
                'to_zip_code' => $aToZip,
                //'from_city_code' => $colgrop[$ndata][$i]['ci_code'],
                'from_zip_code' => $zipcode,
            );

            // dd($dest);
            // $response = array(
            //     'error' => 'error',
            //     'price' => 0
            // );
            // return $response;
            // $result = \App\WMSClient::getPostalInfo($kode, $Items, $dest);
            $result['error'] = 'error';
            // dd($result['items']);
            if(isset($result['error']))
            {
                $return = array(
                    'error' => $result['error'],
                    'price' => 0
                );
                return $return;
            }
            else
            {
                $return = array(
                    'price'         => $result['total_cost'],
                    'items'         => $result['items'],
                    'est_duration'  => $result['est_duration']
                );
                return $return;
            }

        }

    }

    public function cek_kupon($nomor_kupon, $total_belanja_awal, $total_shipping, $postal_code, $carts)
    {
        // -- Deklarasi variable
        $customerid = Session::get('customerid');
        if($nomor_kupon=='' || $nomor_kupon==null){
            $data = array(
                'total_diskon' => 0,
                'kupon_id' => 0
            );
            return $data;
        }
        if($total_shipping == "")
        {
            $total_shipping = floatval(0);
        }
        $total_belanja_akhir = 0;
        $total_diskon = 0;
        date_default_timezone_set('Asia/Jakarta');
        $today = date('Y-m-d H:i:s');//$_GET['today'];
        //dd($today);
        // $total_pajak = $_GET['pajak'];
        $total_pajak = 0;

        $getKupon = City::getKupon($nomor_kupon);
        $get_customer_usage = DB::table("nm_promo_usage")
        ->where('promou_cus_id', $customerid)
        ->where('promou_promoc_id', $getKupon->promoc_id)
        ->first();
        // dd($getKupon->schedp_start_date > $today);
        if($getKupon->schedp_end_date < $today || $getKupon->schedp_start_date > $today)
        {
            $error = array(
                'error' => 'Kupon Tidak Berlaku',
                'total_diskon' => 0
            );
            return $error;
        }
        if($getKupon->promoc_usage_per_customer!=0 || $getKupon->promoc_usage_per_customer!=null || $getKupon->promoc_usage_per_customer!=''){
            if($get_customer_usage!=null)
            {
                if($get_customer_usage->promou_usage >= $getKupon->promoc_usage_per_customer)
                {
                    $error = array(
                        'error' => "Jumlah Pemakaian Kupon Telah Mencapai Batas Maksimal",
                        'total_diskon' => 0
                    );
                    return $error;
                }
            }
        }
        if($getKupon->promoc_usage_limit!=0 || $getKupon->promoc_usage_limit!=null || $getKupon->promoc_usage_limit!=''){
            if($getKupon->promoc_times_used >= $getKupon->promoc_usage_limit){
                $error = array(
                    'error' => "Kupon Telah Habis",
                    'total_diskon' => 0
                );
                return $error;
            }
        }

        // Check Recurring
        if($getKupon->schedp_RecurrenceType==2){ // Recurring Hari
            $days = explode(",", $getKupon->schedp_Int2);
            $tanggal = date('Y-m-d');
            $status_hari = 0;
            foreach ($days as $day) {
                if($day==0){
                    if($tanggal == date('Y-m-d', strtotime('Monday'))){
                        $status_hari = 1;
                    }
                }
                if($day==1){
                    if($tanggal == date('Y-m-d', strtotime('Tuesday'))){
                        $status_hari = 1;
                    }
                }
                if($day==2){
                    if($tanggal == date('Y-m-d', strtotime('Wednesday'))){
                        $status_hari = 1;
                    }
                }
                if($day==3){
                    if($tanggal == date('Y-m-d', strtotime('Thursday'))){
                        $status_hari = 1;
                    }
                }
                if($day==4){
                    if($tanggal == date('Y-m-d', strtotime('Friday'))){
                        $status_hari = 1;
                    }
                }
                if($day==5){
                    if($tanggal == date('Y-m-d', strtotime('Saturday'))){
                        $status_hari = 1;
                    }
                }
                if($day==6){
                    if($tanggal == date('Y-m-d', strtotime('Sunday'))){
                        $status_hari = 1;
                    }
                }
            }
            if($status_hari!=1){
                $error = array(
                    'error' => "Promo Tidak Berlaku Pada Saat Ini",
                    'total_diskon' => 0
                );
                return $error;
            }
            if($getKupon->schedp_recurr_start_time!=0 && $getKupon->schedp_recurr_end_time!=0)
            {
                $time = date('H:i:s');
                if($time<$getKupon->schedp_recurr_start_time || $time>$getKupon->schedp_recurr_end_time)
                {
                    $error = array(
                        'error' => "Promo Tidak Berlaku Pada Saat Ini",
                        'total_diskon' => 0
                    );
                    return $error;
                }
            }
        }elseif ($getKupon->schedp_RecurrenceType==3) { // Recurring Bulan
            $months = explode(",", $getKupon->schedp_Int2);
            $tanggal = date('Y-m-d');
            $status_bulan = 0;
            foreach ($months as $month) {
                if($month==0){
                    if($tanggal == date('Y-m-d', strtotime('January'))){
                        $status_bulan = 1;
                    }
                }
                if($month==1){
                    if($tanggal == date('Y-m-d', strtotime('February'))){
                        $status_bulan = 1;
                    }
                }
                if($month==2){
                    if($tanggal == date('Y-m-d', strtotime('March'))){
                        $status_bulan = 1;
                    }
                }
                if($month==3){
                    if($tanggal == date('Y-m-d', strtotime('April'))){
                        $status_bulan = 1;
                    }
                }
                if($month==4){
                    if($tanggal == date('Y-m-d', strtotime('May'))){
                        $status_bulan = 1;
                    }
                }
                if($month==5){
                    if($tanggal == date('Y-m-d', strtotime('June'))){
                        $status_bulan = 1;
                    }
                }
                if($month==6){
                    if($tanggal == date('Y-m-d', strtotime('July'))){
                        $status_bulan = 1;
                    }
                }
                if($month==7){
                    if($tanggal == date('Y-m-d', strtotime('August'))){
                        $status_bulan = 1;
                    }
                }
                if($month==8){
                    if($tanggal == date('Y-m-d', strtotime('September'))){
                        $status_bulan = 1;
                    }
                }
                if($month==9){
                    if($tanggal == date('Y-m-d', strtotime('October'))){
                        $status_bulan = 1;
                    }
                }
                if($month==10){
                    if($tanggal == date('Y-m-d', strtotime('November'))){
                        $status_bulan = 1;
                    }
                }
                if($month==11){
                    if($tanggal == date('Y-m-d', strtotime('December'))){
                        $status_bulan = 1;
                    }
                }
            }
            if($status_bulan!=1){
                $error = array(
                    'error' => "Promo Tidak Berlaku Pada Saat Ini",
                    'total_diskon' => 0
                );
                return $error;
            }
        }
        //End Check Recurring

        if($getKupon == null)
        {
            $error = array(
                'error' => "Kupon Tidak Berlaku",
                'total_diskon' => 0
            );
            return $error;
        }
        else
        {
            if($getKupon->status_coupons == "sudah dipakai")
            {
                $error = array(
                    'error' => "Kupon Sudah Dipakai",
                    'total_diskon' => 0
                );
                return $error;
            }
            else
            {
                // ambil barang dalam promo
                if($getKupon->schedp_simple_action != 'free_shipping'){
                    $getProductKupon = DB::table('nm_promo_products')
                    ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_promo_products.promop_pro_id')
                    ->where('promop_schedp_id', $getKupon->schedp_id)->get();

                    // dd($getProductKupon);
                }

                // kondisi diskon kupon by percent
                if($getKupon->schedp_simple_action == "by_percent")
                {
                    if($getKupon->schedp_end_date > $today)
                    {
                        if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                            $diskon = 0;
                            // dd($carts);
                            if($getKupon->schedp_coupon_type==1)
                            {
                                $total_belanja_akhir = (((100 - $getKupon->schedp_discount_amount)* $total_belanja_awal) / 100);
                                $total_diskon = (($getKupon->schedp_discount_amount/100) * $total_belanja_awal);
                            }
                            elseif ($getKupon->schedp_coupon_type==2)
                            {
                                foreach($carts as $cart){
                                    foreach ($getProductKupon as $productKupon)
                                    {
                                        if($cart['id'] == $productKupon->promop_pro_id)
                                        {
                                            $diskon += $productKupon->promop_saving_price;
                                        }
                                    }
                                }
                                $total_belanja_akhir = $total_belanja_awal - $diskon;
                                $total_diskon = $diskon;
                                if($total_diskon==0){
                                    $error = array(
                                        'error' => "Barang Tidak Sesuai Dengan Kupon",
                                        'total_diskon' => 0
                                    );
                                    return $error;
                                    die();
                                }
                            }

                            // dd($diskon);
                            $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);

                            $total_belanja_plus_shipping = $total_belanja_setelah_pajak + $total_shipping;

                            if($get_customer_usage!=null){
                                $customer_usage = $get_customer_usage->promou_usage + 1;
                                $update_customer_usage = DB::table("nm_promo_usage")
                                ->where('promou_cus_id', $customerid)
                                ->where('promou_promoc_id', $getKupon->promoc_id)
                                ->update([
                                    'promou_usage' => $customer_usage
                                ]);
                            }else {
                                $insert_customer_usage = DB::table("nm_promo_usage")
                                ->insert([
                                    'promou_usage' => 1,
                                    'promou_cus_id' => $customerid,
                                    'promou_promoc_id' => $getKupon->promoc_id
                                ]);
                            }
                            $coupon_usage = $getKupon->promoc_times_used + 1;
                            $update_coupon_usage = DB::table('nm_promo_coupons')
                            ->where('promoc_voucher_code', $nomor_kupon)
                            ->update([
                                'promoc_times_used' => $coupon_usage
                            ]);

                            $data = array(
                                'schedp_title' => $getKupon->schedp_title, // judul
                                'schedp_discount_amount' => $getKupon->schedp_discount_amount, // jumlah diskonnya
                                'total_belanja_akhir' => $total_belanja_akhir,
                                'total_diskon' => $total_diskon,
                                'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                'total_belanja_setelah_pajak' => $total_belanja_setelah_pajak,
                                'total_belanja_plus_shipping' => $total_belanja_plus_shipping,
                                'kupon_id' => $getKupon->promoc_id
                            );
                            return $data;
                        }
                        else {
                            $error = array(
                                'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                'total_diskon' => 0
                            );
                            return $error;
                        }
                    }
                    else
                    {
                        $error = array(
                            'error' => "Kupon Tidak Berlaku",
                            'total_diskon' => 0
                        );
                        return $error;
                    }
                }

                // kondisi diskon kupon by amount
                else if($getKupon->schedp_simple_action == "by_fixed")
                {
                    if($getKupon->schedp_end_date > $today)
                    {
                        if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                            $diskon = 0;
                            // dd($carts);
                            if($getKupon->schedp_coupon_type==1)
                            {
                                $total_belanja_akhir = ($total_belanja_awal - $getKupon->schedp_discount_amount);
                                $total_diskon = $getKupon->schedp_discount_amount;
                            }
                            elseif ($getKupon->schedp_coupon_type==2) {
                                foreach($carts as $cart)
                                {
                                    foreach ($getProductKupon as $productKupon)
                                    {
                                        if($cart['id'] == $productKupon->promop_pro_id)
                                        {
                                            $diskon += $productKupon->promop_saving_price;
                                        }
                                    }
                                }
                                $total_belanja_akhir = $total_belanja_awal - $diskon;
                                $total_diskon = $diskon;
                                if($total_diskon==0){
                                    $error = array(
                                        'error' => "Barang Tidak Sesuai Dengan Kupon",
                                        'total_diskon' => 0
                                    );
                                    return $error;
                                    die();
                                }
                            }
                            $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);
                            $total_belanja_plus_shipping = $total_belanja_setelah_pajak + $total_shipping;

                            if($get_customer_usage!=null){
                                $customer_usage = $get_customer_usage->promou_usage + 1;
                                $update_customer_usage = DB::table("nm_promo_usage")
                                ->where('promou_cus_id', $customerid)
                                ->where('promou_promoc_id', $getKupon->promoc_id)
                                ->update([
                                    'promou_usage' => $customer_usage
                                ]);
                            }else {
                                $insert_customer_usage = DB::table("nm_promo_usage")
                                ->insert([
                                    'promou_usage' => 1,
                                    'promou_cus_id' => $customerid,
                                    'promou_promoc_id' => $getKupon->promoc_id
                                ]);
                            }
                            $coupon_usage = $getKupon->promoc_times_used + 1;
                            $update_coupon_usage = DB::table('nm_promo_coupons')
                            ->where('promoc_voucher_code', $nomor_kupon)
                            ->update([
                                'promoc_times_used' => $coupon_usage
                            ]);

                            $data = array(
                                'schedp_title' => $getKupon->schedp_title, // judul
                                'schedp_discount_amount' => floatval($getKupon->schedp_discount_amount), // jumlah diskonnya
                                'total_belanja_akhir' => floatval($total_belanja_akhir),
                                'total_diskon' => floatval($total_diskon),
                                'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                'total_belanja_plus_shipping' => $total_belanja_plus_shipping,
                                'kupon_id' => $getKupon->promoc_id
                            );
                            return $data;
                        }
                        else {
                            $error = array(
                                'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                'total_diskon' => 0
                            );
                            return $error;
                        }
                    }
                    else
                    {
                        $error = array(
                            'error' => "Kupon Tidak Berlaku",
                            'total_diskon' => 0
                        );
                        return $error;
                    }
                }
                // kondisi diskon kupon free shipping
                else if($getKupon->schedp_simple_free_shipping == 1)
                {
                    // echo("masuk kondisi free shipping -|- ");
                    // cek kondisi tanggal
                    if($getKupon->schedp_end_date > $today && $getKupon->schedp_start_date < $today)
                    {
                        // echo("masuk lolos cek tanggal -|- ");
                        // echo("today :" .$today ."-|- ");
                        // echo("start_date :".$getKupon->schedp_start_date." -|- ");
                        // echo("end date :".$getKupon->schedp_end_date." -|- ");
                        // kondisi minimal transaksi
                        if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                            // echo("lolos dari minimal transaksi -|- ");
                            // kondisi free shipping postal service tertentu dan kondisi untuk semua postal service
                            if($getKupon->ps_code == $postal_code || $getKupon->ps_code == 0)
                            {
                                // kondisi maximum free shipping
                                if($getKupon->maximum_free_shipping >= $total_shipping)
                                {
                                    $total_belanja_akhir = $total_belanja_awal;
                                    $total_diskon = 0;

                                    $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);

                                    if($get_customer_usage!=null){
                                        $customer_usage = $get_customer_usage->promou_usage + 1;
                                        $update_customer_usage = DB::table("nm_promo_usage")
                                        ->where('promou_cus_id', $customerid)
                                        ->where('promou_promoc_id', $getKupon->promoc_id)
                                        ->update([
                                            'promou_usage' => $customer_usage
                                        ]);
                                    }else {
                                        $insert_customer_usage = DB::table("nm_promo_usage")
                                        ->insert([
                                            'promou_usage' => 1,
                                            'promou_cus_id' => $customerid,
                                            'promou_promoc_id' => $getKupon->promoc_id
                                        ]);
                                    }
                                    $coupon_usage = $getKupon->promoc_times_used + 1;
                                    $update_coupon_usage = DB::table('nm_promo_coupons')
                                    ->where('promoc_voucher_code', $nomor_kupon)
                                    ->update([
                                        'promoc_times_used' => $coupon_usage
                                    ]);

                                    $data = array(
                                        'schedp_title' => $getKupon->schedp_title, // judul
                                        'schedp_discount_amount' => "Free Shipping", // jumlah diskonnya
                                        'total_belanja_akhir' => floatval($total_belanja_akhir),
                                        'total_diskon' => floatval($total_shipping),
                                        'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                        'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                        'shipping_cost' => $total_shipping,
                                        'total_belanja_plus_shipping' => floatval($total_belanja_setelah_pajak),
                                        'kupon_id' => $getKupon->promoc_id
                                    );
                                    return $data;
                                }
                                else
                                {
                                    $total_belanja_akhir = $total_belanja_awal + $total_shipping - floatval($getKupon->maximum_free_shipping);
                                    $total_diskon = 0;

                                    $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);

                                    if($get_customer_usage!=null){
                                        $customer_usage = $get_customer_usage->promou_usage + 1;
                                        $update_customer_usage = DB::table("nm_promo_usage")
                                        ->where('promou_cus_id', $customerid)
                                        ->where('promou_promoc_id', $getKupon->promoc_id)
                                        ->update([
                                            'promou_usage' => $customer_usage
                                        ]);
                                    }else {
                                        $insert_customer_usage = DB::table("nm_promo_usage")
                                        ->insert([
                                            'promou_usage' => 1,
                                            'promou_cus_id' => $customerid,
                                            'promou_promoc_id' => $getKupon->promoc_id
                                        ]);
                                    }
                                    $coupon_usage = $getKupon->promoc_times_used + 1;
                                    $update_coupon_usage = DB::table('nm_promo_coupons')
                                    ->where('promoc_voucher_code', $nomor_kupon)
                                    ->update([
                                        'promoc_times_used' => $coupon_usage
                                    ]);

                                    $data = array(
                                        'schedp_title' => $getKupon->schedp_title, // judul
                                        'schedp_discount_amount' => "Free Shipping", // jumlah diskonnya
                                        'total_belanja_akhir' => floatval($total_belanja_akhir),
                                        'total_diskon' => floatval($getKupon->maximum_free_shipping),
                                        'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                        'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                        'shipping_cost' => $total_shipping,
                                        'total_belanja_plus_shipping' => floatval($total_belanja_setelah_pajak),
                                        'kupon_id' => $getKupon->promoc_id
                                    );
                                    return $data;
                                }
                            }
                            else
                            {
                                $error = array(
                                    'error' => "Kupon tidak berlaku untuk pengiriman melalui "+ $postal_code,
                                    'total_diskon' => 0
                                );
                                return $error;
                            }
                        }
                        else {
                            $error = array(
                                'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                'total_diskon' => 0
                            );
                            return $error;
                        }
                    }
                    else
                    {
                        $error = array(
                            'error' => "Kupon Expired",
                            'total_diskon' => 0
                        );
                        return $error;
                    }
                }
                // if kondisi untuk buy x get y
                else if ($getKupon->schedp_simple_action == 'buy_x_get_y')
                {
                    if($getKupon->schedp_end_date > $today)
                    {
                        if($total_belanja_awal >= $getKupon->promoc_minimal_transaction){
                            //dd($carts);
                            foreach($carts as $cart){
                                foreach ($getProductKupon as $productKupon)
                                {
                                    if($cart['id'] == $productKupon->promop_pro_id)
                                    {
                                        $quantity_diskon = floor(($cart['qty']/($getKupon->schedp_x + $getKupon->schedp_y))*$getKupon->schedp_y);
                                        $total_diskon = $quantity_diskon * $cart['price'];
                                        // if($productKupon->pro_disprice == 0 || $productKupon->pro_disprice == null || $productKupon->pro_disprice == ''){
                                        //     $total_diskon = $quantity_diskon * $productKupon->pro_price;
                                        // }else {
                                        //     $total_diskon = $quantity_diskon * $productKupon->pro_disprice;
                                        // }
                                    }
                                }
                            }
                            if($quantity_diskon==0){
                                $error = array(
                                    'error' => "Barang Tidak Sesuai Dengan Kupon",
                                    'total_diskon' => 0
                                );
                                return $error;
                                die();
                            }
                            $total_belanja_akhir = $total_belanja_awal - $total_diskon;
                            $total_belanja_setelah_pajak = ($total_belanja_akhir * ($total_pajak/100) + $total_belanja_akhir);
                            $total_belanja_plus_shipping = $total_belanja_setelah_pajak + $total_shipping;

                            if($get_customer_usage!=null){
                                $customer_usage = $get_customer_usage->promou_usage + 1;
                                $update_customer_usage = DB::table("nm_promo_usage")
                                ->where('promou_cus_id', $customerid)
                                ->where('promou_promoc_id', $getKupon->promoc_id)
                                ->update([
                                    'promou_usage' => $customer_usage
                                ]);
                            }else {
                                $insert_customer_usage = DB::table("nm_promo_usage")
                                ->insert([
                                    'promou_usage' => 1,
                                    'promou_cus_id' => $customerid,
                                    'promou_promoc_id' => $getKupon->promoc_id
                                ]);
                            }
                            $coupon_usage = $getKupon->promoc_times_used + 1;
                            $update_coupon_usage = DB::table('nm_promo_coupons')
                            ->where('promoc_voucher_code', $nomor_kupon)
                            ->update([
                                'promoc_times_used' => $coupon_usage
                            ]);

                            $data = array(
                                'schedp_title' => $getKupon->schedp_title,
                                'schedp_discount_amount' => "FREE ".$quantity_diskon,
                                'total_belanja_akhir' => $total_belanja_akhir,
                                'total_diskon' => $total_diskon,
                                'schedp_simple_action' =>$getKupon->schedp_simple_action,
                                'total_belanja_setelah_pajak' => floatval($total_belanja_setelah_pajak),
                                'total_belanja_plus_shipping' => floatval($total_belanja_plus_shipping),
                                'kupon_id' => $getKupon->promoc_id
                            );
                            return $data;
                        }
                        else {
                            $error = array(
                                'error' => "Total Belanja Kurang Dari Minimal Transaksi",
                                'total_diskon' => 0
                            );
                            return $error;
                        }
                    }
                    else
                    {
                        $error = array(
                            'error' => 'Kupon Tidak Berlaku',
                            'total_diskon' => 0
                        );
                        return $error;
                    }
                }
            }
        }
    }

    public function random_key()
    {
        $random_key = rand();
        $check_exist_transaction_id = Home::check_exist_transaction_id($random_key);
        if($check_exist_transaction_id == null)
        {
            return $random_key;
        }else {
            self::random_key();
        }
    }

    public function payment_checkout_process()
    {
        //session_start();
        //dd($_POST);
        date_default_timezone_set('Asia/Jakarta');
        $cust_id  = Session::get('customerid');
        $pay_type = Input::get('select_payment_type');
        if (isset($_SESSION['cart'])) {
            $result_cart = Home::get_add_to_cart_details();
        } else {
            $result_cart = '';
        }
        if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
        {
            $max=count($_SESSION['cart']);
        }

        if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
        {
            $max=count($_SESSION['deal_cart'])-1;
        }
        $aItems = array();
        for($i=0;$i<$max;$i++)
        {
            if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
            {
                $pid      = $_SESSION['cart'][$i]['productid'];
                $q        = $_SESSION['cart'][$i]['qty'];
                $weight   = $_SESSION['cart'][$i]['weight'];
                $length   = $_SESSION['cart'][$i]['length'];
                $width    = $_SESSION['cart'][$i]['width'];
                $height   = $_SESSION['cart'][$i]['height'];
            }

            if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
            {
                $pid=$_SESSION['deal_cart'][$i]['productid'];
                $q=$_SESSION['deal_cart'][$i]['qty'];
            }

            $total_price = 0;
            //dd($result_cart);
            foreach($result_cart[$pid] as $session_cart_result)
            {
                $id        = $session_cart_result->pro_id;
                $weight2    = $session_cart_result->pro_weight;
                $length2    = floatval($session_cart_result->pro_length);
                $width2     = floatval($session_cart_result->pro_width);
                $height2    = floatval($session_cart_result->pro_height);

                if ($session_cart_result->wholesale_level1_min>1) {
                    if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q):
                        $price = floatval($session_cart_result->wholesale_level5_price);
                    elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q):
                        $price = floatval($session_cart_result->wholesale_level4_price);
                    elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q):
                        $price = floatval($session_cart_result->wholesale_level3_price);
                    elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q):
                        $price = floatval($session_cart_result->wholesale_level2_price);
                    elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q):
                        $price = floatval($session_cart_result->wholesale_level1_price);
                    else:
                        if ($session_cart_result->pro_disprice==0 || $session_cart_result->pro_disprice=='' || $session_cart_result->pro_disprice==null):
                            $price = floatval($session_cart_result->pro_price);
                        else:
                            $price = floatval($session_cart_result->pro_disprice);
                        endif;
                    endif;
                } else
                if(!isset($session_cart_result->promop_schedp_id) || empty($session_cart_result->promop_schedp_id)){
                    if ($session_cart_result->pro_disprice==0 || $session_cart_result->pro_disprice=='' || $session_cart_result->pro_disprice==null){
                        $price = floatval($session_cart_result->pro_price);
                    } else {
                        $price = floatval($session_cart_result->pro_disprice);
                    }
                }else{
                    if ($session_cart_result->pro_disprice==0 || $session_cart_result->pro_disprice=='' || $session_cart_result->pro_disprice==null){
                        $price = floatval($session_cart_result->promop_discount_price);
                    } else {
                        $price = floatval($session_cart_result->promop_discount_price);
                    }
                }

                $volume     = $length2 * $width2 * $height2;

                // $aItems .= '{id:"'.$id.'",qty:'.$q.',weight:"'.$weight2.'",volume:"'.$volume.'"},';
                $aItems[] = array(
                    'id' => $id,
                    'qty' => $q,
                    'weight' => $weight2,
                    'volume' =>$volume,
                    'price' => $price
                );

                $total_price = $total_price + $price;
                //dd($total_price);
            }
        }
        // dd($_POST);
        // 6 for sprintasia
        if ($pay_type == 6) {

            if ($_POST) //Post Data received from product list page.
            {
                //Other important variables like tax, shipping cost
                if (isset($_POST['tax_price']) && $_POST['tax_price'] != '') {
                    $TotalTaxAmount = $_POST['tax_price']; //Sum of tax for all items in this order.
                } else {
                    $TotalTaxAmount = 0;
                }
                $HandalingCost   = 0.00; //Handling cost for this order.
                $InsuranceCost   = 0.00; //shipping insurance cost for this order.
                $ShippinDiscount = 0.00; //Shipping discount for this order. Specify this as negative number.
                if (isset($_POST['shipping_price_label_hidden']) && $_POST['shipping_price_label_hidden'] != '') {
                    $ShippinCost = $_POST['shipping_price_label_hidden']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
                } else {
                    $ShippinCost = 0;
                }
                $paypal_data    = '';
                $ItemTotalPrice = 0;
                $now            = date('Y-m-d H:i:s');
                $insert_id      = '';
                $random_key     = self::random_key(); //
                // bcaklikpay sprintasia Toto 20170306
                $diskon_temp    = 0;
                $i_shipping     = 0;
                $trx_is_cicilan = 0;
                $get_setting_extended   = Settings::get_setting_extended_db();
                foreach ($_POST['item_name'] as $key => $itmname) {
                    $product_code = filter_var($_POST['item_code'][$key], FILTER_SANITIZE_STRING);

                    // bcaklikpay sprintasia Toto 20170306
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($_POST['item_name'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($_POST['item_code'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($_POST['item_price'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($_POST['item_qty'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_DESC' . $key . '=' . urlencode("Color : " . $_POST['item_color_name'][$key] . " - Size : " . $_POST['item_size_name'][$key]);

                    // item price X quantity
                    $get_product = Products::get_product_by_id($_POST['item_code'][$key]);
                    //dd($get_product);
                    // if ($get_product->wholesale_level1_min>1) {
                    //     if ($get_product->wholesale_level5_min>1 && $get_product->wholesale_level5_min<=$q):
                    //         $item_price = floatval($get_product->wholesale_level5_price);
                    //     elseif ($get_product->wholesale_level4_min>1 && $get_product->wholesale_level4_min<=$q):
                    //         $item_price = floatval($get_product->wholesale_level4_price);
                    //     elseif ($get_product->wholesale_level3_min>1 && $get_product->wholesale_level3_min<=$q):
                    //         $item_price = floatval($get_product->wholesale_level3_price);
                    //     elseif ($get_product->wholesale_level2_min>1 && $get_product->wholesale_level2_min<=$q):
                    //         $item_price = floatval($get_product->wholesale_level2_price);
                    //     elseif ($get_product->wholesale_level1_min>1 && $get_product->wholesale_level1_min<=$q):
                    //         $item_price = floatval($get_product->wholesale_level1_price);
                    //     else:
                    //         if ($get_product->pro_disprice==0 || $get_product->pro_disprice=='' || $get_product->pro_disprice==null):
                    //             $item_price = floatval($get_product->pro_price);
                    //         else:
                    //             $item_price = floatval($get_product->pro_disprice);
                    //         endif;
                    //     endif;
                    // } else
                    if ($get_product->pro_disprice==0 || $get_product->pro_disprice=='' || $get_product->pro_disprice==null){
                        $item_price = $get_product->pro_price;
                    } else {
                        $item_price = $get_product->pro_disprice;
                    }

                    $subtotal = ($item_price * $_POST['item_qty'][$key]);
                    //total price
                    $ItemTotalPrice = $ItemTotalPrice + $subtotal;

                    //Harga Shipping
                    $aKode = $_POST['pengiriman_oke'];
                    $aToZip = $_POST['zipcode'];
                    $aCity = $_POST['city'];

                    $shipping = $this->kirim_request($aKode, $aItems, $aToZip, $aCity);
                    if(array_key_exists('error', $shipping) && $_POST['pengiriman_oke'] != "AL"){
                        dd('error: '.$shipping['error']);
                    }
                    //Harga Diskon
                    $input_kupon = $_POST['input_kupon'];
                    $harga_shipping = $shipping['price']; // live
                    if($_POST['pengiriman_oke'] == "AL")
                    {
                        $harga_shipping = 0;
                        $harga_shipping_per_item = 0;
                    }elseif ($_POST['pengiriman_oke'] == "Columbia") {
                        $harga_shipping_per_item = $shipping['items'][$i_shipping]['cost'];
                    }else {
                        $harga_shipping_per_item = $shipping['items'][$i_shipping]->cost;
                    }
                    //$harga_shipping = 0; // test
                    $postal_code = $_POST['pengiriman_oke'];
                    $carts = $aItems;
                    $diskon = self::cek_kupon($input_kupon, $total_price, $harga_shipping, $postal_code, $carts);
                    //dd($diskon);
                    // var_dump($get_product->pro_title);
                    // var_dump($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min) ;
                    if($_POST['item_schedp_id'][$key] != 0){
                        $get_flash = Home::promo_flash_time_by_id($_POST['item_pro_id'][$key], $_POST['item_schedp_id'][$key]);
                        $diskon_flash = $get_flash[0]->promop_saving_price * $_POST['item_qty'][$key];
                        $diskon_temp += $diskon_flash;
                    }elseif ($get_product->wholesale_level1_min != 0 || isset($get_product->wholesale_level1_min) || !empty($get_product->wholesale_level1_min)) {
                        if($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min)
                        {
                            // var_dump($get_product->wholesale_level1_min>1);
                            if ($get_product->wholesale_level5_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level5_min && $get_product->wholesale_level5_min<=$_POST['item_qty'][$key]):
                                // dd('test');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level4_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level4_min && $get_product->wholesale_level4_min<=$_POST['item_qty'][$key]):
                                // dd('test2');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level3_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level3_min && $get_product->wholesale_level3_min<=$_POST['item_qty'][$key]):
                                // dd('test3');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level2_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level2_min && $get_product->wholesale_level2_min<=$_POST['item_qty'][$key]):
                                // dd('test4');
                                if($get_product->pro_disprice != 0){
                                        $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                    }else{
                                        $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                    }
                            elseif ($get_product->wholesale_level1_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level1_min && $get_product->wholesale_level1_min<= $_POST['item_qty'][$key]):
                                // dd('test5');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                }
                            endif;
                        }
                    }
                    // create items for session
                    // bcaklikpay sprintasia Toto 20170306
                    $paypal_product['items'][] = array(
                        'itm_name' => $_POST['item_name'][$key],
                        'itm_price' => $_POST['item_price'][$key],
                        'itm_code' => $_POST['item_code'][$key],
                        'itm_qty' => $_POST['item_qty'][$key]

                    );
                    $shipaddresscus = Input::get('fname') . ';' . Input::get('addr_line') . ';' . Input::get('addr1_line') . ';' . Input::get('state') . ';' . Input::get('zipcode') . ';' . Input::get('phone1_line') . ';' . Input::get('email');
                    $data_customer = array(
                        'name' => Input::get('fname'),
                        'address1' => Input::get('addr_line'),
                        'address2' => Input::get('addr1_line'),
                        'state' => Input::get('state'),
                        'zipcode' => Input::get('zipcode'),
                        'phone' => Input::get('phone1_line'),
                        'email' => Input::get('email')
                    );
                    // $shipaddresscus            = Input::get('fname' . $key) . ',' . Input::get('addr_line' . $key) . ',' . Input::get('addr1_line' . $key) . ',' . Input::get('state' . $key) . ',' . Input::get('zipcode' . $key) . ',' . Input::get('phone1_line' . $key);
                    //print_r($shipaddresscus);
                    //exit();
                    $jumlah_warranty = 0;

                    if($_POST['item_warranty_val'][$key] == 1)
                    {
                        $jumlah_warranty = floatval($get_setting_extended[0]->besaran_warranty)*floatval($subtotal)/100;
                    }

                    $is_cicilan = 0;
                    if (array_key_exists(1, $_POST['cicilan'])) {
                        if ($_POST['cicilan'][$key] == 1) {
                            $is_cicilan = 1;
                            $trx_is_cicilan = 1;
                        }
                    }

                    $data = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => $_POST['item_code'][$key],
                        'order_type' => $_POST['item_type'][$key],
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => $_POST['item_qty'][$key],
                        'order_amt' => $subtotal,
                        'order_tax' => $_POST['item_tax'][$key],
                        'order_date' => $now,
                        'order_status' => 3, // 1-success,2-complete,3-hold,4-failed
                    // bcaklikpay sprintasia Toto 20170306
                        'order_paytype' => 6, // 1-paypal, 2-doku, 3-sprintasia
                        'order_pro_color' => $_POST['item_color'][$key],
                        'order_pro_size' => $_POST['item_size'][$key],
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'order_shipping_price' => $harga_shipping_per_item,
                        'order_use_warranty' => $_POST['item_warranty_val'][$key],
                        'order_jumlah_warranty' => $jumlah_warranty,
                        'order_is_cicilan' => $is_cicilan,
                        'payment_channel' => Input::get('payment_channel')
                    );
                    $data_shipping = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 1,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => $harga_shipping, //new
                        // 'order_amt' => $_POST['shipping_price_label_hidden'], //old
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => 6,
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'payment_channel' => Input::get('payment_channel')
                    );
                    $data_diskon = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 2,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => ($diskon['total_diskon'] + $diskon_temp) * -1,
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => 6,
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'payment_channel' => Input::get('payment_channel')
                    );
                    if (($_POST['item_type'][$key]) != 2) { // nm_products
                        $add_purchase = Home::purchased_checkout_product_insert($_POST['item_code'][$key], $_POST['item_qty'][$key], $_POST['item_schedp_id'][$key]);
                        if($add_purchase == 'Failed'){
                            return Redirect::to('addtocart')->with('session_result', 'Stok Tidak Mencukupi');
                        }elseif ($add_purchase == 'Failed Flash') {
                            return Redirect::to('addtocart')->with('session_result', 'Stok Flash Tidak Mencukupi');
                        }
                    } // else nm_deals

                    // bcaklikpay sprintasia Toto 20170306
                    Home::paypal_checkout_insert($data); // inserts table('nm_order')->insert($data)

                    $new_insert = DB::getPdo()->lastInsertId();
                    $insert_id .= DB::getPdo()->lastInsertId() . ',';
                    if (Input::get('load_ship' . $key) != 1) {
                          $data = array(
                            'ship_name' => Input::get('fname'),
                            'ship_address1' => Input::get('addr_line'),
                            'ship_address2' => Input::get('addr1_line'),
                            'ship_state' => Input::get('state'),
                            'ship_postalcode' => Input::get('zipcode'),
                            'ship_phone' => Input::get('phone1_line'),
                            'ship_email' => Input::get('email'),
                            'ship_cus_id' => $cust_id,
                            'ship_order_id' => $new_insert,
                            'ship_ci_id' => Input::get('id_city_ha'),
                            'ship_country' => Input::get('country'),
                            'ship_dis_id' => Input::get('district'),
                            'ship_sdis_id' => Input::get('subdistrict'),
                        );

                        // bcaklikpay sprintasia Toto 20170306
                        Home::insert_shipping_addr($data, $cust_id); // inserts table('nm_shipping')->insert($data);
                    }
                    $i_shipping++;
                }
                Home::paypal_checkout_insert($data_shipping);
                Home::paypal_checkout_insert($data_diskon);

                Session::put('last_insert_id', trim($insert_id, ','));
                $transaction_id = 'unpay'.$random_key; //
                $trans_id = $new_insert;
                $pajak = 0;
                $total_amount = Home::get_total_amount($transaction_id);

                // -- detail customer --
                $get_data_cus = Home::get_data_detail_cus(Session::get('customerid'));

                // -- penggunaan poin --
                if(Input::get('chk_poin') == 1)
                {
                    // -- jumlah pemakaian poin
                    $penggunaan_poin_trs = $get_data_cus->total_poin;
                    // -- jumlah belanja dikurang poin
                    $total_amount['total_amount_sebelum_pajak'] = $total_amount['total_amount_sebelum_pajak'] - floatval($penggunaan_poin_trs);
                    Home::update_poin_dipakai(Session::get('customerid'));
                }
                else
                {
                    $penggunaan_poin_trs = 0;
                }

                if (array_key_exists('est_duration', $shipping)) {
                    $estimate_duration = $shipping['est_duration'];
                }else {
                    $estimate_duration = 0;
                }

                $insert_transaksi = DB::table('nm_transaksi')->insert([
                    'transaction_id' => $transaction_id,
                    'total_belanja' => $total_amount['total_amount_sebelum_pajak'],
                    'diskon' => $total_amount['total_diskon'],
                    'diskon_voucher' => $diskon['total_diskon'],
                    'shipping' => $total_amount['total_shipping'],
                    'metode_pembayaran' => 'Sprintasia',
                    'status_pembayaran' => 'belum dibayar',
                    'pajak' => $total_amount['total_amount_sebelum_pajak'] * $pajak,
                    'total_poin' => floatval($total_amount['total_amount_sebelum_pajak'])/1000,
                    'poin_digunakan' => $penggunaan_poin_trs,
                    'trans_lama_ship' => $estimate_duration,
                    'is_cicilan' => $trx_is_cicilan,
                    'kupon_id' => $diskon['kupon_id']
                ]);
                $date = \Carbon\Carbon::now()->addMinutes(config('expire.time'));

                \Queue::later($date, new \App\Commands\UpdateExpired('unpay'.$random_key));

                // \Queue::later($date, new \App\Commands\SalesOrder());

                $response = self::sprintasiaInsertPayment($transaction_id, $data_customer);
                if (array_key_exists('redirectURL', $response) && array_key_exists('insertStatus', $response)) {
                    if($response['insertStatus'] == "00" AND $response['redirectURL'] != "" ) { // Validate insertStatus and redirectURL for BCA KlikPay
                       $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($transaction_id);
                       $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($transaction_id);

                       if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                           $payment_channel = null;
                       }else {
                           $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                       }
                       $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                       ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                       ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                       ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                       ->first();
                       if ($metode_pembayaran_raw == null) {
                           $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                           ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                           ->first();
                           $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                       }else {
                           $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                       }

                       $send_mail_data = array(
                               'id_transaksi' => $transaction_id,
                               'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                               'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                               'metode_pay' => $metode_pembayaran,
                               'items_order' => $get_data_order_by_trans_id
                       );
                       $admin = DB::table('nm_emailsetting')->first();
                       $admin_email = $admin->es_noreplyemail;
                       Config::set('mail.from.address', $admin_email);
                       // Mail::send(
                       //     'emails.notifikasi_email_transaksi_checkout',
                       //     $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                       //         $get_cus_data_by_trans_id){
                       //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
                       //     }
                       // );

                        // Redirect process
                        return view('sprintasiaInsertPayment')
                            ->with('klikPayCode', $response['redirectData']['klikPayCode'])
                            ->with('transactionNo', $response['redirectData']['transactionNo'])
                            ->with('totalAmount', $response['redirectData']['totalAmount'])
                            ->with('currency', $response['redirectData']['currency'])
                            ->with('payType', $response['redirectData']['payType'])
                            ->with('callback', $response['redirectData']['callback'])
                            ->with('transactionDate', $response['redirectData']['transactionDate'])
                            ->with('descp', $response['redirectData']['descp'])
                            ->with('miscFee', $response['redirectData']['miscFee'])
                            ->with('signature', $response['redirectData']['signature'])
                            ->with('redirectURL', $response['redirectURL']);
                    }else{
                        // Print error message
                        die($response['insertMessage']);
                    }
                }elseif (array_key_exists('insertStatus', $response)) {
                    if ($response['insertStatus'] == "00") { //For BCA VA And Permata VA
                        $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($transaction_id);
                        $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($transaction_id);

                        if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                            $payment_channel = null;
                        }else {
                            $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                        }
                        $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                        ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                        ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                        ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                        ->first();
                        if ($metode_pembayaran_raw == null) {
                            $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                            ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                            ->first();
                            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                        }else {
                            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                        }

                        $send_mail_data = array(
                                'id_transaksi' => $transaction_id,
                                'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                                'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                                'metode_pay' => $metode_pembayaran,
                                'items_order' => $get_data_order_by_trans_id
                        );
                        $admin = DB::table('nm_emailsetting')->first();
                        $admin_email = $admin->es_noreplyemail;
                        Config::set('mail.from.address', $admin_email);
                        // Mail::send(
                        //     'emails.notifikasi_email_transaksi_checkout',
                        //     $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                        //         $get_cus_data_by_trans_id){
                        //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
                        //     }
                        // );

                        unset($_SESSION['cart']);
                        unset($_SESSION['deal_cart']);

                        Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
                        return Redirect::to('info_pembayaran' . '/' . $transaction_id)->with('result', 'Order Anda Telah Disimpan. Silahkan Selesaikan Pembayaran Anda');
                    }else{
                        // Print error message
                        die($response['insertMessage']);
                    }
                }else{
                    // Print error message
                    dd('Something Went Wrong');
                }
            }

        }
        // pembayaran dengan paypal
        elseif ($pay_type == 1) {
            $settings = Home::get_settings();
            foreach ($settings as $s) {
            }
            if ($s->ps_paypal_pay_mode == '0') {
                $mode = 'sandbox';
            } elseif ($s->ps_paypal_pay_mode == '1') {
                $mode = 'live';
            }
            $PayPalMode         = $mode; // sandbox or live
            $PayPalApiUsername  = $s->ps_paypalaccount;

            $PayPalApiPassword  = $s->ps_paypal_api_pw;

            $PayPalApiSignature = $s->ps_paypal_api_signature;

            $PayPalCurrencyCode = $s->ps_curcode;
            $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
            $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel
            //todo balikin
            include '../paypal/paypal.class.php';
            $paypalmode = ($PayPalMode == $mode) ? '.' . $mode : '';
            if ($_POST) //Post Data received from product list page.
            {
                //Other important variables like tax, shipping cost
                if (isset($_POST['tax_price']) && $_POST['tax_price'] != '') {
                    $TotalTaxAmount = $_POST['tax_price']; //Sum of tax for all items in this order.
                } else {
                    $TotalTaxAmount = 0;
                }
                $HandalingCost   = 0.00; //Handling cost for this order.
                $InsuranceCost   = 0.00; //shipping insurance cost for this order.
                $ShippinDiscount = 0.00; //Shipping discount for this order. Specify this as negative number.
                if (isset($_POST['shipping_price_label_hidden']) && $_POST['shipping_price_label_hidden'] != '') {
                    $ShippinCost = $_POST['shipping_price_label_hidden']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
                } else {
                    $ShippinCost = 0;
                }


                $paypal_data    = '';
                $ItemTotalPrice = 0;
                $now            = date('Y-m-d H:i:s');
                $insert_id      = '';
                $random_key     = rand();
                foreach ($_POST['item_name'] as $key => $itmname)
                {
                    $product_code = filter_var($_POST['item_code'][$key], FILTER_SANITIZE_STRING);

                    $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($_POST['item_name'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($_POST['item_code'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($_POST['item_price'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($_POST['item_qty'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_DESC' . $key . '=' . urlencode("Color : " . $_POST['item_color_name'][$key] . " - Size : " . $_POST['item_size_name'][$key]);


                    // item price X quantity

                    $subtotal = ($_POST['item_price'][$key] * $_POST['item_qty'][$key]);

                    //total price
                    $ItemTotalPrice = $ItemTotalPrice + $subtotal;

                    //create items for session
                    $paypal_product['items'][] = array(
                        'itm_name' => $_POST['item_name'][$key],
                        'itm_price' => $_POST['item_price'][$key],
                        'itm_code' => $_POST['item_code'][$key],
                        'itm_qty' => $_POST['item_qty'][$key]

                    );
                    $shipaddresscus = Input::get('fname') . ';' . Input::get('addr_line') . ';' . Input::get('addr1_line') . ';' . Input::get('state') . ';' . Input::get('zipcode') . ';' . Input::get('phone1_line') . ';' . Input::get('email');

                    // $shipaddresscus            = Input::get('fname' . $key) . ',' . Input::get('addr_line' . $key) . ',' . Input::get('addr1_line' . $key) . ',' . Input::get('state' . $key) . ',' . Input::get('zipcode' . $key) . ',' . Input::get('phone1_line' . $key);
                    //print_r($shipaddresscus);
                    //exit();
                    $data                      = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => $_POST['item_code'][$key],
                        'order_type' => $_POST['item_type'][$key],
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => $_POST['item_qty'][$key],
                        'order_amt' => $subtotal,
                        'order_tax' => $_POST['item_tax'][$key],
                        'order_date' => $now,
                        'order_status' => 3,
                        'order_paytype' => '1',
                        'order_pro_color' => $_POST['item_color'][$key],
                        'order_pro_size' => $_POST['item_size'][$key],
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke')
                    );
                    $data_shipping = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 1,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => $_POST['shipping_price_label_hidden'],
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '1',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke')
                    );
                    $data_diskon = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 2,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => $_POST['total_diskon_kupon_hidden'] * -1,
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '1',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke')
                    );
                    if (($_POST['item_type'][$key]) != 2) {
                        Home::purchased_checkout_product_insert($_POST['item_code'][$key], $_POST['item_qty'][$key]);
                    }
                    Home::paypal_checkout_insert($data);

                    $new_insert = DB::getPdo()->lastInsertId();
                    $insert_id .= DB::getPdo()->lastInsertId() . ',';
                    if (Input::get('load_ship' . $key) != 1) {
                          $data = array(
                            'ship_name' => Input::get('fname'),
                            'ship_address1' => Input::get('addr_line'),
                            'ship_address2' => Input::get('addr1_line'),
                            'ship_state' => Input::get('state'),
                            'ship_postalcode' => Input::get('zipcode'),
                            'ship_phone' => Input::get('phone1_line'),
                            'ship_email' => Input::get('email'),
                            'ship_cus_id' => $cust_id,
                            'ship_order_id' => $new_insert
                        );

                        Home::insert_shipping_addr($data, $cust_id);
                    }

                }

                Home::paypal_checkout_insert($data_shipping);
                Home::paypal_checkout_insert($data_diskon);
                Session::put('last_insert_id', trim($insert_id, ','));
                //Grand total including all tax, insurance, shipping cost and discount
                $GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);

                $paypal_product['assets'] = array(
                    'tax_total' => $TotalTaxAmount,
                    'handaling_cost' => $HandalingCost,
                    'insurance_cost' => $InsuranceCost,
                    'shippin_discount' => $ShippinDiscount,
                    'shippin_cost' => $ShippinCost,
                    'grand_total' => $GrandTotal
                );

                //create session array for later use
                $_SESSION["paypal_products"] = $paypal_product;

                //Parameters for SetExpressCheckout, which will be sent to PayPal
                $padata = '&METHOD=SetExpressCheckout' . '&RETURNURL=' . urlencode($PayPalReturnURL) . '&CANCELURL=' . urlencode($PayPalCancelURL) . '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode("SALE") . $paypal_data . '&NOSHIPPING=0' . //set 1 to hide buyer's shipping address, in-case products that does not require shipping
                    '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($ItemTotalPrice) . '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($TotalTaxAmount) . '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($ShippinCost) . '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($HandalingCost) . '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($ShippinDiscount) . '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($InsuranceCost) . '&PAYMENTREQUEST_0_AMT=' . urlencode($GrandTotal) . '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($PayPalCurrencyCode) . '&LOCALECODE=EN' . //PayPal pages to match the language on your website.

                //'&LOGOIMG=http://www.sanwebe.com/wp-content/themes/sanwebe/img/logo.png'. //site logo
                    '&CARTBORDERCOLOR=FFFFFF' . //border color of cart
                    '&ALLOWNOTE=1';

                //We need to execute the "SetExpressCheckOut" method to obtain paypal token
                //salah
                $paypal               = new MyPayPal();
                $httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
                //print_r($httpParsedResponseAr["ACK"]); exit();
                //Respond according to message we receive from Paypal

                if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
                    //Redirect user to PayPal store with Token received.

                    $paypalurl = 'https://www' . $paypalmode . '.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $httpParsedResponseAr["TOKEN"] . '';
                    //header('Location: '.$paypalurl);
                    echo '<script>window.location="' . $paypalurl . '"</script>';
                } else {
                    //Show error message
                    return Redirect::to('index');
                }
            }
        }
        // doku
        elseif ($pay_type == 2) {
            $settings = Home::get_settings();
            //print_r($settings);
            foreach ($settings as $s) {
            }

            if ($s->ps_paypal_pay_mode == '0') {
                $mode = 'sandbox';
            }
            elseif ($s->ps_paypal_pay_mode == '1') {
                $mode = 'live';
            }

            $PayPalMode         = $mode; // sandbox or live
            $PayPalApiUsername  = $s->ps_paypalaccount;
            $PayPalApiPassword  = $s->ps_paypal_api_pw;
            $PayPalApiSignature = $s->ps_paypal_api_signature;
            $PayPalCurrencyCode = $s->ps_curcode;
            $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
            $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel
            //balikin
            //require 'paypal/paypal.class.php';

            $paypalmode = ($PayPalMode == $mode) ? '.' . $mode : '';
            if ($_POST) //Post Data received from product list page.
                {
                if (isset($_POST['tax_price']) && $_POST['tax_price'] != '') {
                    $TotalTaxAmount = $_POST['tax_price']; //Sum of tax for all items in this order.
                } else {
                    $TotalTaxAmount = 0;
                }
                $HandalingCost   = 0.00; //Handling cost for this order.
                $InsuranceCost   = 0.00; //shipping insurance cost for this order.
                $ShippinDiscount = 0.00; //Shipping discount for this order. Specify this as negative number.
                if (isset($_POST['shipping_price_label_hidden']) && $_POST['shipping_price_label_hidden'] != '') {
                    $ShippinCost = $_POST['shipping_price_label_hidden']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
                } else {
                    $ShippinCost = 0;
                }

                $paypal_data    = '';
                $ItemTotalPrice = 0;
                $insert_id      = '';
                $now            = date('Y-m-d H:i:s');
                $random_key     = self::random_key(); //
                $diskon_temp    = 0;
                $i_shipping     = 0;
                $trx_is_cicilan = 0;
                $get_setting_extended   = Settings::get_setting_extended_db();
                foreach ($_POST['item_name'] as $key => $itmname) {
                    $product_code = filter_var($_POST['item_code'][$key], FILTER_SANITIZE_STRING);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($_POST['item_name'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($_POST['item_code'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($_POST['item_price'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($_POST['item_qty'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_DESC' . $key . '=' . urlencode("Color : " . $_POST['item_color_name'][$key] . " - Size : " . $_POST['item_size_name'][$key]);


                    // item price X quantity

                    $get_product = Products::get_product_by_id($_POST['item_code'][$key]);
                    if($get_product->pro_disprice==0 || $get_product->pro_disprice=='' || $get_product->pro_disprice==null){
                        $item_price = $get_product->pro_price;
                    }else {
                        $item_price = $get_product->pro_disprice;
                    }

                    $subtotal = ($item_price * $_POST['item_qty'][$key]);

                    //total price
                    $ItemTotalPrice = $ItemTotalPrice + $subtotal;

                    //Harga Shipping
                    $aKode = $_POST['pengiriman_oke'];
                    $aToZip = $_POST['zipcode'];
                    $aCity = $_POST['city'];
                    $shipping = $this->kirim_request($aKode, $aItems, $aToZip, $aCity);
                    if(array_key_exists('error', $shipping) && $_POST['pengiriman_oke'] != "AL"){
                        dd('error: '.$shipping['error']);
                    }
                    //Harga Diskon
                    $input_kupon = $_POST['input_kupon'];
                    $harga_shipping = $shipping['price']; // live
                    if($_POST['pengiriman_oke'] == "AL")
                    {
                        $harga_shipping = 0;
                        $harga_shipping_per_item = 0;
                    }elseif ($_POST['pengiriman_oke'] == "Columbia") {
                        $harga_shipping_per_item = $shipping['items'][$i_shipping]['cost'];
                    }else {
                        $harga_shipping_per_item = $shipping['items'][$i_shipping]->cost;
                    }
                    // $harga_shipping = 0; //test
                    $postal_code = $_POST['pengiriman_oke'];
                    $carts = $aItems;
                    $diskon = self::cek_kupon($input_kupon, $total_price, $harga_shipping, $postal_code, $carts);

                    // var_dump($get_product->pro_title);
                    // var_dump($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min) ;
                    if($_POST['item_schedp_id'][$key] != 0){
                        $get_flash = Home::promo_flash_time_by_id($_POST['item_pro_id'][$key], $_POST['item_schedp_id'][$key]);
                        $diskon_flash = $get_flash[0]->promop_saving_price * $_POST['item_qty'][$key];
                        $diskon_temp += $diskon_flash;
                    }elseif ($get_product->wholesale_level1_min != 0 || isset($get_product->wholesale_level1_min) || !empty($get_product->wholesale_level1_min)) {
                        if($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min)
                        {
                            // var_dump($get_product->wholesale_level1_min>1);
                            if ($get_product->wholesale_level5_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level5_min && $get_product->wholesale_level5_min<=$_POST['item_qty'][$key]):
                                // dd('test');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level4_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level4_min && $get_product->wholesale_level4_min<=$_POST['item_qty'][$key]):
                                // dd('test2');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level3_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level3_min && $get_product->wholesale_level3_min<=$_POST['item_qty'][$key]):
                                // dd('test3');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level2_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level2_min && $get_product->wholesale_level2_min<=$_POST['item_qty'][$key]):
                                // dd('test4');
                                if($get_product->pro_disprice != 0){
                                        $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                    }else{
                                        $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                    }
                            elseif ($get_product->wholesale_level1_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level1_min && $get_product->wholesale_level1_min<= $_POST['item_qty'][$key]):
                                // dd('test5');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                }
                            endif;
                        }
                    }
                    //create items for session_id

                    $paypal_product['items'][] = array(
                        'itm_name' => $_POST['item_name'][$key],
                        'itm_price' => $_POST['item_price'][$key],
                        'itm_code' => $_POST['item_code'][$key],
                            'itm_qty' => $_POST['item_qty'][$key]

                    );
                    $shipaddresscus = Input::get('fname') . ';' . Input::get('addr_line') . ';' . Input::get('addr1_line') . ';' . Input::get('state') . ';' . Input::get('zipcode') . ';' . Input::get('phone1_line') . ';' . Input::get('email');

                    $jumlah_warranty = 0;

                    if($_POST['item_warranty_val'][$key] == 1)
                    {
                        $jumlah_warranty = floatval($get_setting_extended[0]->besaran_warranty)*floatval($subtotal)/100;
                    }

                    $is_cicilan = 0;
                    if (array_key_exists(1, $_POST['cicilan'])) {
                        if ($_POST['cicilan'][$key] == 1) {
                            $is_cicilan = 1;
                            $trx_is_cicilan = 1;
                        }
                    }

                    $data  = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => $_POST['item_code'][$key],
                        'order_type' => $_POST['item_type'][$key],
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => $_POST['item_qty'][$key],
                        'order_amt' => $subtotal,
                        'order_tax' => $_POST['item_tax'][$key],
                        'order_date' => $now,
                        'order_status' => 3,
                        'order_paytype' => '2',
                        'order_pro_color' => $_POST['item_color'][$key],
                        'order_pro_size' => $_POST['item_size'][$key],
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'order_shipping_price' => $harga_shipping_per_item,
                        'order_use_warranty' => $_POST['item_warranty_val'][$key],
                        'order_jumlah_warranty' => $jumlah_warranty,
                        'order_is_cicilan' => $is_cicilan,
                        'payment_channel' => Input::get('payment_channel')
                    );
                    $data_shipping = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 1,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => $harga_shipping, //new
                        // 'order_amt' => $_POST['shipping_price_label_hidden'], //old
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '2',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'payment_channel' => Input::get('payment_channel')
                    );
                    $data_diskon = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 2,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => ($diskon['total_diskon'] + $diskon_temp) * -1,
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '2',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'payment_channel' => Input::get('payment_channel')
                    );
                    if (($_POST['item_type'][$key]) != 2) {

                        $add_purchase = Home::purchased_checkout_product_insert($_POST['item_code'][$key], $_POST['item_qty'][$key], $_POST['item_schedp_id'][$key]);
                        if($add_purchase == 'Failed'){
                            return Redirect::to('addtocart')->with('session_result', 'Stok Tidak Mencukupi');
                        }elseif ($add_purchase == 'Failed Flash') {
                            return Redirect::to('addtocart')->with('session_result', 'Stok Flash Tidak Mencukupi');
                        }
                    }

                    Home::paypal_checkout_insert($data);

                    // echo "Selesai NM ORDER";

                    $new_insert = DB::getPdo()->lastInsertId();
                    $insert_id .= DB::getPdo()->lastInsertId() . ',';
                    if (Input::get('load_ship' . $key) != 1) {
                        if(Input::get('pengiriman_oke') == 'PopBox') {

                            $data = array(
                                'ship_name' => Input::get('fname'),
                                'ship_address1' => Input::get('addr_line'),
                                'ship_address2' => Input::get('addr1_line'),
                                'ship_state' => Input::get('state'),
                                'ship_postalcode' => Input::get('zipcode'),
                                'ship_phone' => Input::get('phone1_line'),
                                'ship_email' => Input::get('email'),
                                'country' => Input::get('country_popbox_input'),
                                'city' => Input::get('city_popbox_input'),
                                'shipping_ref_number' => Input::get('shipping_reg_number'),
                                'ship_cus_id' => $cust_id,
                                'ship_order_id' => $new_insert
                            );

                            Home::insert_shipping_addr($data, $cust_id);
                        }
                        else
                        {
                            //echo("Masuk Shipping");
                            $data = array(
                                'ship_name' => Input::get('fname'),
                                'ship_address1' => Input::get('addr_line'),
                                'ship_address2' => Input::get('addr1_line'),
                                'ship_state' => Input::get('state'),
                                'ship_postalcode' => Input::get('zipcode'),
                                'ship_phone' => Input::get('phone1_line'),
                                'ship_email' => Input::get('email'),
                                'ship_cus_id' => $cust_id,
                                'ship_order_id' => $new_insert,
                                'ship_ci_id' => Input::get('id_city_ha'),
                                'ship_country' => Input::get('country'),
                                'ship_dis_id' => Input::get('district'),
                                'ship_sdis_id' => Input::get('subdistrict'),
                            );

                            Home::insert_shipping_addr($data, $cust_id);

                            //echo("End Shipiing");
                        }
                    }
                    $i_shipping++;
                }
                Home::paypal_checkout_insert($data_shipping);
                Home::paypal_checkout_insert($data_diskon);
                //echo "Sukses Insert Data";

                Session::put('last_insert_id', trim($insert_id, ','));
                $trans_id = 'unpay'.$random_key;
                $mallid = config('dokularavel.MALL_ID');
                $currency = config('dokularavel.CURRENCY');
                $purchasecurrency = config('dokularavel.CURRENCY');
                $sharedkey = config('dokularavel.SHARED_KEY');
                $live_mode = config('dokularavel.LIVE_MODE');
                $chainmerchant = 'NA';
                $pajak = 0;
                $data_submit = Home::get_data_order($trans_id);
                $data_amount = Home::get_total_amount($trans_id);
                $total_amount = round($data_amount['total_amount']);
                $total_amount = number_format($total_amount, 2, '.', '');
                date_default_timezone_set('Asia/Jakarta');
                $date = date('YmdHis');
                $basket = "";
                foreach($data_submit as $data_sub){
                    $produk = DB::table('nm_product')->where('pro_id', $data_sub->order_pro_id)->first();
                    if ($data_sub->order_pro_id == 1 || $data_sub->order_pro_id == 2) {
                        $basket .= $produk->pro_title.','.$data_sub->order_amt.','.$data_sub->order_qty.','.$data_sub->order_amt.';';
                    }else {
                        $basket .= $produk->pro_title.','.$produk->pro_price.','.$data_sub->order_qty.','.$data_sub->order_amt.';';
                    }
                }
                $paymentchannel = Input::get('payment_channel');
                $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
                $sessionid = '';
                $bookingcode = '';
                for ($j=0; $j < 20; $j++) {
                    $rnum = floor(lcg_value() * strlen($chars));
                    $sessionid .= substr($chars, $rnum, 1);
                }
                for ($k=0; $k < 6; $k++) {
                    $rnum = floor(lcg_value() * strlen($chars));
                    $bookingcode .= substr($chars, $rnum, 1);
                }
                $words = sha1($total_amount.$mallid.$sharedkey.$trans_id);
                $email = Input::get('email');
                $name = Input::get('fname');
                $address = Input::get('addr_line');
                $country = '360';
                $mobilephone = Input::get('phone1_line');
                if($live_mode == FALSE){
                    $url_doku = 'http://staging.doku.com/Suite/Receive';
                }elseif ($live_mode == TRUE) {
                    $url_doku = 'https://pay.doku.com/Suite/Receive';
                }else {
                    dd('error: Live Mode must be set');
                }



                // -- detail customer --
                $get_data_cus = Home::get_data_detail_cus(Session::get('customerid'));

                // -- penggunaan poin --
                if(Input::get('chk_poin') == 1)
                {
                    // -- jumlah pemakaian poin
                    $penggunaan_poin_trs = $get_data_cus->total_poin;
                    // -- jumlah belanja dikurang poin
                    $data_amount['total_amount_sebelum_pajak'] = $data_amount['total_amount_sebelum_pajak'] - floatval($penggunaan_poin_trs);
                    Home::update_poin_dipakai(Session::get('customerid'));
                }
                else
                {
                    $penggunaan_poin_trs = 0;
                }

                if (array_key_exists('est_duration', $shipping)) {
                    $estimate_duration = $shipping['est_duration'];
                }else {
                    $estimate_duration = 0;
                }

                $data_amount['total_amount'] = $data_amount['total_amount'] - $penggunaan_poin_trs;
                $insert_transaksi = DB::table('nm_transaksi')->insert([
                    'transaction_id' => $trans_id,
                    'total_belanja' => $data_amount['total_amount_sebelum_pajak'],
                    'diskon' => $data_amount['total_diskon'],
                    'diskon_voucher' => $diskon['total_diskon'],
                    'shipping' => $data_amount['total_shipping'],
                    'metode_pembayaran' => 'Doku',
                    'status_pembayaran' => 'belum dibayar',
                    'pajak' => $data_amount['total_amount_sebelum_pajak'] * $pajak,
                    'total_poin' => floatval($data_amount['total_amount_sebelum_pajak'])/1000,
                    'poin_digunakan' => $penggunaan_poin_trs,
                    'trans_lama_ship' => $estimate_duration,
                    'is_cicilan' => $trx_is_cicilan,
                    'kupon_id' => $diskon['kupon_id']
                ]);
                $update_data_order = DB::table('nm_order')
                ->where('transaction_id', $trans_id)->update([
                    'words' => $words,
                    'payer_email' => $email,
                    'payer_name' => $name,
                    'session_id' => $sessionid
                ]);

                $date_queue = \Carbon\Carbon::now()->addMinutes(config('expire.time'));

                \Queue::later($date_queue, new \App\Commands\UpdateExpired('unpay'.$random_key));

                // \Queue::later($date, new \App\Commands\SalesOrder());
                //$this->recall_unsync(); di panggil ketika payment sukses
                // return redirect('doku?trans_id='.$trans_id);

                // imel transfer bank
               $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($trans_id);
               $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($trans_id);

               if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                   $payment_channel = null;
               }else {
                   $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
               }
               $metode_pembayaran_raw = DB::table('nm_payment_method_header')
               ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
               ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
               ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
               ->first();
               if ($metode_pembayaran_raw == null) {
                   $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                   ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                   ->first();
                   $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
               }else {
                   $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
               }
            //    echo "selesai query";

               $send_mail_data = array(
                       'id_transaksi' => $trans_id,
                       'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                       'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                       'metode_pay' => $metode_pembayaran,
                       'items_order' => $get_data_order_by_trans_id
               );
               $admin = DB::table('nm_emailsetting')->first();
               $admin_email = $admin->es_noreplyemail;
               Config::set('mail.from.address', $admin_email);
               // Mail::send(
               //     'emails.notifikasi_email_transaksi_checkout',
               //     $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
               //         $get_cus_data_by_trans_id){
               //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
               //     }
               // );

                //--- for email notifikasi transaction --
                // $send_mail_data = array(
                //     'first_name' => '8989',
                //     'password' => '9999'
                // );
                // Mail::send(
                //     'emails.notifikasi_email_transaksi_checkout',$send_mail_data, function ($message) use ($get_data_cus){
                //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Transaksi');
                //     }
                // );

                return view('doku')
                ->with('URL', $url_doku)
                ->with('BASKET', $basket)
                ->with('MALLID', $mallid)
                ->with('CHAINMERCHANT', $chainmerchant)
                ->with('CURRENCY', $currency)
                ->with('PURCHASECURRENCY', $purchasecurrency)
                ->with('AMOUNT', $data_amount['total_amount'])
                ->with('PURCHASEAMOUNT', $data_amount['total_amount'])
                ->with('TRANSIDMERCHANT', $trans_id)
                ->with('PAYMENTCHANNEL', $paymentchannel)
                ->with('SHAREDKEY', $sharedkey)
                ->with('WORDS', $words)
                ->with('REQUESTDATETIME', $date)
                ->with('SESSIONID', $sessionid)
                ->with('EMAIL', $email)
                ->with('NAME', $name)
                ->with('ADDRESS', $address)
                ->with('COUNTRY', $country)
                ->with('MOBILEPHONE', $mobilephone);

                // $data_curl = [
                //     'BASKET' => $basket,
                //     'MALLID' => $mallid,
                //     'CHAINMERCHANT' => $chainmerchant,
                //     'CURRENCY' => $currency,
                //     'PURCHASECURRENCY' => $purchasecurrency,
                //     'AMOUNT' => $total_amount,
                //     'PURCHASEAMOUNT' => $total_amount,
                //     'TRANSIDMERCHANT' => $trans_id,
                //     'PAYMENTCHANNEL' => $paymentchannel,
                //     'SHAREDKEY' => $sharedkey,
                //     'WORDS' => $words,
                //     'REQUESTDATETIME' => $date,
                //     'SESSIONID' => $sessionid,
                //     'EMAIL' => $email,
                //     'NAME' => $name,
                //     'ADDRESS' => $address,
                //     'COUNTRY' => $country,
                //     'MOBILEPHONE' => $mobilephone
                // ];
                // $URL_insert = "http://staging.doku.com/Suite/Receive";
                // $OPT        = http_build_query($data_curl);
                //
                // $ch = curl_init();
                // curl_setopt($ch, CURLOPT_URL, $URL_insert);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $OPT);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 30);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                //
                // $raw_response = curl_exec($ch);
                // if(curl_error($ch)){
                //     die("CURL Error ::". curl_error($ch));
                // }
                // curl_close($ch);
                //
                // $response   = json_decode($raw_response,TRUE);
                // // dd($response);
                // return $response;
            }
        }
        // pembayaran columbia
        elseif ($pay_type == 4) {
            // echo("masuk esle columbia");
            $settings = Home::get_settings();
            //print_r($settings);
            foreach ($settings as $s) {
            }

            if ($s->ps_paypal_pay_mode == '0') {
                $mode = 'sandbox';
            }
            elseif ($s->ps_paypal_pay_mode == '1') {
                $mode = 'live';
            }

            $PayPalMode         = $mode; // sandbox or live
            $PayPalApiUsername  = $s->ps_paypalaccount;

            $PayPalApiPassword  = $s->ps_paypal_api_pw;

            $PayPalApiSignature = $s->ps_paypal_api_signature;

            $PayPalCurrencyCode = $s->ps_curcode;
            $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
            $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel
            //balikin
            //require 'paypal/paypal.class.php';

            $paypalmode = ($PayPalMode == $mode) ? '.' . $mode : '';

            if ($_POST) //Post Data received from product list page.
                {
                    // var_dump("masuk if post");
                if (isset($_POST['tax_price']) && $_POST['tax_price'] != '') {
                    $TotalTaxAmount = $_POST['tax_price']; //Sum of tax for all items in this order.
                } else {
                    $TotalTaxAmount = 0;
                }
                $HandalingCost   = 0.00; //Handling cost for this order.
                $InsuranceCost   = 0.00; //shipping insurance cost for this order.
                $ShippinDiscount = 0.00; //Shipping discount for this order. Specify this as negative number.
                if (isset($_POST['shipping_price_label_hidden']) && $_POST['shipping_price_label_hidden'] != '') {
                    $ShippinCost = $_POST['shipping_price_label_hidden']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
                } else {
                    $ShippinCost = 0;
                }

                $paypal_data    = '';
                $ItemTotalPrice = 0;
                $insert_id      = '';
                $now            = date('Y-m-d H:i:s');
                $random_key     = self::random_key(); //
                $kode           = 'unpay'.$random_key;
                $diskon_temp = 0;
                $jumlah_belanja_untuk_detail_transaksi = 0;
                $get_setting_extended   = Settings::get_setting_extended_db();

                $i_shipping = 0;
                $trx_is_cicilan = 0;
                foreach ($_POST['item_name'] as $key => $itmname) {

                    $total_apalah = floatval($_POST['item_price'][$key])*floatval($_POST['item_qty'][$key]);
                    $jumlah_belanja_untuk_detail_transaksi = $jumlah_belanja_untuk_detail_transaksi + $total_apalah;

                    $product_code = filter_var($_POST['item_code'][$key], FILTER_SANITIZE_STRING);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($_POST['item_name'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($_POST['item_code'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($_POST['item_price'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($_POST['item_qty'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_DESC' . $key . '=' . urlencode("Color : " . $_POST['item_color_name'][$key] . " - Size : " . $_POST['item_size_name'][$key]);


                    // item price X quantity
                    $get_product = Products::get_product_by_id($_POST['item_code'][$key]);
                    if ($get_product->pro_disprice==0 || $get_product->pro_disprice=='' || $get_product->pro_disprice==null){
                        $item_price = $get_product->pro_price;
                    } else {
                        $item_price = $get_product->pro_disprice;
                    }

                    $subtotal = ($item_price * $_POST['item_qty'][$key]);

                    //total price
                    $ItemTotalPrice = $ItemTotalPrice + $subtotal;
                    //  echo $subtotal;
                    //  echo $ItemTotalPrice;

                     //Harga Shipping
                     $aKode = $_POST['pengiriman_oke'];
                     $aToZip = $_POST['zipcode'];
                     $aCity = $_POST['city'];
                     $shipping = $this->kirim_request($aKode, $aItems, $aToZip, $aCity);
                     if(array_key_exists('error', $shipping) && $_POST['pengiriman_oke'] != "AL"){
                         dd('error: '.$shipping['error']);
                     }
                     //Harga Diskon
                     $input_kupon = $_POST['input_kupon'];
                     $harga_shipping = $shipping['price']; //live
                     if($_POST['pengiriman_oke'] == "AL")
                     {
                         $harga_shipping = 0;
                         $harga_shipping_per_item = 0;
                     }elseif ($_POST['pengiriman_oke'] == "Columbia") {
                         $harga_shipping_per_item = $shipping['items'][$i_shipping]['cost'];
                     }else {
                         $harga_shipping_per_item = $shipping['items'][$i_shipping]->cost;
                     }
                    //   $harga_shipping = 0; //test
                     $postal_code = $_POST['pengiriman_oke'];
                     $carts = $aItems;
                     $diskon = self::cek_kupon($input_kupon, $total_price, $harga_shipping, $postal_code, $carts);

                    //  var_dump($get_product->pro_title);
                    //  var_dump($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min) ;
                     if($_POST['item_schedp_id'][$key] != 0){
                         $get_flash = Home::promo_flash_time_by_id($_POST['item_pro_id'][$key], $_POST['item_schedp_id'][$key]);
                         $diskon_flash = $get_flash[0]->promop_saving_price * $_POST['item_qty'][$key];
                         $diskon_temp += $diskon_flash;
                     }elseif ($get_product->wholesale_level1_min != 0 || isset($get_product->wholesale_level1_min) || !empty($get_product->wholesale_level1_min)) {
                         if($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min)
                         {
                            //  var_dump($get_product->wholesale_level1_min>1);
                             if ($get_product->wholesale_level5_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level5_min && $get_product->wholesale_level5_min<=$_POST['item_qty'][$key]):
                                 // dd('test');
                                 if($get_product->pro_disprice != 0){
                                     $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                 }else{
                                     $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                 }
                             elseif ($get_product->wholesale_level4_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level4_min && $get_product->wholesale_level4_min<=$_POST['item_qty'][$key]):
                                 // dd('test2');
                                 if($get_product->pro_disprice != 0){
                                     $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                 }else{
                                     $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                 }
                             elseif ($get_product->wholesale_level3_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level3_min && $get_product->wholesale_level3_min<=$_POST['item_qty'][$key]):
                                 // dd('test3');
                                 if($get_product->pro_disprice != 0){
                                     $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                 }else{
                                     $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                 }
                             elseif ($get_product->wholesale_level2_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level2_min && $get_product->wholesale_level2_min<=$_POST['item_qty'][$key]):
                                 // dd('test4');
                                 if($get_product->pro_disprice != 0){
                                         $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                     }else{
                                         $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                     }
                             elseif ($get_product->wholesale_level1_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level1_min && $get_product->wholesale_level1_min<= $_POST['item_qty'][$key]):
                                 // dd('test5');
                                 if($get_product->pro_disprice != 0){
                                     $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                 }else{
                                     $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                 }
                             endif;
                         }
                     }
                    //create items for session_id

                    $paypal_product['items'][] = array(
                        'itm_name' => $_POST['item_name'][$key],
                        'itm_price' => $_POST['item_price'][$key],
                        'itm_code' => $_POST['item_code'][$key],
                        'itm_qty' => $_POST['item_qty'][$key]

                    );
                    $shipaddresscus = Input::get('fname') . ';' . Input::get('addr_line') . ';' . Input::get('addr1_line') . ';' . Input::get('state') . ';' . Input::get('zipcode') . ';' . Input::get('phone1_line') . ';' . Input::get('email');

                    $dp = 0.4 * $subtotal;
                    $order_installment = $subtotal - $dp / 3;

                    $jumlah_warranty = 0;

                    if($_POST['item_warranty_val'][$key] == 1)
                    {
                        $jumlah_warranty = floatval($get_setting_extended[0]->besaran_warranty)*floatval($subtotal)/100;
                    }

                    $is_cicilan = 0;
                    if (array_key_exists(1, $_POST['cicilan'])) {
                        if ($_POST['cicilan'][$key] == 1) {
                            $is_cicilan = 1;
                            $trx_is_cicilan = 1;
                        }
                    }

                    $data  = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => $_POST['item_code'][$key],
                        'order_type' => $_POST['item_type'][$key],
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => $_POST['item_qty'][$key],
                        'order_amt' => $subtotal,
                        'order_tax' => $_POST['item_tax'][$key],
                        'order_date' => $now,
                        'order_status' => 3,
                        'order_paytype' => '4',
                        'order_pro_color' => $_POST['item_color'][$key],
                        'order_pro_size' => $_POST['item_size'][$key],
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'order_use_warranty' => $_POST['item_warranty_val'][$key],
                        'order_shipping_price' => $harga_shipping_per_item,
                        'order_jumlah_warranty' => $jumlah_warranty,
                        'order_is_cicilan' => $is_cicilan
                    );
                    $data_shipping = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 1,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => $harga_shipping, //new
                        // 'order_amt' => $_POST['shipping_price_label_hidden'], //old
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '4',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke')
                    );
                    $data_diskon = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 2,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => ($diskon['total_diskon'] + $diskon_temp) * -1,
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '4',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke')
                    );

                    //dd($data_shipping);

                    if (($_POST['item_type'][$key]) != 2) {

                        $add_purchase = Home::purchased_checkout_product_insert($_POST['item_code'][$key], $_POST['item_qty'][$key], $_POST['item_schedp_id'][$key]);
                        if($add_purchase == 'Failed'){
                            return Redirect::to('addtocart')->with('session_result', 'Stok Tidak Mencukupi');
                        }elseif ($add_purchase == 'Failed Flash') {
                            return Redirect::to('addtocart')->with('session_result', 'Stok Flash Tidak Mencukupi');
                        }
                    }

                    Home::paypal_checkout_insert($data);

                    // echo "selesai inser nm_order";

                    $new_insert = DB::getPdo()->lastInsertId();
                    $insert_id .= DB::getPdo()->lastInsertId() . ',';
                    if (Input::get('load_ship' . $key) != 1) {
                        if(Input::get('pengiriman_oke') == 'PopBox') {
                            // echo "masuk popbox";
                            $data = array(
                                'ship_name' => Input::get('fname'),
                                'ship_address1' => Input::get('addr_line'),
                                'ship_address2' => Input::get('addr1_line'),
                                'ship_state' => Input::get('state'),
                                'ship_postalcode' => Input::get('zipcode'),
                                'ship_phone' => Input::get('phone1_line'),
                                'ship_email' => Input::get('email'),
                                'country' => Input::get('country_popbox_input'),
                                'city' => Input::get('city_popbox_input'),
                                'shipping_ref_number' => Input::get('shipping_reg_number'),
                                'ship_cus_id' => $cust_id,
                                'ship_order_id' => $new_insert
                            );

                            Home::insert_shipping_addr($data, $cust_id);
                        }
                        else
                        {
                            //echo("Masuk Shipping");
                            $data = array(
                                'ship_name' => Input::get('fname'),
                                'ship_address1' => Input::get('addr_line'),
                                'ship_address2' => Input::get('addr1_line'),
                                'ship_state' => Input::get('state'),
                                'ship_postalcode' => Input::get('zipcode'),
                                'ship_phone' => Input::get('phone1_line'),
                                'ship_email' => Input::get('email'),
                                'ship_cus_id' => $cust_id,
                                'ship_order_id' => $new_insert,
                                'ship_ci_id' => Input::get('id_city_ha'),
                                'ship_country' => Input::get('country'),
                                'ship_dis_id' => Input::get('district'),
                                'ship_sdis_id' => Input::get('subdistrict'),
                            );

                            Home::insert_shipping_addr($data, $cust_id);

                            //echo("End Shipiing");
                        }
                    }
                    $i_shipping++;
                }
                Home::paypal_checkout_insert($data_shipping);
                $new_insert = DB::getPdo()->lastInsertId();
                $insert_id .= DB::getPdo()->lastInsertId() . ',';
                Home::paypal_checkout_insert($data_diskon);
                $new_insert = DB::getPdo()->lastInsertId();
                $insert_id .= DB::getPdo()->lastInsertId() . ',';

                //dd("end");



                // -- detail customer --
                $get_data_cus = Home::get_data_detail_cus(Session::get('customerid'));

                // -- penggunaan poin --
                if(Input::get('chk_poin') == 1)
                {
                    // -- jumlah pemakaian poin
                    $penggunaan_poin_trs = $get_data_cus->total_poin;
                    // -- jumlah belanja dikurang poin
                    $jumlah_belanja_untuk_detail_transaksi = $jumlah_belanja_untuk_detail_transaksi - floatval($penggunaan_poin_trs);

                    Home::update_poin_dipakai(Session::get('customerid'));
                }
                else
                {
                    $penggunaan_poin_trs = 0;
                }

                // total yang harus di bayar
                $tot_yuhu = floatval($jumlah_belanja_untuk_detail_transaksi) - floatval($diskon['total_diskon'] + $diskon_temp) + floatval($harga_shipping);

                //dp 30%
                $dp_columbia = $tot_yuhu * 0.3;

                // -- getting lama cicilan --
                $get_pay_settings = Settings::get_pay_settings();
                //dd($get_pay_settings);
                //cicilan perbulan
                $cicilan_perbulan = ($tot_yuhu - $dp_columbia) / floatval($get_pay_settings[0]->ps_lama_cicilan_columbia);

                if (array_key_exists('est_duration', $shipping)) {
                    $estimate_duration = $shipping['est_duration'];
                }else {
                    $estimate_duration = 0;
                }

                $data_detail_transaksi = array (
                    'transaction_id' => 'unpay'.$random_key,
                    'diskon' => floatval($diskon['total_diskon'] + $diskon_temp),
                    'diskon_voucher' => $diskon['total_diskon'],
                    'shipping' => floatval($harga_shipping),
                    'metode_pembayaran' => 'Columbia',
                    'status_pembayaran' => 'belum dibayar',
                    'total_belanja' => $ItemTotalPrice,
                    'pajak' => floatval($jumlah_belanja_untuk_detail_transaksi)*0,
                    'transaksi_dp' => $dp_columbia,
                    'lama_cicilan' => $get_pay_settings[0]->ps_lama_cicilan_columbia,
                    'adminfee' => '50000',
                    'cicilan_perbulan' => $cicilan_perbulan,
                    'total_poin' => floatval($jumlah_belanja_untuk_detail_transaksi)/1000,
                    'poin_digunakan' => $penggunaan_poin_trs,
                    'trans_lama_ship' => $estimate_duration,
                    'is_cicilan' => $trx_is_cicilan,
                    'kupon_id' => $diskon['kupon_id']
                    );

                // echo "end data detail transaksi";
                // push ke queue
                $date = \Carbon\Carbon::now()->addMinutes(config('expire.time'));

                \Queue::later($date, new \App\Commands\UpdateExpired('unpay'.$random_key));

                //\Queue::later($date, new \App\Commands\SalesOrder());
                //---------------------------------------------------------------------------

                Home::insert_header_transaksi($data_detail_transaksi);

                Session::put('last_insert_id', trim($insert_id, ','));

                $trans_id = $new_insert;

                $currenttransactionorderid = base64_encode(Session::get('last_insert_id'));

                unset($_SESSION["cart"]);
                // echo "selesai queue";

                //--- for email transaction --
                // imel columbia
                $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($kode);
                $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($kode);
                if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                    $payment_channel = null;
                }else {
                    $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                }
                $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                ->first();
                if ($metode_pembayaran_raw == null) {
                    $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                    ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                    ->first();
                    $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                }else {
                    $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                }
                // echo "selesai query";

                $send_mail_data = array(
                        'id_transaksi' => $kode,
                        'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                        'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                        'metode_pay' => $metode_pembayaran,
                        'items_order' => $get_data_order_by_trans_id
                );
                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                // Mail::send(
                //     'emails.notifikasi_email_transaksi_checkout',
                //     $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                //         $get_cus_data_by_trans_id){
                //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
                //     }
                // );
                // echo "selesai mail";

                // -- redirect --
                return Redirect::to('show_payment_result_columbia' . '/' . 'unpay'.$random_key)->with('result', 'Pesanan Anda Akan Diproses');
            }
        }

        // pembayaran transfer bank
        elseif ($pay_type == 5) {
            //dd($_POST);

            $settings = Home::get_settings();
            //print_r($settings);
            foreach ($settings as $s) {
            }

            if ($s->ps_paypal_pay_mode == '0') {
                $mode = 'sandbox';
            }
            elseif ($s->ps_paypal_pay_mode == '1') {
                $mode = 'live';
            }

            $PayPalMode         = $mode; // sandbox or live
            $PayPalApiUsername  = $s->ps_paypalaccount;

            $PayPalApiPassword  = $s->ps_paypal_api_pw;

            $PayPalApiSignature = $s->ps_paypal_api_signature;

            $PayPalCurrencyCode = $s->ps_curcode;
            $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
            $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel

            $paypalmode = ($PayPalMode == $mode) ? '.' . $mode : '';
            $get_setting_extended   = Settings::get_setting_extended_db();
            if ($_POST) //Post Data received from product list page.
                {
                if (isset($_POST['tax_price']) && $_POST['tax_price'] != '') {
                    $TotalTaxAmount = $_POST['tax_price']; //Sum of tax for all items in this order.
                } else {
                    $TotalTaxAmount = 0;
                }
                $HandalingCost   = 0.00; //Handling cost for this order.
                $InsuranceCost   = 0.00; //shipping insurance cost for this order.
                $ShippinDiscount = 0.00; //Shipping discount for this order. Specify this as negative number.
                if (isset($_POST['shipping_price_label_hidden']) && $_POST['shipping_price_label_hidden'] != '') {
                    $ShippinCost = $_POST['shipping_price_label_hidden']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
                } else {
                    $ShippinCost = 0;
                }

                $paypal_data    = '';
                $ItemTotalPrice = 0;
                $insert_id      = '';
                $now            = date('Y-m-d H:i:s');
                $random_key     = self::random_key(); //
                $kode           = 'unpay'.$random_key;
                $diskon_temp = 0;
                $jumlah_belanja_untuk_detail_transaksi = 0;
                $total_poin = 0;
                // dd($_POST);
                $i_shipping = 0;
                $trx_is_cicilan = 0;
                foreach ($_POST['item_name'] as $key => $itmname)
                {
                    $total_apalah = floatval($_POST['item_price'][$key])*floatval($_POST['item_qty'][$key]);
                    $jumlah_belanja_untuk_detail_transaksi = $jumlah_belanja_untuk_detail_transaksi + $total_apalah;

                    $total_poin = $total_poin + floatval($_POST['item_poin'][$key]);

                    $product_code = filter_var($_POST['item_code'][$key], FILTER_SANITIZE_STRING);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($_POST['item_name'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($_POST['item_code'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($_POST['item_price'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($_POST['item_qty'][$key]);
                    $paypal_data .= '&L_PAYMENTREQUEST_0_DESC' . $key . '=' . urlencode("Color : " . $_POST['item_color_name'][$key] . " - Size : " . $_POST['item_size_name'][$key]);


                    // item price X quantity
                    // if()

                    $get_product = Products::get_product_by_id($_POST['item_code'][$key]);
                    // dd($get_product);
                    //dd($_POST);
                    if ($get_product->pro_disprice==0 || $get_product->pro_disprice=='' || $get_product->pro_disprice==null){
                        $item_price = $get_product->pro_price;
                    } else {
                        $item_price = $get_product->pro_disprice;
                    }

                    $subtotal = ($item_price * $_POST['item_qty'][$key]);
                    // echo ($subtotal);
                    //total price
                    $ItemTotalPrice = $ItemTotalPrice + $subtotal;
                    // echo $subtotal;
                    // echo $ItemTotalPrice;
                    //dd($ItemTotalPrice);

                    //Harga Shipping
                    $aKode = $_POST['pengiriman_oke'];
                    $aToZip = $_POST['zipcode'];
                    $aCity = $_POST['city'];

                    $shipping = $this->kirim_request($aKode, $aItems, $aToZip, $aCity);
                    if(array_key_exists('error', $shipping) && $_POST['pengiriman_oke'] != "AL"){
                        dd('error: '.$shipping['error']);
                    }
                    //Harga Diskon
                    $input_kupon = $_POST['input_kupon'];
                    //dd($_POST);
                   // $saving_price = $_POST['saving_price'][$key];
                    $harga_shipping = $shipping['price']; //live
                    if($_POST['pengiriman_oke'] == "AL")
                    {
                        $harga_shipping = 0;
                        $harga_shipping_per_item = 0;
                    }elseif ($_POST['pengiriman_oke'] == "Columbia") {
                        $harga_shipping_per_item = $shipping['items'][$i_shipping]['cost'];
                    }else {
                        $harga_shipping_per_item = $shipping['items'][$i_shipping]->cost;
                    }
                    //  $harga_shipping = 0; //test
                    $postal_code = $_POST['pengiriman_oke'];
                    $carts = $aItems;
                    $diskon = self::cek_kupon($input_kupon, $total_price, $harga_shipping, $postal_code, $carts);
                    //  dd($diskon);
                    // var_dump($get_product->pro_title);
                    // var_dump($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min) ;
                    if($_POST['item_schedp_id'][$key] != 0){
                        // dd('test');
                        $get_flash = Home::promo_flash_time_by_id($_POST['item_pro_id'][$key], $_POST['item_schedp_id'][$key]);
                        $diskon_flash = $get_flash[0]->promop_saving_price * $_POST['item_qty'][$key];
                        $diskon_temp += $diskon_flash;
                    }elseif ($get_product->wholesale_level1_min != 0 || isset($get_product->wholesale_level1_min) || !empty($get_product->wholesale_level1_min)) {
                        if($_POST['item_qty'][$key] >= $get_product->wholesale_level1_min)
                        {
                            // var_dump($get_product->wholesale_level1_min>1);
                            if ($get_product->wholesale_level5_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level5_min && $get_product->wholesale_level5_min<=$_POST['item_qty'][$key]):
                                // dd('test');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level5_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level4_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level4_min && $get_product->wholesale_level4_min<=$_POST['item_qty'][$key]):
                                // dd('test2');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level4_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level3_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level3_min && $get_product->wholesale_level3_min<=$_POST['item_qty'][$key]):
                                // dd('test3');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level3_price) * $_POST['item_qty'][$key]);
                                }
                            elseif ($get_product->wholesale_level2_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level2_min && $get_product->wholesale_level2_min<=$_POST['item_qty'][$key]):
                                // dd('test4');
                                if($get_product->pro_disprice != 0){
                                        $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                    }else{
                                        $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level2_price) * $_POST['item_qty'][$key]);
                                    }
                            elseif ($get_product->wholesale_level1_min>1 && $_POST['item_qty'][$key] >= $get_product->wholesale_level1_min && $get_product->wholesale_level1_min<= $_POST['item_qty'][$key]):
                                // dd('test5');
                                if($get_product->pro_disprice != 0){
                                    $diskon_temp += floatval(($get_product->pro_disprice - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                }else{
                                    $diskon_temp += floatval(($get_product->pro_price - $get_product->wholesale_level1_price) * $_POST['item_qty'][$key]);
                                }
                            endif;
                        }
                    }
                    // var_dump($diskon_temp);

                    // if(empty($_POST['item_schedp_id'][$key]) || $_POST['item_schedp_id'][$key] == null){
                    //     // dd('test');
                    //     $diskon = self::cek_kupon($input_kupon, $total_price, $harga_shipping, $postal_code, $carts);
                    //     $diskon_temp = $diskon_temp + $_POST['item_diskon'][$key];
                    //     $diskon['total_diskon'] += $diskon_temp;
                    // }else{
                    //     // dd($_POST);
                    //     $diskon_temp = $diskon_temp + $_POST['item_diskon'][$key];
                    //     $diskon['total_diskon'] += $diskon_temp;
                    //     //dd($_POST);
                    // }
                    // var_dump($_POST['item_diskon'][$key]);
                    // dd($diskon['total_diskon']);
                    $paypal_product['items'][] = array(
                        'itm_name' => $_POST['item_name'][$key],
                        'itm_price' => $_POST['item_price'][$key],
                        'itm_code' => $_POST['item_code'][$key],
                        'itm_qty' => $_POST['item_qty'][$key]
                    );
                    $shipaddresscus = Input::get('fname') . ';' . Input::get('addr_line') . ';' . Input::get('addr1_line') . ';' . Input::get('state') . ';' . Input::get('zipcode') . ';' . Input::get('phone1_line') . ';' . Input::get('email');

                    $dp = 0.4 * $subtotal;
                    $order_installment = $subtotal - $dp / 3;

                    $jumlah_warranty = 0;

                    if($_POST['item_warranty_val'][$key] == 1)
                    {
                        $jumlah_warranty = floatval($get_setting_extended[0]->besaran_warranty)*floatval($subtotal)/100;
                    }

                    $is_cicilan = 0;
                    if (array_key_exists(1, $_POST['cicilan'])) {
                        if ($_POST['cicilan'][$key] == 1) {
                            $is_cicilan = 1;
                            $trx_is_cicilan = 1;
                        }
                    }

                    $data  = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => $_POST['item_code'][$key],
                        'order_type' => $_POST['item_type'][$key],
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => $_POST['item_qty'][$key],
                        'order_amt' => $subtotal,
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,
                        'order_paytype' => '5',
                        'order_pro_color' => $_POST['item_color'][$key],
                        'order_pro_size' => $_POST['item_size'][$key],
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'is_transfer_bank' => 1,
                        'order_use_warranty' => $_POST['item_warranty_val'][$key],
                        'order_shipping_price' => $harga_shipping_per_item,
                        'order_jumlah_warranty' => $jumlah_warranty,
                        'order_is_cicilan' => $is_cicilan,
                        'payment_channel' => Input::get('payment_channel')
                    );
                    // dd($data);
                    $data_shipping = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 1,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => floatval($harga_shipping),
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '5',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'payment_channel' => Input::get('payment_channel')
                    );
                    $data_diskon = array(
                        'order_cus_id' => Session::get('customerid'),
                        'order_pro_id' => 2,
                        'order_type' => '',
                        'transaction_id' => 'unpay'.$random_key,
                        'order_qty' => 1,
                        'order_amt' => floatval($diskon['total_diskon'] + $diskon_temp) * -1,
                        'order_tax' => 0,
                        'order_date' => $now,
                        'order_status' => 3,

                        'order_paytype' => '5',
                        'order_pro_color' => '',
                        'order_pro_size' => '',
                        'order_shipping_add' => $shipaddresscus,
                        'postalservice_code' => Input::get('pengiriman_oke'),
                        'payment_channel' => Input::get('payment_channel')
                    );

                //    var_dump($diskon['total_diskon']);

                    //dd($data_diskon);
                    if (($_POST['item_type'][$key]) != 2) {

                        $add_purchase = Home::purchased_checkout_product_insert($_POST['item_code'][$key], $_POST['item_qty'][$key], $_POST['item_schedp_id'][$key]);
                        if($add_purchase == 'Failed'){
                            return Redirect::to('addtocart')->with('session_result', 'Stok Tidak Mencukupi');
                        }elseif ($add_purchase == 'Failed Flash') {
                            return Redirect::to('addtocart')->with('session_result', 'Stok Flash Tidak Mencukupi');
                        }
                    }

                    Home::paypal_checkout_insert($data);

                    // echo("selesai inser nm_order");

                    $new_insert = DB::getPdo()->lastInsertId();
                    $insert_id .= DB::getPdo()->lastInsertId() . ',';
                    if (Input::get('load_ship' . $key) != 1) {
                        if(Input::get('pengiriman_oke') == 'PopBox') {
                            // echo "masuk popbox";
                            $data = array(
                                'ship_name' => Input::get('fname'),
                                'ship_address1' => Input::get('addr_line'),
                                'ship_address2' => Input::get('addr1_line'),
                                'ship_state' => Input::get('state'),
                                'ship_postalcode' => Input::get('zipcode'),
                                'ship_phone' => Input::get('phone1_line'),
                                'ship_email' => Input::get('email'),
                                'country' => Input::get('country_popbox_input'),
                                'city' => Input::get('city_popbox_input'),
                                'shipping_ref_number' => Input::get('shipping_reg_number'),
                                'ship_cus_id' => $cust_id,
                                'ship_order_id' => $new_insert
                            );

                            Home::insert_shipping_addr($data, $cust_id);
                        }
                        else
                        {
                            // echo("Masuk Shipping");
                            $data = array(
                                'ship_name' => Input::get('fname'),
                                'ship_address1' => Input::get('addr_line'),
                                'ship_address2' => Input::get('addr1_line'),
                                'ship_state' => Input::get('state'),
                                'ship_postalcode' => Input::get('zipcode'),
                                'ship_phone' => Input::get('phone1_line'),
                                'ship_email' => Input::get('email'),
                                'ship_cus_id' => $cust_id,
                                'ship_order_id' => $new_insert,
                                'ship_ci_id' => Input::get('id_city_ha'),
                                'ship_country' => Input::get('country'),
                                'ship_dis_id' => Input::get('district'),
                                'ship_sdis_id' => Input::get('subdistrict'),
                            );

                            Home::insert_shipping_addr($data, $cust_id);

                            // echo("End Shipiing");
                        }
                    }
                    $i_shipping++;
                }
                // dd($diskon['total_diskon'] + $diskon_temp);
                Home::paypal_checkout_insert($data_shipping);

                $new_insert = DB::getPdo()->lastInsertId();
                $insert_id .= DB::getPdo()->lastInsertId() . ',';

                Home::paypal_checkout_insert($data_diskon);

                $new_insert = DB::getPdo()->lastInsertId();
                $insert_id .= DB::getPdo()->lastInsertId() . ',';
                //dd("end");

                // -- detail customer --
                $get_data_cus = Home::get_data_detail_cus(Session::get('customerid'));

                // -- penggunaan poin --
                if(Input::get('chk_poin') == 1)
                {
                    // -- jumlah pemakaian poin
                    $penggunaan_poin_trs = $get_data_cus->total_poin;
                    // -- jumlah belanja dikurang poin
                    $jumlah_belanja_untuk_detail_transaksi = $jumlah_belanja_untuk_detail_transaksi - floatval($penggunaan_poin_trs);

                    Home::update_poin_dipakai(Session::get('customerid'));
                }
                else
                {
                    $penggunaan_poin_trs = 0;
                }
                if (array_key_exists('est_duration', $shipping)) {
                    $estimate_duration = $shipping['est_duration'];
                }else {
                    $estimate_duration = 0;
                }

                 //dd($diskon_temp);
                // -- insert to table transaksi --
                $data_detail_transaksi = array (
                    'transaction_id' => 'unpay'.$random_key,
                    'diskon' => floatval($diskon['total_diskon'] + $diskon_temp),
                    'diskon_voucher' => $diskon['total_diskon'],
                    'shipping' => floatval($harga_shipping),
                    'metode_pembayaran' => 'Manual Payment',
                    'status_pembayaran' => 'belum dibayar',
                    'total_belanja' => $ItemTotalPrice,
                    'pajak' => floatval($jumlah_belanja_untuk_detail_transaksi)*0,
                    'total_poin' => floatval($jumlah_belanja_untuk_detail_transaksi)/1000,
                    'poin_digunakan' => $penggunaan_poin_trs,
                    'trans_lama_ship' => $estimate_duration,
                    'is_cicilan' => $trx_is_cicilan,
                    'kupon_id' => $diskon['kupon_id']
                    );

                Home::insert_header_transaksi($data_detail_transaksi);

                // -- push ke queue --
                $date = \Carbon\Carbon::now()->addMinutes(config('expire.time'));
                // dd($date);
                \Queue::later($date, new \App\Commands\UpdateExpired('unpay'.$random_key));

                //\Queue::later($date, new \App\Commands\SalesOrder());

                Session::put('last_insert_id', trim($insert_id, ','));

                $trans_id = $new_insert;

                $currenttransactionorderid = base64_encode(Session::get('last_insert_id'));

                unset($_SESSION["cart"]);
                // echo "selesai queue";
                //dd(Input::get('input_kupon'));
                if(Input::get('input_kupon') != "")
                {
                    Home::update_status_pemakaian_voucher(Input::get('input_kupon'));
                }

                // -- push WMS --
                //$this->recall_unsync();

                // -- email notifikasi --

                 // imel transfer bank
                $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($kode);
                $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($kode);

                if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                    $payment_channel = null;
                }else {
                    $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                }
                $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                ->first();
                if ($metode_pembayaran_raw == null) {
                    $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                    ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                    ->first();
                    $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                }else {
                    $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                }
                // echo "selesai query";

                $send_mail_data = array(
                        'id_transaksi' => $kode,
                        'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                        'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                        'metode_pay' => $metode_pembayaran,
                        'items_order' => $get_data_order_by_trans_id
                );
                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                // Mail::send(
                //     'emails.notifikasi_email_transaksi_checkout',
                //     $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                //         $get_cus_data_by_trans_id){
                //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
                //     }
                // );

                // -- redirect --
                return Redirect::to('info_pembayaran' . '/' .'unpay'.$random_key)->with('result', 'Pesanan Anda Akan Diproses');
            }
        }
        else {
            $now            = date('Y-m-d H:i:s');
            $insert_id      = '';
            $ItemTotalPrice = 0;
            $transaction_id = str_random(8);
            $trans_check    = Home::trans_check($transaction_id);

            if ($trans_check) {
                $transaction_id = str_random(8);
            }
            foreach ($_POST['item_name'] as $key => $itmname) {
                $product_code = $_POST['item_code'][$key];

                $subtotal       = ($_POST['item_price'][$key] * $_POST['item_qty'][$key]);
                //total price
                $ItemTotalPrice = $ItemTotalPrice + $subtotal;

                $shipaddresscus = Input::get('fname') . ';' . Input::get('addr_line') . ';' . Input::get('addr1_line') . ';' . Input::get('state') . ';' . Input::get('zipcode') . ';' . Input::get('phone1_line') . ';' . Input::get('email');

                $data           = array(
                    'cod_cus_id' => Session::get('customerid'),
                    'cod_order_type' => $_POST['item_type'][$key],
                    'cod_transaction_id' => $transaction_id,
                    'cod_pro_id' => $product_code,
                    'cod_qty' => $_POST['item_qty'][$key],
                    'cod_amt' => $subtotal,
                    'cod_tax' => $_POST['item_tax'][$key],
                    'cod_date' => $now,
                    'cod_status' => 3,
                    'cod_paytype' => 'COD',

                    'cod_pro_color' => $_POST['item_color'][$key],
                    'cod_pro_size' => $_POST['item_size'][$key],
                    'cod_ship_addr' => $shipaddresscus,
                    'postalservice_code' => Input::get('pengiriman_oke')
                );
                $data_shipping = array(
                'cod_cus_id' => Session::get('customerid'),
                'cod_pro_id' => 1,
                'cod_order_type' => '',
                'cod_transaction_id' => $transaction_id,
                'cod_qty' => 1,
                'cod_amt' => $_POST['shipping_price_label_hidden'],
                'cod_tax' => 0,
                'cod_date' => $now,
                'cod_status' => 3,

                'cod_paytype' => 'COD',
                'cod_pro_color' => '',
                'cod_pro_size' => '',
                'cod_ship_addr' => $shipaddresscus,
                'postalservice_code' => Input::get('pengiriman_oke')
                );
                $data_diskon = array(
                'cod_cus_id' => Session::get('customerid'),
                'cod_pro_id' => 2,
                'cod_order_type' => '',
                'cod_transaction_id' => $transaction_id,
                'cod_qty' => 1,
                'cod_amt' => $_POST['total_diskon_kupon_hidden'] * -1,
                'cod_tax' => 0,
                'cod_date' => $now,
                'cod_status' => 3,

                'cod_paytype' => 'COD',
                'cod_pro_color' => '',
                'cod_pro_size' => '',
                'cod_ship_addr' => $shipaddresscus,
                'postalservice_code' => Input::get('pengiriman_oke')
                );

                if (($_POST['item_type'][$key]) != 2) {
                    Home::purchased_checkout_product_insert($_POST['item_code'][$key], $_POST['item_qty'][$key]);
                }
                Home::cod_checkout_insert($data);

                $new_insert = DB::getPdo()->lastInsertId();
                $insert_id .= DB::getPdo()->lastInsertId() . ',';
                if (Input::get('load_ship' . $key) != 1) {
                    $data_ship = array(
                        'ship_name' => Input::get('fname'),
                        'ship_address1' => Input::get('addr_line'),
                        'ship_address2' => Input::get('addr1_line'),
                        'ship_state' => Input::get('state'),
                        'ship_postalcode' => Input::get('zipcode'),
                        'ship_phone' => Input::get('phone1_line'),
                        'ship_cus_id' => $cust_id,
                        'ship_order_id' => $new_insert,
                        'ship_email' => Input::get('email')
                    );

                    Home::insert_shipping_addr($data_ship, $cust_id);
                }


            }
            Home::cod_checkout_insert($data_shipping);
            Home::cod_checkout_insert($data_diskon);

            Session::put('last_insert_id', trim($insert_id, ','));

            unset($_SESSION['cart']);
            unset($_SESSION['deal_cart']);
            Session::flash('payment_success', 'Your Cod Payment is Success');
            //include('SMTP/sendmail.php');
            $emailsubject = "Your COD  Successfully Registered.....";
            $subject      = "COD Acknowledgement.....";
            $name         = Session::get('username');
            $transid      = $transaction_id;
            $shipaddress  = $shipaddresscus;
            $address      = "";

            $resultmail   = "success";
            ob_start();
            include '../Emailsub/paymentcod.php';
            $body = ob_get_contents();
            ob_clean();
            //balikin nih
            //Send_Mail($address, $subject, $body);

            $trans = Session::get('last_insert_id');
            $trans_id = Home::transaction_id($trans);
            $get_subtotal                = Home::get_subtotal($trans_id);
            $get_tax                     = Home::get_tax($trans_id);
            $get_shipping_amount         = Home::get_shipping_amount($trans_id);

            $currenttransactionorderid   = base64_encode($trans_id);

            //$currenttransactionorderid = base64_encode(Session::get('last_insert_id'));


            $value = DB::table('nm_product')->where('pro_id', '=', $data['cod_pro_id'])->first();

            $merchant_details = DB::table('nm_product')->join('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')->where('pro_id', '=', $data['cod_pro_id'])->first();
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            //Customer Mail after order complete
            Mail::send(
                'emails.ordermail', array(
                'customer_name' => $name,
                'transaction_id' => $transid,
                'Sub_total' =>  $get_subtotal,
                'Tax' =>  $get_tax,
                'Shipping_amount' =>  $get_shipping_amount,
                'qty' => $data['cod_qty'],
                'item_price' => $subtotal,
                'address1' => $data['cod_ship_addr'],
                'product_name' => $value->pro_title
                ), function ($message) use ($data) {

                    $customer_mail = $data['cod_ship_addr'];

                    $allpas        = explode(",", $customer_mail);
                    $cus_mail      = $allpas[6];
                    //echo $allpas[6];
                    $message->to($cus_mail)->subject('Your Order Confirmation Details Placed Successfully');
                }
            );

            //Merchant Mail after order complete
            $merchant_trans_id = Home::get_merchant_based_transaction_id($trans_id);
            if(isset($merchant_trans_id) && $merchant_trans_id != "") {
                foreach($merchant_trans_id as $mer=>$m) {
                    $merchant_id = $m->cod_merchant_id;
                    $get_mer_subtotal = Home::get_mer_subtotal($trans_id, $merchant_id);
                    $get_mer_tax = Home::get_mer_tax($trans_id, $merchant_id);
                    $get_mer_shipping_amount = Home::get_mer_shipping_amount($trans_id, $merchant_id);
                    $admin = DB::table('nm_emailsetting')->first();
    				$admin_email = $admin->es_noreplyemail;
                    Config::set('mail.from.address', $admin_email);
                    Mail::send(
                        'emails.order-merchantmail', array(
                        'transaction_id' => $trans_id,
                        'Sub_total' =>  $get_mer_subtotal,
                        'Tax' =>  $get_mer_tax,
                        'Shipping_amount' =>  $get_mer_shipping_amount,'merchant_id' => $merchant_id), function ($message) use ($data) {
                            if (isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])) {
                                $merchant = DB::table('nm_deals')->where('deal_id', '=', $data['cod_pro_id'])->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_deals.deal_merchant_id')->first();
                                  $merchant_mail = $merchant->mer_email;
                            }
                            if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
                                $merchant = DB::table('nm_product')->where('pro_id', '=', $data['cod_pro_id'])->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')->first();
                                  $merchant_mail = $merchant->mer_email;
                                //   echo $merchant_mail;
                            }

                            $message->to('kailashkumar.r@pofitec.com')->subject('Hi Merchant! Your Product Purchased!!');
                        }
                    );
                }
            }
            unset($_SESSION['cart']);
            unset($_SESSION['deal_cart']);


            // $this->recall_unsync();

            return Redirect::to('show_payment_result_cod' . '/' . $currenttransactionorderid)->with('result', $data);

        }
    }

    public function test(Request $request)
    {
        $document_length = DB::table('term_frequency')
        ->where('pro_id', 78)
        ->where('field', 'Total')
        ->sum('frequency');
        $total_document_length = DB::table('term_frequency')
        ->where('field', 'Total')
        ->sum('frequency');
        $total_product = DB::table('term_frequency')
        ->selectRaw('count(distinct pro_id) as total')
        ->first();
        $average_document_length = $total_document_length / intval($total_product->total);

        return $average_document_length;
    }

    public function test1()
    {
        $kocak = Home::get_transaction_detail("unpay25189");

        //dd($kocak);

        foreach ($kocak as $ngibul) {

            $get_back = floatval($ngibul->pro_no_of_purchase) - floatval($ngibul->order_qty);

            var_dump($get_back ."|". $ngibul->pro_id);

            Home::get_back_the_stock_dude($ngibul->pro_id, $get_back);

        }
    }

    public function dokuHostedNotify($screet_code)
    {

        if(!$screet_code) { abort(404);
        }

        if($screet_code != config('dokularavel.NOTIFY_SCREET_CODE')) { abort(404);
        }

        //
        // //$ip_range = "103.10.129.";

        //if ( $_SERVER['REMOTE_ADDR'] != '182.253.5.91' && (substr($_SERVER['REMOTE_ADDR'],0,strlen($ip_range)) !== $ip_range) )
        // //if ( $_SERVER['REMOTE_ADDR'] != '103.10.129.16' && (substr($_SERVER['REMOTE_ADDR'],0,strlen($ip_range)) !== $ip_range) )
        // Filter IP ( ip doku dev = 103.10.129.16, ip doku production = 103.10.129.9 )
        // {
        if(!$_POST) {
            // Log::info("Stop : Access Not Valid");
            echo "Stop : Access Not Valid";
            die();
        }

        $trans_id_doku = $_POST['TRANSIDMERCHANT'];
        $totalamount_doku = $_POST['AMOUNT'];
        $words_doku = $_POST['WORDS'];
        $statustype_doku = $_POST['STATUSTYPE'];
        $response_code_doku = $_POST['RESPONSECODE'];
        $approvalcode_doku = $_POST['APPROVALCODE'];
        $status_doku = $_POST['RESULTMSG'];
        $paymentchannel_doku = $_POST['PAYMENTCHANNEL'];
        $paymentcode_doku = $_POST['PAYMENTCODE'];
        $session_id_doku = $_POST['SESSIONID'];
        $bank_issuer_doku = $_POST['BANK'];
        $cardnumber_doku = $_POST['MCN'];
        $payment_date_time_doku = $_POST['PAYMENTDATETIME'];
        $verifyid_doku = $_POST['VERIFYID'];
        $verifyscore_doku = $_POST['VERIFYSCORE'];
        $verifystatus_doku = $_POST['VERIFYSTATUS'];
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d H:i:s');

        $live_mode = config('dokularavel.LIVE_MODE');

        if ($live_mode == FALSE) {
            $ip_doku = '103.10.129.16';
        }elseif ($live_mode == TRUE) {
            $ip_doku = '103.10.129.9';
        }
        if($_SERVER['REMOTE_ADDR']==$ip_doku) {
            if($trans_id_doku) {
                $mallid = config('dokularavel.MALL_ID');
                $currency = config('dokularavel.CURRENCY');
                $purchasecurrency = config('dokularavel.CURRENCY');
                $sharedkey = config('dokularavel.SHARED_KEY');
                $data_submit = Home::get_data_order($trans_id_doku);
                $data_amount = Home::get_total_amount($trans_id_doku);
                $total_amount = round($data_amount['total_amount']);
                $total_amount = number_format($total_amount, 2, '.', '');
                $words = sha1($total_amount.$mallid.$sharedkey.$trans_id_doku.$status_doku.$verifystatus_doku);
                if($words_doku == $words) {
                    $date = \Carbon\Carbon::now()->addMinutes(10);
                    // \Queue::later($date, new \App\Commands\SalesOrder());
                    if($status_doku == 'SUCCESS') {
                        // $hook->afterPayment(true,$allldata);
                        try{
                             $query1 = DB::table('nm_order')
                             ->where('transaction_id', $trans_id_doku)
                             ->update(
                                 [
                                 'result_msg' => $status_doku, /*1, 20170219 Toto : 'PAID', */
                                 'doku_payment_datetime' => $payment_date_time_doku,
                                 'approval_code' => $approvalcode_doku,
                                 'order_status' => 1,
                                 'order_tgl_pesanan_dibayarkan' => $tanggal
                                 ]
                             );
                             $query2 = DB::table('nm_transaksi')
                             ->where('transaction_id', $trans_id_doku)
                             ->update([
                                'status_pembayaran' => 'sudah dibayar',
                                'order_status' => 1
                             ]);
                             $orderid = $trans_id_doku;
                             $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($orderid);
                             $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($orderid);

                             //-- kondisi poin digunakan
                             $get_detail_transaksi = Home::get_detail_transaksi($orderid);

                             $jumlah_poin = (floatval($get_detail_transaksi[0]->total_belanja) + floatval($get_detail_transaksi[0]->shipping) - floatval($get_detail_transaksi[0]->diskon) - floatval($get_detail_transaksi[0]->poin_digunakan))/1000;

                             if($get_detail_transaksi[0]->poin_digunakan > 0)
                             {
                                 Transactions::update_poin_to_zero($get_cus_data_by_trans_id[0]->cus_id);
                             }else {
                                 // -- jumlah poin customer lama + baru --
                                 $jumlah_poin = floatval($jumlah_poin) + floatval($get_cus_data_by_trans_id[0]->total_poin);
                             }

                             Transactions::change_jumlah_poin($jumlah_poin, $get_cus_data_by_trans_id[0]->cus_id);

                             if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                                 $payment_channel = null;
                             }else {
                                 $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                             }
                             $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                             ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                             ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                             ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                             ->first();
                             if ($metode_pembayaran_raw == null) {
                                 $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                                 ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                                 ->first();
                                 $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                             }else {
                                 $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                             }

                             $send_mail_data = array(
                                 'id_transaksi' => $orderid,
                                 'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                                 'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                                 'metode_pay' => $metode_pembayaran,
                                 'items_order' => $get_data_order_by_trans_id
                             );

                             //dd($send_mail_data);
                             $admin = DB::table('nm_emailsetting')->first();
             				$admin_email = $admin->es_noreplyemail;
                             Config::set('mail.from.address', $admin_email);
                             Mail::send(
                                 'emails.notifikasi_email_konfirmasi_pembayaran',
                                 $send_mail_data, function ($message) use ($get_cus_data_by_trans_id){
                                     $message->to($get_cus_data_by_trans_id[0]->cus_email)
                                     ->subject('Notifikasi Pembayaran');
                                 }
                             );

                             foreach ($get_data_order_by_trans_id as $row) {
                                 $select_mer_id = Transactions::get_data_mer_by_order_id($row->order_id);
                                 if($select_mer_id->postalservice_code == "JNE_REG"){
                                     $postalservice = "JNE Reguler";
                                 }elseif ($select_mer_id->postalservice_code == "JNE_OK") {
                                     $postalservice = "JNE OK";
                                 }elseif ($select_mer_id->postalservice_code == "JNE_YES") {
                                     $postalservice = "JNE YES";
                                 }elseif ($select_mer_id->postalservice_code == "POPBOX") {
                                     $postalservice = "Popbox";
                                 }elseif ($select_mer_id->postalservice_code == "AL") {
                                     $postalservice = "Ambil Langsung";
                                 }else {
                                     $postalservice = $select_mer_id->postalservice_code;
                                 }
                                 if($select_mer_id->mer_email != '' || $select_mer_id->mer_email != null)
                                 {
                                     $send_mail_to_mer = array(
                                         'trans_id' => $select_mer_id->transaction_id,
                                         'title' => $select_mer_id->pro_title,
                                         'pro_sku_merchant' => $select_mer_id->pro_sku_merchant,
                                         'order_qty' => $select_mer_id->order_qty,
                                         'cus_name' =>$select_mer_id->cus_name,
                                         'cus_phone' => $select_mer_id->cus_phone,
                                         'postalservice' => $postalservice
                                     );
                                     $admin = DB::table('nm_emailsetting')->first();
                     				$admin_email = $admin->es_noreplyemail;
                                     Config::set('mail.from.address', $admin_email);
                                         Mail::send(
                                         'emails.notifikasi_email_konfirmasi_ke_merchant',
                                         $send_mail_to_mer, function($message) use ($select_mer_id,$send_mail_to_mer){
                                             $message->to($select_mer_id->mer_email)
                                             ->subject('Notifikasi Transaksi');
                                         }
                                     );
                                 }
                                 else{
                                 }
                             }
                             $this->recall_unsync();
                             echo 'Continue';
                        }catch(\Exception $e) {
                            throw $e;
                            // Log::info('stop1');
                                     die('Stop1');
                        }
                    }else{
                        try{
                            $query = DB::table('nm_order')
                            ->where('transaction_id', $trans_id_doku)
                            ->update(
                                [
                                'result_msg' => $status_doku, /* 3, 'FAILED' */
                                ]
                            );
                             /* 20170219 Toto
                                storefront.order_status : 0-requested,1-success(paid),2-complete,3-hold(unpaid),4-failed,5-waitingtransfer
                             */
                        }catch(\Exception $e) {
                               throw $e;
                            //    Log::info('Error (Status Response Failed)');
                            die('Error (Status Response Failed)');
                        }
                    }
                    // Log::info('Continue');
                    // echo 'Continue';
                }else {
                    // Log::info($words_doku);
                    // Log::info($words->words);
                    // Log::info("Stop : Request Not Valid");
                    echo "Stop : Request Not Valid";
                    die();
                }
            }else{
                // Log::info('Stop : Transaction Not Found');
                   echo 'Stop : Transaction Not Found';
                die();
            }
        }else {
            // Log::info('Transaction is Finish');
            die('Transaction is Finish');
        }
        /*
        // Basic SQL

        $sql = "select transidmerchant,totalamount from doku where transidmerchant='" . $order_number . "'and trxstatus='Requested'";
        $checkout = mysql_fetch_array(mysql_query($sql));
        // echo "sql : ".$sql;
        $hasil = $checkout['transidmerchant'];
        $amount = $checkout['totalamount'];

        // Custom Field

        if (!$hasil) {

        echo 'Stop1';

        } else {

        if ($status == "SUCCESS") {
            $sql = "UPDATE doku SET trxstatus='Success', words='$words', statustype='$statustype', response_code='$response_code', approvalcode='$approvalcode',
		         trxstatus='$status', payment_channel='$paymentchannel', paymentcode='$paymentcode', session_id='$session_id', bank_issuer='$bank_issuer', creditcard='$cardnumber',
        payment_date_time='$payment_date_time', verifyid='$verifyid', verifyscore='$verifyscore', verifystatus='$verifystatus' where transidmerchant='$order_number'";
            // echo "sql : ".$sql;
            $result = mysql_query($sql) or die ("Stop2");

        } else {

            $sql = "UPDATE doku set trxstatus='Failed' where transidmerchant='" . $order_number . "'";

            $result = mysql_query($sql) or die ("Stop3");


        }
        echo 'Continue';

        }
        */
        /*
        $allldata	   = Request::all();
        $trans_id          = urldecode(Request::get('TRANSIDMERCHANT'));
        $status            = Request::get('RESULTMSG');
        $payment_date_time = Request::get('PAYMENTDATETIME');
        $approvalcode      = Request::get('APPROVALCODE');
        */

        //		print_r($trans_id);
        //		print_r($status);
        //		print_r($payment_date_time);
        //		print_r($approvalcode);
        //		exit();

        //		$hook = new \App\Http\Controllers\DokuLaravelHookController;
        //order_number

        // }
    }

    public function paypal_checkout_success()
    {
        $set = Home::get_settings();
        foreach ($set as $se) {
        }
        if ($se->ps_paypal_pay_mode == '0') {
            $mode = 'sandbox';
        } elseif ($se->ps_paypal_pay_mode == '1') {
            $mode = 'live';
        }
        //
        $PayPalMode         = $mode; // sandbox or live
        $PayPalApiUsername  = $se->ps_paypalaccount;

        $PayPalApiPassword  = $se->ps_paypal_api_pw;

        $PayPalApiSignature = $se->ps_paypal_api_signature;

        $PayPalCurrencyCode = $se->ps_curcode; //Paypal Currency Code
        $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
        $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel
        include '../paypal/paypal.class.php';
        if (isset($_GET["token"]) && isset($_GET["PayerID"])) {
            //we will be using these two variables to execute the "DoExpressCheckoutPayment"
            //Note: we haven't received any payment yet.

            $token    = $_GET["token"];
            $payer_id = $_GET["PayerID"];

            //get session variables
            //session_start();
            $paypal_product = $_SESSION["paypal_products"];
            $paypal_data    = '';
            $ItemTotalPrice = 0;

            foreach ($paypal_product['items'] as $key => $p_item) {
                $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($p_item['itm_qty']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($p_item['itm_price']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($p_item['itm_name']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($p_item['itm_code']);

                // item price X quantity
                $subtotal = ($p_item['itm_price'] * $p_item['itm_qty']);

                //total price
                $ItemTotalPrice = ($ItemTotalPrice + $subtotal);
            }

            $padata = '&TOKEN=' . urlencode($token) . '&PAYERID=' . urlencode($payer_id) . '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode("SALE") . $paypal_data . '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($ItemTotalPrice) . '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($paypal_product['assets']['tax_total']) . '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($paypal_product['assets']['shippin_cost']) . '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($paypal_product['assets']['handaling_cost']) . '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($paypal_product['assets']['shippin_discount']) . '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($paypal_product['assets']['insurance_cost']) . '&PAYMENTREQUEST_0_AMT=' . urlencode($paypal_product['assets']['grand_total']) . '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($PayPalCurrencyCode);

            //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
            $paypal               = new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

            //Check if everything went ok..
            if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

                echo '<h2>Success</h2>';
                echo 'Your Transaction ID : ' . urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);

                /*
                //Sometimes Payment are kept pending even when transaction is complete.
                //hence we need to notify user about it and ask him manually approve the transiction
                */

                if ('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {
                    echo '<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';

                } elseif ('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {

                }

                // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
                // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
                $padata               = '&TOKEN=' . urlencode($token);
                $paypal               = new MyPayPal();
                $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);


                if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
                    $data = array(
                        'transaction_id' => urldecode($httpParsedResponseAr['PAYMENTREQUEST_0_TRANSACTIONID']),
                        'token_id' => urldecode($httpParsedResponseAr['TOKEN']),
                        'payer_email' => urldecode($httpParsedResponseAr['EMAIL']),
                        'payer_id' => urldecode($httpParsedResponseAr['PAYERID']),
                        'payer_name' => urldecode($httpParsedResponseAr['FIRSTNAME']),
                        'currency_code' => urldecode($httpParsedResponseAr['PAYMENTREQUEST_0_CURRENCYCODE']),
                        'payment_ack' => urldecode($httpParsedResponseAr['ACK']),
                        'payer_status' => urldecode($httpParsedResponseAr['PAYERSTATUS']),
                        'order_status' => 1,
                        'order_paytype' => 1
                    );


                    Home::paypal_checkout_update($data, Session::get('last_insert_id'));
                    unset($_SESSION['cart']);
                    unset($_SESSION['deal_cart']);
                    Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
                    //include('SMTP/sendmail.php');
                    $emailsubject = "Your Payment Successfully completed.....";
                    $subject      = "Payment Acknowledgement.....";
                    $name         = $data['payer_name'];
                    $transid      = $data['transaction_id'];
                    $payid        = $data['payer_id'];
                    $ack          = $data['payment_ack'];
                    $address      = "yamuna@nexplocindia.com";

                    $resultmail   = "success";
                    ob_start();
                    include '../Emailsub/paymentemail.php';
                    $body = ob_get_contents();
                    ob_clean();
                    //balikin
                    //Send_Mail($address, $subject, $body);
                    $currenttransactionorderid = base64_encode(Session::get('last_insert_id'));

                    //todo
                    $this->recall_unsync();

                    return Redirect::to('show_payment_result' . '/' . $currenttransactionorderid)->with('result', $data);
                } else {
                    unset($_SESSION['cart']);
                    unset($_SESSION['deal_cart']);
                    Session::flash('payment_failed', 'Your Payment Has Been Failed');
                    $currenttransactionorderid = base64_encode(0);
                    return Redirect::to('show_payment_result' . '/' . $currenttransactionorderid)->with('fail', "fail");

                }

            } else {
                unset($_SESSION['cart']);
                unset($_SESSION['deal_cart']);
                Session::flash('payment_error', 'Some error Occured during Payment');
                return Redirect::to('index');
            }
        }
    }

    public function paypal_checkout_cancel()
    {
        Session_start();
        unset($_SESSION['cart']);
        unset($_SESSION['deal_cart']);
        Session::flash('payment_cancel', 'Your Payment Has Been Cancelled');
        return Redirect::to('index');

    }

    // doku checkout success
    public function doku_checkout_success($transaction_id)
    {
        $transaction_data = DB::table('nm_order')
        ->where('transaction_id', $transaction_id)
        ->first();
        $transaction_data2 = DB::table('nm_transaksi')
        ->where('transaction_id', $transaction_id)
        ->first();

        $data_submit = Home::get_data_order($transaction_id);
        $data_amount = Home::get_total_amount($transaction_id);
        $total_amount = round($data_amount['total_amount']);

        if($transaction_data ) {
            if($transaction_data2->status_pembayaran == "sudah dibayar") { // success payment
                unset($_SESSION['cart']);
                unset($_SESSION['deal_cart']);
                if ($transaction_data->order_type == 3) {
                    date_default_timezone_set('Asia/Jakarta');
                    $now = date('Y-m-d H:i:s');
                    $update_tgl_proses = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                        'order_tgl_pesanan_diproses' => $now,
                        'status_outbound' => 2
                    ]);
                    $update_outbound = DB::table('nm_transaksi')->where('transaction_id', $transaction_id)->update([
                        'status_outbound_transaksi' => 2
                    ]);
                    $get_product = DB::table('nm_product')->where('pro_id', $transaction_data->order_pro_id)->first();
                    if ($get_product->pro_sepulsa_type == 'mobile') {
                        $create_transaction_sepulsa = Sepulsa::create_mobile_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'bpjs_kesehatan') {
                        $create_transaction_sepulsa = Sepulsa::create_bpjs_kesehatan_transaction($transaction_data->order_sepulsa_customer_number, $transaction_data->order_sepulsa_payment_period, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'electricity') {
                        $create_transaction_sepulsa = Sepulsa::create_electricity_prepaid_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_data->order_sepulsa_meter_number, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'electricity_postpaid') {
                        $create_transaction_sepulsa = Sepulsa::create_electricity_postpaid_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'game') {
                        $create_transaction_sepulsa = Sepulsa::create_game_voucher_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'multi') {
                        $create_transaction_sepulsa = Sepulsa::create_multifinance_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'telkom_postpaid') {
                        $create_transaction_sepulsa = Sepulsa::create_telkom_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'pdam') {
                        $create_transaction_sepulsa = Sepulsa::create_pdam_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_data->order_sepulsa_operator_code, $transaction_id);
                    }
                    if ($create_transaction_sepulsa != null) {
                        if (array_key_exists('response_code', $create_transaction_sepulsa)) {
                            if($create_transaction_sepulsa['response_code'] == '00'){
                                date_default_timezone_set('Asia/Jakarta');
                                $now = date('Y-m-d H:i:s');
                                $update_tgl_dikirim = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                                    'order_tgl_pesanan_dikirim' => $now,
                                    'status_outbound' => 4
                                ]);
                                $update_outbound = DB::table('nm_transaksi')->where('transaction_id', $transaction_id)->update([
                                    'status_outbound_transaksi' => 4
                                ]);
                            }
                            if ($create_transaction_sepulsa['response_code'] == '10') {
                                $date = \Carbon\Carbon::now()->addSeconds(15);
                                \Queue::later($date, new \App\Commands\UpdateTransactionStatusSepulsa($create_transaction_sepulsa['transaction_id']));
                            }
                            if ($create_transaction_sepulsa['response_code'] == '00' || $create_transaction_sepulsa['response_code'] == '10') {
                                $update_transaction_id_sepulsa = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                                    'order_sepulsa_transaction_id' => $create_transaction_sepulsa['transaction_id']
                                ]);
                            }
                            $update_response_code = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                                'order_sepulsa_response_code' => $create_transaction_sepulsa['response_code'],
                            ]);
                        }
                    }
                }
                $data = array(
                    'transaction_id' => $transaction_id,
                    'words' => $transaction_data->words, // '',
                    'payer_email' => $transaction_data->payer_email, // 'EMAIL',
                    'payer_name' => $transaction_data->payer_name, // 'FIRSTNAME',
                    'currgency_code' => 'IDR', // '',
                    'payer_status' => $transaction_data->message, // '',
                    'order_status' => 1, //1=success payment
                    'order_paytype' => 2 // 2=doku
               );
               Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
               return Redirect::to('info_pembayaran' . '/' . $transaction_id)->with('result', 'Selamat! Transaksi Anda Berhasil');
            }else{
                unset($_SESSION['cart']);
                unset($_SESSION['deal_cart']);
                Session::flash('payment_failed', 'Your Payment Has Been Failed');
                $currenttransactionorderid = base64_encode(0);
                return Redirect::to('info_pembayaran' . '/' . $transaction_id)->with('fail', "Transaksi Anda Gagal");

            }
        }else{
            unset($_SESSION['cart']);
            unset($_SESSION['deal_cart']);
            Session::flash('payment_error', 'Some error Occured during Payment');
            return Redirect::to('index');
        }

        // $set = Home::get_settings();
        // foreach ($set as $se) {
        // }
        // if ($se->ps_paypal_pay_mode == '0') {
        //     $mode = 'sandbox';
        // } elseif ($se->ps_paypal_pay_mode == '1') {
        //     $mode = 'live';
        // }
        // //
        // $PayPalMode         = $mode; // sandbox or live
        // $PayPalApiUsername  = $se->ps_paypalaccount;
        //
        // $PayPalApiPassword  = $se->ps_paypal_api_pw;
        //
        // $PayPalApiSignature = $se->ps_paypal_api_signature;
        //
        // $PayPalCurrencyCode = $se->ps_curcode; //Paypal Currency Code
        // $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
        // $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel
        // include '../paypal/paypal.class.php';
        // if (isset($_GET["token"]) && isset($_GET["PayerID"])) {
        //     //we will be using these two variables to execute the "DoExpressCheckoutPayment"
        //     //Note: we haven't received any payment yet.
        //
        //     $token    = $_GET["token"];
        //     $payer_id = $_GET["PayerID"];
        //
        //     //get session variables
        //     //session_start();
        //     $paypal_product = $_SESSION["paypal_products"];
        //     $paypal_data    = '';
        //     $ItemTotalPrice = 0;
        //
        //     foreach ($paypal_product['items'] as $key => $p_item) {
        //         $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($p_item['itm_qty']);
        //         $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($p_item['itm_price']);
        //         $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($p_item['itm_name']);
        //         $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($p_item['itm_code']);
        //
        //         // item price X quantity
        //         $subtotal = ($p_item['itm_price'] * $p_item['itm_qty']);
        //
        //         //total price
        //         $ItemTotalPrice = ($ItemTotalPrice + $subtotal);
        //     }
        //
        //     $padata = '&TOKEN=' . urlencode($token) . '&PAYERID=' . urlencode($payer_id) . '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode("SALE") . $paypal_data . '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($ItemTotalPrice) . '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($paypal_product['assets']['tax_total']) . '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($paypal_product['assets']['shippin_cost']) . '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($paypal_product['assets']['handaling_cost']) . '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($paypal_product['assets']['shippin_discount']) . '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($paypal_product['assets']['insurance_cost']) . '&PAYMENTREQUEST_0_AMT=' . urlencode($paypal_product['assets']['grand_total']) . '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($PayPalCurrencyCode);
        //
        //     //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
        //     $paypal               = new MyPayPal();
        //     $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
        //
        //     //Check if everything went ok..
        //     if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
        //
        //         echo '<h2>Success</h2>';
        //         echo 'Your Transaction ID : ' . urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);
        //
        //         /*
        //         //Sometimes Payment are kept pending even when transaction is complete.
        //         //hence we need to notify user about it and ask him manually approve the transiction
        //         */
        //
        //         if ('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {
        //             echo '<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';
        //
        //         } elseif ('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {
        //
        //         }
        //
        //         // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
        //         // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
        //         $padata               = '&TOKEN=' . urlencode($token);
        //         $paypal               = new MyPayPal();
        //         $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
        //
        //
        //         if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
        //             $data = array(
        //                 'transaction_id' => urldecode($httpParsedResponseAr['PAYMENTREQUEST_0_TRANSACTIONID']),
        //                 'token_id' => urldecode($httpParsedResponseAr['TOKEN']),
        //                 'payer_email' => urldecode($httpParsedResponseAr['EMAIL']),
        //                 'payer_id' => urldecode($httpParsedResponseAr['PAYERID']),
        //                 'payer_name' => urldecode($httpParsedResponseAr['FIRSTNAME']),
        //                 'currency_code' => urldecode($httpParsedResponseAr['PAYMENTREQUEST_0_CURRENCYCODE']),
        //                 'payment_ack' => urldecode($httpParsedResponseAr['ACK']),
        //                 'payer_status' => urldecode($httpParsedResponseAr['PAYERSTATUS']),
        //                 'order_status' => 1,
        //                 'order_paytype' => 1
        //             );
        //
        //
        //             Home::paypal_checkout_update($data, Session::get('last_insert_id'));
        //             unset($_SESSION['cart']);
        //             unset($_SESSION['deal_cart']);
        //             Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
        //             //include('SMTP/sendmail.php');
        //             $emailsubject = "Your Payment Successfully completed.....";
        //             $subject      = "Payment Acknowledgement.....";
        //             $name         = $data['payer_name'];
        //             $transid      = $data['transaction_id'];
        //             $payid        = $data['payer_id'];
        //             $ack          = $data['payment_ack'];
        //             $address      = "yamuna@nexplocindia.com";
        //
        //             $resultmail   = "success";
        //             ob_start();
        //             include '../Emailsub/paymentemail.php';
        //             $body = ob_get_contents();
        //             ob_clean();
        //             //balikin
        //             //Send_Mail($address, $subject, $body);
        //             $currenttransactionorderid = base64_encode(Session::get('last_insert_id'));
        //
        //             //todo
        //             $this->recall_unsync();
        //
        //             return Redirect::to('show_payment_result' . '/' . $currenttransactionorderid)->with('result', $data);
        //         } else {
        //             unset($_SESSION['cart']);
        //             unset($_SESSION['deal_cart']);
        //             Session::flash('payment_failed', 'Your Payment Has Been Failed');
        //             $currenttransactionorderid = base64_encode(0);
        //             return Redirect::to('show_payment_result' . '/' . $currenttransactionorderid)->with('fail', "fail");
        //
        //         }
        //
        //     } else {
        //         unset($_SESSION['cart']);
        //         unset($_SESSION['deal_cart']);
        //         Session::flash('payment_error', 'Some error Occured during Payment');
        //         return Redirect::to('index');
        //     }
        // }
    }

    public function doku_checkout_cancel()
    {
        // Session_start();
        unset($_SESSION['cart']);
        unset($_SESSION['deal_cart']);
        Session::flash('payment_cancel', 'Your Payment Has Been Cancelled');
        return Redirect::to('index');
    }

    public function sprintasia_checkout_success($transaction_id)
    {
        // echo $transaction_id; // 20170410
        $transaction_data = DB::table('nm_order')
        ->where('transaction_id', $transaction_id)
        ->first();

        $total_amount = DB::table('nm_order')->where('transaction_id', $transaction_id)->sum('order_amt'); // 20170503

        if($transaction_data ) {
            if(( $transaction_data->payment_status == "00") || ( $transaction_data->payment_status == "02")) { // success payment
                $this->recall_unsync();
                unset($_SESSION['cart']);
                unset($_SESSION['deal_cart']);
                if ($transaction_data->order_type == 3) {
                    date_default_timezone_set('Asia/Jakarta');
                    $now = date('Y-m-d H:i:s');
                    $update_tgl_proses = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                        'order_tgl_pesanan_diproses' => $now,
                        'status_outbound' => 2
                    ]);
                    $update_outbound = DB::table('nm_transaksi')->where('transaction_id', $transaction_id)->update([
                        'status_outbound_transaksi' => 2
                    ]);
                    $get_product = DB::table('nm_product')->where('pro_id', $transaction_data->order_pro_id)->first();
                    if ($get_product->pro_sepulsa_type == 'mobile') {
                        $create_transaction_sepulsa = Sepulsa::create_mobile_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'bpjs_kesehatan') {
                        $create_transaction_sepulsa = Sepulsa::create_bpjs_kesehatan_transaction($transaction_data->order_sepulsa_customer_number, $transaction_data->order_sepulsa_payment_period, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'electricity') {
                        $create_transaction_sepulsa = Sepulsa::create_electricity_prepaid_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_data->order_sepulsa_meter_number, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'electricity_postpaid') {
                        $create_transaction_sepulsa = Sepulsa::create_electricity_postpaid_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'game') {
                        $create_transaction_sepulsa = Sepulsa::create_game_voucher_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'multi') {
                        $create_transaction_sepulsa = Sepulsa::create_multifinance_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'telkom_postpaid') {
                        $create_transaction_sepulsa = Sepulsa::create_telkom_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_id);
                    }elseif ($get_product->pro_sepulsa_type == 'pdam') {
                        $create_transaction_sepulsa = Sepulsa::create_pdam_transaction($transaction_data->order_sepulsa_customer_number, $get_product->pro_sepulsa_id, $transaction_data->order_sepulsa_operator_code, $transaction_id);
                    }
                    if ($create_transaction_sepulsa != null) {
                        if (array_key_exists('response_code', $create_transaction_sepulsa)) {
                            if($create_transaction_sepulsa['response_code'] == '00'){
                                date_default_timezone_set('Asia/Jakarta');
                                $now = date('Y-m-d H:i:s');
                                $update_tgl_dikirim = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                                    'order_tgl_pesanan_dikirim' => $now,
                                    'status_outbound' => 4
                                ]);
                                $update_outbound = DB::table('nm_transaksi')->where('transaction_id', $transaction_id)->update([
                                    'status_outbound_transaksi' => 4
                                ]);
                            }
                            if ($create_transaction_sepulsa['response_code'] == '10') {
                                $date = \Carbon\Carbon::now()->addSeconds(15);
                                \Queue::later($date, new \App\Commands\UpdateTransactionStatusSepulsa($create_transaction_sepulsa['transaction_id']));
                            }
                            if ($create_transaction_sepulsa['response_code'] == '00' || $create_transaction_sepulsa['response_code'] == '10') {
                                $update_transaction_id_sepulsa = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                                    'order_sepulsa_transaction_id' => $create_transaction_sepulsa['transaction_id']
                                ]);
                            }
                            $update_response_code = DB::table('nm_order')->where('transaction_id', $transaction_id)->update([
                                'order_sepulsa_response_code' => $create_transaction_sepulsa['response_code'],
                            ]);
                        }
                    }
                }
                $data = array(
                    'transaction_id' => $transaction_id,
                    'token_id' => $transaction_data->words, // '',
                    'payer_email' => '$transaction_data->payer_email', // 'EMAIL',
                    'payer_id' => $transaction_data->insert_id, // 'PAYERID',
                    'payer_name' => $transaction_data->payer_name, // 'FIRSTNAME',
                    'currency_code' => $transaction_data->currency_code, // '',
                    'payment_ack' => 'ACK',
                    'payer_status' => $transaction_data->message, // '',
                    'order_status' => 1, //1=success payment
                    'order_paytype' => 3 // 3=sprintasia
               );
               Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
               return Redirect::to('info_pembayaran' . '/' . $transaction_id)->with('result', 'Selamat! Transaksi Anda Berhasil');
            }else{
                unset($_SESSION['cart']);
                unset($_SESSION['deal_cart']);
                Session::flash('payment_failed', 'Your Payment Has Been Failed');
                $currenttransactionorderid = base64_encode(0);
                return Redirect::to('info_pembayaran' . '/' . $transaction_id)->with('fail', "Transaksi Anda Gagal");

            };
        }else{
            unset($_SESSION['cart']);
            unset($_SESSION['deal_cart']);
            Session::flash('payment_error', 'Some error Occured during Payment');
            return Redirect::to('index');
        };


        /*
        $set = Home::get_settings();
        foreach ($set as $se) {
        }
        if ($se->ps_paypal_pay_mode == '0') {
            $mode = 'sandbox';
        } elseif ($se->ps_paypal_pay_mode == '1') {
            $mode = 'live';
        }
        //
        $PayPalMode         = $mode; // sandbox or live
        $PayPalApiUsername  = $se->ps_paypalaccount;

        $PayPalApiPassword  = $se->ps_paypal_api_pw;

        $PayPalApiSignature = $se->ps_paypal_api_signature;

        $PayPalCurrencyCode = $se->ps_curcode; //Paypal Currency Code
        $PayPalReturnURL    = url('paypal_checkout_success'); //Point to process.php page
        $PayPalCancelURL    = url('paypal_checkout_cancel'); //Cancel URL if user clicks cancel
        require 'paypal/paypal.class.php';
        if (isset($_GET["token"]) && isset($_GET["PayerID"])) {
            //we will be using these two variables to execute the "DoExpressCheckoutPayment"
            //Note: we haven't received any payment yet.

            $token    = $_GET["token"];
            $payer_id = $_GET["PayerID"];

            //get session variables
            //session_start();
            $paypal_product = $_SESSION["paypal_products"];
            $paypal_data    = '';
            $ItemTotalPrice = 0;

            foreach ($paypal_product['items'] as $key => $p_item) {
                $paypal_data .= '&L_PAYMENTREQUEST_0_QTY' . $key . '=' . urlencode($p_item['itm_qty']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_AMT' . $key . '=' . urlencode($p_item['itm_price']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NAME' . $key . '=' . urlencode($p_item['itm_name']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER' . $key . '=' . urlencode($p_item['itm_code']);

                // item price X quantity
                $subtotal = ($p_item['itm_price'] * $p_item['itm_qty']);

                //total price
                $ItemTotalPrice = ($ItemTotalPrice + $subtotal);
            }

            $padata = '&TOKEN=' . urlencode($token) . '&PAYERID=' . urlencode($payer_id) . '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode("SALE") . $paypal_data . '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($ItemTotalPrice) . '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($paypal_product['assets']['tax_total']) . '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($paypal_product['assets']['shippin_cost']) . '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($paypal_product['assets']['handaling_cost']) . '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($paypal_product['assets']['shippin_discount']) . '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($paypal_product['assets']['insurance_cost']) . '&PAYMENTREQUEST_0_AMT=' . urlencode($paypal_product['assets']['grand_total']) . '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($PayPalCurrencyCode);

            //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
            $paypal               = new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

            //Check if everything went ok..
            if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

                echo '<h2>Success</h2>';
                echo 'Your Transaction ID : ' . urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);

                / *
                //Sometimes Payment are kept pending even when transaction is complete.
                //hence we need to notify user about it and ask him manually approve the transiction
                * /

                if ('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {
                    echo '<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';

                } elseif ('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {

                }

                // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
                // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
                $padata               = '&TOKEN=' . urlencode($token);
                $paypal               = new MyPayPal();
                $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);


                if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
                    $data = array(
                        'transaction_id' => urldecode($httpParsedResponseAr['PAYMENTREQUEST_0_TRANSACTIONID']),
                        'token_id' => urldecode($httpParsedResponseAr['TOKEN']),
                        'payer_email' => urldecode($httpParsedResponseAr['EMAIL']),
                        'payer_id' => urldecode($httpParsedResponseAr['PAYERID']),
                        'payer_name' => urldecode($httpParsedResponseAr['FIRSTNAME']),
                        'currency_code' => urldecode($httpParsedResponseAr['PAYMENTREQUEST_0_CURRENCYCODE']),
                        'payment_ack' => urldecode($httpParsedResponseAr['ACK']),
                        'payer_status' => urldecode($httpParsedResponseAr['PAYERSTATUS']),
                        'order_status' => 1,
                        'order_paytype' => 1
                    );
                    Home::paypal_checkout_update($data, Session::get('last_insert_id'));
                    unset($_SESSION['cart']);
                    unset($_SESSION['deal_cart']);
                    Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
                    include('SMTP/sendmail.php');
                    $emailsubject = "Your Payment Successfully completed.....";
                    $subject      = "Payment Acknowledgement.....";
                    $name         = $data['payer_name'];
                    $transid      = $data['transaction_id'];
                    $payid        = $data['payer_id'];
                    $ack          = $data['payment_ack'];
                    $address      = "yamuna@nexplocindia.com";

                    $resultmail   = "success";
                    ob_start();
                    include('Emailsub/paymentemail.php');
                    $body = ob_get_contents();
                    ob_clean();
                    Send_Mail($address, $subject, $body);
                    $currenttransactionorderid = base64_encode(Session::get('last_insert_id'));
                    return Redirect::to('show_payment_result' . '/' . $currenttransactionorderid)->with('result', $data);
                } else {
                    unset($_SESSION['cart']);
                    unset($_SESSION['deal_cart']);
                    Session::flash('payment_failed', 'Your Payment Has Been Failed');
                    $currenttransactionorderid = base64_encode(0);
                    return Redirect::to('show_payment_result' . '/' . $currenttransactionorderid)->with('fail', "fail");

                }

            } else {
                unset($_SESSION['cart']);
                unset($_SESSION['deal_cart']);
                Session::flash('payment_error', 'Some error Occured during Payment');
                return Redirect::to('index');
            }
        }
        */
    }

    public function sprintasia_checkout_cancel()
    {
        // Session_start();
        unset($_SESSION['cart']);
        unset($_SESSION['deal_cart']);
        Session::flash('payment_cancel', 'Your Payment Has Been Cancelled');
        return Redirect::to('index');

    }


    // Sprintasia starts : insert payment
    public function sprintasiaInsertPayment($transaction_id, $data_customer)
    {
        if (config('dokularavel.LIVE_MODE') == true) {
            $secretKey = '1C4FD2C7965C978D6FBB1D9F525A6401';
        }else {
            $secretKey = '58a85394b0fe867ade8cdbe5a787dfae';    // Sprintasia Secret Key
        }


        $insert_data = DB::table('nm_order')
        ->select('order_id', 'currency_code', 'transaction_id', 'order_amt', 'order_tax', 'order_date', 'order_expire', 'payer_name', 'payer_email', 'message', 'payment_channel')
        ->where('transaction_id', $transaction_id)
        ->first();

        if ($insert_data->payment_channel == '00') {
            $channelId = config('sprintasia.bcaklikpay_channelid'); // Sprintasia Channel ID For BCA KlikPay
        }elseif ($insert_data->payment_channel == '01') {
            $channelId = config('sprintasia.bcava_channelid'); // Sprintasia Channel ID For BCA VA
        }elseif ($insert_data->payment_channel == '02') {
            $channelId = config('sprintasia.permatava_channelid'); // Sprintasia Channel ID For Permata VA
        }elseif ($insert_data->payment_channel == '03') {
            $channelId = config('sprintasia.kredivo_channelid'); // Sprintasia Channel ID For Kredivo
        }else {
            $channelId = '';
        }

        $pajak = 0;
        $data_submit = DB::table('nm_order')->where('transaction_id', $transaction_id)->get();
        $total_amount_sebelum_pajak = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '<>', '1')->where('order_pro_id', '<>', '2')->sum('order_amt');
        $total_shipping = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '1')->sum('order_amt');
        $total_diskon = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '2')->sum('order_amt');
        $total_amount = ($total_amount_sebelum_pajak + ($total_amount_sebelum_pajak * $pajak)) + $total_shipping + $total_diskon; // 20170503
        $total_amount = intval($total_amount);
        // dd($total_amount);
        //				'payer_name', 'payer_email', 'message')->where('order_id', $order_id)->first(); //
        // sum(order_amt)
        /*
        $insert_data    = DB::table('nm_order')
        				->select(
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.order_id),'0') as order_id")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.currency_code),'0') as currency_code")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.transaction_id),'0') as transaction_id")],
        					['nm_order.*', DB::raw("IFNULL(sum(nm_order.order_amt),0) as order_amt")],
        					['nm_order.*', DB::raw("IFNULL(sum(nm_order.order_tax),0) as order_tax")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.order_date),'0') as order_date")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.order_expire),'0') as order_expire")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.payer_name),'') as payer_name")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.payer_email),'') as payer_email")],
        					['nm_order.*', DB::raw("IFNULL(min(nm_order.message),'') as message")]
        					)
        				->where('transaction_id', $transaction_id)
        				->groupBy('nm_order.transaction_id')->get();
        */
        /*
        $query = DB::table('purchases')
        ->select(['purchases.*',
                   DB::raw("IFNULL(sum(payments.amount),0) as total")
            ])
         ->leftJoin('payments','payments.purchase_id','=','purchases.id')
         ->whereNull('payments.deleted_at')
         ->groupBy('purchases.id')->get();

        */

        $authCode = hash("sha256", $transaction_id.$total_amount.$channelId.$secretKey); // 20170503
        $order_time = strtotime($insert_data->order_date);
        $expire_time = strtotime('+24 hours', $order_time);
        $expire_date = date("Y-m-d H:i:s", $expire_time);
        $customerAccount = '';
        // dd($expire_date);
        if ($insert_data->payment_channel == '00') { //BCA KLikPay
            $data = array(
                "channelId"             => $channelId,
                "currency"              => "IDR",
                "transactionNo"         => $insert_data->transaction_id,
                "transactionAmount"     => $total_amount,
                "transactionFee"        => "0",
                "transactionDate"       => $insert_data->order_date,
                "transactionExpire"     => $expire_date,
                "callbackURL"           => url('sprintasia_checkout_success/'.$transaction_id), // Fill with your callback URL
                "description"           => "-", //$insert_data->message", // memakai field message, diubah menjadi NOT NULL
                "customerAccount"       => "",
                "customerName"          => $data_customer['name'],
                "customerEmail"         => $data_customer['phone'],
                "authCode"              => $authCode,
            );
        }elseif ($insert_data->payment_channel == '01') { //BCA VA
            $explode_transaction_id = explode('unpay', $insert_data->transaction_id);
            $compCode = config('sprintasia.bcava_code');
            // generate customerAccount
            $length = 16 - intval(strlen($compCode));
            $customerAccount = $compCode.sprintf('%0'.$length.'d', $explode_transaction_id[1]);

            $data = array(
                "channelId"         => $channelId,
                "currency"          => "IDR",
                "transactionNo"     => $insert_data->transaction_id,
                "transactionAmount" => $total_amount,
                "transactionDate"   => $insert_data->order_date,
                "transactionExpire" => $expire_date,
                "description"       => 'Order'.$insert_data->transaction_id,
                "authCode"          => $authCode,
                "additionalData"    => '',
                "customerAccount"   => $customerAccount,
                "customerName"      => $data_customer['name'],
            );
        }elseif ($insert_data->payment_channel == '02') { // Permata VA
            $explode_transaction_id = explode('unpay', $insert_data->transaction_id);
            $bin = config('sprintasia.permatava_code');
            // generate customerAccount
            $length = 16 - intval(strlen($bin));
            $customerAccount = $bin.sprintf('%0'.$length.'d', $explode_transaction_id[1]);

            // Prepare insert transaction data
            $data = array(
                "channelId"         => $channelId,
                "currency"          => 'IDR',
                "transactionNo"     => $insert_data->transaction_id,
                "transactionAmount" => $total_amount,
                "transactionDate"   => $insert_data->order_date,
                "transactionExpire" => $expire_date,
                "description"       => 'Order'.$insert_data->transaction_id,
                "authCode"          => $authCode,
                "additionalData"    => '',
                "customerAccount"   => $customerAccount,
                "customerName"      => $data_customer['name'],
            );
        }else {
            $data = [];
        }
        if (!function_exists('curl_init')){   // Check existing cURL
            die('Sorry cURL is not installed!');
        }

        $URL_insert = config('sprintasia.url_insert');

        $OPT        = http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL_insert);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $OPT);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $raw_response = curl_exec($ch);
        if(curl_error($ch)) {
            die("CURL Error ::". curl_error($ch));
        }
        curl_close($ch);

        $response   = json_decode($raw_response, true);

        $insertResponse_data = [
            'message' => $response['insertMessage'],
            'words' => $authCode,
            'status_code' => $response['insertStatus'],
            'insert_status' => $response['insertStatus'],
            'insert_id' => $response['insertId'],
            'order_status' => "3",
            'order_customer_account' => $customerAccount
        ];
        if ($insert_data->payment_channel == '00') {
            $insertResponse_data['redirectURL'] = $response['redirectURL'];
        }
        $insertResponse = DB::table('nm_order')
        ->where('transaction_id', $insert_data->transaction_id)
        ->update($insertResponse_data);

         return $response;
    }

    /* Sprintasia hosted : sprintasia payment flag
    */
    public function sprintasiaCheckPaymentFlag(Request $request)
    {
        /*
          Title : PHP Example for BCA KlikPay Payment Flag Process
          Ver   : 1.0
          Desc  :
          This file is a simple example of BCA KlikPay Payment Flag.
          The main role is validating conditions which are:
          1.  Validate Transaction. Check whether Transaction is exist in database
          2.  Validate Channel Id. Check whether Channel Id sent by Sprint is matched with our credential
          3.  Validate Currency. Check wheter Currency sent by Sprint is IDR
          4.  Validate Transaction Number. Check wheter Transaction Number sent by Sprint is exists in database
          5.  Validate Transaction Amount. Check wheter Transaction Amount sent by Sprint is matched with transaction saved in database
          6.  Validate Transaction Status. Check wheter Transaction Amount sent by Sprint is 00.
          7.  Validate Insert ID. Check wheter Insert ID sent by Sprint is matched with transaction saved in database
          8.  Validate Transaction Status. Check whether the transaction was already paid.
          9.  Validate Transaction Status. Check whether the transaction was expired .
          10. Validate Transaction Status. Check whether the transaction was cancelled.
          11. Validate authCode. Check wheter authCode sent by Sprint is matched with merchant authCode.
        */

        // Parsing request data from Sprint
        $data_sprint = array(
          "channelId"           => $request->input('channelId'),
          "currency"            => $request->input('currency'),
          "transactionNo"       => $request->input('transactionNo'),
          "transactionAmount"   => $request->input('transactionAmount'),
          "transactionDate"     => $request->input('transactionDate'),
          "transactionStatus"   => $request->input('transactionStatus'),
          "transactionMessage"  => $request->input('transactionMessage'),
          "flagType"            => $request->input('flagType'),
          "insertId"            => $request->input('insertId'),
          "paymentReffId"       => $request->input('paymentReffId'),
          "authCode"            => $request->input('authCode'),
          "additionalData"      => $request->input('additionalData')
          );
        $paymentReffId = json_decode($request->input('paymentReffId'), true);
        // Log::info($paymentReffId);
        /*
        $response   = array(
          "channelId"           => 'KUKURYKKP01',      	// Channel ID sent by Sprint
          "currency"            => 'IDR',       	// Currency sent by Sprint (IDR)
          "paymentStatus"       => "00",                // Payment Status ( 00 => Success , 01,03 => Failed , 02 => isPaid , 04 => isExpired , 05 => isCancelled )
          "paymentMessage"      => "Success",           // Payment Message
          "flagType"            => '11',       		// Flag Type sent by Sprint
          "paymentReffId"       => 'paymentReffId',  	// Payment Referrence ID sent by Sprint
        );
        */

        // Prepare response data
        $response   = array(
          "channelId"           => $request->input('channelId'),          // Channel ID sent by Sprint
          "currency"            => $request->input('currency'),           // Currency sent by Sprint (IDR)
          "paymentStatus"       => "",                              // Payment Status ( 00 => Success , 01,03 => Failed , 02 => isPaid , 04 => isExpired , 05 => isCancelled )
          "paymentMessage"      => "",                              // Payment Message
          "flagType"            => $request->input('flagType'),           // Flag Type sent by Sprint
          "paymentReffId"       => $request->input('paymentReffId'),      // Payment Referrence ID sent by Sprint
        );

        // BCA KlikPay Credential
        //$channelId  = ""; // Your Channel ID
        //$secretKey  = ""; // Your secretkey

        $channelId      = config('sprintasia.bcaklikpay_channelid');
        if (config('dokularavel.LIVE_MODE') == true) {
            $secretKey = '1C4FD2C7965C978D6FBB1D9F525A6401';
        }else {
            $secretKey = '58a85394b0fe867ade8cdbe5a787dfae';    // Sprintasia Secret Key
        }
        //        $insert_data    = DB::table('nm_order')->select('order_id', 'currency_code', 'transaction_id', 'order_amt', 'order_tax', 'order_date', 'order_expire', 'payer_name', 'payer_email', 'message')->where('order_id', $order_id)->first();
        //        $authCode   	= hash("sha256", $insert_data->transaction_id.$insert_data->order_amt.$channelId.$secretKey);

        /*
        // Configs
        $db_host    = "localhost";
        $db_user    = "dbuser0123";
        $db_pass    = "My#Example#P455w0rd";
        $db_name    = "store_order";

        // Database initialization
        $db_conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
        */
        /*
        Get Transaction from database
        1. transactionNo
        2. currency           => IDR
        3. transactionAmount
        4. transactionDate
        5. insertId           => insertId from Sprint in Insert Transaction Response
        6. status             => payment order status
        */
        /*
        $sql_transaction    = "SELECT transactionNo, currency, transactionAmount, transactionDate, insertId, status FROM bcaklikpay_transaction_table WHERE transactionNo = '$_REQUEST[transactionNo]' LIMIT 1";
        $result             = mysqli_query($db_conn, $sql_transaction);
        $transaction_data   = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        */

        //        $transaction_data    = DB::table('nm_order')->select('order_id', 'currency_code', 'transaction_id', 'order_amt', 'order_tax', 'order_date', 'order_expire',
        //				'payer_name', 'payer_email', 'message')->where('transaction_id', $transaction_id)->first(); //
        //----------------------------------------------------------------------------------------------------------------------------
        date_default_timezone_set('Asia/Jakarta');
        $strtotime = strtotime($request->input('transactionDate'));
        $strtotime2 = strtotime('+24 hours', $strtotime);
        $expire_date = date('Y-m-d H:i:s', $strtotime2);
        $tanggal = date('Y-m-d H:i:s');
        $transaction_id = $data_sprint['transactionNo'];
        $transaction_data2 = DB::table('nm_transaksi')->where('transaction_id', $transaction_id)->first();
        $transaction_data = DB::table('nm_order')
        ->select('order_id', 'currency_code', 'transaction_id', 'order_amt', 'order_tax', 'order_date', 'order_expire', 'payer_name', 'payer_email', 'message', 'insert_id', 'insert_status', 'payment_status', 'words')
        ->where('transaction_id', $transaction_id)
        ->first();
        $pajak = 0;
        $data_submit = DB::table('nm_order')->where('transaction_id', $transaction_id)->get();
        $total_amount_sebelum_pajak = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '<>', '1')->where('order_pro_id', '<>', '2')->sum('order_amt');
        $total_shipping = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '1')->sum('order_amt');
        $total_diskon = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '2')->sum('order_amt');
        $total_amount = ($total_amount_sebelum_pajak + ($total_amount_sebelum_pajak * $pajak)) + $total_shipping + $total_diskon; // 20170503
        $total_amount = intval($total_amount);

        // Generate authCode
        ////$authCode = hash("sha256", $transaction_data['transactionNo'].$transaction_data['transactionAmount'].$channelId.$data_sprint['transactionStatus'].$transaction_data['insertId'].$secretKey);
        ////$authCode = hash("sha256", $transaction_data['transactionNo'].$transaction_data['transactionAmount'].$channelId.$secretKey);

        if($transaction_data ) {
            //  $authCode = hash("sha256", $transaction_data->transaction_id.$transaction_data->order_amt.$channelId.$data_sprint['transactionStatus'].$transaction_data->insert_id.$secretKey);
            $authCode = hash("sha256", $transaction_data->transaction_id.$total_amount.$channelId.$data_sprint['transactionStatus'].$transaction_data->insert_id.$secretKey); // 20170503
        }else{
            $authCode = "";
        };

          $order_status = "4";
          $status_code = "01";
          $response['paymentStatus']  = "01";
          $response['paymentMessage'] = "Unknown Error";
              /*
              Validation process
              */
        if(!$transaction_data ) { // 1.  Validate Transaction // tested 20170405
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction";
        }elseif($data_sprint['channelId'] != $channelId ) { // 2.  Validate Channel Id // tested 20170405
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Channel ID";
        }elseif($data_sprint['currency'] != "IDR" ) { // 3.  Validate Currency // tested 20170405
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Currency";
        }elseif($data_sprint['transactionNo'] != $transaction_data->transaction_id ) { // 4.  Validate Transaction Number // tested 20170405
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction Number";
            // }elseif( $data_sprint['transactionAmount'] != $transaction_data->order_amt ){ // 5.  Validate Transaction Amount // tested 20170405
        }elseif($data_sprint['transactionAmount'] != $total_amount ) { // 5.  Validate Transaction Amount // tested 20170405 // 20170503
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction Amount";
        }elseif($data_sprint['transactionStatus'] != "00" ) { // 6.  Validate Transaction Status // tested 20170405
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction Status";
        }elseif($data_sprint['insertId'] != $transaction_data->insert_id ) { // 7.  Validate Insert ID // tested 20170405
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Insert ID";
        }elseif($transaction_data->payment_status == "00") { // $transaction_data['status'] == "PAID" ) // 8.  Validate Transaction Status => PAID // tested 20170406
            $response['paymentStatus']  = "02";
            $response['paymentMessage'] = "Transaction has been paid";
            $order_status = "1";
            $status_code = "02";
        }elseif($transaction_data->payment_status == "02") { // $transaction_data['status'] == "PAID" ) // 8.  Validate Transaction Status => PAID // tested 20170406
            $response['paymentStatus']  = "02";
            $response['paymentMessage'] = "Transaction has been paid";
            $order_status = "1";
            $status_code = "02";
        }elseif($tanggal > $expire_date || $transaction_data2->status_pembayaran=="expired") { // $transaction_data['status'] == "EXPIRED" ){ // 9.  Validate Transaction Status => EXPIRED // optional updated status by checking transaction expire datetime
            $response['paymentStatus']  = "04";
            $response['paymentMessage'] = "Transaction has been expired";
            $status_code = "04";
        }elseif($transaction_data->payment_status == "05") { // $transaction_data['status'] == "CANCELLED" ){ // 10.  Validate Transaction Status => CANCELLED // optional updated status triggered by merchant
            $response['paymentStatus']  = "05";
            $response['paymentMessage'] = "Transaction has been cancelled";
            $status_code = "05";
        }elseif($data_sprint['authCode'] != $authCode ) { // 11. Validate authCode // tested 20170407
                 $response['paymentStatus']  = "01";
                 $response['paymentMessage'] = "Invalid authCode";
        }else{ // Success // tested 20170407
            $response['paymentStatus']  = "00"; // -> "02";
            $response['paymentMessage'] = "Success"; // -> "PAID";
            $order_status = "1";
            $status_code = "00";

            $update_transaksi = DB::table('nm_transaksi')
            ->where('transaction_id', $transaction_id)
            ->update([
                'status_pembayaran' => 'sudah dibayar',
                'order_status' => $order_status
            ]);
            $update_order = DB::table('nm_order')
            ->where('transaction_id', $transaction_id)
            ->update([
                'order_tgl_pesanan_dibayarkan' => $tanggal,
                'approval_code' => $paymentReffId['fullTransaction']
            ]);

            // -- poin ----

            $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($transaction_id);
            $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($transaction_id);

            //-- kondisi poin digunakan
            $get_detail_transaksi = Home::get_detail_transaksi($transaction_id);

            $jumlah_poin = (floatval($get_detail_transaksi[0]->total_belanja) + floatval($get_detail_transaksi[0]->shipping) - floatval($get_detail_transaksi[0]->diskon) - floatval($get_detail_transaksi[0]->poin_digunakan))/1000;

            if($get_detail_transaksi[0]->poin_digunakan > 0)
            {
                Transactions::update_poin_to_zero($get_cus_data_by_trans_id[0]->cus_id);
            }else {
                // -- jumlah poin customer lama + baru --
                $jumlah_poin = floatval($jumlah_poin) + floatval($get_cus_data_by_trans_id[0]->total_poin);
            }

            Transactions::change_jumlah_poin($jumlah_poin, $get_cus_data_by_trans_id[0]->cus_id);

            // recall unsync
            self::recall_unsync();

            if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                $payment_channel = null;
            }else {
                $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
            }
            $metode_pembayaran_raw = DB::table('nm_payment_method_header')
            ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
            ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
            ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
            ->first();
            if ($metode_pembayaran_raw == null) {
                $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                ->first();
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
            }else {
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
            }

            $send_mail_data = array(
                'id_transaksi' => $transaction_id,
                'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                'metode_pay' => $metode_pembayaran,
                'items_order' => $get_data_order_by_trans_id

            );

            //dd($send_mail_data);
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
                'emails.notifikasi_email_konfirmasi_pembayaran',
                $send_mail_data, function ($message) use ($get_cus_data_by_trans_id){
                    $message->to($get_cus_data_by_trans_id[0]->cus_email)
                    ->subject('Notifikasi Pembayaran');
                }
            );

            foreach ($get_data_order_by_trans_id as $row) {
                $select_mer_id = Transactions::get_data_mer_by_order_id($row->order_id);

                if($select_mer_id->postalservice_code == "JNE_REG"){
                    $postalservice = "JNE Reguler";
                }elseif ($select_mer_id->postalservice_code == "JNE_OK") {
                    $postalservice = "JNE OK";
                }elseif ($select_mer_id->postalservice_code == "JNE_YES") {
                    $postalservice = "JNE YES";
                }elseif ($select_mer_id->postalservice_code == "POPBOX") {
                    $postalservice = "Popbox";
                }elseif ($select_mer_id->postalservice_code == "AL") {
                    $postalservice = "Ambil Langsung";
                }else {
                    $postalservice = $select_mer_id->postalservice_code;
                }

                if($select_mer_id->mer_email != '' || $select_mer_id->mer_email != null)
                {
                    $send_mail_to_mer = array(
                        'trans_id' => $select_mer_id->transaction_id,
                        'title' => $select_mer_id->pro_title,
                        'pro_sku_merchant' => $select_mer_id->pro_sku_merchant,
                        'order_qty' => $select_mer_id->order_qty,
                        'cus_name' =>$select_mer_id->cus_name,
                        'cus_phone' => $select_mer_id->cus_phone,
                        'postalservice' => $postalservice
                    );
                    $admin = DB::table('nm_emailsetting')->first();
    				$admin_email = $admin->es_noreplyemail;
                    Config::set('mail.from.address', $admin_email);
                        Mail::send(
                        'emails.notifikasi_email_konfirmasi_ke_merchant',
                        $send_mail_to_mer, function($message) use ($select_mer_id,$send_mail_to_mer){
                            $message->to($select_mer_id->mer_email)
                            ->subject('Notifikasi Transaksi');
                        }
                    );
                }
                else{
                }
            }
        };

                // Update transaction status

              $insertResponse = DB::table('nm_order')
              ->where('transaction_id', $transaction_id)
              ->update(
                  ['message' => $response['paymentMessage'],
                  'status_code' => $status_code,
                  'payment_status' => $status_code,
                  'order_status' => $order_status]
              );

        return response()->json($response)->header('Content-Type: ', 'application/json');
    }
    public function sprintasia_check_payment_flag($channel, Request $request)
    {
        //01 BCA Virtual Account
        //02 Permata Virtual Account
        //03 Kredivo
        if ($channel == '01') {
            $channelId = config('sprintasia.bcava_channelid');
        }elseif ($channel == '02') {
            $channelId = config('sprintasia.permatava_channelid');
        }elseif ($channel == '03') {
            $channelId = config('sprintasia.kredivo_channelid');
        }else {
            return 'Unknown Payment Flag URL';
        }
        $secretKey = config('sprintasia.secret_key');

        // Parsing request data from Sprint
        $data_sprint = array(
          "channelId"           => $request->input('channelId'),
          "currency"            => $request->input('currency'),
          "transactionNo"       => $request->input('transactionNo'),
          "transactionAmount"   => $request->input('transactionAmount'),
          "transactionDate"     => $request->input('transactionDate'),
          "transactionStatus"   => $request->input('transactionStatus'),
          "transactionMessage"  => $request->input('transactionMessage'),
          "flagType"            => $request->input('flagType'),
          "insertId"            => $request->input('insertId'),
          "paymentReffId"       => $request->input('paymentReffId'),
          "authCode"            => $request->input('authCode'),
          "additionalData"      => $request->input('additionalData')
          );
        $paymentReffId = json_decode($request->input('paymentReffId'), true);

        // Prepare response data
        $response   = array(
          "channelId"           => $request->input('channelId'),          // Channel ID sent by Sprint
          "currency"            => $request->input('currency'),           // Currency sent by Sprint (IDR)
          "paymentStatus"       => "",                              // Payment Status ( 00 => Success , 01,03 => Failed , 02 => isPaid , 04 => isExpired , 05 => isCancelled )
          "paymentMessage"      => "",                              // Payment Message
          "flagType"            => $request->input('flagType'),           // Flag Type sent by Sprint
          "paymentReffId"       => $request->input('paymentReffId'),      // Payment Referrence ID sent by Sprint
        );

        date_default_timezone_set('Asia/Jakarta');
        $strtotime = strtotime($request->input('transactionDate'));
        $strtotime2 = strtotime('+24 hours', $strtotime);
        $expire_date = date('Y-m-d H:i:s', $strtotime2);
        $tanggal = date('Y-m-d H:i:s');

        $transaction_id = $data_sprint['transactionNo'];
        $transaction_data = DB::table('nm_order')
        ->leftJoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
        ->where('nm_order.transaction_id', $transaction_id)
        ->first();

        $pajak = 0;
        $data_submit = DB::table('nm_order')->where('transaction_id', $transaction_id)->get();
        $total_amount_sebelum_pajak = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '<>', '1')->where('order_pro_id', '<>', '2')->sum('order_amt');
        $total_shipping = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '1')->sum('order_amt');
        $total_diskon = DB::table('nm_order')->where('transaction_id', $transaction_id)->where('order_pro_id', '2')->sum('order_amt');
        $total_amount = ($total_amount_sebelum_pajak + ($total_amount_sebelum_pajak * $pajak)) + $total_shipping + $total_diskon; // 20170503
        $total_amount = intval($total_amount);

        if($transaction_data) {
            $authCode = hash("sha256", $transaction_data->transaction_id.$total_amount.$channelId.$data_sprint['transactionStatus'].$transaction_data->insert_id.$secretKey); // 20170503
        }else{
            $authCode = "";
        }

        $order_status = "4";
        $status_code = "01";
        $response['paymentStatus']  = "01";
        $response['paymentMessage'] = "Unknown Error";

        if(!$transaction_data ) { // 1.  Validate Transaction
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction";
        }elseif($data_sprint['channelId'] != $channelId ) { // 2.  Validate Channel Id
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Channel ID";
        }elseif($data_sprint['currency'] != "IDR" ) { // 3.  Validate Currency
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Currency";
        }elseif($data_sprint['transactionNo'] != $transaction_data->transaction_id ) { // 4.  Validate Transaction Number
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction Number";
        }elseif($data_sprint['transactionAmount'] != $total_amount ) { // 5.  Validate Transaction Amount
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction Amount";
        }elseif($data_sprint['transactionStatus'] != "00" ) { // 6.  Validate Transaction Status
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Transaction Status";
        }elseif ($data_sprint['flagType'] != '11' && $data_sprint['flagType'] != '12' && $data_sprint['flagType'] != '13'
                && $data_sprint['flagType'] != '21' && $data_sprint['flagType'] != '22' && $data_sprint['flagType'] != '23') { // 7. Validate FlagType
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Flagtype";
        }elseif($data_sprint['insertId'] != $transaction_data->insert_id ) { // 8.  Validate Insert ID
            $response['paymentStatus']  = "01";
            $response['paymentMessage'] = "Invalid Insert ID";
        }elseif($transaction_data->payment_status == "00") { // 9.  Validate Transaction Status => PAID
            $response['paymentStatus']  = "02";
            $response['paymentMessage'] = "Transaction has been paid";
            $order_status = "1";
            $status_code = "02";
        }elseif($transaction_data->payment_status == "02") { // 9.  Validate Transaction Status => PAID
            $response['paymentStatus']  = "02";
            $response['paymentMessage'] = "Transaction has been paid";
            $order_status = "1";
            $status_code = "02";
        }elseif($tanggal > $expire_date || $transaction_data->status_pembayaran=="expired") { // 10.  Validate Transaction Status => EXPIRED // optional updated status by checking transaction expire datetime
            $response['paymentStatus']  = "04";
            $response['paymentMessage'] = "Transaction has been expired";
            $status_code = "04";
        }elseif($transaction_data->payment_status == "05") { // 11.  Validate Transaction Status => CANCELLED // optional updated status triggered by merchant
            $response['paymentStatus']  = "05";
            $response['paymentMessage'] = "Transaction has been cancelled";
            $status_code = "05";
        }elseif($data_sprint['authCode'] != $authCode ) { // 12. Validate authCode
             $response['paymentStatus']  = "01";
             $response['paymentMessage'] = "Invalid authCode";
        }else {
            $response['paymentStatus']  = "00"; // -> "02";
            $response['paymentMessage'] = "Success"; // -> "PAID";
            $order_status = "1";
            $status_code = "00";

            $update_transaksi = DB::table('nm_transaksi')
            ->where('transaction_id', $transaction_id)
            ->update([
                'status_pembayaran' => 'sudah dibayar',
                'order_status' => $order_status
            ]);
            $update_order = DB::table('nm_order')
            ->where('transaction_id', $transaction_id)
            ->update([
                'order_tgl_pesanan_dibayarkan' => $tanggal,
                'approval_code' => $paymentReffId['fullTransaction']
            ]);

            // -- poin ----

            $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($transaction_id);
            $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($transaction_id);

            //-- kondisi poin digunakan
            $get_detail_transaksi = Home::get_detail_transaksi($transaction_id);

            $jumlah_poin = (floatval($get_detail_transaksi[0]->total_belanja) + floatval($get_detail_transaksi[0]->shipping) - floatval($get_detail_transaksi[0]->diskon) - floatval($get_detail_transaksi[0]->poin_digunakan))/1000;

            if($get_detail_transaksi[0]->poin_digunakan > 0)
            {
                Transactions::update_poin_to_zero($get_cus_data_by_trans_id[0]->cus_id);
            }else {
                // -- jumlah poin customer lama + baru --
                $jumlah_poin = floatval($jumlah_poin) + floatval($get_cus_data_by_trans_id[0]->total_poin);
            }

            Transactions::change_jumlah_poin($jumlah_poin, $get_cus_data_by_trans_id[0]->cus_id);

            // recall unsync
            self::recall_unsync();

            if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                $payment_channel = null;
            }else {
                $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
            }
            $metode_pembayaran_raw = DB::table('nm_payment_method_header')
            ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
            ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
            ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
            ->first();
            if ($metode_pembayaran_raw == null) {
                $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                ->first();
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
            }else {
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
            }

            $send_mail_data = array(
                'id_transaksi' => $transaction_id,
                'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                'metode_pay' => $metode_pembayaran,
                'items_order' => $get_data_order_by_trans_id

            );

            //dd($send_mail_data);
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
                'emails.notifikasi_email_konfirmasi_pembayaran',
                $send_mail_data, function ($message) use ($get_cus_data_by_trans_id){
                    $message->to($get_cus_data_by_trans_id[0]->cus_email)
                    ->subject('Notifikasi Pembayaran');
                }
            );

            foreach ($get_data_order_by_trans_id as $row) {
                $select_mer_id = Transactions::get_data_mer_by_order_id($row->order_id);

                if($select_mer_id->postalservice_code == "JNE_REG"){
                    $postalservice = "JNE Reguler";
                }elseif ($select_mer_id->postalservice_code == "JNE_OK") {
                    $postalservice = "JNE OK";
                }elseif ($select_mer_id->postalservice_code == "JNE_YES") {
                    $postalservice = "JNE YES";
                }elseif ($select_mer_id->postalservice_code == "POPBOX") {
                    $postalservice = "Popbox";
                }elseif ($select_mer_id->postalservice_code == "AL") {
                    $postalservice = "Ambil Langsung";
                }else {
                    $postalservice = $select_mer_id->postalservice_code;
                }

                if($select_mer_id->mer_email != '' || $select_mer_id->mer_email != null)
                {
                    $send_mail_to_mer = array(
                        'trans_id' => $select_mer_id->transaction_id,
                        'title' => $select_mer_id->pro_title,
                        'pro_sku_merchant' => $select_mer_id->pro_sku_merchant,
                        'order_qty' => $select_mer_id->order_qty,
                        'cus_name' =>$select_mer_id->cus_name,
                        'cus_phone' => $select_mer_id->cus_phone,
                        'postalservice' => $postalservice
                    );
                    $admin = DB::table('nm_emailsetting')->first();
    				$admin_email = $admin->es_noreplyemail;
                    Config::set('mail.from.address', $admin_email);
                        Mail::send(
                        'emails.notifikasi_email_konfirmasi_ke_merchant',
                        $send_mail_to_mer, function($message) use ($select_mer_id,$send_mail_to_mer){
                            $message->to($select_mer_id->mer_email)
                            ->subject('Notifikasi Transaksi');
                        }
                    );
                }
            }
        }

        $insertResponse = DB::table('nm_order')
        ->where('transaction_id', $transaction_id)
        ->update(
            ['message' => $response['paymentMessage'],
            'status_code' => $status_code,
            'payment_status' => $status_code,
            'order_status' => $order_status]
        );

        return response()->json($response)->header('Content-Type: ', 'application/json');
    }
    // Sprintasia ends

    public function bid_payment()
    {
        $bid_price  = Input::get('bid_update_value');
        $bid_auc_id = Input::get('auction_bid_proid_popup');
        $return_url = Input::get('return_url');
        if (Session::get('customerid')) {
            $customerid = Session::get('customerid');
        } else {
            $customerid = 0;
        }
        $customerdetails             = Customerprofile::get_customer_details($customerid);
        $get_social_media_url        = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title              = Home::get_cms_page_title();
        $get_image_logoicons_details = Home::get_image_logoicons_details();
        $get_acution_details         = Home::get_action_details_by_id($bid_auc_id);
        $getlogodetails              = Home::getlogodetails();
        $getmetadetails              = Home::getmetadetails();
        $get_contact_det             = Footer::get_contact_details();
        $getanl                      = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $footer                      = view('includes.footer')
        ->with('cms_page_title', $cms_page_title)
        ->with('get_partners', $get_partners)
        ->with('get_social_media_url', $get_social_media_url)
        ->with('getanl', $getanl);

        return view('bid_payment')->with('footer', $footer)
        ->with('get_image_logoicons_details', $get_image_logoicons_details)
        ->with('get_acution_details', $get_acution_details)
        ->with('price', $bid_price)
        ->with('customerdetails', $customerdetails)
        ->with('return_url', $return_url)
        ->with('metadetails', $getmetadetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);

    }

    public function place_bid_payment()
    {

        $bid_price        = Input::get('bid_amt');
        $bid_auc_id       = Input::get('auction_bid_proid_popup');
        $bid_auc_shipping = Input::get('bid_shipping');
        $return_url       = Input::get('return_url');
        $entry            = array(
            'oa_pro_id' => Input::get('oa_pro_id'),
            'oa_cus_id' => Input::get('oa_cus_id'),
            'oa_cus_name' => Input::get('oa_cus_name'),
            'oa_cus_email' => Input::get('oa_cus_email'),
            'oa_cus_address' => Input::get('oa_cus_address'),
            'oa_bid_amt' => Input::get('oa_bid_amt'),
            'oa_bid_shipping_amt' => Input::get('bid_shipping'),
            'oa_original_bit_amt' => Input::get('oa_original_bit_amt')
        );

        Home::save_bidding_details($entry);
        $get_social_media_url        = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title              = Home::get_cms_page_title();
        $get_image_logoicons_details = Home::get_image_logoicons_details();
        $get_acution_details         = Home::get_action_details_by_id($bid_auc_id);
        $getlogodetails              = Home::getlogodetails();
        $getmetadetails              = Home::getmetadetails();
        $get_contact_det             = Footer::get_contact_details();
        $getanl                      = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $footer                      = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('getanl', $getanl);

        return view('place_bid_payment')->with('footer', $footer)->with('get_image_logoicons_details', $get_image_logoicons_details)->with('get_acution_details', $get_acution_details)->with('price', $bid_price)->with('return_url', $return_url)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);

    }

    public function bid_payment_error()
    {
        return Redirect::to('index')->with('error', ' Error!  Already Auction has bid. Try with new amount!');
    }

    public function show_payment_result_columbia($transaction_id)
    {
        if(Session::has('customerid'))
        {
            //dd($transaction_id);
            $cust_id                      = Session::get('customerid');
            $cus_details                  = Home::get_customer_details_by_id($cust_id);
            $transaction_header           = Home::get_transaction_header($transaction_id);
            $transaction_detail           = Home::get_transaction_detail2($transaction_id);
            $get_pay_settings_new         = Settings::get_pay_settings_new();
            $get_history_transaksi        = Home::get_history_transaksi($transaction_id);

            // ceking apakah customer sama dengan yang beli
            if($cus_details[0]->cus_id != $transaction_detail[0]->order_cus_id)
            {
                return Redirect::to('');
            }

            // bawaan
            $city_details                 = Register::get_city_details();
            $header_category              = Home::get_header_category();
            $most_visited_product         = Home::get_most_visited_product();
            $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
            $category_count               = Home::get_category_count($header_category);
            $get_product_details_typeahed = Home::get_product_details_typeahed();
            $main_category                = Home::get_header_category();
            $sub_main_category            = Home::get_sub_main_category($main_category);
            $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
            $second_sub_main_category     = Home::get_second_sub_main_category();
            $get_social_media_url         = Home::get_social_media_url();
            $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
            $cms_page_title               = Home::get_cms_page_title();
            $country_details              = Register::get_country_details();
            $addetails                    = Home::get_ad_details();
            $noimagedetails               = Home::get_noimage_details();
            $getbannerimagedetails        = Home::getbannerimagedetails();
            $getmetadetails               = Home::getmetadetails();
            $getlogodetails               = Home::getlogodetails();
            $get_contact_det              = Footer::get_contact_details();
            $country_details              = Register::get_country_details();
            $getanl                       = Settings::social_media_settings();
            $general                      = Home::get_general_settings();
            $get_pm_img_details           = Footer::get_paymentmethod_img_details();
            $get_pay_settings             = Settings::get_pay_settings();
            $getdetailtransaksi           = Home::get_detail_transaksi($transaction_id);
            // dd($transaction_detail);
            $email_settings = Settings::view_email_settings();
            // dd($email_settings);
            if (Session::has('customerid')) {
                $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
            } else {
                $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
            }

            $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
            $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

            return view('paymentresult_columbia')
            ->with('navbar', $navbar)
            ->with('header', $header)
            ->with('cus_details', $cus_details)
            ->with('footer', $footer)
            ->with('header_category', $header_category)
            ->with('getdetailtransaksi', $getdetailtransaksi)
            ->with('get_product_details_by_cat', $get_product_details_by_cat)
            ->with('most_visited_product', $most_visited_product)
            ->with('category_count', $category_count)
            ->with('get_product_details_typeahed', $get_product_details_typeahed)
            ->with('main_category', $main_category)
            ->with('sub_main_category', $sub_main_category)
            ->with('second_main_category', $second_main_category)
            ->with('second_sub_main_category', $second_sub_main_category)
            ->with('addetails', $addetails)
            ->with('noimagedetails', $noimagedetails)
            ->with('bannerimagedetails', $getbannerimagedetails)
            ->with('get_meta_details', $getmetadetails)
            ->with('get_contact_det', $get_contact_det)
            ->with('get_pay_settings', $get_pay_settings)
            ->with('general', $general)
            ->with('transaction_header', $transaction_header)
            ->with('transaction_detail', $transaction_detail)
            ->with('get_pay_settings_new', $get_pay_settings_new)
            ->with('get_history_transaksi',$get_history_transaksi)
            ->with('email_settings', $email_settings);
        }
        else
        {
            return Redirect::to('');
        }
    }

    public function show_payment_result_transfer_bank($orderid)
    {
        $cust_id = Session::get('customerid');
        //dd($cust_id);
        $cus_details = Home::get_customer_details_by_id($cust_id);
        // dd($cus_details[0]->cus_name);

        $converorderid = base64_decode($orderid);
        //dd($converorderid);

        $getorderdetails = Home::getorderdetails($converorderid);
        //dd($getorderdetails);
        //common

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();

        $most_visited_product         = Home::get_most_visited_product();

        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();

        $get_contact_det              = Footer::get_contact_details();
        $country_details              = Register::get_country_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_pay_settings             = Settings::get_pay_settings();
        //dd($get_pay_settings);
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('paymentresult_transfer_bank')
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('cus_details', $cus_details)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('addetails', $addetails)
        ->with('noimagedetails', $noimagedetails)
        ->with('bannerimagedetails', $getbannerimagedetails)
        ->with('get_meta_details', $getmetadetails)
        ->with('orderdetails', $getorderdetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('get_pay_settings', $get_pay_settings)
        ->with('general', $general);
    }

    public function show_payment_result_cod($orderid)
    {
        $cust_id = Session::get('customerid');

        $converorderid = base64_decode($orderid);

        $getorderdetails              = Home::getordercoddetails($converorderid);
        $get_subtotal                 = Home::get_subtotal($converorderid);
        $get_tax                      = Home::get_tax($converorderid);
        $get_shipping_amount          = Home::get_shipping_amount($converorderid);
        //common
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();

        $most_visited_product         = Home::get_most_visited_product();

        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $country_details = Register::get_country_details();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('paymentresultcod')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('addetails', $addetails)->with('noimagedetails', $noimagedetails)->with('bannerimagedetails', $getbannerimagedetails)->with('metadetails', $getmetadetails)->with('orderdetails', $getorderdetails)->with('get_contact_det', $get_contact_det)->with('get_subtotal', $get_subtotal)->with('get_tax', $get_tax)->with('get_shipping_amount', $get_shipping_amount)->with('general', $general);
    }

    public function show_payment_result($orderid)
    {
        $cust_id = Session::get('customerid');

        $converorderid = base64_decode($orderid);

        $getorderdetails = Home::getorderdetails($converorderid);

        //common
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();

        $most_visited_product         = Home::get_most_visited_product();

        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();

        $get_contact_det              = Footer::get_contact_details();
        $country_details              = Register::get_country_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pay                      = Settings::get_pay_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $total_amount_sebelum_pajak = DB::table('nm_order')->where('transaction_id', $converorderid)->where('order_pro_id', '<>', '1')->where('order_pro_id', '<>', '2')->sum('order_amt');
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('paymentresult')->with('get_cur', $get_cur)->with('total_amount_sebelum_pajak', $total_amount_sebelum_pajak)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('addetails', $addetails)->with('noimagedetails', $noimagedetails)->with('bannerimagedetails', $getbannerimagedetails)->with('get_meta_details', $getmetadetails)->with('orderdetails', $getorderdetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function run_preprocessing()
    {
        $date = \Carbon\Carbon::now();
        \Queue::later($date, new \App\Commands\PreProcessing('test'));

        return "Berhasil";
    }

    public function run_preprocessing2()
    {
        $date = \Carbon\Carbon::now();
        \Queue::later($date, new \App\Commands\PreProcessing2('test'));

        return "Berhasil";
    }

    public function run_preprocessing3()
    {
        $date = \Carbon\Carbon::now();
        \Queue::later($date, new \App\Commands\PreProcessing3('test'));

        return "Berhasil";
    }

    public function search()
    {
        $q = Input::get('q');
        $q = strtolower($q);

        $searchTerms = explode(' ', $q);

        $b = 0.75;

        $terms = DB::table('term_frequency')
        ->where('term_frequency.field', 'Total')
        ->where('nm_product.pro_isapproved', '=', 1)
        ->where(function($q) use($searchTerms){
            foreach ($searchTerms as $term) {
                $q = $q->orWhere('term_frequency.term', $term);
            }
        })
        // ->leftJoin('inverse_document_frequency', 'inverse_document_frequency.term', '=', 'term_frequency.term')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'term_frequency.pro_id')
        ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->groupBy('term_frequency.pro_id')
        // ->selectRaw('sum(weight) as weight, term_frequency.pro_id, pro_title, pro_price, pro_disprice, pro_no_of_purchase, pro_Img, pro_desc, pro_qty, mc_name, smc_name, sb_name, ssb_name')
        ->selectRaw('sum(weight_bm25) as weight, term_frequency.pro_id, pro_title, pro_price, pro_disprice, pro_no_of_purchase, pro_Img, pro_desc, pro_qty, mc_name, smc_name, sb_name, ssb_name')
        ->orderBy('weight', 'desc')
        ->get();
        dd($terms);
        $id          = $q;
        $mc_id = Input::get('mc_id');
        $smc_id =Input::get('smc_id');
        $sb_id = Input::get('sb_id');
        $ssb_id = Input::get('ssb_id');
        $spc_value = Input::get('spc_value');

        $param['pro_title']         = $id;
        $param['mc_id']             = Input::get('mc_id');
        $param['smc_id']            = Input::get('smc_id');
        $param['sb_id']             = Input::get('sb_id');
        $param['ssb_id']            = Input::get('ssb_id');

        $param['spc_value']          = Input::get('spc_value');



        $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

        if (strpos($url, '?') == false) {
          $url = $url.'?';
        }
        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );


        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();
        $searchTermss                 = Home::get_deal_search($id);
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $mc_result                    = Products::search_mc($param);
        $smc_result                   = Products::search_smc($param);
        $sb_result                    = Products::search_sb($param);
        $ssb_result                   = Products::search_ssb($param);
        $spc_result                   = Products::search_spc($param);
        $search_max_min               = Products::search_max_min($param);
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')
            ->with('country_details', $country_details)
            ->with('metadetails', $getmetadetails)
            ->with('general', $general)
            ->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')
            ->with('country_details', $country_details)
            ->with('metadetails', $getmetadetails)
            ->with('general', $general)
            ->with('getanl', $getanl);
        }

        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);

        return view('search')
        ->with('q', $q)
        ->with('mc_id', $mc_id)
        ->with('smc_id', $smc_id)
        ->with('sb_id', $sb_id)
        ->with('ssb_id', $ssb_id)


        ->with('mc_result', $mc_result)
        ->with('smc_result', $smc_result)
        ->with('sb_result', $sb_result)
        ->with('ssb_result', $ssb_result)
        ->with('spc_result', $spc_result)

        ->with('requestUrl', $escaped_url)
        ->with('search_max_min', $search_max_min)
        // ->with('get_spec', $get_spec)
        // ->with('get_spec_details', $get_spec_details)

        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('product_details', $product_details)
        ->with('deals_details', $deals_details)
        ->with('auction_details', $auction_details)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('addetails', $addetails)
        ->with('noimagedetails', $noimagedetails)
        ->with('bannerimagedetails', $getbannerimagedetails)
        ->with('metadetails', $getmetadetails)
        ->with('searchTerms', $terms)
        ->with('searchTermss', $searchTermss)
        ->with('get_contact_det', $get_contact_det)
        ->with('general', $general);
    }

    public function register()
    {
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }
        $header         = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer         = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
        $country_return = Merchant::get_country_detail();
        return view('register')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('country_details', $country_return)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function register_submit()
    {

        $data = Input::except(
            array(
            '_token'
            )
        );

        $cemail      = Input::get('email');
        $check_email = Register::check_email_ajax($cemail);
        if ($check_email) {
            return Redirect::to('registers')->with('mail_exist', 'Already Use Email Exist')->withInput();
        }

        else
        {
            $cname = Input::get('customername');
            $cemail = Input::get('email');
            $caddress1 = Input::get('address1');
            $caddress2 = Input::get('address2');
            $cpwd      = Input::get('pwd');
            $cus_phone = Input::get('mobile');
            $pwd       = md5($cpwd);
            $city = Input::get('select_city');
            $country = Input::get('select_country');
            $address = Input::get('email');

            $entry_mail_cus = array(
                'first_name' => Input::get('email'),
                'cus_name' => Input::get('customername'),
                'password' => $cpwd,
                );

            \App\Mail_Cus::insert_mail_cus($entry_mail_cus);

            //var_dump("selesao insert mail");

            //$fiveSecs = \Carbon\Carbon::now()->addSeconds(5);

            //Queue::push(new SendCustomerMail("cek"));
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            // Mail::send(
            //     'emails.registermail', $entry_mail_cus, function ($message) {
            //         $message->to(Input::get('email'))->subject('Register Account Created Successfully');
            //     }
            // );
            $test = FALSE;
            if ($test) {
                return Redirect::to('registers')->with('mail_exist', 'Registered Failed, Mail Not Send');
            }else {
                $entry = array(
                    'cus_name' => Input::get('customername'),
                    'cus_address1' => $caddress1,
                    'cus_address2' => $caddress2,
                    'cus_email' => Input::get('email'),
                    'cus_pwd' => $pwd,
                    'cus_phone' => Input::get('mobile'),
                    'cus_country' => Input::get('select_country'),
                    'cus_city' => Input::get('select_city'),
                    'cus_logintype' => 2,
                    'cus_status' => 0

                );

                $customerid = Register::insert_customer($entry);

                $entry_shipping = array(

                    'ship_name' => Input::get('customername'),
                    'ship_address1' => $caddress1,
                    'ship_address2' => $caddress2,
                    'ship_phone' => $cus_phone,
                    'ship_email' => $cemail,
                    'ship_ci_id' => Input::get('select_city'),
                    'ship_country' => Input::get('select_country'),
                    'ship_cus_id' => $customerid
                );

                Register::insert_customer_shipping($entry_shipping);
            }

        }

        return Redirect::to('registers')->with('success', 'Registered Successfully, Please Verified Your Account First Before Login');
    }

    public function newsletter()
    {

        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();
        $product_details              = Home::get_product_details();
        $most_visited_product         = Home::get_most_visited_product();
        $deals_details                = Home::get_deals_details();
        $auction_details              = Home::get_auction_details();
        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();
        $get_contact_det              = Footer::get_contact_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();
        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header         = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer         = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
        $country_return = Merchant::get_country_detail();
        return view('newsletter')->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('header_category', $header_category)->with('get_product_details_by_cat', $get_product_details_by_cat)->with('most_visited_product', $most_visited_product)->with('category_count', $category_count)->with('get_product_details_typeahed', $get_product_details_typeahed)->with('main_category', $main_category)->with('sub_main_category', $sub_main_category)->with('second_main_category', $second_main_category)->with('second_sub_main_category', $second_sub_main_category)->with('country_details', $country_return)->with('metadetails', $getmetadetails)->with('get_contact_det', $get_contact_det)->with('general', $general);
    }

    public function subscription_submit()
    {
        $data = Input::except(
            array(
            '_token'
            )
        );

        $email       = Input::get('email');
        $check_email = Register::check_email_ajaxs($email);
        if ($check_email) {
            return Redirect::to('newsletter')->with('Error_letter', 'Already Use Email Exist');
        } else {
            $email = Input::get('email');
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
                'emails.subscription_mail', array(
                'email' => Input::get('email')
                ), function ($message) {
                    $message->to(Input::get('email'))->subject('Email Has Been Subscription Successfully');
                }
            );
            $entry = array(

                'email' => Input::get('email')
            );
            $email = Register::insert_email($entry);
        }
        return Redirect::to('newsletter')->with('subscribe', 'Your Email Subscribed Successfully');
    }

    public function compare()
    {
        return view('compare');
    }

    public function addtowish()
    {
        $data = Input::except(
            array(
            '_token'
            )
        );

        $pro_id                       = Input::get('pro_id');
        $cus_id                       = Input::get('cus_id');
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $entry                        = array(

            'ws_pro_id' => Input::get('pro_id'),
            'ws_cus_id' => Input::get('cus_id')
        );
        $wish                         = Register::insert_wish($entry);
        return Redirect::to('user_profile')->with('wish', 'Product Added in Your Wishlist');

    }

    public function productcomments()
    {

        $data = Input::except(
            array(
            '_token'
            )
        );

        $customer_id = Input::get('customer_id');

        $product_id = Input::get('product_id');
        $title      = Input::get('title');
        $comments   = Input::get('comments');
        $ratings    = Input::get('ratings');
        $mcid       = Input::get('mcid');
        $scid       = Input::get('scid');
        $sbid       = Input::get('sbid');
        $ssbid       = Input::get('ssbid');
        $entry = array(
         'customer_id' => Input::get('customer_id'),

            'product_id' => Input::get('product_id'),
            'title' => Input::get('title'),
            'comments' => Input::get('comments'),
            'ratings' => Input::get('ratings')
        );

        $comments = Home::comment_insert($entry);

        return Redirect::to('productview/'.$mcid.'/'.$scid.'/'.$sbid.'/'.$ssbid.'/'.base64_encode($product_id).'');

    }

    public function dealcomments()
    {

        $data = Input::except(
            array(
            '_token'
            )
        );

        $customer_id = Input::get('customer_id');

        $deal_id  = Input::get('deal_id');
        $title    = Input::get('title');
        $comments = Input::get('comments');
        $ratings  = Input::get('ratings');

        $entry = array(
          'customer_id' => Input::get('customer_id'),

            'deal_id' => Input::get('deal_id'),
            'title' => Input::get('title'),
            'comments' => Input::get('comments'),
            'ratings' => Input::get('ratings')
        );

        $comments = Home::comment_insert($entry);

        return Redirect::to('deals')->with('success1', 'Your Deal Product Review Post Successfully');

    }

    public function storecomments()
    {

        $data = Input::except(
            array(
            '_token'
            )
        );

        $customer_id = Input::get('customer_id');

        $store_id = Input::get('store_id');
        $title    = Input::get('title');
        $comments = Input::get('comments');
        $ratings  = Input::get('ratings');
        $entry = array(
        'customer_id' => Input::get('customer_id'),

            'store_id' => Input::get('store_id'),
            'title' => Input::get('title'),
            'comments' => Input::get('comments'),
            'ratings' => Input::get('ratings')
        );

        $comments = Home::comment_insert($entry);

        return Redirect::to('stores')->with('success_store', 'Your Deal Store Review Post Successfully');

    }

    public function smtp_mail_settings()
    {

        $smtp_mail = Home::get_smtp_mail();

        return view('app/config/mail')->with('smtp_mail', $smtp_mail);
    }

    public function get_order_amount()
    {
        $sum_purchase = DB::table('nm_order')->select([DB::raw("SUM(order_amt) as revenue"), DB::raw("SUM(order_tax) as tax")])->get();

        $get_order_date = DB::table('nm_order')
        ->select([DB::raw("DATE_FORMAT(order_date,'%y-%m') as time_key"),DB::raw("SUM(order_qty) as qty")])
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->groupBy('time_key')
        ->get();

        $latest_order = DB::table('nm_order')
                                          ->join('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                                          ->select('nm_order.order_amt', 'nm_order.order_qty', 'nm_customer.cus_name', 'nm_order.order_date')
                                          ->where('order_pro_id', '!=', 1)
                                          ->where('order_pro_id', '!=', 2)
                                          ->orderBy('order_date', 'desc')
                                          ->take(5)->get();

        $latest_search = DB::table('nm_searchterm')->orderBy('search_term_uses_time', 'desc')->take(5)->get();
        $top_search = DB::table('nm_searchterm')->orderBy('search_term_uses_count', 'desc')->take(5)->get();

        $data = [
            'sum_purchase' => $sum_purchase,
            'get_order_date' => $get_order_date,
            'latest_order' => $latest_order,
            'latest_search' => $latest_search,
            'top_search' => $top_search
        ];
        // return view('index_2', ['sum_purchases' => $sum_purchase, 'get_order_dates'=>$get_order_date, 'latest_orders'=>$latest_order, 'latest_searches' => $latest_search, 'top_search'=>$top_search]);
        return response()->json($data);
    }

    public function terms_merchant(){
      $city_details                 = Register::get_city_details();
      $header_category              = Home::get_header_category();
      $getlogodetails               = Home::getlogodetails();
      $getmetadetails               = Home::getmetadetails();
      $cms_page_title               = Home::get_cms_page_title();
      $general                      = Home::get_general_settings();
      $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
      $get_social_media_url         = Home::get_social_media_url();
      $get_contact_det              = Footer::get_contact_details();
      $getanl                       = Settings::social_media_settings();
      $country_details              = Register::get_country_details();
      $get_pm_img_details           = Footer::get_paymentmethod_img_details();

      if (Session::has('customerid')) {
          $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general);
      } else {
          $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
      }
      $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
      $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
      $mer_terms_details = Footer::get_mer_termsandconditons_details();

      return view('terms_merchant')->with('cms_result',$mer_terms_details)->with('navbar',$navbar)->with('header',$header)
      ->with('footer',$footer)->with('header_category',$header_category);

    }

    public function pilih_produk_warna_cb(Request $request)
    {
        $hasil = Home::get_product_details_for_color_cb($request->input('aId'));
        //dd($hasil);
        //return Redirect::to('productview/'.$hasil->mc_name.'/'.$hasil->smc_name.'/'.$hasil->sb_name.'/'.$hasil->ssb_name.'/'.base64_encode($request->input('aId')));

        $url = '/productview/'.$hasil->mc_name.'/'.$hasil->smc_name.'/'.$hasil->sb_name.'/'.$hasil->ssb_name.'/'.base64_encode($request->input('aId'));

        return $url;
    }

    public function api_update_invoice(Request $request)
    {
        $transaction_id = $request->input('transaction_id');
        $items = $request->input('items');
        $status = $request->input('status');
        $tracking_number = $request->input('tracking_number');
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');
        $tgl_pesanan_diproses = 0;
        $tgl_pesanan_dikemas = 0;
        $tgl_pesanan_dikirim = 0;

        if($transaction_id==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }



        $transaksi = DB::table('nm_transaksi')->where('transaction_id', $transaction_id)->first();
        $order = DB::table('nm_order')
        ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('transaction_id', $transaction_id)
        ->where('mer_fname', 'Kukuruyuk')
        ->first();

        // h
        $get_cus_data_order_by_order_id = Transactions::get_cus_data_order_by_order_id($order->order_id);
        // h
        $get_data_by_order_id = Transactions::get_data_order_kukuruyuk_by_trans_id($transaction_id);
        // dd($get_data_by_order_id);
        if ($get_cus_data_order_by_order_id->payment_channel == '') {
            $payment_channel = null;
        }else {
            $payment_channel = $get_cus_data_order_by_order_id->payment_channel;
        }
        $metode_pembayaran_raw = DB::table('nm_payment_method_header')
        ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
        ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_order_by_order_id->order_paytype)
        ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
        ->first();
        if ($metode_pembayaran_raw == null) {
            $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
            ->where('kode_pay_detail', $get_cus_data_order_by_order_id->order_paytype)
            ->first();
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
        }else {
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
        }

        if($order->order_nomor_resi == '' || $order->order_nomor_resi == null){
            if(!$tracking_number){
                $tracking_number = '';
            }
        }else {
            if($tracking_number){
                $tracking_number = $order->order_nomor_resi.', '.$tracking_number;
            }else {
                $tracking_number = $order->order_nomor_resi;
            }
        }

        $tgl_pesanan_diproses = $order->order_tgl_pesanan_diproses;
        $tgl_pesanan_dikemas = $order->order_tgl_pesanan_dikemas;
        $tgl_pesanan_dikirim = $order->order_tgl_pesanan_dikirim;

        if($status=='PICKED'){
            if($order->status_outbound<2){
                $status = 2;

                // e-mail
                $send_mail_data = array(
                    'id_transaksi' => $get_data_by_order_id[0]->transaction_id,
                    'cus_name' => $get_cus_data_order_by_order_id->cus_name,
                    'cus_shipaddress' => $get_cus_data_order_by_order_id->order_shipping_add,
                    'items_order' => $get_data_by_order_id,
                    'metode_pay' => $metode_pembayaran,
                );
                // dd($send_mail_data);
                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                Mail::send(
                    'emails.notifikasi_email_pick',
                    $send_mail_data, function ($message) use ($get_data_by_order_id,$get_cus_data_order_by_order_id){
                        $message->to($get_cus_data_order_by_order_id->cus_email)
                        ->subject('Notifikasi Barang Diproses');
                    }
                );
                // end email
                // return "PICKED";
            }else {
                $status = $order->status_outbound;
            }
            if($order->order_tgl_pesanan_diproses==0){
                $tgl_pesanan_diproses = $date;
            }else {
                $tgl_pesanan_diproses = $order->order_tgl_pesanan_diproses;
            }
        }elseif ($status=='PACKED') {
            if($order->status_outbound<3){
                $status = 3;

                // start email
                $send_mail_data = array(
                    'id_transaksi' => $get_data_by_order_id[0]->transaction_id,
                    'cus_name' => $get_cus_data_order_by_order_id->cus_name,
                    'cus_shipaddress' => $get_cus_data_order_by_order_id->order_shipping_add,
                    'items_order' => $get_data_by_order_id,
                    'metode_pay' => $metode_pembayaran,
                );
                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                Mail::send(
                    'emails.notifikasi_email_pack',
                    $send_mail_data, function ($message) use ($get_data_by_order_id,$get_cus_data_order_by_order_id){
                        $message->to($get_cus_data_order_by_order_id->cus_email)
                        ->subject('Notifikasi Barang Dikemas');
                    }
                );
                // end email
                // return 'PACKED';
            }else {
                $status = $order->status_outbound;
            }
            if($order->order_tgl_pesanan_dikemas==0){
                $tgl_pesanan_dikemas = $date;
            }else {
                $tgl_pesanan_dikemas = $order->order_tgl_pesanan_dikemas;
            }
        }elseif ($status=='SHIPPED') {
            if($order->status_outbound<4){
                $status = 4;

                // start mail
                $send_mail_data = array(
                    'id_transaksi' => $get_cus_data_order_by_order_id->transaction_id,
                    'cus_name' => $get_cus_data_order_by_order_id->cus_name,
                    'cus_shipaddress' => $get_cus_data_order_by_order_id->ship_address1,
                    'metode_pay' => $metode_pembayaran,
                    'ship_service' => $get_cus_data_order_by_order_id->postalservice_code,
                    'no_resi' => $tracking_number,
                    'items_order' => $get_data_by_order_id
                );
                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                Mail::send(
                    'emails.notifikasi_email_shipping_checkout',
                    $send_mail_data, function ($message) use ($get_data_by_order_id,$get_cus_data_order_by_order_id){
                        $message->to($get_cus_data_order_by_order_id->cus_email)
                        ->subject('Notifikasi Barang Dikirim');
                    }
                );
                //end Mail
                // return 'SHIPPED';
            }else {
                $status = $order->status_outbound;
            }
            if($order->order_tgl_pesanan_dikirim==0){
                $tgl_pesanan_dikirim = $date;
            }else {
                $tgl_pesanan_dikirim = $order->order_tgl_pesanan_dikirim;
            }

            $get_transaction_order = DB::table('nm_order')
            ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
            ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
            ->where('transaction_id', $transaction_id)
            ->where('mer_fname', 'Kukuruyuk')
            ->where('order_pro_id', '!=', '1')
            ->where('order_pro_id', '!=', '2')
            ->get();

            foreach ($get_transaction_order as $item) {
                $get_order_qty = $item->order_qty;
                $get_shipped_qty = $item->shipped_qty;

                $get_product = DB::table('nm_product')->where('pro_id', $item->order_pro_id)->first();
                // $get_order_qty = DB::table('nm_order')
                // ->where('transaction_id', $transaction_id)
                // ->where('order_pro_id', $pro_id)
                // ->sum('order_qty');
                // $get_shipped_qty = DB::table('nm_order')
                // ->where('transaction_id', $transaction_id)
                // ->where('order_pro_id', $pro_id)
                // ->sum('shipped_qty');

                // $acc_shipped_qty = $item['acc_shipped_qty'];
                // $acc_shipped_qty = floatval($acc_shipped_qty);
                // $shipped_qty = $item['shipped_qty'];
                // $shipped_qty = floatval($shipped_qty);
                if($get_order_qty > $get_shipped_qty){
                    $old_pro_no_of_purchase = $get_product->pro_no_of_purchase;
                    $new_pro_no_of_purchase = $old_pro_no_of_purchase - $get_order_qty;

                    // $shipped_qty += $get_shipped_qty;

                    if($new_pro_no_of_purchase < $get_product->pro_qty){
                        $sold_status = 1;
                    }elseif ($new_pro_no_of_purchase >= $get_product->pro_qty) {
                        $sold_status = 0;
                    }

                    $update_order = DB::table('nm_order')
                    ->where('transaction_id', $transaction_id)
                    ->where('order_pro_id', $get_product->pro_id)
                    ->update([
                        'acc_shipped_qty'=> $get_order_qty,
                        'shipped_qty' => $get_order_qty,
                    ]);

                    $update_product = DB::table('nm_product')
                    ->where('pro_id', $get_product->pro_id)
                    ->update([
                        'pro_no_of_purchase' => $new_pro_no_of_purchase,
                        'sold_status' => $sold_status
                    ]);
                }
                $update_transaksi = DB::table('nm_transaksi')
                ->where('transaction_id', $transaction_id)
                ->update(['status_outbound_transaksi'=>$status]);
            }
        }else {
            $status = $transaksi->status_outbond_transaksi;
        }
        // if($items){
        //     foreach ($items as $item) {
        //         // if($item['acc_shipped_qty'] > $item['qty'])
        //         // {
        //         //     return response('Akumulasi Quantity Terkirim Melebihi Quantity Yang Dipesan');
        //         //     die();
        //         // }
        //         $get_product = DB::table('nm_product')->where('pro_sku', $item['sku'])->first();
        //
        //         $pro_id = $get_product->pro_id;
        //
        //         $get_order_qty = DB::table('nm_order')
        //         ->where('transaction_id', $transaction_id)
        //         ->where('order_pro_id', $pro_id)
        //         ->sum('order_qty');
        //         $get_shipped_qty = DB::table('nm_order')
        //         ->where('transaction_id', $transaction_id)
        //         ->where('order_pro_id', $pro_id)
        //         ->sum('shipped_qty');
        //
        //         // $acc_shipped_qty = $item['acc_shipped_qty'];
        //         // $acc_shipped_qty = floatval($acc_shipped_qty);
        //         // $shipped_qty = $item['shipped_qty'];
        //         // $shipped_qty = floatval($shipped_qty);
        //         if($get_order_qty > $get_shipped_qty){
        //             $old_pro_no_of_purchase = $get_product->pro_no_of_purchase;
        //             $new_pro_no_of_purchase = $old_pro_no_of_purchase - $get_order_qty;
        //
        //             // $shipped_qty += $get_shipped_qty;
        //
        //             if($new_pro_no_of_purchase < $get_product->pro_qty){
        //                 $sold_status = 1;
        //             }elseif ($new_pro_no_of_purchase >= $get_product->pro_qty) {
        //                 $sold_status = 0;
        //             }
        //
        //             $update_order = DB::table('nm_order')
        //             ->where('transaction_id', $transaction_id)
        //             ->where('order_pro_id', $pro_id)
        //             ->update([
        //                 'acc_shipped_qty'=> $get_order_qty,
        //                 'shipped_qty' => $get_order_qty,
        //             ]);
        //
        //             $update_product = DB::table('nm_product')
        //             ->where('pro_id', $pro_id)
        //             ->update([
        //                 'pro_no_of_purchase' => $new_pro_no_of_purchase,
        //                 'sold_status' => $sold_status
        //             ]);
        //         }
        //         $update_transaksi = DB::table('nm_transaksi')
        //         ->where('transaction_id', $transaction_id)
        //         ->update(['status_outbound_transaksi'=>$status]);
        //     }
        // }

        $update_order = DB::table('nm_order')
        ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('transaction_id', $transaction_id)
        ->where('mer_fname', 'Kukuruyuk')
        ->update([
            'status_outbound' => $status,
            'order_nomor_resi' => $tracking_number,
            'order_tgl_pesanan_dikirim' => $tgl_pesanan_dikirim,
            'order_tgl_pesanan_dikemas' => $tgl_pesanan_dikemas,
            'order_tgl_pesanan_diproses' => $tgl_pesanan_diproses
        ]);

        $update_transaksi = DB::table('nm_transaksi')
        ->where('transaction_id', $transaction_id)
        ->update(['status_outbound_transaksi'=>$status]);

        return response('Sukses', 200);
    }

    public function bayar_ulang(Request $request, $jenis)
    {
        if($jenis=='doku'){
            $trans_id = $request->input('transaction_id');
            $mallid = config('dokularavel.MALL_ID');
            $currency = config('dokularavel.CURRENCY');
            $purchasecurrency = config('dokularavel.CURRENCY');
            $sharedkey = config('dokularavel.SHARED_KEY');
            $chainmerchant = 'NA';
            $pajak = 0;
            $data_submit = DB::table('nm_order')->where('transaction_id', $trans_id)->get();
            $total_amount_sebelum_pajak = DB::table('nm_order')->where('transaction_id', $trans_id)->where('order_pro_id', '<>', '1')->where('order_pro_id', '<>', '2')->sum('order_amt');
            $total_shipping = DB::table('nm_order')->where('transaction_id', $trans_id)->where('order_pro_id', '1')->sum('order_amt');
            $total_diskon = DB::table('nm_order')->where('transaction_id', $trans_id)->where('order_pro_id', '2')->sum('order_amt');
            $total_amount = ($total_amount_sebelum_pajak + ($total_amount_sebelum_pajak * $pajak)) + $total_shipping + $total_diskon;
            $total_amount = round($total_amount);
            $total_amount = number_format($total_amount, 2, '.', '');
            date_default_timezone_set('Asia/Jakarta');
            $date = date('YmdHis');
            $basket = $data_submit[0]->basket;
            $paymentchannel = '04';
            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            $sessionid = $data_submit[0]->session_id;
            $bookingcode = $data_submit[0]->booking_code;
            $words = sha1($total_amount.$mallid.$sharedkey.$trans_id);
            $order_shipping_add = explode(';', $data_submit[0]->order_shipping_add);
            $email = $order_shipping_add[6];
            $name = $order_shipping_add[0];
            $address = $order_shipping_add[1];
            $country = '360';
            $mobilephone = $order_shipping_add[5];
            $live_mode = config('dokularavel.LIVE_MODE');
            if($live_mode == FALSE){
                $url_doku = 'http://staging.doku.com/Suite/Receive';
            }elseif ($live_mode == TRUE) {
                $url_doku = 'https://pay.doku.com/Suite/Receive';
            }else {
                dd('error: Live Mode must be set');
            }
            // $date_queue = \Carbon\Carbon::now()->addMinutes(30);
            //
            // \Queue::later($date, new \App\Commands\UpdateExpired($trans_id));

            // \Queue::later($date, new \App\Commands\SalesOrder());
            //$this->recall_unsync(); di panggil ketika payment sukses
            // return redirect('doku?trans_id='.$trans_id);
            return view('doku')
            ->with('URL', $url_doku)
            ->with('BASKET', $basket)
            ->with('MALLID', $mallid)
            ->with('CHAINMERCHANT', $chainmerchant)
            ->with('CURRENCY', $currency)
            ->with('PURCHASECURRENCY', $purchasecurrency)
            ->with('AMOUNT', $total_amount)
            ->with('PURCHASEAMOUNT', $total_amount)
            ->with('TRANSIDMERCHANT', $trans_id)
            ->with('PAYMENTCHANNEL', $paymentchannel)
            ->with('SHAREDKEY', $sharedkey)
            ->with('WORDS', $words)
            ->with('REQUESTDATETIME', $date)
            ->with('SESSIONID', $sessionid)
            ->with('EMAIL', $email)
            ->with('NAME', $name)
            ->with('ADDRESS', $address)
            ->with('COUNTRY', $country)
            ->with('MOBILEPHONE', $mobilephone);
        }
        elseif ($jenis == 'sprintasia') {
            $transaction_id = $request->input('transaction_id');
            $pajak = 0;
            $total_amount = Home::get_total_amount($transaction_id);
            $get_transaction = DB::table("nm_order")->where('transaction_id', $transaction_id)->first();
            $raw_data_customer = $get_transaction->order_shipping_add;
            $explode_data_customer = explode(';', $raw_data_customer);
            $data_customer = [
                'name' => $explode_data_customer[0],
                'address1' => $explode_data_customer[1],
                'address2' => $explode_data_customer[2],
                'state' => $explode_data_customer[3],
                'zipcode' => $explode_data_customer[4],
                'phone' => $explode_data_customer[5],
                'email' => $explode_data_customer[6]
            ];
            $response = self::sprintasiaInsertPayment($transaction_id, $data_customer);
            if($response['insertStatus'] == "00" AND $response['redirectURL'] != "" ) { // Validate insertStatus and redirectURL

            // -- push WMS --
            // $this->recall_unsync();

            // -- email notifikasi --

            // $send_mail_data = array(
            //     'first_name' => '8989',
            //     'password' => '9999'
            // );
            // Mail::send(
            //     'emails.notifikasi_email_transaksi_checkout',$send_mail_data, function ($message) use ($get_data_cus){
            //         $message->to($get_data_cus->cus_email)->subject('Notifikasi Transaksi');
            //     }
            // );

            // Redirect process
            return view('sprintasiaInsertPayment')
                ->with('klikPayCode', $response['redirectData']['klikPayCode'])
                ->with('transactionNo', $response['redirectData']['transactionNo'])
                ->with('totalAmount', $response['redirectData']['totalAmount'])
                ->with('currency', $response['redirectData']['currency'])
                ->with('payType', $response['redirectData']['payType'])
                ->with('callback', $response['redirectData']['callback'])
                ->with('transactionDate', $response['redirectData']['transactionDate'])
                ->with('descp', $response['redirectData']['descp'])
                ->with('miscFee', $response['redirectData']['miscFee'])
                ->with('signature', $response['redirectData']['signature'])
                ->with('redirectURL', $response['redirectURL']);
                //           exit;
            }

            else{
                // Print error message
                die($response['insertMessage']);
            }
        }
    }

    public function account_verification($email)
    {
        if(!$email){
            Session::flash('payment_cancel', 'Your Verification is Failed');
            return Redirect::to('index');
        }else {
            $verification = Register::verification_status($email);
            Session::flash('success', 'Your Verification is Success, Please Login');
            return Redirect::to('index');
        }

    }

    public function status_garansi(Request $request)
    {
        $invoice_number = $request->input('invoice_number');
        $date = $request->input('date');
        $sku = $request->input('sku');

        if ($invoice_number == null) {
            return response('Invoice Number Is Null', 400);
            die();
        }
        if ($sku == null) {
            return response('SKU Is Null', 400);
            die();
        }

        $pro_id = DB::table('nm_product')->select('pro_id')->where('pro_sku', $sku)->first();
        if ($pro_id == null) {
            return response('SKU Is Not Exist', 400);
        }
        $get_garansi = DB::table('nm_order')->select('order_garansi_expired', 'order_jumlah_warranty')->where('transaction_id', $invoice_number)->where('order_pro_id', $pro_id->pro_id)->first();
        if ($get_garansi == null) {
            return response('Unknown Order', 400);
        }
        if ($date == null) {
            date_default_timezone_set('Asia/Jakarta');
            $return_date = date('Y-m-d H:i:s');
        }else {
            $return_date = date('Y-m-d H:i:s', strtotime($date));
            $return_date = strtotime($return_date);
            $return_date = date('Y-m-d H:i:s', strtotime('+7 hours', $return_date));
        }
        $garansi_expired = strtotime($get_garansi->order_garansi_expired);
        $extended_warranty_expired = date('Y-m-d H:i:s', strtotime('-90 days', $garansi_expired));

        if ($get_garansi->order_garansi_expired == 0) {
            // $data = [
            //     'warranty_price' => $get_garansi->order_jumlah_warranty,
            //     'status' => 'Order Belum Diterima'
            // ];
            $data = 'Order Belum Diterima';
        }elseif ($get_garansi->order_garansi_expired > $return_date && $extended_warranty_expired > $return_date) {
            // $data = [
            //     'warranty_price' => $get_garansi->order_jumlah_warranty,
            //     'status' => 'Masih Berlaku'
            // ];
            $data = 'Garansi Dasar Sedang Berjalan. Harga Garansi Tambahan: '.$get_garansi->order_jumlah_warranty;
        }elseif ($get_garansi->order_garansi_expired > $return_date && $extended_warranty_expired < $return_date) {
            // $data = [
            //     'warranty_price' => $get_garansi->order_jumlah_warranty,
            //     'status' => 'Masih Berlaku'
            // ];
            $data = 'Garansi Tambahan Sedang Berjalan';
        }elseif ($get_garansi->order_garansi_expired <= $return_date) {
            // $data = [
            //     'warranty_price' => $get_garansi->order_jumlah_warranty,
            //     'status' => 'Expired'
            // ];
            $data = 'Garansi Expired';
        }else {
            // $data = [
            //     'warranty_price' => $get_garansi->order_jumlah_warranty,
            //     'status' => 'Unknown'
            // ];
            $data = 'Unknown';
        }

        return $data;
    }

    public function test_sepulsa()
    {
        $customer_number = '0000001430071800';
        $payment_period = '01';
        $product_id = '34';
        $test = Sepulsa::inquire_bpjs_kesehatan($customer_number, $payment_period, $product_id);

        return $test;
    }

    public function sepulsa_checkout_process(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        if (Session::get('customerid') == null) {
            dd('Login First');
        }

        $get_product = DB::table('nm_product')
        ->where('pro_id', $request->input('pro_id'))
        ->first();
        $get_data_cus = Home::get_data_detail_cus(Session::get('customerid'));

        $tagihan = 0;
        $customer_number = $request->input('customer_number');
        $payment_period = $request->input('payment_period');
        $operator_code = $request->input('operator_code');
        $meter_number = $request->input('meter_number');
        $product_id = $get_product->pro_sepulsa_id;
        if ($get_product->pro_sepulsa_type == 'bpjs_kesehatan') {
            $inquire_sepulsa = Sepulsa::inquire_bpjs_kesehatan($customer_number, $payment_period, $product_id);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }else {
                $tagihan = $inquire_sepulsa['premi'];
            }
        }elseif ($get_product->pro_sepulsa_type == 'electricity') {
            $inquire_sepulsa = Sepulsa::inquire_electricity_prepaid($meter_number, $product_id);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }
        }elseif ($get_product->pro_sepulsa_type == 'electricity_postpaid') {
            $inquire_sepulsa = Sepulsa::inquire_electricity_postpaid($meter_number, $product_id);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }else {
                $tagihan = $inquire_sepulsa['amount'] - $inquire_sepulsa['admin_charge'];
            }
        }elseif ($get_product->pro_sepulsa_type == 'telkom_postpaid') {
            $inquire_sepulsa = Sepulsa::inquire_telkom($customer_number, $product_id);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }else {
                $tagihan = $inquire_sepulsa['jumlah_tagihan'];
            }
        }elseif ($get_product->pro_sepulsa_type == 'pdam') {
            $inquire_sepulsa = Sepulsa::inquire_pdam($customer_number, $product_id, $operator_code);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }else {
                $tagihan = $inquire_sepulsa['amount'];
            }
        }elseif ($get_product->pro_sepulsa_type == 'multi') {
            $inquire_sepulsa = Sepulsa::inquire_multifinance($customer_number, $product_id);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }else {
                $tagihan = $inquire_sepulsa['jumlah_tagihan'];
            }
        }elseif ($get_product->pro_sepulsa_type == 'mobile_postpaid') {
            $inquire_sepulsa = Sepulsa::inquire_mobile_postpaid($customer_number, $product_id);
            if ($inquire_sepulsa['status'] == false) {
                return Redirect::to('index')->with('payment_cancel', $inquire_sepulsa['message']);
            }else {
                $tagihan = $inquire_sepulsa['bill_amount'];
            }
        }

        if ($get_product->pro_disprice > 0) {
            $price = $get_product->pro_disprice + $tagihan;
        }else {
            $price = $get_product->pro_price + $tagihan;
        }

        if ($request->input('payment_type') == 2) {
            $metode_pembayaran = 'Doku';
        }elseif ($request->input('payment_type') == 6) {
            $metode_pembayaran = 'Sprintasia';
        }


        $random_key = self::random_key();

        $data_order = [
            'order_cus_id' => Session::get('customerid'),
            'order_pro_id' => $request->input('pro_id'),
            'order_type' => 3,
            'transaction_id' => 'unpay'.$random_key,
            'order_qty' => 1,
            'order_amt' => $price,
            'order_date' => $now,
            'order_status' => 3,
            'order_paytype' => $request->input('payment_type'),
            'payment_channel' => $request->input('payment_channel'),
            'order_sepulsa_customer_number' => $request->input('customer_number'),
            'order_sepulsa_operator_code' => $request->input('operator_code'),
            'order_sepulsa_payment_period' => $request->input('payment_period'),
            'order_sepulsa_meter_number' => $request->input('meter_number')
        ];
        $data_transaksi = [
            'transaction_id' => 'unpay'.$random_key,
            'total_belanja' => $price,
            'diskon' => 0,
            'diskon_voucher' => 0,
            'shipping' => 0,
            'metode_pembayaran' => $metode_pembayaran,
            'status_pembayaran' => 'belum dibayar',
            'pajak' => 0,
            'order_status' => 3,
            'status_outbound_transaksi' => 1,
            'kupon_id' => 0,
            'total_poin' => $price/1000,
            'poin_digunakan' => 0
        ];

        $insert_order = DB::table('nm_order')->insert($data_order);
        $insert_transaksi = DB::table('nm_transaksi')->insert($data_transaksi);

        $date = \Carbon\Carbon::now()->addMinutes(config('expire.time'));
        \Queue::later($date, new \App\Commands\UpdateExpired('unpay'.$random_key));

        //Payment
        //Doku Payment
        if ($request->input('payment_type') == 2) {
            $trans_id = 'unpay'.$random_key;
            $mallid = config('dokularavel.MALL_ID');
            $currency = config('dokularavel.CURRENCY');
            $purchasecurrency = config('dokularavel.CURRENCY');
            $sharedkey = config('dokularavel.SHARED_KEY');
            $live_mode = config('dokularavel.LIVE_MODE');
            $chainmerchant = 'NA';
            $pajak = 0;
            $data_submit = Home::get_data_order($trans_id);
            $data_amount = Home::get_total_amount($trans_id);
            $total_amount = round($data_amount['total_amount']);
            $total_amount = number_format($total_amount, 2, '.', '');
            date_default_timezone_set('Asia/Jakarta');
            $date = date('YmdHis');
            $basket = "";
            foreach($data_submit as $data_sub){
                $produk = DB::table('nm_product')->where('pro_id', $data_sub->order_pro_id)->first();
                if ($data_sub->order_pro_id == 1 || $data_sub->order_pro_id == 2) {
                    $basket .= $produk->pro_title.','.$data_sub->order_amt.','.$data_sub->order_qty.','.$data_sub->order_amt.';';
                }else {
                    $basket .= $produk->pro_title.','.$produk->pro_price.','.$data_sub->order_qty.','.$data_sub->order_amt.';';
                }
            }
            $paymentchannel = Input::get('payment_channel');
            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            $sessionid = '';
            $bookingcode = '';
            for ($j=0; $j < 20; $j++) {
                $rnum = floor(lcg_value() * strlen($chars));
                $sessionid .= substr($chars, $rnum, 1);
            }
            for ($k=0; $k < 6; $k++) {
                $rnum = floor(lcg_value() * strlen($chars));
                $bookingcode .= substr($chars, $rnum, 1);
            }
            $words = sha1($total_amount.$mallid.$sharedkey.$trans_id);
            $email = Input::get('email');
            $name = Input::get('fname');
            $address = Input::get('addr_line');
            $country = '360';
            $mobilephone = Input::get('phone1_line');
            if($live_mode == FALSE){
                $url_doku = 'http://staging.doku.com/Suite/Receive';
            }elseif ($live_mode == TRUE) {
                $url_doku = 'https://pay.doku.com/Suite/Receive';
            }else {
                dd('error: Live Mode must be set');
            }

            $date_queue = \Carbon\Carbon::now()->addMinutes(config('expire.time'));
            \Queue::later($date_queue, new \App\Commands\UpdateExpired('unpay'.$random_key));

            $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($trans_id);
            $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($trans_id);

            if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                $payment_channel = null;
            }else {
                $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
            }
            $metode_pembayaran_raw = DB::table('nm_payment_method_header')
            ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
            ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
            ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
            ->first();
            if ($metode_pembayaran_raw == null) {
                $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                ->first();
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
            }else {
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
            }

            $send_mail_data = array(
                   'id_transaksi' => $trans_id,
                   'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                   'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                   'metode_pay' => $metode_pembayaran,
                   'items_order' => $get_data_order_by_trans_id
            );
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
               'emails.notifikasi_email_transaksi_checkout',
               $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                   $get_cus_data_by_trans_id){
                   $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
               }
            );

            return view('doku')
            ->with('URL', $url_doku)
            ->with('BASKET', $basket)
            ->with('MALLID', $mallid)
            ->with('CHAINMERCHANT', $chainmerchant)
            ->with('CURRENCY', $currency)
            ->with('PURCHASECURRENCY', $purchasecurrency)
            ->with('AMOUNT', $data_amount['total_amount'])
            ->with('PURCHASEAMOUNT', $data_amount['total_amount'])
            ->with('TRANSIDMERCHANT', $trans_id)
            ->with('PAYMENTCHANNEL', $paymentchannel)
            ->with('SHAREDKEY', $sharedkey)
            ->with('WORDS', $words)
            ->with('REQUESTDATETIME', $date)
            ->with('SESSIONID', $sessionid)
            ->with('EMAIL', $email)
            ->with('NAME', $name)
            ->with('ADDRESS', $address)
            ->with('COUNTRY', $country)
            ->with('MOBILEPHONE', $mobilephone);
        }
        //Sprintasia Payment
        elseif ($request->input('payment_type') == 6) {
            $get_data_customer = DB::table('nm_customer')->where('cus_id', Session::get('customerid'))->first();
            $data_customer = array(
                'name' => $get_data_customer->cus_name,
                'address1' => $get_data_customer->cus_address1,
                'address2' => $get_data_customer->cus_address2,
                'phone' => $get_data_customer->cus_phone,
                'email' => $get_data_customer->cus_email
            );

            $response = self::sprintasiaInsertPayment('unpay'.$random_key, $data_customer);
            if (array_key_exists('redirectURL', $response) && array_key_exists('insertStatus', $response)) {
                if($response['insertStatus'] == "00" AND $response['redirectURL'] != "" ) {
                    $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id('unpay'.$random_key);
                    $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id('unpay'.$random_key);

                    if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                        $payment_channel = null;
                    }else {
                        $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                    }
                    $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                    ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                    ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                    ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                    ->first();
                    if ($metode_pembayaran_raw == null) {
                        $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                        ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                        ->first();
                        $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                    }else {
                        $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                    }

                    //    echo "selesai query";

                    $send_mail_data = array(
                           'id_transaksi' => 'unpay'.$random_key,
                           'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                           'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                           'metode_pay' => $metode_pembayaran,
                           'items_order' => $get_data_order_by_trans_id
                    );
                    $admin = DB::table('nm_emailsetting')->first();
                    $admin_email = $admin->es_noreplyemail;
                    Config::set('mail.from.address', $admin_email);
                    Mail::send(
                       'emails.notifikasi_email_transaksi_checkout',
                       $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                           $get_cus_data_by_trans_id){
                           $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
                       }
                    );

                    // Redirect process
                    return view('sprintasiaInsertPayment')
                        ->with('klikPayCode', $response['redirectData']['klikPayCode'])
                        ->with('transactionNo', $response['redirectData']['transactionNo'])
                        ->with('totalAmount', $response['redirectData']['totalAmount'])
                        ->with('currency', $response['redirectData']['currency'])
                        ->with('payType', $response['redirectData']['payType'])
                        ->with('callback', $response['redirectData']['callback'])
                        ->with('transactionDate', $response['redirectData']['transactionDate'])
                        ->with('descp', $response['redirectData']['descp'])
                        ->with('miscFee', $response['redirectData']['miscFee'])
                        ->with('signature', $response['redirectData']['signature'])
                        ->with('redirectURL', $response['redirectURL']);
                }else{
                    // Print error message
                    die($response['insertMessage']);
                }
            }elseif (array_key_exists('insertStatus', $response)) {
                if ($response['insertStatus'] == "00") { //For BCA VA And Permata VA
                    $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id('unpay'.$random_key);
                    $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id('unpay'.$random_key);

                    if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
                        $payment_channel = null;
                    }else {
                        $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
                    }
                    $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                    ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                    ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                    ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                    ->first();
                    if ($metode_pembayaran_raw == null) {
                        $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                        ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
                        ->first();
                        $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
                    }else {
                        $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
                    }

                    $send_mail_data = array(
                            'id_transaksi' => 'unpay'.$random_key,
                            'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
                            'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
                            'metode_pay' => $metode_pembayaran,
                            'items_order' => $get_data_order_by_trans_id
                    );
                    $admin = DB::table('nm_emailsetting')->first();
                    $admin_email = $admin->es_noreplyemail;
                    Config::set('mail.from.address', $admin_email);
                    Mail::send(
                        'emails.notifikasi_email_transaksi_checkout',
                        $send_mail_data, function ($message) use ($get_data_cus,$get_data_order_by_trans_id,
                            $get_cus_data_by_trans_id){
                            $message->to($get_data_cus->cus_email)->subject('Notifikasi Order');
                        }
                    );

                    unset($_SESSION['cart']);
                    unset($_SESSION['deal_cart']);

                    Session::flash('payment_success', 'Your Payment Has Been Completed Successfully');
                    return Redirect::to('info_pembayaran' . '/' . 'unpay'.$random_key)->with('result', 'Order Anda Telah Disimpan. Silahkan Selesaikan Pembayaran Anda');
                }else{
                    // Print error message
                    die($response['insertMessage']);
                }
            }else{
                dd('Something Went Wrong');
            }
        }
    }

    public function inquire_sepulsa(Request $request)
    {
        $get_product = DB::table('nm_product')
        ->where('pro_id', $request->input('input3'))
        ->select('pro_sepulsa_id', 'pro_sepulsa_type', 'pro_price')
        ->first();

        $customer_number = $request->input('input1');
        $payment_period = $request->input('input2');
        $product_id = $get_product->pro_sepulsa_id;
        $operator_code = $request->input('input2');
        $tagihan = 0;
        if ($get_product != null) {
            if ($get_product->pro_sepulsa_type == 'bpjs_kesehatan') {
                $inquire_sepulsa = Sepulsa::inquire_bpjs_kesehatan($customer_number, $payment_period, $product_id);
                if ($inquire_sepulsa['status'] == true) {
                    $tagihan = $inquire_sepulsa['premi'];
                }
            }elseif ($get_product->pro_sepulsa_type == 'electricity') {
                $inquire_sepulsa = Sepulsa::inquire_electricity_prepaid($customer_number, $product_id);
            }elseif ($get_product->pro_sepulsa_type == 'electricity_postpaid') {
                $inquire_sepulsa = Sepulsa::inquire_electricity_postpaid($customer_number, $product_id);
                if ($inquire_sepulsa['status'] == true) {
                    $tagihan = $inquire_sepulsa['amount'] - $inquire_sepulsa['admin_charge'];
                }
            }elseif ($get_product->pro_sepulsa_type == 'telkom_postpaid') {
                $inquire_sepulsa = Sepulsa::inquire_telkom($customer_number, $product_id);
                if ($inquire_sepulsa['status'] == true) {
                    $tagihan = $inquire_sepulsa['jumlah_tagihan'];
                }
            }elseif ($get_product->pro_sepulsa_type == 'pdam') {
                $inquire_sepulsa = Sepulsa::inquire_pdam($customer_number, $product_id, $operator_code);
                if ($inquire_sepulsa['status'] == true) {
                    $tagihan = $inquire_sepulsa['amount'];
                }
            }elseif ($get_product->pro_sepulsa_type == 'multi') {
                $inquire_sepulsa = Sepulsa::inquire_multifinance($customer_number, $product_id);
                if ($inquire_sepulsa['status'] == true) {
                    $tagihan = $inquire_sepulsa['jumlah_tagihan'];
                }
            }elseif ($get_product->pro_sepulsa_type == 'mobile_postpaid') {
                $inquire_sepulsa = Sepulsa::inquire_mobile_postpaid($customer_number, $product_id);
                if ($inquire_sepulsa['status'] == true) {
                    $tagihan = $inquire_sepulsa['bill_amount'];
                }
            }

            $data = [
                'data_inquire' => $inquire_sepulsa,
                'data_product' => $get_product,
                'tagihan' => $tagihan
            ];
            return $data;
        }
    }

    public function auto_get_pdam_operators(Request $request)
    {
        $product_id = $request->input('product_id');
        $get_pdam_operators = Sepulsa::get_pdam_operators($product_id);

        $return_operators = [];
        if ($get_pdam_operators) {
            foreach ($get_pdam_operators['OperatorLists'] as $operator) {
                $array = [
                    'value' => $operator['code'],
                    'label' => $operator['description']
                ];
                array_push($return_operators, $array);
            }
        }

        return $return_operators;
    }
    public function run_auto_update_sepulsa()
    {
        if (Session::get('username')) {
            $date = \Carbon\Carbon::now()->addMinutes(360);
            \Queue::later($date, new \App\Commands\UpdateProductStatusSepulsa());
            return 'OK';
        }else {
            return response('Unauthorized', 401);
        }
    }
    public function callback_sepulsa(Request $request)
    {
        $response = $request->getContent();
        $data = json_decode($response, TRUE);
        $order_id = $data['order_id'];
        $data_update = [
            'order_sepulsa_response_code' => $data['response_code']
        ];
        $update = DB::table('nm_order')->where('transaction_id', $order_id)->update($data_update);

        if ($update) {
            return 'success';
        }else {
            return 'failed';
        }
    }

    public function sum_rating($customer_id)
    {
        $ratings = DB::table('nm_review')
        ->where('customer_id', $customer_id)
        ->groupBy('product_id')
        ->selectRaw('POWER(AVG(ratings), 2) as ratings')
        ->get();

        $value = 0;
        foreach ($ratings as $rating) {
            $value = $value + $rating->ratings;
        }

        return $value;
    }

    public function review_matrix()
    {
        $truncate_review_matrix = DB::table("review_matrix")->truncate();
        $customers = DB::table("nm_customer")->get();

        foreach ($customers as $customer) {
            $reviews = DB::table("nm_review")
            ->where('customer_id', $customer->cus_id)
            ->groupBy('product_id')
            ->selectRaw('AVG(ratings) as ratings, customer_id, product_id')
            ->get();
            $sum_ratings = self::sum_rating($customer->cus_id);

            if ($reviews != null) {
                $root_sum_ratings = sqrt($sum_ratings);
                foreach ($reviews as $review) {
                    if ($review->customer_id == null || $review->product_id == null) {
                        echo 'customer_id:'.$review->customer_id.' product_id:'.$review->product_id.'<br>';
                        break;
                    }
                    $review_value = $review->ratings / $root_sum_ratings;
                    $insert = DB::table('review_matrix')->insert([
                        'customer_id' => $review->customer_id,
                        'product_id' => $review->product_id,
                        'value' => $review_value
                    ]);
                    echo "customer_id:".$review->customer_id.' product_id:'.$review->product_id.' value:'.$review_value.'<br>';
                }
            }else {
                echo "customer_id:".$customer->cus_id.' reviews:null<br>';
            }

        }

        return "Done";

    }

    public function neighbor()
    {
        $truncate_neighbor = DB::table('neighbor')->truncate();
        $customers = DB::table('review_matrix')
        ->groupBy('customer_id')
        ->get();

        foreach ($customers as $customer) {
            $other_customers = DB::table('review_matrix')
            ->where('customer_id', '!=', $customer->customer_id)
            ->groupBy('customer_id')
            ->get();

            foreach ($other_customers as $other_customer) {
                $customer_x = DB::table('review_matrix')
                ->where('customer_id', $customer->customer_id)
                ->get();

                $value = 0;
                foreach ($customer_x as $cus_x) {
                    $customer_y = DB::table('review_matrix')
                    ->where('customer_id', $other_customer->customer_id)
                    ->where('product_id', $cus_x->product_id)
                    ->first();

                    if ($customer_y == null) {
                        $value = $value + 0;
                    }else {
                        $value = $value + ($cus_x->value * $customer_y->value);
                    }
                }

                $insert = DB::table('neighbor')
                ->insert([
                    'customer_id' => $customer->customer_id,
                    'to_customer_id' => $other_customer->customer_id,
                    'value' => $value
                ]);
                echo 'customer_id:'.$customer->customer_id.' to_customer_id:'.$other_customer->customer_id.' value:'.$value.'<br>';

            }
        }

        return "Done";
    }

}
