<?php
namespace App\Http\Controllers;
use App\Province;
use Illuminate\Http\Request;
use DB;

class ProvinceController extends Controller
{
    public function apiPutProvince(Request $request)
    {
        $prov_code = $request->input('prov_code');
        if(!$prov_code)
        {
            return response('Masukkan Kode Provinsi', 400);
            die();
        }

        //find country
        if($request->has('prov_country_id')){
            $country = DB::table('nm_country')->find($request->input('prov_co_id'));
        }else{
            $country = DB::table('nm_country')->where('co_code', $request->input('prov_co_code'))->first();
        }

        if(!$country)
        {
            return response('Country Tidak Ditemukan', 400);
            die();
        }

        $province = Province::where('prov_code', $prov_code)->first();
        if(!$province){
            $province = new Province();
            $province->prov_code = $prov_code;
        }

        //update nama
        $prov_name = $request->input('prov_name', '');
        $province->prov_name = $prov_name;

        //update status
        $prov_status = $request->input('prov_status');
        $province->prov_status = $prov_status;

        $province->prov_co_id = $country->co_id;

        $province->save();

        return response('Sukses', 200);
    }

    public function auto_provinsi(Request $request)
    {
        $get_province = Province::where('prov_name', 'like', '%'.$request->input('term').'%');
        if ($request->input('country') != '' && $request->input('country') != null) {
            $get_province = $get_province->where('prov_co_id', $request->input('country'));
        }
        $get_province = $get_province->get();

        $return_provinsi = [];

        foreach ($get_province as $province) {
            $array = [
                'value' => $province->prov_name,
                'label' => $province->prov_name,
                'id' => $province->prov_id
            ];
            array_push($return_provinsi, $array);
        }

        return $return_provinsi;
    }
}
