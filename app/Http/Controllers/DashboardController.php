<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Merchantproducts;
use App\MerchantTransactions;
use App\Merchantdeals;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function view_include()
	{
		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}

            $blogcmtcount = Blog::get_msgcount();
            Session::put('blogcmtcount', $blogcmtcount);
            $newsubscriberscount = Dashboard::get_subscriberscount();
            Session::put('subscriberscount', $newsubscriberscount);
            $inquiriescnt = Dashboard::get_inquiriescnt();
            Session::put('inquiriescnt', $inquiriescnt);
            $adrequestcnt = Admodel::get_msgcount();
            Session::put('adrequestcnt', $adrequestcnt);

            $adminheader      = view('siteadmin.includes.admin_header')
            ->with("routemenu", "dashboard")
            ->with('privileges', $privileges)
            ->with("blogcmtcount", "blogcmtcount")
            ->with("newsubscriberscount", "newsubscriberscount")
            ->with("inquiriescnt", "inquiriescnt")
            ->with("adrequestcnt", "adrequestcnt");
            $adminfooter      = view('siteadmin.includes.admin_footer');
            $return = [
                'adminheader' => $adminheader,
                'adminfooter' => $adminfooter
            ];
            return $return;
        } else {
            return Redirect::to('siteadmin');
        }
	}

    public function siteadmin_dashboard()
    {
        if (Session::has('userid')) {
            $get_merchant_id = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
            $kuku_merchant_id = $get_merchant_id->mer_id;
            $include = self::view_include();
            $adminheader = $include['adminheader'];
            $adminfooter = $include['adminfooter'];
            $activeproductscnt   = Dashboard::get_active_products();
            $approval_product    = Dashboard::get_approval_products($kuku_merchant_id);
            $wms_products        = Dashboard::get_wms_products($kuku_merchant_id);
            $merchant_outbound   = Dashboard::get_merchant_outbound();
            $transfer_bank       = Dashboard::get_transfer_bank();
            $soldproductscnt     = Dashboard::get_sold_products();
            $merchantscnt        = Dashboard::get_merchants();
            $customers           = Dashboard::get_customers();
            $columbia            = Dashboard::get_columbia();
            $activedealscnt      = Dashboard::get_active_deals();
            $archievddealcnt     = Deals::get_archievd_deals();
            $auctioncnt          = Dashboard::get_active_auction();
            $archievdauction_cnt = Auction::get_archievd_auction();
            $actionwinnerscnt    = Dashboard::get_auction_winners();
            $subscriberscnt      = Dashboard::get_subscribers();
            $storescnt           = Dashboard::get_stores();
            $fundrequest         = Dashboard::get_fundrequest();
            /*for chart*/
            $customercnt        = Dashboard::get_customer_list();
            $cus_count          = Dashboard::get_chart_details();
            $get_chart6_details = Dashboard::get_chart6_details();
            $transaction_chart  = Dashboard::get_charttransaction_details();
            $get_banner_history = Dashboard::get_banner_history();
            //dd($get_banner_history);

            $get_status_banner_all = Dashboard::get_status_banner_all();
            //dd($get_status_banner_all);

            $get_all_banner     = Dashboard::get_all_dashboard();
            // dd($transfer_bank);
            return view('siteadmin.admin_dashboard')
            ->with('columbia',$columbia)
            ->with('fundrequest', $fundrequest)
            ->with('transfer_bank', $transfer_bank)
            ->with('merchant_outbound', $merchant_outbound)
            ->with('wms_products', $wms_products)
            ->with('approval_product', $approval_product)
            ->with('adminheader', $adminheader)
            ->with('adminfooter', $adminfooter)
            ->with('activeproductscnt', $activeproductscnt)
            ->with('soldproductscnt', $soldproductscnt)
            ->with('merchantscnt', $merchantscnt)
            ->with('customers', $customers)
            ->with('activedealscnt', $activedealscnt)
            ->with('archievddealcnt', $archievddealcnt)
            ->with('auctioncnt', $auctioncnt)
            ->with('archievdauction_cnt', $archievdauction_cnt)
            ->with('actionwinnerscnt', $actionwinnerscnt)
            ->with('subscriberscnt', $subscriberscnt)
            ->with('storescnt', $storescnt)
            ->with('customercnt', $customercnt)
            ->with('cus_count', $cus_count)
            ->with('get_chart6_details', $get_chart6_details)
            ->with('transaction_chart', $transaction_chart)
            ->with('get_banner_history', $get_banner_history)
            ->with('get_status_banner_all', $get_status_banner_all)
            ->with('get_all_banner', $get_all_banner);
        }

        else {
            return Redirect::to('siteadmin');
        }

    }

    public function detail_transaksi_admin($id_transaksi)
    {
        if (Session::has('userid')) {

            $transaction_header           = Home::get_transaction_header($id_transaksi);
            $transaction_detail           = Home::get_transaction_detail2($id_transaksi);
            $get_history_transaksi        = Home::get_history_transaksi($id_transaksi);

            $get_merchant_id = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
            $kuku_merchant_id = $get_merchant_id->mer_id;

            $include = self::view_include();
            $adminheader = $include['adminheader'];
            $adminfooter = $include['adminfooter'];

            $activeproductscnt   = Dashboard::get_active_products();
            $approval_product    = Dashboard::get_approval_products($kuku_merchant_id);
            $wms_products        = Dashboard::get_wms_products($kuku_merchant_id);
            $merchant_outbound   = Dashboard::get_merchant_outbound();
            $transfer_bank       = Dashboard::get_transfer_bank();
            $soldproductscnt     = Dashboard::get_sold_products();
            $merchantscnt        = Dashboard::get_merchants();
            $customers           = Dashboard::get_customers();
            $activedealscnt      = Dashboard::get_active_deals();
            $archievddealcnt     = Deals::get_archievd_deals();
            $auctioncnt          = Dashboard::get_active_auction();
            $archievdauction_cnt = Auction::get_archievd_auction();
            $actionwinnerscnt    = Dashboard::get_auction_winners();
            $subscriberscnt      = Dashboard::get_subscribers();
            $storescnt           = Dashboard::get_stores();

            /*for chart*/
            $customercnt        = Dashboard::get_customer_list();
            $cus_count          = Dashboard::get_chart_details();
            $get_chart6_details = Dashboard::get_chart6_details();
            $transaction_chart  = Dashboard::get_charttransaction_details();
            $get_banner_history = Dashboard::get_banner_history();
            //dd($get_banner_history);

            $get_status_banner_all = Dashboard::get_status_banner_all();
            //dd($get_status_banner_all);

            $get_all_banner     = Dashboard::get_all_dashboard();
            // dd($transfer_bank);
            if ($transaction_detail[0]->payment_channel == '') {
                $payment_channel = null;
            }else {
                $payment_channel = $transaction_detail[0]->payment_channel;
            }
            $metode_pembayaran_raw = DB::table('nm_payment_method_header')
            ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
            ->where('nm_paymnet_method_detail.kode_pay_detail', $transaction_detail[0]->order_paytype)
            ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
            ->first();
            if ($metode_pembayaran_raw == null) {
                $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                ->where('kode_pay_detail', $transaction_detail[0]->order_paytype)
                ->first();
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
            }else {
                $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
            }

            return view('siteadmin.detail_transaksi_admin')
            ->with('transfer_bank', $transfer_bank)
            ->with('merchant_outbound', $merchant_outbound)
            ->with('wms_products', $wms_products)
            ->with('approval_product', $approval_product)
            ->with('adminheader', $adminheader)
            ->with('adminfooter', $adminfooter)
            ->with('activeproductscnt', $activeproductscnt)
            ->with('soldproductscnt', $soldproductscnt)
            ->with('merchantscnt', $merchantscnt)
            ->with('customers', $customers)
            ->with('activedealscnt', $activedealscnt)
            ->with('archievddealcnt', $archievddealcnt)
            ->with('auctioncnt', $auctioncnt)
            ->with('archievdauction_cnt', $archievdauction_cnt)
            ->with('actionwinnerscnt', $actionwinnerscnt)
            ->with('subscriberscnt', $subscriberscnt)
            ->with('storescnt', $storescnt)
            ->with('customercnt', $customercnt)
            ->with('cus_count', $cus_count)
            ->with('get_chart6_details', $get_chart6_details)
            ->with('transaction_chart', $transaction_chart)
            ->with('get_banner_history', $get_banner_history)
            ->with('get_status_banner_all', $get_status_banner_all)
            ->with('get_all_banner', $get_all_banner)
            ->with('transaction_header', $transaction_header)
            ->with('transaction_detail', $transaction_detail)
            ->with('get_history_transaksi', $get_history_transaksi)
            ->with('metode_pembayaran', $metode_pembayaran);
        }

        else {
            return Redirect::to('siteadmin');
        }

    }

    public function sitemerchant_dashboard()
    {
        if (Session::has('merchantid')) {
            $date                = date('Y-m-d H:i:s');
            $mer_id              = Session::get('merchantid');
            $activeproductscnt   = Dashboard::get_mer_active_products($mer_id);
            $soldproductscnt     = Dashboard::get_mer_sold_products($mer_id);
            $merchantscnt        = Dashboard::get_merchants();
            $customers           = Dashboard::get_customers();
            $activedealscnt      = Dashboard::get_mer_active_deals($mer_id, $date);
            $archievddealcnt     = Dashboard::get_mer_archievd_deals($mer_id);
            $auctioncnt          = Dashboard::get_mer_active_auction($mer_id);
            $archievdauction_cnt = Dashboard::get_mer_archievd_auction($mer_id);
            $actionwinnerscnt    = Dashboard::get_mer_auction_winners($mer_id);
            $subscriberscnt      = Dashboard::get_subscribers();
            $storescnt           = Dashboard::get_mer_stores($mer_id);
            $fundreqpaid         = Dashboard::get_fundreq_paid($mer_id);

            $merid            = Session::get('merchantid');
            $getproductidlist = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
            } else {
                $productlist = '';
            }
            $getauctionidlistrs = Merchant::getauctionidlist($merid);
            if ($getauctionidlistrs) {
                $getauctionidlist    = $getauctionidlistrs[0]->proid;
                $auctionchartdetails = MerchantTransactions::get_chart_auction_details($getauctionidlist);
            } else {
                $auctionchartdetails = '';
            }
            if ($getproductidlist) {
                $productchartdetails = MerchantTransactions::get_chart_product_details($productlist);
                $dealchartdetails    = MerchantTransactions::get_chart_deals_details($productlist);
            } else {
                $productchartdetails = '';
                $dealchartdetails    = '';
            }


            /*for chart*/
            $customercnt        = Dashboard::get_customer_list();
            $cus_count          = Dashboard::get_chart_details();
            $get_chart6_details = Dashboard::get_chart6_details();
            $transaction_chart  = Dashboard::get_charttransaction_details();
            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter     = view('sitemerchant.includes.merchant_footer');
            $active_deals_count = Dashboard::get_active_deals();
            return view('sitemerchant.merchant_dashboard')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('activeproductscnt', $activeproductscnt)->with('soldproductscnt', $soldproductscnt)->with('merchantscnt', $merchantscnt)->with('customers', $customers)->with('activedealscnt', $activedealscnt)->with('archievddealcnt', $archievddealcnt)->with('auctioncnt', $auctioncnt)->with('archievdauction_cnt', $archievdauction_cnt)->with('actionwinnerscnt', $actionwinnerscnt)->with('subscriberscnt', $subscriberscnt)->with('storescnt', $storescnt)->with('customercnt', $customercnt)->with('cus_count', $cus_count)->with('get_chart6_details', $get_chart6_details)->with('transaction_chart', $transaction_chart)->with('productchartdetails', $productchartdetails)->with('dealchartdetails', $dealchartdetails)->with('auctionchartdetails', $auctionchartdetails)->with('fundreqpaid',$fundreqpaid);
            ;
        } else {
            return Redirect::to('sitemerchant');
        }
    }


}
