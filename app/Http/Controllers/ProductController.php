<?php
namespace App\Http\Controllers;
use DB;
use Session;
use File;
use Excel;
use URL;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Products;
use App\Specification;
use App\UsersRoles;
use App\Users;
use App\RolesPrivileges;
use App\AuditTrail;
use App\Sepulsa;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class ProductController extends Controller
{


    /*

    |--------------------------------------------------------------------------

    | Default Home Controller

    |--------------------------------------------------------------------------

    |

    | You may wish to use controllers instead of, or in addition to, Closure

    | based routes. That's great! Here is an example controller method to

    | get you started. To route to this controller, just add the route:

    |

    |	Route::get('/', 'HomeController@showWelcome');

    |

    */
    public function view_include($routemenu, $left_menu)
 	{
 		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $routemenu)->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.'.$left_menu)->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter,
                 'privileges' => $privileges
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}

    public function tambahkan_model($id_pro, $id_header)
    {
        $data = array(
            'id_grouping_product_model_header' => $id_header,
            'id_product' => $id_pro
            );

        Products::tambahkan_model($data);

        return Redirect::to('add_product_model/'.$id_header);
    }

    public function keluarkan_model($id_detail, $id_header)
    {
        Products::keluarkan_model($id_detail);

        return Redirect::to('add_product_model/'.$id_header);
    }

    public function product_dashboard()
    {

        if (Session::has('userid')) {
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $sold_details = Products::get_sold_products();
            $sold_count = Products::get_sold_products_count();
            $active_cnt = Products::get_active_products();
            $sold_count_merchant = Products::get_sold_products_merchant_count();
            $active_cnt_merchant = Products::get_active_products_merchant();
            $blocked_cnt = Products::get_block_products();
            $producttdy = Products::get_today_product();
            $product7days = Products::get_7days_product();
            $product30days = Products::get_30days_product();
            $product12mnth = Products::get_12mnth_product();
            $producttdy_merchant = Products::get_today_product_merchant();
            $product7days_merchant = Products::get_7days_product_merchant();
            $product30days_merchant = Products::get_30days_product_merchant();
            $product12mnth_merchant = Products::get_12mnth_product_merchant();
            $ordermnth_count = Products::get_chart_details();
            $get_pay        = Settings::get_pay_settings();
            $get_cur        = $get_pay[0]->ps_cursymbol;
            // dd($sold_count_merchant);
            return view('siteadmin.product_dashboard')
            ->with('get_cur', $get_cur)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('sold_details', $sold_details)
            ->with('sold_count', $sold_count)
            ->with('active_count', $active_cnt)
            ->with('sold_count_merchant', $sold_count_merchant)
            ->with('active_count_merchant', $active_cnt_merchant)
            ->with('blocked_cnt', $blocked_cnt)
            ->with('producttdy', $producttdy)
            ->with('product7days', $product7days)
            ->with('product30days', $product30days)
            ->with('product12mnth', $product12mnth)
            ->with('producttdy_merchant', $producttdy_merchant)
            ->with('product7days_merchant', $product7days_merchant)
            ->with('product30days_merchant', $product30days_merchant)
            ->with('product12mnth_merchant', $product12mnth_merchant)
            ->with('ordermnth_count', $ordermnth_count);
        }
        else {
            return Redirect::to('siteadmin');
        }


    }

    public function get_product(Request $request)
    {
        $get_merchant_id = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
        $kuku_merchant_id = $get_merchant_id->mer_id;
        if($request->input('store')=='kukuruyuk'){
            $get_product = DB::table('nm_product')->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')->where('pro_mr_id', $kuku_merchant_id)->get();
        }elseif ($request->input('store')=='merchant') {
            $get_product = DB::table('nm_product')->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')->where('pro_mr_id', '<>', $kuku_merchant_id)->where('pro_isapproved', 1)->get();
        }else {
            $get_product = DB::table('nm_product')->get();
        }

        return $get_product;
    }
    public function add_product()
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $productcategory = Products::get_product_category();
            $productcolor = Products::get_product_color();
            $productsize = Products::get_product_size();
            $productspecification = Products::get_product_specification();
            $merchantdetails = Products::get_merchant_details();
            $productgrade = Products::get_grade();
            $get_setting_extended   = Settings::get_setting_extended_db();

            return view('siteadmin.add_product')
            ->with('productgrade', $productgrade)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('productcategory', $productcategory)
            ->with('productcolor', $productcolor)
            ->with('productsize', $productsize)
            ->with('productspecification', $productspecification)
            ->with('merchantdetails', $merchantdetails)
            ->with('get_setting_extended', $get_setting_extended);
        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    public function edit_product($id)
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $category = Products::get_product_category();
            $product_list = Products::get_product($id);
            $pro_smc_id = $product_list[0]->pro_smc_id;
            $productcolor = Products::get_product_color();
            $merchantdetails = Products::get_merchant_details();
            $productsize = Products::get_product_size();

            $productspecification = DB::table('nm_product')

                ->Leftjoin('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
                ->Leftjoin('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
                // ->leftJoin('nm_spec_value','nm_spec_value.nm_specification_id','=','nm_specification.sp_id')


                ->where('pro_id', $id)
                ->get();

            $values = [];
            foreach ($productspecification as $prospec) {
                if ($prospec->sp_type) {
                    $raw_values = Products::get_product_value($prospec->sp_id);
                    array_push($values, $raw_values);
                }
            }
            $existingspecification = Products::get_product_exist_specification($id);
            $existingcolor = Products::get_product_exist_color($id);
            $existingsize = Products::get_product_exist_size($id);
            $productgrade = Products::get_grade();
            $get_setting_extended   = Settings::get_setting_extended_db();

            return view('siteadmin.edit_product')
                ->with('privileges', $include['privileges'])
                ->with('productgrade', $productgrade)
                ->with('values',$values)
                ->with('adminheader', $adminheader)
                ->with('adminleftmenus', $adminleftmenus)
                ->with('adminfooter', $adminfooter)
                ->with('productcolor', $productcolor)
                ->with('category', $category)
                ->with('product_list', $product_list)
                ->with('merchantdetails', $merchantdetails)
                ->with('productspecification', $productspecification)
                ->with('productsize', $productsize)
                ->with('existingspecification', $existingspecification)
                ->with('existingcolor', $existingcolor)
                ->with('existingsize', $existingsize)
                ->with('get_setting_extended', $get_setting_extended);
        }

        else {
            return Redirect::to('siteadmin');
        }

    }

    public function manage_product()
    {
        if (Session::has('userid')) {

            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }
            $merchant_search = Input::get('merchant_search');
            // $productrep = Products::get_productreports($from_date, $to_date);
            $get_merchant_id = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
            $kuku_merchant_id = $get_merchant_id->mer_id;
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $details = DB::table('nm_product')
            ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            ->where('pro_mr_id', $kuku_merchant_id)
            ->where('pro_qty', '<>', 0)
            ->where('pro_isapproved', 1)
            ->where('pro_issepulsa', 0)
            ->get();

            $details_mer = $get_product = DB::table('nm_product')
            ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
            ->where('pro_mr_id', '<>', $kuku_merchant_id)
            ->where('pro_qty', '<>', 0)
            ->where('pro_isapproved', 1)
            ->where('pro_issepulsa', 0)
            ->get();

            $details_sepulsa = DB::table('nm_product')
            ->where('pro_issepulsa', 1)
            ->orderBy('pro_id', 'desc')
            ->get();

			$delete_product = Products::get_order_details();

            if ($from_date != '' && $to_date == '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.activated_date', '>=', $from_date)
                ->where('nm_product.pro_status', '=', 1)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.activated_date', '>=', $from_date)
                ->where('nm_product.pro_status', '=', 1)
                ->where('nm_product.pro_isapproved', 1)
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_sepulsa = DB::table('nm_product')
                ->where('activated_date', '>=', $from_date)
                ->where('pro_issepulsa', 1)
                ->orderBy('pro_id', 'DESC')
                ->get();
            }
            elseif ($from_date == '' && $to_date != '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.activated_date', '<=', $to_date)
                ->where('nm_product.pro_status', '=', 1)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.activated_date', '<=', $to_date)
                ->where('nm_product.pro_status', '=', 1)
                ->where('nm_product.pro_isapproved', 1)
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_sepulsa = DB::table('nm_product')
                ->where('activated_date', '<=', $to_date)
                ->where('pro_issepulsa', 1)
                ->orderBy('pro_id', 'DESC')
                ->get();
            }
            elseif ($from_date != '' && $to_date != '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.activated_date', array($from_date, $to_date))
                ->where('nm_product.pro_status', '=', 1)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.activated_date', array($from_date, $to_date))
                ->where('nm_product.pro_status', '=', 1)
                ->where('nm_product.pro_isapproved', 1)
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_sepulsa = DB::table('nm_product')
                ->whereBetween('activated_date', array($from_date, $to_date))
                ->where('pro_issepulsa', 1)
                ->orderBy('pro_id', 'DESC')
                ->get();
            }else {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.pro_status', '=', 1)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.pro_status', '=', 1)
                ->where('nm_product.pro_isapproved', 1)
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_sepulsa = DB::table('nm_product')
                ->where('pro_issepulsa', 1)
                ->orderBy('pro_id', 'desc')
                ->get();
            }


            return view('siteadmin.manage_product')->with('productrep_sepulsa', $productrep_sepulsa)->with('sepulsa_product_details', $details_sepulsa)->with('mer_product_details',$details_mer)->with('kuku_merchant_id', $kuku_merchant_id)->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('product_details', $details)->with('productrep', $productrep)->with('productrep_mer', $productrep_mer)->with('delete_product', $delete_product);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    public function approval_product()
    {
        if (Session::has('userid')) {
            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }
            $merchant_search = Input::get('merchant_search');
            // $productrep = Products::get_productreports($from_date, $to_date);
            $get_merchant_id = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
            $kuku_merchant_id = $get_merchant_id->mer_id;
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $details = DB::table('nm_product')
            ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            ->where('pro_mr_id', '<>', $kuku_merchant_id)
            ->where('pro_isresponse', 0)
            ->where('pro_isapproved', 0)
            ->where('pro_title','!=','Jasa Pengiriman')
            ->where('pro_title','!=','Biaya Pengiriman')
            ->where('pro_title','!=','Diskon')
            ->orderBy('nm_product.pro_id', 'asc')
            ->get();
            $delete_product = Products::get_order_details();

            if ($from_date != '' && $to_date == '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.created_date', '>=', $from_date)
                ->where('pro_isresponse', 0)
                ->where('pro_isapproved', 0)
                ->where('pro_title','!=','Jasa Pengiriman')
                ->where('pro_title','!=','Biaya Pengiriman')
                ->where('pro_title','!=','Diskon')
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'asc')
                ->get();
            }
            elseif ($from_date == '' && $to_date != '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('nm_product.created_date', '<=', $to_date)
                ->where('pro_isresponse', 0)
                ->where('pro_isapproved', 0)
                ->where('pro_title','!=','Jasa Pengiriman')
                ->where('pro_title','!=','Biaya Pengiriman')
                ->where('pro_title','!=','Diskon')
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'asc')
                ->get();
            }
            elseif ($from_date != '' && $to_date != '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', array($from_date,$to_date))
                ->where('pro_isresponse', 0)
                ->where('pro_isapproved', 0)
                ->where('pro_title','!=','Jasa Pengiriman')
                ->where('pro_title','!=','Biaya Pengiriman')
                ->where('pro_title','!=','Diskon')
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();
            }else {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('pro_isresponse', 0)
                ->where('pro_isapproved', 0)
                ->where('pro_title','!=','Jasa Pengiriman')
                ->where('pro_title','!=','Biaya Pengiriman')
                ->where('pro_title','!=','Diskon')
                ->where('pro_mr_id', '<>', $kuku_merchant_id)
                ->orderBy('nm_product.pro_id', 'asc')
                ->get();
            }

            return view('siteadmin.approval_product')
            ->with('kuku_merchant_id', $kuku_merchant_id)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('product_details', $details)
            ->with('productrep', $productrep)
            ->with('delete_product', $delete_product);
        }else {
            return Redirect::to('siteadmin');
        }
    }

    public function approve_product($id)
    {
        if (Session::has('userid')) {
            date_default_timezone_set('Asia/Jakarta');
            $date = date('Y-m-d H:i:s');

            $approve = DB::table('nm_product')->where('pro_id', $id)->update([
                'pro_isapproved'=>1,
                'pro_status'=>1,
                'pro_isresponse'=>1,
                'activated_date'=>$date
            ]);

            $audit_trail = new AuditTrail;
            $audit_trail->aud_table = 'nm_product';
            $audit_trail->aud_action = 'update';
            $audit_trail->aud_detail = '{"pro_id":"'.$id.'","pro_isapproved":"1","pro_status":"1","pro_isresponse":"1","activated_date":"'.$date.'"}';
            $audit_trail->aud_date = $date;
            $audit_trail->aud_user_name = Session::get('username');
            $audit_trail->save();

            if($approve){
                return Redirect::to('approval_product')->with('block_message', 'Product has been approved');
            }
        }else {
            return Redirect::to('siteadmin');
        }
    }

    public function reject_product($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $query = DB::table('nm_product')->where('pro_id', $id);
            $return = DB::table('nm_product')->where('pro_id', '=', $id)->first();
            $selectmaincategory = DB::table('nm_maincategory')->where('mc_id', $return->pro_mc_id)->first();
            $selectsecmaincategory = DB::table('nm_secmaincategory')->where('smc_id', $return->pro_smc_id)->first();
            $selectsubcategory = DB::table('nm_subcategory')->where('sb_id', $return->pro_sb_id)->first();
            $selectsecsubcategory = DB::table('nm_secsubcategory')->where('ssb_id', $return->pro_ssb_id)->first();
            $selectgrade = DB::table('nm_grade')->where('grade_id', $return->pro_grade_id)->first();
            $selectcolor = DB::table('nm_color')->where('co_id', $return->pro_color_id)->first();
            // dd($return);
            if($selectmaincategory){
                $query = $query->join('nm_maincategory', 'nm_product.pro_mc_id', '=', 'nm_maincategory.mc_id');
            }
            if($selectsecmaincategory){
                $query = $query->join('nm_secmaincategory', 'nm_product.pro_smc_id', '=', 'nm_secmaincategory.smc_id');
            }
            if($selectsubcategory){
                $query = $query->join('nm_subcategory', 'nm_product.pro_sb_id', '=', 'nm_subcategory.sb_id');
            }
            if($selectsecsubcategory){
                $query = $query->join('nm_secsubcategory', 'nm_product.pro_ssb_id', '=', 'nm_secsubcategory.ssb_id');
            }
            if($selectgrade){
                $query = $query->join('nm_grade', 'nm_product.pro_grade_id', '=', 'nm_grade.grade_id');
            }
            if($selectcolor){
                $query = $query->join('nm_color', 'nm_product.pro_color_id', '=', 'nm_color.co_id');
            }
            $query = $query->get();

            $get_pay        = Settings::get_pay_settings();
            $get_cur        = $get_pay[0]->ps_cursymbol;

            $productspecification = DB::table('nm_product')
            ->Leftjoin('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
            ->Leftjoin('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
            ->Leftjoin('nm_prospec', 'nm_prospec.spc_sp_id', '=', 'nm_specification.sp_id')
            ->where('pro_id', $id)
            ->where('spc_pro_id', $id)
            ->get();
            return view('siteadmin.reject_product')->with('get_cur', $get_cur)->with('productspecification', $productspecification)->with('id', $id)->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('product_list', $query);
        }else {
            return Redirect::to('siteadmin');
        }
    }

    public function reject_product_process(Request $request)
    {
        if (Session::has('userid')) {
            $id = $request->input('id');
            $message = $request->input('message');
            $reject = DB::table('nm_product')->where('pro_id', $id)->update(['pro_isresponse'=>1, 'pro_message'=>$message, 'pro_isapproved'=>0, 'pro_status'=>0]);
            return Redirect::to('approval_product')->with('block_message', 'Product has been rejected');
        }else {
            return Redirect::to('siteadmin');
        }
    }
    public function delete_product($id)
	{

		if(Session::has('userid'))
		{
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
    	 $del_pro = Products::delete_product($id);

         date_default_timezone_set('Asia/Jakarta');
         $now = date('Y-m-d H:i:s');

         $audit_trail = new AuditTrail;
         $audit_trail->aud_table = 'nm_product';
         $audit_trail->aud_action = 'delete';
         $audit_trail->aud_detail = '{"pro_id":"'.$id.'"}';
         $audit_trail->aud_date = $now;
         $audit_trail->aud_user_name = Session::get('username');
         $audit_trail->save();

		 return Redirect::to(redirect()->back()->getTargetUrl())->with('Product Deleted','Product Deleted Successfully');
		}
		else
        {
         return Redirect::to('siteadmin');
        }
	}

    public function sold_product()
    {

        if (Session::has('userid')) {
            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }

            $soldrep   = Products::get_soldrep($from_date, $to_date);

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $details = DB::table('nm_product')
            ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
            // ->where('pro_status', 1)
            ->where('pro_isapproved', 1)
            ->where('pro_title', '!=', 'Diskon')
            ->where('pro_title', '!=', 'Jasa Pengiriman')
            ->where('pro_title', '!=', 'Biaya Pengiriman')
            ->get();
            //dd($details);

            return view('siteadmin.sold_products')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('product_details', $details)->with('soldrep', $soldrep);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }



    public function add_product_submit()
    {
        if (Session::has('userid')) {

            $date = date('m/d/Y');
            $data = Input::except(array(
                '_token'
            ));

            $count = Input::get('count');
            $selectedgrade = Input::get('selectprograde');
            $selectedcolors = Input::get('co');
            $trimmedselectedcolors = trim($selectedcolors, ",");
            $colorarray = explode(",", $trimmedselectedcolors);
            $colorcount = count($colorarray) - 1;

            $specificationcount = Input::get('specificationcount');
            $selectedsizes = Input::get('si');
            $trimmedsizes = trim($selectedsizes, ",");
            $sizearray = explode(",", $trimmedsizes);
            $productsizecount = Input::get('productsizecount');

            $filename_new_get = "";
            for ($i = 0; $i < $count; $i++) {
                $file_more = Input::file('file_more' . $i);
                $file_more_name = $file_more->getClientOriginalName();

                $move_more_img = explode('.', $file_more_name);
                $filename_new = $move_more_img[0] . "." . $move_more_img[1];
                $newdestinationPath = './assets/product/';

                $uploadSuccess_new = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);
                $filename_new_get .= $filename_new . "/**/";
            }

            $now = date('Y-m-d H:i:s');
            $inputs = Input::all();
            $file     = Input::file('file');
            $filename = $file->getClientOriginalName();

            $move_img = explode('.', $filename);
            $filename = $move_img[0] . "." . $move_img[1];

            $destinationPath = './assets/product/';
            $uploadSuccess = Input::file('file')->move($destinationPath, $filename);
            $file_name_insert = $filename . "/**/" . $filename_new_get;
            $Product_Title = Input::get('Product_Title');
            $Product_Category = Input::get('Product_Category');
            $Product_MainCategory = Input::get('Product_MainCategory');
            $Product_SubCategory = Input::get('Product_SubCategory');
            $Product_SecondSubCategory = Input::get('Product_SecondSubCategory');
            $Original_Price = Input::get('Original_Price');
            $Discounted_Price = Input::get('Discounted_Price');
            $Shipping_Amount = Input::get('Shipping_Amount');
            if ($Shipping_Amount == "") {
                $Shipping_Amount = 0;
            }

            $Description = Input::get('Description');
            $pquantity = Input::get('Quantity_Product');
            $Delivery_Days = Input::get('Delivery_Days');
            $Delivery_Policy = Input::get('Delivery_Policy');
            $Meta_Title = Input::get('Meta_Title');
            $Meta_Keywords = Input::get('Meta_Keywords');
            $Meta_Description = Input::get('Meta_Description');
            $Select_Merchant = Input::get('Select_Merchant');
            $Select_Shop = Input::get('Select_Shop');
            $inc_tax = Input::get('inctax');
            $add_spec = 1;
            $postfb = Input::get('postfb');
            $img_count = Input::get('count');

            for ($i = 0; $i <= $specificationcount; $i++) {
                if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {
                    $add_spec = 2;
                }
            }

            // wholesale
            $ws1_min = Input::get('qty_min_1');
            $ws1_max = Input::get('qty_max_1');
            $ws1_price = Input::get('prd_prc_1');
            $ws2_min = Input::get('qty_min_2');
            $ws2_max = Input::get('qty_max_2');
            $ws2_price = Input::get('prd_prc_2');
            $ws3_min = Input::get('qty_min_3');
            $ws3_max = Input::get('qty_max_3');
            $ws3_price = Input::get('prd_prc_3');
            $ws4_min = Input::get('qty_min_4');
            $ws4_max = Input::get('qty_max_4');
            $ws4_price = Input::get('prd_prc_4');
            $ws5_min = Input::get('qty_min_5');
            $ws5_max = Input::get('qty_max_5');
            $ws5_price = Input::get('prd_prc_5');

            // warranty
            $warranty = Input::get('extended_warranty');

            $check_store = Products::check_store($Product_Title, $Select_Shop);
            if ($check_store) {
                return Redirect::to('add_product')->with('success', 'The Product Already exist in the Store');
            } else {
                $entry = array(
                    'pro_title' => $Product_Title,
                    'pro_mc_id' => $Product_Category,
                    'pro_smc_id' => $Product_MainCategory,
                    'pro_sb_id' => $Product_SubCategory,
                    'pro_ssb_id' => $Product_SecondSubCategory,
                    'pro_qty' => $pquantity,
                    'pro_price' => $Original_Price,
                    'pro_disprice' => $Discounted_Price,
                    'pro_inctax' => $inc_tax,
                    'pro_shippamt' => $Shipping_Amount,
                    'pro_desc' => $Description,
                    'pro_isspec' => $add_spec,
                    'pro_delivery' => $Delivery_Days,
                    'pro_mr_id' => $Select_Merchant,
                    'pro_sh_id' => $Select_Shop,
                    'pro_mtitle' => $Meta_Title,

                    'pro_mkeywords' => $Meta_Keywords,
                    'pro_mdesc' => $Meta_Description,

                    'pro_Img' => $file_name_insert,
                    'pro_image_count' => $img_count,
                    'pro_qty' => $pquantity,
                    'created_date' => $date,
                    'pro_grade_id' => $selectedgrade,
                    'pro_isapproved' => 1,
                    'pro_isresponse' => 1,

                    'wholesale_level1_min' => $ws1_min,
                    'wholesale_level1_max' => $ws1_max,
                    'wholesale_level1_price' => $ws1_price,
                    'wholesale_level2_min' => $ws2_min,
                    'wholesale_level2_max' => $ws2_max,
                    'wholesale_level2_price' => $ws2_price,
                    'wholesale_level3_min' => $ws3_min,
                    'wholesale_level3_max' => $ws3_max,
                    'wholesale_level3_price' => $ws3_price,
                    'wholesale_level4_min' => $ws4_min,
                    'wholesale_level4_max' => $ws4_max,
                    'wholesale_level4_price' => $ws4_price,
                    'wholesale_level5_min' => $ws5_min,
                    'wholesale_level5_max' => $ws5_max,
                    'wholesale_level5_price' => $ws5_price,

                    'have_warranty' => $warranty,
                );

                $productid = Products::insert_product($entry);
            }

            if ($productid) {
                if ($colorcount > 0) {
                    for ($i = 0; $i < $colorcount; $i++) {
                        $val = Input::get('colorcheckbox' . $colorarray[$i]);
                        if ($val == 1) {
                            $colorentry = array(
                                'pc_pro_id' => $productid,
                                'pc_co_id' => $colorarray[$i]
                            );
                            Products::insert_product_color_details($colorentry);
                        }
                    }
                }

                if ($add_spec == 1) {
                    for ($i = 0; $i <= $specificationcount; $i++) {
                        if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {
                        }
                        else {
                            $specificationentry = array(
                                'spc_pro_id' => $productid,
                                'spc_sp_id' => Input::get('spec' . $i),
                                'spc_value' => Input::get('spectext' . $i)
                            );
                            Products::insert_product_specification_details($specificationentry);
                        }
                    }
                }

                if ($productsizecount > 0) {
                    for ($i = 0; $i < $productsizecount; $i++) {
                        $val = Input::get('sizecheckbox' . $sizearray[$i]);
                        if ($val == 1) {
                            if (Input::get('quantity' . $sizearray[$i]) == "") {
                                $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    'ps_si_id' => $sizearray[$i],
                                    'ps_volume' => 0
                                );
                            }
                            else {
                                $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    'ps_si_id' => $sizearray[$i],
                                    'ps_volume' => Input::get('quantity' . $sizearray[$i])
                                );
                            }
                            Products::insert_product_size_details($productsizeentry);
                        }
                    }
                }
            }
            return Redirect::to('manage_product');
        }

        else {
            return Redirect::to('siteadmin');
        }
    }


    public function add_product_submitold()
    {

        if (Session::has('userid')) {

            $data = Input::except(array(
                '_token'
            ));

            $count = Input::get('count');

            $selectedcolors = Input::get('co');

            $trimmedselectedcolors = trim($selectedcolors, ",");

            $colorarray = explode(",", $trimmedselectedcolors);

            $colorcount = count($colorarray) - 1;

            $specificationcount = Input::get('specificationcount');

            $selectedsizes = Input::get('si');

            $trimmedsizes = trim($selectedsizes, ",");

            $sizearray = explode(",", $trimmedsizes);

            $productsizecount = Input::get('productsizecount');

            $filename_new_get = "";

            for ($i = 0; $i < $count; $i++) {

                $file_more = Input::file('file_more' . $i);

                $file_more_name = $file_more->getClientOriginalName();

                $move_more_img = explode('.', $file_more_name);

                $filename_new = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];

                $newdestinationPath = './assets/product/';

                $uploadSuccess_new = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);

                $filename_new_get .= $filename_new . "/**/";

            }


            $now = date('Y-m-d H:i:s');

            $inputs = Input::all();

            $file = Input::file('file');

            $filename = $file->getClientOriginalName();

            $move_img = explode('.', $filename);

            $filename = $move_img[0] . str_random(8) . "." . $move_img[1];

            $destinationPath = './assets/product/';

            $uploadSuccess = Input::file('file')->move($destinationPath, $filename);

            $file_name_insert = $filename . "/**/" . $filename_new_get;

            $Product_Title = Input::get('Product_Title');

            $Product_Category = Input::get('Product_Category');

            $Product_MainCategory = Input::get('Product_MainCategory');

            $Product_SubCategory = Input::get('Product_SubCategory');

            $Product_SecondSubCategory = Input::get('Product_SecondSubCategory');

            $Original_Price = Input::get('Original_Price');

            $Discounted_Price = Input::get('Discounted_Price');

            $Shipping_Amount = Input::get('Shipping_Amount');

            $Description = Input::get('Description');

            $pquantity = Input::get('Quantity_Product');

            $Delivery_Days = Input::get('Delivery_Days');

            $Delivery_Policy = Input::get('Delivery_Policy');

            $Meta_Keywords = Input::get('Meta_Keywords');

            $Meta_Description = Input::get('Meta_Description');

            $Select_Merchant = Input::get('Select_Merchant');

            $Select_Shop = Input::get('Select_Shop');

            $inc_tax = Input::get('inctax');

            $add_spec = Input::get('specification');

            $postfb = Input::get('postfb');

            $img_count = Input::get('count');

            if ($inc_tax == 1) {

            }

            else {

                $inc_tax = 0;

            }

            $entry = array(

                'pro_title' => $Product_Title,

                'pro_mc_id' => $Product_Category,

                'pro_smc_id' => $Product_MainCategory,

                'pro_sb_id' => $Product_SubCategory,

                'pro_ssb_id' => $Product_SecondSubCategory,

                'pro_price' => $Original_Price,

                'pro_disprice' => $Discounted_Price,

                'pro_inctax' => $inc_tax,

                'pro_shippamt' => $Shipping_Amount,

                'pro_desc' => $Description,

                'pro_isspec' => $add_spec,

                'pro_delivery' => $Delivery_Days,

                'pro_mr_id' => $Select_Merchant,

                'pro_sh_id' => $Select_Shop,

                'pro_mkeywords' => $Meta_Keywords,

                'pro_mdesc' => $Meta_Description,

                'pro_Img' => $file_name_insert,

                'pro_image_count' => $img_count,

                'pro_qty' => $pquantity

            );

            $productid = Products::insert_product($entry);

            if ($productid) {

                if ($colorcount > 0) {

                    for ($i = 0; $i < $colorcount; $i++) {

                        $colorentry = array(
                            'pc_pro_id' => $productid,

                            'pc_co_id' => $colorarray[$i]

                        );

                        Products::insert_product_color_details($colorentry);

                    }

                }

                for ($i = 0; $i <= $specificationcount; $i++) {

                    $specificationentry = array(
                        'spc_pro_id' => $productid,

                        'spc_sp_id' => Input::get('spec' . $i),

                        'spc_value' => Input::get('spectext' . $i)

                    );

                    Products::insert_product_specification_details($specificationentry);

                }

                if ($productsizecount > 0) {

                    for ($i = 0; $i < $productsizecount; $i++) {

                        $val = Input::get('sizecheckbox' . $sizearray[$i]);

                        if ($val == 1) {

                            $productsizeentry = array(
                                'ps_pro_id' => $productid,

                                'ps_si_id' => $sizearray[$i],

                                'ps_volume' => Input::get('quantity' . $sizearray[$i])

                            );

                            Products::insert_product_size_details($productsizeentry);

                        }

                        else {



                        }

                    }

                }

            }

            return Redirect::to('manage_product');

        }

        else {

            return Redirect::to('siteadmin');

        }

    }


    public function edit_product_submit()
    {

        if (Session::has('userid')) {
            date_default_timezone_set('Asia/Jakarta');
            $now = date('Y-m-d H:i:s');
            $inputs = Input::all();
            $id = Input::get('product_edit_id');
            $productid = Input::get('product_edit_id');
            $selectedgrade = Input::get('selectprograde');
            $selectedcolors = Input::get('co');
            $trimmedselectedcolors = trim($selectedcolors, ",");
            $colorarray = explode(",", $trimmedselectedcolors);
            $colorcount = count($colorarray);
            $returncolor = Products::delete_product_color($id);
            $returnsize = Products::delete_product_size($id);
            $returnspec = Products::delete_product_spec($id);
            $specificationcount = Input::get('specificationcount');
            $selectedsizes = Input::get('si');
            $trimmedsizes = trim($selectedsizes, ",");
            $sizearray = explode(",", $trimmedsizes);
            $productsizecount = Input::get('productsizecount');
            $img_count = Input::get('count');
            $warranty = Input::get('extended_warranty');
            $cicilan_3bulan = Input::get('cicilan_3bulan');
            $cicilan_6bulan = Input::get('cicilan_6bulan');
            $cicilan_12bulan = Input::get('cicilan_12bulan');

            $filename_new_get = "";
            for ($i = 0; $i < $img_count; $i++) {
                $file_more = Input::file('file_more' . $i);
                if ($file_more == "") {
                    $dile_name_new_get = Input::get('file_more_new' . $i);
                    $filename_new_get .= $dile_name_new_get . "/**/";
                } else {
                    $file_more_name = $file_more->getClientOriginalName();
                    $move_more_img = explode('.', $file_more_name);
                    $filename_new = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];
                    $newdestinationPath = './assets/product/';
                    $uploadSuccess_new = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);
                    $filename_new_get .= $filename_new . "/**/";
                }
            }

            $file = Input::file('file');
            if ($file == "") {
                $filename = Input::get('file_new');
            }

            else {
                $filename1 = $file->getClientOriginalName();
                $move_img = explode('.', $filename1);
                $filename = $move_img[0] . str_random(8) . "." . $move_img[1];
                $destinationPath = './assets/product/';
                $uploadSuccess = Input::file('file')->move($destinationPath, $filename);
            }

            $file_name_insert = $filename . "/**/" . $filename_new_get;

            $id = Input::get('product_edit_id');
            $Product_Title = Input::get('Product_Title');
            $Product_Category = Input::get('category');
            $Product_MainCategory = Input::get('maincategory');
            $Product_SubCategory = Input::get('subcategory');
            $Product_SecondSubCategory = Input::get('secondsubcategory');
            $Original_Price = Input::get('Original_Price');
            $Discounted_Price = Input::get('Discounted_Price');
            $Shipping_Amount = Input::get('Shipping_Amount');

            if ($Shipping_Amount == "") {
                $Shipping_Amount = 0;
            }

            $Description = Input::get('Description');
            $Delivery_Days = Input::get('Delivery_Days');
            $Delivery_Policy = Input::get('Delivery_Policy');
            $Meta_Title = Input::get('Meta_Title');
            $Meta_Keywords = Input::get('Meta_Keywords');
            $Meta_Description = Input::get('Meta_Description');
            $Select_Merchant = Input::get('Select_Merchant');
            $Select_Shop = Input::get('Select_Shop');
            $inc_tax = Input::get('inctax');
            $add_spec = 1;
            $postfb = Input::get('postfb');
            $img_count = Input::get('count');
            $pquantity = Input::get('Quantity_Product');
            $sold_out  = 1;
            $SKU = Input::get('Product_SKU');

            // wholesale
            $ws1_min = Input::get('qty_min_1');
            $ws1_max = Input::get('qty_max_1');
            $ws1_price = Input::get('prd_prc_1');
            $ws2_min = Input::get('qty_min_2');
            $ws2_max = Input::get('qty_max_2');
            $ws2_price = Input::get('prd_prc_2');
            $ws3_min = Input::get('qty_min_3');
            $ws3_max = Input::get('qty_max_3');
            $ws3_price = Input::get('prd_prc_3');
            $ws4_min = Input::get('qty_min_4');
            $ws4_max = Input::get('qty_max_4');
            $ws4_price = Input::get('prd_prc_4');
            $ws5_min = Input::get('qty_min_5');
            $ws5_max = Input::get('qty_max_5');
            $ws5_price = Input::get('prd_prc_5');

            $entry = array(
                'pro_title' => $Product_Title,
                //SKU
                'pro_weight' => Input::get('Product_Weight'),
                'pro_length' => Input::get('Product_Length'),
                'pro_width' => Input::get('Product_Width'),
                'pro_height' => Input::get('Product_Height'),
                'pro_mc_id' => $Product_Category,
                'pro_smc_id' => $Product_MainCategory,
                'pro_sb_id' => $Product_SubCategory,
                'pro_ssb_id' => $Product_SecondSubCategory,
                'pro_qty' => $pquantity,
                'pro_price' => $Original_Price,
                'pro_disprice' => $Discounted_Price,
                'pro_desc' => $Description,
                //'pro_inctax' => $inc_tax,
                //'pro_shippamt' => $Shipping_Amount,
                'pro_isspec' => $add_spec,
                //'pro_delivery' => $Delivery_Days,
                'pro_mr_id' => $Select_Merchant,
                'pro_sh_id' => $Select_Shop,
                'pro_mtitle' => $Meta_Title,
                'pro_mkeywords' => $Meta_Keywords,
                'pro_mdesc' => $Meta_Description,
                'pro_Img' => $file_name_insert,
                'pro_image_count' => $img_count,
                'sold_status' => $sold_out,

                'pro_grade_id' => $selectedgrade,
                'pro_sku_merchant' => $SKU,
                'pro_color_id' => Input::get('select_color'),
                'pro_grade_id' => Input::get('selectprograde'),
                'pro_poin' => Input::get('product_point'),

                'wholesale_level1_min' => $ws1_min,
                'wholesale_level1_max' => $ws1_max,
                'wholesale_level1_price' => $ws1_price,
                'wholesale_level2_min' => $ws2_min,
                'wholesale_level2_max' => $ws2_max,
                'wholesale_level2_price' => $ws2_price,
                'wholesale_level3_min' => $ws3_min,
                'wholesale_level3_max' => $ws3_max,
                'wholesale_level3_price' => $ws3_price,
                'wholesale_level4_min' => $ws4_min,
                'wholesale_level4_max' => $ws4_max,
                'wholesale_level4_price' => $ws4_price,
                'wholesale_level5_min' => $ws5_min,
                'wholesale_level5_max' => $ws5_max,
                'wholesale_level5_price' => $ws5_price,

                'have_warranty' => $warranty,

                'jumlah_cicilan_3bulan' => $cicilan_3bulan,
                'jumlah_cicilan_6bulan' => $cicilan_6bulan,
                'jumlah_cicilan_12bulan' => $cicilan_12bulan,

                // 'activated_date' => $now
            );

            $return = Products::edit_product($entry, $id);

            $include = self::view_include('products', 'admin_left_menu_product');

            $product_sku = DB::table('nm_product')
            ->select('pro_sku')
            ->where('pro_id', $id)
            ->first();

            foreach ($include['privileges'] as $priv) {
                $priv_name = explode(' ', $priv['rp_priv_name']);

                if($priv_name[3] == 'content' && $priv_name[4] == 'edit_product'){
                    $audit_trail = new AuditTrail;
                    $audit_trail->aud_table = 'nm_product';
                    $audit_trail->aud_action = 'update';
                    $audit_trail->aud_detail = '{"pro_id":"'.$id.'",';
                    $audit_trail->aud_detail .= '"pro_sku":"'.$product_sku->pro_sku.'",';
                    $audit_trail->aud_detail .= '"pro_title":"'.$Product_Title.'",';
                    $audit_trail->aud_detail .= '"pro_weight":"'.Input::get('Product_Weight').'",';
                    $audit_trail->aud_detail .= '"pro_length":"'.Input::get('Product_Length').'",';
                    $audit_trail->aud_detail .= '"pro_width":"'.Input::get('Product_Width').'",';
                    $audit_trail->aud_detail .= '"pro_height":"'.Input::get('Product_Height').'",';
                    $audit_trail->aud_detail .= '"pro_mc_id":"'.$Product_Category.'",';
                    $audit_trail->aud_detail .= '"pro_smc_id":"'.$Product_MainCategory.'",';
                    $audit_trail->aud_detail .= '"pro_sb_id":"'.$Product_SubCategory.'",';
                    $audit_trail->aud_detail .= '"pro_ssb_id":"'.$Product_SecondSubCategory.'",';
                    $audit_trail->aud_detail .= '"pro_qty":"'.$pquantity.'",';
                    $audit_trail->aud_detail .= '"pro_desc":"'.$Description.'",';
                    $audit_trail->aud_detail .= '"pro_isspec":"'.$add_spec.'",';
                    $audit_trail->aud_detail .= '"pro_mr_id":"'.$Select_Merchant.'",';
                    $audit_trail->aud_detail .= '"pro_sh_id":"'.$Select_Shop.'",';
                    $audit_trail->aud_detail .= '"pro_mtitle":"'.$Meta_Title.'",';
                    $audit_trail->aud_detail .= '"pro_mkeywords":"'.$Meta_Keywords.'",';
                    $audit_trail->aud_detail .= '"pro_mdesc":"'.$Meta_Description.'",';
                    $audit_trail->aud_detail .= '"pro_Img":"'.$file_name_insert.'",';
                    $audit_trail->aud_detail .= '"pro_image_count":"'.$img_count.'",';
                    $audit_trail->aud_detail .= '"sold_status":"'.$sold_out.'",';
                    $audit_trail->aud_detail .= '"pro_grade_id":"'.$selectedgrade.'",';
                    $audit_trail->aud_detail .= '"pro_sku_merchant":"'.$SKU.'",';
                    $audit_trail->aud_detail .= '"pro_color_id":"'.Input::get('select_color').'",';
                    $audit_trail->aud_detail .= '"pro_grade_id":"'.Input::get('selectprograde').'",';
                    $audit_trail->aud_detail .= '"pro_poin":"'.Input::get('product_point').'",';
                    $audit_trail->aud_detail .= '"have_warranty":"'.$warranty.'"';
                    $audit_trail->aud_detail .= '}';
                    $audit_trail->aud_date = $now;
                    $audit_trail->aud_user_name = Session::get('username');
                    $audit_trail->save();
                }
                if ($priv_name[3] == 'price' && $priv_name[4] == 'edit_product') {
                    $audit_trail = new AuditTrail;
                    $audit_trail->aud_table = 'nm_product';
                    $audit_trail->aud_action = 'update';
                    $audit_trail->aud_detail = '{"pro_id":"'.$id.'",';
                    $audit_trail->aud_detail .= '"pro_sku":"'.$product_sku->pro_sku.'",';
                    $audit_trail->aud_detail .= '"pro_price":"'.$Original_Price.'",';
                    $audit_trail->aud_detail .= '"pro_disprice":"'.$Discounted_Price.'",';
                    $audit_trail->aud_detail .= '"wholesale_level1_min":"'.$ws1_min.'",';
                    $audit_trail->aud_detail .= '"wholesale_level1_max":"'.$ws1_max.'",';
                    $audit_trail->aud_detail .= '"wholesale_level1_price":"'.$ws1_price.'",';
                    $audit_trail->aud_detail .= '"wholesale_level2_min":"'.$ws2_min.'",';
                    $audit_trail->aud_detail .= '"wholesale_level2_max":"'.$ws2_max.'",';
                    $audit_trail->aud_detail .= '"wholesale_level2_price":"'.$ws2_price.'",';
                    $audit_trail->aud_detail .= '"wholesale_level3_min":"'.$ws3_min.'",';
                    $audit_trail->aud_detail .= '"wholesale_level3_max":"'.$ws3_max.'",';
                    $audit_trail->aud_detail .= '"wholesale_level3_price":"'.$ws3_price.'",';
                    $audit_trail->aud_detail .= '"wholesale_level4_min":"'.$ws4_min.'",';
                    $audit_trail->aud_detail .= '"wholesale_level4_max":"'.$ws4_max.'",';
                    $audit_trail->aud_detail .= '"wholesale_level4_price":"'.$ws4_price.'",';
                    $audit_trail->aud_detail .= '"wholesale_level5_min":"'.$ws5_min.'",';
                    $audit_trail->aud_detail .= '"wholesale_level5_max":"'.$ws5_max.'",';
                    $audit_trail->aud_detail .= '"wholesale_level5_price":"'.$ws5_price.'",';
                    $audit_trail->aud_detail .= '"jumlah_cicilan_3bulan":"'.$cicilan_3bulan.'",';
                    $audit_trail->aud_detail .= '"jumlah_cicilan_6bulan":"'.$cicilan_6bulan.'",';
                    $audit_trail->aud_detail .= '"jumlah_cicilan_12bulan":"'.$cicilan_12bulan.'"';
                    $audit_trail->aud_detail .= '}';
                    $audit_trail->aud_date = $now;
                    $audit_trail->aud_user_name = Session::get('username');
                    $audit_trail->save();
                }
            }


            $get_product = DB::table('nm_product')->where('pro_id', $id)->first();
            if (!$get_product->pro_title
            || $get_product->pro_weight == 0
            || $get_product->pro_length == 0
            || $get_product->pro_width == 0
            || $get_product->pro_height == 0
            || $get_product->pro_mc_id == 0
            || $get_product->pro_smc_id == 0
            || $get_product->pro_sb_id == 0
            || $get_product->pro_ssb_id == 0
            || $get_product->pro_qty == 0
            || $get_product->pro_price == 0
            || !$get_product->pro_desc
            || $get_product->pro_mr_id == 0
            || $get_product->pro_sh_id == 0
            || !$get_product->pro_mtitle
            || !$get_product->pro_mkeywords
            || !$get_product->pro_mdesc
            || $get_product->pro_Img == '/**/'
            || $get_product->pro_grade_id == 0
            || $get_product->pro_color_id == 0) {
                $input = [
                    'pro_isapproved' => 0,
                    'pro_isresponse' => 0,
                    'pro_status' => 0,
                    'activated_date' => null
                ];
            }else {
                $input = [
                    'pro_isapproved' => 1,
                    'pro_isresponse' => 1,
                    'pro_status' => 1,
                    'activated_date' => $now
                ];
            }

            $return2 = Products::edit_product($input, $id);

            if ($colorcount > 0) {
                for ($i = 0; $i < $colorcount; $i++) {
                    $val = Input::get('colorcheckbox' . $colorarray[$i]);
                    if ($val == 1) {
                        $colorentry = array(
                            'pc_pro_id' => $productid,
                            'pc_co_id' => $colorarray[$i]
                        );

                        Products::insert_product_color_details($colorentry);
                    }
                    else {
                    }
                }
            }

            for ($i = 0; $i <= $specificationcount; $i++) {
                // dd(Input::get('spectext'.$i));
                if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {
                }
                else {
                    $specificationentry = array(
                        'spc_pro_id' => $productid,
                        'spc_sp_id' => Input::get('spec' . $i),
                        'spc_value' => Input::get('spectext' . $i)
                    );
                    Products::insert_product_specification_details($specificationentry);
                }
            }

            if ($productsizecount > 0) {
                for ($i = 0; $i < $productsizecount; $i++) {
                    $val = Input::get('sizecheckbox' . $sizearray[$i]);
                    if ($val == 1) {
                        $productsizeentry = array(
                            'ps_pro_id' => $productid,
                            'ps_si_id' => $sizearray[$i],
                            'ps_volume' => Input::get('quantity' . $sizearray[$i])
                        );
                        Products::insert_product_size_details($productsizeentry);
                    }

                    else {
                    }
                }
            }

            return Redirect::to(Input::get('url'))->with('block_message', 'Product Updated Successfully');
        }
        else {
            return Redirect::to('siteadmin');
        }
    }


    public function edit_product_submitold()
    {

        if (Session::has('userid')) {

            $now = date('Y-m-d H:i:s');

            $inputs = Input::all();

            $pquantity = Input::get('Quantity_Product');

            $id = Input::get('product_edit_id');

            $productid = Input::get('product_edit_id');

            $returncolor = Products::delete_product_color($id);

            $returnsize = Products::delete_product_size($id);

            $returnspec = Products::delete_product_spec($id);

            $selectedcolors = Input::get('co');

            $trimmedselectedcolors = trim($selectedcolors, ",");

            $colorarray = explode(",", $trimmedselectedcolors);

            $colorcount = count($colorarray);

            $specificationcount = Input::get('specificationcount');

            $selectedsizes = Input::get('si');

            $trimmedsizes = trim($selectedsizes, ",");

            $sizearray = explode(",", $trimmedsizes);

            $productsizecount = Input::get('productsizecount');

            $img_count = Input::get('count');

            $filename_new_get = "";

            for ($i = 0; $i < $img_count; $i++) {

                $file_more = Input::file('file_more' . $i);

                if ($file_more == "") {

                    $dile_name_new_get = Input::get('file_more_new' . $i);

                    $filename_new_get .= $dile_name_new_get . "/**/";

                } else {

                    $file_more_name = $file_more->getClientOriginalName();

                    $move_more_img = explode('.', $file_more_name);

                    $filename_new = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];

                    $newdestinationPath = './assets/product/';

                    $uploadSuccess_new = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);

                    $filename_new_get .= $filename_new . "/**/";

                }

            }

            $file = Input::file('file');

            if ($file == "") {

                $filename = Input::get('file_new');

            }

            else {

                $filename = $file->getClientOriginalName();

                $move_img = explode('.', $filename);

                $filename = $move_img[0] . str_random(8) . "." . $move_img[1];

                $destinationPath = './assets/deals/';

                $uploadSuccess = Input::file('file')->move($destinationPath, $filename);

            }

            echo $file_name_insert = $filename . "/**/" . $filename_new_get . "<br>" . Input::get('deal_edit_id');

            $id = Input::get('product_edit_id');

            $Product_Title = Input::get('Product_Title');

            $Product_Category = Input::get('category');

            $Product_MainCategory = Input::get('maincategory');

            $Product_SubCategory = Input::get('subcategory');

            $Product_SecondSubCategory = Input::get('secondsubcategory');

            $Original_Price = Input::get('Original_Price');

            $Discounted_Price = Input::get('Discounted_Price');

            $Shipping_Amount = Input::get('Shipping_Amount');

            $Description = Input::get('Description');

            $Delivery_Days = Input::get('Delivery_Days');

            $Delivery_Policy = Input::get('Delivery_Policy');

            $Meta_Keywords = Input::get('Meta_Keywords');

            $Meta_Description = Input::get('Meta_Description');

            $Select_Merchant = Input::get('Select_Merchant');

            $Select_Shop = Input::get('Select_Shop');

            $inc_tax = Input::get('inctax');

            $add_spec = Input::get('specification');

            $postfb = Input::get('postfb');

            $img_count = Input::get('count');

            if ($inc_tax == 1) {

            }

            else {

                $inc_tax = 0;

            }

            $entry = array(

                'pro_title' => $Product_Title,

                'pro_mc_id' => $Product_Category,

                'pro_smc_id' => $Product_MainCategory,

                'pro_sb_id' => $Product_SubCategory,

                'pro_ssb_id' => $Product_SecondSubCategory,

                'pro_price' => $Original_Price,

                'pro_disprice' => $Discounted_Price,

                'pro_inctax' => $inc_tax,

                'pro_shippamt' => $Shipping_Amount,

                'pro_desc' => $Description,

                'pro_isspec' => $add_spec,

                'pro_delivery' => $Delivery_Days,

                'pro_mr_id' => $Select_Merchant,

                'pro_sh_id' => $Select_Shop,

                'pro_mkeywords' => $Meta_Keywords,

                'pro_mdesc' => $Meta_Description,

                'pro_Img' => $file_name_insert,

                'pro_image_count' => $img_count,

                'pro_qty' => $pquantity

            );

            $return = Products::edit_product($entry, $id);

            if ($colorcount > 0) {

                for ($i = 0; $i < $colorcount; $i++) {

                    $colorentry = array(
                        'pc_pro_id' => $productid,

                        'pc_co_id' => $colorarray[$i]

                    );

                    Products::insert_product_color_details($colorentry);

                }

            }

            if ($add_spec == 1) {

                for ($i = 0; $i <= $specificationcount; $i++) {

                    if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {

                    }

                    else {

                        $specificationentry = array(
                            'spc_pro_id' => $productid,

                            'spc_sp_id' => Input::get('spec' . $i),

                            'spc_value' => Input::get('spectext' . $i)

                        );

                        Products::insert_product_specification_details($specificationentry);

                    }

                }

            }

            if ($productsizecount > 0) {

                for ($i = 0; $i < $productsizecount; $i++) {

                    $val = Input::get('sizecheckbox' . $sizearray[$i]);

                    if ($val == 1) {

                        $productsizeentry = array(
                            'ps_pro_id' => $productid,

                            'ps_si_id' => $sizearray[$i],

                            'ps_volume' => Input::get('quantity' . $sizearray[$i])

                        );

                        Products::insert_product_size_details($productsizeentry);

                    }

                    else {



                    }

                }

            }

            return Redirect::to('manage_product')->with('block_message', 'Product Updated Successfully');

        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    public function get_secmaincategory_ajax(Request $request)
    {
        $main_category = Products::load_maincategory_ajax($request->input('mc_id'));

        return $main_category;
    }

    public function get_subcategory_ajax(Request $request)
    {
        $sub_category = Products::load_subcategory_ajax($request->input('smc_id'));

        return $sub_category;
    }

    public function product_getmaincategory()
    {

        $categoryid = $_GET['id'];

        $main_category = Products::load_maincategory_ajax($categoryid);

        if ($main_category) {

            $maincategoryresult = "";

            $maincategoryresult .= "<option value='0'> -Select Main Category- </option>";

            foreach ($main_category as $main_category_ajax) {

                $maincategoryresult .= "<option value='" . $main_category_ajax->smc_id . "'> " . $main_category_ajax->smc_name . " </option>";

            }

            echo $maincategoryresult;

        }

        else {

            echo $maincategoryresult = "<option value='0'>No list available in the category </option>";

        }

    }

    public function product_getsubcategory()
    {

        $categoryid = $_GET['id'];

        $sub_category = Products::load_subcategory_ajax($categoryid);

        if ($sub_category) {

            $subcategoryresult = "";

            $subcategoryresult = "<option value='0'> -- Select sub Category-- </option>";

            foreach ($sub_category as $sub_category_ajax) {

                $subcategoryresult .= "<option value='" . $sub_category_ajax->sb_id . "'> " . $sub_category_ajax->sb_name . " </option>";

            }

            echo $subcategoryresult;

        }

        else {

            echo $subcategoryresult = "<option value='0'>No list available in the category </option>";

        }

    }



    public function product_getsecondsubcategory()
    {

        $categoryid = $_GET['id'];

        $secondsub_category = Products::get_second_sub_category_ajax($categoryid);

        if ($secondsub_category) {

            $secondsubcategoryresult = "";

            $secondsubcategoryresult = "<option value='0'> -- Select Second Sub Category-- </option>";

            foreach ($secondsub_category as $second_sub_category_ajax) {

                $secondsubcategoryresult .= "<option value='" . $second_sub_category_ajax->ssb_id . "'> " . $second_sub_category_ajax->ssb_name . " </option>";

            }

            echo $secondsubcategoryresult;

        }

        else {

            echo $secondsubcategoryresult = "<option value='0'>No list available in the category </option>";

        }

    }

    public function product_getspecification()
    {
        $categoryid = $_GET['id'];
        $specification = '';
        $specification_group = DB::table('nm_spgroup')->where('spg_smc_id', $categoryid)->first();
        $specification = DB::table('nm_specification')->where('sp_spg_id', $specification_group->spg_id)->get();
        return $specification;
        // dd($specification);
        // if ($specification!='') {
        //     $specificationresult = "";
        //     $specificationresult = "<option value='0'> -- Select Specification-- </option>";
        //     foreach ($specification as $specification0_ajax) {
        //         foreach ($specification0_ajax as $specification_ajax) {
        //             $specificationresult .= "<option value='" . $specification_ajax->sp_id . "'> " . $specification_ajax->sp_name . " </option>";
        //         }
        //     }
        //     echo $specificationresult;
        // }
        // else {
        //     echo $specificationresult = "<option value='0'>No list available</option>";
        // }
    }

    public function product_getcolor()
    {

        $colorid = $_GET['id'];

        if ($_GET['co_text_box'] == "") {

            $get_text_array = 0;

        }

        else {

            $get_text_array = trim($_GET['co_text_box'], ',');

            $result_array = explode(',', $get_text_array);

            $countval = count($result_array);

        }

        $array_push_values = array();

        for ($i = 0; $i < $countval; $i++) {

            array_push($array_push_values, $result_array[$i]);

        }

        $result_colorname = Products::get_colorname_ajax($colorid);

        foreach ($result_colorname as $result_colorname_ajax) {

            $returnvalue = $result_colorname_ajax->co_name . "," . $result_colorname_ajax->co_id . "," . $result_colorname_ajax->co_code;

        }

        if (in_array($colorid, $array_push_values)) {

            return $returnvalue . ",failed";

        }

        else {

            return $returnvalue . ",success";

        }



    }



    public function product_getcolor_edit()
    {

        $colorid = $_GET['id'];

        if ($_GET['co_text_box'] == "") {

            $get_text_array = 0;

        }

        else {

            $get_text_array = trim($_GET['co_text_box'], ',');

            $result_array = explode(',', $get_text_array);

            $countval = count($result_array);

        }

        if ($get_text_array == 0) {

            $array_push_values = array();

            for ($i = 0; $i < $countval; $i++) {

                array_push($array_push_values, $result_array[$i]);

            }

        }

        else {

            $array_push_values = array();

            for ($i = 0; $i <= $countval; $i++) {

                array_push($array_push_values, $result_array[$i]);

            }



        }

        $result_colorname = Products::get_colorname_ajax($colorid);

        foreach ($result_colorname as $result_colorname_ajax) {

            $returnvalue = $result_colorname_ajax->co_name . "," . $result_colorname_ajax->co_id . "," . $result_colorname_ajax->co_code;

        }

        if (in_array($colorid, $array_push_values)) {

            return $returnvalue . ",failed";

        }

        else {

            return $returnvalue . ",success";

        }

    }



    public function product_getsize()
    {

        $sizeid = $_GET['id'];

        if ($_GET['si_text_box'] == "") {

            $get_text_array = 0;

        }

        else {

            $get_text_array = trim($_GET['si_text_box'], ',');

            $result_array = explode(',', $get_text_array);

            $countval = count($result_array);

        }

        $array_push_values = array();

        for ($i = 0; $i < $countval; $i++) {

            array_push($array_push_values, $result_array[$i]);

        }

        $result_sizename = Products::get_sizename_ajax($sizeid);

        foreach ($result_sizename as $result_sizename_ajax) {

                     $returnvalue = $result_sizename_ajax->si_name . "," . $result_sizename_ajax->si_id . "," . $result_sizename_ajax->si_name;

        }

        if (in_array($sizeid, $array_push_values)) {

            return $returnvalue . ",failed";

        }

        else {

            return $returnvalue . ",success";

        }



    }


    public function product_edit_getmaincategory()
    {

        $id = $_GET['edit_id'];

        $main_cat = Products::get_main_category_ajax_edit($id);

        if ($main_cat) {

            $return = "";

            foreach ($main_cat as $main_cat_ajax) {

                $return = "<option value='" . $main_cat_ajax->smc_id . "' selected> " . $main_cat_ajax->smc_name . " </option>";

            }

            echo $return;

        }

        else {

            echo $return = "<option value='0'> No datas found </option>";

        }

    }



    public function product_edit_getsubcategory()
    {

        $id = $_GET['edit_sub_id'];

        $main_cat = Products::get_sub_category_ajax_edit($id);

        if ($main_cat) {

            $return = "";

            foreach ($main_cat as $main_cat_ajax) {

                $return = "<option value='" . $main_cat_ajax->sb_id . "' selected> " . $main_cat_ajax->sb_name . " </option>";

            }

            echo $return;

        }

        else {

            echo $return = "<option value='0'> No datas found </option>";

        }

    }


    public function product_getmerchantshop()
    {

        $id = $_GET['id'];

        $shop_det = Products::get_product_details_formerchant($id);

        if ($shop_det) {

            $return = "";

            foreach ($shop_det as $shop_det_ajax) {

                $return .= "<option value='" . $shop_det_ajax->stor_id . "' selected> " . $shop_det_ajax->stor_name . " </option>";

            }

            echo $return;

        }

        else {

            echo $return = "<option value='0'> No datas found </option>";

        }

    }



    public function Product_edit_getsecondsubcategory()
    {

        $id = $_GET['edit_second_sub_id'];

        $main_cat = Products::get_second_sub_category_ajax_edit($id);

        if ($main_cat) {

            $return = "";

            foreach ($main_cat as $main_cat_ajax) {

                $return = "<option value='" . $main_cat_ajax->ssb_id . "' selected> " . $main_cat_ajax->ssb_name . " </option>";

            }

            echo $return;

        }

        else {

            echo $return = "<option value='0'> No datas found </option>";

        }

    }



    public function block_product($id, $status)
    {

        if (Session::has('userid')) {
            date_default_timezone_set('Asia/Jakarta');
            $date = date('Y-m-d H:i:s');

            $entry = array(

                'pro_status' => $status

            );

            Products::block_product_status($id, $entry);

            $audit_trail = new AuditTrail;
            $audit_trail->aud_table = 'nm_product';
            $audit_trail->aud_action = 'update';
            $audit_trail->aud_detail = '{"pro_id":"'.$id.'","pro_status":"'.$status.'"}';
            $audit_trail->aud_date = $date;
            $audit_trail->aud_user_name = Session::get('username');
            $audit_trail->save();

            if ($status == 1) {
                $update_time = DB::table('nm_product')->where('pro_id', $id)->update([
                    'activated_date' => $date
                ]);
                return Redirect::to(redirect()->back()->getTargetUrl())->with('block_message', 'Product unblocked');

            }

            else if ($status == 0) {

                return Redirect::to(redirect()->back()->getTargetUrl())->with('block_message', 'Product Blocked');

            }

        }

        else {

            return Redirect::to('siteadmin');

        }

    }



    public function product_details($id)
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $return = Products::get_product_view($id);
            //dd($return);
            $get_setting_extended   = Settings::get_setting_extended_db();

            $productspecification = DB::table('nm_product')
            ->Leftjoin('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
            ->Leftjoin('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
            ->Leftjoin('nm_prospec', 'nm_prospec.spc_sp_id', '=', 'nm_specification.sp_id')
            ->where('pro_id', $id)
            ->where('spc_pro_id', $id)
            ->get();

            $get_pay        = Settings::get_pay_settings();
            $get_cur        = $get_pay[0]->ps_cursymbol;

            return view('siteadmin.product_details')
            ->with('productspecification', $productspecification)
            ->with('get_cur', $get_cur)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('product_list', $return)
            ->with('get_setting_extended', $get_setting_extended);
        }

        else {

            return Redirect::to('siteadmin');

        }

    }


    public function manage_product_shipping_details()
    {
        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $shippingdetails = Products::get_shipping_details();


            $qty = Products::get_qty_details();

            $amt = Products::get_amt_details();

            return view('siteadmin.shipping_list')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('shippingdetails', $shippingdetails)->with('qty', $qty)->with('amt', $amt);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    public function manage_cashondelivery_details()
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $coddetails = Products::get_cod_details();

            $qty = Products::get_qtycod_details();

            $amt = Products::get_amtcod_details();

            return view('siteadmin.cod_list')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('qty', $qty)->with('amt', $amt)->with('coddetail', $coddetails);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }


    public function add_estimated_zipcode()
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $zipcode = Products::get_zipcode();

            return view('siteadmin.add_estimated_zipcode')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('zipcode', $zipcode);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }



    public function add_estimated_zipcode_submit()
    {

        if (Session::has('userid')) {

            $data = Input::except(array(
                '_token'
            ));

            $rule = array(

                'zip_code' => 'required|numeric',

                'zip_code2' => 'required|numeric',

                'delivery_days' => 'required|numeric'

            );

            $validator = Validator::make($data, $rule);

            if ($validator->fails()) {

                return Redirect::to('add_estimated_zipcode')->withErrors($validator->messages())->withInput();

            }

            else {

                $check1 = Products::check_zip_code(Input::get('zip_code'));

                if ($check1) {

                    return Redirect::to('add_estimated_zipcode')->with('success', 'Start code already exist')->withInput();

                }

                else {

                    $check2 = Products::check_zip_code(Input::get('zip_code2'));

                    if ($check2) {

                        return Redirect::to('add_estimated_zipcode')->with('success', 'End code already exist')->withInput();

                    }

                    else {

                        $check3 = Products::check_zip_code_range(Input::get('zip_code'), Input::get('zip_code2'));

                        if ($check3) {

                            return Redirect::to('add_estimated_zipcode')->with('success', 'The Range Overlaps')->withInput();

                        } else {

                            $entry = array(

                                'ez_code_series' => Input::get('zip_code'),

                                'ez_code_series_end' => Input::get('zip_code2'),

                                'ez_code_days' => Input::get('delivery_days')

                            );

                            $return = Products::save_zip_code($entry);

                            return Redirect::to('estimated_zipcode')->with('success', 'Record Updated Successfully');

                        }

                    }

                }

            }

        }

        else {

            return Redirect::to('siteadmin');

        }

    }



    public function estimated_zipcode()
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $zipcode = Products::get_zipcode();

            return view('siteadmin.estimated_zipcode')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('zipcode', $zipcode);



        }

        else {

            return Redirect::to('siteadmin');

        }

    }



    public function edit_zipcode($id)
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $zipcode = Products::edit_zip_code($id);

            return view('siteadmin.edit_estimated_zipcode')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('zipcode', $zipcode);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }



    public function edit_estimated_zipcode_submit()
    {

        if (Session::has('userid')) {

            $data = Input::except(array(
                '_token'
            ));

            $rule = array(

                'zip_code' => 'required|numeric',

                'zip_code2' => 'required|numeric',

                'delivery_days' => 'required|numeric'

            );

            $validator = Validator::make($data, $rule);

            if ($validator->fails()) {

                return Redirect::to('edit_zipcode/' . Input::get('id'))->withErrors($validator->messages())->withInput();


            }

            else {

                $check = Products::check_zip_code_edit(Input::get('id'), Input::get('zip_code'));

                if ($check) {

                    return Redirect::to('edit_zipcode/' . Input::get('id'))->with('success', 'Start code already exist')->withInput();

                }

                else {

                    $check1 = Products::check_zip_code_edit(Input::get('id'), Input::get('zip_code2'));

                    if ($check1) {

                        return Redirect::to('edit_zipcode/' . Input::get('id'))->with('success', 'End code already exist')->withInput();

                    }

                    else {

                        $check2 = Products::check_zip_code_edit_range(Input::get('id'), Input::get('zip_code'), Input::get('zip_code2'));

                        if ($check2) {

                            return Redirect::to('edit_zipcode/' . Input::get('id'))->with('success', 'The Range Overlaps')->withInput();

                        }

                        else {

                            $entry = array(

                                'ez_code_series' => Input::get('zip_code'),

                                'ez_code_series_end' => Input::get('zip_code2'),

                                'ez_code_days' => Input::get('delivery_days')

                            );



                            $return = Products::update_zip_code($entry, Input::get('id'));

                            return Redirect::to('estimated_zipcode')->with('success', 'Record Updated Successfully');

                        }

                    }

                }



            }

        }

        else {

            return Redirect::to('siteadmin');

        }

    }


    public function block_zipcode($id, $status)
    {

        if (Session::has('userid')) {

            Products::block_zip_code($id, $status);

            if ($status == 1) {

                return Redirect::to('estimated_zipcode')->with('success', 'Record Activated Successfully');
            } else {

                return Redirect::to('estimated_zipcode')->with('success', 'Record Blocked Successfully');

            }

        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    public function KeluarkanProduct($id_pro, $id_group)
    {
        Products::delete_group_product($id_pro);

        return Redirect::to('halaman_product?grop='.$id_group);
    }

    public function delete_master_grouping($id_master_grouping)
    {
        Products::delete_master_group($id_master_grouping);

        return Redirect::to('add_groping_page');
    }

    public function delete_model_header($id_model_grouping)
    {
        Products::delete_model_header($id_model_grouping);

        return Redirect::to('manage_model');
    }

    public function TambahakanProduct($id_pro, $id_group)
    {
        $now = date('Y-m-d H:i:s');
        $select_sort = DB::table('nm_groping_product_detail')->where('group_detail_id_grouping', $id_group)->orderBy('group_detail_sort', 'desc')->first();
        if($select_sort){
            $sort = $select_sort->group_detail_sort + 1;
        }else {
            $sort = 1;
        }
        $data = array(
            'group_detail_id_grouping' => $id_group,
            'group_detail_id_product' => $id_pro,
            'group_detail_sort' => $sort,
            'status' => 'aktif',
            'created_date' => $now
        );

        Products::insert_group_product($data);

        return Redirect::to('halaman_product?grop='.$id_group);
    }

    public function update_payment_aktif_x(Request $request)
    {
        $id_header = $request->input('aid_header');
        $id_detail = $request->input('aid_detail');

        //dd($id_header ."|". $id_detail);

        Settings::ubah_payment_header($id_header, $id_detail);

        return Redirect::to('manage_payment_aktif_setting');
    }

    public function update_channel(Request $request)
    {
        $id_header = $request->input('aid_header');
        $id_channel = $request->input('aid_channel');

        // dd($id_header ."|". $id_channel);

        Settings::update_channel($id_header, $id_channel);

        return Redirect::to('manage_payment_aktif_setting');
    }

    public function hapus_payment_header($id)
    {
        Settings::hapus_payment_header($id);

        return Redirect::to('manage_payment_aktif_setting');
    }

    public function add_grouping_master(Request $request)
    {
        //dd($request->input('nama_group'));

        $nama_group = $request->input('nama_group');
        $data = array(
            'group_nama' => $nama_group
            );

        Products::insert_grouping($data);

        return Redirect::to('add_groping_page');
    }

    public function add_header_payment(Request $request)
    {
        $nama_header = $request->input('nama_header');

        $data = array(
            'nama_pay_header' => $nama_header
            );

        Settings::insert_header_pay($data);

        return Redirect::to('manage_payment_aktif_setting');
    }

    public function add_grouping_model_header(Request $request)
    {
        //dd($request->input('nama_group'));

        $nama_group = $request->input('nama_group');
        $data = array(
            'nama_grouping_model' => $nama_group
            );

        Products::insert_grouping_model_header($data);

        return Redirect::to('manage_model');
    }

    public function add_groping_page()
    {
        if (Session::has('userid'))
        {
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $list_group = Products::get_list_group();

            return view('siteadmin.add_groping_page')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('list_group', $list_group)
            ->with('adminfooter', $adminfooter);
        }
        else {

            return Redirect::to('siteadmin');

        }
    }

    public function edit_grouping_page($id)
    {
        if(Session::has('userid'))
        {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
        $group_detail = Products::grouppage_detail($id);
        return view('siteadmin.edit_grouping_page')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('group_detail', $group_detail);
        }
        else
        {
        return Redirect::to('siteadmin');
        }
    }

    public function edit_grouping_submit(){
        if(Session::has('userid'))
      {
        $data = Input::except(array('_token')) ;
        $id = Input::get('group_id');
        $rule = array(
        'group_id' => 'required'
      );
       $validator = Validator::make($data,$rule);
        if ($validator->fails())
        {
           return Redirect::to('edit_grouping_page/'.$id)->withErrors($validator->messages())->withInput();
        }
         else
        {
          $inputs = Input::all();
          $entry = array(
          'group_nama' => Input::get('group_nama')
          );
        }
          $return = Products::update_grouppage($entry,$id);
          return Redirect::to('add_groping_page')->with('success','Record Updated Successfully');
        }
      else
      {
         return Redirect::to('siteadmin');
      }
    }


    public function manage_payment_aktif_setting()
    {
        if(Session::has('userid'))
        {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $list_payment_header = Settings::list_payment_header();
            //dd($list_payment_header);
            $list_payment_details = Settings::list_payment_details();
            $list_customer_group = DB::table('nm_customer_group')->get();

            return view('siteadmin.manage_payment_aktif')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('list_payment_header', $list_payment_header)
            ->with('list_payment_details', $list_payment_details)
            ->with('list_customer_group', $list_customer_group)
            ->with('adminfooter', $adminfooter);
        }
        else
        {
            return Redirect::to('siteadmin');
        }
    }

    public function update_payment_customer_group(Request $request)
    {
        $update = DB::table('nm_payment_method_header')
        ->where('id_pay_header', $request->input('id_pay_header'))
        ->update(['cus_group_id'=>$request->input('cus_group_id')]);

        return $update;
    }

    public function manage_model()
    {
        if (Session::has('userid'))
        {
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $list_model = Products::list_model();

            return view('siteadmin.manage_model')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('list_model', $list_model)
            ->with('adminfooter', $adminfooter);
        }
        else {

            return Redirect::to('siteadmin');

        }
    }

    public function add_manage_model($id_model_header)
    {
        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $all_product = Products::get_product_details_manage();
            //dd($all_product);

            $details = Products::get_product_details_manage_spec_grop2($id_model_header);
            //dd($details);

            $detail_header = Products::get_detail_header($id_model_header);
            //dd($detail_header);

            return view('siteadmin.manage_model_detail')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('all_product', $all_product)
            ->with('detail_header', $detail_header)
            ->with('details', $details);
        }

        else {

            return Redirect::to('siteadmin');

        }
    }

    public function sort_product($id_pro, $id_group)
    {
        // dd('test');
        $sort = Input::get('sort');
        $update_sort = Products::update_sort($sort, $id_pro, $id_group);

        return Redirect::to('halaman_product?grop='.$id_group);
    }

    public function halaman_product()
    {
        if (Session::has('userid')) {
            $from_date      = Input::get('from_date');
            $to_date        = Input::get('to_date');
            $id_group       = Input::get('grop');

            if($id_group == null)
            {
                $set = Products::get_last_id_group();
                $id_group = $set->group_id;
            }

            $productrep = Products::get_productreports($from_date, $to_date);
            $groping_main = Home::get_main_group_product();
            //dd($groping_main);

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $details_all = Products::get_product_details_manage();
            $details = Products::get_product_details_manage_spec_grop($id_group);
            $detail_ket_group = Products::get_detail_ket_group($id_group);

            $delete_product = Products::get_order_details();


            return view('siteadmin.halaman_product')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('product_details', $details)
            ->with('productrep', $productrep)
            ->with('delete_product', $delete_product)
            ->with('id_group', $id_group)
            ->with('product_details_all', $details_all)
            ->with('detail_ket_group',$detail_ket_group)
            ->with('groping_main',$groping_main);
        }

        else {

            return Redirect::to('siteadmin');

        }
    }

    // public function spec_halaman_product($id)
    // {
    //     if (Session::has('userid')) {

    //         $from_date      = Input::get('from_date');
    //         $to_date        = Input::get('to_date');
    //         $productrep     = Products::get_productreports($from_date, $to_date);
    //         $id_grouping    = Input::get('id');
    //         //dd($productrep);
    //         //master groping
    //         $groping_main = Home::get_main_group_product();
    //         //dd($groping_main);

    //         $adminheader = view('siteadmin.includes.admin_header')->with("routemenu", "products");

    //         $adminleftmenus = view('siteadmin.includes.admin_left_menu_product');

    //         $adminfooter = view('siteadmin.includes.admin_footer');

    //         $details = Products::get_product_details_manage_spec_grop($id);
    //         //dd($details);

    //         $delete_product = Products::get_order_details();

    //         ;
    //         return view('siteadmin.halaman_product')
    //         ->with('adminheader', $adminheader)
    //         ->with('adminleftmenus', $adminleftmenus)
    //         ->with('adminfooter', $adminfooter)
    //         ->with('product_details', $details)
    //         ->with('productrep', $productrep)
    //         ->with('delete_product', $delete_product)
    //         ->with('groping_main',$groping_main);
    //     }

    //     else {

    //         return Redirect::to('siteadmin');

    //     }
    // }

    //Product Review
    public function manage_product_pic(){
        if (Session::has('userid')) {
            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d', $to_date);
            }
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}
            $merchant_search = Input::get('merchant_search');
            // $productrep = Products::get_productreports($from_date, $to_date);
            $get_merchant_id = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
            $kuku_merchant_id = $get_merchant_id->mer_id;
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $details = DB::table('nm_product')
            ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            ->where('pro_mr_id', $kuku_merchant_id)
            ->where('pro_qty', '<>', 0)
            ->where(function($q){
                $q = $q->where('pro_title', '')
                ->orWhere('pro_weight', 0)
                ->orWhere('pro_length', 0)
                ->orWhere('pro_width', 0)
                ->orWhere('pro_height', 0)
                ->orWhere('pro_mc_id', 0)
                ->orWhere('pro_smc_id', 0)
                ->orWhere('pro_sb_id', 0)
                ->orWhere('pro_ssb_id', 0)
                ->orWhere('pro_desc', '')
                ->orWhere('pro_mr_id', 0)
                ->orWhere('pro_sh_id', 0)
                ->orWhere('pro_mtitle', '')
                ->orWhere('pro_mkeywords', '')
                ->orWhere('pro_mdesc', '')
                ->orWhere('pro_Img', '/**/')
                ->orWhere('pro_grade_id', 0)
                ->orWhere('pro_color_id', 0);
            })
            ->get();
            // dd($details);






            $details_mer = $get_product = DB::table('nm_product')
            ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
            ->where('pro_mr_id', $kuku_merchant_id)
            ->where('pro_qty', '<>', 0)
            ->where('pro_price', 0)
            ->get();


			$delete_product = Products::get_order_details();

            // if ($from_date != '' & $to_date == '') {
            //     if($merchant_search=='kukuruyuk'){
            //         // dd('test1');
            //         $productrep = DB::table('nm_product')
            //         ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            //         ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
            //         ->where('nm_product.created_date', '>=', $from_date)
            //         ->where('nm_product.pro_status', '=', 1)
            //         ->where('pro_mr_id', $kuku_merchant_id)
            //         ->orderBy('nm_product.pro_id', 'DESC')
            //         ->get();
            //     }
            //     elseif ($merchant_search=='merchant') {
            //         // dd('test2');
            //         $productrep = DB::table('nm_product')
            //         ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            //         ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
            //         ->where('nm_product.created_date', '>=', $from_date)
            //         ->where('nm_product.pro_status', '=', 1)
            //         ->where('nm_product.pro_isapproved', 1)
            //         ->where('pro_mr_id', '<>', $kuku_merchant_id)
            //         ->orderBy('nm_product.pro_id', 'DESC')
            //         ->get();
            //     }
            // }
            //else
            if ($from_date != '' && $to_date != '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', array($from_date, $to_date))
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where(function($q){
                    $q = $q->where('pro_title', '')
                    ->orWhere('pro_weight', 0)
                    ->orWhere('pro_length', 0)
                    ->orWhere('pro_width', 0)
                    ->orWhere('pro_height', 0)
                    ->orWhere('pro_mc_id', 0)
                    ->orWhere('pro_smc_id', 0)
                    ->orWhere('pro_sb_id', 0)
                    ->orWhere('pro_ssb_id', 0)
                    ->orWhere('pro_desc', '')
                    ->orWhere('pro_mr_id', 0)
                    ->orWhere('pro_sh_id', 0)
                    ->orWhere('pro_mtitle', '')
                    ->orWhere('pro_mkeywords', '')
                    ->orWhere('pro_mdesc', '')
                    ->orWhere('pro_Img', '/**/')
                    ->orWhere('pro_grade_id', 0)
                    ->orWhere('pro_color_id', 0);
                })
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', array($from_date, $to_date))
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where('pro_price', 0)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();
            }
            elseif ($from_date != '' && $to_date == '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', '>=', $from_date)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where(function($q){
                    $q = $q->where('pro_title', '')
                    ->orWhere('pro_weight', 0)
                    ->orWhere('pro_length', 0)
                    ->orWhere('pro_width', 0)
                    ->orWhere('pro_height', 0)
                    ->orWhere('pro_mc_id', 0)
                    ->orWhere('pro_smc_id', 0)
                    ->orWhere('pro_sb_id', 0)
                    ->orWhere('pro_ssb_id', 0)
                    ->orWhere('pro_desc', '')
                    ->orWhere('pro_mr_id', 0)
                    ->orWhere('pro_sh_id', 0)
                    ->orWhere('pro_mtitle', '')
                    ->orWhere('pro_mkeywords', '')
                    ->orWhere('pro_mdesc', '')
                    ->orWhere('pro_Img', '/**/')
                    ->orWhere('pro_grade_id', 0)
                    ->orWhere('pro_color_id', 0);
                })
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', '>=', $from_date)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where('pro_price', 0)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();
            }
            elseif ($from_date == '' && $to_date != '') {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', '<=', $to_date)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where(function($q){
                    $q = $q->where('pro_title', '')
                    ->orWhere('pro_weight', 0)
                    ->orWhere('pro_length', 0)
                    ->orWhere('pro_width', 0)
                    ->orWhere('pro_height', 0)
                    ->orWhere('pro_mc_id', 0)
                    ->orWhere('pro_smc_id', 0)
                    ->orWhere('pro_sb_id', 0)
                    ->orWhere('pro_ssb_id', 0)
                    ->orWhere('pro_desc', '')
                    ->orWhere('pro_mr_id', 0)
                    ->orWhere('pro_sh_id', 0)
                    ->orWhere('pro_mtitle', '')
                    ->orWhere('pro_mkeywords', '')
                    ->orWhere('pro_mdesc', '')
                    ->orWhere('pro_Img', '/**/')
                    ->orWhere('pro_grade_id', 0)
                    ->orWhere('pro_color_id', 0);
                })
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->whereBetween('nm_product.created_date', '<=', $to_date)
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where('pro_price', 0)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();
            }
            else {
                $productrep = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where(function($q){
                    $q = $q->where('pro_title', '')
                    ->orWhere('pro_weight', 0)
                    ->orWhere('pro_length', 0)
                    ->orWhere('pro_width', 0)
                    ->orWhere('pro_height', 0)
                    ->orWhere('pro_mc_id', 0)
                    ->orWhere('pro_smc_id', 0)
                    ->orWhere('pro_sb_id', 0)
                    ->orWhere('pro_ssb_id', 0)
                    ->orWhere('pro_desc', '')
                    ->orWhere('pro_mr_id', 0)
                    ->orWhere('pro_sh_id', 0)
                    ->orWhere('pro_mtitle', '')
                    ->orWhere('pro_mkeywords', '')
                    ->orWhere('pro_mdesc', '')
                    ->orWhere('pro_Img', '/**/')
                    ->orWhere('pro_grade_id', 0)
                    ->orWhere('pro_color_id', 0);
                })
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();

                $productrep_mer = DB::table('nm_product')
                ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
                ->where('pro_mr_id', $kuku_merchant_id)
                ->where('pro_qty', '<>', 0)
                ->where('pro_price', 0)
                ->orderBy('nm_product.pro_id', 'DESC')
                ->get();
            }
            // dd($productrep);


            return view('siteadmin.manage_product_pic')
            ->with('mer_product_details',$details_mer)
            ->with('kuku_merchant_id', $kuku_merchant_id)
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('product_details', $details)
            ->with('productrep', $productrep)
            ->with('productrep_mer', $productrep_mer)
            ->with('delete_product', $delete_product)
            ->with('privileges', $privileges);

        }
    }
    public function manage_review()
    {
        if (Session::has('userid')) {
            Session::put('blogcmtcount', 0);
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $read_review = Products::change_read_status();
            $get_review = Products::get_product_review();

             return view('siteadmin.manage_review')
             ->with('adminheader', $adminheader)
             ->with('adminleftmenus', $adminleftmenus)
             ->with('adminfooter', $adminfooter)
             ->with('get_review', $get_review);
        }
        else{
            return Redirect::to('siteadmin');
        }
    }
     public function edit_review($id)
    {

        if (Session::has('userid')) {

            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $result = Products::edit_review($id);

            return view('siteadmin.edit_review')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('result',$result);

        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    public function edit_review_submit()
    {
        if (Session::has('userid')) {
            $now = date('Y-m-d H:i:s');

            $inputs = Input::all();
            $review_id = Input::get('comment_id');
            $review_title = Input::get('review_title');
            $review_comment = Input::get('review_comment');

            $entry = array(

                'title' => $review_title,

                'comments' => $review_comment,

            );
            $return = Products::update_review($entry, $review_id);
            return Redirect::to('manage_review');
        }
        else{
            return Redirect::to('siteadmin');
        }
    }
    public function delete_review($id)
    {
        if(Session::has('userid'))
        {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
         $del_review = Products::delete_review($id);
         return Redirect::to('manage_review')->with('product Deleted','Review Deleted Successfully');
        }
        else
        {
         return Redirect::to('siteadmin');
        }
    }
        public function block_review($id, $status)
    {

        if (Session::has('userid')) {

            $entry = array(

                'status' => $status

            );

            Products::block_review_status($id, $entry);

            if ($status == 0) {

                return Redirect::to('manage_review')->with('block_message', 'Product unblocked');

            }

            else if ($status == 1) {

                return Redirect::to('manage_review')->with('block_message', 'Product Blocked');

            }

        }

        else {

            return Redirect::to('siteadmin');

        }

    }

    // Fungsi API Create Product
    public function apicreateproduct(Request $request)
	{
        if($request->input('pro_sku')==null||$request->input('pro_title')==null||$request->input('mc_code')==null){
            return response ('Data Harus Diisi Dengan Benar', 400);
            die();
        }

        //ambil ambil

        //mc code
        $mc_code = $request->input('mc_code');
        $nMaincategory = DB::table('nm_maincategory')->where('mc_code','=', $mc_code)->first();
        if($nMaincategory==null){
            return response ('Main Category Tidak Ditemukan', 400);
            die();
        }

        //smc code
        $smc_code = $request->input('smc_code');
        $nSecmaincategory = DB::table('nm_secmaincategory')->where('smc_code','=', $smc_code)->first();
        if($nSecmaincategory==null){
            return response ('Second Main Category Tidak Ditemukan', 400);
            die();
        }

        //sb code
        $sb_code = $request->input('sb_code');
        $nSubcategory = DB::table('nm_subcategory')->where('sb_code','=', $sb_code)->first();
        if($nSubcategory==null){
            return response ('Sub Category Tidak Ditemukan', 400);
            die();
        }

        //ssb code
        $ssb_code = $request->input('ssb_code');
        $seccsubcategory = DB::table('nm_secsubcategory')->where('ssb_code','=', $ssb_code)->first();
        if($seccsubcategory==null){
            return response ('Second Sub Category Tidak Ditemukan', 400);
            die();
        }
        $get_stor_id = DB::table('nm_store')->select('stor_id')->where('stor_name', 'Kukuruyuk')->first();
        $get_mer_id = DB::table('nm_merchant')->select('mer_id')->where('mer_fname', 'Kukuruyuk')->first();

        if($get_stor_id==null
        || $get_mer_id==null)
        {
            return response('Default Merchant Atau Default Store Belum Dimasukan', 400);
            die();
        }
        $get_grade = DB::table('nm_grade')->where('grade_code', $request->input('pro_grade'))->first();
        $get_color = DB::table('nm_color')->where('co_code', $request->input('pro_color'))->first();

        if($get_grade==null)
        {
            return response('Grade Tidak Ditemukan', 400);
            die();
        }
        if($get_color==null)
        {
            return response('Warna Tidak Ditemukan', 400);
            die();
        }

        $pro_sku = $request->input('pro_sku');
		$pro_title = $request->input('pro_title');
        $pro_grade = $get_grade->grade_id;
        $pro_color = $get_color->co_id;
        $pro_mc_id = $nMaincategory->mc_id;
        $pro_smc_id = $nSecmaincategory->smc_id;
        $pro_sb_id = $nSubcategory->sb_id;
		$pro_ssb_id = $seccsubcategory->ssb_id;
        $pro_sh_id = $get_stor_id->stor_id;
        $pro_mer_id = $get_mer_id->mer_id;
        if($pro_grade==null)
        {
            $pro_grade = 0;
        }
        if($request->input('pro_length')==null)
        {
            $pro_length = '';
        }else {
            $pro_length = $request->input('pro_length');
        }
        if($request->input('pro_width')==null)
        {
            $pro_width = '';
        }else {
            $pro_width = $request->input('pro_width');
        }
        if($request->input('pro_height')==null)
        {
            $pro_height = '';
        }else {
            $pro_height = $request->input('pro_height');
        }
        if($request->input('pro_weight')==null) {
            $pro_weight = '';
        }else {
            $pro_weight = $request->input('pro_weight');
        }
        if($request->input('pro_status')==null)
        {
            $pro_status = 1;
        }elseif($request->input('pro_status')==1 || $request->input('pro_status')==0){
            $pro_status = $request->input('pro_status');
        }

        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d');
        $entry = [];
        $entry['pro_sku'] = $pro_sku;
        $entry['pro_title'] = $pro_title;
        $entry['pro_mc_id'] = $pro_mc_id;
        $entry['pro_smc_id'] = $pro_smc_id;
        $entry['pro_sb_id'] = $pro_sb_id;
        $entry['pro_ssb_id'] = $pro_ssb_id;
        $entry['pro_sh_id'] = $pro_sh_id;
        $entry['pro_mr_id'] = $pro_mer_id;
        $entry['pro_status'] = $pro_status;
        $entry['pro_length'] = $pro_length;
        $entry['pro_width'] = $pro_width;
        $entry['pro_height'] = $pro_height;
        $entry['pro_weight'] = $pro_weight;
        $entry['pro_grade_id'] = $pro_grade;
        $entry['pro_color_id'] = $pro_color;
        $entry['created_date'] = $now;
        //cek SKU di Storefront
        $cek_sku = DB::table('nm_product')->where('pro_sku','=', $pro_sku)->first();

        if($cek_sku==null){
            Products::insert_product($entry);
        }
        else
        {
            $get_pro_id = Products::get_product_id($pro_sku);
			$row = Products::find($get_pro_id->pro_id);
            $row->pro_title = $pro_title;
            $row->pro_mc_id = $pro_mc_id;
            $row->pro_smc_id = $pro_smc_id;
            $row->pro_sb_id = $pro_sb_id;
            $row->pro_ssb_id = $pro_ssb_id;
            $row->pro_length = $pro_length;
            $row->pro_width = $pro_width;
            $row->pro_height = $pro_height;
            $row->pro_weight = $pro_weight;
            $row->pro_grade_id = $pro_grade;
            $row->save();
        }

        return response('Sukses', 200);
	}

    public function apiUpdateStock(Request $request)
    {
        if($request->input('items')==null)
        {
            return response('Required Parameter Tidak Boleh Kosong', 400);
            die();
        }
        $items = $request->input('items');
        $error = [];
        foreach($items as $item)
        {
            $get_pro_id = Products::get_product_id($item['pro_sku']);

            if($get_pro_id!=null){
                if($get_pro_id->pro_no_of_purchase < $item['pro_qty']){
                    $sold_status = 1;
                }elseif ($get_pro_id->pro_no_of_purchase >= $item['pro_qty']) {
                    $sold_status = 0;
                }

            }

            if($get_pro_id==null)
            {
                array_push($error, 'sku : '.$item['pro_sku'].'; message : SKU Not Found');
            }else {
                $row = Products::find($get_pro_id->pro_id);
                $row->pro_qty = $item['pro_qty'];
                $row->sold_status = $sold_status;
                $row->save();
            }
        }

        return response()->json(['errors'=>$error]);
    }

    public function apiCreateColor(Request $request)
    {
        $check_exist_color = DB::table('nm_color')->where('co_code', $request->input('co_code'))->first();

        if($request->input('co_code')==null
        || $request->input('co_name')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($check_exist_color!=null)
        {
            if($request->input('co_code')==null
            || $request->input('co_name')==null)
            {
                return response('Required Parameter Is Null', 400);
                die();
            }
            if($request->input('co_name')!=null)
                $name = DB::table('nm_color')->where('co_code', $request->input('co_code'))->update(['co_name' => $request->input('co_name')]);

            return response('Sukses', 200);
        }

        $createColor = DB::table('nm_color')->insert([
            'co_code' => $request->input('co_code'),
            'co_name' => $request->input('co_name')
        ]);

        return response('Sukses', 200);
    }

    public function apiUpdateColor(Request $request)
    {
        $check_exist_color = DB::table('nm_color')->where('co_code', $request->input('co_code'))->first();

        if($request->input('co_code')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($check_exist_color==null)
        {
            return response('Color Code Not Found', 400);
            die();
        }
        if($request->input('co_name')!=null)
            $name = DB::table('nm_color')->where('co_code', $request->input('co_code'))->update(['co_name' => $request->input('co_name')]);

        return response('Sukses', 200);
    }

    public function apiDeleteColor(Request $request)
    {
        $check_exist_color = DB::table('nm_color')->where('co_code', $request->input('co_code'))->first();

        if($request->input('co_code')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($check_exist_color==null)
        {
            return response('Color Code Not Found', 400);
            die();
        }
        $deleteAtProcolor = DB::table('nm_procolor')->where('pc_co_id', $check_exist_color->co_id)->update(['pc_co_id'=>'']);
        $delete = DB::table('nm_color')->where('co_code', $request->input('co_code'))->delete();

        return response('Sukses', 200);
    }

	public function apiGetImage(Request $request)
    {
        $prod = DB::table('nm_product')->where('pro_sku', $request->input('sku'))->first();
		if($prod != null){
			$imgs = explode('/**/', $prod->pro_Img);
			foreach($imgs as $img){
				if(!empty($img))return url('assets/product/'.$img);
			}
		}
		return '';
    }

    public function api_grade(Request $request)
    {
        $grade_name = $request->input('name');
        $grade_code = $request->input('code');

        if(!$grade_code){
            return response('Required Parameter Is Null', 400);
            die();
        }
        $select = DB::table('nm_grade')->where('grade_code', $grade_code)->first();
        if($select){ //update
            if($grade_name){
                $update = DB::table('nm_grade')->where('grade_code', $grade_code)->update([
                    'grade_name' => $grade_name
                ]);
            }
            return response('Sukses', 200);
            die();
        }
        // insert
        if(!$grade_name){
            $grade_name = '';
        }
        $insert = DB::table('nm_grade')->insert([
            'grade_name' => $grade_name,
            'grade_code' => $grade_code
        ]);
        return response('Sukses', 200);
    }

    public function update_warranty(Request $request)
    {
        $id = $_GET['id'];
        $warranty = $_GET['warranty'];

        $update_warranty_db = Products::update_warranty_db($id, $warranty);
    }

    public function upload_excel_product()
    {
        if (Session::has('userid')) {
            $include = self::view_include('products', 'admin_left_menu_product');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $data_merchant = Merchant::view_merchant_details();
            $data_maincategory = Products::get_product_category();
            $data_user = Users::get();

            return view('siteadmin.upload_excel_product')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('data_merchant', $data_merchant)
            ->with('data_maincategory', $data_maincategory)
            ->with('data_user', $data_user)
            ->with('privileges', $include['privileges']);
        }else {
            return Redirect::to('siteadmin');
        }
    }

    public function upload_excel_product_process(Request $request)
    {
        if (Session::has('userid')) {
            //get privileges
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}
            //end get privileges
            //check privilege
            $content_priv = 0;
            $price_priv = 0;
            foreach ($privileges as $priv) {
                $priv_name = explode(' ', $priv['rp_priv_name']);
                if ($priv_name[3] == 'content' && $priv_name[4] == 'edit_product') {
                    $content_priv = 1;
                }
                if ($priv_name[3] == 'price' && $priv_name[4] == 'edit_product') {
                    $price_priv = 1;
                }
            }
            //end check privileges

            $file = Input::file('file');
            if($file){
                $errors = 0;
                $total_empty = 0;
                $success = 0;
                $totalRows = 0;
                $messages = [];
                $excel = Excel::selectSheetsByIndex(0)->load($file, function($reader) use(&$content_priv, &$price_priv, &$errors, &$totalRows, &$messages, &$total_empty, &$success){
                    $rows = $reader->all();
                    $header = $reader->get()->first()->toArray();
                    $totalRows = count($rows);
                    date_default_timezone_set('Asia/Jakarta');
                    $now = date('Y-m-d H:i:s');
                    // execute
                    for($line=0;$line<$totalRows;$line++)
                    {
                        $row = $rows[$line];
                        if(
                            $row->sku == null &&
                            $row->name == null &&
                            $row->description == null &&
                            $row->meta_title == null &&
                            $row->meta_keyword == null &&
                            $row->meta_description == null &&
                            $row->price == null &&
                            $row->discounted_price == null &&
                            $row->status == null &&
                            $row->extended_warranty == null
                        ){
                            $total_empty++;
                            continue;
                        }
                        //check sku if empty
                        if($row->sku == null){
                            $errors++;
                            $message = [
                                'sku' => $row->sku,
                                'message' => 'SKU wajib diisi'
                            ];
                            array_push($messages, $message);
                            continue;
                        }else {
                            $check_sku = Products::get_product_id($row->sku);

                            if ($content_priv == 0 && $price_priv == 0) {
                                $errors++;
                                $message = [
                                    'sku' => $row->sku,
                                    'message' => 'Unauthorized'
                                ];
                                array_push($messages, $message);
                                continue;
                            }
                            //check sku if exist
                            if(!$check_sku){
                                $errors++;
                                $message = [
                                    'sku' => $row->sku,
                                    'message' => 'SKU tidak ditemukan'
                                ];
                                array_push($messages, $message);
                                // $messages = '{sku:"'.$row->sku.'",message:"SKU tidak ditemukan"},';
                            }else {
                                $get_spgroup = Products::get_spgroup_with_smc($check_sku->pro_smc_id);
                                $get_smc = Products::get_main_category_ajax_edit($check_sku->pro_smc_id);
                                //check if sku have category
                                // dd($get_smc);
                                if(!$get_smc){
                                    $errors++;
                                    $message = [
                                        'sku' => $row->sku,
                                        'message' => 'SKU tidak mempunyai category'
                                    ];
                                    array_push($messages, $message);
                                    continue;
                                }
                                $product_id = DB::table('nm_product')->select('pro_id')->where('pro_sku', $row->sku)->first();

                                if ($content_priv == 1) {
                                    //update product
                                    $entry = array();
                                    if ($row->name != null) {
                                        $entry['pro_title'] = $row->name;
                                    }
                                    if ($row->description != null) {
                                        $entry['pro_desc'] = $row->description;
                                    }
                                    if ($row->meta_title != null) {
                                        $entry['pro_mtitle'] = $row->meta_title;
                                    }
                                    if ($row->meta_keyword != null) {
                                        $entry['pro_mkeywords'] = $row->meta_keyword;
                                    }
                                    if ($row->meta_description != null) {
                                        $entry['pro_mdesc'] = $row->meta_description;
                                    }
                                    if ($row->extended_warranty == 0 || $row->extended_warranty == 1) {
                                        $entry['have_warranty'] = $row->extended_warranty;
                                    }
                                    $update = DB::table('nm_product')->where('pro_sku', $row->sku)->update($entry);

                                    $entry_json = $entry;
                                    $entry_json['pro_id'] = $product_id->pro_id;
                                    $entry_json['pro_sku'] = $row->sku;
                                    $aud_detail = json_encode($entry_json);
                                    $audit_trail = new AuditTrail;
                                    $audit_trail->aud_table = 'nm_product';
                                    $audit_trail->aud_action = 'update';
                                    $audit_trail->aud_detail = $aud_detail;
                                    $audit_trail->aud_date = $now;
                                    $audit_trail->aud_user_name = Session::get('username');
                                    $audit_trail->save();
                                    //end update product

                                    //update specification
                                    if ($row->spec_name_1 != null) {
                                        for($spec_column=1;$spec_column<999;$spec_column++)
                                        {
                                            if(array_key_exists('spec_name_'.$spec_column, $header)){
                                                $spec_name = 'spec_name_'.$spec_column;
                                                $spec_value = 'spec_value_'.$spec_column;
                                                if(!$get_spgroup){
                                                    $entry_spgroup = [
                                                        'spg_name' => $get_smc[0]->smc_name,
                                                        'spg_smc_id' => $check_sku->pro_smc_id
                                                    ];
                                                    $create_spgroup = Specification::save_specification_group($entry_spgroup);

                                                    $entry_json = $entry_group;
                                                    $aud_detail = json_encode($entry_json);
                                                    $audit_trail = new AuditTrail;
                                                    $audit_trail->aud_table = 'nm_spgroup';
                                                    $audit_trail->aud_action = 'create';
                                                    $audit_trail->aud_detail = $aud_detail;
                                                    $audit_trail->aud_date = $now;
                                                    $audit_trail->aud_user_name = Session::get('username');
                                                    $audit_trail->save();
                                                }
                                                $get_spgroup = Products::get_spgroup_with_smc($check_sku->pro_smc_id);
                                                $get_spec = Specification::get_specification_by_name($row->$spec_name, $get_spgroup->spg_id);
                                                if(!$get_spec){
                                                    $check_spec_order = Specification::check_last_order_spec($get_spgroup->spg_id);
                                                    $sp_order = $check_spec_order->sp_order + 1;
                                                    $entry_spec = [
                                                        'sp_name' => $row->$spec_name,
                                                        'sp_spg_id' => $get_spgroup->spg_id,
                                                        'sp_order' => $sp_order
                                                    ];
                                                    $create_spec = Specification::save_specification_detail($entry_spec);

                                                    $entry_json = $entry_spec;
                                                    $aud_detail = json_encode($entry_json);
                                                    $audit_trail = new AuditTrail;
                                                    $audit_trail->aud_table = 'nm_specification';
                                                    $audit_trail->aud_action = 'create';
                                                    $audit_trail->aud_detail = $aud_detail;
                                                    $audit_trail->aud_date = $now;
                                                    $audit_trail->aud_user_name = Session::get('username');
                                                    $audit_trail->save();
                                                }
                                                $get_spec = Specification::get_specification_by_name($row->$spec_name, $get_spgroup->spg_id);
                                                $entry_prospec = [
                                                    'spc_pro_id' => $check_sku->pro_id,
                                                    'spc_sp_id' => $get_spec->sp_id,
                                                    'spc_value' => $row->$spec_value
                                                ];
                                                $get_prospec = Products::get_prospec($check_sku->pro_id, $get_spec->sp_id);
                                                if($get_prospec)
                                                {
                                                    $update_prospec = Products::update_prospec($entry_prospec);

                                                    $entry_json = $entry_prospec;
                                                    $aud_detail = json_encode($entry_json);
                                                    $audit_trail = new AuditTrail;
                                                    $audit_trail->aud_table = 'nm_prospec';
                                                    $audit_trail->aud_action = 'update';
                                                    $audit_trail->aud_detail = $aud_detail;
                                                    $audit_trail->aud_date = $now;
                                                    $audit_trail->aud_user_name = Session::get('username');
                                                    $audit_trail->save();
                                                }else {
                                                    $create_prospec = Products::insert_product_specification_details($entry_prospec);

                                                    $entry_json = $entry_prospec;
                                                    $aud_detail = json_encode($entry_json);
                                                    $audit_trail = new AuditTrail;
                                                    $audit_trail->aud_table = 'nm_prospec';
                                                    $audit_trail->aud_action = 'create';
                                                    $audit_trail->aud_detail = $aud_detail;
                                                    $audit_trail->aud_date = $now;
                                                    $audit_trail->aud_user_name = Session::get('username');
                                                    $audit_trail->save();
                                                }
                                            }else {
                                                break;
                                            }
                                        }
                                    }
                                    //end update specification
                                }
                                if ($price_priv == 1) {
                                    //update product
                                    $entry = [
                                        'pro_price' => $row->price,
                                        'pro_disprice' => $row->discounted_price,
                                        'wholesale_level1_min' => $row->wholesale_level1_min,
                                        'wholesale_level1_max' => $row->wholesale_level1_max,
                                        'wholesale_level1_price' => $row->wholesale_level1_price,
                                        'wholesale_level2_min' => $row->wholesale_level2_min,
                                        'wholesale_level2_max' => $row->wholesale_level2_max,
                                        'wholesale_level2_price' => $row->wholesale_level2_price,
                                        'wholesale_level3_min' => $row->wholesale_level3_min,
                                        'wholesale_level3_max' => $row->wholesale_level3_max,
                                        'wholesale_level3_price' => $row->wholesale_level3_price,
                                        'wholesale_level4_min' => $row->wholesale_level4_min,
                                        'wholesale_level4_max' => $row->wholesale_level4_max,
                                        'wholesale_level4_price' => $row->wholesale_level4_price,
                                        'wholesale_level5_min' => $row->wholesale_level5_min,
                                        'wholesale_level5_max' => $row->wholesale_level5_max,
                                        'wholesale_level5_price' => $row->wholesale_level5_price,
                                    ];
                                    $update_price = DB::table('nm_product')->where('pro_sku', $row->sku)->update($entry);

                                    $entry_json = $entry;
                                    $entry_json['pro_id'] = $product_id->pro_id;
                                    $entry_json['pro_sku'] = $row->sku;
                                    $aud_detail = json_encode($entry_json);
                                    $audit_trail = new AuditTrail;
                                    $audit_trail->aud_table = 'nm_product';
                                    $audit_trail->aud_action = 'update';
                                    $audit_trail->aud_detail = $aud_detail;
                                    $audit_trail->aud_date = $now;
                                    $audit_trail->aud_user_name = Session::get('username');
                                    $audit_trail->save();
                                    //end update product
                                }
                                //update status product
                                date_default_timezone_set('Asia/Jakarta');
                                $now = date('Y-m-d H:i:s');
                                $get_product = DB::table('nm_product')->where('pro_sku', $row->sku)->first();
                                if (!$get_product->pro_title
                                || $get_product->pro_weight == 0
                                || $get_product->pro_length == 0
                                || $get_product->pro_width == 0
                                || $get_product->pro_height == 0
                                || $get_product->pro_mc_id == 0
                                || $get_product->pro_smc_id == 0
                                || $get_product->pro_sb_id == 0
                                || $get_product->pro_ssb_id == 0
                                || $get_product->pro_qty == 0
                                || $get_product->pro_price == 0
                                || !$get_product->pro_desc
                                || $get_product->pro_mr_id == 0
                                || $get_product->pro_sh_id == 0
                                || !$get_product->pro_mtitle
                                || !$get_product->pro_mkeywords
                                || !$get_product->pro_mdesc
                                || $get_product->pro_Img == '/**/'
                                || $get_product->pro_grade_id == 0
                                || $get_product->pro_color_id == 0) {
                                    $input = [
                                        'pro_isapproved' => 0,
                                        'pro_isresponse' => 0,
                                        'pro_status' => 0,
                                        'activated_date' => null
                                    ];
                                }else {
                                    $input = [
                                        'pro_isapproved' => 1,
                                        'pro_isresponse' => 1,
                                        'pro_status' => 1,
                                        'activated_date' => $now
                                    ];
                                }
                                $update_status = DB::table('nm_product')->where('pro_sku', $row->sku)->update($input);

                                $entry_json = $input;
                                $entry_json['pro_id'] = $product_id->pro_id;
                                $entry_json['pro_sku'] = $row->sku;
                                $aud_detail = json_encode($entry_json);
                                $audit_trail = new AuditTrail;
                                $audit_trail->aud_table = 'nm_product';
                                $audit_trail->aud_action = 'update';
                                $audit_trail->aud_detail = $aud_detail;
                                $audit_trail->aud_date = $now;
                                $audit_trail->aud_user_name = Session::get('username');
                                $audit_trail->save();
                                //end update status product
                                $success++;
                            }
                        }
                    }
                    //end execute
                });
                return response()->json(
                    [
                        'total' => $totalRows,
                        'total baris kosong' => $total_empty,
                        'total gagal' => $errors,
                        'total sukses' => $success,
                        'error' => $messages
                    ]
                );
            }else {
                return 'tidak ada file yang diupload';
            }
        }else {
            return Redirect::to('siteadmin');
        }
    }
    public function download_template_product()
    {
        if (Session::has('userid')) {
            //get privileges
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}
            //end get privileges
            //check privilege
            $content_priv = 0;
            $price_priv = 0;
            foreach ($privileges as $priv) {
                $priv_name = explode(' ', $priv['rp_priv_name']);
                if ($priv_name[3] == 'content' && $priv_name[4] == 'edit_product') {
                    $content_priv = 1;
                }
                if ($priv_name[3] == 'price' && $priv_name[4] == 'edit_product') {
                    $price_priv = 1;
                }
            }
            //end check privileges
            Excel::create('Template Product', function($excel) use($content_priv, $price_priv) {
                // Set the title
                $excel->setTitle('Template Product');

                // Chain the setters
                $excel->setCreator('Kukuruyuk')->setCompany('Kukuruyuk');

                // Call them separately
                $excel->setDescription('Template Product');

                $excel->sheet('Data Product', function($sheet) use($content_priv, $price_priv) {
                    if ($content_priv == 1 && $price_priv == 0) {
                        $sheet->row(1, array(
                            'sku',
                            'name',
                            'description',
                            'spec_name_1',
                            'spec_value_1',
                            'meta_title',
                            'meta_keyword',
                            'meta_description',
                            'extended_warranty'
                        ));
                    }
                    elseif ($content_priv == 0 && $price_priv == 1) {
                        $sheet->row(1, array(
                            'sku',
                            'price',
                            'discounted_price',
                            'wholesale_level1_min',
                            'wholesale_level1_max',
                            'wholesale_level1_price',
                            'wholesale_level2_min',
                            'wholesale_level2_max',
                            'wholesale_level2_price',
                            'wholesale_level3_min',
                            'wholesale_level3_max',
                            'wholesale_level3_price',
                            'wholesale_level4_min',
                            'wholesale_level4_max',
                            'wholesale_level4_price',
                            'wholesale_level5_min',
                            'wholesale_level5_max',
                            'wholesale_level5_price'
                        ));
                    }
                    elseif ($content_priv == 1 && $price_priv == 1) {
                        $sheet->row(1, array(
                            'sku',
                            'name',
                            'description',
                            'spec_name_1',
                            'spec_value_1',
                            'meta_title',
                            'meta_keyword',
                            'meta_description',
                            'extended_warranty',
                            'price',
                            'discounted_price',
                            'wholesale_level1_min',
                            'wholesale_level1_max',
                            'wholesale_level1_price',
                            'wholesale_level2_min',
                            'wholesale_level2_max',
                            'wholesale_level2_price',
                            'wholesale_level3_min',
                            'wholesale_level3_max',
                            'wholesale_level3_price',
                            'wholesale_level4_min',
                            'wholesale_level4_max',
                            'wholesale_level4_price',
                            'wholesale_level5_min',
                            'wholesale_level5_max',
                            'wholesale_level5_price',
                        ));
                    }else {
                        $sheet->row(1, array(
                            'UNAUTHORIZED'
                        ));
                    }
                });
                $excel->sheet('Instruksi', function($sheet) {
                    $sheet->mergeCells('A2:E2');
                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 30,
                        'C' => 30,
                        'D' => 30,
                        'E' => 30
                    ));
                    $sheet->row(2, array(
                        'Instruksi cara pengisian data di dalam template yang di download ini adalah sebagai berikut'
                    ));
                    $sheet->row(2, function($row){
                        $row->setBackground('#ff0000');
                    });
                    $sheet->row(4, function($row){
                        $row->setFontWeight('bold');
                        $row->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->row(4, array(
                        'Kolom',
                        'Keterangan',
                        'Nilai',
                        'Required',
                        'Catatan'
                    ));
                    $columns = [
                        'sku' => 'sku barang;text;required; ;',
                        'name' => 'nama barang;text;optional; ;',
                        'description' => 'deskripsi barang;text;optional; ;',
                        'spec_name_1' => 'nama spesifikasi;text;optional;spesifikasi barang dapat ditambah dengan menambah kolom menjadi spec_name_2 dst;',
                        'spec_value_1' => 'nilai dari spec_name_1;text;optional;spesifikasi barang dapat ditambah dengan menambah kolom menjadi spec_value_2 dst',
                        // 'product_image_1' => 'gambar produk;text;optional;gambar dapat ditambah dengan menambah kolom menjadi product_image_2 dst hingga 4 kolom;',
                        'meta_title' => 'meta title;text;optional; ;',
                        'meta_keyword' => 'meta keyword;text;optional; ;',
                        'meta_description' => 'meta description;text;optional; ;',
                        'price' => 'harga barang;number;optional (jika tidak diisi dianggap 0); ;',
                        'discounted_price' => 'harga barang setelah didiskon;number;optional (jika tidak diisi dianggap 0); ;',
                        'wholesale_levelx_min' => 'kuantiti minimal wholesale untuk level tertentu;number;optional (jika tidak diisi dianggap 0);x diisi dengan angka dari 1 hingga 5;',
                        'wholesale_levelx_max' => 'kuantiti maksimal wholesale untuk level tertentu;number;optional (jika tidak diisi dianggap 0);x diisi dengan angka dari 1 hingga 5;',
                        'wholesale_levelx_price' => 'harga wholesale untuk level tertentu;number;optional (jika tidak diisi dianggap 0);x diisi dengan angka dari 1 hingga 5;',
                        // 'status' => 'status barang aktif atau tidak;number;optional;Aktif = 1, Tidak Aktif = 0;',
                        'extended_warranty' => 'apakah barang mempunyai extended warranty;number;optional (jika tidak diisi dianggap tidak aktif);Aktif = 1, Tidak Aktif = 0'
                    ];
                    $i=5;
                    foreach($columns as $column=>$value)
                    {
                        $values = explode(';', $value);
                        $sheet->getStyle('A'.$i.':E'.$i)->getAlignment()->setWrapText(true);
                        $sheet->row($i, function($row){
                            $row->setBorder('thin', 'thin', 'thin', 'thin');
                            $row->setValignment('center');
                        });
                        $sheet->row($i, array(
                            $column,
                            $values[0],
                            $values[1],
                            $values[2],
                            $values[3]
                        ));
                        $i++;
                    }
                });

                $excel->sheet('Contoh Input', function($sheet) {
                    $sheet->getStyle('A1:P10')->getAlignment()->setWrapText(true);
                    $sheet->row(1, array(
                        'sku',
                        'name',
                        'description',
                        'spec_name_1',
                        'spec_value_1',
                        'spec_name_2',
                        'spec_value_2',
                        // 'product_image_1',
                        // 'product_image_2',
                        'meta_title',
                        'meta_keyword',
                        'meta_description',
                        'price',
                        'discounted_price',
                        'wholesale_level1_min',
                        'wholesale_level1_max',
                        'wholesale_level1_price',
                        // 'status',
                        'extended_warranty'
                    ));
                    $sheet->row(2, array(
                        'ART53010931',
                        'ARTEC SE-VPH Vintage Phase Shifter - Hitam - Grade A',
                        'SE-VPH membuat historis PHASE SHIFTER efek Suara. Anda dapat menyesuaikan resonansi untuk mengontrol KETAJAMAN dari Phase Shift sound. SE-VPH membuat suara friendy dari musik Funky Rotary Speaker.',
                        'Dimensi',
                        '7.5 x 11.5 x 4.7',
                        'Berat',
                        '290 gram',
                        // 'https://kukuruyuk-telin.akamaized.net/media/catalog/product/cache/1/image/650x/040ec09b1e35df139433887a97daa66f/a/r/artec_se_vph_vintage_phase_-5_1.jpg',
                        // 'https://kukuruyuk-telin.akamaized.net/media/catalog/product/cache/1/image/650x/040ec09b1e35df139433887a97daa66f/a/r/artec_se_vph_vintage_phase_-5_1.jpg',
                        'Jual Efek Pedal SE-VPH | Kukuruyuk.com',
                        'pedal gratis, pedal rusak, pedal cabutan, pedal original, pedal tarikan, pedal rekondisi, pedal refurbihed, pedal servisan, pedal rakitan, jual pedal gratis, jual pedal rusak, jual pedal cabutan',
                        'SE-VPH membuat historis PHASE SHIFTER efek Suara. Anda dapat menyesuaikan resonansi untuk mengontrol KETAJAMAN dari Phase Shift sound. SE-VPH membuat suara friendy dari musik Funky Rotary Speaker.',
                        '525000',
                        '393300',
                        '2',
                        '5',
                        '350000',
                        // '1',
                        '1',
                    ));
                    $sheet->row(3, array(
                        'ART53010932',
                        'ARTEC SE-VPH Vintage Phase Shifter - Hitam - Grade B',
                        'SE-VPH membuat historis PHASE SHIFTER efek Suara. Anda dapat menyesuaikan resonansi untuk mengontrol KETAJAMAN dari Phase Shift sound. SE-VPH membuat suara friendy dari musik Funky Rotary Speaker.',
                        'Dimensi',
                        '7.5 x 11.5 x 4.7',
                        'Berat',
                        '290 gram',
                        // 'https://kukuruyuk-telin.akamaized.net/media/catalog/product/cache/1/image/650x/040ec09b1e35df139433887a97daa66f/a/r/artec_se_vph_vintage_phase_-5_1.jpg',
                        // 'https://kukuruyuk-telin.akamaized.net/media/catalog/product/cache/1/image/650x/040ec09b1e35df139433887a97daa66f/a/r/artec_se_vph_vintage_phase_-5_1.jpg',
                        'Jual Efek Pedal SE-VPH | Kukuruyuk.com',
                        'pedal gratis, pedal rusak, pedal cabutan, pedal original, pedal tarikan, pedal rekondisi, pedal refurbihed, pedal servisan, pedal rakitan, jual pedal gratis, jual pedal rusak, jual pedal cabutan, jual pedal original, jual pedal tarikan, jual pedal rekondisi, jual pedal refurbihed',
                        'SE-VPH membuat historis PHASE SHIFTER efek Suara. Anda dapat menyesuaikan resonansi untuk mengontrol KETAJAMAN dari Phase Shift sound. SE-VPH membuat suara friendy dari musik Funky Rotary Speaker.',
                        '500000',
                        '350000',
                        '2',
                        '5',
                        '300000',
                        // '0',
                        '0',
                    ));
                });

                $excel->setActiveSheetIndex(0);

            })->export('xlsx');
        }else {
            return Redirect::to('siteadmin');
        }
    }
    public function download_product(Request $request)
    {
        if (Session::has('userid')) {
            //deklarasi variabel
            $merchant = $request->input('merchant');
            $mc = $request->input('top_category');
            $smc = $request->input('main_category');
            $sb = $request->input('sub_category');
            $extended_warranty = $request->input('extended_warranty');
            $qty = $request->input('qty');
            $stock_ready = $request->input('stock_ready');
            $column = $request->input('column');

            //query get product
            $param = [
                'merchant' => $merchant,
                'mc' => $mc,
                'smc' => $smc,
                'sb' => $sb,
                'extended_warranty' => $extended_warranty,
                'qty' => $qty,
                'stock_ready' => $stock_ready
            ];
            $get_product = Products::download_product($param);

            $column_checked = [];
            $spec_checked = 0;
            // dd($column);
            foreach ($column as $col) {
                if($col == 'sku'){
                    array_push($column_checked, 'sku');
                }
                if($col == 'name'){
                    array_push($column_checked, 'name');
                }
                if($col == 'category'){
                    array_push($column_checked, 'main_category');
                    array_push($column_checked, 'secmain_category');
                    array_push($column_checked, 'sub_category');
                    array_push($column_checked, 'secsub_category');
                }
                if($col == 'merchant_name'){
                    array_push($column_checked, 'merchant_name');
                }
                if($col == 'description'){
                    array_push($column_checked, 'description');
                }
                if($col == 'meta_title'){
                    array_push($column_checked, 'meta_title');
                }
                if($col == 'meta_keyword'){
                    array_push($column_checked, 'meta_keyword');
                }
                if($col == 'meta_description'){
                    array_push($column_checked, 'meta_description');
                }
                if($col == 'price'){
                    array_push($column_checked, 'price');
                }
                if($col == 'discounted_price'){
                    array_push($column_checked, 'discounted_price');
                }
                if($col == 'qty'){
                    array_push($column_checked, 'quantity');
                    array_push($column_checked, 'stock_in_purchase');
                    array_push($column_checked, 'stock_ready_to_sale');
                }
                if($col == 'status'){
                    array_push($column_checked, 'status');
                }
                if($col == 'extended_warranty'){
                    array_push($column_checked, 'extended_warranty');
                }
                if($col == 'specification'){
                    $spec_checked = 1;
                }
            }
            date_default_timezone_set('Asia/Jakarta');
            $date = date('d-m-Y');
            // dd($column_checked);
            //excel
            Excel::create('Data Products '.$date, function($excel) use($column_checked, $get_product, $spec_checked) {

                // Set the title
                $excel->setTitle('Data Products');

                // Chain the setters
                $excel->setCreator('Kukuruyuk')->setCompany('Kukuruyuk');

                // Call them separately
                $excel->setDescription('Data Products');

                $excel->sheet('Data Products', function($sheet) use($column_checked, $get_product, $spec_checked) {
                    $sheet->freezeFirstRow();
                    $sheet->row(1, $column_checked);
                    $i=0;
                    $j=1;
                    $l=0;
                    $max=1;
                    $pro_id_before = '';
                    $data = [];
                    // dd($get_product);
                    foreach ($get_product as $get_pro) {
                        $merchant_name = $get_pro->mer_fname.' '.$get_pro->mer_lname;
                        if($l > 0){
                            if($get_pro->pro_id == $pro_id_before){
                                $j++;
                                if($spec_checked == 1){
                                    array_push($data, $get_pro->sp_name);
                                    array_push($data, $get_pro->spc_value);
                                }
                                if($j>$max){
                                    $max=$j;
                                }
                            }else {
                                $j=1;
                                $data = [];
                                foreach ($column_checked as $col) {
                                    if($col == 'sku'){
                                        if ($get_pro->pro_sku != '' || $get_pro->pro_sku != null) {
                                            array_push($data, $get_pro->pro_sku);
                                        }else {
                                            array_push($data, $get_pro->pro_sku_merchant);
                                        }
                                    }
                                    if($col == 'name'){
                                        array_push($data, $get_pro->pro_title);
                                    }
                                    if($col == 'main_category'){
                                        array_push($data, $get_pro->mc_name);
                                        array_push($data, $get_pro->smc_name);
                                        array_push($data, $get_pro->sb_name);
                                        array_push($data, $get_pro->ssb_name);
                                    }
                                    if($col == 'merchant_name'){
                                        array_push($data, $merchant_name);
                                    }
                                    if($col == 'description'){
                                        array_push($data, $get_pro->pro_desc);
                                    }
                                    if($col == 'meta_title'){
                                        array_push($data, $get_pro->pro_mtitle);
                                    }
                                    if($col == 'meta_keyword'){
                                        array_push($data, $get_pro->pro_mkeywords);
                                    }
                                    if($col == 'meta_description'){
                                        array_push($data, $get_pro->pro_mdesc);
                                    }
                                    if($col == 'price'){
                                        array_push($data, $get_pro->pro_price);
                                    }
                                    if($col == 'discounted_price'){
                                        array_push($data, $get_pro->pro_disprice);
                                    }
                                    if($col == 'quantity'){
                                        array_push($data, $get_pro->pro_qty);
                                        array_push($data, $get_pro->pro_no_of_purchase);
                                        array_push($data, $get_pro->pro_qty - $get_pro->pro_no_of_purchase);
                                    }
                                    if($col == 'status'){
                                        array_push($data, $get_pro->pro_status);
                                    }
                                    if($col == 'extended_warranty'){
                                        array_push($data, $get_pro->have_warranty);
                                    }
                                }
                                if($spec_checked == 1){
                                    array_push($data, $get_pro->sp_name);
                                    array_push($data, $get_pro->spc_value);
                                }
                                $i++;
                            }
                        }else {
                            foreach ($column_checked as $col) {
                                if($col == 'sku'){
                                    if ($get_pro->pro_sku != '' || $get_pro->pro_sku != null) {
                                        array_push($data, $get_pro->pro_sku);
                                    }else {
                                        array_push($data, $get_pro->pro_sku_merchant);
                                    }
                                }
                                if($col == 'name'){
                                    array_push($data, $get_pro->pro_title);
                                }
                                if($col == 'main_category'){
                                    array_push($data, $get_pro->mc_name);
                                    array_push($data, $get_pro->smc_name);
                                    array_push($data, $get_pro->sb_name);
                                    array_push($data, $get_pro->ssb_name);
                                }
                                if($col == 'merchant_name'){
                                    array_push($data, $merchant_name);
                                }
                                if($col == 'description'){
                                    array_push($data, $get_pro->pro_desc);
                                }
                                if($col == 'meta_title'){
                                    array_push($data, $get_pro->pro_mtitle);
                                }
                                if($col == 'meta_keyword'){
                                    array_push($data, $get_pro->pro_mkeywords);
                                }
                                if($col == 'meta_description'){
                                    array_push($data, $get_pro->pro_mdesc);
                                }
                                if($col == 'price'){
                                    array_push($data, $get_pro->pro_price);
                                }
                                if($col == 'discounted_price'){
                                    array_push($data, $get_pro->pro_disprice);
                                }
                                if($col == 'quantity'){
                                    array_push($data, $get_pro->pro_qty);
                                    array_push($data, $get_pro->pro_no_of_purchase);
                                    array_push($data, $get_pro->pro_qty - $get_pro->pro_no_of_purchase);
                                }
                                if($col == 'status'){
                                    array_push($data, $get_pro->pro_status);
                                }
                                if($col == 'extended_warranty'){
                                    array_push($data, $get_pro->have_warranty);
                                }
                            }
                            if($spec_checked == 1){
                                array_push($data, $get_pro->sp_name);
                                array_push($data, $get_pro->spc_value);
                            }
                            // $i++;
                            $l++;
                        }
                        // $data = [
                        //     $i,
                        //     $j,
                        //     $max
                        // ];
                        // var_dump($data);
                        $range = $i+2;
                        $sheet->row($range, function($row){
                            $row->setValignment('center');
                        });
                        $sheet->getStyle('A'.$range.':AZ'.$range)->getAlignment()->setWrapText(true);
                        $sheet->row($i+2, $data);
                        $pro_id_before = $get_pro->pro_id;
                    }

                    for ($k=1; $k <= $max; $k++) {
                        array_push($column_checked, 'spec_name_'.$k, 'spec_value_'.$k);
                    }
                    $sheet->row(1, $column_checked);
                });
            })->export('xlsx');
        }else {
            return Redirect::to('siteadmin');
        }

    }

    public function report_pricing(Request $request)
    {
        $from_date  = Input::get('from_date');
        if ($from_date != '') {
            $from_date = strtotime($from_date);
            $from_date_text = date('d F Y', $from_date);
            $from_date = date('Y-m-d 00:00:00', $from_date);

        }else {
            $from_date_text = 'Awal';
        }
        $to_date    = Input::get('to_date');
        if ($to_date != '') {
            $to_date = strtotime($to_date);
            $to_date = strtotime('+1 day', $to_date);
            $to_date_text = date('d F Y', $to_date);
            $to_date = date('Y-m-d 23:59:59', $to_date);
        }else {
            date_default_timezone_set('Asia/Jakarta');
            $to_date_text = date('d F Y');
        }
        $user_name = $request->input('user_name');

        $get_report = AuditTrail::where('aud_table', 'nm_product');
        if ($from_date != '' && $from_date != null) {
            $get_report = $get_report->where('aud_date', '>=', $from_date);
        }
        if ($to_date != '' && $to_date != null) {
            $get_report = $get_report->where('aud_date', '<=', $to_date);
        }
        if ($user_name != '' && $user_name != null) {
            $get_report = $get_report->where('aud_user_name', $user_name);
        }
        $get_report = $get_report->get();

        Excel::create('Report Pricing', function($excel) use($get_report, $from_date_text, $to_date_text){
            // Set the title
            $excel->setTitle('Report Pricing');

            // Chain the setters
            $excel->setCreator('Kukuruyuk')->setCompany('Kukuruyuk');

            // Call them separately
            $excel->setDescription('Report Pricing');

            $excel->sheet('Report Pricing', function($sheet) use($get_report, $from_date_text, $to_date_text) {
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->setAutoSize(['A', 'B', 'C', 'D', 'E', 'F']);
                $sheet->cells('A1:F4', function($cells){
                    $cells->setFontWeight('bold');
                    $cells->setValignment('center');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A4:F4', function($cells){
                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                });
                $sheet->row(1, array('REPORT PRICING'));
                $sheet->row(2, array('PERIODE '.$from_date_text.' - '.$to_date_text));
                $sheet->row(4, array(
                    'SKU',
                    'NAMA PRODUK',
                    'HARGA DISKON',
                    'HARGA NORMAL',
                    'TANGGAL PEMBUATAN',
                    'PEMBUAT'
                ));
                $i = 5;

                foreach ($get_report as $report) {
                    $sheet->cells('A'.$i.':F'.$i, function($cells){
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                    });
                    $aud_detail = json_decode($report->aud_detail);

                    if (array_key_exists('pro_price', $aud_detail)) {
                        $product = DB::table('nm_product')
                        ->select('pro_sku', 'pro_title')
                        ->where('pro_id', $aud_detail->pro_id)
                        ->first();

                        $sheet->row($i, array(
                            $product->pro_sku,
                            $product->pro_title,
                            $aud_detail->pro_disprice,
                            $aud_detail->pro_price,
                            $report->aud_date,
                            $report->aud_user_name
                        ));

                        $i++;
                    }
                }
            });
        })->export('xlsx');
    }

    public function sepulsa_product()
    {
        $include = self::view_include('products', 'admin_left_menu_product');
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        $mobile_products = Sepulsa::get_product('mobile', '', '');
        $electricity_products = Sepulsa::get_product('electricity', '', '');
        $electricity_postpaid_products = Sepulsa::get_product('electricity_postpaid', '', '');
        $bpjs_kesehatan_products = Sepulsa::get_product('bpjs_kesehatan', '', '');
        $game_products = Sepulsa::get_product('game', '', '');
        $multi_products = Sepulsa::get_product('multi', '', '');
        $telkom_postpaid_products = Sepulsa::get_product('telkom_postpaid', '', '');
        $pdam_products = Sepulsa::get_product('pdam', '', '');

        return view('siteadmin.sepulsa_product')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('mobile_products', $mobile_products['list'])
        ->with('electricity_products', $electricity_products['list'])
        ->with('electricity_postpaid_products', $electricity_postpaid_products['list'])
        ->with('bpjs_kesehatan_products', $bpjs_kesehatan_products['list'])
        ->with('game_products', $game_products['list'])
        ->with('multi_products', $multi_products['list'])
        ->with('telkom_postpaid_products', $telkom_postpaid_products['list'])
        ->with('pdam_products', $pdam_products['list']);
    }

    public function add_sepulsa_product(Request $request)
    {
        $include = self::view_include('products', 'admin_left_menu_product');
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        return view('siteadmin.update_sepulsa_product')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('product_id', $request->input('product_id'))
        ->with('type', $request->input('type'))
        ->with('label', $request->input('label'))
        ->with('operator', $request->input('operator'))
        ->with('nominal', $request->input('nominal'))
        ->with('price', $request->input('price'))
        ->with('enabled', $request->input('enabled'))
        ->with('url_cancel', 'sepulsa_product');
    }

    public function update_sepulsa_product_process(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d');
        $now1 = date('Y-m-d H:i:s');
        $get_mer_kuku = DB::table('nm_merchant')
        ->leftJoin('nm_store', 'nm_store.stor_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('mer_fname', 'Kukuruyuk')
        ->select('mer_id', 'stor_id')
        ->first();

        $pro_sku = 'SEPULSA'.$request->input('product_id');
        $pro_title = $request->input('title');
        $pro_price = $request->input('price');
        $pro_disprice = $request->input('disprice');
        $pro_isspec = 2;
        $pro_mr_id = 0;
        $pro_sh_id = 0;
        $created_date = $now;
        $pro_status = $request->input('enabled');
        $pro_qty = 1;
        $pro_isapproved = 1;
        $pro_isresponse = 1;
        $activated_date = $now1;
        $pro_issepulsa = 1;
        $pro_sepulsa_id = $request->input('product_id');
        $pro_sepulsa_type = $request->input('type');
        $pro_sepulsa_operator = $request->input('operator');
        $pro_sepulsa_nominal = $request->input('nominal');

        $data = [
            'pro_sku' => $pro_sku,
            'pro_title' => $pro_title,
            'pro_price' => $pro_price,
            'pro_disprice' => $pro_disprice,
            'pro_isspec' => $pro_isspec,
            'pro_mr_id' => $pro_mr_id,
            'pro_sh_id' => $pro_sh_id,
            'created_date' => $created_date,
            'pro_status' => $pro_status,
            'pro_qty' => $pro_qty,
            'pro_isapproved' => $pro_isapproved,
            'pro_isresponse' => $pro_isresponse,
            'activated_date' => $activated_date,
            'pro_issepulsa' => $pro_issepulsa,
            'pro_sepulsa_id' => $pro_sepulsa_id,
            'pro_sepulsa_type' => $pro_sepulsa_type,
            'pro_sepulsa_nominal' => $pro_sepulsa_nominal,
            'pro_sepulsa_operator' => $pro_sepulsa_operator
        ];

        $get_product = DB::table('nm_product')->where('pro_sepulsa_id', $request->input('product_id'))->first();
        $update = null;
        $insert = null;
        if ($get_product) {
            $update = DB::table('nm_product')->where('pro_sepulsa_id', $request->input('product_id'))->update($data);
        }else {
            $insert = DB::table('nm_product')->insert($data);
        }


        if ($update) {
            return Redirect::to('manage_product')->with('block_message', 'Barang Berhasil Diupdate');
        }elseif ($insert) {
            return Redirect::to('sepulsa_product')->with('success', 'Barang Berhasil Ditambahkan');
        }else {
            return Redirect::to('manage_product')->with('error', 'Terdapat Kesalahan Pada Saat Barang Ditambahkan');
        }
    }

    public function update_sepulsa_product($id)
    {
        $include = self::view_include('products', 'admin_left_menu_product');
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        $get_product = Products::get_product_by_id($id);

        return view('siteadmin.update_sepulsa_product')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('product_id', $get_product->pro_sepulsa_id)
        ->with('type', $get_product->pro_sepulsa_type)
        ->with('label', $get_product->pro_title)
        ->with('operator', $get_product->pro_sepulsa_operator)
        ->with('nominal', $get_product->pro_sepulsa_nominal)
        ->with('price', $get_product->pro_price)
        ->with('disprice', $get_product->pro_disprice)
        ->with('enabled', $get_product->pro_status)
        ->with('url_cancel', 'manage_product');
    }

    public function sepulsa_product_details(Request $request)
    {
        $sepulsa_operator = config('sepulsa.operator');
        $operator = '';
        foreach ($sepulsa_operator as $key => $value) {
            if ($key == $request->input('number')) {
                $operator = $value;
            }
        }

        $get_product_list = DB::table('nm_product')
        ->where('pro_sepulsa_type', $request->input('type'));
        if ($operator != '') {
            $get_product_list = $get_product_list->where('pro_sepulsa_operator', $operator);
        }
        $get_product_list = $get_product_list->where('pro_status', 1)
        ->get();

        return $get_product_list;
    }
}
