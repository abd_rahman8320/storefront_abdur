<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Config;
use URL;
use Route;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\UsersRoles;
use App\RolesPrivileges;
use App\AuditTrail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class MerchantController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public function view_include($routemenu, $left_menu)
 	{
 		if (Session::has('userid')) {
             $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $routemenu)->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.'.$left_menu)->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}

    public function hapus_merchant($id_merchant)
    {
        $get_mail_mer = Merchant::get_reg_mail_merchant($id_merchant);
        $send_mail_data = array(
            'email' => $get_mail_mer->mer_email
        );
        $admin = DB::table('nm_emailsetting')->first();
        $admin_email = $admin->es_noreplyemail;
        Config::set('mail.from.address', $admin_email);
        Mail::send('emails.notifikasi_email_mau_reg_merchant',$send_mail_data, function($message) use ($get_mail_mer)
        {
            $message->to($get_mail_mer->mer_email)->subject('Konfirmasi Pendaftaran Merchant');
        });
        Merchant::hapus_merchant_q($id_merchant);

        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $audit_trail = new AuditTrail;
        $audit_trail->aud_table = 'nm_merchant';
        $audit_trail->aud_action = 'delete';
        $audit_trail->aud_detail = '{"mer_id":"'.$id_merchant.'"}';
        $audit_trail->aud_date = $now;
        $audit_trail->aud_user_name = Session::get('username');
        $audit_trail->save();

        return Redirect::to('approval_merchant');
    }

    public function update_approve_merchant($id_merchant, $email, $password){

        Merchant::update_approve_merchant_q($id_merchant);

        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $audit_trail = new AuditTrail;
        $audit_trail->aud_table = 'nm_merchant';
        $audit_trail->aud_action = 'update';
        $audit_trail->aud_detail = '{"mer_id":"'.$id_merchant.'","status_approval":"2"}';
        $audit_trail->aud_date = $now;
        $audit_trail->aud_user_name = Session::get('username');
        $audit_trail->save();

        $send_mail_data = array(
            'first_name' => $email,
            'password' => $password
        );
        # It will show these lines as error but no issue it will work fine Line no 119 - 122
        $admin = DB::table('nm_emailsetting')->first();
        $admin_email = $admin->es_noreplyemail;
        Config::set('mail.from.address', $admin_email);
        Mail::send('emails.merchantmail', $send_mail_data, function($message) use ($email)
        {
            $message->to($email, $email)->subject('Merchant Account Created Successfully');
        });

        return Redirect::to('approval_merchant');
    }

    public function merchant_dashboard()
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $merchant_count      = Merchant::get_merchant_count();
            $store_cnt           = Merchant::get_store_cnt();
            $admin_stores_cnt    = Merchant::get_admin_stores();
            $merchant_stores_cnt = Merchant::get_merchant_stores();
            return view('siteadmin.merchant_dashboard')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('merchant_count', $merchant_count)->with('store_cnt', $store_cnt)->with('admin_stores_cnt', $admin_stores_cnt)->with('merchant_stores_cnt', $merchant_stores_cnt);
        } else {
            return Redirect::to('siteadmin');
        }

    }

    public function add_merchant()
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_return = Merchant::get_country_detail();
            $city_return    = Merchant::get_city_detail();

            return view('siteadmin.add_merchant')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('country_details', $country_return)
            ->with('city_details', $city_return);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function approval_merchant()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }

            $merchantrep = Merchant::get_merchantreports_approval($from_date, $to_date);


            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $merchant_return_approval_details        = Merchant::merchant_return_approval_details();
            //dd($merchant_return_approval_details);

            return view('siteadmin.approval_merchant')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('merchant_return_approval_details', $merchant_return_approval_details)
            ->with('merchantrep', $merchantrep);
        }
        else
        {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_merchant()
    {
        if (Session::has('userid')) {
            $data      = Input::except(array(
                '_token'
            ));
            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }

            $merchantrep = Merchant::get_merchantreports($from_date, $to_date);

            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $merchant_return               = Merchant::view_merchant_details();
            $store_count                   = Merchant::get_store_count($merchant_return);
            $merchant_is_or_not_in_deals   = Merchant::merchant_is_or_not_in_deals($merchant_return);
            $merchant_is_or_not_in_product = Merchant::merchant_is_or_not_in_product($merchant_return);
            $merchant_is_or_not_in_auction = Merchant::merchant_is_or_not_in_auction($merchant_return);
            return view('siteadmin.manage_merchant')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('merchant_return', $merchant_return)
            ->with('store_count', $store_count)
            ->with('merchant_is_or_not_in_deals', $merchant_is_or_not_in_deals)
            ->with('merchant_is_or_not_in_product', $merchant_is_or_not_in_product)
            ->with('merchant_is_or_not_in_auction', $merchant_is_or_not_in_auction)
            ->with('merchantrep', $merchantrep);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function ajax_select_city()
    {
        $cityid    = $_GET['city_id'];
        $city_ajax = Merchant::get_city_detail_ajax($cityid);
        if ($city_ajax) {
            $return = "";
            $return = "<option value='0'> -- Select -- </option>";
            foreach ($city_ajax as $fetch_city_ajax) {
                $return .= "<option value='" . $fetch_city_ajax->ci_id . "'> " . $fetch_city_ajax->ci_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value=''> No datas found </option>";
        }
    }

    public function clear_dokumen_merchant($id_dokumen)
    {
        $bukti     = Merchant::clear_dokumen_merchant($id_dokumen);

        $id_merchant = Merchant::get_id_merchant_by_id_dokumen($id_dokumen);
        //dd($id_merchant);
        return Redirect::to('edit_merchant/'.$id_merchant->id_merchant);
    }

    public function download_dokumen_merchant($id)
    {
        $base64_file_get = Merchant::get_base64_file_by_id($id);
        // dd($base64_file_get[0]->base64_file);

        $base64_file_get_det = $base64_file_get[0]->base64_file;

        $data = 'data:image/png;base64,'.$base64_file_get_det;

        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data); // Decode image using base64_decode
        $file = 'tmp/image.png'; //Now you can put this image data to your desired file using file_put_contents function like below:
        $success = file_put_contents($file, $data);

        return response()->download('tmp/image.png');
    }

    public function add_merchant_submit_signup()
    {
        if (Session::has('userid')=="" || Session::has('customerid')=="") {
            $data = Input::except(array(
                '_token'
            ));
            $date = date('m/d/Y');
            $rule = array(
                'first_name' => 'required',
                'last_name' => 'required',
                'email_id' => 'required',
                'select_mer_country' => 'required',
                'mer_city_id' => 'required',
                'mer_province_id' => 'required',
                'mer_district_id' => 'required',
                'mer_subdistrict_id' => 'required',
                'phone_no' => 'required',
                'nama_bank' => 'required',
                'nomor_rekening' => 'required',
                'nama_pemilik_rekening' => 'required',
                'addreess_one' => 'required',
                'store_name' => 'required',
                'store_pho' => 'required',
                'store_add_one' => 'required',
                'select_country' => 'required',
                'select_city_id' => 'required',
                'select_province_id' => 'required',
                'select_district_id' => 'required',
                'select_subdistrict_id' => 'required',
                'zip_code' => 'required',
                'latitude' => 'required',
                'longtitude' => 'required',
                'commission' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                //dd("masuk ke kocak");
                return Redirect::to('merchant_signup')->withErrors($validator->messages());
            } else {

              //dd("jokl");

                $mer_email         = Input::get('email_id');

                $check_merchant_id = Merchant::check_merchant_email($mer_email);
                if ($check_merchant_id) {
                  //dd("email sama ");
                    return Redirect::back()->with('mail_exist','email is exists');
                } else {
                  $file             = Input::file('file');
                  //dd($file);
                    if($file != '' || $file != null ) {

                    //var_dump("t1");
                    $filename         = $file->getClientOriginalName();
                    //var_dump("t2");
                    $move_img         = explode('.', $filename);
                    //var_dump("t3");
                    $filename         = $move_img[0] . str_random(8) . "." . $move_img[1];
                    //var_dump("t4");
                    $destinationPath  = './assets/storeimage/';

                    $uploadSuccess    = Input::file('file')->move($destinationPath, $filename);
                  }
                    $get_new_password = Merchant::randomPassword();
                    $merchantid       = Session::get('merchantid');
                    $admin            = '0';

                    // SIUP



                    $file_SIUP        = Input::file('file_siup');
                    //dd($file_SIUP);
                    $imagedata_SIUP = file_get_contents($file_SIUP);
                    //dd($imagedata_SIUP);
                    $base64_SIUP = base64_encode($imagedata_SIUP);

                    // Tanda daftar Perusahaan

                    $file_TDP        = Input::file('file_tanda_daftar_perusahaan');
                    $imagedata_TDP = file_get_contents($file_TDP);

                    $base64_TDP = base64_encode($imagedata_TDP);

                    // Surat Domisili

                    $file_SD        = Input::file('file_surat_domisili');
                    $imagedata_SD = file_get_contents($file_SD);

                    $base64_SD = base64_encode($imagedata_SD);

                    // NPWP

                    $file_NPWP        = Input::file('file_npwp');
                    $imagedata_NPWP = file_get_contents($file_NPWP);

                    $base64_NPWP = base64_encode($imagedata_NPWP);

                    //KTP

                    $file_KTP        = Input::file('file_ktp');
                    $imagedata_KTP = file_get_contents($file_KTP);

                    $base64_KTP = base64_encode($imagedata_KTP);

                    //nantinya ini fungsi download base64 pic

                    //$base642 = base64_decode($imagedata);

                    //$img = $_POST['img']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
                    //$base64 = str_replace('data:image/png;base64,', '', $base64);
                    //$base64 = str_replace(' ', '+', $base64);
                    // $data = base64_decode($base64);
                    // file_put_contents('tmp/image.png', $base64);

                    // return response()->download('tmp/image.png');

                    //dd("sampai");


                    if ($merchantid) {
                        $merchant_entry = array(
                            'addedby' => $merchantid,
                            'mer_fname' => Input::get('first_name'),
                            'mer_lname' => Input::get('last_name'),
                            'mer_password' => $get_new_password,
                            'mer_email' => Input::get('email_id'),
                            'mer_phone' => Input::get('phone_no'),
                            'mer_address1' => Input::get('addreess_one'),
                            'mer_address2' => Input::get('address_two'),
                            'mer_co_id' => Input::get('select_mer_country'),
                            'mer_ci_id' => Input::get('mer_city_id'),
                            'mer_prov_id' => Input::get('mer_province_id'),
                            'mer_dis_id' => Input::get('mer_district_id'),
                            'mer_sdis_id' => Input::get('mer_subdistrict_id'),
                            'mer_payment' => Input::get('payment_account'),
                            'mer_commission' => Input::get('commission'),
                            'created_date' => $date,
                            'nama_bank' => Input::get('nama_bank'),
                            'nomor_rekening' => Input::get('nomor_rekening'),
                            'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
                        );
                    } else {
                        $merchant_entry = array(
                            'addedby' => $admin,
                            'mer_fname' => Input::get('first_name'),
                            'mer_lname' => Input::get('last_name'),
                            'mer_password' => $get_new_password,
                            'mer_email' => Input::get('email_id'),
                            'mer_phone' => Input::get('phone_no'),
                            'mer_address1' => Input::get('addreess_one'),
                            'mer_address2' => Input::get('address_two'),
                            'mer_co_id' => Input::get('select_mer_country'),
                            'mer_ci_id' => Input::get('mer_city_id'),
                            'mer_prov_id' => Input::get('mer_province_id'),
                            'mer_dis_id' => Input::get('mer_district_id'),
                            'mer_sdis_id' => Input::get('mer_subdistrict_id'),
                            'mer_payment' => Input::get('payment_account'),
                            'mer_commission' => Input::get('commission'),
                            'created_date' => $date,
                            'nama_bank' => Input::get('nama_bank'),
                            'nomor_rekening' => Input::get('nomor_rekening'),
                            'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
                        );
                    }

                    //dd($merchant_entry);
                    $inserted_merchant_id = Merchant::insert_merchant($merchant_entry);

                    if($file == "" || $file == null)
                    {
                      $store_entry          = array(
                          'stor_name' => Input::get('store_name'),
                          'stor_merchant_id' => $inserted_merchant_id,
                          'stor_phone' => Input::get('store_pho'),
                          'stor_address1' => Input::get('store_add_one'),
                          'stor_address2' => Input::get('store_add_two'),
                          'stor_country' => Input::get('select_country'),
                          'stor_city' => Input::get('select_city_id'),
                          'stor_prov_id' => Input::get('select_province_id'),
                          'stor_dis_id' => Input::get('select_district_id'),
                          'stor_sdis_id' => Input::get('select_subdistrict_id'),
                          'stor_zipcode' => Input::get('zip_code'),
                          'stor_website' => Input::get('website'),
                          'stor_latitude' => Input::get('latitude'),
                          'stor_longitude' => Input::get('longtitude'),
                          'stor_img' => "",
                          'created_date' => $date
                      );
                    }
                    else {
                      $store_entry          = array(
                          'stor_name' => Input::get('store_name'),
                          'stor_merchant_id' => $inserted_merchant_id,
                          'stor_phone' => Input::get('store_pho'),
                          'stor_address1' => Input::get('store_add_one'),
                          'stor_address2' => Input::get('store_add_two'),
                          'stor_country' => Input::get('select_country'),
                          'stor_city' => Input::get('select_city_id'),
                          'stor_prov_id' => Input::get('select_province_id'),
                          'stor_dis_id' => Input::get('select_district_id'),
                          'stor_sdis_id' => Input::get('select_subdistrict_id'),
                          'stor_zipcode' => Input::get('zip_code'),
                          'stor_website' => Input::get('website'),
                          'stor_latitude' => Input::get('latitude'),
                          'stor_longitude' => Input::get('longtitude'),
                          'stor_img' => $filename,
                          'created_date' => $date
                      );
                    }

                    Merchant::insert_store($store_entry);

                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'SIUP',
                        'base64_file' => $base64_SIUP
                        );

                    //dd($dokumen_merchant_SIUP);

                    Merchant::insert_dokumen($dokumen_merchant_SIUP);

                    $dokumen_merchant_TDP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'tanda daftar perusahaan',
                        'base64_file' => $base64_TDP
                        );

                    Merchant::insert_dokumen($dokumen_merchant_TDP);

                    $dokumen_merchant_SD = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'surat domisili',
                        'base64_file' => $base64_SD
                        );

                    Merchant::insert_dokumen($dokumen_merchant_SD);

                    $dokumen_merchant_NPWP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'NPWP',
                        'base64_file' => $base64_NPWP
                        );

                    Merchant::insert_dokumen($dokumen_merchant_NPWP);

                    $dokumen_merchant_KTP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'KTP',
                        'base64_file' => $base64_KTP
                        );

                    Merchant::insert_dokumen($dokumen_merchant_KTP);

                    return Redirect::to('mer_register_success')->with('result', 'Anda Telah Terdaftar Silahkan Cek Email Anda Untuk Login');

                }
            }
        }
        else {
          //dd("diangap login");
            return Redirect::to('merchant_signup')->with('result', 'Anda telah login, silahkan logout terlebih dahulu');
        }
    }

    public function mer_register_success(){
      $city_details                 = Register::get_city_details();
      $header_category              = Home::get_header_category();
      $getbannerimagedetails        = Home::getbannerimagedetails();
      $getmetadetails               = Home::get_meta_details();
      $getlogodetails               = Home::getlogodetails();
      $get_contact_det              = Footer::get_contact_details();
      $getanl                       = Settings::social_media_settings();
      $general                      = Home::get_general_settings();
      $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
      $cms_page_title               = Home::get_cms_page_title();
      $country_details              = Register::get_country_details();
      $get_social_media_url         = Home::get_social_media_url();
      $get_pm_img_details           = Footer::get_paymentmethod_img_details();
      $thanks_register              = Footer::get_mer_thanks_reg_page_details();
      if (Session::has('customerid')) {
          $navbar = view('includes.loginnavbar')->with('country_details', $country_details)
          ->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
      } else {
          $navbar = view('includes.navbar')->with('country_details', $country_details)
          ->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
      }

      $header = view('includes.header')->with('header_category', $header_category)
      ->with('logodetails', $getlogodetails);
      $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)
      ->with('cms_page_title', $cms_page_title)
      ->with('get_partners', $get_partners)
      ->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)
      ->with('getanl', $getanl);

      return view('mer_reg_success')
      ->with('navbar', $navbar)
      ->with('header', $header)
      ->with('footer', $footer)
      ->with('cms_result',$thanks_register)
      ->with('header_category', $header_category)
      ->with('bannerimagedetails', $getbannerimagedetails)
      ->with('metadetails', $getmetadetails)
      ->with('get_contact_det', $get_contact_det)
      ->with('general', $general);
    }

    public function add_merchant_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $date = date('m/d/Y');
            $rule = array(
                'first_name' => 'required|alpha_dash',
                'last_name' => 'required|alpha_dash',
                'email_id' => 'required|email',
                'select_mer_country' => 'required',
                'select_mer_city' => 'required',
                'phone_no' => 'required',
                'nama_bank' => 'required',
                'nomor_rekening' => 'required',
                'nama_pemilik_rekening' => 'required',
                'addreess_one' => 'required',
                'address_two' => 'required',
                'store_name' => 'required',
                'store_pho' => 'required',
                'store_add_one' => 'required',
                'store_add_two' => 'required',
                'select_country' => 'required',
                'select_city' => 'required',
                'zip_code' => 'required',
                'website' => 'required',
                'latitude' => 'required',
                'longtitude' => 'required',
                'commission' => 'required|numeric',
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                //dd("masuk ke kocak");
                return Redirect::to('add_merchant')->with('tidak lengkap', 'Data Tidak Lengkap')->withInput();
            } else {
                $mer_email         = Input::get('email_id');

                $check_merchant_id = Merchant::check_merchant_email($mer_email);
                if ($check_merchant_id) {
                    return Redirect::to('add_merchant')->with('mail_exist', 'Merchant Email Exist')->withInput();
                } else {
                    $file             = Input::file('file');
                    $filename         = $file->getClientOriginalName();
                    $move_img         = explode('.', $filename);
                    $filename         = $move_img[0] . str_random(8) . "." . $move_img[1];
                    $destinationPath  = './assets/storeimage/';

                    $uploadSuccess    = Input::file('file')->move($destinationPath, $filename);
                    $get_new_password = Merchant::randomPassword();
                    $merchantid       = Session::get('merchantid');
                    $admin            = '0';

                    // SIUP



                    $file_SIUP        = Input::file('file_siup');
                    //dd($file_SIUP);
                    $imagedata_SIUP = file_get_contents($file_SIUP);
                    //dd($imagedata_SIUP);
                    $base64_SIUP = base64_encode($imagedata_SIUP);

                    // Tanda daftar Perusahaan

                    $file_TDP        = Input::file('file_tanda_daftar_perusahaan');
                    $imagedata_TDP = file_get_contents($file_TDP);

                    $base64_TDP = base64_encode($imagedata_TDP);

                    // Surat Domisili

                    $file_SD        = Input::file('file_surat_domisili');
                    $imagedata_SD = file_get_contents($file_SD);

                    $base64_SD = base64_encode($imagedata_SD);

                    // NPWP

                    $file_NPWP        = Input::file('file_npwp');
                    $imagedata_NPWP = file_get_contents($file_NPWP);

                    $base64_NPWP = base64_encode($imagedata_NPWP);

                    //KTP

                    $file_KTP        = Input::file('file_ktp');
                    $imagedata_KTP = file_get_contents($file_KTP);

                    $base64_KTP = base64_encode($imagedata_KTP);

                    //nantinya ini fungsi download base64 pic

                    //$base642 = base64_decode($imagedata);

                    //$img = $_POST['img']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
                    //$base64 = str_replace('data:image/png;base64,', '', $base64);
                    //$base64 = str_replace(' ', '+', $base64);
                    // $data = base64_decode($base64);
                    // file_put_contents('tmp/image.png', $base64);

                    // return response()->download('tmp/image.png');

                    //dd("sampai");


                    if ($merchantid) {
                        $merchant_entry = array(
                            'addedby' => $merchantid,
                            'mer_fname' => Input::get('first_name'),
                            'mer_lname' => Input::get('last_name'),
                            'mer_password' => $get_new_password,
                            'mer_email' => Input::get('email_id'),
                            'mer_phone' => Input::get('phone_no'),
                            'mer_address1' => Input::get('addreess_one'),
                            'mer_address2' => Input::get('address_two'),
                            'mer_co_id' => Input::get('select_mer_country'),
                            'mer_ci_id' => Input::get('select_mer_city'),
                            'mer_payment' => Input::get('payment_account'),
                            'mer_commission' => Input::get('commission'),
                            'created_date' => $date,
                            'nama_bank' => Input::get('nama_bank'),
                            'nomor_rekening' => Input::get('nomor_rekening'),
                            'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
                        );
                    } else {
                        $merchant_entry = array(
                            'addedby' => $admin,
                            'mer_fname' => Input::get('first_name'),
                            'mer_lname' => Input::get('last_name'),
                            'mer_password' => $get_new_password,
                            'mer_email' => Input::get('email_id'),
                            'mer_phone' => Input::get('phone_no'),
                            'mer_address1' => Input::get('addreess_one'),
                            'mer_address2' => Input::get('address_two'),
                            'mer_co_id' => Input::get('select_mer_country'),
                            'mer_ci_id' => Input::get('select_mer_city'),
                            'mer_payment' => Input::get('payment_account'),
                            'mer_commission' => Input::get('commission'),
                            'created_date' => $date,
                            'nama_bank' => Input::get('nama_bank'),
                            'nomor_rekening' => Input::get('nomor_rekening'),
                            'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
                        );
                    }

                    $inserted_merchant_id = Merchant::insert_merchant($merchant_entry);

                    $store_entry          = array(
                        'stor_name' => Input::get('store_name'),
                        'stor_merchant_id' => $inserted_merchant_id,
                        'stor_phone' => Input::get('store_pho'),
                        'stor_address1' => Input::get('store_add_one'),
                        'stor_address2' => Input::get('store_add_two'),
                        'stor_country' => Input::get('select_country'),
                        'stor_city' => Input::get('select_city'),
                        'stor_zipcode' => Input::get('zip_code'),
                        'stor_website' => Input::get('website'),
                        'stor_latitude' => Input::get('latitude'),
                        'stor_longitude' => Input::get('longtitude'),
                        'stor_img' => $filename,
                        'created_date' => $date
                    );

                    Merchant::insert_store($store_entry);

                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'SIUP',
                        'base64_file' => $base64_SIUP
                        );

                    //dd($dokumen_merchant_SIUP);

                    Merchant::insert_dokumen($dokumen_merchant_SIUP);

                    $dokumen_merchant_TDP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'tanda daftar perusahaan',
                        'base64_file' => $base64_TDP
                        );

                    Merchant::insert_dokumen($dokumen_merchant_TDP);

                    $dokumen_merchant_SD = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'surat domisili',
                        'base64_file' => $base64_SD
                        );

                    Merchant::insert_dokumen($dokumen_merchant_SD);

                    $dokumen_merchant_NPWP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'NPWP',
                        'base64_file' => $base64_NPWP
                        );

                    Merchant::insert_dokumen($dokumen_merchant_NPWP);

                    $dokumen_merchant_KTP = array(
                        'id_merchant' => $inserted_merchant_id,
                        'jenis_dokumen' => 'KTP',
                        'base64_file' => $base64_KTP
                        );

                    Merchant::insert_dokumen($dokumen_merchant_KTP);

                    return Redirect::to('manage_merchant')->with('result', 'Record Inserted Successfully');

                }
            }
        } else {
            return Redirect::to('siteadmin');
            return Redirect::to('siteadmin');
        }
    }

    public function edit_merchant_submit(Request $request)
    {
        if (Session::has('userid')) {
            $mer_id = Input::get('mer_id');
            $data   = Input::except(array(
                '_token'
            ));
            $rule   = array(
                'first_name' => 'required|alpha_dash',
                'last_name' => 'required|alpha_dash',
                // 'email_id' => 'required|email',
                'select_mer_country' => 'required',
                'select_mer_city' => 'required',
                'phone_no' => 'required|numeric',
                'addreess_one' => 'required',
                'commission' => 'required|numeric',
                'nama_bank' => 'required',
                'nomor_rekening' => 'required',
                'nama_pemilik_rekening' => 'required'
            );

            $validator = Validator::make($data, $rule);
            $url = URL::previous();
            if ($validator->fails()) {
                return Redirect::to($url)->withErrors($validator->messages())->withInput();
            } else {
                $mer_email         = Input::get('email_id');
                $check_merchant_id = Merchant::check_merchant_email_edit($mer_email, $mer_id);
                if ($check_merchant_id) {
                    //return Redirect::to('edit_merchant/' . $mer_id)->with('mail_exist', 'Merchant Email Exist')->withInput();

                    $merchant_entry       = array(
                        'mer_fname' => Input::get('first_name'),
                        'mer_lname' => Input::get('last_name'),
                        //'mer_email' => Input::get('email_id'),
                        'mer_phone' => Input::get('phone_no'),
                        'mer_address1' => Input::get('addreess_one'),
                        'mer_address2' => Input::get('address_two'),
                        'mer_co_id' => Input::get('select_mer_country'),
                        'mer_ci_id' => Input::get('select_mer_city'),
                        'mer_payment' => Input::get('payment_account'),
                        'mer_commission' => Input::get('commission'),
                        'nama_bank' => Input::get('nama_bank'),
                        'nomor_rekening' => Input::get('nomor_rekening'),
                        'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
                    );
                    $inserted_merchant_id = Merchant::edit_merchant($merchant_entry, $mer_id);

                    date_default_timezone_set('Asia/Jakarta');
                    $now = date('Y-m-d H:i:s');
                    $merchant_entry_json = $merchant_entry;
                    $merchant_entry_json['mer_id'] = $mer_id;
                    $aud_detail = json_encode($merchant_entry_json);
                    $audit_trail = new AuditTrail;
                    $audit_trail->aud_table = 'nm_merchant';
                    $audit_trail->aud_action = 'update';
                    $audit_trail->aud_detail = $aud_detail;
                    $audit_trail->aud_date = $now;
                    $audit_trail->aud_user_name = Session::get('username');
                    $audit_trail->save();

                } else {
                    $merchant_entry       = array(
                        'mer_fname' => Input::get('first_name'),
                        'mer_lname' => Input::get('last_name'),
                        //'mer_email' => Input::get('email_id'),
                        'mer_phone' => Input::get('phone_no'),
                        'mer_address1' => Input::get('addreess_one'),
                        'mer_address2' => Input::get('address_two'),
                        'mer_co_id' => Input::get('select_mer_country'),
                        'mer_ci_id' => Input::get('select_mer_city'),
                        'mer_payment' => Input::get('payment_account'),
                        'mer_commission' => Input::get('commission'),
                        'nama_bank' => Input::get('nama_bank'),
                        'nomor_rekening' => Input::get('nomor_rekening'),
                        'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
                    );
                    $inserted_merchant_id = Merchant::edit_merchant($merchant_entry, $mer_id);

                    date_default_timezone_set('Asia/Jakarta');
                    $now = date('Y-m-d H:i:s');
                    $merchant_entry_json = $merchant_entry;
                    $merchant_entry_json['mer_id'] = $mer_id;
                    $aud_detail = json_encode($merchant_entry_json);
                    $audit_trail = new AuditTrail;
                    $audit_trail->aud_table = 'nm_merchant';
                    $audit_trail->aud_action = 'update';
                    $audit_trail->aud_detail = $aud_detail;
                    $audit_trail->aud_date = $now;
                    $audit_trail->aud_user_name = Session::get('username');
                    $audit_trail->save();
                }

                //dd(Input::file('file_SIUP'));

                if(Input::file('file_SIUP') != "")
                {


                    $file_SIUP        = Input::file('file_SIUP');

                    $imagedata_SIUP = file_get_contents($file_SIUP);

                    $base64_SIUP = base64_encode($imagedata_SIUP);


                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $mer_id,
                        'jenis_dokumen' => 'SIUP',
                        'base64_file' => $base64_SIUP
                        );

                    Merchant::update_dokumen_merchant($dokumen_merchant_SIUP);
                    //dd($dokumen_merchant_SIUP);
                }

                if(Input::file('file_TDP') != "")
                {
                    $file_SIUP        = Input::file('file_TDP');

                    $imagedata_SIUP = file_get_contents($file_SIUP);

                    $base64_SIUP = base64_encode($imagedata_SIUP);


                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $mer_id,
                        'jenis_dokumen' => 'tanda daftar perusahaan',
                        'base64_file' => $base64_SIUP
                        );

                    Merchant::update_dokumen_merchant($dokumen_merchant_SIUP);
                }

                if(Input::file('file_SD') != "")
                {
                    $file_SIUP        = Input::file('file_SD');

                    $imagedata_SIUP = file_get_contents($file_SIUP);

                    $base64_SIUP = base64_encode($imagedata_SIUP);


                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $mer_id,
                        'jenis_dokumen' => 'surat domisili',
                        'base64_file' => $base64_SIUP
                        );

                    Merchant::update_dokumen_merchant($dokumen_merchant_SIUP);
                }

                if(Input::file('file_NPWP') != "")
                {
                    $file_SIUP        = Input::file('file_NPWP');

                    $imagedata_SIUP = file_get_contents($file_SIUP);

                    $base64_SIUP = base64_encode($imagedata_SIUP);


                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $mer_id,
                        'jenis_dokumen' => 'NPWP',
                        'base64_file' => $base64_SIUP
                        );

                    Merchant::update_dokumen_merchant($dokumen_merchant_SIUP);
                }

                if(Input::file('file_KTP') != "")
                {
                    $file_SIUP        = Input::file('file_KTP');

                    $imagedata_SIUP = file_get_contents($file_SIUP);

                    $base64_SIUP = base64_encode($imagedata_SIUP);


                    $dokumen_merchant_SIUP = array(
                        'id_merchant' => $mer_id,
                        'jenis_dokumen' => 'KTP',
                        'base64_file' => $base64_SIUP
                        );

                    Merchant::update_dokumen_merchant($dokumen_merchant_SIUP);
                }

                $url_explode = explode('/', str_replace(url(''), '', $url));
                // dd($url_explode);
                if ($url_explode[1] == 'edit_merchant') {
                    return Redirect::to('manage_merchant')->with('result', 'Record Updated Successfully');
                }elseif ($url_explode[1] == 'view_merchant_approval') {
                    return Redirect::to('approval_merchant')->with('result', 'Record Updated Successfully');
                }

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function view_merchant_approval($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_return  = Merchant::get_country_detail();
            $merchant_return = Merchant::get_induvidual_merchant_detail($id);
            $store_return = DB::table('nm_store')
            ->leftJoin('nm_country', 'nm_country.co_id', '=', 'nm_store.stor_country')
            ->leftJoin('nm_city', 'nm_city.ci_id', '=', 'nm_store.stor_city')
            ->select('nm_store.*', 'nm_country.co_name', 'nm_city.ci_name')
            ->where('stor_merchant_id', $id)
            ->first();
            // dd($merchant_return);
            $dokumen_detail_merchant = Merchant::get_dokumen_detail($id);
            // dd($dokumen_detail_merchant);

            $dokumen_merchant_SIUP = Merchant::get_dokumen_SIUP($id);
            $dokumen_merchant_TDP = Merchant::get_dokumen_TDP($id);
            $dokumen_merchant_SD = Merchant::get_dokumen_SD($id);
            $dokumen_merchant_NPWP = Merchant::get_dokumen_merchant_NPWP($id);
            $dokumen_merchant_KTP = Merchant::get_dokumen_merchant_KTP($id);

            return view('siteadmin.view_merchant_approval')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('country_details', $country_return)
            ->with('merchant_details', $merchant_return)
            ->with('dokumen_detail_merchant', $dokumen_detail_merchant)
            ->with('store_return', $store_return)

            ->with('dokumen_merchant_SIUP', $dokumen_merchant_SIUP)
            ->with('dokumen_merchant_TDP', $dokumen_merchant_TDP)
            ->with('dokumen_merchant_SD', $dokumen_merchant_SD)
            ->with('dokumen_merchant_NPWP', $dokumen_merchant_NPWP)
            ->with('dokumen_merchant_KTP', $dokumen_merchant_KTP);

        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_merchant($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_return  = Merchant::get_country_detail();
            $merchant_return = Merchant::get_induvidual_merchant_detail($id);

            $dokumen_detail_merchant = Merchant::get_dokumen_detail($id);
            //dd($dokumen_detail_merchant);

            $dokumen_merchant_SIUP = Merchant::get_dokumen_SIUP($id);
            $dokumen_merchant_TDP = Merchant::get_dokumen_TDP($id);
            $dokumen_merchant_SD = Merchant::get_dokumen_SD($id);
            $dokumen_merchant_NPWP = Merchant::get_dokumen_merchant_NPWP($id);
            $dokumen_merchant_KTP = Merchant::get_dokumen_merchant_KTP($id);

            return view('siteadmin.edit_merchant')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('country_details', $country_return)
            ->with('merchant_details', $merchant_return)
            ->with('dokumen_detail_merchant', $dokumen_detail_merchant)

            ->with('dokumen_merchant_SIUP', $dokumen_merchant_SIUP)
            ->with('dokumen_merchant_TDP', $dokumen_merchant_TDP)
            ->with('dokumen_merchant_SD', $dokumen_merchant_SD)
            ->with('dokumen_merchant_NPWP', $dokumen_merchant_NPWP)
            ->with('dokumen_merchant_KTP', $dokumen_merchant_KTP);

        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function ajax_select_city_edit()
    {
        $cityid    = $_GET['city_id_ajax'];
        $city_ajax = Merchant::get_city_detail_ajax_edit($cityid);
        if ($city_ajax) {
            $return = "";
            foreach ($city_ajax as $fetch_city_ajax) {
                $return .= "<option value='" . $fetch_city_ajax->ci_id . "' selected > " . $fetch_city_ajax->ci_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value=''> No datas found </option>";
        }
    }


    public function block_merchant($id, $status)
    {
        if (Session::has('userid')) {
            $entry = array(
                'mer_staus' => $status
            );
            Merchant::block_merchant_status($id, $entry);

            date_default_timezone_set('Asia/Jakarta');
            $now = date('Y-m-d H:i:s');
            $entry_json = $entry;
            $entry_json['mer_id'] = $id;
            $aud_detail = json_encode($entry_json);
            $audit_trail = new AuditTrail;
            $audit_trail->aud_table = 'nm_merchant';
            $audit_trail->aud_action = 'update';
            $audit_trail->aud_detail = $aud_detail;
            $audit_trail->aud_date = $now;
            $audit_trail->aud_user_name = Session::get('username');
            $audit_trail->save();

            if ($status == 1) {
                return Redirect::to('manage_merchant')->with('result', 'Merchant Activated Successfully');
            } else {
                return Redirect::to('manage_merchant')->with('result', 'Merchant Blocked Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_store($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $store_return               = Merchant::view_store_details($id);
			//print_r($store_return); exit;
            $store_is_or_not_in_deals   = Merchant::store_is_or_not_in_deals($store_return);
            $store_is_or_not_in_product = Merchant::store_is_or_not_in_product($store_return);
            $store_is_or_not_in_auction = Merchant::store_is_or_not_in_auction($store_return);
           return view('siteadmin.manage_store')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('store_return', $store_return)->with('store_is_or_not_in_deals', $store_is_or_not_in_deals)->with('store_is_or_not_in_product', $store_is_or_not_in_product)->with('store_is_or_not_in_auction', $store_is_or_not_in_auction);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_store($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_return = Merchant::get_country_detail();
            return view('siteadmin.add_store')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_return)->with('id', $id);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_store_submit()
    {
        if (Session::has('userid')) {
            $merchant_id = Input::get('store_merchant_id');
            $data        = Input::except(array(
                '_token'
            ));
            $rule        = array(
                'store_name' => 'required',
                'store_pho' => 'required|numeric',
                'store_add_one' => 'required',
                'store_add_two' => 'required',
                'select_country' => 'required',
                'select_city' => 'required',
                'zip_code' => 'required|numeric',
                'meta_keyword' => 'required',
                'meta_description' => 'required',
                'website' => 'required',
                'latitude' => 'required',
                'longtitude' => 'required',
                'file' => 'image'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_store/' . $merchant_id)->withErrors($validator->messages())->withInput();
            } else {
                $mer_email       = Input::get('email_id');
                $file            = Input::file('file');
                $filename        = $file->getClientOriginalName();
                $move_img        = explode('.', $filename);
                $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
                $destinationPath = './assets/storeimage/';

                $uploadSuccess   = Input::file('file')->move($destinationPath, $filename);

                $store_entry = array(
                    'stor_name' => Input::get('store_name'),
                    'stor_merchant_id' => $merchant_id,
                    'stor_phone' => Input::get('store_pho'),
                    'stor_address1' => Input::get('store_add_one'),
                    'stor_address2' => Input::get('store_add_two'),
                    'stor_country' => Input::get('select_country'),
                    'stor_city' => Input::get('select_city'),
                    'stor_zipcode' => Input::get('zip_code'),
                    'stor_metakeywords' => Input::get('meta_keyword'),
                    'stor_metadesc' => Input::get('meta_description'),
                    'stor_website' => Input::get('website'),
                    'stor_latitude' => Input::get('latitude'),
                    'stor_longitude' => Input::get('longtitude'),
                    'stor_img' => $filename
                );

                Merchant::insert_store($store_entry);
                return Redirect::to('manage_store/' . $merchant_id)->with('result', 'Record Inserted Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_store($id, $mer_id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_return = Merchant::get_country_detail();
            $store_return   = Merchant::get_induvidual_store_detail($id);
            return view('siteadmin.edit_store')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_return)->with('id', $id)->with('store_return', $store_return)->with('mer_id', $mer_id);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_store_submit()
    {
        if (Session::has('userid')) {
            $merchant_id = Input::get('mer_id');
            $store_id    = Input::get('store_id');
            $data        = Input::except(array(
                '_token'
            ));
            $rule        = array(
                'store_name' => 'required',
                'store_pho' => 'required|numeric',
                'store_add_one' => 'required',
                'store_add_two' => 'required',
                'select_country' => 'required',
                'select_city' => 'required',
                'zip_code' => 'required|numeric',
                'meta_keyword' => 'required',
                'meta_description' => 'required',
                'website' => 'required',
                'latitude' => 'required',
                'longtitude' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_store/' . $store_id)->withErrors($validator->messages())->withInput();
            } else {

                $file = Input::file('file');
                if ($file == '') {
                    $filename = Input::get('file_new');
                } else {
                    $filename        = $file->getClientOriginalName();
                    $move_img        = explode('.', $filename);
                    $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
                    $destinationPath = './assets/storeimage/';
                    $uploadSuccess   = Input::file('file')->move($destinationPath, $filename);
                }
                $store_entry = array(
                    'stor_name' => Input::get('store_name'),
                    'stor_phone' => Input::get('store_pho'),
                    'stor_address1' => Input::get('store_add_one'),
                    'stor_address2' => Input::get('store_add_two'),
                    'stor_country' => Input::get('select_country'),
                    'stor_city' => Input::get('select_city'),
                    'stor_zipcode' => Input::get('zip_code'),
                    'stor_metakeywords' => Input::get('meta_keyword'),
                    'stor_metadesc' => Input::get('meta_description'),
                    'stor_website' => Input::get('website'),
                    'stor_latitude' => Input::get('latitude'),
                    'stor_longitude' => Input::get('longtitude'),
                    'stor_img' => $filename
                );

                Merchant::edit_store($store_id, $store_entry);
                return Redirect::to('manage_store/' . $merchant_id)->with('result', 'Record Updated Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function block_store($id, $status, $mer_id)
    {
        if (Session::has('userid')) {
            $entry = array(
                'stor_status' => $status
            );
            Merchant::block_store_status($id, $entry);
            if ($status == 1) {
                return Redirect::to('manage_store/' . $mer_id)->with('result', 'Store Activated Successfully');
            } else {
                return Redirect::to('manage_store/' . $mer_id)->with('result', 'Store Blocked Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_enquiry()
    {
        if (Session::has('userid')) {
            $include = self::view_include('merchant', 'admin_left_menu_merchant');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $merchant_return               = Merchant::view_merchant_details();
            $store_count                   = Merchant::get_store_count($merchant_return);
            $merchant_is_or_not_in_deals   = Merchant::merchant_is_or_not_in_deals($merchant_return);
            $merchant_is_or_not_in_product = Merchant::merchant_is_or_not_in_product($merchant_return);
            $merchant_is_or_not_in_auction = Merchant::merchant_is_or_not_in_auction($merchant_return);
            $enquiry_return                = Merchant::view_enquiry_details();
            $enquiry_count                 = Merchant::get_enquiry_count($enquiry_return);
            return view('siteadmin.manage_enquiry')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('merchant_return', $merchant_return)->with('store_count', $store_count)->with('merchant_is_or_not_in_deals', $merchant_is_or_not_in_deals)->with('merchant_is_or_not_in_product', $merchant_is_or_not_in_product)->with('merchant_is_or_not_in_auction', $merchant_is_or_not_in_auction)->with('enquiry_return', $enquiry_return)->with('enquiry_count', $enquiry_count);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function apiCreateMerchant(Request $request)
	{
        $req_city = Merchant::get_city($request->input('mer_ci_code'));
        $req_country = Merchant::get_country($request->input('mer_co_code'));
        $req_merchant = Merchant::get_merchant($request->input('mer_code'));

        if($req_merchant!=null)
        {
            $get_mer_id = Merchant::get_merchant($request->input('mer_code'));

            if($request->input('mer_code')==null)
            {
                return response('Required Parameter Is Null', 400);
                die();
            }
            if($request->input('mer_ci_code'))
            {
                $req_city = Merchant::get_city($request->input('mer_ci_code'));
                if($req_city==null)
                {
                    return response('City Code Not Found', 400);
                    die();
                }
            }
            if($request->input('mer_co_code'))
            {
                $req_country = Merchant::get_country($request->input('mer_co_code'));
                if($req_country==null)
                {
                    return response('Country Code Not Found', 400);
                    die();
                }
            }
            if($get_mer_id==null)
            {
                return response('Merchant Code Not Found', 400);
                die();
            }

            $row = Merchant::find($get_mer_id->mer_id);
            if($request->input('addedby')!=null)
                $row->addedby=$request->input('addedby');
            if($request->input('mer_fname')!=null)
    		    $row->mer_fname=$request->input('mer_fname');
            if($request->input('mer_lname')!=null)
    		    $row->mer_lname=$request->input('mer_lname');
            if($request->input('mer_password')!=null)
    		    $row->mer_password=$request->input('mer_password');
            if($request->input('mer_email')!=null)
                $row->mer_email=$request->input('mer_email');
            if($request->input('mer_phone')!=null)
                $row->mer_phone=$request->input('mer_phone');
            if($request->input('mer_address1')!=null)
                $row->mer_address1=$request->input('mer_address1');
            if($request->input('mer_address2')!=null)
                $row->mer_address2=$request->input('mer_address2');
            if($request->input('mer_ci_code')!=null)
                $row->mer_ci_id=$req_city->ci_id;
            if($request->input('mer_co_code')!=null)
                $row->mer_co_id=$req_country->co_id;
            if($request->input('mer_staus')!=null)
                $row->mer_staus=$request->input('mer_staus');
            if($request->input('mer_payment')!=null)
                $row->mer_payment=$request->input('mer_payment');
            if($request->input('mer_commission')!=null)
                $row->mer_commission=$request->input('mer_commission');

            $row->save();

            return response('Sukses', 200);
        }
        if($request->input('mer_code')==null
        || $request->input('mer_fname')==null
        || $request->input('mer_lname')==null
        || $request->input('mer_password')==null
        || $request->input('mer_email')==null
        || $request->input('mer_phone')==null
        || $request->input('mer_address1')==null
        || $request->input('mer_ci_code')==null
        || $request->input('mer_co_code')==null
        || $request->input('mer_commission')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($req_city == null)
        {
            return response('City Code Not Found', 400);
            die();
        }
        if($req_country == null)
        {
            return response('Country Code Not Found', 400);
            die();
        }

		$row = new Merchant();
        $row->mer_code=$request->input('mer_code');
        if($request->input('addedby'))
		    $row->addedby=$request->input('addedby');
        else
            $row->addedby='';
		$row->mer_fname=$request->input('mer_fname');
		$row->mer_lname=$request->input('mer_lname');
		$row->mer_password=$request->input('mer_password');
        $row->mer_email=$request->input('mer_email');
        $row->mer_phone=$request->input('mer_phone');
        $row->mer_address1=$request->input('mer_address1');
        $row->mer_ci_id=$req_city->ci_id;
        $row->mer_co_id=$req_country->co_id;
        if($request->input('created_date'))
            $row->created_date=$request->input('created_date');
        else
            $row->created_date='';
        $row->mer_staus=1;
        if($request->input('mer_address2')!=null)
        {
            $row->mer_address2=$request->input('mer_address2');
        }else{
            $row->mer_address2='';
        }
        if($request->input('mer_payment')!=null)
        {
            $row->mer_payment=$request->input('mer_payment');
        }else {
            $row->mer_payment='';
        }
        $row->mer_commission=$request->input('mer_commission');

		$row->save();

        return response('Sukses', 200);
	}

    public function apiUpdateMerchant(Request $request)
    {
        $get_mer_id = Merchant::get_merchant($request->input('mer_code'));

        if($request->input('mer_code')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($request->input('mer_ci_code'))
        {
            $req_city = Merchant::get_city($request->input('mer_ci_code'));
            if($req_city==null)
            {
                return response('City Code Not Found', 400);
                die();
            }
        }
        if($request->input('mer_co_code'))
        {
            $req_country = Merchant::get_country($request->input('mer_co_code'));
            if($req_country==null)
            {
                return response('Country Code Not Found', 400);
                die();
            }
        }
        if($get_mer_id==null)
        {
            return response('Merchant Code Not Found', 400);
            die();
        }

        $row = Merchant::find($get_mer_id->mer_id);
        if($request->input('addedby')!=null)
            $row->addedby=$request->input('addedby');
        if($request->input('mer_fname')!=null)
		    $row->mer_fname=$request->input('mer_fname');
        if($request->input('mer_lname')!=null)
		    $row->mer_lname=$request->input('mer_lname');
        if($request->input('mer_password')!=null)
		    $row->mer_password=$request->input('mer_password');
        if($request->input('mer_email')!=null)
            $row->mer_email=$request->input('mer_email');
        if($request->input('mer_phone')!=null)
            $row->mer_phone=$request->input('mer_phone');
        if($request->input('mer_address1')!=null)
            $row->mer_address1=$request->input('mer_address1');
        if($request->input('mer_address2')!=null)
            $row->mer_address2=$request->input('mer_address2');
        if($request->input('mer_ci_code')!=null)
            $row->mer_ci_id=$req_city->ci_id;
        if($request->input('mer_co_code')!=null)
            $row->mer_co_id=$req_country->co_id;
        if($request->input('mer_staus')!=null)
            $row->mer_staus=$request->input('mer_staus');
        if($request->input('mer_payment')!=null)
            $row->mer_payment=$request->input('mer_payment');
        if($request->input('mer_commission')!=null)
            $row->mer_commission=$request->input('mer_commission');

        $row->save();

        return response('Sukses', 200);
    }

    public function apiDeleteMerchant(Request $request)
    {
        $get_mer_id = Merchant::get_merchant($request->input('mer_code'));

        if($get_mer_id==null)
        {
            return response('Merchant Code Not Found', 400);
            die();
        }
        $row = Merchant::find($get_mer_id->mer_id);
        $row->delete();

        return response('Sukses', 200);
    }
}
