<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Products;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\MerchantTransactions;
use App\Fund;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class FundController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function fund_request()
    {
        $merchant_id    = Session::get('merchantid');
        $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "funds");
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_fund');

        $fundtransactiondetails = Fund::get_fund_transaction_details($merchant_id);
        $merchant_details       = Fund::get_merchant_details($merchant_id);
        $get_pay                = Settings::get_pay_settings();
        $get_cur                = $get_pay[0]->ps_cursymbol;

        return view('sitemerchant.fund_request')
        ->with('merchant_details', $merchant_details)
        ->with('get_cur', $get_cur)
        ->with('adminheader', $adminheader)
        ->with('adminfooter', $adminfooter)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('fundtransactiondetails', $fundtransactiondetails);
    }

    public function konfirmasi_fund_request($id)
    {
        $update = DB::table('nm_withdraw_request')
        ->where('wd_id', $id)
        ->update([
            'wd_status' => 2
        ]);
        return Redirect::to('fund_request');
    }

    public function download_fund_request($id)
    {
        $get = DB::table('nm_withdraw_request')->where('wd_id', $id)->first();
        $fname = $get->wd_image;
        $destinationPath = public_path('assets/buktifundrequest/').$fname;

        return response()->download($destinationPath);
    }

    public function with_fund_request()
    {
        $merchnat_id         = Session::get('merchantid');
        $adminheader         = view('sitemerchant.includes.merchant_header')->with("routemenu", "funds");
        $adminfooter         = view('sitemerchant.includes.merchant_footer');
        $adminleftmenus      = view('sitemerchant.includes.merchant_left_menu_fund');
        $deal_count          = Fund::deal_no_count($merchnat_id);
        $deal_discount_count = Fund::deal_discount_count($merchnat_id);

        $product_no_count       = Fund::product_no_count($merchnat_id);
        $product_discount_count = Fund::product_discount_count($merchnat_id);
        $commison_amt           = Fund::commison_amt($merchnat_id);
        $paidamounttomerchantrs = Fund::paid_amt($merchnat_id);
        $paidamounttomerchant   = $paidamounttomerchantrs[0]->paid_amt;

        $fund_details           = Fund::fund_details($merchnat_id);
        $get_pay                = Settings::get_pay_settings();
        $get_cur                = $get_pay[0]->ps_cursymbol;

        $product_merchant = Fund::product_detail($merchnat_id);
        // dd($product_merchant);
        return view('sitemerchant.with_fund_request1')
        ->with('adminheader', $adminheader)
        ->with('adminfooter', $adminfooter)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('fund_details', $fund_details)
        ->with('get_cur', $get_cur)
        ->with('deal_count', $deal_count)
        ->with('deal_discount_count', $deal_discount_count)
        ->with('product_no_count', $product_no_count)
        ->with('product_discount_count', $product_discount_count)
        ->with('commison_amt', $commison_amt)
        ->with('paidamounttomerchant', $paidamounttomerchant)
        ->with('product_merchant', $product_merchant);
    }

    public function withdraw_submit()
    {
        $id = Input::get('merchant_id');
        $select_merchant_commision = DB::table('nm_merchant')->select('mer_commission')->where('mer_id', $id)->first();
        $total = DB::table('nm_order')
        ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->where('is_funded', 0)
        ->where('pro_mr_id', $id)
        ->where('status_outbound', 5)
        ->where('is_fund_request', 0)
        ->sum('order_amt');
        $total = $total - ($total * $select_merchant_commision->mer_commission/100);
        $faktur_pajak = Input::get('faktur_pajak');
        date_default_timezone_set('Asia/Jakarta');
        $today = date('Y-m-d H:i:s');
        $entry = array(
            'wd_mer_id' => $id,
            'wd_total_wd_amt' => $total,
            'wd_date' => $today,
            'wd_faktur_pajak' => $faktur_pajak
        );
        $select_detail = DB::table('nm_order')
        ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->where('is_funded', 0)
        ->where('pro_mr_id', $id)
        ->where('status_outbound', 5)
        ->where('is_fund_request', 0)
        ->get();
        if(!$select_detail){
            return Redirect::to('with_fund_request')->with('success', 'Record Have Been Updated');
        }
        $insert = Fund::save_withdraw($entry);
        $insert_detail = Fund::save_withdraw_detail($id);
        // dd('test');
        return Redirect::to('with_fund_request')->with('success', 'Record Updated Successfully');
    }


}
