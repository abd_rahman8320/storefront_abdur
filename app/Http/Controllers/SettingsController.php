<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\AuditTrail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
Use Log;
Use Carbon;
Use App\Commands\Command;
use App\UsersRoles;
use App\RolesPrivileges;
Use Illuminate\Queue\SerializesModels;
Use Illuminate\Queue\InteractsWithQueue;
Use Illuminate\Contracts\Bus\SelfHandling;
Use Illuminate\Contracts\Queue\ShouldBeQueued;
class SettingsController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function view_include($routemenu, $left_menu)
 	{
 		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
 			$privileges = [];
 			foreach ($user_role as $ur) {
 				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
 				foreach ($role_privilege as $rp) {
                     $rp = $rp->toArray();
 					array_push($privileges, $rp);
 				}
 			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", $routemenu)->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.'.$left_menu)->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}

    public function general_setting()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $general_settings = Settings::view_general_setting();
            $language_list    = Settings::view_language_list();
            $theme_list       = Settings::view_theme_list();
            return view('siteadmin.general_settings')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('general_settings', $general_settings)->with('language_list', $language_list)->with('theme_list', $theme_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function general_setting_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'site_name' => 'required',
                'meta_title' => 'required',
                'meta_key' => 'required',
                'meta_desc' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('general_setting')->withErrors($validator->messages())->withInput();

            } else {

                $entry  = array(
                    'gs_sitename' => Input::get('site_name'),
                    'gs_metatitle' => Input::get('meta_title'),
                    'gs_metakeywords' => Input::get('meta_key'),
                    'gs_metadesc' => Input::get('meta_desc'),
                    'gs_payment_status' => Input::get('payment_status'),
                    'gs_themes' => Input::get('themes')
                );
                $return = Settings::save_general_set($entry);
                return Redirect::to('general_setting')->with('success', 'Record Updated Successfully');

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function email_setting()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $email_settings = Settings::view_email_settings();
            return view('siteadmin.email_settings')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('email_settings', $email_settings);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function smtp_setting()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $smtp_settings  = Settings::view_smtp_settings();
            $send_settings  = Settings::view_send_settings();
            return view('siteadmin.smtp_settings')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('smtp_settings', $smtp_settings)->with('send_settings', $send_settings);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function email_setting_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'contact_name' => 'required',
                'contact_email' => 'required|email',
                'webmaster_email' => 'required|email',
                'no_reply_email' => 'required|email',
                'contact_pno' => 'required|numeric|min:10',
                'lati' => 'required',
                'long' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('email_setting')->withErrors($validator->messages())->withInput();

            } else {

                $entry  = array(
                    'es_contactname' => Input::get('contact_name'),
                    'es_contactemail' => Input::get('contact_email'),
                    'es_webmasteremail' => Input::get('webmaster_email'),
                    'es_noreplyemail' => Input::get('no_reply_email'),
                    'es_phone1' => Input::get('contact_pno'),
                    'es_phone2' => Input::get('contact_pno2'),
                    'es_latitude' => Input::get('lati'),
                    'es_longitude' => Input::get('long')

                );
                $return = Settings::save_email_set($entry);
                return Redirect::to('email_setting')->with('success', 'Record Updated Successfully');

            }
        } else {
            var_dump("masuk else 1");
            //return Redirect::to('siteadmin');
        }
    }

    public function smtp_setting_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'smtp_host' => 'required',
                'smtp_port' => 'required',
                'smtp_username' => 'required',
                'password' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('smtp_setting')->withErrors($validator->messages())->withInput();

            } else {

                $entry  = array(
                    'sm_host' => Input::get('smtp_host'),
                    'sm_port' => Input::get('smtp_port'),
                    'sm_uname' => Input::get('smtp_username'),
                    'sm_pwd' => Input::get('password'),
                    'sm_isactive' => 1
                 );
                $return = Settings::save_smtp_set_def();
                $return = Settings::save_smtp_set($entry);
                return Redirect::to('smtp_setting')->with('success', 'Record Updated Successfully');

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function send_setting_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'send_host' => 'required',
                'send_port' => 'required',
                'send_username' => 'required',
                'send_password' => 'required'

            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('smtp_setting')->withErrors($validator->messages())->withInput();

            } else {

                $entry  = array(
                    'sm_host' => Input::get('send_host'),
                    'sm_port' => Input::get('send_port'),
                    'sm_uname' => Input::get('send_username'),
                    'sm_pwd' => Input::get('send_password'),
                    'sm_isactive' => 1

                );
                $return = Settings::save_smtp_set_def();
                $return = Settings::save_send_set($entry);
                return Redirect::to('smtp_setting')->with('success', 'Record Updated Successfully');

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function social_media_settings()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $social_settings = Settings::social_media_settings();
            return view('siteadmin.social_media_settings')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('social_settings', $social_settings);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function social_media_setting_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'fb_app_id' => 'required',
                'fb_secret_key' => 'required',
                'fb_page_url' => 'required|url',
                'fb_like_box_url' => 'required|url',
                'twitter_page_url' => 'required|url',
                'twitter_app_id' => 'required',
                'twitter_secret_key' => 'required',
                'linkedin_page_url' => 'required|url',
                'youtube_page_url' => 'required|url',
                'gmap_app_key' => 'required',
                'analytics_code' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('social_media_settings')->withErrors($validator->messages())->withInput();
            } else {
                $entry  = array(
                    'sm_fb_app_id' => Input::get('fb_app_id'),
                    'sm_fb_sec_key' => Input::get('fb_secret_key'),
                    'sm_fb_page_url' => Input::get('fb_page_url'),
                    'sm_fb_like_page_url' => Input::get('fb_like_box_url'),
                    'sm_twitter_url' => Input::get('twitter_page_url'),
                    'sm_twitter_app_id' => Input::get('twitter_app_id'),
                    'sm_twitter_sec_key' => Input::get('twitter_secret_key'),
                    'sm_linkedin_url' => Input::get('linkedin_page_url'),
                    'sm_youtube_url' => Input::get('youtube_page_url'),
                    'sm_gmap_app_key' => Input::get('gmap_app_key'),
                    'sm_android_page_url' => Input::get('android_page_url'),
                    'sm_iphone_url' => Input::get('iphone_page_url'),
                    'sm_analytics_code' => Input::get('analytics_code')
                );
                $result = Settings::update_social_media_settings($entry);
                if ($result) {
                    return Redirect::to('social_media_settings')->with('success', 'Record updated successfully');
                } else {
                    return Redirect::to('social_media_settings')->with('success', 'Something Went wrong');
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function payment_settings()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_settings = Settings::get_country_details();
            $get_pay_settings = Settings::get_pay_settings();
            //dd($get_pay_settings);

            return view('siteadmin.payment_settings')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('country_settings', $country_settings)
            ->with('get_pay_settings', $get_pay_settings);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_bank_akun()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            return view('siteadmin.add_bank_akun')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_bank_akun_submit()
    {
        $data = array(
            'nama_bank' => Input::get('nama_bank'),
            'nomor_rekening' => Input::get('nomor_rekening'),
            'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
            );

        Settings::input_akun_bank($data);

        return Redirect::to('add_bank_akun');
    }

    public function edit_bank_akun($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $detail_bank_akun = Settings::get_detail_bank_akun($id);
            //dd($detail_bank_akun);

            return view('siteadmin.edit_bank_akun')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('detail_bank_akun', $detail_bank_akun)
            ->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_bank_akun_submit()
    {
        $data = array(
            'id' => Input::get('id_bank'),
            'nama_bank' => Input::get('nama_bank'),
            'nomor_rekening' => Input::get('nomor_rekening'),
            'nama_pemilik_rekening' => Input::get('nama_pemilik_rekening')
            );

        Settings::update_akun_bank($data);

        return Redirect::to('bank_akun_kuku');
    }

    public function delete_bank_akun($id)
    {
        Settings::delete_bank_akun_db($id);

        return Redirect::to('bank_akun_kuku');
    }

    public function bank_akun_kuku()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $get_list_bank_akun = Settings::get_list_bank_akun();
            //dd($get_list_bank_akun);

            return view('siteadmin.manage_bank_akun')
            ->with('adminheader', $adminheader)
            ->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('get_list_bank_akun', $get_list_bank_akun);
        } else {
            return Redirect::to('siteadmin');
        }
    }



    public function payment_settings_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'country_name' => 'required',
                'country_code' => 'required',
                'currency_symbol' => 'required',
                'currency_code' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('payment_settings')->withErrors($validator->messages())->withInput();
            } else {
                $entry            = array(
                    'ps_flatshipping' => Input::get('flat_shipping'),
                    'ps_taxpercentage' => Input::get('tax_percentage'),
                    'ps_extenddays' => Input::get('extended_days'),
                    'ps_alertdays' => Input::get('alert_day'),
                    'ps_minfundrequest' => Input::get('maximum_fund_request'),
                    'ps_maxfundrequest' => Input::get('maximum_fund_request'),
                    'ps_referralamount' => Input::get('referral_amount'),
                    'ps_countryid' => Input::get('country_name'),
                    'ps_countrycode' => Input::get('country_code'),
                    'ps_cursymbol' => Input::get('currency_symbol'),
                    'ps_curcode' => Input::get('currency_code'),
                    'ps_paypalaccount' => Input::get('paypal_account'),
                    'ps_paypal_api_pw' => Input::get('paypal_api_password'),
                    'ps_paypal_api_signature' => Input::get('paypal_api_signature'),
                    'ps_authorize_trans_key' => Input::get('authorizenet_trans_key'),
                    'ps_authorize_api_id' => Input::get('authorizenet_api_id'),
                    'ps_paypal_pay_mode' => Input::get('payment_mode'),
                    'ps_bankbranchname' => Input::get('nama_bank'),
                    'ps_bankaccountno' => Input::get('no_rekening'),
                    'ps_bankaccountname' => Input::get('atas_nama'),
                    'ps_lama_cicilan_columbia' => Input::get('lama_cicilan_columbia'),
                    'ps_biaya_columbia' => Input::get('biaya_columbia')
                );
                $get_pay_settings = Settings::update_payment_settings($entry);
                return Redirect::to('payment_settings')->with('success', 'Record updated successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function select_currency_value_ajax()
    {
        $id = $_GET['id'];
        $get_currency = Settings::get_country_value_ajax($id);
        if ($get_currency) {
            foreach ($get_currency as $get_currency_ajax) {
            }
            echo '<div id="whole_currency_div">
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-3" for="text1">Country Code <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="country_code" placeholder="12" id="text1" value="' . $get_currency_ajax->co_code . '" readonly >
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-3" for="text1">Currency Symbol   <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="currency_symbol" placeholder="Rs." id="text1" value="' . $get_currency_ajax->co_cursymbol . '" readonly >
                    </div>
                </div>
                        </div>

						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-3" for="text1">Currency Code   <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="currency_code" placeholder="INR" id="text1" value="' . $get_currency_ajax->co_curcode . '" readonly  >
                    </div>
                </div>
                 </div>
              </div>';

        } else {
            echo '<div id="whole_currency_div">
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-3" for="text1"></label>

                    <div class="col-lg-4">
                  <h4>Please select Country</h4>
                    </div>
                </div>
                        </div>';
        }
    }

    public function module_settings()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $module_setting_details = Settings::get_module_details();
            return view('siteadmin.module_settings')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('module_setting_details', $module_setting_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function module_settings_submit()
    {
        if (Session::has('userid')) {
            $data             = Input::except(array(
                '_token'
            ));
            $entry            = array(
                'ms_dealmodule' => Input::get('deal_module'),
                'ms_productmodule' => Input::get('product_module'),
                'ms_auctionmodule' => Input::get('auction'),
                'ms_blogmodule' => Input::get('blog'),
                'ms_nearmemapmodule' => Input::get('near_me_map'),
                'ms_storelistmodule' => Input::get('store_list'),
                'ms_pastdealmodule' => Input::get('past_deal'),
                'ms_faqmodule' => Input::get('faq'),
                'ms_cod' => Input::get('cod'),
                'ms_paypal' => Input::get('paypal'),
                'ms_creditcard' => Input::get('credit_card'),
                'ms_googlecheckout' => Input::get('google_checkout'),
                'ms_shipping' => Input::get('shipping_settings'),
                'ms_newsletter_template' => Input::get('newsletter_temp'),
                'ms_citysettings' => Input::get('city_settings')
            );
            $get_pay_settings = Settings::update_modul_settings($entry);
            return Redirect::to('module_settings')->with('success', 'Record updated successfully');
        } else {
            return Redirect::to('siteadmin');
        }

    }

    public function manage_newsletter_subscribers()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $subscriber_list = Settings::get_newsletter_subscribers();
            return view('siteadmin.manage_newsletter_subscribers')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('subscriber_list', $subscriber_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_newsletter_subscriber_status($id, $status)
    {
        if (Session::has('userid')) {
            $return = Settings::edit_newsletter_subs_status($id, $status);
            if ($status == 0) {
                return Redirect::to('manage_newsletter_subscribers')->with('success', 'Record Blocked Successfully');
            } else if ($status == 1) {
                return Redirect::to('manage_newsletter_subscribers')->with('success', 'Record Unblocked Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_newsletter_subscriber($id)
    {
        if (Session::has('userid')) {
            Settings::delete_newsletter_subs($id);
            return Redirect::to('manage_newsletter_subscribers')->with('success', 'Record Deleted Successfully');

        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function send_newsletter()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $city_list      = Settings::get_data_subscribers();
            //dd($city_list);
            return view('siteadmin.send_newsletter')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)
            ->with('adminfooter', $adminfooter)
            ->with('city_list',$city_list);
        } else {
            return Rediect::to('siteadmin');
        }
    }

    public function manage_newsletter(){
      if(Session::has('userid')) {
          $include = self::view_include('settings', 'admin_left_menus');
          $adminheader 	= $include['adminheader'];
          $adminleftmenus = $include['adminleftmenus'];
          $adminfooter 	= $include['adminfooter'];
          $news_list      = Settings::get_data_newsletter();
          //dd($city_list);
          return view('siteadmin.manage_newsletter')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)
          ->with('adminfooter', $adminfooter)
          ->with('news_list',$news_list);
      } else {
          return Rediect::to('siteadmin');
      }
    }

    // public function subscribe_newsletter($email,$status)
    // {
    // 	   $return = Settings::subscribe($email,$status);
    // 	    return Redirect::to('unsubscribe');
    //  }
    //
    public function submit_unsubscribe(){
      $email = Input::get('imel');
      $apdet = Settings::unsubscribe($email);

      return view('unsubscribe_success')->with('apdet',$email);
    }

     public function unsubscribe($email)
     {
       //$toall          = Settings::get_data_subscribers_byemail($email);
       //dd($toall);
       //$email        = $toall->email;

       return view('unsubscribe')->with('email',$email);
     }

    public function send_newsletter_submit($id,$status)
    {
      date_default_timezone_set('Asia/Bangkok');
      $getdatanews    = Settings::get_data_newsletter_by_id($id);

      $date = \Carbon\Carbon::parse($getdatanews->tanggal_kirim);
      //dd($date);
      \Queue::later($date, new \App\Commands\SNewsletter($id,$status));

      $now = date('Y-m-d H:i:s');
      $audit_trail = new AuditTrail;
      $audit_trail->aud_table = 'nm_emailblast';
      $audit_trail->aud_action = 'update';
      $audit_trail->aud_detail = '{"status":"'.$status.'","id_emb":"'.$id.'"}';
      $audit_trail->aud_date = $now;
      $audit_trail->aud_user_name = Session::get('username');
      $audit_trail->save();

      return Redirect::to('manage_newsletter')->with('success','Send success');
    }

    public function save_newsletter_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'judul' => 'required',
                'subject' => 'required',
                'startdate' => 'required',
                'message' => 'required'
            );
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('send_newsletter')->withErrors($validator->messages())->withInput();
            }
            else {
              $inputs = Input::all();
              $entry  = array(
              'judul' => Input::get('judul'),
              'subject' => Input::get('subject'),
              'tanggal_kirim' => Input::get('startdate'),
              'isi' => Input::get('message'),
              'status_email' => '0',
              'status' => '0'
          	);
              $return = Settings::submit_data_newsblast($entry);

              date_default_timezone_set('Asia/Jakarta');
              $now = date('Y-m-d H:i:s');
              $aud_detail = json_encode($entry);
              $audit_trail = new AuditTrail;
              $audit_trail->aud_table = 'nm_emailblast';
              $audit_trail->aud_action = 'create';
              $audit_trail->aud_detail = $aud_detail;
              $audit_trail->aud_date = $now;
              $audit_trail->aud_user_name = Session::get('username');
              $audit_trail->save();

              return Redirect::to('manage_newsletter')->with('success', 'Save Success');
            }
        }
        else {
            return Redirect::to('siteadmin');
        }
    }

    public function status_enable_submit($id,$status)
    {
     	if(Session::has('userid'))
    	{
    	   $return = Settings::status_enable_submit($id,$status);

           date_default_timezone_set('Asia/Jakarta');
           $now = date('Y-m-d H:i:s');
           $audit_trail = new AuditTrail;
           $audit_trail->aud_table = 'nm_emailblast';
           $audit_trail->aud_action = 'update';
           $audit_trail->aud_detail = '{"status":"'.$status.'","id_emb":"'.$id.'"}';
           $audit_trail->aud_date = $now;
           $audit_trail->aud_user_name = Session::get('username');
           $audit_trail->save();

    	    return Redirect::to('manage_newsletter')->with('success','Record Updated Successfully');
    	}
    	else
    	{
    	   return Redirect::to('siteadmin');
    	}
     }

     public function delete_newsletter_submit($id)
     {
        if(Session::has('userid'))
        {
          $return = Settings::delete_newsletter($id);

          date_default_timezone_set('Asia/Jakarta');
          $now = date('Y-m-d H:i:s');
          $audit_trail = new AuditTrail;
          $audit_trail->aud_table = 'nm_emailblast';
          $audit_trail->aud_action = 'delete';
          $audit_trail->aud_detail = '{"id_emb":"'.$id.'"}';
          $audit_trail->aud_date = $now;
          $audit_trail->aud_user_name = Session::get('username');
          $audit_trail->save();

          return Redirect::to('manage_newsletter')->with('success','Deleted Successfully');
        }
        else
        {
          return Redirect::to('siteadmin');
        }
     }

    //  public function tampil_news()
    //  {
    //    if(Session::has('userid'))
    //   	{
    //       return view('emails.newsletter');
    //     }
    //     else {
    //       return Redirect::to('siteadmin');
    //     }
    //  }

     public function edit_newsletter($id)
     {
     	if(Session::has('userid'))
     	{
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
       	$banner_detail = Settings::emailblast_detail($id);
     	return view('siteadmin.edit_newsletter')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('banner_detail', $banner_detail);
     	}
     	else
     	{
     	return Redirect::to('siteadmin');
     	}
     }

     public function edit_newsletter_submit()
     {
      if(Session::has('userid'))
      {
        $data = Input::except(array('_token')) ;
        $id = Input::get('id_emb');
        $rule = array(
        'judul' => 'required',
        'subject' => 'required',
        'startdate' => 'required',
        'message' => 'required'
      );
       $validator = Validator::make($data,$rule);
        if ($validator->fails())
        {
           return Redirect::to('edit_newsletter/'.$id)->withErrors($validator->messages())->withInput();
        }
         else
        {
          $inputs = Input::all();
          $entry = array(
          'judul' => Input::get('judul'),
          'subject' => Input::get('subject') ,
          'tanggal_kirim' => Input::get('startdate'),
          'isi' => Input::get('message')
          );
        }
          $return = Settings::update_newsletter_detail($entry,$id);

          date_default_timezone_set('Asia/Jakarta');
          $now = date('Y-m-d H:i:s');
          $entry_json = $entry;
          $entry_json['id_emb'] = $id;
          $aud_detail = json_encode($entry_json);
          $audit_trail = new AuditTrail;
          $audit_trail->aud_table = 'nm_emailblast';
          $audit_trail->aud_action = 'update';
          $audit_trail->aud_detail = $aud_detail;
          $audit_trail->aud_date = $now;
          $audit_trail->aud_user_name = Session::get('username');
          $audit_trail->save();

          return Redirect::to('manage_newsletter')->with('success','Record Updated Successfully');
        }
      else
      {
         return Redirect::to('siteadmin');
      }
    }

    public function extended_warranty()
    {
        if (Session::has('userid')) {
            $include = self::view_include('settings', 'admin_left_menus');
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $get_setting_extended   = Settings::get_setting_extended_db();

            return view('siteadmin.extended_warranty_view')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('get_setting_extended', $get_setting_extended);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function extended_warranty_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'besaran_extended' => 'required|numeric',
                'lama_garansi_dasar' => 'required|numeric'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('extended_warranty')->withErrors($validator->messages())->withInput();

            } else {

                $entry  = array(
                    'besaran_warranty' => Input::get('besaran_extended'),
                    'lama_garansi_dasar' => Input::get('lama_garansi_dasar')
                );
                $return = Settings::save_exrended_warranty($entry);
                return Redirect::to('extended_warranty')->with('success', 'Record Updated Successfully');

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }
}

?>
