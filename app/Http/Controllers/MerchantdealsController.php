<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Customer;
use App\PlanTimer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\Merchantdeals;
use App\Products;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class MerchantdealsController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function add_deals()
    {
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $category       = Merchantdeals::get_category();

        return view('sitemerchant.add_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'save')->with('category', $category);
    }

    public function add_deals_submit()
    {
        $count            = Input::get('count');
        $filename_new_get = "";
        for ($i = 0; $i < $count; $i++) {
            $file_more          = Input::file('file_more' . $i);
            $file_more_name     = $file_more->getClientOriginalName();
            $move_more_img      = explode('.', $file_more_name);
            $filename_new       = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];
            $newdestinationPath = './assets/deals/';
            $uploadSuccess_new  = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);
            $filename_new_get .= $filename_new . "/**/";
        }
        $filename_new_get;
        $now                      = date('Y-m-d H:i:s');
        $inputs                   = Input::all();
        $file                     = Input::file('file');
        $filename                 = $file->getClientOriginalName();
        $move_img                 = explode('.', $filename);
        $filename                 = $move_img[0] . str_random(8) . "." . $move_img[1];
        $destinationPath          = './assets/deals/';
        $uploadSuccess            = Input::file('file')->move($destinationPath, $filename);
        $file_name_insert         = $filename . "/**/" . $filename_new_get;
        $deal_saving_price        = Input::get('originalprice') - Input::get('discountprice');
        $deal_discount_percentage = round(($deal_saving_price / Input::get('originalprice')) * 100, 2);
        $date1                    = date('m/d/Y');


        $entry  = array(
            'deal_title' => Input::get('title'),
            'deal_category' => Input::get('category'),
            'deal_main_category' => Input::get('maincategory'),
            'deal_sub_category' => Input::get('subcategory'),
            'deal_second_sub_category' => Input::get('secondsubcategory'),
            'deal_original_price' => Input::get('originalprice'),
            'deal_discount_price' => Input::get('discountprice'),
            'deal_discount_percentage' => $deal_discount_percentage,
            'deal_saving_price' => $deal_saving_price,
            'deal_start_date' => Input::get('startdate'),
            'deal_end_date' => Input::get('enddate'),
            'deal_expiry_date' => Input::get('enddate'),
            'deal_description' => Input::get('description'),
            'deal_merchant_id' => Input::get('merchant'),
            'deal_shop_id' => Input::get('shop'),
            'deal_meta_keyword' => Input::get('metakeyword'),
            'deal_meta_description' => Input::get('metadescription'),
            'deal_min_limit' => Input::get('minlimt'),
            'deal_max_limit' => Input::get('maxlimit'),
            'deal_image_count' => Input::get('count'),
            'deal_image' => $file_name_insert,
            'deal_posted_date' => $now,
            'created_date' => $date1
        );
        $return = Merchantdeals::save_deal($entry);
        return Redirect::to('mer_manage_deals')->with('block_message', 'Deal Inserted Successfully');
    }

    public function edit_deals($id)
    {
        $adminheader      = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter      = view('sitemerchant.includes.merchant_footer');
        $category         = Merchantdeals::get_category();
        $merchant_details = Merchantdeals::get_merchant_details();
        $deal_list        = Merchantdeals::get_deals($id);
        return view('sitemerchant.edit_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'update')->with('category', $category)->with('deal_list', $deal_list)->with('merchant_details', $merchant_details);
    }

    public function edit_deals_submit()
    {

        $now    = date('Y-m-d H:i:s');
        $inputs = Input::all();

        $count            = Input::get('count');
        $filename_new_get = "";
        for ($i = 0; $i < $count; $i++) {
            $file_more = Input::file('file_more' . $i);
            if ($file_more == "") {
                $dile_name_new_get = Input::get('file_more_new' . $i);
                $filename_new_get .= $dile_name_new_get . "/**/";
            } else {
                $file_more_name     = $file_more->getClientOriginalName();
                $move_more_img      = explode('.', $file_more_name);
                $filename_new       = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];
                $newdestinationPath = './assets/deals/';
                $uploadSuccess_new  = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);
                $filename_new_get .= $filename_new . "/**/";
            }

        }

        $file = Input::file('file');
        if ($file == "") {
            $filename = Input::get('file_new');
        } else {
            $filename        = $file->getClientOriginalName();
            $move_img        = explode('.', $filename);
            $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
            $destinationPath = './assets/deals/';
            $uploadSuccess   = Input::file('file')->move($destinationPath, $filename);
        }

        $file_name_insert         = $filename . "/**/" . $filename_new_get;
        $id                       = Input::get('deal_edit_id');
        $deal_saving_price        = Input::get('originalprice') - Input::get('discountprice');
        $deal_discount_percentage = round(($deal_saving_price / Input::get('originalprice')) * 100, 2);
        $entry                    = array(
            'deal_title' => Input::get('title'),
            'deal_category' => Input::get('category'),
            'deal_main_category' => Input::get('maincategory'),
            'deal_sub_category' => Input::get('subcategory'),
            'deal_second_sub_category' => Input::get('secondsubcategory'),
            'deal_original_price' => Input::get('originalprice'),
            'deal_discount_price' => Input::get('discountprice'),
            'deal_discount_percentage' => $deal_discount_percentage,
            'deal_saving_price' => $deal_saving_price,
            'deal_start_date' => Input::get('startdate'),
            'deal_end_date' => Input::get('enddate'),
            'deal_expiry_date' => Input::get('enddate'),
            'deal_description' => Input::get('description'),
            'deal_merchant_id' => Input::get('merchant'),
            'deal_shop_id' => Input::get('shop'),
            'deal_meta_keyword' => Input::get('metakeyword'),
            'deal_meta_description' => Input::get('metadescription'),
            'deal_min_limit' => Input::get('minlimt'),
            'deal_max_limit' => Input::get('maxlimit'),

            'deal_image_count' => Input::get('count'),
            'deal_image' => $file_name_insert,
            'deal_posted_date' => $now
        );
        $return                   = Merchantdeals::edit_deal($entry, $id);
        return Redirect::to('mer_manage_deals')->with('block_message', 'Deal Updated Successfully');
    }

    public function deals_select_main_cat()
    {
        $id       = $_GET['id'];
        $main_cat = Merchantdeals::get_main_category_ajax($id);
        if ($main_cat) {
            $return = "";
            $return = "<option value='0'> -- Select -- </option>";
            foreach ($main_cat as $main_cat_ajax) {
                $return .= "<option value='" . $main_cat_ajax->smc_id . "'> " . $main_cat_ajax->smc_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function deals_select_sub_cat()
    {
        $id       = $_GET['id'];
        $main_cat = Merchantdeals::get_sub_category_ajax($id);
        if ($main_cat) {
            $return = "";
            $return = "<option value='0'> -- Select -- </option>";
            foreach ($main_cat as $main_cat_ajax) {
                $return .= "<option value='" . $main_cat_ajax->sb_id . "'> " . $main_cat_ajax->sb_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function deals_select_second_sub_cat()
    {
        $id       = $_GET['id'];
        $main_cat = Merchantdeals::get_second_sub_category_ajax($id);
        if ($main_cat) {
            $return = "";
            $return = "<option value='0'> -- Select -- </option>";
            foreach ($main_cat as $main_cat_ajax) {
                $return .= "<option value='" . $main_cat_ajax->ssb_id . "'> " . $main_cat_ajax->ssb_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function deals_edit_select_main_cat()
    {
        $id       = $_GET['edit_id'];
        $main_cat = Merchantdeals::get_main_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->smc_id . "' selected> " . $main_cat_ajax->smc_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function deals_edit_select_sub_cat()
    {
        $id       = $_GET['edit_sub_id'];
        $main_cat = Merchantdeals::get_sub_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->sb_id . "' selected> " . $main_cat_ajax->sb_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function deals_edit_second_sub_cat()
    {
        $id       = $_GET['edit_secnd_sub_id'];
        $main_cat = Merchantdeals::get_second_sub_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->ssb_id . "' selected> " . $main_cat_ajax->ssb_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function check_title_exist()
    {
        $id          = $_GET['title'];
        $exist_title = Merchantdeals::check_title_exist_ajax($id);
        if ($exist_title) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function check_title_exist_edit()
    {
        $title       = $_GET['title'];
        $id          = $_GET['dealid'];
        $exist_title = Merchantdeals::check_title_exist_ajax_edit($id, $title);
        if ($exist_title) {
            echo 1;
        } else {
            echo 0;
        }
    }



    public function manage_deals()
    {
        $date   = date('Y-m-d H:i:s');
        $mer_id = Session::get('merchantid');

        $from_date        = Input::get('from_date');
        $to_date          = Input::get('to_date');
        $merchant_dealrep = Merchantdeals::merchant_dealrep($from_date, $to_date, $mer_id);
        $delete_deals 	  = Merchantdeals::get_order_deals_details();
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantdeals::get_deal_details($date, $mer_id);
        return view('sitemerchant.manage_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('deal_details', $return)->with('merchant_dealrep', $merchant_dealrep)->with('delete_deals', $delete_deals);
    }

    public function block_deals($id, $status)
    {
        $entry = array(
            'deal_status' => $status
        );
        Merchantdeals::block_deal_status($id, $entry);
        if ($status == 1) {
            return Redirect::to('mer_manage_deals')->with('block_message', 'Deal Activated');
        } else if ($status == 0) {
            return Redirect::to('mer_manage_deals')->with('block_message', 'Deal Blocked');
        }
    }


    public function deal_details($id)
    {
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantdeals::get_deals_view($id);
        return view('sitemerchant.deal_details')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('deal_list', $return);
    }

    public function validate_coupon_code()
    {
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');

        return view('sitemerchant.validate_coupon')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
    }

    public function redeem_coupon_list()
    {
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');

        return view('sitemerchant.redeem_coupon')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
    }

	public function delete_deals($id)
	{
		if(Session::has('merchantid'))
		{
	     $adminheader 		= view('sitemerchant.includes.merchant_header')->with('routemenu',"deals");
		 $adminleftmenus	= view('sitemerchant.includes.merchant_left_menu_deals');
		 $adminfooter 		= view('sitemerchant.includes.merchant_footer');
		 $del_deals = Merchantdeals::delete_deals($id);

		return Redirect::to('manage_deals')->with('Deal Deleted','Deal Deleted Successfully');
		}
		else
        {
        return Redirect::to('sitemerchant');
        }
     }

     public function mer_add_plan_timer(){
       if(Session::has('merchantid')){
         $id = Session::get('merchantid');
         $adminheader 		= view('sitemerchant.includes.merchant_header')->with('routemenu',"deals");
         $adminleftmenus	= view('sitemerchant.includes.merchant_left_menu_deals');
         $adminfooter 		= view('sitemerchant.includes.merchant_footer');
         $category          = Deals::get_category();
         $get_pay           = Settings::get_pay_settings();
         $get_cur           = $get_pay[0]->ps_cursymbol;
         $merchant_details  = Deals::get_merchant_details();
         $get_products      = DB::table('nm_product')->where('pro_mr_id','=',$id)->get();

         //dd($get_products);
         $list_postal_sevices = DB::table('nm_postal_services')->select('ps_id', 'ps_code')->get();
        return view('sitemerchant.mer_add_plan_timer')
        ->with('list_postal_sevices', $list_postal_sevices)
        ->with('get_cur',$get_cur)
        ->with('get_products', $get_products)
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('action', 'save')
        ->with('category', $category)
        ->with('merchant_details', $merchant_details);
     } else {
         return Redirect::to('sitemerchant');
       }
     }

     public function mer_add_plan_timer_submit(){
       if (Session::has('merchantid')) {
         $inputs     = Input::all();
         $all_product = 'off';
         $all_product_value = 2;
         if (!empty($inputs['all_products'])) {
           $all_product = $inputs['all_products'];
           $all_product_value = 1;
         }
         //dd($inputs);
         $simple_action = $inputs['simple_action'];
         $title      = $inputs['title'];
         $voucher_code = $inputs['voucher_code'];
         $startdate  = $inputs['startdate'];
         $enddate    = $inputs['enddate'];
         $opt_time   = $inputs['opt_time'];
         $voucher_limit = $inputs['voucher_limit'];
         $customer_voucher_limit = $inputs['customer_voucher_limit'];
         $minimal_transaction = $inputs['minimal_transaction'];
         $start_time = $inputs['start_time'];
         $end_time = $inputs['end_time'];
         if ($opt_time == 2) {
          $rec_week_every = implode(",", $inputs['rec_week_every']);
      }elseif($opt_time == 3){
          $rec_week_every = implode(",", $inputs['rec_month_every']);
          $start_time = 0;
          $end_time = 0;
      }else{
          $rec_week_every = null;
          $start_time = 0;
          $end_time = 0;
      }
         if($simple_action=='free_shipping'){
             $ps_id = $inputs['ps_id'];
             $entry_promo  = [
                 'schedp_simple_free_shipping' => 1,
                 'schedp_title' => $title,
                 'schedp_start_date' => $startdate,
                 'schedp_end_date' => $enddate,
                 'schedp_RecurrenceType' => $opt_time,
                 'schedp_Int2' => $rec_week_every,
                 'schedp_ps_id' => $ps_id,
                 'schedp_simple_action' => $simple_action
                 // 'code_promo' => $code_promo,
             ];
         }elseif ($simple_action=='by_fixed') {
             $discounted_price = $inputs['discounted_price'];
             $discount = $discounted_price;
             $promop_saving_price = $discounted_price;
             $entry_promo  = [
                 'schedp_title' => $title,
                 'schedp_start_date' => $startdate,
                 'schedp_end_date' => $enddate,
                 'schedp_RecurrenceType' => $opt_time,
                 'schedp_Int2' => $rec_week_every,
                 'schedp_simple_action' => $simple_action,
                 'schedp_discount_amount' => $discount,
                 'schedp_coupon_type' => $all_product_value,
                 'schedp_recurr_start_time' => $start_time,
                 'schedp_recurr_end_time' => $end_time
                 // 'code_promo' => $code_promo,
               ];
         }elseif ($simple_action=='by_percent') {
             $discounted_percentage = $inputs['discounted_percentage'];
             $discount = $discounted_percentage;
             $entry_promo  = [
                 'schedp_title' => $title,
                 'schedp_start_date' => $startdate,
                 'schedp_end_date' => $enddate,
                 'schedp_RecurrenceType' => $opt_time,
                 'schedp_Int2' => $rec_week_every,
                 'schedp_simple_action' => $simple_action,
                 'schedp_coupon_type' => $all_product_value,
                 'schedp_discount_amount' => $discount,
                 'schedp_recurr_start_time' => $start_time,
                 'schedp_recurr_end_time' => $end_time
                 // 'code_promo' => $code_promo,
              ];
         }elseif ($simple_action=='buy_x_get_y') {
             $nilai_x = $inputs['nilai_x'];
             $nilai_y = $inputs['nilai_y'];
             $entry_promo  = [
                 'schedp_title' => $title,
                 'schedp_start_date' => $startdate,
                 'schedp_end_date' => $enddate,
                 'schedp_RecurrenceType' => $opt_time,
                 'schedp_Int2' => $rec_week_every,
                 'schedp_simple_action' => $simple_action,
                 'schedp_coupon_type' => $all_product_value,
                 'schedp_x' => $nilai_x,
                 'schedp_y' => $nilai_y,
                 'schedp_recurr_start_time' => $start_time,
                 'schedp_recurr_end_time' => $end_time
                 // 'code_promo' => $code_promo,
              ];
         }
         // $simple_free_shipping = $inputs['free_shipping'];

         $return = PlanTimer::save_scheduled_promo($entry_promo);
         $get_schedp_id = PlanTimer::get_last_schedule();

         $entry = [
             'promoc_schedp_id' => $get_schedp_id->schedp_id,
             'promoc_voucher_code' => $voucher_code,
             'promoc_minimal_transaction' => $minimal_transaction,
             'promoc_usage_limit' => $voucher_limit,
             'promoc_usage_per_customer' => $customer_voucher_limit
         ];
         
         $return_coupons = PlanTimer::save_promo_coupons($entry);

         if($simple_action!='free_shipping')
         {
           if ($all_product == 'on') {
             $product = $inputs['product'];
           }else {
             $product = $inputs['products'];
           }

             $i = 0;
             foreach ($product as $value) {

               $prod_value[] = Products::get_product_by_id($value);
             //   $promop_schedp_id = PlanTimer::get_promo_id($code_promo);

               $pro_id = $prod_value[$i]->pro_id;
               $pro_mr_id = $prod_value[$i]->pro_mr_id;
               $pro_sh_id = $prod_value[$i]->pro_sh_id;
               $pro_status = $prod_value[$i]->pro_status;
               $pro_price = $prod_value[$i]->pro_price;
               $pro_original_disprice = $prod_value[$i]->pro_disprice;

               if($simple_action=='buy_x_get_y'){
                   $entry_product  = [
                               'promop_schedp_id' => $get_schedp_id->schedp_id,
                               'promop_merchant_id' => $pro_mr_id,
                               'promop_shop_id' => $pro_sh_id,
                               'promop_pro_id' => $pro_id,
                               'promop_status' => $pro_status,
                               'promop_original_price' => $pro_price,
                               'promop_original_disprice' => $pro_original_disprice,
                               'promop_discount_price' => 0,
                               'promop_discount_percentage' => 0,
                               'promop_saving_price' => 0,
                             ];
               }else{
                   if($simple_action=='by_percent'){
                         if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                             // dd('test1');
                             $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_price;
                             $pro_disprice = $pro_price-$promop_saving_price;
                         }
                         else{
                             // dd('test2');
                             $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_disprice;
                             $pro_disprice = $pro_original_disprice-$promop_saving_price;
                             // dd($promop_saving_price);
                         }

                   }elseif ($simple_action=='by_fixed') {
                       if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                           $pro_disprice = $pro_price - $promop_saving_price;
                           $discounted_percentage = ($promop_saving_price/$pro_price)*100;
                       }else {
                           $pro_disprice = $pro_original_disprice - $promop_saving_price;
                           $discounted_percentage = ($promop_saving_price/$pro_original_disprice)*100;
                       }

                   }
                   $entry_product  = [
                               'promop_schedp_id' => $get_schedp_id->schedp_id,
                               'promop_merchant_id' => $pro_mr_id,
                               'promop_shop_id' => $pro_sh_id,
                               'promop_pro_id' => $pro_id,
                               'promop_status' => $pro_status,
                               'promop_original_price' => $pro_price,
                               'promop_original_disprice' => $pro_original_disprice,
                               'promop_discount_price' => $pro_disprice,
                               'promop_discount_percentage' => $discounted_percentage,
                               'promop_saving_price' => $promop_saving_price,
                             ];
               }

               $return_pro = PlanTimer::save_promo_products($entry_product);
               $i++;
             }
           }
           return Redirect::to('mer_manage_schedule');
         }
       }
       public function mer_expired_deals()
       {
           $date   = date('Y-m-d H:i:s');
           $mer_id = Session::get('merchantid');

           $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
           $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
           $adminfooter    = view('sitemerchant.includes.merchant_footer');
           //$get_time_schedule = PlanTimer::mer_get_schedule($mer_id);
           $return         = Merchantdeals::get_expired_deals($date, $mer_id);
           return view('sitemerchant.mer_expired_deals')
           ->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)
           ->with('adminfooter', $adminfooter)->with('get_time_schedule', $return);
       }

       public function mer_manage_schedule(){
         if (Session::has('merchantid')) {
             $date      = date('Y-m-d H:i:s');
             $mer_id = Session::get('merchantid');
             //dd($id_merchant);
             $get_time_schedule = PlanTimer::mer_get_schedule($mer_id,$date);
             //dd($get_time_schedule);
             $adminheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "mer_manage_schedule");
             $delete_deals 	= Deals::get_order_deals_details();
             //dd($delete_deals);
             $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
             $adminfooter    = view('sitemerchant.includes.merchant_footer');
             $return         = Deals::get_deal_details($date);
             return view('sitemerchant.mer_manage_schedule')
             ->with('get_time_schedule', $get_time_schedule)
             ->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)
             ->with('adminfooter', $adminfooter)->with('deal_details', $return)
             ->with('delete_deals', $delete_deals);
         } else {
             return Redirect::to('sitemerchant');
         }
       }

       public function mer_schedule_details($id)
        {
            if (Session::has('merchantid'))
            {
              $adminheader      = view('sitemerchant.includes.merchant_header')->with("routemenu", "deals");
              $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_deals');
              $adminfooter      = view('sitemerchant.includes.merchant_footer');
              $get_schedule     = PlanTimer::get_schedule_by_id($id);
              //dd($get_schedule);
              $get_promo_product = PlanTimer::get_promo_by_id($get_schedule[0]->schedp_id);
           //    dd($get_promo_product);
              $get_products_all   = Products::get_all();
           //  dd($get_products_all);
               $get_promo_coupons = PlanTimer::get_promo_coupons($id);
               $get_pay                      = Settings::get_pay_settings();
               $get_cur                      = $get_pay[0]->ps_cursymbol;
               if($get_schedule[0]->schedp_ps_id == 0)
               {
                   $get_postal_services = '';
               }else {
                   $get_postal_services = DB::table('nm_postal_services')->where('ps_id', $get_schedule[0]->schedp_ps_id)->first();
               }
               // dd($get_postal_services);
                foreach ($get_promo_product as $value) {
                  $get_products[] = Products::get_product_by_id($value->promop_pro_id);
                }
                if($get_promo_product == null){
                    $get_products = '';
                }

              return view('sitemerchant.mer_schedule_details')->with('get_cur', $get_cur)->with('get_postal_services', $get_postal_services)->with('get_promo_coupons', $get_promo_coupons)->with('get_products_all', $get_products_all)->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'update')->with('get_schedules', $get_schedule)->with('get_products', $get_products)->with('get_promo_product', $get_promo_product);

            }
            else {
                return Redirect::to('sitemerchant');
            }
        }

        public function mer_expired_schedule_details($id)
         {
             if (Session::has('merchantid'))
             {
               $adminheader      = view('sitemerchant.includes.merchant_header')->with("routemenu", "deals");
               $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_deals');
               $adminfooter      = view('sitemerchant.includes.merchant_footer');
               $get_schedule     = PlanTimer::get_schedule_by_id($id);
               //dd($get_schedule);
               $get_promo_product = PlanTimer::get_promo_by_id($get_schedule[0]->schedp_id);
            //    dd($get_promo_product);
               $get_products_all   = Products::get_all();
            //  dd($get_products_all);
                $get_promo_coupons = PlanTimer::get_promo_coupons($id);
                if($get_schedule[0]->schedp_ps_id == 0)
                {
                    $get_postal_services = '';
                }else {
                    $get_postal_services = DB::table('nm_postal_services')->where('ps_id', $get_schedule[0]->schedp_ps_id)->first();
                }
                // dd($get_postal_services);
                 foreach ($get_promo_product as $value) {
                   $get_products[] = Products::get_product_by_id($value->promop_pro_id);
                 }
                 if($get_promo_product == null){
                     $get_products = '';
                 }

               return view('sitemerchant.mer_expired_schedule_details')->with('get_postal_services', $get_postal_services)->with('get_promo_coupons', $get_promo_coupons)->with('get_products_all', $get_products_all)->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'update')->with('get_schedules', $get_schedule)->with('get_products', $get_products)->with('get_promo_product', $get_promo_product);

             }
             else {
                 return Redirect::to('sitemerchant');
             }
         }

        public function mer_edit_plan_timer($id)
        {
            if (Session::has('merchantid')) {
                $adminheader      = view('sitemerchant.includes.merchant_header')->with("routemenu", "deals");
                $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_deals');
                $adminfooter      = view('sitemerchant.includes.merchant_footer');
                $get_schedule     = PlanTimer::get_schedule_by_id($id);
                //dd($get_schedule);
                $get_promo_product = PlanTimer::get_promo_by_id($get_schedule[0]->schedp_id);
                $category         = Deals::get_category();
                //dd($get_promo_product);
                //dd(SESSION::get('merchantid'));
                $get_products_all   = Products::get_all();
                //dd($get_products_all);
                $get_promo_coupons = PlanTimer::get_promo_coupons($id);
                //dd($get_promo_coupons);
                $list_postal_services = DB::table('nm_postal_services')->select('ps_id', 'ps_code')->get();
                $get_pay                      = Settings::get_pay_settings();
                $get_cur                      = $get_pay[0]->ps_cursymbol;
                //$productcategory = Products::get_product_category();
                //dd($list_postal_services);
                  foreach ($get_promo_product as $value) {
                      $get_products[] = Products::get_product_by_id($value->promop_pro_id);
                      //dd($get_products);
                  }
                  if($get_promo_product == null){
                      $get_products = '';
                      //dd($get_products);
                  }
                //   dd($get_promo_product);
                return view('sitemerchant.mer_edit_plan_timer')
                ->with('id',$id)
                ->with('get_cur', $get_cur)
                ->with('list_postal_services', $list_postal_services)
                ->with('get_promo_coupons', $get_promo_coupons)
                ->with('get_products_all', $get_products_all)
                ->with('adminheader', $adminheader)
                ->with('adminleftmenus', $adminleftmenus)
                ->with('adminfooter', $adminfooter)
                ->with('action', 'update')
                ->with('get_schedules', $get_schedule)
                ->with('get_products', $get_products)
                ->with('get_promo_product', $get_promo_product)
                ->with('category', $category);
            } else {
                return Redirect::to('sitemerchant');
            }
        }

        public function mer_edit_plan_timer_submit(){
            if (Session::has('merchantid')) {
                $inputs     = Input::all();
                $all_product = 'off';
                $all_product_value = 2;
                if (!empty($inputs['all_products'])) {
                  $all_product = $inputs['all_products'];
                  $all_product_value = 1;
                }
                $simple_action = $inputs['simple_action'];
                $title      = $inputs['title'];
                $voucher_code = $inputs['voucher_code'];
                $startdate  = $inputs['startdate'];
                $enddate    = $inputs['enddate'];
                $opt_time   = $inputs['opt_time'];
                $voucher_limit = $inputs['voucher_limit'];
                $start_time = $inputs['start_time'];
                $end_time = $inputs['end_time'];
                $customer_voucher_limit = $inputs['customer_voucher_limit'];
                $minimal_transaction = $inputs['minimal_transaction'];
                if ($opt_time == 2) {
                      $rec_week_every = implode(",", $inputs['rec_week_every']);
                }elseif($opt_time==3){
                      $rec_week_every = implode(",", $inputs['rec_month_every']);
                      $start_time = 0;
                      $end_time = 0;
                }else {
                  $rec_week_every = null;
                  $start_time = 0;
                  $end_time = 0;
                }
                if($simple_action=='free_shipping'){
                    $ps_id = $inputs['ps_id'];
                    $entry_promo  = [
                        'schedp_simple_free_shipping' => 1,
                        'schedp_title' => $title,
                        'schedp_start_date' => $startdate,
                        'schedp_end_date' => $enddate,
                        'schedp_RecurrenceType' => $opt_time,
                        'schedp_Int2' => $rec_week_every,
                        'schedp_ps_id' => $ps_id,
                        'schedp_simple_action' => $simple_action
                        // 'code_promo' => $code_promo,
                    ];
                }elseif ($simple_action=='by_fixed') {
                    $discounted_price = $inputs['discounted_price'];
                    $discount = $discounted_price;
                    $pro_disprice = $discounted_price;
                    $entry_promo  = [
                        'schedp_title' => $title,
                        'schedp_start_date' => $startdate,
                        'schedp_end_date' => $enddate,
                        'schedp_RecurrenceType' => $opt_time,
                        'schedp_Int2' => $rec_week_every,
                        'schedp_simple_action' => $simple_action,
                        'schedp_coupon_type' => $all_product_value,
                        'schedp_discount_amount' => $discount,
                        'schedp_recurr_start_time' => $start_time,
                        'schedp_recurr_end_time' => $end_time
                        // 'code_promo' => $code_promo,
                      ];
                }elseif ($simple_action=='by_percent') {
                    $discounted_percentage = $inputs['discounted_percentage'];
                    $discount = $discounted_percentage;
                    $entry_promo  = [
                        'schedp_title' => $title,
                        'schedp_start_date' => $startdate,
                        'schedp_end_date' => $enddate,
                        'schedp_RecurrenceType' => $opt_time,
                        'schedp_Int2' => $rec_week_every,
                        'schedp_simple_action' => $simple_action,
                        'schedp_coupon_type' => $all_product_value,
                        'schedp_discount_amount' => $discount,
                        'schedp_recurr_start_time' => $start_time,
                        'schedp_recurr_end_time' => $end_time
                        // 'code_promo' => $code_promo,
                     ];
                }elseif ($simple_action=='buy_x_get_y') {
                    $nilai_x = $inputs['nilai_x'];
                    $nilai_y = $inputs['nilai_y'];
                    $entry_promo  = [
                        'schedp_title' => $title,
                        'schedp_start_date' => $startdate,
                        'schedp_end_date' => $enddate,
                        'schedp_RecurrenceType' => $opt_time,
                        'schedp_Int2' => $rec_week_every,
                        'schedp_simple_action' => $simple_action,
                        'schedp_x' => $nilai_x,
                        'schedp_y' => $nilai_y,
                        'schedp_recurr_start_time' => $start_time,
                        'schedp_recurr_end_time' => $end_time
                        // 'code_promo' => $code_promo,
                     ];
                }
                //$get_schedule_id = DB::table('nm_promo_coupons')->where('promoc_voucher_code', $inputs['voucher_code'])->first();
                $edit = PlanTimer::edit_schedule_promo($entry_promo, $inputs['id']);
                PlanTimer::delete_promo_product_by_id($inputs['id']);

                $entry = [
                    'promoc_schedp_id' => $inputs['id'],
                    'promoc_voucher_code' => $voucher_code,
                    'promoc_usage_limit' => $voucher_limit,
                    'promoc_usage_per_customer' => $customer_voucher_limit,
                    'promoc_minimal_transaction' => $minimal_transaction
                ];
                $return_coupons = PlanTimer::edit_promo_coupons($inputs['id'], $entry);

                if($simple_action!='free_shipping')
            {
              if ($all_product == 'on') {
                $product = $inputs['product'];
              }else {
                $product = $inputs['product'];
              }

                $i = 0;
                // dd($product);
                foreach ($product as $value) {

                  $prod_value[] = Products::get_product_by_id($value);
                //   $promop_schedp_id = PlanTimer::get_promo_id($code_promo);

                  $pro_id = $prod_value[$i]->pro_id;
                  $pro_mr_id = $prod_value[$i]->pro_mr_id;
                  $pro_sh_id = $prod_value[$i]->pro_sh_id;
                  $pro_status = $prod_value[$i]->pro_status;
                  $pro_price = $prod_value[$i]->pro_price;
                  $pro_original_disprice = $prod_value[$i]->pro_disprice;

                  if($simple_action=='buy_x_get_y'){
                      $entry_product  = [
                                  'promop_schedp_id' => $get_schedp_id->schedp_id,
                                  'promop_merchant_id' => $pro_mr_id,
                                  'promop_shop_id' => $pro_sh_id,
                                  'promop_pro_id' => $pro_id,
                                  'promop_status' => $pro_status,
                                  'promop_original_price' => $pro_price,
                                  'promop_original_disprice' => $pro_original_disprice,
                                  'promop_discount_price' => 0,
                                  'promop_discount_percentage' => 0,
                                  'promop_saving_price' => 0,
                                ];
                  }else{
                      if($simple_action=='by_percent'){
                            if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                                // dd('test1');
                                $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_price;
                                $pro_disprice = $pro_price-$promop_saving_price;
                            }
                            else{
                                // dd('test2');
                                $promop_saving_price = $discounted_percentage/100 * $prod_value[$i]->pro_disprice;
                                $pro_disprice = $pro_original_disprice-$promop_saving_price;
                                // dd($promop_saving_price);
                            }

                      }elseif ($simple_action=='by_fixed') {
                          $promop_saving_price = $inputs['discounted_price'];
                          if($prod_value[$i]->pro_disprice==0 || $prod_value[$i]->pro_disprice==null || $prod_value[$i]->pro_disprice == ''){
                              $pro_disprice = $pro_price - $promop_saving_price;
                              $discounted_percentage = ($promop_saving_price/$pro_price)*100;
                          }else {
                              $pro_disprice = $pro_original_disprice - $promop_saving_price;
                              $discounted_percentage = ($promop_saving_price/$pro_original_disprice)*100;
                          }

                      }
                    //   dd($get_schedp_id);
                      $entry_product  = [
                                  'promop_schedp_id' => $inputs['id'],
                                  'promop_merchant_id' => $pro_mr_id,
                                  'promop_shop_id' => $pro_sh_id,
                                  'promop_pro_id' => $pro_id,
                                  'promop_status' => $pro_status,
                                  'promop_original_price' => $pro_price,
                                  'promop_original_disprice' => $pro_original_disprice,
                                  'promop_discount_price' => $pro_disprice,
                                  'promop_discount_percentage' => $discounted_percentage,
                                  'promop_saving_price' => $promop_saving_price,
                                ];
                  }
                      $return_pro = PlanTimer::save_promo_products($entry_product);
                      $i++;
                  }
                }
                return Redirect::to('mer_manage_schedule');
            }
        }

        public function mer_block_deals($id, $status)
        {
            if (Session::has('merchantid')) {
                $entry = array(
                    'schedp_status' => $status
                );
                $result = Deals::block_deal_status($id, $entry);
                if ($status == 1) {
                    return Redirect::to('mer_manage_schedule')->with('block_message', 'Deal Activated');
                } else if ($status == 0) {
                    return Redirect::to('mer_manage_schedule')->with('block_message', 'Deal Blocked');
                }
            } else {
                return Redirect::to('sitemerchant');
            }
        }

        public function mer_delete_schedule($id)
      	{

      		if(Session::has('merchantid'))
      		{
      		 $adminheader 		= view('sitemerchant.includes.merchant_header')->with("routemenu","settings");
      		 $adminleftmenus	= view('sitemerchant.includes.merchant_left_menu_deals');
      		 $adminfooter 		= view('sitemerchant.includes.merchant_footer');

      		 $del_deals = PlanTimer::delete_schedule($id);

      		return Redirect::to('mer_manage_schedule')->with('block_message','Schedule Deleted Successfully');
      		}
      		else
              {
              return Redirect::to('sitemerchant');
              }
      	}
}
