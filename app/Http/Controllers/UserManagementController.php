<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Config;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Userlogin;
use App\Users;
use App\UsersRoles;
use App\Roles;
use App\RolesPrivileges;
use App\Privileges;
use App\AuditTrail;
use App\PasswordResetRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
class UserManagementController extends Controller
{
    public function view_include()
    {
        if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}
            // dd($privileges);
            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", "access")->with('privileges', $privileges);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menu_user_management')->with('privileges', $privileges);
            $adminfooter      = view('siteadmin.includes.admin_footer');
            $return = [
                'adminheader' => $adminheader,
                'adminleftmenus' => $adminleftmenus,
                'adminfooter' => $adminfooter
            ];
            return $return;
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function users()
    {
        $include = self::view_include();
        $users_list = Users::get();

        return view('siteadmin.users')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter'])
        ->with('users_list', $users_list);
    }

    public function add_users()
    {
        $include = self::view_include();
        $roles_list = Roles::orderBy('roles_name', 'asc')->get();

        return view('siteadmin.add_users')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter'])
        ->with('roles_list', $roles_list);
    }

    public function add_user_submit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');
        $expired = date('Y-m-d H:i:s', strtotime('+24 hours'));

        $validator = Validator::make(
            [
                'name' => $request->input('name'),
                'full_name' => $request->input("full_name"),
                'email' => $request->input('email')
            ],
            [
                'name' => 'required|max:150|unique:nm_user,user_name',
                'full_name' => 'required|max:150',
                'email' => 'required|max:150'
            ]
        );

        if ($validator->fails()) {
            return Redirect::to('users')->withErrors($validator);
        }

        $request_uuid = Uuid::generate()->string;

        $user = new Users;
        $user->user_name = $request->input('name');
        $user->user_profile_full_name = $request->input('full_name');
        $user->user_profile_email = $request->input('email');
        $user->user_pwd_method = 'sha256';
        $user->created_by = Session::get('username');
        $user->lastupdated_by = Session::get('username');
        $user->save();

        if ($request->input('roles')) {
            foreach ($request->input('roles') as $role) {
                $user_role = new UsersRoles;
                $user_role->ur_user_name = $request->input('name');
                $user_role->ur_roles_name = $role;
                $user_role->created_by = Session::get('username');
                $user_role->lastupdated_by = Session::get('username');
                $user_role->save();
            }
        }

        $prr_delete = PasswordResetRequest::where('prr_user_name', $request->input('name'))->delete();

        $prr = new PasswordResetRequest;
        $prr->prr_user_name = $request->input('name');
        $prr->prr_request_uuid = $request_uuid;
        $prr->prr_is_new = true;
        $prr->prr_expired_ts = $expired;
        $prr->created_by = Session::get('username');
        $prr->lastupdated_by = Session::get('username');
        $prr->save();

        $send_mail_data = [
            'request_uuid' => $request_uuid
        ];

        Mail::send('emails.create_password', $send_mail_data, function($message) use($request){
            $message->to($request->input('email'))->subject('Create Password');
        });

        if ($user->save()) {
            return Redirect::to('users')->with('success', 'User Added Successfully. Check Your Email And Do The Next Step');
        }else {
            return Redirect::to('users')->with('error', 'Something Went Wrong When Added User');
        }
    }

    public function delete_user($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => 'required'
            ]
        );

        if ($validator->fails()) {
            return Redirect::to('users')->withErrors($validator);
        }

        $user = Users::find($id);
        $user->deleted_by = Session::get('username');
        $user->lastupdated_by = Session::get('username');
        $user->is_deleted = 1;
        $user->user_active = 0;
        $user->save();
        $user->delete();

        if ($user->trashed()) {
            return Redirect::to('users')->with('success', 'User Deleted Successfully');
        }else {
            return Redirect::to('users')->with('error', 'Something Went Wrong When Deleted User');
        }
    }

    public function delete_users(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        foreach ($request->input('id') as $id) {
            $user = Users::find($id);
            $user->deleted_by = Session::get('username');
            $user->lastupdated_by = Session::get('username');
            $user->is_deleted = 1;
            $user->save();
            $user->delete();
        }

        if ($user->trashed()) {
            return Redirect::to('users')->with('success', 'Users Deleted Successfully');
        }else {
            return Redirect::to('users')->with('error', 'Something Went Wrong When Deleted Users');
        }
    }

    public function view_users($id)
    {
        $include = self::view_include();
        $user = Users::find($id);
        $roles_list = Roles::orderBy('roles_name', 'asc')->get();
        $user_role = UsersRoles::where('ur_user_name', $user->user_name)->get();

        if ($user) {
            return view('siteadmin.view_users')
            ->with('adminheader', $include['adminheader'])
            ->with('adminleftmenus', $include['adminleftmenus'])
            ->with('adminfooter', $include['adminfooter'])
            ->with('user', $user)
            ->with('roles_list', $roles_list)
            ->with('user_role', $user_role);
        }else {
            return Redirect::to('users')->with('error', 'No query result');
        }

    }

    public function view_user_submit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');
        $expired = date('Y-m-d H:i:s', strtotime('+24 hours'));

        $validator = Validator::make(
            [
                'id' => $request->input('id'),
                'name' => $request->input('name'),
                'full_name' => $request->input('full_name'),
                'email' => $request->input('email'),
            ],
            [
                'id' => 'required',
                'name' => 'required|max:150|unique:nm_user,user_name,'.$request->input('id').',user_id',
                'full_name' => 'required|max:150',
                'email' => 'required|max:150'
            ]
        );

        if ($validator->fails()) {
            return Redirect::to('users')->withErrors($validator);
        }

        if (isset($_POST['reset'])) { //reset password
            $uuid = Uuid::generate()->string;

            $prr_delete = PasswordResetRequest::where('prr_user_name', $request->input('name'))->delete();

            $prr = new PasswordResetRequest;
            $prr->prr_user_name = $request->input('name');
            $prr->prr_request_uuid = $uuid;
            $prr->prr_is_new = false;
            $prr->prr_expired_ts = $expired;
            $prr->created_by = Session::get('username');
            $prr->lastupdated_by = Session::get('username');
            $prr->save();

            $send_mail_data = [
                'request_uuid' => $uuid
            ];

            Mail::send('emails.reset_password', $send_mail_data, function($message) use($request){
                $message->to($request->input('email'))->subject('Reset Password');
            });

            if ($prr->save()) {
                return Redirect::to('users')->with('success', 'Your Request Has Been Sent. Check User\'s Mail');
            }else {
                return Redirect::to('users')->with('error', 'Your Request Failed');
            }
        }elseif (isset($_POST['submit'])) { //edit user
            $user = Users::find($request->input('id'));
            $user_role = UsersRoles::where('ur_user_name', $user->user_name)->update(['ur_user_name' => $request->input('name')]);
            $user->user_name = $request->input('name');
            $user->user_profile_full_name = $request->input('full_name');
            $user->user_profile_email = $request->input('email');
            $user->save();

            $user_role_delete = UsersRoles::where('ur_user_name', $request->input('name'))->forceDelete();
            if ($request->input('roles')) {
                foreach ($request->input('roles') as $role) {
                    $user_role = new UsersRoles;
                    $user_role->ur_user_name = $request->input('name');
                    $user_role->ur_roles_name = $role;
                    $user_role->created_by = Session::get('username');
                    $user_role->lastupdated_by = Session::get('username');
                    $user_role->save();
                }
            }

            if ($user->save()) {
                return Redirect::to('users')->with('success', 'User Updated Successfully');
            }else {
                return Redirect::to('users')->with('error', 'Something Went Wrong When Updated User');
            }
        }else {
            dd('Access Denied');
        }
    }

    public function roles()
    {
        $include = self::view_include();
        $roles_list = Roles::get();

        return view('siteadmin.roles')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter'])
        ->with('roles_list', $roles_list);
    }

    public function add_roles()
    {
        $include = self::view_include();
        $members_list = Users::orderBy('user_name', 'asc')->get();
        $privileges_list = Privileges::orderBy('priv_name', 'asc')->get();

        return view('siteadmin.add_roles')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter'])
        ->with('members_list', $members_list)
        ->with('privileges_list', $privileges_list);
    }

    public function add_roles_submit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'name' => $request->input('name'),
                'label' => $request->input('label'),
                'description' => $request->input('description')
            ],
            [
                'name' => 'required|max:150|unique:nm_roles,roles_name',
                'label' => 'required|max:200',
                'description' => 'max:500',
            ]
        );

        if ($validator->fails()) {
            return Redirect::to('roles')->withErrors($validator);
        }

        $role = new Roles;
        $role->roles_name = $request->input('name');
        $role->roles_label = $request->input('label');
        $role->roles_description = $request->input('description');
        $role->created_by = Session::get('username');
        $role->lastupdated_by = Session::get('username');
        $role->save();

        if ($request->input('member')) {
            foreach ($request->input('member') as $member) {
                $user_role = new UsersRoles;
                $user_role->ur_roles_name = $request->input('name');
                $user_role->ur_user_name = $member;
                $user_role->created_by = Session::get('username');
                $user_role->lastupdated_by = Session::get('username');
                $user_role->save();
            }
        }
        if ($request->input('privilege')) {
            foreach ($request->input('privilege') as $privilege) {
                $role_privilege = new RolesPrivileges;
                $role_privilege->rp_roles_name = $request->input('name');
                $role_privilege->rp_priv_name = $privilege;
                $role_privilege->created_by = Session::get('username');
                $role_privilege->lastupdated_by = Session::get('username');
                $role_privilege->save();
            }
        }

        if ($role->save()) {
            return Redirect::to('roles')->with('success', 'Role Added Successfully');
        }else {
            return Redirect::to('roles')->with('error', 'Something Went Wrong When Added Role');
        }
    }

    public function view_roles($id)
    {
        $include = self::view_include();
        $role = Roles::find($id);
        $members_list = Users::orderBy('user_name', 'asc')->get();
        $privileges_list = Privileges::orderBy('priv_name', 'asc')->get();
        $user_role = UsersRoles::where('ur_roles_name', $role->roles_name)->get();
        $role_privilege = RolesPrivileges::where('rp_roles_name', $role->roles_name)->get();

        if ($role) {
            return view('siteadmin.view_roles')
            ->with('adminheader', $include['adminheader'])
            ->with('adminleftmenus', $include['adminleftmenus'])
            ->with('adminfooter', $include['adminfooter'])
            ->with('role', $role)
            ->with('members_list', $members_list)
            ->with('privileges_list', $privileges_list)
            ->with('user_role', $user_role)
            ->with('role_privilege', $role_privilege);
        }
        else {
            return Redirect::to('roles')->with('error', 'No query results');
        }
    }

    public function view_roles_submit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'id' => $request->input('id'),
                'name' => $request->input('name'),
                'label' => $request->input('label'),
                'description' => $request->input('description')
            ],
            [
                'id' => 'required',
                'name' => 'required|max:150|unique:nm_roles,roles_name,'.$request->input('id').',roles_id',
                'label' => 'required|max:200',
                'description' => 'max:500'
            ]
        );

        if($validator->fails()){
            return Redirect::to('view_privileges/'.$request->input('id'))->withErrors($validator);
        }

        $role = Roles::find($request->input('id'));
        $role_privilege = RolesPrivileges::where('rp_roles_name', $role->roles_name)->update(['rp_roles_name' => $request->input('name')]);
        $user_role = UsersRoles::where('ur_roles_name', $role->roles_name)->update(['ur_roles_name' => $request->input('name')]);
        $role->roles_name = $request->input('name');
        $role->roles_label = $request->input('label');
        $role->roles_description = $request->input('description');
        $role->lastupdated_by = Session::get('username');
        $role->save();

        $role_privilege_delete = RolesPrivileges::where('rp_roles_name', $request->input('name'))->forceDelete();
        $user_role_delete = UsersRoles::where('ur_roles_name', $request->input('name'))->forceDelete();
        if ($request->input('privilege')) {
            foreach ($request->input('privilege') as $privilege) {
                $role_privilege_input = new RolesPrivileges;
                $role_privilege_input->rp_roles_name = $request->input('name');
                $role_privilege_input->rp_priv_name = $privilege;
                $role_privilege_input->created_by = Session::get('username');
                $role_privilege_input->lastupdated_by = Session::get('username');
                $role_privilege_input->save();
            }
        }
        if ($request->input('member')) {
            foreach ($request->input('member') as $member) {
                $user_role_input = new UsersRoles;
                $user_role_input->ur_roles_name = $request->input('name');
                $user_role_input->ur_user_name = $member;
                $user_role_input->created_by = Session::get('username');
                $user_role_input->lastupdated_by = Session::get('username');
                $user_role_input->save();
            }
        }

        if ($role->save()) {
            return Redirect::to('roles')->with('success', 'Role Updated Successfully');
        }else {
            return Redirect::to('roles')->with('error', 'Something Went Wrong When Updated Role');
        }
    }

    public function delete_role($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => 'required'
            ]
        );

        if ($validator->fails()) {
            return Redirect::to('roles')->withErrors($validator);
        }

        $role = Roles::find($id);
        $role->deleted_by = Session::get('username');
        $role->lastupdated_by = Session::get('username');
        $role->is_deleted = 1;
        $role->save();
        $role->delete();

        if ($role->trashed()) {
            return Redirect::to('roles')->with('success', 'Role Deleted Successfully');
        }else {
            return Redirect::to('roles')->with('error', 'Something Went Wrong When Deleted Role');
        }
    }

    public function delete_roles(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        foreach ($request->input('id') as $id) {
            $role = Roles::find($id);
            $role->deleted_by = Session::get('username');
            $role->lastupdated_by = Session::get('username');
            $role->is_deleted = 1;
            $role->save();
            $role->delete();
        }

        if ($role->trashed()) {
            return Redirect::to('roles')->with('success', 'Roles Deleted Successfully');
        }else {
            return Redirect::to('roles')->with('error', 'Something Went Wrong When Deleted Roles');
        }
    }

    public function privileges()
    {
        $include = self::view_include();
        $privileges_list = Privileges::get();

        // dd($privileges_list);

        return view('siteadmin.privileges')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter'])
        ->with('privileges_list', $privileges_list);
    }

    public function add_privileges()
    {
        $include = self::view_include();

        return view('siteadmin.add_privileges')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter']);
    }

    public function add_privileges_submit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'name' => $request->input('name'),
                'label' => $request->input('label'),
                'description' => $request->input('description')
            ],
            [
                'name' => 'required|max:150|unique:nm_privileges,priv_name',
                'label' => 'required',
                'description' => 'required'
            ]
        );

        if($validator->fails()){
            return Redirect::to('add_privileges')->withErrors($validator);
        }

        $privilege = new Privileges;
        $privilege->priv_name = $request->input('name');
        $privilege->priv_label = $request->input('label');
        $privilege->priv_description = $request->input('description');
        $privilege->created_by = Session::get('username');
        $privilege->lastupdated_by = Session::get('username');
        $privilege->save();

        if ($privilege->save()) {
            return Redirect::to('privileges')->with('success', 'Privilege Added Successfully');
        }else {
            return Redirect::to('privileges')->with('error', 'Something Went Wrong When Added Privilege');
        }

    }

    public function view_privileges($id)
    {
        $include = self::view_include();
        $privilege = Privileges::find($id);

        if($privilege){
            return view('siteadmin.view_privileges')
            ->with('adminheader', $include['adminheader'])
            ->with('adminleftmenus', $include['adminleftmenus'])
            ->with('adminfooter', $include['adminfooter'])
            ->with('privilege', $privilege);
        }else {
            return Redirect::to('privileges')->with('error', 'No query results');
        }
    }

    public function view_privileges_submit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'id' => $request->input('id'),
                'name' => $request->input('name'),
                'label' => $request->input('label'),
                'description' => $request->input('description')
            ],
            [
                'id' => 'required',
                'name' => 'required|max:150|unique:nm_privileges,priv_name,'.$request->input('id').',priv_id',
                'label' => 'required',
                'description' => 'required'
            ]
        );

        if($validator->fails()){
            return Redirect::to('view_privileges/'.$request->input('id'))->withErrors($validator);
        }

        $privilege = Privileges::find($request->input('id'));
        $role_privilege = RolesPrivileges::where('rp_priv_name', $privilege->priv_name)->update(['rp_priv_name' => $request->input('name')]);
        $privilege->priv_name = $request->input('name');
        $privilege->priv_label = $request->input('label');
        $privilege->priv_description = $request->input('description');
        $privilege->lastupdated_by = Session::get('username');
        $privilege->save();




        if ($privilege->save()) {
            return Redirect::to('privileges')->with('success', 'Privilege Updated Successfully');
        }else {
            return Redirect::to('privileges')->with('error', 'Something Went Wrong When Updated Privilege');
        }

    }

    public function delete_privilege($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => 'required'
            ]
        );

        if ($validator->fails()) {
            return Redirect::to('privileges')->withErrors($validator);
        }

        $privilege = Privileges::find($id);
        $privilege->deleted_by = Session::get('username');
        $privilege->lastupdated_by = Session::get('username');
        $privilege->is_deleted = 1;
        $privilege->save();
        $privilege->delete();

        if ($privilege->trashed()) {
            return Redirect::to('privileges')->with('success', 'Privilege Deleted Successfully');
        }else {
            return Redirect::to('privileges')->with('error', 'Something Went Wrong When Deleted Privilege');
        }
    }

    public function delete_privileges(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        foreach ($request->input('id') as $id) {
            $privilege = Privileges::find($id);
            $privilege->deleted_by = Session::get('username');
            $privilege->lastupdated_by = Session::get('username');
            $privilege->is_deleted = 1;
            $privilege->save();
            $privilege->delete();
        }

        if ($privilege->trashed()) {
            return Redirect::to('privileges')->with('success', 'Privileges Deleted Successfully');
        }else {
            return Redirect::to('privileges')->with('error', 'Something Went Wrong When Deleted Privileges');
        }
    }

    public function password_reset_request($uuid)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $get_data = PasswordResetRequest::where('prr_request_uuid', $uuid)->where('prr_expired_ts', '>', $now)->first();
        if ($get_data != null) {
            return view('siteadmin.create_password')
            ->with('user_name', $get_data->prr_user_name);
        }else {
            dd('Your Request Is Invalid');
        }
    }

    public function create_password(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $salt = Uuid::generate()->string;
        $pwd_secret = hash('sha256', $salt.$request->input('password_new'));

        $user = Users::where('user_name', $request->input('user_name'))->first();
        if ($user) {
            $user_id = $user->user_id;

            $insert_password = Users::find($user_id);
            $insert_password->user_pwd_salt = $salt;
            $insert_password->user_pwd_secret = $pwd_secret;
            $insert_password->user_active = true;
            $insert_password->lastupdated_by = $request->input('user_name');
            $insert_password->save();

            if ($insert_password->save()) {
                return Redirect::to('siteadmin_login');
            }else {
                dd('Proses Gagal');
            }
        }else {
            dd('Nama Anda Tidak Terdaftar');
        }

    }

    public function reset_password(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        $user = Users::where('user_name', $request->input('user_name'))->first();
        $password_old = hash('sha256', $user->user_pwd_salt.$request->input('password_old'));
        if ($user->user_pwd_secret != $password_old) {
            return URL::previous()->with('error', 'Your old password is wrong');
        }
        if ($user) {
            $salt = $user->user_pwd_salt;
            $pwd_secret = hash('sha256', $salt.$request->input('password_new'));

            $reset_password = Users::find($user->user_id);
            $reset_password->user_pwd_secret = $pwd_secret;
            $reset_password->lastupdated_by = $request->input('user_name');
            $reset_password->save();

            if ($reset_password->save()) {
                return Redirect::to('siteadmin');
            }else {
                dd('Proses Gagal');
            }
        }else {
            dd('Nama Anda Tidak Terdaftar');
        }
    }

    public function audit_trail(Request $request)
    {
        $include = self::view_include();
        $from_date  = $request->input('from_date');
        if ($from_date != '') {
            $from_date = strtotime($from_date);
            $from_date = date('Y-m-d H:i:s', $from_date);
        }
        $to_date    = $request->input('to_date');
        if ($to_date != '') {
            $to_date = strtotime($to_date);
            $to_date = strtotime('+1 day', $to_date);
            $to_date = date('Y-m-d H:i:s', $to_date);
        }

        if ($from_date != '' && $to_date != '') {
            $audittrail = AuditTrail::whereBetween('aud_date', array($from_date, $to_date))->orderBy('aud_date', 'desc')->get();
        }
        elseif ($from_date != '' && $to_date == '') {
            $audittrail = AuditTrail::where('aud_date', '>=', $from_date)->orderBy('aud_date', 'desc')->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            $audittrail = AuditTrail::where('aud_date', '<=', $to_date)->orderBy('aud_date', 'desc')->get();
        }
        else {
            $audittrail = AuditTrail::orderBy('aud_date', 'desc')->get();
        }

        return view('siteadmin.audit_trail')
        ->with('adminheader', $include['adminheader'])
        ->with('adminleftmenus', $include['adminleftmenus'])
        ->with('adminfooter', $include['adminfooter'])
        ->with('audittrail', $audittrail);
    }

}
?>
