<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Country;
use App\Province;
use App\Customer;
use App\City;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class CityController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function view_include()
	{
		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}

            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", "settings")->with('privileges', $privileges);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menus')->with('privileges', $privileges);
            $adminfooter      = view('siteadmin.includes.admin_footer');
            $return = [
                'adminheader' => $adminheader,
                'adminleftmenus' => $adminleftmenus,
                'adminfooter' => $adminfooter
            ];
            return $return;
        } else {
            return Redirect::to('siteadmin');
        }
	}

    public function add_city()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $country_details = City::view_country_details();
            $province_details = Province::get();
            return view('siteadmin.add_city')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_details)->with('province_details', $province_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_city()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $citydetails = City::view_city_detail();

            return view('siteadmin.manage_cities')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('citydetails', $citydetails);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_city($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $country_details = City::view_country_details();
            $province_details = Province::get();

            $cityresult = City::show_city_detail($id);

            return view('siteadmin.edit_city')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('cityresult', $cityresult)->with('country_details', $country_details)->with('province_details', $province_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_city($id)
    {
        if (Session::has('userid')) {
            $affected = City::delete_city_detail($id);
            return Redirect::to('manage_city')->with('success', 'Cities Deleted successfully');
        } else {
            return Redirect::to('siteadmin');
        }


    }

    public function status_city_submit($id, $status)
    {
        if (Session::has('userid')) {
            $return = City::status_city($id, $status);
            return Redirect::to('manage_city')->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_city_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'province_name' => 'required',
                'city_name' => 'required',
                'city_lat' => 'required',
                'city_lng' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_city')->withErrors($validator->messages())->withInput();

            } else {
                $cityname              = Input::get('city_name');
                $province_code           = Input::get('province_name');
                $check_exist_city_name = City::check_exist_city_name($cityname, $province_code);

                if ($check_exist_city_name) {
                    return Redirect::to('add_city')->with('error', "City Already Exists")->withInput();
                } else {
                    $entry = array(

                        'ci_name' => Input::get('city_name'),
                        'ci_prov_id' => Input::get('province_name'),
                        'ci_lati' => Input::get('city_lat'),
                        'ci_long' => Input::get('city_lng'),
                        'ci_default' => 0,
                        'ci_status' => 1
                    );

                    $return = City::save_City_detail($entry);
                    return Redirect::to('manage_city')->with('success', 'Record Inserted Successfully');
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_city_submit()
    {
        if (Session::has('userid')) {

            //$inputs = Input::all();
            $data = Input::except(array(
                '_token'
            ));
            $id   = Input::get('city_id');
            $rule = array(
                'province_name' => 'required',
                'city_name' => 'required',
                'city_lat' => 'required',
                'city_lng' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_city/' . $id)->withErrors($validator->messages())->withInput();

            } else {

                $entry = array(
                    'ci_name' => Input::get('city_name'),
                    'ci_prov_id' => Input::get('province_name'),
                    'ci_lati' => Input::get('city_lat'),
                    'ci_long' => Input::get('city_lng')
                );

                $affected = City::update_City_detail($id, $entry);
                return Redirect::to('manage_city')->with('success', 'Record Updated Successfully');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function update_default_city_submit()
    {
        if (Session::has('userid')) {
            $id     = Input::get('default_city_id');
            $entry  = array(
                'ci_default' => 0
            );
            $return = City::update_default_city_submit1($entry);
            $entry  = array(
                'ci_default' => 1
            );
            $return = City::update_default_city_submit($id, $entry);
            return Redirect::to('manage_city')->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function apiCreateCity(Request $request)
    {
        $req_country = Province::where('prov_code', $request->input('ci_prov_code'))->first();
        $req_city = City::get_city($request->input('ci_code'));

        if($req_city!=null)
        {
            if($request->input('ci_prov_code')!=null)
            {
                $req_country = Province::where('prov_code', $request->input('ci_prov_code'))->first();
                if($req_country == null)
                {
                    return response('Province Code Tidak Ditemukan', 400);
                    die();
                }
            }

            $get_ci_id= City::get_city($request->input('ci_code'));

            if($request->input('ci_code')==null)
            {
                return response('Data Harus Diisi Dengan Benar', 400);
                die();
            }
            if($get_ci_id==null)
            {
                return response('City Code Tidak Ditemukan', 400);
                die();
            }

            $row = City::find($get_ci_id->ci_id);
            if($request->input('ci_name')!=null)
                $row->ci_name=$request->input('ci_name');
            if($request->input('ci_prov_code')!=null)
                $row->ci_prov_id=$req_country->prov_id;
            if($request->input('ci_lati')!=null)
                $row->ci_lati=$request->input('ci_lati');
            if($request->input('ci_long')!=null)
                $row->ci_long=$request->input('ci_long');
            if($request->input('ci_default')!=null)
                $row->ci_default=$request->input('ci_default');
            if($request->input('ci_status')!=null)
                $row->ci_status=$request->input('ci_status');

            $row->save();

            return response('Sukses', 200);
        }
        if($request->input('ci_code')==null
        || $request->input('ci_name')==null
        || $request->input('ci_prov_code')==null)
        {
            return response('Data Harus Diisi Dengan Benar', 400);
            die();
        }
        if($req_country == null)
        {
            return response('Province Code Tidak Ditemukan. Silahkan Mengisi Data Province Terlebih Dahulu', 400);
            die();
        }

        $row = new City();
        $row->ci_code=$request->input('ci_code');
        $row->ci_name=$request->input('ci_name');
        $row->ci_prov_id=$req_country->prov_id;
        if($request->input('ci_lati')!=null)
            $row->ci_lati=$request->input('ci_lati');
        else
            $row->ci_lati=='';
        if($request->input('ci_long')!=null)
            $row->ci_long=$request->input('ci_long');
        else
            $row->ci_long='';
        if($request->input('ci_default')!=null)
            $row->ci_default=$request->input('ci_default');
        else
            $row->ci_default=0;
        if($request->input('ci_status')!=null)
            $row->ci_status=$request->input('ci_status');
        else
            $row->ci_status=1;


        $row->save();

        return response('Sukses', 200);
    }

    public function apiUpdateCity(Request $request)
    {
        if($request->input('ci_prov_code')!=null)
        {
            $req_country = Province::where('prov_code', $request->input('ci_prov_code'))->first();
            if($req_country == null)
            {
                return response('Province Code Tidak Ditemukan', 400);
                die();
            }
        }

        $get_ci_id= City::get_city($request->input('ci_code'));

        if($request->input('ci_code')==null)
        {
            return response('Data Harus Diisi Dengan Benar', 400);
            die();
        }
        if($get_ci_id==null)
        {
            return response('City Code Tidak Ditemukan', 400);
            die();
        }

        $row = City::find($get_ci_id->ci_id);
        if($request->input('ci_name')!=null)
            $row->ci_name=$request->input('ci_name');
        if($request->input('ci_prov_code')!=null)
            $row->ci_prov_id=$req_country->prov_id;
        if($request->input('ci_lati')!=null)
            $row->ci_lati=$request->input('ci_lati');
        if($request->input('ci_long')!=null)
            $row->ci_long=$request->input('ci_long');
        if($request->input('ci_default')!=null)
            $row->ci_default=$request->input('ci_default');
        if($request->input('ci_status')!=null)
            $row->ci_status=$request->input('ci_status');

        $row->save();

        return response('Sukses', 200);
    }

    public function apiDeleteCity(Request $request)
    {
        $get_ci_id = City::get_city($request->input('ci_code'));

        if($get_ci_id==null)
        {
            return response('City Code Tidak Ditemukan', 400);
            die();
        }
        $deleteCityAtMerchant = DB::table('nm_merchant')->where('mer_ci_id', $get_ci_id->ci_id)->update(['mer_ci_id'=>'']);

        $row = City::find($get_ci_id->ci_id);
        $row->delete();

        return response('Sukses', 200);
    }

    public function getCity(Request $request)
    {
        $get_city = DB::table('nm_city')->where('ci_prov_id', $request->input('aState'))->where('ci_status', 1)->get();

        return $get_city;
    }

    public function apiPutDistrict(Request $request)
    {
        $dis_code = $request->input('dis_code');
        if($request->input('dis_name')==null)
        {
            $dis_name = '';
        }
        else
        {
            $dis_name = $request->input('dis_name');
        }
        if($request->input('dis_city_code')==null)
        {
            $dis_city_id = '';
            $dis_city_code = null;
        }
        else
        {
            $dis_city_code = $request->input('dis_city_code');
        }

        $dis_status = $request->input('dis_status');

        $get_city = DB::table('nm_city')->where('ci_code', $dis_city_code)->first();

        if($get_city==null)
        {
            return response('Kode City Tidak Ditemukan', 400);
            die();
        }else {
            $dis_city_id = $get_city->ci_id;
        }
        if($dis_code==null)
        {
            return response('Masukkan Kode Kecamatan', 400);
            die();
        }

        $check_district = DB::table('nm_districts')->where('dis_code', $dis_code)->first();

        if($check_district!=null)
        {
            if($dis_name!=null)
                $name = DB::table('nm_districts')->where('dis_code', $dis_code)->update(['dis_name' => $dis_name]);
            if($get_city!=null)
                $city_id = DB::table('nm_districts')->where('dis_code', $dis_code)->update(['dis_city_id' => $dis_city_id]);
            if($dis_status!=null)
                $status = DB::table('nm_districts')->where('dis_code', $dis_code)->update(['dis_status' => $dis_status]);

            return response('Sukses', 200);
            die();
        }

        $insert = DB::table('nm_districts')->insert([
            'dis_code' => $dis_code,
            'dis_name' => $dis_name,
            'dis_city_id' => $dis_city_id,
            'dis_status' => 1]);

        return response('Sukses', 200);
    }

    public function apiPutSubdistrict(Request $request)
    {
        $sdis_code = $request->input('sdis_code');
        if($request->input('sdis_name')==null)
            $sdis_name = '';
        else
            $sdis_name = $request->input('sdis_name');
        if($request->input('sdis_district_code')==null)
        {
            $sdis_dis_id = '';
            $sdis_district_code = null;
        }
        else
            $sdis_district_code = $request->input('sdis_district_code');
        if($request->input('sdis_zip_code')==null)
            $sdis_zip_code = '';
        else
            $sdis_zip_code = $request->input('sdis_zip_code');

        $sdis_status = $request->input('sdis_status');

        $get_district = DB::table('nm_districts')->where('dis_code', $sdis_district_code)->first();

        if($get_district==null)
        {
            return response('Kode Kecamatan Tidak Ditemukan', 400);
            die();
        }else {
            $sdis_dis_id = $get_district->dis_id;
        }
        if($sdis_code==null)
        {
            return response('Masukkan Kode Kelurahan', 400);
            die();
        }

        $check_subdistrict = DB::table('nm_subdistricts')->where('sdis_code', $sdis_code)->first();

        if($check_subdistrict!=null)
        {
            if($sdis_name!='')
                $name = DB::table('nm_subdistricts')->where('sdis_code', $sdis_code)->update(['sdis_name' => $sdis_name]);
            if($get_district!=null)
                $dis_id = DB::table('nm_subdistricts')->where('sdis_code', $sdis_code)->update(['sdis_dis_id' => $sdis_dis_id]);
            if($sdis_zip_code!='')
                $zip_code = DB::table('nm_subdistricts')->where('sdis_code', $sdis_code)->update(['sdis_zip_code' => $sdis_zip_code]);
            if($sdis_status!='')
                $status = DB::table('nm_subdistricts')->where('sdis_code', $sdis_code)->update(['sdis_status' => $sdis_status]);

            return response('Sukses', 200);
            die();
        }

        $insert = DB::table('nm_subdistricts')->insert([
            'sdis_code' => $sdis_code,
            'sdis_name' => $sdis_name,
            'sdis_dis_id' => $sdis_dis_id,
            'sdis_zip_code' => $sdis_zip_code,
            'sdis_status' => 1]);

        return response('Sukses', 200);
    }

    public function getDistrict(Request $request){
        $get_district = '';
        if($request->input('aCity')!='')
        {
            $get_city_id = DB::table('nm_city')->where('ci_code', $request->input('aCity'))->first();
            $get_district = DB::table('nm_districts')->where('dis_city_id', $get_city_id->ci_id)->where('dis_status', 1)->get();
        }

        return $get_district;
    }

    public function getSubDistrict(Request $request){
        $get_subdistrict = '';
        if($request->input('aDistrict')!='')
        {
            $get_districts_id = DB::table('nm_districts')->where('dis_code', $request->input('aDistrict'))->first();
            $get_subdistrict = DB::table('nm_subdistricts')->where('sdis_dis_id', $get_districts_id->dis_id)->where('sdis_status', 1)->get();
        }

        return $get_subdistrict;
    }

    public function auto_get_city(Request $request){
        $get_city = DB::table('nm_city')->where('ci_name', 'like', '%'.$request->input('term').'%');
        if ($request->input('state') != '' && $request->input('state') != null) {
            $get_city = $get_city->where('ci_prov_id', $request->input('state'));
        }
        $get_city = $get_city->select('ci_name', 'ci_id', 'ci_code')->get();

        $return_city = [];

        foreach ($get_city as $city) {
            $array = [
                'value' => $city->ci_name,
                'label' => $city->ci_name,
                'id' => $city->ci_id,
                'code' => $city->ci_code,
            ];
            array_push($return_city, $array);
        }

        return $return_city;
    }

    public function auto_get_district(Request $request){
        $get_districts = DB::table('nm_districts')->where('dis_name', 'like', '%'.$request->input('term').'%');
        if ($request->input('city') != '' && $request->input('city')) {
            $get_districts = $get_districts->where('dis_city_id', $request->input('city'));
        }
        $get_districts = $get_districts->select('dis_name', 'dis_id', 'dis_code')->get();

        $return_districts = [];

        foreach ($get_districts as $districts) {
            $array = [
                'value' => $districts->dis_name,
                'label' => $districts->dis_name,
                'id' => $districts->dis_id,
                'code' => $districts->dis_code,
            ];
            array_push($return_districts, $array);
        }

        return $return_districts;
    }

    public function auto_get_subdistrict(Request $request){
        $get_subdistricts = DB::table('nm_subdistricts')->where('sdis_name', 'like', '%'.$request->input('term').'%');
        if ($request->input('district') != '' && $request->input('district') != null) {
            $get_subdistricts = $get_subdistricts->where('sdis_dis_id', $request->input('district'));
        }
        $get_subdistricts = $get_subdistricts->select('sdis_name', 'sdis_id', 'sdis_code', 'sdis_zip_code')->get();

        $return_subdistricts = [];

        foreach ($get_subdistricts as $subdistricts) {
            $array = [
                'value' => $subdistricts->sdis_name,
                'label' => $subdistricts->sdis_name,
                'id' => $subdistricts->sdis_id,
                'code' => $subdistricts->sdis_code,
                'zip_code' => $subdistricts->sdis_zip_code
            ];
            array_push($return_subdistricts, $array);
        }

        return $return_subdistricts;
    }
}
