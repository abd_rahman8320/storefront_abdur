<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Excel;
use Config;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Customer;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class CustomerController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
     public function view_include()
 	{
 		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}

             $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", "customer")->with('privileges', $privileges);
             $adminleftmenus   = view('siteadmin.includes.admin_left_menu_customer')->with('privileges', $privileges);
             $adminfooter      = view('siteadmin.includes.admin_footer');
             $return = [
                 'adminheader' => $adminheader,
                 'adminleftmenus' => $adminleftmenus,
                 'adminfooter' => $adminfooter
             ];
             return $return;
         } else {
             return Redirect::to('siteadmin');
         }
 	}

    public function customer_dashboard()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $website_users    = Customer::get_website_users();
            $fb_users         = Customer::get_fb_users();
            $admin_users      = Customer::get_admin_users();
            $logintoday       = Customer::get_logintoday_users();
            $login7days       = Customer::get_login7days_users();
            $login30days      = Customer::get_login30days_users();
            $login12mnth      = Customer::get_login12mnth_users();
            $ordermnth_count  = Customer::get_chart_detailsnew();
            $get_meta_details = Home::get_meta_details();
            return view('siteadmin.customer_dashboard')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('admin_user', $admin_users)->with('fb_users', $fb_users)->with('website_users', $website_users)->with('logintoday', $logintoday)->with('login7days', $login7days)->with('login30days', $login30days)->with('login12mnth', $login12mnth)->with('ordermnth_count', $ordermnth_count)->with('get_meta_details', $get_meta_details);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_customer()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $countryresult  = Customer::get_country();
            return view('siteadmin.add_customer')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('countryresult', $countryresult);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_customer($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $countryresult  = Customer::get_country();
            $customerresult = Customer::get_customer($id);
            return view('siteadmin.edit_customer')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('countryresult', $countryresult)->with('customerresult', $customerresult);
        } else {
            return Redirect::to('siteadmin');
        }

    }

    public function send_inquiry_email($id)
    {
        if (Session::has('userid')) {
            Session::put('inquiriesid', $id);
            $get_inquiry_details = Customer::get_inquiry_details_email($id);
            $email               = $get_inquiry_details[0]->iq_emailid;
            $name                = $get_inquiry_details[0]->iq_name;

            $send_mail_data = array(
                'name' => $name,
                'password' => $email,
                'email' => $email,
                'name' => $name
            );
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            # It will show these lines as error but no issue it will work fine Line no 119 - 122
            Mail::send('emails.marchantmail', $send_mail_data, function($message)
            {
                $id                  = Session::get('inquiriesid');
                $get_inquiry_details = Customer::get_inquiry_details_email($id);
                $email               = $get_inquiry_details[0]->iq_emailid;
                $name                = $get_inquiry_details[0]->iq_name;
                $message->to($email, $name)->subject('Reply To Your Inquiry');
            });

            return Redirect::to('manage_inquires')->with('success', 'Email Successfully sent');
        } else {
            return Redirect::to('siteadmin');
        }

    }

    public function manage_customer()
    {
        if (Session::has('userid')) {
            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }

            $customerrep    = Customer::get_customerreports($from_date, $to_date);
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $customerresult = Customer::get_customer_list();
            $citylist       = Customer::get_city_list();
            return view('siteadmin.manage_customer')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('customerresult', $customerresult)->with('customerrep', $customerrep)->with('cityresult', $citylist);
        } else {
            return Redirect::to('siteadmin');
        }

    }

    public function delete_customer($id)
    {
        if (Session::has('userid')) {
            $return = Customer::delete_customer($id);
            $delete_relation = DB::table('nm_customer_group_relation')->where('cgr_cus_id', $id)->delete();
            return Redirect::to('manage_customer')->with('delete_result', 'Record Deleted Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function status_customer_submit($id, $status)
    {
        if (Session::has('userid')) {
            $return = Customer::status_customer($id, $status);
            return Redirect::to('manage_customer')->with('updated_result', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function customer_edit_getcity()
    {
        $id         = $_GET['city_id'];
        $selectcity = Customer::get_customer_city($id);
        if ($selectcity) {
            $return = "";
            foreach ($selectcity as $selectcity_ajax) {
                $return .= "<option value='" . $selectcity_ajax->ci_id . "' selected> " . $selectcity_ajax->ci_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value='0'> No datas found </option>";
        }
    }

    public function add_customer_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $date = date('m/d/Y');
            $rule = array(
                'customer_first_name' => 'required',
                'customer_Email' => 'required|email',
                'customer_phone' => 'required|numeric',
                'customer_address1' => 'required',
                'customer_address2' => 'required',
                'select_customer_country' => 'required',
                'select_customer_city' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_customer')->withErrors($validator->messages())->withInput();
            }

            else {
                $emailaddr = Input::get('customer_Email');


                $checkemailaddr = Customer::check_emailaddress($emailaddr);

                if ($checkemailaddr) {
                    return Redirect::to('add_customer')->withMessage("Already EmailId Exists")->withInput();
                } else {
                    $uname   = Input::get('customer_first_name');
                    $address = Input::get('customer_Email');
                    $mdpwd   = md5(Input::get('customer_first_name'));
                    $entry   = array(

                        'cus_name' => Input::get('customer_first_name'),
                        'cus_email' => Input::get('customer_Email'),
                        'cus_pwd' => $mdpwd,
                        'cus_phone' => Input::get('customer_phone'),
                        'cus_address1' => Input::get('customer_address1'),
                        'cus_address2' => Input::get('customer_address2'),
                        'cus_country' => Input::get('select_customer_country'),
                        'cus_city' => Input::get('select_customer_city'),
                        'cus_logintype' => 2,
                        'created_date' => $date
                    );


                    $return = Customer::insert_customer($entry);
                    include('../SMTP/sendmail.php');
                    $subject      = "You Account has been created.....";
                    $emailsubject = "You Account has been created.....";
                    $message      = "Your Login Credentials <br>";
                    $message .= "Email: " . $address . "<br>";
                    $message .= "Password: " . $uname;

                     ob_start();
                     include('../Emailsub/customeraccountcreation.php');
                     $body = ob_get_contents();
                     ob_clean();

                    Send_Mail($address, $subject, $body);

                    return Redirect::to('manage_customer');

                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_customer_submit()
    {
        if (Session::has('userid')) {
            $data       = Input::except(array(
                '_token'
            ));
            $customerid = Input::get('customer_edit_id');

            $rule = array(
                'customer_first_name' => 'required',
                'customer_Email' => 'required|email',
                'customer_phone' => 'required|numeric',
                'customer_address1' => 'required',
                'customer_address2' => 'required',
                'select_customer_country' => 'required',
                'select_customer_city' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_customer/' . $customerid)->withErrors($validator->messages())->withInput();
            }

            else {

                $emailaddr      = Input::get('customer_Email');
                $checkemailaddr = Customer::check_emailaddress_edit($emailaddr, $customerid);

                if ($checkemailaddr) {

                    return Redirect::to('edit_customer/' . $customerid)->withMessage('Already EmailId Exists')->withInput();
                } else {

                    $entry = array(

                        'cus_name' => Input::get('customer_first_name'),
                        'cus_email' => Input::get('customer_Email'),
                        'cus_phone' => Input::get('customer_phone'),
                        'cus_address1' => Input::get('customer_address1'),
                        'cus_address2' => Input::get('customer_address2'),
                        'cus_country' => Input::get('select_customer_country'),
                        'cus_city' => Input::get('select_customer_city')
                    );


                    $return = Customer::update_customer($customerid, $entry);
                    return Redirect::to('manage_customer')->with('updated_result', 'Record Updated successfully');
                }

            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_subscribers()
    {
        if (Session::has('userid')) {
            Session::put('newsubscriberscount', 0);
            Customer::update_subscriber_msgstatus();
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $subscriber_list = Customer::subscriber_list();
            return view('siteadmin.manage_subscribers')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('subscriber_list', $subscriber_list);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_subscriber($id)
    {
        if (Session::has('userid')) {
            $return = Customer::delete_subscriber($id);
            return Redirect::to('manage_subscribers')->with('success', 'Record Deleted Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_subscriber_status($id, $status)
    {
        if (Session::has('userid')) {
            $return = Customer::edit_subscriber_status($id, $status);
            return Redirect::to('manage_subscribers')->with('success', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_inquires()
    {
        if (Session::has('userid')) {

            $from_date  = Input::get('from_date');
            if ($from_date != '') {
                $from_date = strtotime($from_date);
                $from_date = date('Y-m-d H:i:s', $from_date);
            }
            $to_date    = Input::get('to_date');
            if ($to_date != '') {
                $to_date = strtotime($to_date);
                $to_date = strtotime('+1 day', $to_date);
                $to_date = date('Y-m-d H:i:s', $to_date);
            }
            $enquiresrep = Customer::get_enquires($from_date, $to_date);
            $count_enquiries = Customer::count_enquiries();
            Session::put('inquiriescnt', $count_enquiries);
            Customer::update_inquiries_msgstatus();
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $enquires_list  = Customer::enquires_list();
            return view('siteadmin.manage_inquires')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('enquires_list', $enquires_list)->with('enquiresrep', $enquiresrep);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_inquires($id)
    {
        if (Session::has('userid')) {
            $return = Customer::delete_inquires($id);
            return Redirect::to('manage_inquires')->with('success', 'Record Deleted Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_referral_users()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            $referral_list  = Customer::referral_list();
            return view('siteadmin.manage_referral_users')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('referral_list', $referral_list);
        }

        else {
            return Redirect::to('siteadmin');
        }
    }

    public function download_customer()
    {
        if (Session::has('userid')) {
            Excel::create('Data Customer', function($excel) {
                date_default_timezone_set('Asia/Jakarta');
                $date = date('d/m/Y H:i:s');
                // Set the title
                $excel->setTitle('Data Customer');

                // Chain the setters
                $excel->setCreator('Kukuruyuk')->setCompany('Kukuruyuk');

                // Call them separately
                $excel->setDescription('Data Customer Kukuruyuk');

                $excel->sheet('sheet1', function($sheet) use($date) {
                    $customerresult = Customer::get_customer_list();
                    $sheet->row(1, array(
                        'nama' => 'Nama Dokumen',
                        'data' => 'Data Customer Kukuruyuk'
                    ));
                    $sheet->row(2, array('Tanggal', $date));
                    $sheet->cells('A4:I4', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setBorder('thick', 'thick', 'thick', 'thick');
                    });
                    $sheet->row(4, array(
                        'Nama',
                        'Alamat 1',
                        'Alamat 2',
                        'Negara',
                        'Kota',
                        'Email',
                        'Telepon',
                        'Tipe login',
                        'Tanggal Bergabung'
                    ));
                    $sheet->setFreeze('A5');
                    $i = 5;
                    foreach($customerresult as $result)
                    {
                        $sheet->cells('A'.$i.':I'.$i, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cells('G'.$i, function($cells) {
                            $cells->setAlignment('left');
                        });
                        if($result->cus_logintype == 1){
                            $type = 'Admin User';
                        }elseif ($result->cus_logintype == 2) {
                            $type = 'Website User';
                        }elseif ($result->cus_logintype == 3) {
                            $type = 'Facebook User';
                        }else {
                            $type = 'Unknown User';
                        }
                        $data = array(
                            'Nama' => $result->cus_name,
                            'Alamat 1' => $result->cus_address1,
                            'Alamat 2' => $result->cus_address2,
                            'Negara' => $result->co_name,
                            'Kota' => $result->ci_name,
                            'Email' => $result->cus_email,
                            'Telepon' => $result->cus_phone,
                            'Tipe Login' => $type,
                            'Tanggal Bergabung' => $result->cus_joindate
                        );
                        $sheet->row($i, $data);
                        $i++;
                    }
                });
            })->export('xlsx');
        }

        else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_customer_group()
    {
        $include = self::view_include();
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        $get_customer_group = DB::table('nm_customer_group')->get();

        return view('siteadmin.add_customer_group')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('get_customer_group', $get_customer_group);
    }

    public function add_customer_group_submit(Request $request)
    {
        $get_customer_group = DB::table('nm_customer_group')->orderBy('cus_group_code', 'desc')->first();
        if ($get_customer_group == null) {
            $code = 1;
        }else {
            $code = intval($get_customer_group->cus_group_code) + 1;
        }
        $entry = [
            'cus_group_code' => $code,
            'cus_group_desc' => $request->input('customer_group')
        ];

        $insert = DB::table('nm_customer_group')->insert($entry);
        if ($insert) {
            return Redirect::to('add_customer_group')->with('success', 'Customer Group Has Been Added Successfully');
        }else {
            return Redirect::to('add_customer_group')->with('error', 'Something Went Wrong When Add Customer Group');
        }
    }

    public function edit_customer_group($id)
    {
        $include = self::view_include();
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        $customer = DB::table('nm_customer_group')->where('cus_group_id', $id)->first();

        return view('siteadmin.edit_customer_group')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('customer', $customer);
    }

    public function edit_customer_group_submit(Request $request)
    {
        $entry = [
            'cus_group_desc' => $request->input('cus_group_desc')
        ];
        $update = DB::table('nm_customer_group')
        ->where('cus_group_id', $request->input('cus_group_id'))
        ->update($entry);

        if ($update) {
            return Redirect::to('add_customer_group')->with('success', 'Customer Group Has Been Updated Successfully');
        }else {
            return Redirect::to('add_customer_group')->with('error', 'Something Went Wrong When Update Customer Group');
        }
    }

    public function delete_customer_group($id)
    {
        $delete = DB::table('nm_customer_group')->where('cus_group_id', $id)->delete();
        $delete_relation = DB::table('nm_customer_group_relation')->where('cgr_cus_group_id', $id)->delete();

        if ($delete) {
            return Redirect::to('add_customer_group')->with('success', 'Customer Group Has Been Deleted Successfully');
        }else {
            return Redirect::to('add_customer_group')->with('error', 'Something Went Wrong When Delete Customer Group');
        }
    }

    public function manage_customer_group(Request $request)
    {
        $include = self::view_include();
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        $get_customer_group = DB::table('nm_customer_group')->get();
        $get_customer = '';
        if ($request->input('cus_group_id')) {
            $get_customer = DB::table('nm_customer')
            ->leftJoin('nm_customer_group_relation', 'nm_customer_group_relation.cgr_cus_id', '=', 'nm_customer.cus_id')
            ->where('nm_customer_group_relation.cgr_cus_group_id', $request->input('cus_group_id'))
            ->get();
        }else {
            if ($get_customer_group != null) {
                $get_customer = DB::table('nm_customer')
                ->leftJoin('nm_customer_group_relation', 'nm_customer_group_relation.cgr_cus_id', '=', 'nm_customer.cus_id')
                ->where('nm_customer_group_relation.cgr_cus_group_id', $get_customer_group[0]->cus_group_id)
                ->get();
            }
        }

        return view('siteadmin.manage_customer_group')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('get_customer', $get_customer)
        ->with('get_customer_group', $get_customer_group)
        ->with('cus_group_id', $request->input('cus_group_id'));
    }

    public function add_customer_group_relation(Request $request)
    {
        $include = self::view_include();
        $adminheader 	= $include['adminheader'];
        $adminleftmenus = $include['adminleftmenus'];
        $adminfooter 	= $include['adminfooter'];

        $customer_group = DB::table('nm_customer_group')->where('cus_group_id', $request->input('cus_group_id'))->first();
        $get_customer = DB::table('nm_customer')->where('cus_status', 0)->get();

        return view('siteadmin.add_customer_group_relation')
        ->with('adminheader', $adminheader)
        ->with('adminleftmenus', $adminleftmenus)
        ->with('adminfooter', $adminfooter)
        ->with('customer_group', $customer_group)
        ->with('get_customer', $get_customer);
    }

    public function add_customer_group_relation_submit(Request $request)
    {
        $entry = [
            'cgr_cus_id' => $request->input('cus_id'),
            'cgr_cus_group_id' => $request->input('cus_group_id')
        ];

        $insert = DB::table('nm_customer_group_relation')->insert($entry);

        if ($insert) {
            return Redirect::to('manage_customer_group')->with('success', 'Customer Has Been Added To Group Successfully');
        }else {
            return Redirect::to('manage_customer_group')->with('error', 'Something Went Wrong When Added Customer To Group');
        }
    }

    public function delete_customer_group_relation($cus_id, $cus_group_id)
    {
        $delete = DB::table('nm_customer_group_relation')->where('cgr_cus_id', $cus_id)->where('cgr_cus_group_id', $cus_group_id)->delete();

        if ($delete) {
            return Redirect::to('manage_customer_group')->with('success', 'Customer Has Been Removed From Group Successfully');
        }else {
            return Redirect::to('manage_customer_group')->with('error', 'Something Went Wrong When Remove Customer From Group');
        }
    }

    public function get_customer_group_relation(Request $request)
    {
        $get_customer = DB::table('nm_customer')
        ->leftJoin('nm_customer_group_relation', 'nm_customer_group_relation.cgr_cus_id', '=', 'nm_customer.cus_id')
        ->where('nm_customer_group_relation.cgr_cus_group_id', $request->input('cus_group_id'))
        ->get();

        return $get_customer;
    }
}
