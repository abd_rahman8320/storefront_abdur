<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Userlogin;
use App\Customerprofile;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Dompdf\Dompdf;

class CustomerprofileController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */


    public function download_invoice($id_transaksi)
    {
        // getting header transaksi
        $transaction_header           = Home::get_transaction_header($id_transaksi);
            //dd($transaction_header);
        // getting detail transaksi
        $transaction_detail           = Home::get_transaction_detail2($id_transaksi);

        $url = env('VAR_1');

        $dompdf = new Dompdf();

        $html = '<html><body>';
            foreach ($data as $details)
            {
                $html.= $details->firstname . "<br>";
            }

            $html .= '</body></html>';

            $html2 = '
                <h2 style="text-align:center;">Invoice Customer</h2>
                <hr>
                <div id="kiri" style="float:left; width:50%;">
                    <table style="font-weight:bold; font-size: 19px; margin-top: 15px; width: 90%">
                        <tr style="margin-top:10px;">
                            <td>Metode Pembayaran</td>
                            <td style="margin-left:10%">: '; $html2 .= $transaction_header->metode_pembayaran.'</td>';
                        $html2 .= '
                        </tr>
                        <tr>
                            <td>ID Transaksi</td>
                            <td style="margin-left:10%">:'; $html2 .= $transaction_header->transaction_id.'<td>';
                        $html2 .= '
                        </tr>
                    </table>
                </div>
                        ';
                $html2 .= '
                <div id="kanan" style="float:left; width:50%;">
                <h3>Rincian Pesanan</h3>
                <table style="font-size: 19px; margin-top: 15px; width: 100%">
                    <tr style="margin-top:10px; border: 1px solid black;">
                        <td>Product Name</td>
                        <td>Wa</td>
                        <td>Product Quantity</td>
                        <td>Amount</td>
                    </tr>
                ';
                    foreach($transaction_detail as $orderdet)
                    {
                        echo "kocak";
                    }
                    $html2 ='

                    ';

        $dompdf->load_html('
                <h2 style="text-align:center;">Invoice Customer</h2>
                <hr>
                <div id="kiri" style="float:left; width:50%;">
                    <table style="font-weight:bold; font-size: 19px; margin-top: 15px; width: 90%">
                        <tr style="margin-top:10px;">
                            <td>Metode Pembayaran</td>
                            <td style="margin-left:10%">: '.$transaction_header->metode_pembayaran.'</td>
                        </tr>
                        <tr>
                            <td>ID Transaksi</td>
                            <td style="margin-left:10%">: '.$transaction_header->transaction_id.'</td>
                        </tr>
                    </table>
                </div>

                <div id="kanan" style="float:left; width:50%;">
                    <h3>Rincian Pesanan</h3>
                    <table style="font-size: 19px; margin-top: 15px; width: 100%">
                        <tr style="margin-top:10px; border: 1px solid black;">
                            <td>Product Name</td>
                            <td>Wa</td>
                            <td>Product Quantity</td>
                            <td>Amount</td>
                        </tr>

                        <tr>
                            <td>'.$transaction_header->transaction_id.'</td>
                            <td style="margin-left:10%">: '.$transaction_header->transaction_id.'</td>
                        </tr>
                    </table>
                </div>
            ');

        // // (Optional) Setup the paper size and orientation
         $dompdf->setPaper('A4', 'landscape');

        // // Render the HTML as PDF
         $dompdf->render();

        // // Output the generated PDF to Browser
         $dompdf->stream();
    }

   public function hapus_file_bukti($id_transaksi)
    {
        $bukti     = Customerprofile::hapus_file_bukti($id_transaksi);

        return Redirect::to('info_pembayaran/'.$id_transaksi);
    }

    public function download_file_bukti($fname)
    {
        $destinationPath = public_path('assets/buktiTransfer/').$fname;

        return response()->download($destinationPath);
    }

    public function get_base64_file_by_id($id)
    {
        return DB::table('nm_dokumen_merchant')->where('id', '=', $id)->select('base64_file')->first();
    }

    public function profile_image_submit()
    {

        $customerid = Session::get('customerid');
        $inputs   = Input::all();
        $file     = Input::file('imgfile');
        $filename = $file->getClientOriginalName();
        $move_img = explode('.', $filename);
        $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
        $destinationPath = './assets/profileimage/';
        $uploadSuccess   = Input::file('imgfile')->move($destinationPath, $filename);
        $updateimage     = Customerprofile::update_profileimage($customerid, $filename);
        if ($updateimage) {
            return Redirect::to('user_profile');
        }
    }

    public function konfirmasi_barang_cus(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $date = date('Y-m-d H-i-sa');

        $data = array(
            'id_transaksi' => $request->input('transaksi_id'),
            'tgl_update' => $date,
            'status' => 5,
            );

        Customerprofile::insert_history_approval($data);
        Customerprofile::update_status_outbound($request->input('transaksi_id'));

        return Redirect::to('info_pembayaran/'.$request->input('transaksi_id'));
    }

    public function update_status_konfirmasi($order_id, $transaksi_id)
    {
        $get_order = DB::table('nm_order')->select('order_use_warranty', 'order_tgl_konfirmasi_penerimaan_barang')->where('order_id', $order_id)->first();
        date_default_timezone_set('Asia/Bangkok');
            $date = date('Y-m-d H:i:s');

            $basic_warranty = DB::table('nm_setting_warranty')->where('id', 1)->sum('lama_garansi_dasar');
            $extended_warranty = 90;
            if ($get_order->order_use_warranty == 1) {
                $lama_garansi = $basic_warranty + $extended_warranty;
            }else {
                $lama_garansi = $basic_warranty;
            }
            $warranty_date = date('Y-m-d H:i:s', strtotime('+'.$lama_garansi.' days', strtotime($date)));

            Customerprofile::update_status_konfirmasi($order_id, $date, $warranty_date);

            Customerprofile::update_outboud_order($order_id);

            $det_pro = Customerprofile::get_detail_product_by_order_id($order_id);

            //dd($det_pro);
            $pro_id = base64_encode($det_pro->pro_id);

            return Redirect::to('productcomment/'.$det_pro->mc_name.'/'.$det_pro->smc_name.'/'.$det_pro->sb_name.'/'.$det_pro->ssb_name.'/'.$pro_id);

    }

    public function upload_bukti_transfer_bank_file(Request $request)
    {
        $transaksi_id = $request->input('transaksi_id');
        $get_detail_transaksi = Home::get_transaction_header($transaksi_id);

        $customerid = Session::get('customerid');
        $inputs   = Input::all();
        $file     = Input::file('upload_bukti');
        $filename = $file->getClientOriginalName();
        $move_img = explode('.', $filename);
        $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
        $destinationPath = './assets/buktiTransfer/';
        $uploadSuccess   = Input::file('upload_bukti')->move($destinationPath, $filename);
        //dd($transaksi_id);

        $data = array(
            'transaction_id' => $transaksi_id,
            'bukti_transfer' => $filename,
            'status_pembayaran' => 'belum dibayar'
            );

        $bukti     = Customerprofile::insert_bukti_transfer($data);

        if($get_detail_transaksi->metode_pembayaran == "Columbia")
        {
            return Redirect::to('show_payment_result_columbia/'.$transaksi_id);
        }
        else
        {
            return Redirect::to('info_pembayaran/'.$transaksi_id);
        }
    }

    public function info_pembayaran($id_transaksi)
    {
        $cust_id                      = Session::get('customerid');
        $cus_details                  = Home::get_customer_details_by_id($cust_id);

        $facebook_id                  = Session::get('facebook_id');
        $city_details                 = Register::get_city_details();
        $header_category              = Home::get_header_category();

        $most_visited_product         = Home::get_most_visited_product();

        $get_product_details_by_cat   = Home::get_product_details_by_category($header_category);
        $category_count               = Home::get_category_count($header_category);
        $get_product_details_typeahed = Home::get_product_details_typeahed();
        $main_category                = Home::get_header_category();
        $sub_main_category            = Home::get_sub_main_category($main_category);
        $second_main_category         = Home::get_second_main_category($main_category, $sub_main_category);
        $second_sub_main_category     = Home::get_second_sub_main_category();
        $get_social_media_url         = Home::get_social_media_url();
        $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
        $cms_page_title               = Home::get_cms_page_title();
        $country_details              = Register::get_country_details();
        $addetails                    = Home::get_ad_details();
        $noimagedetails               = Home::get_noimage_details();
        $getbannerimagedetails        = Home::getbannerimagedetails();
        $getmetadetails               = Home::getmetadetails();
        $getlogodetails               = Home::getlogodetails();

        $get_contact_det              = Footer::get_contact_details();
        $country_details              = Register::get_country_details();
        $getanl                       = Settings::social_media_settings();
        $general                      = Home::get_general_settings();

        $get_pay_settings             = Settings::get_pay_settings();
        //dd($get_pay_settings);

        $get_pay_settings_new         = Settings::get_pay_settings_new();
        //dd($get_pay_settings_new);

        $getorderdetails              = Home::getorderdetails_trs($id_transaksi);
        //dd($getorderdetails);
        $getdetailtransaksi           = Home::get_detail_transaksi($id_transaksi);
        //dd($getdetailtransaksi);
        $last_status_outbound         = Customerprofile::get_last_history($id_transaksi);
        //dd($last_status_outbound);
        $get_pay                      = Settings::get_pay_settings();
        $get_history_transaksi        = Home::get_history_transaksi($id_transaksi);
        $get_cur                      = $get_pay[0]->ps_cursymbol;
        $get_pm_img_details           = Footer::get_paymentmethod_img_details();

        //dd($getorderdetails);

        if($cus_details[0]->cus_id != $getorderdetails[0]->order_cus_id)
        {
            return Redirect::to('');
        }

        if (Session::has('customerid')) {
            $navbar = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        } else {
            $navbar = view('includes.navbar')->with('country_details', $country_details)->with('metadetails', $getmetadetails)->with('general', $general)->with('getanl', $getanl);
        }

        $header = view('includes.header')->with('header_category', $header_category)->with('logodetails', $getlogodetails);
        $footer = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('cms_page_title', $cms_page_title)->with('get_partners', $get_partners)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
        $headertop = view('includes.headertop')->with('metadetails', $getmetadetails);

        // dd($get_history_transaksi);

        return view('indo_pembayaran_customer')
        ->with('navbar', $navbar)
        ->with('header', $header)
        ->with('metadetails', $getmetadetails)
        ->with('get_cur', $get_cur)
        ->with('cus_details', $cus_details)
        ->with('footer', $footer)
        ->with('header_category', $header_category)
        ->with('get_product_details_by_cat', $get_product_details_by_cat)
        ->with('most_visited_product', $most_visited_product)
        ->with('category_count', $category_count)
        ->with('get_product_details_typeahed', $get_product_details_typeahed)
        ->with('main_category', $main_category)
        ->with('sub_main_category', $sub_main_category)
        ->with('second_main_category', $second_main_category)
        ->with('second_sub_main_category', $second_sub_main_category)
        ->with('addetails', $addetails)
        ->with('noimagedetails', $noimagedetails)
        ->with('bannerimagedetails', $getbannerimagedetails)
        ->with('get_meta_details', $getmetadetails)
        ->with('orderdetails', $getorderdetails)
        ->with('get_contact_det', $get_contact_det)
        ->with('get_pay_settings', $get_pay_settings)
        ->with('get_pay_settings_new', $get_pay_settings_new)
        ->with('getdetailtransaksi', $getdetailtransaksi)
        ->with('get_history_transaksi', $get_history_transaksi)
        ->with('last_status_outbound', $last_status_outbound)
        ->with('general', $general);
    }

    public function get_userprofile()
    {
        if (Session::has('customerid')) {
            $customerid          = Session::get('customerid');
			      $facebook_id         = Session::get('facebook_id');
            $get_category_header = Home::get_category_header();
            $product_details     = Home::get_product_details();
            $customerdetails     = Customerprofile::get_customer_details($customerid);
            //dd($customerdetails);
            $general = Customerprofile::get_general_settings();
            $wishlistdetails = Customerprofile::get_wishlistdetails($customerid);
            $wishlistcnt     = Customerprofile::get_wishlistdetailscnt($customerid);
            $bidhistory      = Customerprofile::get_bidhistory($customerid);
            $bidhistorycnt   = Customerprofile::get_bidhistorycnt($customerid);
            $checkcustomership = Customerprofile::get_customer_shipping_details($customerid,$facebook_id);
            $city_details                = Register::get_city_details();
            $header_category             = Home::get_header_category();
            $country_details             = Register::get_country_details();
            $getproductordersdetails     = Customerprofile::getproductordersdetails($customerid);
            //dd($getproductordersdetails);
            $getdetailtransaksi2         = Customerprofile::get_detail_transaksi($customerid);
            $getproductordersdetailss    = Customerprofile::getproductordersdetailss($customerid);
            $cms_page_title              = Home::get_cms_page_title();
            $get_social_media_url        = Footer::get_social_media_url();
            $get_meta_details            = Home::get_meta_details();
            $get_image_favicons_details  = Home::get_image_favicons_details();
            $get_image_logoicons_details = Home::get_image_logoicons_details();
            $getlogodetails              = Home::getlogodetails();
            $get_contact_det             = Footer::get_contact_details();
            $getanl                      = Settings::social_media_settings();
            $get_partners                 = DB::table('nm_partners')->where('par_status', 0)->get();
            $get_pm_img_details           = Footer::get_paymentmethod_img_details();
            $navbar                      = view('includes.loginnavbar')->with('country_details', $country_details)->with('metadetails', $get_meta_details)->with('general', $general)->with('getanl', $getanl);
            $header                      = view('includes.header')->with('header_category', $header_category)->with('product_name', '')->with('get_image_logoicons_details', $get_image_logoicons_details)->with('logodetails', $getlogodetails);
            $footer                      = view('includes.footer')->with('pm_image_details',$get_pm_img_details)->with('get_partners', $get_partners)->with('cms_page_title', $cms_page_title)->with('get_social_media_url', $get_social_media_url)->with('get_contact_det', $get_contact_det)->with('getanl', $getanl);
            $shippingdetails             = Customerprofile::get_shipping_details($customerid);
            // dd($shippingdetails);
            $get_pay                      = Settings::get_pay_settings();
            $get_cur                      = $get_pay[0]->ps_cursymbol;
            // dd($getdetailtransaksi2);
            $citycode = '';
            $discode = '';
            $sdiscode = '';
            $zipcode = '';
            $cityname = '';
            $disname = '';
            $sdisname = '';
            if($shippingdetails != null && !empty($shippingdetails) && $shippingdetails != 0){
                foreach($shippingdetails as $shipping_info){}
                    // dd($shipping_info);

                if($shipping_info->ship_ci_id!=0){
                    $get_citycode = DB::table('nm_city')->where('ci_id', $shipping_info->ship_ci_id)->first();
                    if($get_citycode == null){
                        $citycode = '';
                        $cityname = '';
                    }else {
                        $citycode = $get_citycode->ci_code;
                        $cityname = $get_citycode->ci_name;
                    }
                }
                if($shipping_info->ship_dis_id!=0){
                    $get_discode = DB::table('nm_districts')->where('dis_id', $shipping_info->ship_dis_id)->first();
                    if($get_discode == null){
                        $discode = '';
                        $disname = '';
                    }else {
                        $discode = $get_discode->dis_code;
                        $disname = $get_discode->dis_name;
                    }
                }
                if($shipping_info->ship_sdis_id!=0){
                    $get_sdiscode = DB::table('nm_subdistricts')->where('sdis_id', $shipping_info->ship_sdis_id)->first();
                    if($get_sdiscode == null){
                        $sdiscode = '';
                        $sdisname = '';
                        $zipcode = '';
                    }else {
                        $sdiscode = $get_sdiscode->sdis_code;
                        $sdisname = $get_sdiscode->sdis_name;
                        $zipcode = $get_sdiscode->sdis_zip_code;
                    }

                }
            }
            $metode_pembayaran =[];
            foreach ($getdetailtransaksi2 as $detail) {
                if ($detail->payment_channel == '') {
                    $payment_channel = null;
                }else {
                    $payment_channel = $detail->payment_channel;
                }
                $metode_pembayaran_raw = DB::table('nm_payment_method_header')
                ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
                ->where('nm_paymnet_method_detail.kode_pay_detail', $detail->order_paytype)
                ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
                ->first();
                if ($metode_pembayaran_raw == null) {
                    $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
                    ->where('kode_pay_detail', $detail->order_paytype)
                    ->first();
                    array_push($metode_pembayaran, $metode_pembayaran_raw->nama_pay_detail);
                }else {
                    array_push($metode_pembayaran, $metode_pembayaran_raw->nama_pay_header);
                }
            }


            if ($checkcustomership) {
                return view('customer_profile')->with('zipcode', $zipcode)->with('cityname', $cityname)->with('disname', $disname)->with('sdisname', $sdisname)->with('citycode', $citycode)->with('discode', $discode)->with('sdiscode', $sdiscode)
                ->with('get_cur', $get_cur)->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('customerdetails', $customerdetails)
                ->with('city_details', $city_details)->with('country_details', $country_details)->with('shippingdetails', $shippingdetails)->with('hasship', 1)
                ->with('getproductordersdetails', $getproductordersdetails)->with('getproductordersdetailss', $getproductordersdetailss)
                ->with('metadetails', $get_meta_details)->with('get_image_favicons_details', $get_image_favicons_details)->with('wishlistdetails', $wishlistdetails)
                ->with('wishlistcnt', $wishlistcnt)->with('bidhistory', $bidhistory)->with('bidhistorycnt', $bidhistorycnt)->with('product_details', $product_details)
                ->with('get_contact_det', $get_contact_det)->with('general', $general)
                ->with('getdetailtransaksi2', $getdetailtransaksi2)->with('metode_pembayaran', $metode_pembayaran);
            } else {
                return view('customer_profile')->with('zipcode', $zipcode)->with('cityname', $cityname)->with('disname', $disname)->with('sdisname', $sdisname)->with('citycode', $citycode)->with('discode', $discode)->with('sdiscode', $sdiscode)->with('get_cur', $get_cur)
                ->with('navbar', $navbar)->with('header', $header)->with('footer', $footer)->with('customerdetails', $customerdetails)->with('country_details', $country_details)
                ->with('shippingdetails', $shippingdetails)->with('hasship', 0)->with('getproductordersdetails', $getproductordersdetails)->with('metadetails', $get_meta_details)
                ->with('get_image_favicons_details', $get_image_favicons_details)->with('wishlistdetails', $wishlistdetails)->with('wishlistcnt', $wishlistcnt)
                ->with('bidhistory', $bidhistory)->with('bidhistorycnt', $bidhistorycnt)->with('product_details', $product_details)->with('get_contact_det', $get_contact_det)
                ->with('general', $general);
            }
        }

        else {
            return Redirect::to('/');
        }

    }

    public function user_subscription_submit()
    {
      if (Session::has('customerid')) {
          $data = Input::except(
              array(
              '_token'
              )
          );

          $email       = Input::get('email');
          $check_email = Register::check_email_ajaxs($email);
          if ($check_email) {
              return Redirect::back()->with('Error_letter', 'Already Use Email Exist');
          } else {
              $email = Input::get('email');
              $entry = array(
                  'email' => Input::get('email')
              );
              $email = Register::insert_email($entry);
          }
        }
        else {
          return Redirect::to('index');
        }
        return Redirect::back()->with('subscribe', 'Your Email Subscribed Successfully');
    }

    public function update_username_ajax()
    {
        $customerid  = Session::get('customerid');
        $cname       = $_GET['cname'];
        $checkinsert = Customerprofile::update_customer_name($cname, $customerid);
        if ($checkinsert) {
            echo "success," . $cname;
        } else {
            echo "fail,";
        }
    }

    public function update_phonenumber_ajax()
    {
        $customerid  = Session::get('customerid');
        $phonenum    = $_GET['phonenum'];
        $checkupdate = Customerprofile::update_phonenumber($phonenum, $customerid);
        if ($checkupdate) {
            echo "success," . $phonenum;
        } else {
            echo "fail,";
        }
    }

    public function update_city_ajax()
    {
        $customerid = Session::get('customerid');
        $cityid     = $_GET['cityid'];
        $countryid  = $_GET['countryid'];

        $citynameres    = Customerprofile::getcityname($cityid);
        $cityname       = $citynameres[0]->ci_name;
        $countrynameres = Customerprofile::getcountryname($countryid);
        $countryname    = $countrynameres[0]->co_name;
        $checkupdate    = Customerprofile::update_city($cityid, $countryid, $customerid);
        if ($checkupdate) {
            echo "success," . $countryname . "," . $cityname;
        } else {
            echo "fail,";
        }
    }

    public function update_shipping_ajax()
    {
        $customerid    = Session::get('customerid');
        $shipcus       = $_GET['shipcus'];
        $shipaddr1     = $_GET['shipaddr1'];
        $shipaddr2     = $_GET['shipaddr2'];
        $shipcusmobile = $_GET['shipcusmobile'];
        $shipcusemail  = $_GET['shipcusemail'];
        $shippingstate = $_GET['shippingstate'];
        $zipcode       = $_GET['zipcode'];
        $cityid        = $_GET['shippingcity'];
        $countryid     = $_GET['shippingcountry'];
        $district      = $_GET['shippingdistrict'];
        $subdistrict   = $_GET['shippingsubdistrict'];

        // $get_cityid = DB::table('nm_city')->where('ci_code', $city)->first();
        // $get_district = DB::table('nm_districts')->where('dis_code', $districtcode)->first();
        // $get_subdistrict = DB::table('nm_subdistricts')->where('sdis_code', $subdistrictcode)->first();

        // $cityid = $get_cityid->ci_id;
        // $district = $get_district->dis_id;
        // $subdistrict = $get_subdistrict->sdis_id;

        $entry         = array(
            'ship_name' => $shipcus,
            'ship_address1' => $shipaddr1,
            'ship_address2' => $shipaddr2,
            'ship_ci_id' => $cityid,
            'ship_state' => $shippingstate,
            'ship_country' => $countryid,
            'ship_postalcode' => $zipcode,
            'ship_phone' => $shipcusmobile,
            'ship_email' => $shipcusemail,
            'ship_order_id' => 0,
            'ship_cus_id' => $customerid,
            'ship_dis_id' => $district,
            'ship_sdis_id' => $subdistrict
        );

        $entry2 = [
            'cus_name' => $shipcus,
            'cus_address1' => $shipaddr1,
            'cus_address2' => $shipaddr2,
            'cus_phone' => $shipcusmobile,
            'cus_email' => $shipcusemail,
            'cus_city' => $cityid,
            'cus_country' => $countryid
        ];

        $checkcustomerid = Customerprofile::get_customer_shipping_details($customerid);

        if ($checkcustomerid) {

            $return = Customerprofile::update_shipping($entry, $customerid);
            $return1 = Customerprofile::update_customer_profile($entry2, $customerid);

            if ($return) {

                echo "success";
            } else {
                echo "fail";
            }
        } else {

            $return = Customerprofile::insert_shipping($entry);

            if ($return) {
                echo "success";
            } else {
                echo "fail";
            }



        }

    }

    public function register_getcity_shipping()
    {
        $cityid    = $_GET['id'];
        $city_ajax = Merchant::get_city_detail_ajax_shipping($cityid);
        if ($city_ajax) {
            $return = "";

            foreach ($city_ajax as $fetch_city_ajax) {
                $return .= "<option value='" . $fetch_city_ajax->ci_id . "'> " . $fetch_city_ajax->ci_name . " </option>";
            }
            echo $return;
        } else {
            echo $return = "<option value=''> No datas found </option>";
        }

    }

    public function update_address_ajax()
    {
        $customerid = Session::get('customerid');
        $addr1      = $_GET['addr1'];
        $addr2      = $_GET['addr2'];
        if ($_GET['addr1'] != "" && $_GET['addr2'] != "") {
            $addr1            = $_GET['addr1'];
            $checkupdateaddr1 = Customerprofile::update_address1($addr1, $customerid);
            $checkupdateaddr2 = Customerprofile::update_address2($addr2, $customerid);
            echo "success," . $addr1 . "," . $addr2;

        } else if ($_GET['addr1'] != "") {
            $addr2            = $_GET['addr1'];
            $checkupdateaddr1 = Customerprofile::update_address2($addr1, $customerid);
            echo "success," . $addr1 . "," . $addr2;
        } else if ($_GET['addr2'] != "") {
            $addr2            = $_GET['addr2'];
            $checkupdateaddr2 = Customerprofile::update_address2($addr2, $customerid);
            echo "success," . $addr1 . "," . $addr2;
        }

    }

    public function update_password_ajax()
    {
        $customerid    = Session::get('customerid');
        $oldpwd        = $_POST['oldpwd'];
        $md5oldpwd     = md5($oldpwd);
        $newpwd        = $_POST['newpwd'];
        $confirmpwd    = $_POST['confirmpwd'];
        $md5confirmpwd = md5($confirmpwd);
        $oldpwdcheck   = Customerprofile::check_oldpwd($customerid, $md5oldpwd);

        if ($newpwd != $confirmpwd) {
            echo "fail1,";
        } else {
            if ($oldpwdcheck) {
                $updatecheck = Customerprofile::update_newpwd($customerid, $md5confirmpwd);
                if ($updatecheck) {
                    echo "success,";
                } else {

                    echo "fail2,";

                }

            }

        }


    }

    public function user_logout()
    {
        Session::forget('customerid');
        Session::forget('username');
        Session::flush();
        return Redirect::to('/');
    }
    public function facebook_logout()
    {
        //Session::forget('customerid');
        //Session::forget('username');
        Session::flush();
        return Redirect::to('/');
    }


}
