<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Config;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Merchantadminlogin;
use App\AdminModel;
use App\Users;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function view_include()
	{
		if (Session::has('userid')) {
			$user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}

            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", "settings")->with('privileges', $privileges);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menus')->with('privileges', $privileges);
            $adminfooter      = view('siteadmin.includes.admin_footer');
            $return = [
                'adminheader' => $adminheader,
                'adminleftmenus' => $adminleftmenus,
                'adminfooter' => $adminfooter
            ];
            return $return;
        } else {
            return Redirect::to('siteadmin');
        }
	}

	public function chart()
	{
	   $result = AdminModel::get_chart_details();
	   return view('siteadmin.chart_view');
	}

	public function admin_settings()
	{
		if(Session::has('userid'))
		{
			$include = self::view_include();
			$adminheader 	= $include['adminheader'];
			$adminleftmenus = $include['adminleftmenus'];
			$adminfooter 	= $include['adminfooter'];
			$user = Users::find(Session::get('userid'));
			$user_role = UsersRoles::where('ur_user_name', $user->user_name)->get();
			return view('siteadmin.admin_settings')
			->with('adminheader', $adminheader)
			->with('adminfooter', $adminfooter)
			->with('user' , $user)
			->with('user_role', $user_role);
		}
		else
		{
			return Redirect::to('siteadmin');
		}
	}

	public function admin_profile()
	{
		if(Session::has('userid'))
		{
			$include = self::view_include();
			$adminheader 	= $include['adminheader'];
			$adminleftmenus = $include['adminleftmenus'];
			$adminfooter 	= $include['adminfooter'];
			//   $admin_setting_details = AdminModel::get_admin_profile_details();
			$user = Users::find(Session::get('userid'));
			$user_role = UsersRoles::where('ur_user_name', $user->user_name)->get();
			return view('siteadmin.admin_profile')
			->with('adminheader', $adminheader)
			->with('adminfooter', $adminfooter)
			->with('user', $user)
			->with('user_role', $user_role);
		}
		else
		{
			return Redirect::to('siteadmin');
		}
		}

	public function admin_settings_submit(Request $request)
	{
		// $data = Input::except(array('_token')) ;
		$validator = Validator::make(
            [
                'name' => $request->input('name'),
                'full_name' => $request->input("full_name"),
                'email' => $request->input('email')
            ],
            [
                'name' => 'required|max:150|unique:nm_user,user_name,'.$request->input('id').',user_id',
                'full_name' => 'required|max:150',
                'email' => 'required|max:150'
            ]
        );

		if ($validator->fails())
		{
			return Redirect::to('admin_settings')->withErrors($validator->messages())->withInput();
		}
		else
		{
			$user = Users::find($request->input('id'));
			$user_role = UsersRoles::where('ur_user_name', $user->user_name)->update(['ur_user_name'=>$request->input('name')]);
			$user->user_name = $request->input('name');
			$user->user_profile_full_name = $request->input('full_name');
			$user->user_profile_email = $request->input('email');
			$user->save();

			return Redirect::to('admin_settings')->with('success', 'Record Updated Successfully');
		}
	}

	public function siteadmin()
	{
  		return view('siteadmin.admin_login');
	}

	public function siteadmin_login()
	{
		return view('siteadmin.admin_login');
	}

	public function login_check()
	{
		$inputs = Input::all();
		$uname = Input::get('admin_name');
		$password = Input::get('admin_pass');
		// $check = Merchantadminlogin::login_check($uname,$password);

		$check_username = Users::where('user_name', $uname)->where('user_active', 1)->first();
		if ($check_username) {
			$salt = $check_username->user_pwd_salt;
			$pwd_secret = hash('sha256', $salt.$password);
			$check = Users::where('user_name', $uname)->where('user_pwd_secret', $pwd_secret)->where('user_active', 1)->first();

			if($check)
			{
				Session::put('userid', $check->user_id);
	            Session::put('username', $check->user_name);
				$user_role = UsersRoles::where('ur_user_name', $check->user_name)->get();

				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'siteadmin_dashboard') {
	                    	return Redirect::to('siteadmin_dashboard')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'general_setting') {
	                    	return Redirect::to('general_setting')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'add_logo') {
	                    	return Redirect::to('add_logo')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'deals_dashboard') {
	                    	return Redirect::to('deals_dashboard')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'product_dashboard') {
	                    	return Redirect::to('product_dashboard')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'manage_customer') {
	                    	return Redirect::to('manage_customer')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'manage_merchant') {
	                    	return Redirect::to('manage_merchant')->with('login_success','Login Success');
	                    }
					}
				}
				foreach ($user_role as $ur) {
					$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
					foreach ($role_privilege as $rp) {
						$priv_name = explode(' ', $rp->rp_priv_name);
	                    if ($priv_name[4] == 'product_all_orders') {
	                    	return Redirect::to('product_all_orders')->with('login_success','Login Success');
	                    }
					}
				}
				// return Redirect::to('siteadmin_dashboard')->with('login_success','Login Success');
			}
			else
			{
				return Redirect::to('siteadmin')->with('login_error','Invalid Username and Password');
			}
		}else {
			return Redirect::to('siteadmin')->with('login_error','Invalid Username and Password');
		}
	}

	public function forgot_check()
	{
	  $inputs = Input::all();
	  $email = Input::get('admin_email');
          $check = Merchantadminlogin::forgot_check($email);
	  if($check > 0)
	  {
	  $forgot_check = Merchantadminlogin::forgot_check_details($email);
	  $email = $forgot_check[0]->adm_email;
	  $name = 'admin';
	  $send_mail_data = array('name' => $forgot_check[0]->adm_fname, 'password' =>$forgot_check[0]->adm_password);
	  $admin = DB::table('nm_emailsetting')->first();
	  $admin_email = $admin->es_noreplyemail;
	  Config::set('mail.from.address', $admin_email);
	  Mail::send('emails.admin_passwordrecoverymail', $send_mail_data, function($message) use ($email)
          {
          $message->to($email,'Admin')->subject('Password Recovery Details');
       	  });
  	  return Redirect::to('siteadmin')->with('forgot_success','Mail Send Successfully');
	  }
	  else
	  {
	  return Redirect::to('siteadmin')->with('forgot_error','Invalid Email');
	  }
	}

	public function admin_logout()
	{
          Session::forget('userid');
	  Session::forget('username');
	  Session::flush();
	  return Redirect::to('siteadmin')->with('login_success','Logout Success');
	}

  }
