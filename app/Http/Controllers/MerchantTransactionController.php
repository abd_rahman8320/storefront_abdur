<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Config;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Products;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\MerchantTransactions;
use App\AuditTrail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Log;
use Carbon;
use App\Commands\Command;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class MerchantTransactionController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function input_nomor_resi_merchant()
    {
        $order_id = Input::get('id_order_no_resi');
        $status_outbound = Input::get('status_outbound_no_resi');
        $nomor_resi = Input::get('nomor_resi');

        $get_transaction_header = MerchantTransactions::get_transaction_header_by_order_id($order_id);
        $get_cus_data_order_by_order_id = Transactions::get_cus_data_order_by_order_id($order_id);
        //get
        $get_data_by_order_id = Transactions::get_data_by_order_id($order_id);
        $for_data_order_by_trans_id = Transactions::for_data_order_by_trans_id($get_cus_data_order_by_order_id->transaction_id);

        date_default_timezone_set('Asia/Bangkok');
        $tgl = Date('Y-m-d H:i:s');
        $tgl_estimasi_diterima = date('Y-m-d H:i:s', strtotime("+".$get_transaction_header->trans_lama_ship." days"));

        // update nm_order
        MerchantTransactions::update_tgl_pesanan_dikirim_merchant($order_id, $tgl, $status_outbound, $nomor_resi);

        // update nm_transaksi
        MerchantTransactions::update_trans_estimasi_ship($get_transaction_header->transaction_id, $tgl_estimasi_diterima);

        //update nm_product
        MerchantTransactions::update_stock_merchant($order_id);

        if ($get_cus_data_order_by_order_id->payment_channel == '') {
            $payment_channel = null;
        }else {
            $payment_channel = $get_cus_data_order_by_order_id->payment_channel;
        }
        $metode_pembayaran_raw = DB::table('nm_payment_method_header')
        ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
        ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_order_by_order_id->order_paytype)
        ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
        ->first();
        if ($metode_pembayaran_raw == null) {
            $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
            ->where('kode_pay_detail', $get_cus_data_order_by_order_id->order_paytype)
            ->first();
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
        }else {
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
        }

        $send_mail_data = array(
            'id_transaksi' => $get_cus_data_order_by_order_id->transaction_id,
            'cus_name' => $get_cus_data_order_by_order_id->cus_name,
            'cus_shipaddress' => $get_cus_data_order_by_order_id->ship_address1,
            'metode_pay' => $metode_pembayaran,
            'ship_service' => $get_cus_data_order_by_order_id->postalservice_code,
            'no_resi' => $nomor_resi,
            'items_order' => $for_data_order_by_trans_id
        );
        $admin = DB::table('nm_emailsetting')->first();
        $admin_email = $admin->es_noreplyemail;
        Config::set('mail.from.address', $admin_email);
        Mail::send(
            'emails.notifikasi_email_shipping_checkout',
            $send_mail_data, function ($message) use ($get_data_by_order_id,$get_cus_data_order_by_order_id){
                $message->to($get_cus_data_order_by_order_id->cus_email)
                ->subject('Notifikasi Barang Dikirim');
            }
        );

        $add_days_plus_3 = floatval($get_transaction_header->trans_lama_ship) + 3;

        // push ke queue untuk auto recieving
        $date = \Carbon\Carbon::now()->addDay(floatval($add_days_plus_3));
        \Queue::later($date, new \App\Commands\AutoRecieving($get_transaction_header->transaction_id));

        return Redirect::to('merchant_product_all_orders');
    }

    public function update_outbound_merchant_submit(Request $request)
    {
        // -- start gaya baru
        $order_id = $request->input('orderid');
        $status_outbound = $request->input('status_outbound');
        //dd($order_id);
        date_default_timezone_set('Asia/Bangkok');
        $tgl = Date('Y-m-d H:i:s');
        $get_cus_data_order_by_order_id = Transactions::get_cus_data_order_by_order_id($order_id);
        //get
        $get_data_by_order_id = Transactions::get_data_by_order_id($order_id);
        $for_data_order_by_trans_id = Transactions::for_data_order_by_trans_id($get_data_by_order_id[0]->transaction_id);

        if ($get_cus_data_order_by_order_id->payment_channel == '') {
            $payment_channel = null;
        }else {
            $payment_channel = $get_cus_data_order_by_order_id->payment_channel;
        }
        $metode_pembayaran_raw = DB::table('nm_payment_method_header')
        ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
        ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_order_by_order_id->order_paytype)
        ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
        ->first();
        if ($metode_pembayaran_raw == null) {
            $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
            ->where('kode_pay_detail', $get_cus_data_order_by_order_id->order_paytype)
            ->first();
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
        }else {
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
        }

        if($status_outbound == 2) // pick
        {
            // update colom nm_order.order_tgl_pesanan_diproses
            MerchantTransactions:: update_tgl_pesanan_diproses_merchant($order_id, $tgl,$status_outbound);
            // email pick
            $send_mail_data = array(
                'id_transaksi' => $get_data_by_order_id[0]->transaction_id,
                'cus_name' => $get_cus_data_order_by_order_id->cus_name,
                'cus_shipaddress' => $get_cus_data_order_by_order_id->order_shipping_add,
                'metode_pay' => $metode_pembayaran,
                'items_order' => $for_data_order_by_trans_id
            );
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
                'emails.notifikasi_email_pick',
                $send_mail_data, function ($message) use ($for_data_order_by_trans_id,$get_data_by_order_id,$get_cus_data_order_by_order_id){
                    $message->to($get_cus_data_order_by_order_id->cus_email)
                    ->subject('Notifikasi Barang Diproses');
                }
            );
        }
        elseif($status_outbound == 3) // pack
        {
            // update colom nm_order.order_tgl_pesanan_dikemas
            MerchantTransactions:: update_tgl_pesanan_dikemas_merchant($order_id, $tgl,$status_outbound);

            // email pick
            $send_mail_data = array(
                'id_transaksi' => $get_data_by_order_id[0]->transaction_id,
                'cus_name' => $get_cus_data_order_by_order_id->cus_name,
                'cus_shipaddress' => $get_cus_data_order_by_order_id->order_shipping_add,
                'metode_pay' => $metode_pembayaran,
                'items_order' => $for_data_order_by_trans_id
            );
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
                'emails.notifikasi_email_pack',
                $send_mail_data, function ($message) use ($for_data_order_by_trans_id,$get_data_by_order_id,$get_cus_data_order_by_order_id){
                    $message->to($get_cus_data_order_by_order_id->cus_email)
                    ->subject('Notifikasi Barang Dikemas');
                }
            );
        }
        elseif($status_outbound == 4) // shipp
        {
            // update colom nm_order.order_tgl_pesanan_dikirim
            MerchantTransactions:: update_tgl_pesanan_dikirim_merchant($order_id, $tgl, $status_outbound);
            $update_stock = MerchantTransactions::update_stock_merchant($order_id);
            // email pick
            $send_mail_data = array(
                'id_transaksi' => $get_cus_data_order_by_order_id->transaction_id,
                'cus_name' => $get_cus_data_order_by_order_id->cus_name,
                'cus_shipaddress' => $get_cus_data_order_by_order_id->ship_address1,
                'metode_pay' => $metode_pembayaran,
                'ship_service' => $get_cus_data_order_by_order_id->postalservice_code,
                'no_resi' => $get_cus_data_order_by_order_id->order_nomor_resi,
                'items_order' => $for_data_order_by_trans_id
            );
            $admin = DB::table('nm_emailsetting')->first();
            $admin_email = $admin->es_noreplyemail;
            Config::set('mail.from.address', $admin_email);
            Mail::send(
                'emails.notifikasi_email_shipping_checkout',
                $send_mail_data, function ($message) use ($get_data_order_by_trans_id,$get_cus_data_by_trans_id){
                    $message->to($get_cus_data_by_trans_id[0]->cus_email)
                    ->subject('Notifikasi Barang Dikirim');
                }
            );
        }
        elseif($status_outbound == 6) // lost
        {
            // update colom nm_order.order_tgl_pesanan_dikirim
            MerchantTransactions:: update_tgl_pesanan_diproses_merchant($order_id, $tgl, $status_outbound);
        }

        return Redirect::to('merchant_product_all_orders');
    }

    public function show_merchant_transactions()
    {
        if (Session::has('merchantid')) {
            $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');

            $producttransactioncnt = MerchantTransactions::get_producttransaction();
            $dealtransactioncnt    = MerchantTransactions::get_dealstransaction();
            $auctiontransactioncnt = MerchantTransactions::get_auctiontransaction();

            $producttoday  = MerchantTransactions::get_producttoday_order();
            $produst7days  = MerchantTransactions::get_product7days_order();
            $product30days = MerchantTransactions::get_product30days_order();

            $dealstoday  = MerchantTransactions::get_dealstoday_order();
            $deals7days  = MerchantTransactions::get_deals7days_order();
            $deals30days = MerchantTransactions::get_deals30days_order();

            $auctiontoday     = MerchantTransactions::get_auctiontoday_order();
            $auction7days     = MerchantTransactions::get_auction7days_order();
            $auction30days    = MerchantTransactions::get_auction30days_order();
            $merid            = Session::get('merchantid');
            $getproductidlist = Merchantproducts::getproductidlist($merid);
            $productlist      = $getproductidlist[0]->proid;

            $getauctionidlistrs = Merchant::getauctionidlist($merid);
            $getauctionidlist   = $getauctionidlistrs[0]->proid;

            $productchartdetails = MerchantTransactions::get_chart_product_details($productlist);
            $dealchartdetails    = MerchantTransactions::get_chart_deals_details($productlist);
            $auctionchartdetails = MerchantTransactions::get_chart_auction_details($getauctionidlist);


            return view('sitemerchant.merchant_transactiondashboard')->with('merchantheader', $adminheader)->with('merchantfooter', $adminfooter)->with('merchantleftmenus', $adminleftmenus)->with('producttoday', $producttoday)->with('produst7days', $produst7days)->with('product30days', $product30days)->with('dealstoday', $dealstoday)->with('deals7days', $deals7days)->with('deals30days', $deals30days)->with('auctiontoday', $auctiontoday)->with('auction7days', $auction7days)->with('auction30days', $auction30days)->with('producttransactioncnt', $producttransactioncnt)->with('dealtransactioncnt', $dealtransactioncnt)->with('auctiontransactioncnt', $auctiontransactioncnt)->with('productchartdetails', $productchartdetails)->with('dealchartdetails', $dealchartdetails)->with('auctionchartdetails', $auctionchartdetails);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function product_all_orders_new_h()
    {
        if(Session::get('merchantid'))
        {
            $from_date        = Input::get('from_date');
            $to_date          = Input::get('to_date');

            $merid            = Session::get('merchantid');

            $get_all_product_merchant_paid = MerchantTransactions::get_all_product_merchant_paid($merid);
            $get_all_product_merchant_paid_by_date = MerchantTransactions::get_all_product_merchant_paid_by_date($from_date, $to_date, $merid);
            $get_cus_data_order_by_order_id = Transactions::get_cus_data_order_by_order_id('121');
            $get_data_by_order_id = Transactions::get_data_by_order_id('121');
            $for_data_order_by_trans_id = Transactions::for_data_order_by_trans_id('unpay9562');
            //dd($for_data_order_by_trans_id);



            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');

            return view('sitemerchant.product_allorders_new')
            ->with('merchantheader', $merchantheader)
            ->with('merchantfooter', $merchantfooter)
            ->with('merchantleftmenus', $merchantleftmenus)
            ->with("get_all_product_merchant_paid", $get_all_product_merchant_paid)
            ->with("get_all_product_merchant_paid_by_date", $get_all_product_merchant_paid_by_date);
        }
        else
        {
            return Redirect::to('sitemerchant');
        }
    }

    public function product_all_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merid            = Session::get('merchantid');
            $getproductidlist = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist      = $getproductidlist[0]->proid;
                $orderdetails     = MerchantTransactions::getproduct_all_orders($productlist);
                $alltrans_reports = MerchantTransactions::alltrans_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $alltrans_reports = array();
            } else {
                $orderdetails = array();
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');

            return view('sitemerchant.product_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("alltrans_reports", $alltrans_reports);
        }

        else {
            return Redirect::to('sitemerchant');
        }

    }


    public function product_success_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {

                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getproduct_success_orders($productlist, $merid);
                //dd($orderdetails);
                $allsucessprod_reports = MerchantTransactions::allsucessprod_reports($from_date, $to_date, $productlist,$merid);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allsucessprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_success_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("allsucessprod_reports", $allsucessprod_reports);
        }

        else {
            return Redirect::to('sitemerchant');
        }
    }

    public function product_pick_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {

                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getproduct_pick_orders($productlist, $merid);
                //dd($orderdetails);
                $allsucessprod_reports = MerchantTransactions::allpickprod_reports($from_date, $to_date, $productlist,$merid);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allsucessprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_pick_order')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("allsucessprod_reports", $allsucessprod_reports);
        }

        else {
            return Redirect::to('sitemerchant');
        }
    }

    public function product_packing_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {

                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getproduct_packing_orders($productlist, $merid);
                //dd($orderdetails);
                $allsucessprod_reports = MerchantTransactions::allpackingprod_reports($from_date, $to_date, $productlist,$merid);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allsucessprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_packing_order')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("allsucessprod_reports", $allsucessprod_reports);
        }

        else {
            return Redirect::to('sitemerchant');
        }
    }

    public function product_shipping_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {

                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getproduct_shipping_orders($productlist, $merid);
                //dd($orderdetails);
                $allsucessprod_reports = MerchantTransactions::allshippingprod_reports($from_date, $to_date, $productlist,$merid);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allsucessprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_shipping_order')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("allsucessprod_reports", $allsucessprod_reports);
        }

        else {
            return Redirect::to('sitemerchant');
        }
    }

    public function product_completed_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails             = MerchantTransactions::getproduct_completed_orders($productlist);
                $allcompletedprod_reports = MerchantTransactions::allcompletedprod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allcompletedprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("allcompletedprod_reports", $allcompletedprod_reports);
        } else {
            return Redirect::to('sitemerchant');
        }
    }


    public function product_failed_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails          = MerchantTransactions::getproduct_failed_orders($productlist);
                $allfailedprod_reports = MerchantTransactions::allfailedprod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allfailedprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allholdprod_reports", $allfailedprod_reports);
        }

    }


    public function product_hold_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getproduct_hold_orders($productlist);

                $allholdprod_reports = MerchantTransactions::allholdprod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allholdprod_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.product_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allholdprod_reports", $allholdprod_reports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }



    public function cod_all_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails = MerchantTransactions::getcod_all_orders($productlist);

                $allprod_codreports = MerchantTransactions::allprod_codreports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allprod_codreports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.productcod_all_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("allprod_codreports", $allprod_codreports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function cod_completed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails = MerchantTransactions::getcod_completed_orders($productlist);

                $allpro_codcompleted_reports = MerchantTransactions::allpro_codcompleted_reports($from_date, $to_date, $productlist);
             } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allpro_codcompleted_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.productcod_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("allpro_codcompleted_reports", $allpro_codcompleted_reports);

        } else {
            return Redirect::to('sitemerchant');
        }

    }


    public function cod_failed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist           = $getproductidlist[0]->proid;
                $orderdetails          = MerchantTransactions::getcod_failed_orders($productlist);
                $allprod_failedreports = MerchantTransactions::allprod_failedreports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allprod_failedreports = array();
            } else {
                $orderdetails = array();
            }

            return view('sitemerchant.productcod_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allprod_failedreports", $allprod_failedreports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }


    public function cod_hold_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails = MerchantTransactions::getcod_hold_orders($productlist);

                $allprod_holdreports = MerchantTransactions::allprod_holdreports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allprod_holdreports     = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.productcod_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allprod_holdreports", $allprod_holdreports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function merchant_deals_all_orders($merid)
    {

        if (Session::get('merchantid')) {

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');

            $getproductidlist = MerchantTransactions::getdeals_all_orders($merid);
            if ($getproductidlist) {
                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::get_transaction_details($productlist);
             } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();

            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.deals_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails);


        } else {
            return Redirect::to('sitemerchant');
        }

    }


    public function deals_all_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');


            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');

            $getproductidlist = Merchantproducts::getproductidlist($merid);

            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails = MerchantTransactions::get_deals_all_orders($productlist);
                $dealrepp     = MerchantTransactions::getmerchant_dealreports($from_date, $to_date, $productlist);


            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $dealrepp     = array();
            } else {
                $orderdetails = array();
            }

            return view('sitemerchant.deals_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("dealrepp", $dealrepp);
        } else {
            return Redirect::to('sitemerchant');
        }

    }


    public function deals_success_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');


            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);

            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;


                $orderdetails           = MerchantTransactions::getdeals_success_orders($productlist);
                $getsuccess_dealreports = MerchantTransactions::getsuccess_dealreports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $getsuccess_dealreports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.deals_success_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("getsuccess_dealreports", $getsuccess_dealreports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function deals_completed_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                $orderdetails        = MerchantTransactions::getdeals_completed_orders($productlist);
                $getcomp_dealreports = MerchantTransactions::getcomp_dealreports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $getcomp_dealreports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.deals_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("getcomp_dealreports", $getcomp_dealreports);
        } else {
            return Redirect::to('sitemerchant');
        }
    }


    public function deals_failed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails      = Transactions::getdeals_failed_orders($productlist);
                $allfailed_reports = MerchantTransactions::allfailed_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allfailed_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.deals_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allfailed_reports", $allfailed_reports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function deals_hold_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails    = MerchantTransactions::getdeals_hold_orders($productlist);
                $allhold_reports = MerchantTransactions::allhold_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allhold_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.deals_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allhold_reports", $allhold_reports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function dealscod_all_orders()
    {
        if (Session::get('merchantid')) {
            $from_date         = Input::get('from_date');
            $to_date           = Input::get('to_date');
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;

                $orderdetails   = MerchantTransactions::getdealscod_all_orders($productlist);
                $allcod_reports = MerchantTransactions::allcod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allcod_reports = array();

            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.dealscod_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("allcod_reports", $allcod_reports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }


    public function dealscod_completed_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getdealscod_completed_orders($productlist);

                $allcodcompleted_reports = MerchantTransactions::allcodcompleted_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allcodcompleted_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.dealscod_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("allcodcompleted_reports", $allcodcompleted_reports);
        } else {
            return Redirect::to('sitemerchant');
        }
    }


    public function dealscod_failed_orders()
    {
        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getdealscod_failed_orders($productlist);

                $allcodfailed_reports = MerchantTransactions::allcodfailed_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allcodfailed_reports = array();
            } else {
                $orderdetails = array();
            }

            return view('sitemerchant.dealscod_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allcodfailed_reports", $allcodfailed_reports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }


    public function dealscod_hold_orders()
    {

        if (Session::get('merchantid')) {

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');

            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", "transaction");
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist        = $getproductidlist[0]->proid;
                $orderdetails       = MerchantTransactions::getdealscod_hold_orders($productlist);
                $allcodhold_reports = MerchantTransactions::allcodhold_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();
                $orderdetails = array();
                $allcodhold_reports = array();
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.dealscod_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allcodhold_reports", $allcodhold_reports);
        } else {
            return Redirect::to('sitemerchant');
        }

    }

    public function update_order_cod()
    {
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];

        $updaters = MerchantTransactions::update_cod_status($status, $orderid);
        if ($updaters) {
            echo "success";
        }

    }

    public function update_order_columbia()
    {
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];

        $updaters = MerchantTransactions::update_columbia_status($status, $orderid);
        if ($updaters) {
            echo "success";
        }

    }

    public function update_transfer_bank_order_status()
    {
        date_default_timezone_set('Asia/Bangkok');
        $tgl = Date('Y-m-d H:i:s');
        // -- status bayar nm_transaksi ----
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];

        if($status == 'sudah dibayar')
        {
            $order_status = 1;
        }
        else
        {
            $order_status = 3;
        }

        $updaters = MerchantTransactions::update_transfer_bank_status($status, $orderid, $order_status);
        $get_transaction = DB::table('nm_transaksi')->where('transaction_id', $orderid)->select('id')->first();
        $audit_trail = new AuditTrail;
        $audit_trail->aud_table = 'nm_transaksi';
        $audit_trail->aud_action = 'update';
        $audit_trail->aud_detail = '{"id":"'.$get_transaction->id.'","transaction_id":"'.$orderid.'","status_pembayaran":"'.$status.'","order_status":'.$order_status.'}';
        $audit_trail->aud_date = $tgl;
        $audit_trail->aud_user_name = Session::get('username');
        $audit_trail->save();
        // -- update history order -----

        $update_history = Transactions::update_history_per_order($orderid, $tgl);

        // -- poin ----

        $get_cus_data_by_trans_id = Transactions::get_cus_data_by_trans_id($orderid);
        $get_data_order_by_trans_id = Transactions::get_data_order_by_trans_id($orderid);

        //-- kondisi poin digunakan
        $get_detail_transaksi = Home::get_detail_transaksi($orderid);

        $jumlah_poin = (floatval($get_detail_transaksi[0]->total_belanja) + floatval($get_detail_transaksi[0]->shipping) - floatval($get_detail_transaksi[0]->diskon) - floatval($get_detail_transaksi[0]->poin_digunakan))/1000;

        if($get_detail_transaksi[0]->poin_digunakan > 0)
        {
            Transactions::update_poin_to_zero($get_cus_data_by_trans_id[0]->cus_id);
        }else {
            // -- jumlah poin customer lama + baru --
            $jumlah_poin = floatval($jumlah_poin) + floatval($get_cus_data_by_trans_id[0]->total_poin);
        }

        Transactions::change_jumlah_poin($jumlah_poin, $get_cus_data_by_trans_id[0]->cus_id);

        // recall unsync
        // $recall_unsync = app('App\Http\Controllers\HomeController')->recall_unsync();
        // dd($recall_unsync);
        if ($get_cus_data_by_trans_id[0]->payment_channel == '') {
            $payment_channel = null;
        }else {
            $payment_channel = $get_cus_data_by_trans_id[0]->payment_channel;
        }
        $metode_pembayaran_raw = DB::table('nm_payment_method_header')
        ->leftJoin('nm_paymnet_method_detail', 'nm_paymnet_method_detail.id_pay_detail', '=', 'nm_payment_method_header.id_pay_detail')
        ->where('nm_paymnet_method_detail.kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
        ->where('nm_payment_method_header.channel_pay_header', $payment_channel)
        ->first();
        if ($metode_pembayaran_raw == null) {
            $metode_pembayaran_raw = DB::table('nm_paymnet_method_detail')
            ->where('kode_pay_detail', $get_cus_data_by_trans_id[0]->order_paytype)
            ->first();
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_detail;
        }else {
            $metode_pembayaran = $metode_pembayaran_raw->nama_pay_header;
        }

        $send_mail_data = array(
            'id_transaksi' => $orderid,
            'cus_name' => $get_cus_data_by_trans_id[0]->cus_name,
            'cus_shipaddress' => $get_cus_data_by_trans_id[0]->ship_address1,
            'metode_pay' => $metode_pembayaran,
            'items_order' => $get_data_order_by_trans_id

        );

        //dd($send_mail_data);
        $admin = DB::table('nm_emailsetting')->first();
        $admin_email = $admin->es_noreplyemail;
        Config::set('mail.from.address', $admin_email);
        // Mail::send(
        //     'emails.notifikasi_email_konfirmasi_pembayaran',
        //     $send_mail_data, function ($message) use ($get_cus_data_by_trans_id){
        //         $message->to($get_cus_data_by_trans_id[0]->cus_email)
        //         ->subject('Notifikasi Pembayaran');
        //     }
        // );

        foreach ($get_data_order_by_trans_id as $row) {
                $select_mer_id = Transactions::get_data_mer_by_order_id($row->order_id);

                if($select_mer_id->postalservice_code == "JNE_REG"){
                    $postalservice = "JNE Reguler";
                }elseif ($select_mer_id->postalservice_code == "JNE_OK") {
                    $postalservice = "JNE OK";
                }elseif ($select_mer_id->postalservice_code == "JNE_YES") {
                    $postalservice = "JNE YES";
                }elseif ($select_mer_id->postalservice_code == "POPBOX") {
                    $postalservice = "Popbox";
                }elseif ($select_mer_id->postalservice_code == "AL") {
                    $postalservice = "Ambil Langsung";
                }else {
                    $postalservice = $select_mer_id->postalservice_code;
                }

            if($select_mer_id->mer_email != '' || $select_mer_id->mer_email != null)
            {
                $send_mail_to_mer = array(
                    'trans_id' => $select_mer_id->transaction_id,
                    'title' => $select_mer_id->pro_title,
                    'pro_sku_merchant' => $select_mer_id->pro_sku_merchant,
                    'order_qty' => $select_mer_id->order_qty,
                    'cus_name' =>$select_mer_id->cus_name,
                    'cus_phone' => $select_mer_id->cus_phone,
                    'postalservice' => $postalservice
                );
                $admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
                Config::set('mail.from.address', $admin_email);
                //     Mail::send(
                //     'emails.notifikasi_email_konfirmasi_ke_merchant',
                //     $send_mail_to_mer, function($message) use ($select_mer_id,$send_mail_to_mer){
                //         $message->to($select_mer_id->mer_email)
                //         ->subject('Notifikasi Transaksi');
                //     }
                // );
            }
            else{
            }
        }

        //$get_data_mer_by_trans_id = Transactions::get_data_mer_by_trans_id($orderid);
        if ($_GET['type'] == 'cash') {
            return Redirect::to('cash_hold_orders');
        }else{
            return Redirect::to('tfr_bank_hold_orders');
        }

    }

}
