<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Country;
use App\City;
use App\Popbox;
use MyPayPal;
use App\PlanTimer;
use App\Auction;
use App\Products;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class FilterController extends Controller
{
  public function search_filter(Request $request){
     $param = [];

      $param['pro_title']     = $request->input('pro_title');
      $param['skip']          = $request->input('skip', 0);
      $param['count']         = $request->input('count', 10);
      $param['min_price']     = $request->input('min_price');
      $param['max_price']     = $request->input('max_price');
      $param['mc_id']         = $request->input('mc_id');
      $param['smc_id']        = $request->input('smc_id');
      $param['sb_id']         = $request->input('sb_id');
      $param['ssb_id']        = $request->input('ssb_id');
      $param['grade']         = $request->input('grade');
      $param['spc_value']     = $request->input('spc_value');

      $result = Products::search_filter_category($param);
      $spc_result = Products::search_spc_filter_category($param);
      $grade_result = Products::search_grade_filter_category($param);
      if ($result['total'] == 0) {
          $result = Products::search_filter($param);
          $spc_result = Products::search_spc_filter($param);
          $grade_result = Products::search_grade_filter($param);
      }


     $data =  [
       "total" => $result['total'],
       "data" => $result['data'],
       "spc_result" => $spc_result,
       "grade_result" => $grade_result
     ];
     return response()->json($data);
  }

  public function jplist_filter(Request $request)
  {
      $param = [];
       $statuses      = json_decode(urldecode(stripslashes($request->input('statuses'))));
    //    var_dump($statuses);
    //    die();
       $param['pro_title']     = $statuses[2]->data->value;
       $param['skip']          = $request->input('skip', 0);
       $param['count']         = $statuses[0]->data->number;
       $param['min_price']     = $request->input('min_price');
       $param['max_price']     = $request->input('max_price');
       $param['mc_id']         = $request->input('mc_id');
       $param['smc_id']        = $request->input('smc_id');
       $param['sb_id']         = $request->input('sb_id');
       $param['ssb_id']        = $request->input('ssb_id');
       $param['grade']         = $request->input('grade');
       $param['spc_value']     = $request->input('spc_value');
       $sort_path              = $statuses[1]->data->path;
       if ($sort_path == '.title') {
           $param['sort_path'] = 'pro_title';
       }elseif ($sort_path == '.like') {
           $param['sort_path'] = 'pro_price';
       }else {
           $param['sort_path'] = null;
       }
       $param['order']         = $statuses[1]->data->order;
       $result = Products::search_filter($param);
       $spc_result = Products::search_spc_filter($param);
       $grade_result = Products::search_grade_filter($param);       

      $data = '';
      foreach ($result['data'] as $hasil) {
          $res = base64_encode($hasil->pro_id);
          $product_image = explode('/**/', $hasil->pro_Img);
          $product_title = substr($hasil->pro_title,0,25);
          $disprice = number_format($hasil->pro_disprice);
          $price = number_format($hasil->pro_price);
          $product_saving_price = $hasil->pro_price - $hasil->pro_disprice;
          if($hasil->pro_price==0 || $hasil->pro_price==null)
          {
              $product_discount_percentage = 0;
          }else {
              $product_discount_percentage = round(($product_saving_price/ $hasil->pro_price)*100,2);
          }
          $data .= '<div class="list" style="margin-left:-5px">';
          $data .= '<div class="list-item product" style="margin-right:5px!important;width:198px;">';
          $data .= '<div class="product__info">';
          $data .= '<div class="img">';
          $data .= '<a href="'.url("productview_spesial").'/'.$hasil->mc_name.'/'.$hasil->smc_name.'/'.$hasil->sb_name.'/'.$hasil->ssb_name.'/'.$res.'/'.$hasil->pro_title.'" target="_self">';
          $data .= '<img class="product__image" src="'.url('assets/product/').'/'.$product_image[0].'" alt="" title=""/></a></div>';
          $data .= '<div class="block">';
          $data .= '<p class="title product__title tab-title">'.$product_title.'</p>';
          if ($hasil->pro_disprice != 0) {
              $data .= '<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px;">';
              $data .= '<span style="color: red; text-align: bold;">Rp'.$disprice.'</span></p>';
              $data .= '<p style="display:none;" class="like tab-like">'.$disprice.'</p>';
              if ($product_discount_percentage != 0 && $product_discount_percentage != '') {
                  $data .= '<i class="tag-dis-prod"><div id="dis-val">'.round($product_discount_percentage).'%</div></i>';
              }
              $data .= '<p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color:#aaa; font-size: 13;">Rp'.$price.'</span></strike>';
          }
          else {
              $data .= '<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px;margin-bottom:14.5%;">	<span  style="color: red; text-align: bold;">Rp'.$price.'</span></p>';
              $data .= '<p style="display:none;" class="like tab-like">'.$price.'</p>';
          }
          if ($hasil->pro_no_of_purchase >= $hasil->pro_qty) {
              $data .= '<h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a>';
          }
          else {
              $data .= '</div><a href="'.url("productview_spesial").'/'.$hasil->mc_name.'/'.$hasil->smc_name.'/'.$hasil->sb_name.'/'.$hasil->ssb_name.'/'.$res.'/'.$hasil->pro_title.'" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>';
              $data .= '</div><label class="action action--compare-add"><input class="check-hidden" type="checkbox" /><i class="fa fa-plus"></i><i class="fa fa-check"></i><span class="action__text action__text--invisible">Add to compare</span></label>';
          }
          $data .= '</div></div';
      }

      return $data;
  }
}
?>
