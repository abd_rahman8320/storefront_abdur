<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Country;
use App\Customer;
use App\Attributes;
use App\UsersRoles;
use App\RolesPrivileges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class CountryController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function view_include()
	{
		if (Session::has('userid')) {
            $user_role = UsersRoles::where('ur_user_name', Session::get('username'))->get();
			$privileges = [];
			foreach ($user_role as $ur) {
				$role_privilege = RolesPrivileges::where('rp_roles_name', $ur->ur_roles_name)->get();
				foreach ($role_privilege as $rp) {
                    $rp = $rp->toArray();
					array_push($privileges, $rp);
				}
			}

            $adminheader      = view('siteadmin.includes.admin_header')->with("routemenu", "settings")->with('privileges', $privileges);
            $adminleftmenus   = view('siteadmin.includes.admin_left_menus')->with('privileges', $privileges);
            $adminfooter      = view('siteadmin.includes.admin_footer');
            $return = [
                'adminheader' => $adminheader,
                'adminleftmenus' => $adminleftmenus,
                'adminfooter' => $adminfooter
            ];
            return $return;
        } else {
            return Redirect::to('siteadmin');
        }
	}

    public function add_country()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];
            return view('siteadmin.add_countries')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_country()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $countryresult = Country::view_country_detail();

            return view('siteadmin.manage_countries')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('countryresult', $countryresult);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function edit_country($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $countryresult = Country::showindividual_country_detail($id);

            return view('siteadmin.edit_countries')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('countryresult', $countryresult)->with('id', $id);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function delete_country($id)
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            $affected      = Country::delete_country_detail($id);
            $countryresult = Country::view_country_detail();

            /* return view('siteadmin.manage_countries')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('specificationresult',$countryresult);*/

            return Redirect::to('manage_country')->with('updated_result', 'Country Deleted Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function update_status_country($id, $status)
    {
        if (Session::has('userid')) {
            $return = Country::update_status_country($id, $status);
            return Redirect::to('manage_country')->with('updated_result', 'Record Updated Successfully');
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function add_country_submit()
    {
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $rule = array(
                'cname' => 'required',
                'ccode' => 'required',
                'cursymbol' => 'required',
                'curcode' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_country')->withErrors($validator->messages())->withInput();

            } else {
                $countryname    = Input::get('cname');
                $countrycode    = Input::get('ccode');
                $currencysymbol = Input::get('cursymbol');
                $currencycode   = Input::get('curcode');


                $check_exist_country_name = Country::check_exist_country_name($countryname);
                $check_exist_country_code = Country::check_exist_country_code($countrycode);


                if ($check_exist_country_name) {
                    return Redirect::to('add_country')->withMessage("Country Already Exists")->withInput();
                } else if ($check_exist_country_code) {
                    return Redirect::to('add_country')->withMessage("Country Code  Already Exists")->withInput();
                } else {

                    $entry = array(
                        'co_code' => Input::get('ccode'),
                        'co_name' => Input::get('cname'),
                        'co_cursymbol' => Input::get('cursymbol'),
                        'co_curcode' => Input::get('curcode')
                    );

                    $return = Country::save_country_detail($entry);
                    return Redirect::to('manage_country')->with('insert_result', 'Record Inserted');
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function update_country_submit()
    {
        if (Session::has('userid')) {
            $include = self::view_include();
            $adminheader 	= $include['adminheader'];
            $adminleftmenus = $include['adminleftmenus'];
            $adminfooter 	= $include['adminfooter'];

            //$inputs = Input::all();
            $data = Input::except(array(
                '_token'
            ));
            $id   = Input::get('id');
            $rule = array(
                'ceditname' => 'required',
                'ceditcode' => 'required',
                'cureditsymbol' => 'required',
                'cureditcode' => 'required'
            );

            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {

                return Redirect::to('edit_country/' . $id)->withErrors($validator->messages())->withInput();

            }

            else {

                if ($id != "") {
                    $entry = array(
                        'co_code' => Input::get('ceditcode'),
                        'co_name' => Input::get('ceditname'),
                        'co_cursymbol' => Input::get('cureditsymbol'),
                        'co_curcode' => Input::get('cureditcode')
                    );

                    $countryname    = Input::get('cname');
                    $countrycode    = Input::get('ccode');
                    $currencysymbol = Input::get('cursymbol');
                    $currencycode   = Input::get('curcode');
                    $check_exist_country_name = Country::check_exist_country_name_update($countryname, $id);
                    $check_exist_country_code = Country::check_exist_country_code_update($countrycode, $id);

                    if ($check_exist_country_name) {
                        return Redirect::to('edit_country/' . $id)->withMessage("Country Already Exists")->withInput();
                    } else if ($check_exist_country_code) {
                        return Redirect::to('edit_country/' . $id)->withMessage("Country Code  Already Exists")->withInput();
                    } else {
                        $affected = Country::update_country_detail($id, $entry);
                        return Redirect::to('manage_country')->with('updated_result', 'Record Updated');

                    }
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function apiCreateCountry(Request $request)
    {
        $req_country = Country::get_country($request->input('co_code'));

        if($req_country!=null)
        {
            $get_co_id = Country::get_country($request->input('co_code'));

            if($request->input('co_code')==null)
            {
                return response('Required Parameter Tidak Boleh Kosong', 400);
                die();
            }
            if($get_co_id==null)
            {
                return response('Country Code Not Found', 400);
                die();
            }

            $row = Country::find($get_co_id->co_id);
            if($request->input('co_code')!=null)
                $row->co_code=$request->input('co_code');
            if($request->input('co_name')!=null)
                $row->co_name=$request->input('co_name');
            else
                $row->co_name=$get_co_id->co_name;
            if($request->input('co_cursymbol')!=null)
                $row->co_cursymbol=$request->input('co_cursymbol');
            else
                $row->co_cursymbol=$get_co_id->co_cursymbol;
            if($request->input('co_curcode')!=null)
                $row->co_curcode=$request->input('co_curcode');
            else
                $row->co_curcode=$get_co_id->co_curcode;
            if($request->input('co_status')!=null)
                $row->co_status=$request->input('co_status');
            else
                $row->co_status=$get_co_id->co_status;
    		$row->save();

            return response('Sukses', 200);
        }
        if($request->input('co_code')== null
        || $request->input('co_name')==null)
        {
            return response('Required Parameter Tidak Boleh Kosong', 400);
            die();
        }

        $row = new Country();
		$row->co_code=$request->input('co_code');
		$row->co_name=$request->input('co_name');
        if($request->input('co_cursymbol')!=null)
        {
            $row->co_cursymbol=$request->input('co_cursymbol');
        }else {
            $row->co_cursymbol='';
        }
        if($request->input('co_curcode')!=null)
        {
            $row->co_curcode=$request->input('co_curcode');
        }else {
            $row->co_curcode='';
        }
        if($request->input('co_status')!=null)
        {
            $row->co_status=$request->input('co_status');
        }else{
            $row->co_status=0;
        }

		$row->save();

        return response('Sukses', 200);
    }

    public function apiUpdateCountry(Request $request)
    {
        $get_co_id = Country::get_country($request->input('co_code'));

        if($request->input('co_code')==null)
        {
            return response('Required Parameter Tidak Boleh Kosong', 400);
            die();
        }
        if($get_co_id==null)
        {
            return response('Country Code Not Found', 400);
            die();
        }

        $row = Country::find($get_co_id->co_id);
        if($request->input('co_code')!=null)
            $row->co_code=$request->input('co_code');
        if($request->input('co_name')!=null)
            $row->co_name=$request->input('co_name');
        else
            $row->co_name=$get_co_id->co_name;
        if($request->input('co_cursymbol')!=null)
            $row->co_cursymbol=$request->input('co_cursymbol');
        else
            $row->co_cursymbol=$get_co_id->co_cursymbol;
        if($request->input('co_curcode')!=null)
            $row->co_curcode=$request->input('co_curcode');
        else
            $row->co_curcode=$get_co_id->co_curcode;
        if($request->input('co_status')!=null)
            $row->co_status=$request->input('co_status');
        else
            $row->co_status=$get_co_id->co_status;
		$row->save();

        return response('Sukses', 200);
    }

    public function apiDeleteCountry(Request $request)
    {
        $get_co_id = Country::get_country($request->input('co_code'));

        if($get_co_id==null)
        {
            return response('Country Code Not Found', 400);
            die();
        }
        $deleteCountryAtMerchant = DB::table('nm_merchant')->where('mer_co_id', $get_co_id->co_id)->update(['mer_co_id'=>'']);
        $deleteCountryAtProvince = DB::table('nm_province')->where('prov_co_id', $get_co_id->co_id)->update(['prov_co_id'=>'']);

        $row = Country::find($get_co_id->co_id);
        $row->delete();

        return response('Sukses', 200);
    }

    public function apiCreateCurrency(Request $request)
    {
        $req_currency = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->first();
        $req_country = Country::get_country($request->input('co_code'));
        $get_co_id = Country::get_country($request->input('co_code'));

        if($request->input('cur_name')==null
        || $request->input('cur_code')==null
        || $request->input('cur_symbol')==null
        || $request->input('co_code')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($req_currency!=null)
        {
            $req_currency = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->first();

            if($request->input('cur_code')==null)
            {
                return response('Required Parameter Is Null', 400);
                die();
            }
            if($req_currency==null)
            {
                return response('Currency Code Not Found', 400);
                die();
            }
            if($request->input('co_code')!=null)
            {
                $req_country = Country::get_country($request->input('co_code'));
                $get_co_id = Country::get_country($request->input('co_code'));
                if($req_country==null)
                {
                    return response('Country Code Not Found', 400);
                    die();
                }
                $row = Country::find($get_co_id->co_id);
                if($request->input('cur_symbol')!=null)
                    $row->co_cursymbol=$request->input('cur_symbol');
                $row->co_curcode=$request->input('cur_code');
                $row->save();
            }

            if($request->input('cur_name')!=null)
                $name = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_name'=>$request->input('cur_name')]);
            if($request->input('cur_symbol')!=null)
                $symbol = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_symbol'=>$request->input('cur_symbol')]);
            if($request->input('cur_status')!=null)
                $status = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_status'=>$request->input('cur_status')]);
            if($request->input('cur_default')!=null)
                $default = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_default'=>$request->input('cur_default')]);

            return response('Sukses', 200);
        }
        if($req_country==null)
        {
            return response('Country Code Not Found', 400);
            die();
        }

        $createCurrency = DB::table('nm_currency')->insert([
            'cur_name'=>$request->input('cur_name'),
            'cur_code'=>$request->input('cur_code'),
            'cur_symbol'=>$request->input('cur_symbol'),
            'cur_status'=>1,
            'cur_default'=>0
        ]);
        if($request->input('cur_status')!=null){
            $status = DB::table('nm_currency')
            ->where('cur_code', $request->input('cur_code'))
            ->update(['cur_status'=>$request->input('cur_status')]);
        }
        if($request->input('cur_default')!=null){
            $default = DB::table('nm_currency')
            ->where('cur_code', $request->input('cur_code'))
            ->update(['cur_default'=>$request->input('cur_default')]);
        }

        $row = Country::find($get_co_id->co_id);
        $row->co_cursymbol=$request->input('cur_symbol');
        $row->co_curcode=$request->input('cur_code');
        $row->save();

        return response('Sukses', 200);
    }

    public function apiUpdateCurrency(Request $request)
    {
        $req_currency = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->first();

        if($request->input('cur_code')==null)
        {
            return response('Required Parameter Is Null', 400);
            die();
        }
        if($req_currency==null)
        {
            return response('Currency Code Not Found', 400);
            die();
        }
        if($request->input('co_code')!=null)
        {
            $req_country = Country::get_country($request->input('co_code'));
            $get_co_id = Country::get_country($request->input('co_code'));
            if($req_country==null)
            {
                return response('Country Code Not Found', 400);
                die();
            }
            $row = Country::find($get_co_id->co_id);
            if($request->input('cur_symbol')!=null)
                $row->co_cursymbol=$request->input('cur_symbol');
            $row->co_curcode=$request->input('cur_code');
            $row->save();
        }

        if($request->input('cur_name')!=null)
            $name = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_name'=>$request->input('cur_name')]);
        if($request->input('cur_symbol')!=null)
            $symbol = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_symbol'=>$request->input('cur_symbol')]);
        if($request->input('cur_status')!=null)
            $status = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_status'=>$request->input('cur_status')]);
        if($request->input('cur_default')!=null)
            $default = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->update(['cur_default'=>$request->input('cur_default')]);

        return response('Sukses', 200);
    }

    public function apiDeleteCurrency(Request $request)
    {
        $get_cur_id = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->first();
        $get_co_id = DB::table('nm_country')->where('co_curcode', $request->input('cur_code'))->first();

        if($get_cur_id==null)
        {
            return response('Currency Code Not Found', 400);
            die();
        }
        $row = Country::find($get_co_id->co_id);
        $row->co_curcode='';
        $row->co_cursymbol='';
        $row->save();

        $delete = DB::table('nm_currency')->where('cur_code', $request->input('cur_code'))->delete();

        return response('Sukses', 200);
    }

    public function getCountry()
    {
        $get = DB::table('nm_country')->where('co_status', 0)->get();

        return $get;
    }
}
