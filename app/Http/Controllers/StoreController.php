<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Merchant;

class StoreController extends BaseController
{
    // Fungsi API Create Store
	public function apicreatestore(Request $request)
	{
        if($request->input('merchant_code')==null||$request->input('stor_name')==null||$request->input('stor_phone')==null){
            return response ('Required Parameter is null', 400);
            die();
        }

        $merchant_code = $request->input('merchant_code');
        $merchant = Merchant::where('merchant_code','=', $merchant_code)->first();

        if($merchant==null){
            return response ('Data not found', 400);
            die();
        }

        $stor_merchant_id = $merchant->mer_id;
        $stor_name = $request->input('stor_name');
		$stor_phone = $request->input('stor_phone');
		
		
        $entry = [];
        $entry['stor_merchant_id'] = $stor_merchant_id;
        $entry['stor_name'] = $stor_name;
        $entry['stor_phone'] = $stor_phone;
        
		$merchant->insert_store($entry);
        return response('Sukses', 200);
	}

    
}
?>