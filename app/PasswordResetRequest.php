<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class PasswordResetRequest extends Model
{
    use SoftDeletes;
    
    protected $table = 'nm_password_reset_request';
    protected $primaryKey = 'prr_id';
    const CREATED_AT = 'created_ts';
    const UPDATED_AT = 'lastupdated_ts';
    const DELETED_AT = 'deleted_ts';

    protected $fillable = [
        'prr_user_name',
        'prr_request_uuid',
        'prr_is_new',
        'prr_expired_ts',
        'created_by',
        'lastupdated_by',
        'deleted_by',
        'is_deleted'
    ];

    protected $dates = ['deleted_ts'];

    public function user()
    {
        return $this->belongsTo('App\Users');
    }
}
?>
