<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Province extends Model
{
    protected $table = 'nm_province';
    protected $primaryKey = 'prov_id';
	public $timestamps = false;

    protected $fillable = [
        'prov_code',
        'prov_name',
        'prov_co_id',
        'prov_status'
    ];
}
?>
