<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Roles extends Model
{
    use SoftDeletes;

    protected $table = 'nm_roles';
    protected $primaryKey = 'roles_id';
    const CREATED_AT = 'created_ts';
    const UPDATED_AT = 'lastupdated_ts';
    const DELETED_AT = 'deleted_ts';

    protected $fillable = [
        'roles_name',
        'roles_description',
        'roles_label',
        'created_by',
        'lastupdated_by',
        'deleted_by',
        'is_deleted'
    ];

    protected $dates = ['deleted_ts'];

    public function user_roles()
    {
        return $this->hasMany('App\UsersRoles', 'roles_name');
    }

    public function roles_privileges()
    {
        return $this->hasMany('App\RolesPrivileges', 'roles_name');
    }
}
?>
