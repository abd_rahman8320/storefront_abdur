<?php namespace App\Commands;

use App\Commands\Command;
use DB;
use Log;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class PreProcessing extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

    private $test;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($test)
	{
        $this->test = $test;
	}

	public function handle()
	{
		try {
            Log::info('PreProcessing Start');
            $get_product = DB::table('nm_product')
            ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
            ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
            ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
            ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
            ->select('pro_id', 'pro_title', 'pro_desc', 'mc_name', 'smc_name', 'sb_name', 'ssb_name')
            ->where('pro_id', '!=', 1)
            ->where('pro_id', '!=', 2)
            ->get();

            DB::table('term_frequency')
            ->truncate();

            foreach ($get_product as $row) {
                Log::info($row->pro_id.' Start');
                $title = preg_replace('/[^\p{L}\p{N}\s]/u', '', $row->pro_title);
                $title = preg_replace('~[[:cntrl:]]~', ' ', $title);
                $title = strip_tags($title);
                $title = explode(' ', $title);

                $description = preg_replace('/[^\p{L}\p{N}\s]/u', '', $row->pro_desc);
                $description = preg_replace('~[[:cntrl:]]~', ' ', $description);
                $description = strip_tags($description);
                $description = explode(' ', $description);

                $category = $row->mc_name.' '.$row->smc_name.' '.$row->sb_name.' '.$row->ssb_name;
                $category = preg_replace('/[^\p{L}\p{N}\s]/u', '', $category);
                $category = preg_replace('~[[:cntrl:]]~', ' ', $category);
                $category = strip_tags($category);
                $category = explode(' ', $category);

                foreach ($title as $term_title) {
                    if ($term_title != '') {
                        $check = DB::table('term_frequency')
                        ->where('pro_id', $row->pro_id)
                        ->where('term', $term_title)
                        ->where('field', 'Title')
                        ->first();
						$check2 = DB::table('term_frequency')
						->where('pro_id', $row->pro_id)
                        ->where('term', $term_title)
                        ->where('field', 'Total')
                        ->first();

                        if ($check != null) {
                            DB::table('term_frequency')
                            ->where('pro_id', $row->pro_id)
                            ->where('term', $term_title)
                            ->where('field', 'Title')
                            ->update([
                                'frequency' => $check->frequency + 1
                            ]);
                        }else {
                            DB::table('term_frequency')
                            ->insert([
                                'pro_id' => $row->pro_id,
                                'term' => $term_title,
                                'field' => 'Title',
                                'frequency' => 1,
                                'weight' => 0
                            ]);
                        }

						if ($check2 != null) {
                            DB::table('term_frequency')
                            ->where('pro_id', $row->pro_id)
                            ->where('term', $term_title)
                            ->where('field', 'Total')
                            ->update([
                                'frequency' => $check2->frequency + 1
                            ]);
                        }else {
                            DB::table('term_frequency')
                            ->insert([
                                'pro_id' => $row->pro_id,
                                'term' => $term_title,
                                'field' => 'Total',
                                'frequency' => 1,
                                'weight' => 0
                            ]);
                        }

                    }

                }

                foreach ($description as $term_desc) {
                    if ($term_desc != '') {
                        $check = DB::table('term_frequency')
                        ->where('pro_id', $row->pro_id)
                        ->where('term', $term_desc)
                        ->where('field', 'Description')
                        ->first();
						$check2 = DB::table('term_frequency')
						->where('pro_id', $row->pro_id)
                        ->where('term', $term_desc)
                        ->where('field', 'Total')
                        ->first();

                        if ($check != null) {
                            DB::table('term_frequency')
                            ->where('pro_id', $row->pro_id)
                            ->where('term', $term_desc)
                            ->where('field', 'Description')
                            ->update([
                                'frequency' => $check->frequency + 1
                            ]);
                        }else {
                            DB::table('term_frequency')
                            ->insert([
                                'pro_id' => $row->pro_id,
                                'term' => $term_desc,
                                'field' => 'Description',
                                'frequency' => 1,
                                'weight' => 0
                            ]);
                        }

						if ($check2 != null) {
                            DB::table('term_frequency')
                            ->where('pro_id', $row->pro_id)
                            ->where('term', $term_desc)
                            ->where('field', 'Total')
                            ->update([
                                'frequency' => $check2->frequency + 1
                            ]);
                        }else {
                            DB::table('term_frequency')
                            ->insert([
                                'pro_id' => $row->pro_id,
                                'term' => $term_desc,
                                'field' => 'Total',
                                'frequency' => 1,
                                'weight' => 0
                            ]);
                        }

                    }

                }

                foreach ($category as $term_cat) {
                    if ($term_cat != '') {
                        $check = DB::table('term_frequency')
                        ->where('pro_id', $row->pro_id)
                        ->where('term', $term_cat)
                        ->where('field', 'Category')
                        ->first();
						$check2 = DB::table('term_frequency')
						->where('pro_id', $row->pro_id)
                        ->where('term', $term_cat)
                        ->where('field', 'Total')
                        ->first();

                        if ($check != null) {
                            DB::table('term_frequency')
                            ->where('pro_id', $row->pro_id)
                            ->where('term', $term_cat)
                            ->where('field', 'Category')
                            ->update([
                                'frequency' => $check->frequency + 1
                            ]);
                        }else {
                            DB::table('term_frequency')
                            ->insert([
                                'pro_id' => $row->pro_id,
                                'term' => $term_cat,
                                'field' => 'Category',
                                'frequency' => 1,
                                'weight' => 0
                            ]);
                        }

						if ($check2 != null) {
                            DB::table('term_frequency')
                            ->where('pro_id', $row->pro_id)
                            ->where('term', $term_cat)
                            ->where('field', 'Total')
                            ->update([
                                'frequency' => $check2->frequency + 1
                            ]);
                        }else {
                            DB::table('term_frequency')
                            ->insert([
                                'pro_id' => $row->pro_id,
                                'term' => $term_cat,
                                'field' => 'Total',
                                'frequency' => 1,
                                'weight' => 0
                            ]);
                        }

                    }

                }
            }

            Log::info('PreProcessing Done');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

	}

}
