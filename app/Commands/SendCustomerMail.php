<?php namespace App\Commands;

use App\Commands\Command;
use DB;
use Config;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendCustomerMail extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	protected $secs;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($secs)
	{
		$this->secs = $secs;

		dd($secs);
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$admin = DB::table('nm_emailsetting')->first();
		$admin_email = $admin->es_noreplyemail;
		Config::set('mail.from.address', $admin_email);
		 Mail::send('emails.registermail', $secs, function($message)
         {
           	$message->to(Input::get('email'))->subject('Register Account Created Successfully');
         });
	}

}
