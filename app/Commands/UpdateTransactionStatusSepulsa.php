<?php namespace App\Commands;

use App\Commands\Command;
use DB;
use App\Sepulsa;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class UpdateTransactionStatusSepulsa extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	private $transID;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($transID)
	{
		//var_dump($expired_date);
		$this->transID = $transID;
		//var_dump($this->id);
	}

	public function handle()
	{
		$status = '';
        $type = '';
        $customer_number = '';
        $order_id = '';
        $transaction_id = $this->transID;
        $meter_number = '';

        $get_transaction = Sepulsa::get_transaction($status, $type, $customer_number, $order_id, $transaction_id, $meter_number);

        foreach ($get_transaction as $transaction) {
            if ($transaction['response_code'] == '00') {
                date_default_timezone_set('Asia/Jakarta');
                $now = date('Y-m-d H:i:s');
                $update_tgl_dikirim = DB::table('nm_order')->where('transaction_id', $transaction['order_id'])->update([
                    'order_tgl_pesanan_dikirim' => $now,
                    'status_outbound' => 4
                ]);
                $update_outbound = DB::table('nm_transaksi')->where('transaction_id', $transaction['order_id'])->update([
                    'status_outbound_transaksi' => 4
                ]);
            }elseif ($transaction['response_code'] == '10') {
                $date = \Carbon\Carbon::now()->addSeconds(15);
                \Queue::later($date, new \App\Commands\UpdateTransactionStatusSepulsa($transaction['transaction_id']));
            }
            $update_response_code = DB::table('nm_order')->where('transaction_id', $transaction['order_id'])->update([
                'order_sepulsa_response_code' => $transaction['response_code'],
            ]);
        }
	}
}
