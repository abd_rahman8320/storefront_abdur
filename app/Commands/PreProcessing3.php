<?php namespace App\Commands;

use App\Commands\Command;
use DB;
use Log;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class PreProcessing3 extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

    private $test;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($test)
	{
        $this->test = $test;
	}

	public function handle()
	{
		try {
            Log::info('PreProcessing3 Start');

            $terms = DB::table('term_frequency')
            ->where('field', 'Total')
            ->get();

			$total_document_length = DB::table('term_frequency')
	        ->where('field', 'Total')
	        ->sum('frequency');
	        $total_product = DB::table('term_frequency')
	        ->selectRaw('count(distinct pro_id) as total')
	        ->first();
			$average_document_length = $total_document_length / intval($total_product->total);

			$k = 1.2;
			$b = 0.75;

            foreach ($terms as $term) {
                $total = DB::table('term_frequency')
                ->where('pro_id', $term->pro_id)
                ->where('term', $term->term)
				->where('field', '!=', 'Total')
                ->sum('weight');
				$idf = DB::table('inverse_document_frequency')
				->where('term', $term->term)
				->sum('value');

				$document_length = DB::table('term_frequency')
				->where('pro_id', $term->pro_id)
				->where('field', 'Total')
				->sum('frequency');

				$total_bm25 = ((($k + 1) * $term->frequency) / ($term->frequency + $k * (1 - $b + ($b * $document_length / $average_document_length)))) * $idf;
				$total_bm25f = ($total / ($k + $total)) * $idf;

                DB::table('term_frequency')
                ->where('pro_id', $term->pro_id)
                ->where('term', $term->term)
                ->where('field', 'Total')
                ->update([
                    'weight' => $total_bm25f,
					'weight_bm25' => $total_bm25
                ]);
            }

            Log::info('PreProcessing3 Done');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

	}

}
