<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Home;

class AutoRecieving extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	private $transID;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($transID)
	{
		$this->transID = $transID;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$get_detail_transaksi = Home::getorderdetails_trs($this->transID);

		if($get_detail_transaksi[0]->order_tgl_konfirmasi_penerimaan_barang == "0000-00-00 00:00:00" || $get_detail_transaksi[0]->order_tgl_konfirmasi_penerimaan_barang == null || $get_detail_transaksi[0]->order_tgl_konfirmasi_penerimaan_barang == "")
		{
			$tgl = Date('Y-m-d H:i:s');
			\App\Home::update_status_auto_recieving($this->transID, $tgl);
		}
		else
		{

		}
	}

}
