<?php namespace App\Commands;

use App\Commands\Command;
Use DB;
use Config;
Use App\Home;
Use App\Settings;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SNewsletter extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	public $id;
	public $status;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($id,$status)
	{
			$this->id = $id;
			$this->status = $status;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
			date_default_timezone_set('Asia/Bangkok');
			$today = Date('Y-m-d H:i:s');
			//$getlogodetails = Home::getlogodetails();
			$getdatanews    = \App\Settings::get_data_newsletter_by_id($this->id);
			$toall          = Settings::get_data_subscribers();
			foreach ($toall as $row) {
				 $konten = array(
					 'isi' => $getdatanews->isi,
					 'email' => $row->email
				);

				$admin = DB::table('nm_emailsetting')->first();
				$admin_email = $admin->es_noreplyemail;
				Config::set('mail.from.address', $admin_email);
			 Mail::send(
			 	'emails.newsletter',
					$konten, function ($message) use ($konten,$toall,$getdatanews){
					 $message->to($konten['email'])->subject($getdatanews->subject);
					}
				 );
			}
			\App\Settings::send_newsletter($this->id,$this->status);
			// if($today >= $getdatanews->tanggal_kirim){
			// 	//dd($today > $getdatanews->tanggal_kirim);
			// 	$toall          = Settings::get_data_subscribers();
			// 	foreach ($toall as $row) {
			// 		 $konten = array(
			// 			 'isi' => $getdatanews->isi,
			// 			 'email' => $row->email
			// 		 );

			// 			 Mail::send(
			// 					 'emails.newsletter',
			// 					 $konten, function ($message) use ($konten,$toall,$getdatanews){
			// 							 $message->to($konten['email'])->subject($getdatanews->subject);
			// 					 }
			// 			 );
			// 		 }
			// 		\App\Settings::send_newsletter($this->id,$this->status);
			// }
			// else{
			// 	$date = \Carbon\Carbon::now()->addMinutes(1);
			// 	\Queue::later($date, new \App\Commands\SNewsletter($this->id,$this->status));
			// }
    }
}
