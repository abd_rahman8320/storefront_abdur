<?php namespace App\Commands;

use App\Commands\Command;
use DB;
use Log;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class PreProcessing2 extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

    private $test;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($test)
	{
        $this->test = $test;
	}

	public function handle()
	{
		try {
            Log::info('PreProcessing2 Start');
            //IDF
            Log::info('IDF Start');
            $terms = DB::table('term_frequency')->groupBy('term')->get();
            DB::table('inverse_document_frequency')->truncate();

            foreach ($terms as $term) {
                $products = DB::table('term_frequency')->groupBy('pro_id')->get();
                $products = collect($products)->count();

                $products_with_term = DB::table('term_frequency')
                ->where('term', $term->term)
                ->groupBy('pro_id')
                ->get();
                $products_with_term = collect($products_with_term)->count();

                $idf = log10(($products + 1) / $products_with_term);

                DB::table('inverse_document_frequency')->insert([
                    'term' => $term->term,
                    'df' => $products_with_term,
                    'value' => $idf
                ]);
            }

			//BM25F
            Log::info('BM25F Start');
			$terms = DB::table('term_frequency')
			->where('field', '!=', 'Total')
			->get();

			foreach ($terms as $term) {
				$len_fD = DB::table('term_frequency')
				->where('pro_id', $term->pro_id)
				->where('field', $term->field)
				->sum('frequency');
				$count_product = DB::table('nm_product')
				->where('pro_id', '!=', 1)
				->where('pro_id', '!=', 2)
				->count();
				$len_f = DB::table('term_frequency')
				->where('field', $term->field)
				->sum('frequency');
				$avgLen_f = $len_f / $count_product;
				$tf = $term->frequency;
				$b = 0.75;

				if ($term->field == 'Title') {
					$boostf = 3;
				}elseif ($term->field == 'Category') {
					$boostf = 2;
				}elseif ($term->field == 'Description') {
					$boostf = 1;
				}else {
					$boostf = 0;
				}

				$w_tD1 = $tf * $boostf;
				$w_tD2 = 1 - $b;
				$w_tD3 = $b * $len_fD / $avgLen_f;
				$w_tD4 = $w_tD2  + $w_tD3;
				$w_tD = $w_tD1 / $w_tD4;

				DB::table('term_frequency')
				->where('pro_id', $term->pro_id)
				->where('term', $term->term)
				->where('field', $term->field)
				->update([
					'weight' => $w_tD
				]);
			}

            Log::info('PreProcessing2 Done');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

	}

}
