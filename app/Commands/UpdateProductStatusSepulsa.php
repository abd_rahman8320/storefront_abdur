<?php namespace App\Commands;

use App\Commands\Command;
use DB;
use App\Sepulsa;
use Log;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class UpdateProductStatusSepulsa extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	public function handle()
	{
		$type = '';
		$operator = '';
		$nominal = '';
        $get_product_list = Sepulsa::get_product($type, $operator, $nominal);
        foreach ($get_product_list['list'] as $list) {
            $check_product = DB::table('nm_product')->where('pro_sepulsa_id', $list['product_id'])->first();
            if ($check_product) {
                $update = DB::table('nm_product')
                ->where('pro_sepulsa_id', $list['product_id'])
                ->update(['pro_status' => $list['enabled']]);
            }
        }
		$date = \Carbon\Carbon::now()->addMinutes(360);
        \Queue::later($date, new \App\Commands\UpdateProductStatusSepulsa());
		Log::info('Product Status Updated');
	}

}
