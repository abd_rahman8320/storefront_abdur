<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Home;
class SalesOrder extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data_unsync = Home::unsyncedOrders();
        //dd($data_unsync);

        foreach($data_unsync as $transID => $ndata)
        {
            $totalCharge = 0;
            $items = [];
            $dest_data = [];
            foreach($ndata as $item){
                $items[] = [
                    'sku' => $item->pro_sku,
                    'unit_price' => $item->order_amt,
                    'tax_rate' => $item->order_tax,
                    'qty' => $item->order_qty
                ];
                $totalCharge += $item->order_qty*$item->order_amt - $item->order_tax;
            }
            if($ndata[0]->postalservice_code == 'PopBox') {
                $dest_data[] =[
                    'location_id' => $ndata[0]->shipping_ref_number,
                    'address' => $ndata[0]->ship_address1,
                    'address2' => $ndata[0]->ship_address2,
                    'country' => $ndata[0]->co_name,
                    'city' => $ndata[0]->ci_name,
                    'state' => $ndata[0]->ship_state
                ];

                $data_kirim = array(
                    'invoice_number'=> $transID,
                    'buyer_name'=> $ndata[0]->cus_name,
                    'buyer_email'=>$ndata[0]->cus_email,
                    'postalservice_code'=>$ndata[0]->postalservice_code,

                    'order_date'=>$ndata[0]->order_date,
                    'currency_code'=>'IDR',//todo ambil data
                    //--

                    'allow_duplicate_invoice' => false,

                    'address'=>$ndata[0]->ship_address1,
                    'city_code'=>'1103',//todo ambil data
                    'zip_code'=>$ndata[0]->ship_postalcode,
                    'total_charge'=> $totalCharge,

                    'dest_type_code'=> 'PopBox',
                    'dest_data' => $dest_data,

                    'dest_code' => $ndata[0]->ci_code,

                    'items'=> $items
                );
            }
            else
            {
                $dest_data[] =[
                    'city' => $ndata[0]->ci_name,
                    'address' => $ndata[0]->ship_address1,
                    'address2' => $ndata[0]->ship_address2,
                    'country' => $ndata[0]->co_name,
                    'state' => $ndata[0]->ship_state
                ];

                $data_kirim = array(
                    'invoice_number'=> $transID,
                    'buyer_name'=> $ndata[0]->cus_name,
                    'buyer_email'=>$ndata[0]->cus_email,
                    'postalservice_code'=>$ndata[0]->postalservice_code,

                    'order_date'=>$ndata[0]->order_date,
                    'currency_code'=>'IDR',//todo ambil data
                    //--

                    'allow_duplicate_invoice' => false,

                    'address'=>$ndata[0]->ship_address1,
                    'city_code'=>'1103',//todo ambil data
                    'zip_code'=>$ndata[0]->ship_postalcode,
                    'total_charge'=> $totalCharge,

                    'dest_type_code'=> 'CITY',
                    'dest_data' => $dest_data,

                    'dest_code' => $ndata[0]->ci_code,

                    'items'=> $items
                );
            }


            if($ndata[0]->mer_fname == 'Kukuruyuk') {
                echo "masuk ke Kukuruyuk";
                $result = \App\WMSClient::createSO($data_kirim);

                if($result['created_id'] != null) {
                    DB::table('nm_order')
                    ->where('transaction_id', $transID)
                    ->update(['id' => $result['created_id']]);
                }else{

                }
            }
            else
            {
                echo "Ga masuk kukuruyuk";
            }
        }
	}

}
