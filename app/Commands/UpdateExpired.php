<?php namespace App\Commands;

use App\Commands\Command;
use DB;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class UpdateExpired extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	private $transID;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($transID)
	{
		//var_dump($expired_date);
		$this->transID = $transID;
		//var_dump($this->id);
	}

	public function handle()
	{
		try
		{
			DB::BeginTransaction();

			// query 1
			\App\Home::update_status_expired($this->transID);

			// query 2
			$kocak = \App\Home::get_transaction_detail($this->transID);

			//query 3
			$kembalikan_poin = \App\Home::kembalikan_poin($this->transID);
			
			if($kocak){
		        foreach ($kocak as $ngibul) {

		            $get_back = floatval($ngibul->pro_no_of_purchase) - floatval($ngibul->order_qty);

		            var_dump($get_back ."|". $ngibul->pro_id);

		            // cek jasa pengiriman dan diskon
		            if($ngibul->pro_qty > 0)
		            {
		            	\App\Home::get_back_the_stock_dude($ngibul->pro_id, $get_back);
		            }
		        }
			}

			DB::commit();
		}
		catch(\Exception $e)
		{
			DB::rollback();
		}
	}

}
