<?php

namespace App;
use Cache;
use Carbon\Carbon;

//versi 0.3.13
class WMSClient
{
	const CONFIG_FILENAME = 'wms_client';

	/**
     * Fungsi untuk menambahkan data Sale Order
     * @param type $data
     * @return type
     */
	static function createSO($data)
    {
		
        $param = [
			'invoice_number'=>$data['invoice_number'],
			'buyer_name'=>$data['buyer_name'],
			'buyer_email'=>$data['buyer_email'],
			'postal_service_code'=>$data['postalservice_code'],

			'order_date'=> self::getParam($data, 'order_date'),
			'currency_code'=>self::getParam($data, 'currency_code'),
			'_warehouse_id'=>self::getParam($data,'warehouse_id'),

			'allow_duplicate_invoice'=>self::getParam($data,'allow_duplicate_invoice', true),

			'shipping_address'=>self::getParam($data,'address'),
			'shipping_city_code'=>self::getParam($data,'city_code'),
			'shipping_zip_code'=>self::getParam($data,'zip_code'),

			'destination_type'=>self::getParam($data, 'destination_type', 'CITY'),
			//data detail alamat
			'dest_data' => self::getParam($data, 'dest_data'),
			'dest_code' => self::getParam($data, 'dest_code'),

			//potongan shipping
			'shipping_cut' => self::getParam($data, 'shipping_cut'),

			'items'=>[],
			'discount' => self::getParam($data, 'discount'),
		];

        $items = self::getParam($data, 'items', []);
		for($i=0; $i<count($items); $i++)
		{
            $item = $items[$i];

            //translate param items
			$param['items'][] = [
                'sku' => $item['sku'],
                'qty' => $item['qty'],
                'unit_price' => $item['unit_price'],
                'tax_rate' => self::getParam($item, 'tax_rate', 0),
				'discount' => self::getParam($item, 'discount', 0),
            ];
		}

		$res = self::makeRequest('api/sale_orders', 'POST', [
			'form_params' => $param
		]);

        $result = [
			"created_id" => null
		];

        $resBody = $res->getBody();
        $kocak = $res->getBody()->getContents();
		$resBodyJson = json_decode($resBody, false);
        //parse response
        if($res->getStatusCode() == 201){
            //success
            $result['created_id'] = $resBodyJson->id;
        }else if($resBodyJson){
        	return $kocak;
            //error
            // $result['error'] = $resBodyJson->error;
            // $result['error'] = $resBody;
            //return $res->getBody()->getContents();
        }else{
        	return $kocak;
			//$result["error"] = $resBody;
		}

		return $result;
    }

	/**
     * Fungsi Untuk Mengecek Stok Items
     * @param type $skus
     * @param type $location_id
     * @return type
     */
    static function getStock($skus, $location_id)
	{
		$res = self::makeRequest('api/stocks_level', 'GET', [
            'query' => [
				'sku'=>$skus,
				'_warehouse_id'=>$location_id,
			],
		]);

		return json_decode($res->getBody(), false);
	}

	/**
	 * Mendapatkan informasi biaya shipping, estimasi lama delivery (jika ada)
	 */
	static function getPostalInfo($code, $items, $dest)
	{
		$param = [
			'code'=>$code,
			'to_city_code'=>self::getParam($dest, 'to_city_code'),
			'to_zip_code'=>self::getParam($dest, 'to_zip_code'),
			'from_city_code'=>self::getParam($dest, 'from_city_code'),
			'from_zip_code'=>self::getParam($dest, 'from_zip_code'),
		];

		//parse cart
		$param['items'] = $items;

		$res = self::makeRequest('api/postal_service/calculate', 'GET', [
            'query' => $param,
		]);

		$result = [];
		$resBody = json_decode($res->getBody(), false);
		if($res->getStatusCode() == 200){
            //success
            $result['total_cost'] = $resBody->total_cost;
			$result['items'] = $resBody->items;
			$result['est_duration'] = $resBody->est_duration;
        }else{
            //error
            $result['error'] = $resBody->error;
            $result['total_cost'] = null;
        }
		return $result;
	}

    //------------------- UTILS ------------------
	static function makeRequest($url, $method, $data)
	{
        $client = new \GuzzleHttp\Client(['proxy'=>config(self::CONFIG_FILENAME.'.proxy')]);

        // Membersihkan value access_token yang tersimpan ke dalam cache
		//Cache::forget('wms_access_token');
        $accessToken = Cache::get('wms_access_token', function () use($client){
			return self::getAccessToken($client);
		});

		//jalankan request
		$response = self::sendRequest($url, $method, $data, $accessToken, $client);

		//jika unaouthorize
		$responseStatus = $response->getStatusCode();

		//dd($responseStatus);
		if($responseStatus == 401) //mungkin token expired
		{

			//coba minta lagi
			$accessToken = self::getAccessToken($client);

			//jalankan request dengan token yang baru
			$response = self::sendRequest($url, $method, $data, $accessToken, $client);
		}

		//kembalikan jawaban
		return $response;
	}

	static function sendRequest($url, $method, $data, $token, $client)
	{
		//pakai token untuk autentikasikasi
		$data['headers'] = [
			'Authorization' => 'Bearer '.$token,
		];

        try {
            //jalankan request
            return $client->request( $method, config(self::CONFIG_FILENAME.'.url').$url, $data);
        } catch (\GuzzleHttp\Exception\RequestException $exc) {
        	//dd($exc);
            return $exc->getResponse();


        }
	}

	static function getAccessToken($client)
    {
		$res = $client->request(
			'POST',
			config(self::CONFIG_FILENAME.'.url').'oauth/access_token', //config ke wms kukuruyuk
			[
				'form_params' =>
				[
					'grant_type'    => 'client_credentials',
					'client_id'     => config(self::CONFIG_FILENAME.'.client_id'),
					'client_secret'	=> config(self::CONFIG_FILENAME.'.client_secret'),
				]
			]
		);

		// mengambil akses token dari variabel $res
		$json_response = json_decode($res->getBody(), false);

		//simpan nilai accesstoken yang telah di dapat ke dalam cache
		Cache::put('wms_access_token', $json_response->access_token, Carbon::now()->addSeconds($json_response->expires_in));
		return $json_response->access_token;
    }

    static function getParam($array, $key, $default = null, $emptyUseDefault = true){
        if(array_key_exists($key, $array)){
            $key = $array[$key];
            if(!$emptyUseDefault
                || $key !== ''//cek if empty string allowed
            ){
                $default = $key;
            }
        }
        return ($default instanceof \Closure)?$default($array):$default;
    }
}
