<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Settings extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_generalsetting';

    public static function ubah_payment_header($id_header, $id_detail)
    {
        return DB::table('nm_payment_method_header')
        ->where('id_pay_header', '=', $id_header)
        ->update(array(
            'id_pay_detail' => $id_detail
            ));
    }

    public static function hapus_payment_header($id)
    {
        return DB::table('nm_payment_method_header')
        ->where('id_pay_header','=', $id)
        ->delete();
    }

    public static function insert_header_pay($data)
    {
        return DB::table("nm_payment_method_header")
        ->insert($data);
    }

    public static function list_payment_details()
    {
        return DB::table("nm_paymnet_method_detail")
        ->get();
    }

    public static function list_payment_header()
    {
        return DB::table('nm_payment_method_header')
        ->leftjoin('nm_paymnet_method_detail','nm_paymnet_method_detail.id_pay_detail','=','nm_payment_method_header.id_pay_detail')
        ->orderBy('id_pay_header','DESC')
        ->get();
    }

    public static function update_channel($id_header, $id_channel)
    {
        return DB::table('nm_payment_method_header')
        ->where('id_pay_header', '=', $id_header)
        ->update(array(
            'channel_pay_header' => $id_channel
            ));
    }

    public static function delete_bank_akun_db($id)
    {
        return DB::table('nm_bank_kuku')
        ->where('id', '=', $id)
        ->delete();
    }

    public static function update_akun_bank($data)
    {
        return DB::table('nm_bank_kuku')
        ->where('id', '=', $data["id"])
        ->update(array(
            'nama_bank' => $data["nama_bank"],
            'nomor_rekening' => $data["nomor_rekening"],
            'nama_pemilik_rekening' => $data["nama_pemilik_rekening"]
            ));
    }

    public static function get_detail_bank_akun($id)
    {
        return DB::table('nm_bank_kuku')
        ->where('id', '=', $id)
        ->get();
    }

    public static function input_akun_bank($data)
    {
        return DB::table('nm_bank_kuku')->insert($data);
    }

    public static function get_list_bank_akun()
    {
        return DB::table('nm_bank_kuku')->get();
    }

    public static function view_general_setting()
    {
        return DB::table('nm_generalsetting')->get();
    }

    public static function view_language_list()
    {
        return DB::table('nm_language')->get();
    }

    public static function view_theme_list()
    {
        return DB::table('nm_theme')->get();
    }

    public static function insert_logo($entry)
    {
        return DB::table('nm_imagesetting')->insert($entry);

    }

    public static function insert_noimage($entry)
    {
        return DB::table('nm_imagesetting')->insert($entry);

    }

    public static function insert_favicon($entry)
    {
        return DB::table('nm_imagesetting')->insert($entry);

    }

    public static function get_logo_details()
    {
        return DB::table('nm_imagesetting')->where('imgs_type', '=', 1)->get();
    }

    public static function get_favicon_details()
    {
        return DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get();
    }

    public static function get_noimage_details()
    {
        return DB::table('nm_imagesetting')->where('imgs_type', '=', 3)->get();
    }

    public static function update_logo($filename)
    {
        return DB::table('nm_imagesetting')->where('imgs_type', '=', 1)->update(array(
            'imgs_name' => $filename
        ));
    }

    public static function view_email_settings()
    {
        return DB::table('nm_emailsetting')->get();
    }

    public static function view_smtp_settings()
    {
        return DB::table('nm_smtp')->where('sm_id', '=', '1')->get();
    }

    public static function update_favicon($filename)
    {
        return DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->update(array(
            'imgs_name' => $filename
        ));
    }

    public static function update_noimage($filename)
    {
        return DB::table('nm_imagesetting')->where('imgs_type', '=', 3)->update(array(
            'imgs_name' => $filename
        ));
    }

    public static function view_send_settings()
    {
        return DB::table('nm_smtp')->where('sm_id', '=', '2')->get();
    }

    public static function save_general_set($entry)
    {
        return DB::table('nm_generalsetting')->where('gs_id', '=', '1')->update($entry);
    }

    public static function save_email_set($entry)
    {
        return DB::table('nm_emailsetting')->where('es_id', '=', '1')->update($entry);
    }

    public static function save_smtp_set($entry)
    {
        return DB::table('nm_smtp')->where('sm_id', '=', '1')->update($entry);
    }

    public static function save_send_set($entry)
    {
        return DB::table('nm_smtp')->where('sm_id', '=', '2')->update($entry);
    }

    public static function save_smtp_set_def()
    {
        return DB::table('nm_smtp')->update(array(
            'sm_isactive' => 0
        ));
    }

    public static function social_media_settings()
    {
        return DB::table('nm_social_media')->where('sm_id', '=', '1')->get();
    }

    public static function update_social_media_settings($entry)
    {
        return DB::table('nm_social_media')->where('sm_id', '=', '1')->update($entry);
    }

    public static function get_country_details()
    {
        return DB::table('nm_country')->where('co_status', '=', '0')->get();
    }

    public static function get_country_value_ajax($id)
    {
        return DB::table('nm_country')->where('co_id', '=', $id)->get();
    }

    public static function get_pay_settings()
    {
        return DB::table('nm_paymentsettings')->where('ps_id', '=', '1')->get();
    }

    //new

    public static function get_pay_settings_new()
    {
        return DB::table('nm_bank_kuku')
        ->get();
    }

    public static function update_payment_settings($entry)
    {
        return DB::table('nm_paymentsettings')->where('ps_id', '=', '1')->update($entry);
    }

    public static function get_module_details()
    {
        return DB::table('nm_modulesettings')->where('ms_id', '=', '1')->get();
    }

    public static function update_modul_settings($entry)
    {
        return DB::table('nm_modulesettings')->where('ms_id', '=', '1')->update($entry);
    }

    public static function get_newsletter_subscribers()
    {
        return DB::table('nm_newsletter_subscribers')->leftjoin('nm_city', 'nm_newsletter_subscribers.city_id', '=', 'nm_city.ci_id')->get();
    }

    public static function edit_newsletter_subs_status($id, $status)
    {
        return DB::table('nm_newsletter_subscribers')->where('id', '=', $id)->update(array(
            'status' => $status
        ));
    }

    public static function delete_newsletter_subs($id)
    {
        return DB::table('nm_newsletter_subscribers')->where('id', '=', $id)->delete();
    }

    public static function get_city_details()
    {
        return DB::table('nm_city')->where('ci_status', '=', 1)->get();
    }

    public static function get_data_subscribers()
    {
      return DB::table('nm_newsletter_subscribers')->where('status','=',1)->get();
    }

    public static function submit_data_newsblast($entry)
    {
      return DB::table('nm_emailblast')->insert($entry);
    }

    public static function get_data_newsletter(){
      return DB::table('nm_emailblast')->orderBy('id_emb', 'desc')->get();
    }
    public static function get_data_newsletter_by_id($id){
      return DB::table('nm_emailblast')
      ->where('id_emb','=',$id)
      ->first();
    }
    public static function status_enable_submit($id, $status)
    {
        return DB::table('nm_emailblast')->where('id_emb', '=', $id)->update(array(
            'status' => $status
        ));
    }
    public static function emailblast_detail($id)
    {
        return DB::table('nm_emailblast')->where('id_emb', '=', $id)->get();
    }
    public static function update_newsletter_detail($entry, $id)
    {
        return DB::table('nm_emailblast')->where('id_emb', '=', $id)->update($entry);
    }
    public static function send_newsletter($id,$status)
    {
        return DB::table('nm_emailblast')->where('id_emb','=', $id)->update(array(
            'status_email' => $status
        ));
    }
    public static function delete_newsletter($id){
      return DB::table('nm_emailblast')->where('id_emb', '=', $id)->delete();
    }

    public static function unsubscribe($email){
      return DB::table('nm_newsletter_subscribers')->where('email','=',$email)
      ->update(array(
          'status' => 0
      ));
    }

    public static function subscribe($email,$status)
    {
      return DB::table('nm_newsletter_subscribers')
      ->where('email','=',$email)
      ->update(array('status' => $status));
    }

    public static function get_setting_extended_db()
    {
        return DB::table('nm_setting_warranty')->get();
    }

    public static function save_exrended_warranty($entry)
    {
        return DB::table('nm_setting_warranty')->where('id', '=', '1')->update($entry);
    }
}
?>
