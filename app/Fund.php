<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Fund extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_deals';

    public static function product_detail($id)
    {
        return DB::table('nm_product')->where('pro_mr_id', $id)->where('pro_no_of_purchase', '>', 0)->get();
    }

    public static function fund_details($merchant_id)
    {
        return DB::table('nm_order')
        ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->where('is_funded', 0)
        ->where('pro_mr_id', $merchant_id)
        ->where('status_outbound', 5)
        ->where('is_fund_request', 0)
        ->get();
    }

    public static function deal_no_count($id)
    {
        return DB::table('nm_deals')->where('deal_merchant_id', '=', $id)->sum('deal_no_of_purchase');
    }

    public static function deal_discount_count($id)
    {
        return DB::table('nm_deals')->where('deal_merchant_id', '=', $id)->sum('deal_discount_price');
    }

    public static function product_no_count($id)
    {
        return DB::table('nm_product')->where('pro_mr_id', '=', $id)->sum('pro_no_of_purchase');
    }

    public static function product_discount_count($id)
    {
        return DB::table('nm_product')->where('pro_mr_id', '=', $id)->sum('pro_disprice');
    }

    public static function commison_amt($id)
    {
        return DB::table('nm_merchant')->where('mer_id', '=', $id)->get();
    }

    public static function check_withdraw($id)
    {
        return DB::table('nm_withdraw_request')->where('wd_mer_id', '=', $id)->get();
    }

    public static function save_withdraw($entry)
    {
        return DB::table('nm_withdraw_request')->insert($entry);
    }

    public static function save_withdraw_detail($id)
    {
        $select_detail = DB::table('nm_order')
        ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->where('is_funded', 0)
        ->where('pro_mr_id', $id)
        ->where('status_outbound', 5)
        ->where('is_fund_request', 0)
        ->get();
        $select_merchant_commision = DB::table('nm_merchant')->select('mer_commission')->where('mer_id', $id)->first();
        $select_withdraw = DB::table('nm_withdraw_request')->orderBy('wd_id', 'DESC')->first();
        $wd_id = $select_withdraw->wd_id;
        foreach ($select_detail as $detail) {
            $amount = $detail->order_amt - ($detail->order_amt * $select_merchant_commision->mer_commission/100);
            $entry = array(
                'wdd_wd_id' => $wd_id,
                'wdd_transaction_id' => $detail->transaction_id,
                'wdd_pro_id' => $detail->order_pro_id,
                'wdd_order_qty' => $detail->order_qty,
                'wdd_order_amt' => $amount,
                'wdd_order_shipping_price' => $detail->order_shipping_price
            );
            $insert_detail = DB::table('nm_withdraw_request_detail')->insert($entry);
        }
        $update_status = DB::table('nm_order')
        ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->where('is_funded', 0)
        ->where('pro_mr_id', $id)
        ->where('status_outbound', 5)
        ->where('is_fund_request', 0)
        ->update([
            'is_fund_request' => 1
        ]);

        return $update_status;
    }

    public static function update_withdraw($entry, $id)
    {
        return DB::table('nm_withdraw_request')->where('wd_mer_id', $id)->update($entry);
    }

    public static function paid_amt($merid)
    {
        return DB::select(DB::raw("SELECT sum(wr_paid_amount) as paid_amt FROM `nm_withdraw_request_paypal` WHERE wr_mer_id=$merid"));
    }

    public static function get_fund_transaction_details($merid)
    {
        return DB::table('nm_withdraw_request')
        ->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_withdraw_request.wd_mer_id')
        ->where('wd_mer_id', $merid)->get();
    }

    public static function get_merchant_details($merid)
    {
        return DB::table('nm_merchant')->where('mer_id', $merid)->first();
    }
}

?>
