<?php
namespace App;

class Sepulsa
{
    static function url()
    {
        return config('sepulsa.URL');
    }
    static function username()
    {
        return config('sepulsa.USERNAME');
    }
    static function password()
    {
        return config('sepulsa.PASSWORD');
    }

    static function get_product($type, $operator, $nominal)
    {
        $data_string = '';
        $url = self::url()."product.json?";
        if ($type != '') {
            $url .= 'type='.$type.'&';
        }
        if ($operator != '') {
            $url .= 'operator='.$operator.'&';
        }
        if ($nominal != '') {
            $nominal .= 'nominal='.$nominal.'&';
        }
        $method = 'GET';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function get_balance()
    {
        $data_string = '';
        $url = self::url()."getBalance";
        $method = 'GET';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function get_transaction($status, $type, $customer_number, $order_id, $transaction_id, $meter_number)
    {
        $data_string = '';

        $method = 'GET';
        $url = self::url().'transaction.json?';
        if ($status != '') {
            $url .= 'status='.$status.'&';
        }
        if ($type != '') {
            $url .= 'type='.$type.'&';
        }
        if ($customer_number != '') {
            $url .= 'customer_number='.$customer_number.'&';
        }
        if ($order_id != '') {
            $url .= 'order_id='.$order_id.'&';
        }
        if ($transaction_id != '') {
            $url .= 'transaction_id='.$transaction_id.'&';
        }
        if ($meter_number != '') {
            $url .= 'meter_number='.$meter_number.'&';
        }

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function get_transaction_by_transaction_id($transaction_id)
    {
        $data_string = '';

        $method = 'GET';
        $url = self::url().'transaction/'.$transaction_id.'.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_mobile_transaction($customer_number, $product_id, $order_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/mobile.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_bpjs_kesehatan($customer_number, $payment_period, $product_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'payment_period' => $payment_period,
            'product_id' => $product_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/bpjs_kesehatan.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_bpjs_kesehatan_transaction($customer_number, $payment_period, $product_id, $order_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'payment_period' => $payment_period,
            'product_id' => $product_id,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/bpjs_kesehatan.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_electricity_prepaid($customer_number, $product_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/electricity.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_electricity_prepaid_transaction($customer_number, $product_id, $meter_number, $order_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
            'meter_number' => $meter_number,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/electricity.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_electricity_postpaid($customer_number, $product_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/electricity_postpaid.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_electricity_postpaid_transaction($customer_number, $product_id, $order_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/electricity_postpaid.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_telkom($customer_number, $product_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/telkom_postpaid.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_telkom_transaction($customer_number, $product_id, $order_id)
    {
        $data = [
            'customer_number' => $customer_number,
            'product_id' => $product_id,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/telkom_postpaid.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function get_pdam_operators($product_id)
    {
        $data = [
            'product_id' => $product_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'operator/pdam.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_pdam($customer_number, $product_id, $operator_code)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number,
            'operator_code' => $operator_code
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/pdam.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_pdam_transaction($customer_number, $product_id, $operator_code, $order_id)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number,
            'operator_code' => $operator_code,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/pdam.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_multifinance($customer_number, $product_id)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number,
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/multi.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_multifinance_transaction($customer_number, $product_id, $order_id)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/multi.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_game_voucher_transaction($customer_number, $product_id, $order_id)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number,
            'order_id' => $order_id
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/game.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function inquire_mobile_postpaid($customer_number, $product_id)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'inquire/mobile_postpaid.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function create_mobile_postpaid_transaction($customer_number, $product_id)
    {
        $data = [
            'product_id' => $product_id,
            'customer_number' => $customer_number
        ];
        $data_string = json_encode($data);

        $method = 'POST';
        $url = self::url().'transaction/mobile_postpaid.json';

        $res = self::make_request($method, $data_string, $url);

        return $res;
    }

    static function make_request($method, $data, $url)
    {
        $header = [
            "Accept: application/json",
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, self::username().':'.self::password());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 20);

        $result = curl_exec($ch);
        if($result === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }

        $errors = curl_error($ch);
        $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $response   = json_decode($result, TRUE);

        return $response;
    }
}
