<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class RolesPrivileges extends Model
{
    use SoftDeletes;
    
    protected $table = 'nm_roles_privileges';
    protected $primaryKey = 'rp_id';
    const CREATED_AT = 'created_ts';
    const UPDATED_AT = 'lastupdated_ts';
    const DELETED_AT = 'deleted_ts';

    protected $fillable = [
        'rp_roles_name',
        'rp_priv_name',
        'created_by',
        'lastupdated_by',
        'deleted_by',
        'is_deleted'
    ];

    protected $dates = ['deleted_ts'];

    public function roles()
    {
        return $this->belongsTo('App\Roles');
    }

    public function privileges()
    {
        return $this->belongsTo('App\Privileges');
    }
}
?>
