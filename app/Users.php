<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Users extends Model
{
    use SoftDeletes;
    
    protected $table = 'nm_user';
    protected $primaryKey = 'user_id';
    const CREATED_AT = 'created_ts';
    const UPDATED_AT = 'lastupdated_ts';
    const DELETED_AT = 'deleted_ts';

    protected $fillable = [
        'user_name',
        'user_profile_full_name',
        'user_profile_email',
        'user_pwd_method',
        'user_pwd_salt',
        'user_pwd_secret',
        'user_pwd_expire',
        'user_active',
        'created_by',
        'lastupdated_by',
        'deleted_by',
        'is_deleted'
    ];

    protected $dates = ['deleted_ts'];

    public function user_roles()
    {
        return $this->hasMany('App\UserRoles', 'user_name');
    }

    public function password_reset_request()
    {
        return $this->hasOne('App\PasswordResetRequest', 'user_name');
    }
}
?>
