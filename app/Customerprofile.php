<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Customerprofile extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_customer';

    public static function update_outboud_order($order_id)
    {
        return DB::table('nm_order')
        ->where('nm_order.order_id','=',$order_id)
        ->update(array(
            'status_outbound' => 5
            ));
    }

    public static function get_detail_product_by_order_id($order_id)
    {
        return DB::table("nm_order")
        ->where('nm_order.order_id', '=', $order_id)
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->leftjoin('nm_subcategory','nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->leftjoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->leftjoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->first();
    }

    public static function update_status_konfirmasi($order_id, $date, $warranty_date)
    {
        return DB::table('nm_order')
        ->where('nm_order.order_id', '=', $order_id)
        ->update(array(
                'order_tgl_konfirmasi_penerimaan_barang' => $date,
                'order_garansi_expired' => $warranty_date
            ));
    }

    public static function update_status_outbound($id_transaksi)
    {
        return DB::table('nm_transaksi')
        ->where('nm_transaksi.transaction_id', '=', $id_transaksi)
        ->update(array(
            'status_outbound_transaksi' => 5
        ));
    }

    public static function insert_history_approval($data)
    {
        return DB::table('nm_history_outbound')->insert($data);
    }

    public static function get_last_history($id_transaksi)
    {
        return DB::table('nm_history_outbound')
        ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_history_outbound.id_transaksi')
        ->select('nm_history_outbound.status')
        ->orderBy('nm_history_outbound.tgl_update', 'DESC')
        ->where('nm_history_outbound.id_transaksi', '=', $id_transaksi)
        ->first();
    }

    public static function get_detail_transaksi($customerid)
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.transaction_id','total_belanja as total','diskon','nm_transaksi.order_status', 'order_paytype', 'order_date','pro_title','bukti_transfer','status_pembayaran', 'metode_pembayaran', 'shipping', 'nm_order.postalservice_code', 'nm_order.order_type', 'nm_order.payment_channel')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_id', 'desc')
        ->where('order_cus_id', '=', $customerid)
        ->get();
    }

    public static function hapus_file_bukti($id_transaksi)
    {
        return DB::table('nm_transaksi')->where('transaction_id', '=', $id_transaksi)->update(array('bukti_transfer' => "",'status_pembayaran' => 'belum dibayar'));
    }

    public static function get_customer_details($customerid)
    {
        return DB::table('nm_customer')->leftjoin('nm_city', 'nm_customer.cus_city', '=', 'nm_city.ci_id')->leftjoin('nm_country', 'nm_customer.cus_country', '=', 'nm_country.co_id')->where('cus_id', '=', $customerid)->get();

    }

    public static function get_customer_shipping_details($customerid)
    {
        return DB::table('nm_shipping')->where('ship_cus_id', '=', $customerid)->get();

    }

    public static function getproductordersdetails($customerid)
    {
        return DB::table('nm_order')
        ->join('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->join('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
        ->groupBy('nm_order.transaction_id','nm_order.order_status')
        ->orderBy('nm_order.order_date', 'desc')
        ->select('transaction_id','order_status',DB::raw('sum(order_amt) as total'), 'order_paytype', 'order_date','pro_title')
        ->where('order_cus_id', '=', $customerid)
        ->get();

    }

    public static function getproductordersdetailss($customerid)
    {
        return DB::table('nm_ordercod')->join('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->join('nm_shipping', 'nm_ordercod.cod_cus_id', '=', 'nm_shipping.ship_cus_id')->groupBy('nm_ordercod.cod_pro_id')->orderBy('nm_ordercod.cod_date', 'desc')->where('cod_cus_id', '=', $customerid)->get();

    }

    public static function get_shipping_details($customerid)
    {
        return DB::table('nm_shipping')->where('ship_cus_id', '=', $customerid)->orderBy('ship_id', 'desc')->get();

    }

    public static function update_customer_name($cname, $cusid)
    {
        return DB::table('nm_customer')->where('cus_id', '=', $cusid)->update(array('cus_name' => $cname));

    }

    public static function update_address1($addr1, $cusid)
    {
        return DB::table('nm_customer')->where('cus_id', '=', $cusid)->update(array('cus_address1' => $addr1));

    }

    public static function update_address2($addr2, $cusid)
    {
        return DB::table('nm_customer')->where('cus_id', '=', $cusid)->update(array('cus_address2' => $addr2));

    }

    public static function update_city($city, $country, $cusid)
    {
        return DB::table('nm_customer')->where('cus_id', '=', $cusid)->update(array(
            'cus_city' => $city,
            'cus_country' => $country
        ));

    }

    public static function update_phonenumber($phonenum, $cusid)
    {
        return DB::table('nm_customer')->where('cus_id', '=', $cusid)->update(array('cus_phone' => $phonenum));

    }

    public static function update_newpwd($customerid, $confirmpwd)
    {

        return DB::table('nm_customer')->where('cus_id', '=', $customerid)->update(array('cus_pwd' => $confirmpwd));
    }

    public static function check_oldpwd($cusid, $oldpwd)
    {
        return DB::table('nm_customer')->where('cus_id', '=', $cusid)->where('cus_pwd', '=', $oldpwd)->get();

    }

    public static function insert_shipping($entry)
    {
        return DB::table('nm_shipping')->insert($entry);
    }

    public static function update_shipping($entry, $customerid)
    {
        return DB::table('nm_shipping')->where('ship_cus_id', '=', $customerid)->update($entry);

    }

    public static function update_customer_profile($entry, $customerid)
    {
        return DB::table('nm_customer')->where('cus_id', $customerid)->update($entry);
    }

    public static function update_profileimage($customerid, $filename)
    {

        return DB::table('nm_customer')
        ->where('cus_id', '=', $customerid)
        ->update(array('cus_pic' => $filename));
    }

    public static function insert_bukti_transfer($data)
    {

        return DB::table('nm_transaksi')
        ->where('transaction_id', '=',  $data['transaction_id'])
        ->update(array('bukti_transfer' => $data['bukti_transfer'], 'status_pembayaran' => $data['status_pembayaran']));
    }

    public static function get_wishlistdetails($customerid)
    {
        return DB::table('nm_wishlist')->join('nm_product', 'nm_wishlist.ws_pro_id', '=', 'nm_product.pro_id')->join('nm_maincategory', 'nm_product.pro_mc_id', '=', 'nm_maincategory.mc_id')->join('nm_secmaincategory', 'nm_product.pro_smc_id', '=', 'nm_secmaincategory.smc_id')->join('nm_subcategory', 'nm_product.pro_sb_id', '=', 'nm_subcategory.sb_id')->join('nm_secsubcategory', 'nm_product.pro_ssb_id', '=', 'nm_secsubcategory.ssb_id')->where('ws_cus_id', '=', $customerid)->get();

    }

    public static function get_wishlistdetailscnt($customerid)
    {
        return DB::table('nm_wishlist')->where('ws_cus_id', '=', $customerid)->count();

    }

    public static function get_bidhistorycnt($customerid)
    {
        return DB::table('nm_order_auction')->where('oa_cus_id', '=', $customerid)->count();

    }

    public static function get_bidhistory($customerid)
    {
        return DB::table('nm_order_auction')->join('nm_auction', 'nm_order_auction.oa_pro_id', '=', 'nm_auction.auc_id')->where('oa_cus_id', '=', $customerid)->get();

    }

    public static function get_general_settings()
    {
	return DB::table('nm_generalsetting')->get();

    }

}

?>
