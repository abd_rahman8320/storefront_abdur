<?php
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Bannerset extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_banner';
    protected $table2 = 'nm_paymentmethod';

    public static function save_banner($sizes)
    {
        return DB::table('nm_banner')->insert($sizes);
    }

    public static function save_popup($sizes)
    {
        return DB::table('nm_popup_promo')->insert($sizes);
    }
    public static function view_popup_list()
    {
        return DB::table('nm_popup_promo')->get();
    }

    public static function save_img_paymentmethod($sizes)
    {
        return DB::table('nm_paymentmethod')->insert($sizes);
    }

    public static function view_img_paymentmethod_list()
    {
        return DB::table('nm_paymentmethod')->get();
    }

    public static function view_banner_list()
    {
        return DB::table('nm_banner')
        ->orderby('bn_sort','asc')
        ->get();
    }

    public static function banner_detail($id)
    {
        return DB::table('nm_banner')->where('bn_id', '=', $id)->get();
    }

    public static function popup_details($id)
    {
        return DB::table('nm_popup_promo')->where('pop_id', '=', $id)->get();
    }
    public static function update_popup_detail($entry, $id)
    {
        return DB::table('nm_popup_promo')->where('pop_id', '=', $id)->update($entry);
    }
    public static function status_popup($id, $status)
    {
        return DB::table('nm_popup_promo')->where('pop_id', '=', $id)->update(array(
            'pop_status' => $status
        ));
    }
    public static function status_popup_default($id, $status)
    {
        return DB::table('nm_popup_promo')->where('pop_id', '=', $id)->update(array(
            'pop_default' => $status
        ));
    }
    public static function delete_popup_img($id)
    {

        // To start Image delete from folder 09/11/
        $filename = DB::table('nm_popup_promo')->where('pop_id', '=', $id)->first();
        $getimagename= $filename->pop_image;
        $getextension=explode("/**/",$getimagename);
        foreach($getextension as $imgremove)
        {
            File::delete(base_path('assets/PopUp/').$imgremove);
        }
        // To End
        return DB::table('nm_popup_promo')->where('pop_id', '=', $id)->delete();

    }
    public static function paymentmethodimage_detail($id)
    {
        return DB::table('nm_paymentmethod')->where('pm_id', '=', $id)->get();
    }
    public static function update_paymentmethod_detail($entry, $id)
    {
        return DB::table('nm_paymentmethod')->where('pm_id', '=', $id)->update($entry);
    }
    public static function status_paymentmethod_img($id, $status)
    {
        return DB::table('nm_paymentmethod')->where('pm_id', '=', $id)->update(array(
            'pm_status' => $status
        ));
    }
    public static function delete_paymentmethod_img($id)
    {

        // To start Image delete from folder 09/11/
        $filename = DB::table('nm_paymentmethod')->where('pm_id', '=', $id)->first();
        $getimagename= $filename->pm_image;
        $getextension=explode("/**/",$getimagename);
        foreach($getextension as $imgremove)
        {
            File::delete(base_path('assets/paymentmethodimage/').$imgremove);
        }
        // To End
        return DB::table('nm_paymentmethod')->where('pm_id', '=', $id)->delete();

    }
    // public static function delete_banner($id)
    // {
    //     return DB::table('nm_banner')->where('bn_id', '=', $id)->delete();
    // }
    public static function delete_banner($id)
    {

        // To start Image delete from folder 09/11/
        $filename = DB::table('nm_banner')->where('bn_id', '=', $id)->first();
        $getimagename= $filename->bn_img;
        $getextension=explode("/**/",$getimagename);
        foreach($getextension as $imgremove)
        {
            File::delete(base_path('assets/bannerimage/').$imgremove);
        }
        // To End
        return DB::table('nm_banner')->where('bn_id', '=', $id)->delete();

    }

    public static function update_banner_detail($entry, $id)
    {
        return DB::table('nm_banner')->where('bn_id', '=', $id)->update($entry);
    }

    public static function status_banner($id, $status)
    {
        return DB::table('nm_banner')->where('bn_id', '=', $id)->update(array(
            'bn_status' => $status
        ));
    }

}
?>
