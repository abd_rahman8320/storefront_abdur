<?php
namespace App;
use DB;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class Dashboard extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_customer';

    public static function get_all_dashboard()
    {
        return DB::table("nm_banner")
        ->orderby('nm_banner.bn_id','DESC')
        ->get();
    }

    public static function get_status_banner_all()
    {
        return DB::table("nm_banner")
        ->orderby('nm_banner.bn_id','DESC')
        ->where('nm_banner.bn_status','=', 0)
        ->get();
    }

    public static function get_banner_history()
    {
        return DB::table("nm_history_banner")
        ->leftJoin('nm_banner','nm_banner.bn_id', '=', 'nm_history_banner.id_banner')
        ->get();
    }

    public static function get_active_products()
    {
        return DB::table('nm_product')->where('pro_status', '=', 1)->count();
    }

    public static function get_approval_products($kuku_merchant_id)
    {
        return DB::table('nm_product')
        ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
        ->where('pro_mr_id', '<>', $kuku_merchant_id)
        ->where('pro_isresponse', 0)
        ->where('pro_isapproved', 0)
        ->where('pro_title','!=','Jasa Pengiriman')
        ->where('pro_title','!=','Diskon')
        ->orderBy('nm_product.pro_id', 'asc')
        ->count();
    }

    public static function get_wms_products($kuku_merchant_id)
    {
        return DB::table('nm_product')
        ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
        ->where('pro_mr_id', $kuku_merchant_id)
        ->where('pro_qty', '<>', 0)
        ->where('pro_Img', '=', "")
        ->count();
    }

    public static function get_merchant_outbound()
    {
        return DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_customer', 'nm_customer.cus_id', '=', 'nm_order.order_cus_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
        ->where('nm_product.pro_mr_id','!=','0')
        ->where('nm_transaksi.status_pembayaran', '=', 'sudah dibayar')
        ->count();
    }

    public static function get_transfer_bank()
    {
        return DB::table('nm_transaksi')
        //->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
        ->where('nm_transaksi.metode_pembayaran', '=', 'Transfer Bank')
        ->where('nm_transaksi.bukti_transfer', '!=', '')
        ->count();
    }

    public static function get_columbia()
    {
        return DB::table('nm_transaksi')
        //->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
        ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
        ->where('nm_transaksi.bukti_transfer', '!=', '')
        ->count();
    }

    public static function get_fundreq_paid($mer_id)
    {
        return DB::table('nm_withdraw_request')
        ->where('wd_status','=',1)
        ->where('wd_mer_id','=',$mer_id)
        ->count();
    }

    public static function get_mer_active_products($id)
    {
        return DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mr_id', '=', $id)->count();
    }

    public static function get_shipcnt_details()
    {
        return DB::table('nm_order')->count();

    }

    public static function get_subscriberscount()
    {
        return DB::table('nm_subscription')->where('sub_readstatus', '=', 0)->count();

    }

    public static function get_charttransaction_details()
    {

        $chart_count = "";
        date_default_timezone_set('Asia/Jakarta');
        $month = date('m');
        $year = date('Y');
        $change_year = 0;
        // $results = DB::table('nm_order')->whereMonth('order_date', '=', 12)->whereYear('order_date', '=', 2016)->select('transaction_id', DB::raw("count(transaction_id) as total"))->get();
        for ($i = 1; $i <= 12; $i++) {
            // $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_order WHERE MONTH( `order_date` ) = " . $i));
            if($i > $month && $change_year == 0){
                $year = date('Y', strtotime('-1 year'));
                $change_year = 1;
            }
            $results = DB::table('nm_order')->whereMonth('order_date', '=', $i)->whereYear('order_date', '=', $year)->groupBy('transaction_id')->get();
            // dd(count($results));
            $chart_count .= count($results) . ",";
        }
        // dd($chart_count);
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_inquiriescnt()
    {
        return DB::table('nm_enquiry')->where('status', '=', 0)->count();

    }

    public static function get_merchants()
    {
        return DB::table('nm_merchant')
        ->join('nm_store', 'nm_store.stor_merchant_id', '=', 'nm_merchant.mer_id')
        ->join('nm_city', 'nm_city.ci_id', '=', 'nm_merchant.mer_ci_id')
        ->where('status_approval','=', 1)
        ->orderBy('nm_merchant.mer_id', 'DESC')
        ->count();

    }

    public static function get_customers()
    {
        return DB::table('nm_customer')->where('cus_status', '=', 0)->count();

    }

    public static function get_active_deals()
    {
        date_default_timezone_set("Asia/Kolkata");
        $date = date('Y-m-d H:i:s');
        return DB::table('nm_deals')->where('deal_end_date', '>', $date)->where('deal_status', '=', 1)->count();

    }

    public static function get_active_auction()
    {
        date_default_timezone_set("Asia/Kolkata");
        $date = date('Y-m-d H:i:s');
        return DB::table('nm_auction')->where('auc_end_date', '>', $date)->where('auc_status', '=', 1)->count();

    }

    public static function get_auction_winners()
    {
        return DB::table('nm_order_auction')->where('oa_bid_winner', '=', 1)->count();
    }

    public static function get_mer_active_deals($id, $date)
    {
        return DB::table('nm_deals')->where('deal_end_date', '>', $date)->where('deal_status', '=', 1)->where('deal_merchant_id', '=', $id)->count();

    }

    public static function get_mer_archievd_deals($id)
    {
        $date = date('Y-m-d H:i:s');
        return DB::table('nm_deals')->where('deal_merchant_id', '=', $id)->where('deal_end_date', '<', $date)->count();
    }

    public static function get_mer_archievd_auction($id)
    {
        $date = date('Y-m-d H:i:s');
        return DB::table('nm_auction')->where('auc_merchant_id', '=', 1)->where('auc_end_date', '<', $date)->count();
    }

    public static function get_mer_active_auction($id)
    {
        return DB::table('nm_auction')->where('auc_status', '=', 1)->where('auc_merchant_id', '=', 1)->count();

    }

    public static function get_mer_auction_winners($id)
    {
        return DB::table('nm_order_auction')->where('oa_bid_winner', '=', '1')->count();
    }

    public static function get_subscribers()
    {
        return DB::table('nm_subscription')->count();
    }

    public static function get_mer_stores($id)
    {
        return DB::table('nm_store')->where('stor_merchant_id', '=', $id)->count();
    }

    public static function get_stores()
    {
        return DB::table('nm_store')->where('stor_status', '=', 1)->count();
    }

    public static function get_fundrequest()
    {
        return DB::table('nm_withdraw_request')->where('wd_status', 0)->count();
    }

    public static function get_sold_products()
    {
        return DB::table('nm_product')->get();

    }

    public static function get_mer_sold_products($id)
    {
        return DB::table('nm_product')->where('pro_mr_id', '=', $id)->get();

    }

    public static function get_customer_list()
    {
        return DB::table('nm_customer')->count();
    }

    public static function get_inquires()
    {
        return DB::table('nm_inquiries')->count();

    }

    public static function get_blog()
    {
        return DB::table('nm_blog')->count();

    }

    public static function get_faq()
    {
        return DB::table('nm_faq')->count();

    }

    public static function get_category()
    {
        return DB::table('nm_maincategory')->count();

    }

    public static function get_chart_details()
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_customer WHERE MONTH( `cus_joindate` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_chart6_details()
    {
        $tot_result = "";
        $count      = DB::select(DB::raw("SELECT count(*) as tot FROM `nm_customer` group BY `cus_logintype` ASC ORDER BY `cus_logintype` ASC "));
        foreach ($count as $result) {
            $tot_result .= $result->tot . ",";
        }
        return trim($tot_result, ",");
    }

}

?>
