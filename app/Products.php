<?php
namespace App;
use DB;
use File;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class Products extends Model
{

    protected $guarded = array('id');
    protected $primaryKey = 'pro_id';
    protected $table = 'nm_product';
    public $timestamps = false;

    public static function get_sort($id)
    {
        return DB::table('nm_groping_product_detail')->where('group_detail_id_product', $id)->first();
    }

    public static function get_promo_flash_product(){
      $query = DB::table('nm_scheduled_promo')
                ->join('nm_promo_products', 'nm_promo_products.promop_schedp_id', '=', 'nm_scheduled_promo.schedp_id')
                ->join('nm_product', 'nm_product.pro_id', '=', 'nm_promo_products.promop_pro_id')
                ->join('nm_maincategory', 'nm_maincategory.mc_id' , '=', 'nm_product.pro_mc_id')
                ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id' , '=', 'nm_product.pro_smc_id')
                ->join('nm_subcategory', 'nm_subcategory.sb_id' , '=', 'nm_product.pro_sb_id')
                ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id' , '=', 'nm_product.pro_ssb_id');
      $query->where('nm_scheduled_promo.schedp_simple_action', '=', 'flash_promo')
      ->where('nm_product.pro_status', 1)
      ->where('nm_product.pro_isapproved', 1)
      ->where('nm_product.pro_isresponse', 1)
      ->where('mc_status', 1)
      ->where('smc_status', 1)
      ->where('sb_status', 1)
      ->where('ssb_status', 1)
      ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

      $query = $query->get();
      return $query;
    }
 public static function get_product_value($sp_id)
    {
        return DB::table('nm_spec_value')->where('nm_specification_id', $sp_id)->orderBy('short_desc', 'asc')->get();
    }


    public static function update_sort($sort, $id_pro, $id_group)
    {
        $check_exist = DB::table('nm_groping_product_detail')
        ->where('group_detail_id_grouping', $id_group)
        ->where('group_detail_sort', $sort)
        ->first();
        $select = DB::table('nm_groping_product_detail')
        ->where('group_detail_id_product', $id_pro)
        ->where('group_detail_id_grouping', $id_group)
        ->first();
        if($check_exist)
        {
            $update1 = DB::table('nm_groping_product_detail')
            ->where('group_detail_id', $check_exist->group_detail_id)
            ->update([
                'group_detail_sort' => $select->group_detail_sort
            ]);
            $update2 = DB::table('nm_groping_product_detail')
            ->where('group_detail_id', $select->group_detail_id)
            ->update([
                'group_detail_sort' => $sort
            ]);
        }else {
            $update2 = DB::table('nm_groping_product_detail')
            ->where('group_detail_id', $select->group_detail_id)
            ->update([
                'group_detail_sort' => $sort
            ]);
        }
        return $update2;
    }

    public static function get_grade()
    {
        return DB::table('nm_grade')->get();
    }

      public static function tambahkan_model($data)
      {
        return DB::table('nm_grouping_product_model_detail')
        ->insert($data);
      }

      public static function keluarkan_model($id_detail)
      {
        return DB::table('nm_grouping_product_model_detail')
        ->where('id', '=', $id_detail)
        ->delete();
      }

      public static function get_detail_header($id_model_header)
      {
        return DB::table('nm_grouping_product_model_header')
        ->where('id', '=', $id_model_header)
        ->get();
      }

      public static function delete_model_header($id_model_header)
      {
        return DB::table('nm_grouping_product_model_header')
        ->where('id', '=', $id_model_header)
        ->delete();
      }

      public static function insert_grouping_model_header($data)
      {
        return DB::table('nm_grouping_product_model_header')->insert($data);
      }

      public static function list_model()
      {
        return DB::table('nm_grouping_product_model_header')
        ->orderBy('id','DESC')
        ->get();
      }

      public static function get_all(){
          $mer_id = Session::get('merchantid');
          return DB::table('nm_product')
          ->where('pro_mr_id','=',$mer_id)
          ->where('pro_isapproved', '=', 1)
          ->get();
      }

      public static function get_promo_product()
      {
          return DB::table('nm_product')
          ->where('pro_id', '<>', 1)
          ->where('pro_id', '<>', 2)
          ->where('pro_isapproved', '=', 1)
          ->get();
      }

      public static function get_last_id_group()
      {
        return DB::table('nm_groping_product')
        ->orderBy('group_id', 'ASC')
        ->first();
      }

      //search mc
      public static function search_mc($param){

          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_title', 'LIKE', '%'.$param['pro_title'].'%')
          ->where('pro_status', '=', 1)
          ->where('pro_isapproved', 1)
          ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
          ->where('mc_status', 1)
          ->where('smc_status', 1)
          ->where('sb_status', 1)
          ->where('ssb_status', 1);

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

        $total =$query->count();
        $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_maincategory.mc_name');
        $result = $groupBy->get();

        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_mc_category($param)
      {
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where(function($q) use($param){
              $q = $q->where(function($w) use($param){
                  $w = $w->where('mc_name', '=', $param['pro_title'])
                  ->where('mc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('smc_name', '=', $param['pro_title'])
                  ->where('smc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('sb_name', '=', $param['pro_title'])
                  ->where('sb_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('ssb_name', '=', $param['pro_title'])
                  ->where('ssb_status', 1);
              });
          });
          $query = $query->where('pro_status', '=', 1)
          ->where('pro_isapproved', '=', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

          $total =$query->count();
          $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_maincategory.mc_name');
          $result = $groupBy->get();

          return ['data'=>$result, 'total'=> $total];
      }

      //search smc
      public static function search_smc($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_title', 'LIKE', '%'.$param['pro_title'].'%')
          ->where('pro_status', '=', 1)
          ->where('pro_isapproved', 1)
          ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
          ->where('mc_status', 1)
          ->where('smc_status', 1)
          ->where('sb_status', 1)
          ->where('ssb_status', 1);

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

        $total =$query->count();
        $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_secmaincategory.smc_name');
        $result = $groupBy->get();

        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_smc_category($param)
      {
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where(function($q) use($param){
              $q = $q->where(function($w) use($param){
                  $w = $w->where('mc_name', '=', $param['pro_title'])
                  ->where('mc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('smc_name', '=', $param['pro_title'])
                  ->where('smc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('sb_name', '=', $param['pro_title'])
                  ->where('sb_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('ssb_name', '=', $param['pro_title'])
                  ->where('ssb_status', 1);
              });
          });
          $query = $query->where('pro_status', '=', 1)
          ->where('pro_isapproved', '=', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

          $total =$query->count();
          $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_secmaincategory.smc_name');
          $result = $groupBy->get();

          return ['data'=>$result, 'total'=> $total];
      }

      //search sb
      public static function search_sb($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_title', 'LIKE', '%'.$param['pro_title'].'%')
          ->where('pro_status', '=', 1)
          ->where('pro_isapproved', 1)
          ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
          ->where('mc_status', 1)
          ->where('smc_status', 1)
          ->where('sb_status', 1)
          ->where('ssb_status', 1);

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

        $total =$query->count();
        $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_subcategory.sb_name');
        $result = $groupBy->get();

        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_sb_category($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where(function($q) use($param){
              $q = $q->where(function($w) use($param){
                  $w = $w->where('mc_name', '=', $param['pro_title'])
                  ->where('mc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('smc_name', '=', $param['pro_title'])
                  ->where('smc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('sb_name', '=', $param['pro_title'])
                  ->where('sb_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('ssb_name', '=', $param['pro_title'])
                  ->where('ssb_status', 1);
              });
          });
          $query = $query->where('pro_status', '=', 1)
          ->where('pro_isapproved', '=', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

        $total =$query->count();
        $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_subcategory.sb_name');
        $result = $groupBy->get();

        return ['data'=>$result, 'total'=> $total];
      }

      //search ssb
      public static function search_ssb($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_title', 'LIKE', '%'.$param['pro_title'].'%')
          ->where('pro_status', '=', 1)
          ->where('pro_isapproved', 1)
          ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
          ->where('mc_status', 1)
          ->where('smc_status', 1)
          ->where('sb_status', 1)
          ->where('ssb_status', 1);

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

        $total =$query->count();
        $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_secsubcategory.ssb_name');
        $result = $groupBy->get();

        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_ssb_category($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where(function($q) use($param){
              $q = $q->where(function($w) use($param){
                  $w = $w->where('mc_name', '=', $param['pro_title'])
                  ->where('mc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('smc_name', '=', $param['pro_title'])
                  ->where('smc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('sb_name', '=', $param['pro_title'])
                  ->where('sb_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('ssb_name', '=', $param['pro_title'])
                  ->where('ssb_status', 1);
              });
          });
          $query = $query->where('pro_status', '=', 1)
          ->where('pro_isapproved', '=', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

        $total =$query->count();
        $groupBy = $query->select('*', DB::raw('count(*) as total'))->groupBy('nm_secsubcategory.ssb_name');
        $result = $groupBy->get();

        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_spc($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_status', '=', 1)
          ->where('pro_isapproved', 1)
          ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
          ->where('mc_status', 1)
          ->where('smc_status', 1)
          ->where('sb_status', 1)
          ->where('ssb_status', 1);

          if (!empty($param['pro_title'])) {
              $query = $query->where('pro_title', 'LIKE', '%'.$param['pro_title'].'%');
          }
          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

          $hasil = $query->select('pro_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_product')
          ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
          ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('pro_id', $hasil2->pro_id);
              }
          })
          ->where('spc_value', '!=', '-');
          if (!empty($param['spc_value'])) {
              $last_spc_value = end($param['spc_value']);

              $sp_id = DB::table('nm_prospec')
              ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
              ->where('spc_value', $last_spc_value)
              ->select('sp_id')
              ->first();

              $result = $result->orWhere('sp_id', $sp_id->sp_id);
          }

            $result = $result->groupBy([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
            ])
            ->havingRaw('count(*) > 0')
            ->select([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
              DB::raw('count(*) as qty')
            ])->get();
          $result = collect($result)->groupBy('sp_name');

          return $result;
      }

      public static function search_spc_category($param){
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where(function($q) use($param){
              $q = $q->where(function($w) use($param){
                  $w = $w->where('mc_name', '=', $param['pro_title'])
                  ->where('mc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('smc_name', '=', $param['pro_title'])
                  ->where('smc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('sb_name', '=', $param['pro_title'])
                  ->where('sb_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('ssb_name', '=', $param['pro_title'])
                  ->where('ssb_status', 1);
              });
          });
          $query = $query->where('pro_status', '=', 1)
          ->where('pro_isapproved', '=', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

          $hasil = $query->select('pro_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_product')
          ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
          ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('pro_id', $hasil2->pro_id);
              }
          })
          ->where('spc_value', '!=', '-');
          if (!empty($param['spc_value'])) {
              $last_spc_value = end($param['spc_value']);

              $sp_id = DB::table('nm_prospec')
              ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
              ->where('spc_value', $last_spc_value)
              ->select('sp_id')
              ->first();

              $result = $result->orWhere('sp_id', $sp_id->sp_id);
          }

            $result = $result->groupBy([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
            ])
            ->havingRaw('count(*) > 0')
            ->select([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
              DB::raw('count(*) as qty')
            ])->get();
          $result = collect($result)->groupBy('sp_name');

          return $result;
      }

      public static function search_grade($param)
      {
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_status', '=', 1)
          ->where('pro_isapproved', 1)
          ->where('nm_product.pro_no_of_purchase','<', DB::raw('pro_qty'))
          ->where('mc_status', 1)
          ->where('smc_status', 1)
          ->where('sb_status', 1)
          ->where('ssb_status', 1);

          if (!empty($param['pro_title'])) {
              $query = $query->where('pro_title', 'LIKE', '%'.$param['pro_title'].'%');
          }
          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

          $hasil = $query->select('pro_id', 'pro_grade_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_grade')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('grade_id', $hasil2->pro_grade_id);
              }
          });

            $result = $result->groupBy('grade_id')
            ->havingRaw('count(*) > 0')
            ->select([
              'grade_id',
              'grade_name',
              DB::raw('count(*) as qty')
            ])->get();

          return $result;
      }

      public static function search_grade_category($param)
      {
          $query = DB::table('nm_product')
          ->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where(function($q) use($param){
              $q = $q->where(function($w) use($param){
                  $w = $w->where('mc_name', '=', $param['pro_title'])
                  ->where('mc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('smc_name', '=', $param['pro_title'])
                  ->where('smc_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('sb_name', '=', $param['pro_title'])
                  ->where('sb_status', 1);
              })
              ->orWhere(function($w) use($param){
                  $w = $w->where('ssb_name', '=', $param['pro_title'])
                  ->where('ssb_status', 1);
              });
          });
          $query = $query->where('pro_status', '=', 1)
          ->where('pro_isapproved', '=', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }



              //bikin if else jagain parameter spec,
          if (!empty($param['spc_value'])) {
            //bikin query spec yang sudah terfilter
            //$filtered_query = DB::table('nm_prospec')
              // $filtered_query->whereRaw("spc_value LIKE '". $param['spc_value']."'");
              $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')

              ->where('spc_value', '=', $param['spc_value']);


            //query diatas (product) di wherein id productnya
            //  $query = $query->whereRaw('pro_id in ( '.$filtered_query->toSql().')');
          }
          if (!empty($param['grade'])) {
              $query = $query->where('pro_grade_id', $param['grade']);
          }

          $hasil = $query->select('pro_id', 'pro_grade_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_grade')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('grade_id', $hasil2->pro_grade_id);
              }
          });

            $result = $result->groupBy('grade_id')
            ->havingRaw('count(*) > 0')
            ->select([
              'grade_id',
              'grade_name',
              DB::raw('count(*) as qty')
            ])->get();

          return $result;
      }

      public static function search_spc_filter($param)
      {
          $query = DB::table('nm_product')
          ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
          ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
          ->where('pro_isapproved', 1)
          ->where('pro_status', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['pro_title'])) {
             $query = $query->where('pro_title', 'like' ,'%'.$param['pro_title'].'%');
           }
           if (!empty($param['max_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', 0)->where('pro_price', '<=', floatval($param['max_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '<=', floatval($param['max_price']));
                   });
               });
           //   $query = $query->where('pro_price', '<=', floatval($param['max_price']));
           }
           if (!empty($param['min_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice' , 0)->where('pro_price', '>=', floatval($param['min_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '>=', floatval($param['min_price']));
                   });
               });
           //   $query = $query->where('pro_price', '>=', floatval($param['min_price']));
           }

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }
          if (!empty($param['grade'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where('pro_grade_id', $param['grade'][0]);
                  if (!empty($param['grade'][1])) {
                      for ($i=1; $i < count($param['grade']); $i++) {
                          $q = $q->orWhere('pro_grade_id', $param['grade'][$i]);
                      }
                  }
              });
          }
          if (!empty($param['spc_value'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where('spc_value', $param['spc_value'][0]);
                  if (!empty($param['spc_value'][1])) {
                      for ($i=1; $i < count($param['spc_value']); $i++) {
                          $q = $q->orWhere('spc_value', $param['spc_value'][$i]);
                      }
                  }
              });
          }

          if (!empty($param['skip'])) {
            $query = $query->skip($param['skip']);
          }
          if (!empty($param['count'])) {
            $query = $query->take($param['count']);
          }

          $hasil = $query->select('pro_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_product')
          ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
          ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('pro_id', $hasil2->pro_id);
              }
          })
          ->where('spc_value', '!=', '-');
          if (!empty($param['spc_value'])) {
              $last_spc_value = end($param['spc_value']);

              $sp_id = DB::table('nm_prospec')
              ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
              ->where('spc_value', $last_spc_value)
              ->select('sp_id')
              ->first();

              $result = $result->orWhere('sp_id', $sp_id->sp_id);
          }

            $result = $result->groupBy([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
            ])
            ->havingRaw('count(*) > 0')
            ->select([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
              DB::raw('count(*) as qty')
            ])->get();
          $result = collect($result)->groupBy('sp_name');

          return $result;
      }

      public static function search_spc_filter_category($param)
      {
          $query = DB::table('nm_product')
          ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
          ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
          ->where('pro_isapproved', 1)
          ->where('pro_status', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['pro_title'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where(function($w) use($param){
                      $w = $w->where('mc_name', '=', $param['pro_title'])
                      ->where('mc_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('smc_name', '=', $param['pro_title'])
                      ->where('smc_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('sb_name', '=', $param['pro_title'])
                      ->where('sb_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('ssb_name', '=', $param['pro_title'])
                      ->where('ssb_status', 1);
                  });
              });
           }
           if (!empty($param['max_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', 0)->where('pro_price', '<=', floatval($param['max_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '<=', floatval($param['max_price']));
                   });
               });
           //   $query = $query->where('pro_price', '<=', floatval($param['max_price']));
           }
           if (!empty($param['min_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice' , 0)->where('pro_price', '>=', floatval($param['min_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '>=', floatval($param['min_price']));
                   });
               });
           //   $query = $query->where('pro_price', '>=', floatval($param['min_price']));
           }

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }
          if (!empty($param['grade'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where('pro_grade_id', $param['grade'][0]);
                  if (!empty($param['grade'][1])) {
                      for ($i=1; $i < count($param['grade']); $i++) {
                          $q = $q->orWhere('pro_grade_id', $param['grade'][$i]);
                      }
                  }
              });
          }
          if (!empty($param['spc_value'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where('spc_value', $param['spc_value'][0]);
                  if (!empty($param['spc_value'][1])) {
                      for ($i=1; $i < count($param['spc_value']); $i++) {
                          $q = $q->orWhere('spc_value', $param['spc_value'][$i]);
                      }
                  }
              });
          }

          if (!empty($param['skip'])) {
            $query = $query->skip($param['skip']);
          }
          if (!empty($param['count'])) {
            $query = $query->take($param['count']);
          }

          $hasil = $query->select('pro_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_product')
          ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
          ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('pro_id', $hasil2->pro_id);
              }
          })
          ->where('spc_value', '!=', '-');
          if (!empty($param['spc_value'])) {
              $last_spc_value = end($param['spc_value']);

              $sp_id = DB::table('nm_prospec')
              ->join('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
              ->where('spc_value', $last_spc_value)
              ->select('sp_id')
              ->first();

              $result = $result->orWhere('sp_id', $sp_id->sp_id);
          }

            $result = $result->groupBy([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
            ])
            ->havingRaw('count(*) > 0')
            ->select([
              'nm_specification.sp_name',
              'nm_prospec.spc_value',
              DB::raw('count(*) as qty')
            ])->get();
          $result = collect($result)->groupBy('sp_name');

          return $result;
      }

      public static function search_max_min($param){

        $query = DB::table('nm_product')
        ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->where('pro_isapproved', 1)
        ->where('pro_status', 1)
        ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));;

        if (!empty($param['pro_title'])) {
           $query = $query->where('pro_title', 'like' ,'%'.$param['pro_title'].'%');
         }
        if (!empty($param['max_price'])) {
          $query = $query->where('pro_disprice', '<=', floatval($param['max_price']));
        }
        if (!empty($param['min_price'])) {
          $query = $query->where('pro_disprice', '>=', floatval($param['max_price']));
        }

        if (!empty($param['mc_id'])) {
          $query = $query->where('pro_mc_id', '=', $param['mc_id']);
        }
        if (!empty($param['smc_id'])) {
          $query = $query->where('pro_smc_id', '=', $param['smc_id']);
        }
        if (!empty($param['sb_id'])) {
          $query = $query->where('pro_sb_id', '=', $param['sb_id']);
        }
        if (!empty($param['ssb_id'])) {
          $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
        }

        if (!empty($param['skip'])) {
          $query = $query->skip($param['skip']);
        }
        if (!empty($param['count'])) {
          $query = $query->take($param['count']);
        }

        $result['max_price'] = $query->max('pro_price');
        $result['min_price'] = $query->min('pro_price');

        return $result;
      }

      public static function search_max_min_category($param)
      {
          $query = DB::table('nm_product')
          ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_isapproved', 1)
          ->where('pro_status', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));

          if (!empty($param['pro_title'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where(function($w) use($param){
                      $w = $w->where('mc_name', '=', $param['pro_title'])
                      ->where('mc_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('smc_name', '=', $param['pro_title'])
                      ->where('smc_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('sb_name', '=', $param['pro_title'])
                      ->where('sb_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('ssb_name', '=', $param['pro_title'])
                      ->where('ssb_status', 1);
                  });
              });
           }
          if (!empty($param['max_price'])) {
            $query = $query->where('pro_disprice', '<=', floatval($param['max_price']));
          }
          if (!empty($param['min_price'])) {
            $query = $query->where('pro_disprice', '>=', floatval($param['max_price']));
          }

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }

          if (!empty($param['skip'])) {
            $query = $query->skip($param['skip']);
          }
          if (!empty($param['count'])) {
            $query = $query->take($param['count']);
          }

          $result['max_price'] = $query->max('pro_price');
          $result['min_price'] = $query->min('pro_price');

          return $result;
      }

      public static function get_product_spec($param){

        $query = DB::table('nm_product')
        ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
        ->join('nm_prospec', 'nm_prospec.spc_sp_id', '=', 'nm_specification.sp_id');


        if (!empty($param['pro_title'])) {
           $query = $query->where('pro_title', 'like' ,'%'.$param['pro_title'].'%');
         }
        if (!empty($param['max_price'])) {
          $query = $query->where('pro_disprice', '<=', floatval($param['max_price']));
        }
        if (!empty($param['min_price'])) {
          $query = $query->where('pro_disprice', '>=', floatval($param['max_price']));
        }

        if (!empty($param['mc_id'])) {
          $query = $query->where('pro_mc_id', '=', $param['mc_id']);
        }
        if (!empty($param['smc_id'])) {
          $query = $query->where('pro_smc_id', '=', $param['smc_id']);
        }
        if (!empty($param['sb_id'])) {
          $query = $query->where('pro_sb_id', '=', $param['sb_id']);
        }
        if (!empty($param['ssb_id'])) {
          $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
        }

        if (!empty($param['skip'])) {
          $query = $query->skip($param['skip']);
        }
        if (!empty($param['count'])) {
          $query = $query->take($param['count']);
        }

        $total = $query->count();
        $groupBy = $query->select('*')->groupBy('sp_name');
        $result = $query->get();


        return ['data'=>$result, 'total'=>$total];
      }

      public static function get_product_spec_details($param){

        $query = DB::table('nm_product')
        ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
        ->join('nm_prospec', 'nm_prospec.spc_sp_id', '=', 'nm_specification.sp_id');


        if (!empty($param['pro_title'])) {
           $query = $query->where('pro_title', 'like' ,'%'.$param['pro_title'].'%');
         }
        if (!empty($param['max_price'])) {
          $query = $query->where('pro_disprice', '<=', floatval($param['max_price']));
        }
        if (!empty($param['min_price'])) {
          $query = $query->where('pro_disprice', '>=', floatval($param['max_price']));
        }

        if (!empty($param['mc_id'])) {
          $query = $query->where('pro_mc_id', '=', $param['mc_id']);
        }
        if (!empty($param['smc_id'])) {
          $query = $query->where('pro_smc_id', '=', $param['smc_id']);
        }
        if (!empty($param['sb_id'])) {
          $query = $query->where('pro_sb_id', '=', $param['sb_id']);
        }
        if (!empty($param['ssb_id'])) {
          $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
        }
        if (!empty($param['spc_id'])) {
          $query = $query->where('spc_id', '=', $param['spc_id']);
        }

        if (!empty($param['skip'])) {
          $query = $query->skip($param['skip']);
        }
        if (!empty($param['count'])) {
          $query = $query->take($param['count']);
        }

        $total = $query->count();
        $groupBy = $query->select('*')->groupBy('spc_value');
        $result = $query->get();


        return ['data'=>$result, 'total'=>$total];
      }

      public static function search_filter($param){
        // dd($param);
        $query = DB::table('nm_product')
        ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->where('pro_isapproved', 1)
        ->where('pro_status', 1)
        ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));
        // ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
        // ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
        // ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id');

        if (!empty($param['pro_title'])) {
           $query = $query->where('pro_title', 'like' ,'%'.$param['pro_title'].'%');
         }
        if (!empty($param['max_price'])) {
            $query = $query->where(function($q) use($param){
                $q = $q->where(function($q1) use($param){
                    $q1 = $q1->where('pro_disprice', 0)->where('pro_price', '<=', floatval($param['max_price']));
                })
                ->orWhere(function($q1) use($param){
                    $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '<=', floatval($param['max_price']));
                });
            });
        //   $query = $query->where('pro_price', '<=', floatval($param['max_price']));
        }
        if (!empty($param['min_price'])) {
            $query = $query->where(function($q) use($param){
                $q = $q->where(function($q1) use($param){
                    $q1 = $q1->where('pro_disprice' , 0)->where('pro_price', '>=', floatval($param['min_price']));
                })
                ->orWhere(function($q1) use($param){
                    $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '>=', floatval($param['min_price']));
                });
            });
        //   $query = $query->where('pro_price', '>=', floatval($param['min_price']));
        }

        if (!empty($param['mc_id'])) {
          $query = $query->where('pro_mc_id', '=', $param['mc_id']);
        }
        if (!empty($param['smc_id'])) {
          $query = $query->where('pro_smc_id', '=', $param['smc_id']);
        }
        if (!empty($param['sb_id'])) {
          $query = $query->where('pro_sb_id', '=', $param['sb_id']);
        }
        if (!empty($param['ssb_id'])) {
          $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
        }
        if (!empty($param['grade'])) {
            $query = $query->where(function($q) use($param){
                $q = $q->where('pro_grade_id', $param['grade'][0]);
                if (!empty($param['grade'][1])) {
                    for ($i=1; $i < count($param['grade']); $i++) {
                        $q = $q->orWhere('pro_grade_id', $param['grade'][$i]);
                    }
                }
            });
        }
        if (!empty($param['spc_value'])) {
            $query = $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
            ->where(function($q) use($param){
                $q = $q->where('spc_value', $param['spc_value'][0]);
                if (!empty($param['spc_value'][1])) {
                    for ($i=1; $i < count($param['spc_value']); $i++) {
                        $q = $q->orWhere('spc_value', $param['spc_value'][$i]);
                    }
                }
            });
        }

        if (!empty($param['sort_path']) && !empty($param['order'])) {
            $query = $query->orderBy($param['sort_path'], $param['order']);
        }
        if (!empty($param['skip'])) {
          $query = $query->skip($param['skip']);
        }
        if (!empty($param['count'])) {
          $query = $query->take($param['count']);
        }


        $total =$query->count();
        $result = $query->groupBy('pro_id')->get();
        // dd($result);
        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_filter_category($param){
        // dd($param);
        $query = DB::table('nm_product')
        ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->where('pro_isapproved', 1)
        ->where('pro_status', 1)
        ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));
        // ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
        // ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
        // ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id');

        if (!empty($param['pro_title'])) {
            $query = $query->where(function($q) use($param){
                $q = $q->where(function($w) use($param){
                    $w = $w->where('mc_name', '=', $param['pro_title'])
                    ->where('mc_status', 1);
                })
                ->orWhere(function($w) use($param){
                    $w = $w->where('smc_name', '=', $param['pro_title'])
                    ->where('smc_status', 1);
                })
                ->orWhere(function($w) use($param){
                    $w = $w->where('sb_name', '=', $param['pro_title'])
                    ->where('sb_status', 1);
                })
                ->orWhere(function($w) use($param){
                    $w = $w->where('ssb_name', '=', $param['pro_title'])
                    ->where('ssb_status', 1);
                });
            });
         }
         if (!empty($param['max_price'])) {
             $query = $query->where(function($q) use($param){
                 $q = $q->where(function($q1) use($param){
                     $q1 = $q1->where('pro_disprice', 0)->where('pro_price', '<=', floatval($param['max_price']));
                 })
                 ->orWhere(function($q1) use($param){
                     $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '<=', floatval($param['max_price']));
                 });
             });
         //   $query = $query->where('pro_price', '<=', floatval($param['max_price']));
         }
         if (!empty($param['min_price'])) {
             $query = $query->where(function($q) use($param){
                 $q = $q->where(function($q1) use($param){
                     $q1 = $q1->where('pro_disprice' , 0)->where('pro_price', '>=', floatval($param['min_price']));
                 })
                 ->orWhere(function($q1) use($param){
                     $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '>=', floatval($param['min_price']));
                 });
             });
         //   $query = $query->where('pro_price', '>=', floatval($param['min_price']));
         }

        if (!empty($param['mc_id'])) {
          $query = $query->where('pro_mc_id', '=', $param['mc_id']);
        }
        if (!empty($param['smc_id'])) {
          $query = $query->where('pro_smc_id', '=', $param['smc_id']);
        }
        if (!empty($param['sb_id'])) {
          $query = $query->where('pro_sb_id', '=', $param['sb_id']);
        }
        if (!empty($param['ssb_id'])) {
          $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
        }
        if (!empty($param['grade'])) {
            $query = $query->where(function($q) use($param){
                $q = $q->where('pro_grade_id', $param['grade'][0]);
                if (!empty($param['grade'][1])) {
                    for ($i=1; $i < count($param['grade']); $i++) {
                        $q = $q->orWhere('pro_grade_id', $param['grade'][$i]);
                    }
                }
            });
        }
        if (!empty($param['spc_value'])) {
            $query = $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
            ->where(function($q) use($param){
                $q = $q->where('spc_value', $param['spc_value'][0]);
                if (!empty($param['spc_value'][1])) {
                    for ($i=1; $i < count($param['spc_value']); $i++) {
                        $q = $q->orWhere('spc_value', $param['spc_value'][$i]);
                    }
                }
            });
        }

        if (!empty($param['skip'])) {
          $query = $query->skip($param['skip']);
        }
        if (!empty($param['count'])) {
          $query = $query->take($param['count']);
        }

        $total =$query->count();
        $result = $query->groupBy('pro_id')->get();
        // dd($result);
        return ['data'=>$result, 'total'=> $total];
      }

      public static function search_grade_filter_category($param)
      {
          $query = DB::table('nm_product')
          ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_isapproved', 1)
          ->where('pro_status', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));
          // ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
          // ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
          // ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id');

          if (!empty($param['pro_title'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where(function($w) use($param){
                      $w = $w->where('mc_name', '=', $param['pro_title'])
                      ->where('mc_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('smc_name', '=', $param['pro_title'])
                      ->where('smc_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('sb_name', '=', $param['pro_title'])
                      ->where('sb_status', 1);
                  })
                  ->orWhere(function($w) use($param){
                      $w = $w->where('ssb_name', '=', $param['pro_title'])
                      ->where('ssb_status', 1);
                  });
              });
           }
           if (!empty($param['max_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', 0)->where('pro_price', '<=', floatval($param['max_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '<=', floatval($param['max_price']));
                   });
               });
           //   $query = $query->where('pro_price', '<=', floatval($param['max_price']));
           }
           if (!empty($param['min_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice' , 0)->where('pro_price', '>=', floatval($param['min_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '>=', floatval($param['min_price']));
                   });
               });
           //   $query = $query->where('pro_price', '>=', floatval($param['min_price']));
           }

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }
          if (!empty($param['grade'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where('pro_grade_id', $param['grade'][0]);
                  if (!empty($param['grade'][1])) {
                      for ($i=1; $i < count($param['grade']); $i++) {
                          $q = $q->orWhere('pro_grade_id', $param['grade'][$i]);
                      }
                  }
              });
          }
          if (!empty($param['spc_value'])) {
              $query = $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
              ->where(function($q) use($param){
                  $q = $q->where('spc_value', $param['spc_value'][0]);
                  if (!empty($param['spc_value'][1])) {
                      for ($i=1; $i < count($param['spc_value']); $i++) {
                          $q = $q->orWhere('spc_value', $param['spc_value'][$i]);
                      }
                  }
              });
          }

          if (!empty($param['skip'])) {
            $query = $query->skip($param['skip']);
          }
          if (!empty($param['count'])) {
            $query = $query->take($param['count']);
          }

          $hasil = $query->select('pro_id', 'pro_grade_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_grade')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('grade_id', $hasil2->pro_grade_id);
              }
          });

            $result = $result->groupBy('grade_id')
            ->havingRaw('count(*) > 0')
            ->select([
              'grade_id',
              'grade_name',
              DB::raw('count(*) as qty')
            ])->get();

          return $result;
      }

      public static function search_grade_filter($param)
      {
          $query = DB::table('nm_product')
          ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
          ->join('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
          ->join('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
          ->join('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
          ->where('pro_isapproved', 1)
          ->where('pro_status', 1)
          ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'));
          // ->join('nm_spgroup', 'nm_spgroup.spg_smc_id', '=', 'nm_product.pro_smc_id')
          // ->join('nm_specification', 'nm_specification.sp_spg_id', '=', 'nm_spgroup.spg_id')
          // ->join('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id');

          if (!empty($param['pro_title'])) {
             $query = $query->where('pro_title', 'like' ,'%'.$param['pro_title'].'%');
           }
           if (!empty($param['max_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', 0)->where('pro_price', '<=', floatval($param['max_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '<=', floatval($param['max_price']));
                   });
               });
           //   $query = $query->where('pro_price', '<=', floatval($param['max_price']));
           }
           if (!empty($param['min_price'])) {
               $query = $query->where(function($q) use($param){
                   $q = $q->where(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice' , 0)->where('pro_price', '>=', floatval($param['min_price']));
                   })
                   ->orWhere(function($q1) use($param){
                       $q1 = $q1->where('pro_disprice', '!=', 0)->where('pro_disprice', '>=', floatval($param['min_price']));
                   });
               });
           //   $query = $query->where('pro_price', '>=', floatval($param['min_price']));
           }

          if (!empty($param['mc_id'])) {
            $query = $query->where('pro_mc_id', '=', $param['mc_id']);
          }
          if (!empty($param['smc_id'])) {
            $query = $query->where('pro_smc_id', '=', $param['smc_id']);
          }
          if (!empty($param['sb_id'])) {
            $query = $query->where('pro_sb_id', '=', $param['sb_id']);
          }
          if (!empty($param['ssb_id'])) {
            $query = $query->where('pro_ssb_id', '=', $param['ssb_id']);
          }
          if (!empty($param['grade'])) {
              $query = $query->where(function($q) use($param){
                  $q = $q->where('pro_grade_id', $param['grade'][0]);
                  if (!empty($param['grade'][1])) {
                      for ($i=1; $i < count($param['grade']); $i++) {
                          $q = $q->orWhere('pro_grade_id', $param['grade'][$i]);
                      }
                  }
              });
          }
          if (!empty($param['spc_value'])) {
              $query = $query->LeftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
              ->where(function($q) use($param){
                  $q = $q->where('spc_value', $param['spc_value'][0]);
                  if (!empty($param['spc_value'][1])) {
                      for ($i=1; $i < count($param['spc_value']); $i++) {
                          $q = $q->orWhere('spc_value', $param['spc_value'][$i]);
                      }
                  }
              });
          }

          if (!empty($param['skip'])) {
            $query = $query->skip($param['skip']);
          }
          if (!empty($param['count'])) {
            $query = $query->take($param['count']);
          }

          $hasil = $query->select('pro_id', 'pro_grade_id')->groupBy('pro_id')->get();

          $result = DB::table('nm_grade')
          ->where(function($q) use($hasil){
              foreach ($hasil as $hasil2) {
                  $q = $q->orWhere('grade_id', $hasil2->pro_grade_id);
              }
          });

            $result = $result->groupBy('grade_id')
            ->havingRaw('count(*) > 0')
            ->select([
              'grade_id',
              'grade_name',
              DB::raw('count(*) as qty')
            ])->get();

          return $result;
      }

    public static function insert_grouping($data)
    {
        return DB::table('nm_groping_product')->insert($data);
    }

    public static function get_detail_ket_group($id_group)
    {
        return DB::table('nm_groping_product')->where('group_id', '=', $id_group)->first();
    }

    public static function search($paramTitle, $paramKategori, $skip, $count, $paramMerchant){
      $query = DB::table('nm_product')
      ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
      ->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
      ->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
      ->LeftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
      ->join('nm_merchant','nm_merchant.mer_id', '=', 'nm_product.pro_mr_id');

      //$get_kuku_merchant = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
      //$kuku_id = $get_kuku_merchant->mer_id;
      $total = $query->count();

      $query = $query ->where('pro_id', '<>', 1)
      ->where('pro_id', '<>', 2)
      ->where('pro_isapproved', 1)
      ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'))
      ->where('mc_status', 1)
      ->where('smc_status', 1)
      ->where('sb_status', 1)
      ->where('ssb_status', 1)
      ->where('pro_status', 1);
      //->where('pro_mr_id', $kuku_id);

      if(!empty($paramTitle)){
        $query = $query->where('pro_title', 'like' ,'%'.$paramTitle.'%');
      }
      if(!empty($paramKategori)){
        $query = $query->where('mc_name', '=' , $paramKategori);
      }
      if(!empty($paramMerchant)){
        $query = $query->where('mer_id', '=' , $paramMerchant);
      }

      $query = $query->skip($skip)->take($count)
      ->get();
      return ['data'=>$query, 'total'=> $total];
    }

    public static function mer_search($paramTitle, $paramKategori, $skip, $count, $merch_id){
      $query = DB::table('nm_product')
       ->join('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id');

      $query = $query->where('pro_mr_id','=',$merch_id)
      ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'))
      ->where('pro_isapproved', 1);
      $total =$query->count();

      if(!empty($paramTitle)){
        $query = $query->where('pro_title', 'like' ,'%'.$paramTitle.'%');
      }
      if(!empty($paramKategori)){
        $query = $query->where('mc_name', '=' , $paramKategori);
      }

      $result = $query
      ->skip($skip)->take($count)
      ->get();
      return ['data'=>$result, 'total'=> $total];
    }


    public static function get_product_by_id($id)
    {
        return DB::table('nm_product')->where('pro_id', '=', $id)->first();
    }

    //edit untuk promo product per id
    public static function get_promo_product_by_id($id)
    {
        return DB::table('nm_product')
        ->join('nm_promo_products', 'nm_promo_products.promop_pro_id','=','nm_product.pro_id')
        ->where('pro_id', '=', $id)
        ->first();
    }

    public static function get_list_group()
    {
        return DB::table('nm_groping_product')->get();
    }

    public static function grouppage_detail($id)
    {
        return DB::table('nm_groping_product')->where('group_id', '=', $id)->get();
    }

    public static function update_grouppage($entry, $id)
    {
        return DB::table('nm_groping_product')->where('group_id', '=', $id)->update($entry);
    }

    public static function get_product_id($sku)
    {
        return DB::table('nm_product')->where('pro_sku', $sku)->first();
    }

    public static function insert_product($entry)
    {
       $check_insert = DB::table('nm_product')->insert($entry);

        if ($check_insert) {
            return DB::getPdo()->lastInsertId();
        } else {
            return 0;
        }

    }

    public static function insert_group_product($data)
    {
        DB::table('nm_groping_product_detail')->insert($data);

        return 0;
    }

    public static function delete_group_product($id_pro)
    {
        return DB::table('nm_groping_product_detail')->where('group_detail_id_product', '=', $id_pro)->delete();
    }

    public static function get_chart_details()
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_order WHERE MONTH( `order_date` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_qtycod_details()
    {
        return DB::table('nm_ordercod')->where('cod_status', '=', 2)->sum('cod_qty');

    }

    public static function get_amtcod_details()
    {
        return DB::table('nm_ordercod')->where('cod_status', '=', 2)->sum('cod_amt');

    }

    public static function get_cod_details()
    {
        return DB::table('nm_ordercod')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_shipping', 'nm_ordercod.cod_id', '=', 'nm_shipping.ship_order_id')->leftjoin('nm_colorfixed', 'nm_ordercod.cod_pro_color', '=', 'nm_colorfixed.cf_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->groupBy('nm_ordercod.cod_transaction_id')->get();
    }

    public static function get_qty_details()
    {
        return DB::table('nm_order')->where('order_status', '=', 2)->sum('order_qty');

    }

    public static function get_amt_details()
    {

      return DB::table('nm_order')->where('order_status', '=', 2)->sum('order_amt');

    }

    public static function get_shipping_details()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_shipping', 'nm_order.order_id', '=', 'nm_shipping.ship_order_id')->leftjoin('nm_colorfixed', 'nm_order.order_pro_color', '=', 'nm_colorfixed.cf_id')->leftjoin('nm_size', 'nm_order.order_pro_size', '=', 'nm_size.si_id')->groupBy('nm_order.transaction_id')->get();
    }

    public static function insert_product_color_details($entry)
    {
      return DB::table('nm_procolor')->insert($entry);
    }

    public static function insert_product_specification_details($entry)
    {
       return DB::table('nm_prospec')->insert($entry);
    }

    public static function insert_product_size_details($productsizeentry)
    {
      return DB::table('nm_prosize')->insert($productsizeentry);
    }

    public static function get_product($id)
    {
        return DB::table('nm_product')
          ->leftjoin('nm_color','nm_color.co_id','=','nm_product.pro_color_id')
          ->leftjoin('nm_grade','nm_grade.grade_id','=','nm_product.pro_grade_id')
          ->where('pro_id', '=', $id)
          ->get();
    }

    public static function get_product_specification()
    {
        return DB::table('nm_specification')->get();
    }

    public static function get_product_color()
    {
        return DB::table('nm_color')->get();

    }

    public static function get_product_size()
    {
        return DB::table('nm_size')->get();

    }

    public static function get_sizename_ajax($sizeid)
    {
        return DB::table('nm_size')->where('si_id', '=', $sizeid)->get();

    }

    public static function get_product_category()
    {
        return DB::table('nm_maincategory')->where('mc_status', '=', 1)->get();
    }

    public static function get_product_merchant()
    {
      return DB::table('nm_merchant')->get();
    }

    public static function load_maincategory_ajax($id)
    {
        return DB::table('nm_secmaincategory')->where('smc_mc_id', '=', $id)->where('smc_status', '=', 1)->get();
    }

    public static function load_subcategory_ajax($id)
    {

        return DB::table('nm_subcategory')->where('sb_smc_id', '=', $id)->where('sb_status', '=', 1)->get();
    }

    public static function get_second_sub_category_ajax($id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_sb_id', '=', $id)->where('ssb_status', '=', 1)->get();
    }

    public static function get_colorname_ajax($colorid)
    {
        return DB::table('nm_color')->where('co_id', '=', $colorid)->get();
    }

    public static function get_main_category_ajax_edit($id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->get();
    }

    public static function get_sub_category_ajax_edit($id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->get();
    }

    public static function get_second_sub_category_ajax_edit($id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->get();
    }
    // hard code jasa pengiriman, diskon
    public static function get_product_details()
    {
        return DB::table('nm_product')
        ->where('pro_title' ,'!=', 'Jasa Pengiriman')
        ->where('pro_title', '!=', 'Biaya Pengiriman')
        ->where('pro_title' ,'!=', 'Diskon')
        ->get();
    }

    public static function block_product_status($id, $status)
    {
        return DB::table('nm_product')->where('pro_id', '=', $id)->update($status);
    }

    public static function get_product_view($id)
    {
        $query = DB::table('nm_product')->where('pro_id', $id);
        $return = DB::table('nm_product')->where('pro_id', '=', $id)->first();
        $selectmaincategory = DB::table('nm_maincategory')->where('mc_id', $return->pro_mc_id)->first();
        $selectsecmaincategory = DB::table('nm_secmaincategory')->where('smc_id', $return->pro_smc_id)->first();
        $selectsubcategory = DB::table('nm_subcategory')->where('sb_id', $return->pro_sb_id)->first();
        $selectsecsubcategory = DB::table('nm_secsubcategory')->where('ssb_id', $return->pro_ssb_id)->first();
        $selectgrade = DB::table('nm_grade')->where('grade_id', $return->pro_grade_id)->first();
        $selectcolor = DB::table('nm_color')->where('co_id', $return->pro_color_id)->first();

        if($selectmaincategory){
            $query = $query->join('nm_maincategory', 'nm_product.pro_mc_id', '=', 'nm_maincategory.mc_id');
        }
        if($selectsecmaincategory){
            $query = $query->join('nm_secmaincategory', 'nm_product.pro_smc_id', '=', 'nm_secmaincategory.smc_id');
        }
        if($selectsubcategory){
            $query = $query->join('nm_subcategory', 'nm_product.pro_sb_id', '=', 'nm_subcategory.sb_id');
        }
        if($selectsecsubcategory){
            $query = $query->join('nm_secsubcategory', 'nm_product.pro_ssb_id', '=', 'nm_secsubcategory.ssb_id');
        }
        if($selectgrade){
            $query = $query->join('nm_grade', 'nm_product.pro_grade_id', '=', 'nm_grade.grade_id');
        }
        if($selectcolor){
            $query = $query->join('nm_color', 'nm_color.co_id', '=', 'nm_product.pro_color_id');
        }
        $query = $query->leftjoin('nm_merchant','nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')->get();
        return $query;
    }

    public static function delete_product_color($proid)
    {
        return DB::table('nm_procolor')->where('pc_pro_id', '=', $proid)->delete();
    }

    public static function delete_product_size($proid)
    {
       return DB::table('nm_prosize')->where('ps_pro_id', '=', $proid)->delete();
    }

    public static function delete_master_group($m_id)
    {
       return DB::table('nm_groping_product')->where('group_id', '=', $m_id)->delete();
    }

    public static function delete_product_spec($proid)
    {
        return DB::table('nm_prospec')->where('spc_pro_id', '=', $proid)->delete();
    }

    public static function get_product_exist_specification($id)
    {
        return DB::table('nm_prospec')->where('spc_pro_id', '=', $id)->get();
    }

    public static function get_product_exist_color($id)
    {
        return DB::table('nm_procolor')->join('nm_color', 'nm_procolor.pc_co_id', '=', 'nm_color.co_id')->where('pc_pro_id', '=', $id)->get();
    }

    public static function get_product_exist_size($id)
    {
        return DB::table('nm_prosize')->join('nm_size', 'nm_prosize.ps_si_id', '=', 'nm_size.si_id')->where('ps_pro_id', '=', $id)->get();
    }

    public static function get_product_details_manage()
    {
        return DB::table('nm_product')
        ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
        ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
        ->leftjoin('nm_color','nm_color.co_id', '=','nm_product.pro_color_id')
        ->leftjoin('nm_grade','nm_grade.grade_id','=','nm_product.pro_grade_id')
        ->where('nm_product.pro_title' ,'!=','Jasa Pengiriman')
        ->where('nm_product.pro_title' ,'!=','Diskon')
        ->where('nm_product.pro_isapproved', '=', 1)
        ->orderBy('nm_product.pro_id','DESC')
        ->get();
    }

    public static function get_product_model_header($id_header)
    {
      return DB::table('nm_product')
        ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
        ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
        ->Leftjoin('nm_grouping_product_model_detail', 'nm_grouping_product_model_detail.id_product', '=', 'nm_product.pro_id')
        ->Leftjoin('nm_grouping_product_model_header', 'nm_grouping_product_model_header.id', '=', 'nm_grouping_product_model_detail.id_grouping_product_model_header')
        ->where('nm_grouping_product_model_header', $id)->get();
    }

    public static function get_product_details_manage_spec_grop($id)
    {
        return DB::table('nm_product')
        ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
        ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')

        ->Leftjoin('nm_groping_product_detail', 'nm_groping_product_detail.group_detail_id_product', '=', 'nm_product.pro_id')

        ->Leftjoin('nm_groping_product', 'nm_groping_product.group_id', '=', 'nm_groping_product_detail.group_detail_id_grouping')

        ->where('group_detail_id_grouping', $id)
        ->orderBy('group_detail_sort', 'asc')
        ->get();
    }

    public static function get_product_details_manage_spec_grop2($id)
    {
        return DB::table('nm_grouping_product_model_detail')
        ->where('nm_grouping_product_model_detail.id_grouping_product_model_header', '=', $id)
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_grouping_product_model_detail.id_product')
        ->leftjoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->leftjoin('nm_city', 'nm_city.ci_id', '=', 'nm_store.stor_city')
        ->leftjoin('nm_color','nm_color.co_id', '=','nm_product.pro_color_id')
        ->leftjoin('nm_grade','nm_grade.grade_id','=','nm_product.pro_grade_id')
        ->get();
    }

    public static function edit_product($entry, $id)
    {
        return DB::table('nm_product')->where('pro_id', '=', $id)->update($entry);
    }

    public static function get_merchant_details()
    {
        return DB::table('nm_merchant')->get();
    }

    public static function get_product_details_formerchant($merid)
    {
        return DB::table('nm_store')->where('stor_merchant_id', '=', $merid)->where('stor_status', '=', 1)->get();
    }

    public static function get_active_products()
    {
        return DB::table('nm_product')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('mer_fname', 'Kukuruyuk')
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', '=', 1)
        ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'))
        ->count();
    }

    public static function get_active_products_merchant()
    {
        return DB::table('nm_product')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', '=', 1)
        ->where('pro_qty', '>', DB::raw('pro_no_of_purchase'))
        ->count();
    }

    public static function get_sold_products()
    {
        return DB::table('nm_product')->get();
    }

    public static function get_sold_products_count()
    {
        return DB::table('nm_product')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('mer_fname', 'Kukuruyuk')
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', '=', 1)
        ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
        ->count();
    }

    public static function get_sold_products_merchant_count()
    {
        return DB::table('nm_product')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->where('pro_status', '=', 1)
        ->where('pro_isapproved', '=', 1)
        ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
        ->count();
    }

    public static function get_block_products()
    {
        return DB::table('nm_product')->where('pro_status', '=', 0)->count();
    }

    public static function get_today_product()
    {
        // return DB::select(DB::raw("SELECT count(*) as count,sum(order_amt) as amt  from nm_order where DATEDIFF(DATE(order_date),DATE(NOW()))=0 and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('+1 day');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_first, $date_last])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_first, $date_last])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_7days_product()
    {
        // return DB::select(DB::raw("select count(*) as count,sum(order_amt) as amt from nm_order WHERE (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('-7 days');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_30days_product()
    {
        // return DB::select(DB::raw("select  count(*) as count,sum(order_amt) as amt from nm_order WHERE (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)) and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('-30 days');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_12mnth_product()
    {
        // return DB::select(DB::raw("select count(*) as count,sum(order_amt) as amt from nm_order where order_date >= DATE_SUB(CURDATE(), INTERVAL 12 MONTH) and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('-12 months');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_today_product_merchant()
    {
        // return DB::select(DB::raw("SELECT count(*) as count,sum(order_amt) as amt  from nm_order where DATEDIFF(DATE(order_date),DATE(NOW()))=0 and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('+1 day');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_first, $date_last])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_first, $date_last])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_7days_product_merchant()
    {
        // return DB::select(DB::raw("select count(*) as count,sum(order_amt) as amt from nm_order WHERE (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)) and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('-7 days');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_30days_product_merchant()
    {
        // return DB::select(DB::raw("select  count(*) as count,sum(order_amt) as amt from nm_order WHERE (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)) and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('-30 days');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_12mnth_product_merchant()
    {
        // return DB::select(DB::raw("select count(*) as count,sum(order_amt) as amt from nm_order where order_date >= DATE_SUB(CURDATE(), INTERVAL 12 MONTH) and order_status=1"));
        date_default_timezone_set('Asia/Jakarta');
        $date_first = date('Y-m-d 00:00:00');
        $date_last = strtotime('-12 months');
        $date_last = date('Y-m-d 00:00:00', $date_last);
        $count = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->count();
        $sum = DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->whereBetween('nm_order.order_date', [$date_last, $date_first])
        ->where('order_status', 1)
        ->where('order_pro_id', '!=', 1)
        ->where('order_pro_id', '!=', 2)
        ->where('mer_fname', '!=', 'Kukuruyuk')
        ->sum('order_amt');
        $data = array(
            'count' => $count,
            'amt' => $sum
        );

        return $data;
    }

    public static function get_zipcode()
    {
        return DB::table('nm_estimate_zipcode')->get();
    }

    public static function save_zip_code($entry)
    {
        return DB::table('nm_estimate_zipcode')->insert($entry);
    }

    public static function check_zip_code($from)
    {
        return $get_result_code = DB::table('nm_estimate_zipcode')->where('ez_code_series', '<=', $from)->where('ez_code_series_end', '>=', $from)->get();
    }

    public static function check_zip_code_range($from, $to)
    {
        return $get_result_code = DB::table('nm_estimate_zipcode')->where('ez_code_series', '>=', $from)->where('ez_code_series_end', '<=', $to)->get();

    }

    public static function edit_zip_code($id)
    {
        return DB::table('nm_estimate_zipcode')->where('ez_id', '=', $id)->get();
    }

    public static function update_zip_code($entry, $id)
    {
        return DB::table('nm_estimate_zipcode')->where('ez_id', '=', $id)->update($entry);
    }

    public static function check_zip_code_edit($id, $from)
    {
        return DB::table('nm_estimate_zipcode')->where('ez_code_series', '<=', $from)->where('ez_code_series_end', '>=', $from)->where('ez_id', '!=', $id)->get();

    }

    public static function check_zip_code_edit_range($id, $from, $to)
    {
        return $get_result_code = DB::table('nm_estimate_zipcode')->where('ez_code_series', '>=', $from)->where('ez_code_series_end', '<=', $to)->where('ez_id', '!=', $id)->get();

    }

    public static function block_zip_code($id, $status)
    {
        return DB::table('nm_estimate_zipcode')->where('ez_id', '=', $id)->update(array(
            'ez_status' => $status
        ));
    }

    public static function get_induvidual_product_detail_merchant($id, $merid)
    {

        return DB::table('nm_product')->where('pro_mr_id', '=', $merid)->where('pro_id', '=', $id)->get();
    }

    public static function get_productreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_product')
            ->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            ->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')
            ->where('nm_product.created_date', $from_date)
            ->where('nm_product.pro_status', '=', 1)->orderBy('nm_product.pro_id', 'DESC')
            ->where('nm_product.pro_title', '!=', 'Jasa Pengiriman')
            ->where('nm_product.pro_title', '!=', 'Diskon')
            ->orderBy('nm_product.pro_id', 'DESC')
            ->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_product')->Leftjoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')->Leftjoin('nm_city', 'nm_store.stor_city', '=', 'nm_city.ci_id')->whereBetween('nm_product.created_date', array(
                $from_date,
                $to_date
            ))->where('nm_product.pro_status', '=', 1)->orderBy('nm_product.pro_id', 'DESC')->get();
        } else {

        }

    }
    // hardcode
    public static function get_soldrep($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_product')
            ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
            ->where('pro_status', 1)
            ->where('pro_isapproved', 1)
            ->where('pro_title', '!=', 'Biaya Pengiriman')
            ->where('pro_title', '!=', 'Jasa Pengiriman')
            ->where('pro_title', '!=', 'Diskon')
            ->where('activated_date', '>=', $from_date)
            ->orderBy('pro_id', 'DESC')
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_product')
            ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
            ->where('pro_status', 1)
            ->where('pro_isapproved', 1)
            ->where('pro_title', '!=', 'Biaya Pengiriman')
            ->where('pro_title', '!=', 'Jasa Pengiriman')
            ->where('pro_title', '!=', 'Diskon')
            ->where('activated_date', '<=', $to_date)
            ->orderBy('pro_id', 'DESC')
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_product')
            ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
            ->where('pro_status', 1)
            ->where('pro_isapproved', 1)
            ->where('pro_title', '!=', 'Biaya Pengiriman')
            ->where('pro_title', '!=', 'Jasa Pengiriman')
            ->where('pro_title', '!=', 'Diskon')
            ->whereBetween('activated_date', array($from_date, $to_date))
            ->orderBy('pro_id', 'DESC')
            ->get();
        }
        else {
            return DB::table('nm_product')
            ->where('pro_qty', '<=', DB::raw('pro_no_of_purchase'))
            ->where('pro_status', 1)
            ->where('pro_isapproved', 1)
            ->where('pro_title', '!=', 'Biaya Pengiriman')
            ->where('pro_title', '!=', 'Jasa Pengiriman')
            ->where('pro_title', '!=', 'Diskon')
            ->orderBy('pro_id', 'DESC')
            ->get();
        }
    }

    public static function check_store($Product_Title, $Select_Shop)
    {
        return DB::table('nm_product')->where('pro_title', '=', $Product_Title)->where('pro_sh_id', '=', $Select_Shop)->get();
    }

	public static function get_order_details()
	{
		return DB::table('nm_order')->where('order_type', '=', 1)->get();
	}

	// public static function delete_product($id)
	// {
	//     return DB::table('nm_product')->where('pro_id', '=', $id)->delete();
	// }

    public static function delete_product($id)
    {

        // To start Image delete from folder 09/11/
        $filename = DB::table('nm_product')->where('pro_id', $id)->first();
        $getimagename= $filename->pro_Img;
        $getextension=explode("/**/",$getimagename);
        foreach($getextension as $imgremove)
        {
            File::delete(base_path('assets/product/').$imgremove);
        }
        // To End
        return DB::table('nm_product')->where('pro_id', '=', $id)->delete();

    }
    //Product Review manage
    public static function change_read_status()
    {
        return DB::table('nm_review')->where('sam1', 0)->update(['sam1'=>1]);
    }
    public static function get_product_review()
    {
        return DB::table('nm_review')->Leftjoin('nm_product','nm_review.product_id','=','nm_product.pro_id')->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')->where('nm_review.product_id','!=','NULL')->get();
    }
    public static function edit_review($id)
    {
        return DB::table('nm_review')->where('comment_id', '=', $id)->get();
    }
    public static function update_review($entry, $id)
    {
        return DB::table('nm_review')->where('comment_id', '=', $id)->update($entry);
    }
    public static function delete_review($id)
    {
        return DB::table('nm_review')->where('comment_id', '=', $id)->delete();
    }
    public static function block_review_status($id, $status)
    {
        return DB::table('nm_review')->where('comment_id', '=', $id)->update($status);
    }
    public static function update_warranty_db($id, $warranty)
    {
      return DB::table('nm_product')->where('nm_product.pro_id', '=', $id)->update(array('have_warranty' => $warranty));
    }
    public static function get_spgroup_with_smc($smc_id)
    {
        return DB::table('nm_spgroup')->where('spg_smc_id', $smc_id)->first();
    }
    public static function get_prospec($pro_id, $sp_id)
    {
        return DB::table('nm_prospec')->where('spc_pro_id', $pro_id)->where('spc_sp_id', $sp_id)->first();
    }
    public static function update_prospec($entry)
    {
        return DB::table('nm_prospec')->where('spc_pro_id', $entry['spc_pro_id'])->where('spc_sp_id', $entry['spc_sp_id'])->update(['spc_value'=>$entry['spc_value']]);
    }

    public static function download_product($param)
    {
        $get_kuku = DB::table('nm_merchant')->where('mer_fname', 'Kukuruyuk')->first();
        $kuku_id = $get_kuku->mer_id;
        $get = DB::table('nm_product')->where('pro_id', '!=', 1)->where('pro_id', '!=', 2);

        if($param['merchant'] == 'kukuruyuk'){
            $get = $get->where('pro_mr_id', $kuku_id);
        }elseif ($param['merchant'] != '') {
            $get = $get->where('pro_mr_id', $param['merchant']);
        }
        if($param['mc'] != ''){
            $get = $get->where('pro_mc_id', $param['mc']);
        }
        if($param['smc'] != ''){
            $get = $get->where('pro_smc_id', $param['smc']);
        }
        if($param['sb'] != ''){
            $get = $get->where('pro_sb_id', $param['sb']);
        }
        if($param['extended_warranty'] != ''){
            $get = $get->where('have_warranty', $param['extended_warranty']);
        }
        if($param['stock_ready'] == 1){
            $get = $get->whereRaw('pro_qty-pro_no_of_purchase = 0');
        }elseif ($param['stock_ready'] == 2) {
            $get = $get->whereRaw('pro_qty-pro_no_of_purchase >= 1')->whereRaw('pro_qty-pro_no_of_purchase <= 10');
        }elseif ($param['stock_ready'] == 3) {
            $get = $get->whereRaw('pro_qty-pro_no_of_purchase >= 11')->whereRaw('pro_qty-pro_no_of_purchase <= 30');
        }elseif ($param['stock_ready'] == 4) {
            $get = $get->whereRaw('pro_qty-pro_no_of_purchase > 30');
        }
        if($param['qty'] == 1){
            $get = $get->whereRaw('pro_qty = 0');
        }elseif ($param['qty'] == 2) {
            $get = $get->whereRaw('pro_qty >= 1')->whereRaw('pro_qty <= 10');
        }elseif ($param['qty'] == 3) {
            $get = $get->whereRaw('pro_qty >= 11')->whereRaw('pro_qty <= 30');
        }elseif ($param['qty'] == 4) {
            $get = $get->whereRaw('pro_qty > 30');
        }
        // dd($get);
        $query = $get->get();
        $spec = $get->leftJoin('nm_prospec', 'nm_prospec.spc_pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_specification', 'nm_specification.sp_id', '=', 'nm_prospec.spc_sp_id')
        ->leftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_product.pro_mc_id')
        ->leftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_product.pro_smc_id')
        ->leftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_product.pro_sb_id')
        ->leftJoin('nm_secsubcategory', 'nm_secsubcategory.ssb_id', '=', 'nm_product.pro_ssb_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')->get();

        $return = array($spec, $query);
        return $spec;
    }
}

?>
