<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class UserManagement extends Model
{
    protected $guarded = array('adm_id');
    protected $table = 'nm_admin';

    public static function add_user($entry)
    {
        return DB::table('nm_admin')->insert($entry);
    }
    public static function update_user($entry, $id)
    {
        return DB::table('nm_admin')->where('adm_id', $id)->update($entry);
    }
    public static function delete_user($id)
    {
        return DB::table('nm_admin')->where('adm_id', $id)->delete();
    }
}
?>
