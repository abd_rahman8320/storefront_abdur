<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class City extends Model
{
    protected $guarded = array('id');
    protected $primaryKey = 'ci_id';
    protected $table = 'nm_city';
    public $timestamps = false;

    //kupon
    public static function getKupon($no_kupon)
    {
        return DB::table('nm_promo_coupons')
        ->leftJoin('nm_scheduled_promo', 'nm_scheduled_promo.schedp_id', '=', 'nm_promo_coupons.promoc_schedp_id')
        ->leftJoin('nm_postal_services', 'nm_postal_services.ps_id', '=', 'nm_scheduled_promo.schedp_ps_id')
        ->where('promoc_voucher_code', $no_kupon)
        ->where('nm_scheduled_promo.schedp_status','=',1)
        ->first();
    }

    //postal
    public static function view_postal_service_detail()
    {
        return DB::table('nm_postal_services')->get();
    }

    //postal
    public static function get_zipcode_shipping_item($sku)
    {
        return DB::table('nm_product')
        ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->leftJoin('nm_city', 'nm_city.ci_id', '=', 'nm_store.stor_city')
        ->where('pro_id', $sku)->first();
    }

    public static function get_city($code)
    {
        return DB::table('nm_city')->where('ci_code', $code)->first();
    }

    public static function get_country($code)
    {
        return DB::table('nm_country')->where('co_code', $code)->first();
    }

    public static function view_city_detail()
    {
        return DB::table('nm_city')->leftJoin('nm_province', 'nm_province.prov_id', '=', 'nm_city.ci_prov_id')->get();
    }

    public static function view_country_details()
    {
        return DB::table('nm_country')->get();
    }

    public static function show_city_detail($id)
    {
        return DB::table('nm_city')->where('ci_id', '=', $id)->get();
    }

    public static function delete_city_detail($id)
    {
        return DB::table('nm_city')->where('ci_id', '=', $id)->delete();
    }

    public static function update_city_detail($id, $entry)
    {
        return DB::table('nm_city')->where('ci_id', '=', $id)->update($entry);
    }

    public static function save_city_detail($entry)
    {
        return DB::table('nm_city')->insert($entry);
    }

    public static function status_city($id, $status)
    {
        return DB::table('nm_city')->where('ci_id', '=', $id)->update(array('ci_status' => $status));

    }

    public static function check_exist_city_name($name, $ccode)
    {
        return DB::table('nm_city')->where('ci_prov_id', '=', $ccode)->where('ci_name', '=', $name)->get();

    }

    public static function update_default_city_submit($id, $entry)
    {
        return DB::table('nm_city')->where('ci_id', '=', $id)->update($entry);

    }

    public static function update_default_city_submit1($entry)
    {
        return DB::table('nm_city')->update($entry);

    }
}

?>
