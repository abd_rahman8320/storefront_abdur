<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class UsersRoles extends Model
{
    use SoftDeletes;
    
    protected $table = 'nm_user_roles';
    protected $primaryKey = 'ur_id';
    const CREATED_AT = 'created_ts';
    const UPDATED_AT = 'lastupdated_ts';
    const DELETED_AT = 'deleted_ts';

    protected $fillable = [
        'ur_user_name',
        'ur_roles_name',
        'created_by',
        'lastupdated_by',
        'deleted_by',
        'is_deleted'
    ];

    protected $dates = ['deleted_ts'];

    public function users()
    {
        return $this->belongsTo('App\Users');
    }

    public function roles()
    {
        return $this->belongsTo('App\Roles');
    }
}
?>
