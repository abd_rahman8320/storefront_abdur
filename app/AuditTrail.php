<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class AuditTrail extends Model
{
    protected $table = 'nm_audittrail';
    protected $primaryKey = 'aud_id';
    public $timestamps = false;

    protected $fillable = [
        'aud_table',
        'aud_action',
        'aud_detail',
        'aud_date',
        'aud_user_name'
    ];
}
?>
