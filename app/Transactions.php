<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Transactions extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_order_auction';

    public static function get_invoice_fund_request_detail($wd_id)
    {
        $get = DB::table('nm_withdraw_request')
        ->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_withdraw_request.wd_mer_id')
        ->LeftJoin('nm_withdraw_request_detail', 'nm_withdraw_request_detail.wdd_wd_id', '=', 'nm_withdraw_request.wd_id')
        ->LeftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_withdraw_request_detail.wdd_pro_id')
        ->select('wd_id', 'wd_date', 'wd_faktur_pajak', 'mer_fname', 'mer_lname', 'mer_commission', 'wdd_transaction_id', 'wdd_order_qty', 'wdd_order_amt', 'wdd_order_shipping_price', 'pro_title')
        ->where('wd_id', $wd_id)
        ->get();

        return $get;
    }
    public static function ubah_number_of_purc($orderid, $number_purc)
    {
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->where('nm_order.order_id','=', $orderid)
        ->update(array(
            'pro_no_of_purchase' => $number_purc
            ));
    }

    public static function get_transaction_detail_ex($id)
    {
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=', 'nm_order.order_pro_id')
        ->where('nm_order.transaction_id','=', $id)
        ->where('nm_product.pro_title','!=','Biaya Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->get();
    }

    public static function setujui_columbia($id_transaksi)
    {
        return DB::table('nm_transaksi')
        ->where('nm_transaksi.transaction_id','=',$id_transaksi)
        ->update(array(
            'approval_columbia' => 1
            ));
    }

    public static function tolak_columbia($id_transaksi)
    {
        return DB::table('nm_transaksi')
        ->where('nm_transaksi.transaction_id','=', $id_transaksi)
        ->update(array(
            'approval_columbia' => 0,
            'status_pembayaran' => 'expired'
            ));
    }

    public static function change_jumlah_poin($jumlah_poin, $cus_id)
    {
        return DB::table('nm_customer')
        ->where('nm_customer.cus_id','=', $cus_id)
        ->update(array(
            'total_poin' => $jumlah_poin
            ));
    }

    public static function update_history_per_order($id_transaksi, $tgl_update)
    {
        return DB::table('nm_order')
        ->where('nm_order.transaction_id', '=', $id_transaksi)
        ->update(array(
            'order_tgl_pesanan_dibayarkan' => $tgl_update
            ));
    }

    public static function get_cus_data_by_trans_id($id_transaksi)
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order','nm_order.transaction_id','=', 'nm_transaksi.transaction_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=', 'nm_order.order_cus_id')
        ->leftjoin('nm_shipping','nm_shipping.ship_order_id','=','nm_order.order_id')
        ->where('nm_transaksi.transaction_id','=', $id_transaksi)
        ->get();
    }

    public static function get_data_order_by_trans_id($trans_id){
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftjoin('nm_shipping','nm_shipping.ship_order_id','=','nm_order.order_id')
        ->where('transaction_id','=',$trans_id)
        ->get();
    }


    //pick pack shipping foreach items order
    public static function for_data_order_by_trans_id($trans_id){
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftjoin('nm_shipping','nm_shipping.ship_order_id','=','nm_order.order_id')
        ->where('transaction_id','=',$trans_id)
        ->get();
    }


    //pick pack shipping
    public static function get_data_order_kukuruyuk_by_trans_id($trans_id){
        return DB::table('nm_order')
        ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('transaction_id', $trans_id)
        ->where('mer_fname', 'Kukuruyuk')
        ->get();
    }
    public static function get_cus_data_order_by_order_id($order_id){
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftjoin('nm_shipping','nm_shipping.ship_order_id','=','nm_order.order_id')
        ->where('order_id','=',$order_id)
        ->first();
    }

    //pick pack shipping items order
    public static function get_data_by_order_id($order_id){
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('order_id','=',$order_id)
        ->get();
    }

    //ga
    public static function get_cus_data_by_order_id($orderid)
    {
        return DB::table('nm_order')
        ->leftjoin('nm_customer','nm_customer.cus_id','=', 'nm_order.order_cus_id')
        ->where('nm_order.order_id', '=', $orderid)
        ->get();
    }

    public static function get_data_mer_by_order_id($orderid){
        return DB::table('nm_order')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->leftjoin('nm_merchant','nm_merchant.mer_id','=','nm_product.pro_mr_id')
        ->leftjoin('nm_transaksi','nm_transaksi.transaction_id','=','nm_order.transaction_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('nm_order.order_id','=',$orderid)
        ->first();
    }

    public static function get_data_mer_by_trans_id($trans_id){
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_merchant','nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->first();
    }

    public static function update_status_outbound_transaksi_db($id_transaksi, $status_outbound)
    {
        return DB::table('nm_transaksi')->where('nm_transaksi.transaction_id', '=', $id_transaksi)->update(array(
            'status_outbound_transaksi' => $status_outbound
        ));
    }

    public static function insert_outbound_transaksi($data)
    {
        return DB::table('nm_history_outbound')->insert($data);
    }

    public static function merchant_all_transaction_db()
    {
        return DB::table('nm_order')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftjoin('nm_shipping', 'nm_shipping.ship_order_id', '=', 'nm_order.order_id')
        ->leftjoin('nm_country', 'nm_country.co_id', '=', 'nm_shipping.ship_country')
        ->leftjoin('nm_city', 'nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
        ->leftjoin('nm_districts', 'nm_districts.dis_id', '=', 'nm_shipping.ship_dis_id')
        ->leftjoin('nm_subdistricts', 'nm_subdistricts.sdis_id', '=', 'nm_shipping.ship_sdis_id')
        ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
        ->select('co_name', 'ci_name', 'dis_name','sdis_name', 'nm_order.transaction_id', 'order_id', 'order_amt', 'order_date', 'nm_order.order_status', 'postalservice_code', 'status_outbound', 'order_tgl_konfirmasi_penerimaan_barang', 'pro_title', 'mer_fname', 'mer_lname',
        'status_pembayaran', 'status_outbound_transaksi', 'mer_phone', 'mer_email', 'nm_shipping.*')
        ->where('nm_product.pro_mr_id','!=','0')
        ->where('nm_transaksi.status_pembayaran', '=', 'sudah dibayar')
        ->orderBy('order_id', 'desc')
        ->get();
    }

    public static function merchant_all_transaction_bydate($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_order')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_shipping.ship_order_id', '=', 'nm_order.order_id')
            ->leftjoin('nm_country', 'nm_country.co_id', '=', 'nm_shipping.ship_country')
            ->leftjoin('nm_city', 'nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
            ->leftjoin('nm_districts', 'nm_districts.dis_id', '=', 'nm_shipping.ship_dis_id')
            ->leftjoin('nm_subdistricts', 'nm_subdistricts.sdis_id', '=', 'nm_shipping.ship_sdis_id')
            ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
            ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
            ->select('nm_country.co_name', 'nm_city.ci_name', 'nm_districts.dis_name', 'nm_subdistricts.sdis_name', 'nm_order.*',
            'nm_product.*', 'nm_merchant.mer_id', 'nm_merchant.mer_fname', 'nm_merchant.mer_lname', 'nm_merchant.mer_phone',
            'nm_merchant.mer_email', 'nm_transaksi.*', 'nm_shipping.*')
            ->where('nm_product.pro_mr_id','!=','0')
            ->where('nm_transaksi.status_pembayaran', '=', 'sudah dibayar')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_order')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_shipping.ship_order_id', '=', 'nm_order.order_id')
            ->leftjoin('nm_country', 'nm_country.co_id', '=', 'nm_shipping.ship_country')
            ->leftjoin('nm_city', 'nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
            ->leftjoin('nm_districts', 'nm_districts.dis_id', '=', 'nm_shipping.ship_dis_id')
            ->leftjoin('nm_subdistricts', 'nm_subdistricts.sdis_id', '=', 'nm_shipping.ship_sdis_id')
            ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
            ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
            ->select('nm_country.co_name', 'nm_city.ci_name', 'nm_districts.dis_name', 'nm_subdistricts.sdis_name', 'nm_order.*',
            'nm_product.*', 'nm_merchant.mer_id', 'nm_merchant.mer_fname', 'nm_merchant.mer_lname', 'nm_merchant.mer_phone',
            'nm_merchant.mer_email', 'nm_transaksi.*', 'nm_shipping.*')
            ->where('nm_product.pro_mr_id','!=','0')
            ->where('nm_transaksi.status_pembayaran', '=', 'sudah dibayar')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_order')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_shipping.ship_order_id', '=', 'nm_order.order_id')
            ->leftjoin('nm_country', 'nm_country.co_id', '=', 'nm_shipping.ship_country')
            ->leftjoin('nm_city', 'nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
            ->leftjoin('nm_districts', 'nm_districts.dis_id', '=', 'nm_shipping.ship_dis_id')
            ->leftjoin('nm_subdistricts', 'nm_subdistricts.sdis_id', '=', 'nm_shipping.ship_sdis_id')
            ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
            ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
            ->select('nm_country.co_name', 'nm_city.ci_name', 'nm_districts.dis_name', 'nm_subdistricts.sdis_name', 'nm_order.*',
            'nm_product.*', 'nm_merchant.mer_id', 'nm_merchant.mer_fname', 'nm_merchant.mer_lname', 'nm_merchant.mer_phone',
            'nm_merchant.mer_email', 'nm_transaksi.*', 'nm_shipping.*')
            ->where('nm_product.pro_mr_id','!=','0')
            ->where('nm_transaksi.status_pembayaran', '=', 'sudah dibayar')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_order')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_shipping.ship_order_id', '=', 'nm_order.order_id')
            ->leftjoin('nm_country', 'nm_country.co_id', '=', 'nm_shipping.ship_country')
            ->leftjoin('nm_city', 'nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
            ->leftjoin('nm_districts', 'nm_districts.dis_id', '=', 'nm_shipping.ship_dis_id')
            ->leftjoin('nm_subdistricts', 'nm_subdistricts.sdis_id', '=', 'nm_shipping.ship_sdis_id')
            ->leftjoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
            ->leftjoin('nm_transaksi', 'nm_transaksi.transaction_id', '=', 'nm_order.transaction_id')
            ->select('nm_country.co_name', 'nm_city.ci_name', 'nm_districts.dis_name', 'nm_subdistricts.sdis_name', 'nm_order.*',
            'nm_product.*', 'nm_merchant.mer_id', 'nm_merchant.mer_fname', 'nm_merchant.mer_lname', 'nm_merchant.mer_phone',
            'nm_merchant.mer_email', 'nm_transaksi.*', 'nm_shipping.*')
            ->where('nm_product.pro_mr_id','!=','0')
            ->where('nm_transaksi.status_pembayaran', '=', 'sudah dibayar')
            ->get();
        }
    }

    public static function manage_auction_bidder()
    {
        return DB::table('nm_order_auction')->LeftJoin('nm_auction', 'nm_auction.auc_id', '=', 'nm_order_auction.oa_pro_id')->get();
    }

    public static function auction_by_bidder()
    {
        return DB::table('nm_auction')->get();
    }

    public static function auction_by_bidder_count()
    {
        $auction_det = DB::table('nm_auction')->get();

        foreach ($auction_det as $auction) {
            $catg_result = DB::table('nm_order_auction')->where('oa_pro_id', '=', $auction->auc_id)->get();
            if ($catg_result) {
                $result[$auction->auc_id] = count($catg_result);
            } else {
                $result[$auction->auc_id] = 0;
            }
        }
        return $result;
    }

    public static function auction_by_bidder_amt_count()
    {
        $auction_det = DB::table('nm_auction')->get();

        foreach ($auction_det as $auction) {
            $catg_result = DB::table('nm_order_auction')->where('oa_pro_id', '=', $auction->auc_id)->sum('oa_bid_amt');
            if ($catg_result) {
                $result[$auction->auc_id] = $catg_result;
            } else {
                $result[$auction->auc_id] = 0;
            }
        }
        return $result;
    }

    public static function list_auction_bidders($id)
    {
        return DB::table('nm_order_auction')->where('oa_pro_id', '=', $id)->LeftJoin('nm_auction', 'nm_auction.auc_id', '=', 'nm_order_auction.oa_pro_id')->get();
    }

    public static function auction_winner($id)
    {
        return DB::table('nm_order_auction')->where('oa_pro_id', '=', $id)->where('oa_bid_winner', '=', 1)->get();
    }

    public static function check_delivery_status($id)
    {
        return DB::table('nm_order_auction')->where('oa_id', '=', $id)->get();
    }

    public static function select_auction_winner($entry, $id)
    {
        return DB::table('nm_order_auction')->where('oa_id', '=', $id)->update($entry);
    }

    public static function update_auction_status($entry, $id)
    {
        return DB::table('nm_order_auction')->where('oa_id', '=', $id)->update($entry);
    }

    public static function getproduct_columbia_all_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_date', 'desc')
        ->where('nm_transaksi.metode_pembayaran','=', 'Columbia')
        ->get();
    }

    public static function getproduct_tfr_bank_all_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('nm_order.order_paytype', 5)
        // ->where('nm_order.payment_channel', '01')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->orderBy('nm_order.order_date', 'desc')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();

    }

    public static function getproduct_all_orders()
    {

        return DB::table('nm_transaksi')
        ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->where('nm_order.order_paytype', '!=', 5)
        ->where('nm_order.order_paytype', '!=', 4)
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_customer.*', 'nm_product.*')
        ->orderBy('order_date','DESC')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();

    }

    public static function getproduct_success_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
        ->where('nm_order.order_paytype', '!=', 5)
        ->where('nm_order.order_paytype', '!=', 4)
        ->where('nm_transaksi.status_pembayaran', 'sudah dibayar')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_customer.*', 'nm_product.*')
        ->orderBy('order_date','DESC')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();

    }

    public static function getproduct_completed_orders()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 1)->get();

    }

    public static function getproduct_hold_orders()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 1)->get();

    }

    public static function getproduct_failed_orders()
    {
        return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->where('nm_transaksi.status_pembayaran', '=', 'expired')
            ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_customer.*', 'nm_product.*')
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();

    }

    public static function getcod_all_orders()
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_order_type', '=', 1)->get();

    }

    public static function getcod_completed_orders()
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 2)->where('nm_ordercod.cod_order_type', '=', 1)->get();

    }

    public static function getcod_hold_orders()
    {
        return DB::table('nm_ordercod')
        ->orderBy('cod_date', 'desc')
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
        ->where('nm_ordercod.cod_status', '=', 3)
        ->where('nm_ordercod.cod_order_type', '=', 1)
        ->get();

    }

    public static function getcod_failed_orders()
    {
       return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 1)->get();

    }

    public static function getdeals_all_orders()
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
        ->where('nm_order.order_type', '=', 2)
        ->get();

    }

    public static function getdeals_success_orders()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 2)->get();

    }

    public static function getdeals_completed_orders()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 2)->get();

    }

    public static function getdeals_hold_orders()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 2)->get();

    }

    public static function getdeals_failed_orders()
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 2)->get();

    }

    public static function getdealscod_all_orders()
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_order_type', '=', 2)->get();

    }

    public static function getdealscod_completed_orders()
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 2)->where('nm_ordercod.cod_order_type', '=', 2)->get();

    }
    public static function getdealscod_hold_orders()
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 3)->where('nm_ordercod.cod_order_type', '=', 2)->get();

    }

    public static function getdealscod_failed_orders()
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 2)->get();

    }

    public static function get_producttransaction()
    {
        return DB::table('nm_order')->where('order_type', '=', 1)->count();

    }

    public static function get_dealstransaction()
    {
        return DB::table('nm_order')->where('order_type', '=', 2)->count();
    }

    public static function get_auctiontransaction()
    {
        return DB::table('nm_order_auction')->count();
    }

    public static function get_producttoday_order()
    {
        return DB::select(DB::raw("SELECT count(order_id) as count,sum(order_amt) as amt  from nm_order where order_type=1 and DATEDIFF(DATE(order_date),DATE(NOW()))=0"));

    }

    public static function get_product7days_order()
    {
        return DB::select(DB::raw("select count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=1 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))"));
    }

    public static function get_product30days_order()
    {
        return DB::select(DB::raw("select  count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=1 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY))"));
    }

    public static function get_dealstoday_order()
    {
        return DB::select(DB::raw("SELECT count(order_id) as count,sum(order_amt) as amt  from nm_order where order_type=2 and DATEDIFF(DATE(order_date),DATE(NOW()))=0"));

    }

    public static function get_deals7days_order()
    {
        return DB::select(DB::raw("select count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=2 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))"));
    }

    public static function get_deals30days_order()
    {
        return DB::select(DB::raw("select  count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=2 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY))"));
    }

    public static function get_auctiontoday_order()
    {
        return DB::select(DB::raw("SELECT count(oa_id) as count,sum(oa_original_bit_amt) as amt  from nm_order_auction where   DATEDIFF(DATE(oa_bid_date),DATE(NOW()))=0"));

    }

    public static function get_auction7days_order()
    {
        return DB::select(DB::raw("select count(oa_id) as count,sum(oa_original_bit_amt) as amt from nm_order_auction WHERE  (DATE(oa_bid_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))"));
    }

    public static function get_auction30days_order()
    {
        return DB::select(DB::raw("select  count(oa_id) as count,sum(oa_original_bit_amt) as amt from nm_order_auction WHERE  (DATE(oa_bid_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY))"));
    }

    public static function get_chart_product_details()
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_order WHERE  order_type=1 and MONTH( `order_date` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_chart_deals_details()
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_order WHERE order_type=2 and MONTH( `order_date` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_chart_auction_details()
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_order_auction WHERE MONTH( `oa_bid_date` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_funds()
    {
        return DB::table('nm_withdraw_request')
        ->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_withdraw_request.wd_mer_id')
        ->get();
    }

    public static function success_fund_request()
    {
        return DB::table('nm_withdraw_request')
        ->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_withdraw_request.wd_mer_id')
        ->where('wd_status', 2)
        ->get();
        // return DB::select(DB::raw("  SELECT * FROM  nm_withdraw_request_paypal f LEFT JOIN nm_merchant on f.wr_mer_id=nm_merchant.mer_id where f.wr_status = 'Success'"));
    }

    public static function pending_fund_request()
    {
        return DB::table('nm_withdraw_request')
        ->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_withdraw_request.wd_mer_id')
        ->where('wd_status', 0)
        ->get();
        // return DB::select(DB::raw("  SELECT * FROM  nm_withdraw_request_paypal f LEFT JOIN nm_merchant on f.wr_mer_id=nm_merchant.mer_id where f.wr_status = 'Pending'"));
    }

    public static function failed_fund_request()
    {
        return DB::table('nm_withdraw_request')
        ->LeftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_withdraw_request.wd_mer_id')
        ->where('wd_status', 1)
        ->get();
        // return DB::select(DB::raw("  SELECT * FROM  nm_withdraw_request_paypal f LEFT JOIN nm_merchant on f.wr_mer_id=nm_merchant.mer_id where f.wr_status = 'Failed'"));
    }

   public static function insert_funds_paypal($entry)
    {
        $d = DB::table('nm_withdraw_request_paypal')->insert($entry);

    }

    public static function update_funds_paypal($entry, $id)
    {
        return DB::table('nm_withdraw_request_paypal')->where('wr_txn_id', '=', $id)->update($entry);
    }

    public static function update_cod_status($status, $orderid)
    {
        return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->update(array(
            'cod_status' => $status
        ));

    }

    public static function getall_dealreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_type', '=', 2)->where('nm_order.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {

        }

    }

    public static function get_successreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 2)->where('nm_order.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();


        } else {

        }

    }

    public static function get_holdreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 2)->where('nm_order.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {

        }

    }

    public static function get_failedreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 2)->where('nm_order.created_date', $from_date)->get();
        }

        elseif ($from_date != '' & $to_date != '') {


            return DB::table('nm_order')->orderBy('order_id', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {

        }

    }


    public static function get_codreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_order_type', '=', 2)->where('nm_ordercod.created_date', $from_date)->get();
        }

        elseif ($from_date != '' & $to_date != '') {


            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_order_type', '=', 2)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function get_completedreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 2)->where('nm_ordercod.cod_order_type', '=', 2)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 2)->where('nm_ordercod.cod_order_type', '=', 2)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {

        }

    }

    public static function getcod_holdreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 3)->where('nm_ordercod.cod_order_type', '=', 2)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 3)->where('nm_ordercod.cod_order_type', '=', 2)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }


    public static function getcod_failedreports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 2)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_ordercod')->orderBy('cod_id', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 2)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function getall_reports($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->where('nm_order.order_date', '>=', $from_date)
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }
        elseif ($from_date == '' & $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->where('nm_order.order_date', '<=', $to_date)
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->whereBetween('nm_order.order_date', array(
                $from_date,
                $to_date
            ))
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }

    }

    public static function columbia_all_reports($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_order')
            ->orderBy('order_id', 'desc')
            ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->leftjoin('nm_transaksi', 'nm_transaksi.metode_pembayaran', '=', 'Columbia')
            ->where('nm_order.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_order')
            ->orderBy('order_id', 'desc')
            ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->leftjoin('nm_transaksi', 'nm_transaksi.metode_pembayaran', '=', 'Columbia')
            ->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();
        }

        else {

        }

    }

    public static function tfr_bank_all_reports($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->get();
        }
    }

    public static function product_successrep($from_date, $to_date)
    {
        if ($from_date != '' & $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_transaksi.status_pembayaran', 'sudah dibayar')
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->where('nm_order.order_date', '>=', $from_date)
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }
        elseif ($from_date == '' & $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_transaksi.status_pembayaran', 'sudah dibayar')
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->where('nm_order.order_date', '<=', $to_date)
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_transaksi.status_pembayaran', 'sudah dibayar')
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        } else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
            ->where('nm_order.order_paytype', '!=', 5)
            ->where('nm_order.order_paytype', '!=', 4)
            ->where('nm_transaksi.status_pembayaran', 'sudah dibayar')
            ->where('nm_product.pro_title','!=','Jasa Pengiriman')
            ->where('nm_product.pro_title','!=','Diskon')
            ->orderBy('order_date','DESC')
            ->groupBy('nm_transaksi.transaction_id')
            ->get();
        }

    }

    public static function product_holdrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 1)->where('nm_order.created_date', $from_date)->get();
        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 1)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {

        }

    }

    public static function product_failedrep($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
                ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
                ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
                ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
                ->where('nm_order.order_paytype', '!=', 5)
                ->where('nm_order.order_paytype', '!=', 4)
                ->where('nm_product.pro_title','!=','Jasa Pengiriman')
                ->where('nm_product.pro_title','!=','Diskon')
                ->where('nm_transaksi.status_pembayaran', '=', 'expired')
                ->where('nm_order.order_date', '>=', $from_date)
                ->orderBy('order_date','DESC')
                ->groupBy('nm_transaksi.transaction_id')
                ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
                ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
                ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
                ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
                ->where('nm_order.order_paytype', '!=', 5)
                ->where('nm_order.order_paytype', '!=', 4)
                ->where('nm_product.pro_title','!=','Jasa Pengiriman')
                ->where('nm_product.pro_title','!=','Diskon')
                ->where('nm_transaksi.status_pembayaran', '=', 'expired')
                ->where('nm_order.order_date', '<=', $to_date)
                ->orderBy('order_date','DESC')
                ->groupBy('nm_transaksi.transaction_id')
                ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
                ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
                ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
                ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
                ->where('nm_order.order_paytype', '!=', 5)
                ->where('nm_order.order_paytype', '!=', 4)
                ->where('nm_product.pro_title','!=','Jasa Pengiriman')
                ->where('nm_product.pro_title','!=','Diskon')
                ->where('nm_transaksi.status_pembayaran', '=', 'expired')
                ->whereBetween('nm_order.order_date', array($from_date, $to_date))
                ->orderBy('order_date','DESC')
                ->groupBy('nm_transaksi.transaction_id')
                ->get();
        }
        else {
            return DB::table('nm_transaksi')
                ->leftjoin('nm_order','nm_order.transaction_id','=','nm_transaksi.transaction_id')
                ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
                ->leftjoin('nm_product','nm_product.pro_id','=','nm_order.order_pro_id')
                ->where('nm_order.order_paytype', '!=', 5)
                ->where('nm_order.order_paytype', '!=', 4)
                ->where('nm_product.pro_title','!=','Jasa Pengiriman')
                ->where('nm_product.pro_title','!=','Diskon')
                ->where('nm_transaksi.status_pembayaran', '=', 'expired')
                ->orderBy('order_date','DESC')
                ->groupBy('nm_transaksi.transaction_id')
                ->get();
        }
    }

    public static function product_codrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_order_type', '=', 1)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_order_type', '=', 1)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function product_completedrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 2)->where('nm_ordercod.cod_order_type', '=', 1)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 2)->where('nm_ordercod.cod_order_type', '=', 1)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {

        }

    }

    public static function product_columbia_holdrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'belum dibayar')
            ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
            ->where('nm_order.created_date', $from_date)
            ->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'belum dibayar')
            ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
            ->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function product_tfr_bank_holdrep($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->get();
        }
    }

    public static function product_tfr_bank_success_rep($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->get();
        }

    }

    public static function product_columbia_success_rep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_transaksi.Id', 'desc')
            ->where('nm_order.created_date', $from_date)
            ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
            ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
            ->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_transaksi.Id', 'desc')
            ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
            ->where('nm_transaksi.status_pembayaran','=','sudah dibayar')
            ->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function product_columbia_failedrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'expired')
            ->where('nm_transaksi.metode_pembayaran','=', 'Columbia')
            ->where('nm_order.created_date', $from_date)
            ->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'expired')
            ->where('nm_transaksi.metode_pembayaran','=', 'Columbia')
            ->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function product_transfer_bank_failedrep($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'expired')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'expired')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'expired')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=', 'expired')
            ->where('nm_order.order_paytype', '=', 5)
            // ->where('nm_order.payment_channel', '01')
            ->get();
        }
    }

    public static function get_tfr_bank_hold_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_date', 'desc')
        ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
        ->where('nm_order.order_paytype', '=', 5)
        // ->where('nm_order.payment_channel', '01')
        ->get();
    }

    public static function get_tfr_bank_success_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_date', 'desc')
        ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
        ->where('nm_order.order_paytype', '=', 5)
        // ->where('nm_order.payment_channel', '01')
        ->get();
    }

    public static function get_columbia_success_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_transaksi.Id', 'desc')
        ->where('nm_transaksi.status_pembayaran','=', 'sudah dibayar')
        ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
        ->get();
    }

    public static function get_columbia_hold_orders()
    {
       return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_date', 'desc')
        ->where('nm_transaksi.status_pembayaran', '=', 'belum dibayar')
        ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
        ->get();
    }

    public static function get_columbia_failed_orders()
    {
       return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_date', 'desc')
        ->where('nm_transaksi.status_pembayaran','=', 'expired')
        ->where('nm_transaksi.metode_pembayaran', '=', 'Columbia')
        ->get();
    }

    public static function get_transfer_bank_failed_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->groupBy('nm_transaksi.transaction_id')
        ->orderBy('nm_order.order_date', 'desc')
        ->where('nm_transaksi.status_pembayaran','=', 'expired')
        ->where('nm_order.order_paytype', '=', 5)
        ->where('nm_order.payment_channel', '01')
        ->get();
    }

    public static function productcod_holdrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {


            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 3)->where('nm_ordercod.cod_order_type', '=', 1)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 3)->where('nm_ordercod.cod_order_type', '=', 1)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function productcod_failedrep($from_date, $to_date)
    {

        if ($from_date != '' & $to_date == '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 1)->where('nm_ordercod.created_date', $from_date)->get();

        }

        elseif ($from_date != '' & $to_date != '') {

            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 1)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->get();

        } else {

        }

    }

    public static function update_poin_to_zero($cus_id)
    {
        return DB::table('nm_customer')
        ->where('nm_customer.cus_id', '=', $cus_id)
        ->update(array(
            'total_poin' => 0
            ));
    }

    public static function cash_all_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('nm_order.order_paytype', 5)
        ->where('nm_order.payment_channel', '02')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->orderBy('nm_order.order_date', 'desc')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();
    }

    public static function cash_hold_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('nm_order.order_paytype', 5)
        ->where('nm_order.payment_channel', '02')
        ->where('nm_transaksi.status_pembayaran','=','belum dibayar')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->orderBy('nm_order.order_date', 'desc')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();
    }

    public static function cash_success_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('nm_order.order_paytype', 5)
        ->where('nm_order.payment_channel', '02')
        ->where('nm_transaksi.status_pembayaran','=','sudah dibayar')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->orderBy('nm_order.order_date', 'desc')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();
    }

    public static function cash_failed_orders()
    {
        return DB::table('nm_transaksi')
        ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
        ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
        ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
        ->where('nm_order.order_paytype', 5)
        ->where('nm_order.payment_channel', '02')
        ->where('nm_transaksi.status_pembayaran','=','expired')
        ->where('nm_product.pro_title','!=','Jasa Pengiriman')
        ->where('nm_product.pro_title','!=','Diskon')
        ->select(DB::raw('sum(order_jumlah_warranty) as warranty'), 'nm_transaksi.*', 'nm_order.*', 'nm_product.*', 'nm_customer.*')
        ->orderBy('nm_order.order_date', 'desc')
        ->groupBy('nm_transaksi.transaction_id')
        ->get();
    }

    public static function cash_all_reports($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->get();
        }
    }

    public static function cash_hold_reports($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','belum dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->get();
        }
    }

    public static function cash_success_reports($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','sudah dibayar')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->get();
        }
    }

    public static function cash_failed_reports($from_date, $to_date)
    {
        if ($from_date != '' && $to_date == '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','expired')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '>=', $from_date)
            ->get();
        }
        elseif ($from_date == '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','expired')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->where('nm_order.order_date', '<=', $to_date)
            ->get();
        }
        elseif ($from_date != '' && $to_date != '') {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','expired')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->whereBetween('nm_order.order_date', array($from_date, $to_date))
            ->get();
        }
        else {
            return DB::table('nm_transaksi')
            ->leftjoin('nm_order', 'nm_order.transaction_id','=','nm_transaksi.transaction_id')
            ->leftjoin('nm_product', 'nm_product.pro_id', '=',  'nm_order.order_pro_id')
            ->leftjoin('nm_shipping', 'nm_order.order_cus_id', '=', 'nm_shipping.ship_cus_id')
            ->leftjoin('nm_customer','nm_customer.cus_id','=','nm_order.order_cus_id')
            ->groupBy('nm_transaksi.transaction_id')
            ->orderBy('nm_order.order_date', 'desc')
            ->where('nm_transaksi.status_pembayaran','=','expired')
            ->where('nm_order.order_paytype', '=', 5)
            ->where('nm_order.payment_channel', '02')
            ->get();
        }
    }
}

?>
