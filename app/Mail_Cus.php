<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Mail_Cus extends Model
{
    public static function insert_mail_cus($data)
    {
        return DB::table('nm_mail_cus')->insert($data);
    }

    public static function get_mail_cus()
    {
        return DB::table('nm_mail_cus')
        ->where('status', 0)
        ->first();
    }
}

?>
