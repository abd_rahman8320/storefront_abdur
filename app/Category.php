<?php
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Category extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_maincategory';
    protected $primaryKey = 'mc_id';

    public static function update_main_category($isi, $smc_code)
    {
        return DB::table('nm_secmaincategory')->where('smc_code', $smc_code)->update($isi);
    }

    public static function save_category($sizes)
    {
        return DB::table('nm_maincategory')->insert($sizes);
    }

    public static function maincatg_list()
    {
        return DB::table('nm_maincategory')->get();
    }

    public static function selectedcatg_list($id)
    {
        return DB::table('nm_maincategory')->where('mc_id', '=', $id)->get();
    }

    //select id
    public static function select_id_main($mc_code){
      return DB::table('nm_maincategory')->select('mc_id')->where('mc_code', '=', $mc_code)->first();
    }

    public static function select_id_secmain($smc_code){
      return DB::table('nm_secmaincategory')->select('smc_id', 'smc_mc_id')->where('smc_code', '=', $smc_code)->first();
    }

    public static function select_id_sub($sb_code){
      return DB::table('nm_subcategory')->select('sb_id', 'sb_smc_id', 'mc_id')->where('sb_code', '=', $sb_code)->first();
    }


    public static function update_category_detail($entry, $mc_code)
    {
        return DB::table('nm_maincategory')->where('mc_code', '=', $mc_code)->update($entry);
    }

    public static function status_category_submit($id, $status)
    {
        return DB::table('nm_maincategory')->where('mc_id', '=', $id)->update(array('mc_status' => $status));
    }

    // public static function delete_category($id)
    // {
    //     return DB::table('nm_maincategory')->where('mc_id', '=', $id)->delete();
    // }
    public static function delete_category($id)
    {

        // To start Image delete from folder 09/11/
        $filename = DB::table('nm_maincategory')->where('mc_id', '=', $id)->first();
        $getimagename= $filename->mc_img;
        $getextension=explode("/**/",$getimagename);
        foreach($getextension as $imgremove)
        {
            File::delete(base_path('assets/categoryimage/').$imgremove);
        }
        // To End
        return DB::table('nm_maincategory')->where('mc_id', '=', $id)->delete();

    }
    public static function save_main_category($sizes)
    {
        return DB::table('nm_secmaincategory')->insert($sizes);
    }

    public static function maincatg_sub_list($catg_list)
    {
        foreach ($catg_list as $catg) {
            $catg_result          = DB::table('nm_secmaincategory')->where('smc_mc_id', '=', $catg->mc_id)->get();
            $result[$catg->mc_id] = count($catg_result);
        }

        if (isset($result)) {
            return $result;
        }
    }

    public static function sub_maincatg_list($catg_id)
    {
        return DB::table('nm_secmaincategory')->where('smc_mc_id', '=', $catg_id)->LeftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_secmaincategory.smc_mc_id')->get();
    }

    public static function add_sub_catg_details($catg_id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $catg_id)->get();
    }

    public static function save_sub_category($sizes)
    {
        return DB::table('nm_subcategory')->insert($sizes);
    }

    public static function subcatg_count_list($catg_list)
    {
        if (empty($catg_list)) {
            Redirect::to('index');
        }
        foreach ($catg_list as $catg) {
            $catg_result = DB::table('nm_subcategory')->where('sb_smc_id', '=', $catg->smc_id)->where('mc_id', '=', $catg->smc_mc_id)->get();
            if ($catg_result) {
                $result[$catg->smc_id] = count($catg_result);
            } else {
                $result[$catg->smc_id] = 0;
            }
        }
        return $result;
    }

    public static function sub_catg_list($catg_id)
    {
        return DB::table('nm_subcategory')->where('sb_smc_id', '=', $catg_id)->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_subcategory.sb_smc_id')->get();
    }

    public static function secsubcatg_count_list($catg_list)
    {
        foreach ($catg_list as $catg) {
            $catg_result = DB::table('nm_secsubcategory')->where('ssb_sb_id', '=', $catg->sb_id)->where('ssb_smc_id', '=', $catg->mc_id)->get();
            if ($catg_result) {
                $result[$catg->sb_id] = count($catg_result);
            } else {
                $result[$catg->sb_id] = 0;
            }

        }
        return $result;

    }

    public static function edit_main_catg_details($catg_id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $catg_id)->leftJoin('nm_maincategory', 'nm_maincategory.mc_id', '=', 'nm_secmaincategory.smc_mc_id')->get();
    }

    public static function save_edit_main_category($sizes, $id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->update($sizes);
    }

    public static function status_main_category_submit($id, $status)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->update(array('smc_status' => $status));
    }

    public static function delete_main_category($id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->delete();
    }

    public static function add_secsub_main_category($catg_id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $catg_id)->get();
    }

    public static function save_secsub_category($sizes)
    {
        return DB::table('nm_secsubcategory')->insert($sizes);
    }

    public static function edit_secsub_catg_details($catg_id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $catg_id)->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_subcategory.sb_smc_id')->get();
    }

    public static function save_editsecsub_main_category($sizes, $id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->update($sizes);
    }

    public static function status_subsec_category_submit($id, $status)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->update(array('sb_status' => $status));
    }

    public static function delete_subsec_category($id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->delete();
    }

    public static function secsub_catg_list($catg_id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_sb_id', '=', $catg_id)->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_secsubcategory.ssb_sb_id')->get();
    }

    public static function status_secsub_category_submit($id, $status)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->update(array('ssb_status' => $status));
    }

    public static function delete_secsub_category($id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->delete();
    }

    public static function edit_sec1sub_catg_details($catg_id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $catg_id)->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_secsubcategory.ssb_sb_id')->get();
    }

    public static function save_editsec1sub_main_category($sizes, $id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->update($sizes);
    }

}

?>
