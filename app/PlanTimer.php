<?php
namespace App;
use DB;
use File;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class PlanTimer extends Model
{

  public static function get_coupons_h()
  {
    return DB::table('nm_promo_coupons')
    ->leftJoin('nm_scheduled_promo','nm_scheduled_promo.schedp_id','=','nm_promo_coupons.promoc_schedp_id')
    ->orderBy('promoc_id','DESC')
    ->get();
  }

  public static function save_promo_coupons($entry)
  {
      return DB::table('nm_promo_coupons')->insert($entry);
  }

  public static function save_scheduled_promo($entry)
  {
      return DB::table('nm_scheduled_promo')->insert($entry);
  }

  public static function save_promo_products($entry)
  {
    //dd($entry['promop_pro_id']);
      return DB::table('nm_promo_products')->insert($entry);
  }

  public static function edit_promo_product($entry, $id)
  {
      return DB::table('nm_promo_products')->where('promop_schedp_id', '=', $id)->update($entry);
  }

  public static function edit_schedule_promo($entry, $id)
  {
      return DB::table('nm_scheduled_promo')->where('schedp_id', '=', $id)->update($entry);
  }

  public static function edit_promo_coupons($id, $entry)
  {
      return DB::table('nm_promo_coupons')
      ->where('status_coupons','=','')
      ->where('promoc_schedp_id', $id)->update($entry);
  }

  public static function get_last_schedule()
  {
      return DB::table('nm_scheduled_promo')->orderBy('schedp_id', 'desc')->first();
      //cek
  }

  public static function get_promo_coupons($id)
  {
      return DB::table('nm_promo_coupons')->where('promoc_schedp_id', $id)->first();
  }

  public static function get_promo_id($code_promo)
  {
      return DB::table('nm_scheduled_promo')->select('schedp_id')->where('code_promo', $code_promo)->first();
  }

  public static function get_schedule_by_id($id)
  {
      return DB::table('nm_scheduled_promo')->where('schedp_id', $id)->get();
  }

  public static function get_schedule()
    {
        return DB::table('nm_scheduled_promo')
        ->orderBy('schedp_id','DESC')
        ->distinct()
        ->get();
    }

    public static function get_schedule_merchant()
    {
        return DB::table('nm_scheduled_promo')
        ->orderBy('schedp_id', 'DESC')
        ->distinct()
        ->get();
    }

  public static function mer_get_schedule($mer_id,$date)
	{
	   return DB::table('nm_scheduled_promo')
     ->leftJoin('nm_promo_products','nm_promo_products.promop_schedp_id','=','nm_scheduled_promo.schedp_id')
     ->leftJoin('nm_merchant','nm_merchant.mer_id','=','nm_promo_products.promop_merchant_id')
     ->where('mer_id',$mer_id)
     ->select('nm_scheduled_promo.*')
     ->where('schedp_end_date','>',$date)
     ->distinct()
     ->get();
	}

  public static function get_promo_by_id($id){
    return DB::table('nm_promo_products')->where('promop_schedp_id', '=', $id)->get();
  }

  public static function get_schedule_id_by_code($code){
    return DB::table('nm_scheduled_promo')->select('schedp_id')->where('code_promo', '=', $code)->first();
  }

  public static function delete_schedule($id)
 {
    DB::table('nm_scheduled_promo')->where('schedp_id', '=', $id)->delete();
    DB::table('nm_promo_coupons')->where('promoc_schedp_id', '=', $id)->delete();
    return DB::table('nm_promo_products')->where('promop_schedp_id', '=', $id)->delete();
 }

 public static function delete_promo_product_by_id($id){
    DB::table('nm_promo_products')->where('promop_schedp_id', '=', $id)->delete();
 }

 }
?>
