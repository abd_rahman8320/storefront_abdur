-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 10, 2017 at 06:22 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devkukur_storefront`
--

-- --------------------------------------------------------

--
-- Table structure for table `nm_groping_product_detail`
--

CREATE TABLE `nm_groping_product_detail` (
  `group_detail_id` int(11) NOT NULL,
  `group_detail_id_grouping` int(11) NOT NULL,
  `group_detail_id_product` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nm_groping_product_detail`
--

INSERT INTO `nm_groping_product_detail` (`group_detail_id`, `group_detail_id_grouping`, `group_detail_id_product`, `status`, `created_date`) VALUES
(1, 1, 17, 'aktif', '2017-05-10'),
(2, 1, 38, 'aktif', '2017-05-10'),
(3, 3, 62, 'aktif', '2017-05-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_groping_product_detail`
--
ALTER TABLE `nm_groping_product_detail`
  ADD PRIMARY KEY (`group_detail_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_groping_product_detail`
--
ALTER TABLE `nm_groping_product_detail`
  MODIFY `group_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
