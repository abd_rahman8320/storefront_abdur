-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2017 at 05:11 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `storefront`
--

-- --------------------------------------------------------

--
-- Table structure for table `nm_partners`
--

CREATE TABLE `nm_partners` (
  `par_id` smallint(5) UNSIGNED NOT NULL,
  `par_name` varchar(100) NOT NULL,
  `par_position` tinyint(4) NOT NULL COMMENT '1-Header right,2-Left side bar,3-Bottom footer ',
  `par_pages` tinyint(4) NOT NULL COMMENT '1-home,2-product,3-Deal,4-Auction ',
  `par_redirecturl` varchar(150) NOT NULL,
  `par_img` varchar(150) NOT NULL,
  `par_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-admin 2 customer',
  `par_status` tinyint(3) UNSIGNED NOT NULL,
  `par_read_status` int(11) NOT NULL DEFAULT '0' COMMENT ' 0-not read 1 read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nm_partners`
--
ALTER TABLE `nm_partners`
  ADD PRIMARY KEY (`par_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nm_partners`
--
ALTER TABLE `nm_partners`
  MODIFY `par_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
