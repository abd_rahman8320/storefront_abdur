<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body id="product_page" style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody" style="overflow-x:hidden;">
	<div class="container">
	<div class="row">
	<!-- Sidebar ================================================== -->
	<div id="sidebar" class="span3 pull-right">
		<div class="well well-small btn-warning"><strong>Related Products</strong></div>
		<div class="row">
		<ul class="thumbnails">

	<?php
	if ($get_related_product){ foreach($get_related_product as $product_det){
		if (!isset($get_cur)) $get_cur = "IDR";
		$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
		$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
		$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
		$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
		$res = base64_encode($product_det->pro_id);
	$product_image = explode('/**/',$product_det->pro_Img);
	if($product_det->pro_disprice==0 || $product_det->pro_disprice=='' || $product_det->pro_disprice==null)
	{
		$product_saving_price = 0;
	}else {
		$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
	}

	if($product_det->pro_price==0 || $product_det->pro_price==null)
	{
		$product_discount_percentage = 0;
	}else {
		$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
	}
	  if($product_det->pro_no_of_purchase < $product_det->pro_qty) {
	?>
			<li class="span3">
			  <div class="thumbnail" style="width:95%;">
				  <?php if($product_discount_percentage!=0 && $product_discount_percentage!='')
				  { ?>
				  <i class="tag"><?php

				  echo round($product_discount_percentage);?>%</i>
				  <?php
				  }
				  ?>
				  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
					<a href="<?php echo url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res); ?>"><img alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>" style="height:215px;" target="_self"></a>
					 <?php } ?>
					 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
					 <a href="<?php echo url('productview1/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res); ?>"><img alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>" style="height:215px;" target="_self"></a>
					 <?php } ?>
					 <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
					 <a href="<?php echo url('productview2/'.$mcat.'/'.$smcat.'/'.$res); ?>" target="_self"><img alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>" style="height:215px;" ></a>
					 <?php } ?>
					<div class="caption product_show">
					<h3 class="prev_text"><?php echo substr ($product_det->pro_title,0,20);?></h3>

					<?php
					if($product_det->pro_disprice != 0)
					{
						?>
							<h4 class="top_text dolor_text">{{$get_cur}}<?php echo number_format($product_det->pro_disprice);?></h4>
						<?php
					}
					else
					{
						?>
							<h4 class="top_text dolor_text">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></h4>
						<?php
					}

					?>

                      <?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty) { ?>
                                    <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a></h4>
                                    <?php } else { ?>
					  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
<a href="<?php echo url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res); ?>" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
 <?php } ?>
 <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
 <a href="<?php echo url('productview1/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res); ?>" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
 <?php } ?>
 <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
 <a href="<?php echo url('productview2/'.$mcat.'/'.$smcat.'/'.$res); ?>" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
 <?php } ?>
                      <?php } ?>
					</div>
				  </div>
			</li>
            <?php } } } else{ ?>
         <li class="span3">
			  <div class="thumbnail">
					No Product's Available

				  </div>
			</li>
             <?php } ?>

				  </ul>
				  </div>
		<br>
		  <div class="clearfix"></div>
		<br/>

	</div>

<!-- Sidebar end=============================================== -->
<?php foreach($product_details_by_id as $pro_details_by_id) { }
  $product_img= explode('/**/',trim($pro_details_by_id->pro_Img,"/**/"));
	$img_count = count($product_img);
 ?>
	<div class="span9 tab-land-wid">
<div class="clearfix"></div>
    <ul class="breadcrumb">
    <li><a href="<?php echo url('index');?>" target="_self">Home</a> <span class="divider">/</span></li>
    <li><a href="<?php echo url('products');?>" target="_self">Products</a> <span class="divider">/</span></li>
	<?php foreach($breadcrumb as $fetch_main_cat) { //print_r($fetch_main_cat); exit;
	$pass_cat_id1 = "1,".$fetch_main_cat->pro_mc_id;
	$pass_cat_id2 = "2,".$fetch_main_cat->pro_smc_id;
	$pass_cat_id3 = "3,".$fetch_main_cat->pro_sb_id;
	$pass_cat_id4 = "4,".$fetch_main_cat->pro_ssb_id;//echo $pass_cat_id1; exit;?>
     <?php if($pro_details_by_id->mc_name != '' && $pro_details_by_id->smc_name != '' && $pro_details_by_id->sb_name != '' && $pro_details_by_id->ssb_name != '') { ?>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id1).'');?>" target="_self"><?php echo $pro_details_by_id->mc_name; ?></a> <span class="divider">/</span></li>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id2).'');?>" target="_self"><?php echo $pro_details_by_id->smc_name; ?></a> <span class="divider">/</span></li>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id3).'');?>" target="_self"><?php echo $pro_details_by_id->sb_name; ?></a> <span class="divider">/</span></li>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id4).'');?>" target="_self"><?php echo $pro_details_by_id->ssb_name; ?></a> <span class="divider">/</span></li>
 <?php } ?>
 <?php if($pro_details_by_id->mc_name != '' && $pro_details_by_id->smc_name != '' && $pro_details_by_id->sb_name != '' && $pro_details_by_id->ssb_name == '') { ?>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id1).'');?>" target="_self"><?php echo $pro_details_by_id->mc_name; ?></a> <span class="divider">/</span></li>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id2).'');?>" target="_self"><?php echo $pro_details_by_id->smc_name; ?></a> <span class="divider">/</span></li>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id3).'');?>" target="_self"><?php echo $pro_details_by_id->sb_name; ?></a> <span class="divider">/</span></li>
 <?php } ?>
 <?php if($pro_details_by_id->mc_name != '' && $pro_details_by_id->smc_name != '' && $pro_details_by_id->sb_name == '' && $pro_details_by_id->ssb_name == '') { ?>
  <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id1).'');?>" target="_self"><?php echo $pro_details_by_id->mc_name; ?></a> <span class="divider">/</span></li>
     <li><a href="<?php echo url('catproducts/viewcategorylist/'.base64_encode($pass_cat_id2).'');?>" target="_self"><?php echo $pro_details_by_id->smc_name; ?></a> <span class="divider">/</span></li>

  <?php } } ?>
    <li class="active"><?php echo  $pro_details_by_id->pro_title; ?></li>
    </ul>
    <div class="row">
    <div class="span3">
			<div class="bungkus-foto" style="max-width: 364px;">
            	<a id="Zoomer3"  href="{!! url('assets/product/').'/'.$product_img[0]!!}" class="MagicZoomPlus" rel="selectors-effect: fade; selectors-change: mouseover; " title="<?php echo  $pro_details_by_id->pro_title; ?>" style="max-width:100%;height:212px;"><img style="height:auto;" src="{!! url('assets/product/').'/'.$product_img[0]!!}"/></a> <br/>
 <?php for($i=0;$i < $img_count;$i++) { ?>
        <a href="{!! url('assets/product/').'/'.$product_img[$i]!!}" rel="zoom-id: Zoomer3" rev="{!! url('assets/product/').'/'.$product_img[$i]!!}"><img src="{!! url('assets/product/').'/'.$product_img[$i]!!}" style="width:30px;height:32px"/></a>
 <?php } ?>
 			</div>
		<br>
		<table class="table table-bordered cke_show_border merchant-info">
				<tbody>
					<tr>
						<th>Nama Merchant</th>
						<td><?php echo $get_storename->mer_fname;?></td>
					</tr>
					<tr>
						<th>Lokasi</th>
						<td><?php echo $get_storename->ci_name;?></td>
					</tr>
					<tr>
						<th>Harga Grosir</th>
						<td><?php
							// render wholesale price level 1
							//dd($pro_details_by_id);
							if ($pro_details_by_id->wholesale_level1_min) {
								echo '<span class="wholesale-quantity">';
								echo $pro_details_by_id->wholesale_level1_min.' - ';
								echo $pro_details_by_id->wholesale_level1_max;
								echo '</span>';
								echo '<span class="wholesale-price">';
								echo $get_cur . ' ' . number_format($pro_details_by_id->wholesale_level1_price);
								echo '</span>';
								// render wholesale price level 2
								if ($pro_details_by_id->wholesale_level2_min) {
									echo '<span class="wholesale-quantity">';
									echo $pro_details_by_id->wholesale_level2_min.' - ';
									echo $pro_details_by_id->wholesale_level2_max;
									echo '</span>';
									echo '<span class="wholesale-price">';
									echo $get_cur . ' ' . number_format($pro_details_by_id->wholesale_level2_price);
									echo '</span>';
									// render wholesale price level 2
									if ($pro_details_by_id->wholesale_level3_min) {
										echo '<span class="wholesale-quantity">';
										echo $pro_details_by_id->wholesale_level3_min.' - ';
										echo $pro_details_by_id->wholesale_level3_max;
										echo '</span>';
										echo '<span class="wholesale-price">';
										echo $get_cur . ' ' . number_format($pro_details_by_id->wholesale_level3_price);
										echo '</span>';
										// render wholesale price level 4
										if ($pro_details_by_id->wholesale_level4_min) {
											echo '<span class="wholesale-quantity">';
											echo $pro_details_by_id->wholesale_level4_min.' - ';
											echo $pro_details_by_id->wholesale_level4_max;
											echo '</span>';
											echo '<span class="wholesale-price">';
											echo $get_cur . ' ' . number_format($pro_details_by_id->wholesale_level4_price);
											echo '</span>';
											// render wholesale price level 5
											if ($pro_details_by_id->wholesale_level5_min) {
												echo '<span class="wholesale-quantity">';
												echo $pro_details_by_id->wholesale_level5_min.' - ';
												echo $pro_details_by_id->wholesale_level5_max;
												echo '</span>';
												echo '<span class="wholesale-price">';
												echo $get_cur . ' ' . number_format($pro_details_by_id->wholesale_level5_price);
												echo '</span>';
											}
										}
									}
								}
							} else {
								echo 'n/a';
							}
						?>
						</td>
					</tr>
				</tbody>
			</table>
			@if($pro_details_by_id->jumlah_cicilan_3bulan > 0 || $pro_details_by_id->jumlah_cicilan_6bulan > 0 || $pro_details_by_id->jumlah_cicilan_12bulan > 0)
			<p style="color:green;text-align:center;"><b>SNP FINANCE</b></p>
			<table class="table table-bordered cke_show_border merchant-info">
				<tbody>
					@if($pro_details_by_id->jumlah_cicilan_3bulan > 0)
					<tr>
						<th>Cicilan 3 Bulan</th>
						<td><b>Rp{{number_format($pro_details_by_id->jumlah_cicilan_3bulan, 0, '.', ',')}}/bulan</b></td>
					</tr>
					@endif
					@if($pro_details_by_id->jumlah_cicilan_6bulan > 0)
					<tr>
						<th>Cicilan 6 Bulan</th>
						<td><b>Rp{{number_format($pro_details_by_id->jumlah_cicilan_6bulan, 0, '.', ',')}}/bulan</b></td>
					</tr>
					@endif
					@if($pro_details_by_id->jumlah_cicilan_12bulan > 0)
					<tr>
						<th>Cicilan 12 Bulan</th>
						<td><b>Rp{{number_format($pro_details_by_id->jumlah_cicilan_12bulan, 0, '.', ',')}}/bulan</b></td>
					</tr>
					@endif
				</tbody>
			</table>
			@endif
            </div>

			<div class="span6 tab-produ-view">
				<h3><?php echo  $pro_details_by_id->pro_title; ?> - <?php echo $pro_details_by_id->grade_name ?></h3>
<?php
								$product_count = $one_count + $two_count + $three_count + $four_count + $five_count;

			//echo $product_count;
			$multiple_countone = $one_count *1;

			$multiple_counttwo = $two_count *2;

			$multiple_countthree = $three_count *3;

			$multiple_countfour = $four_count *4;

			$multiple_countfive = $five_count *5;
			$product_total_count = $multiple_countone + $multiple_counttwo + $multiple_countthree + $multiple_countfour + $multiple_countfive;
			//echo $product_total_count;
	if($product_count)
	{
		$product_divide_count = $product_total_count / $product_count;
		if(!isset($jumlah_ratings)){
			$jumlah_ratings = 0;
		}
		if($jumlah_ratings == '1')
		{
			//dd("masuk if 1");
			?>
				<img src='<?php echo url('images/stars-1.png'); ?>' style='margin-bottom:10px;'> Ratings
			<?php
		}
		elseif($jumlah_ratings == '1')
		{
			//dd("masuk else if 1");
			?>
				<img src='<?php echo url('images/stars-1.png'); ?>' style='margin-bottom:10px;'> Ratings
			<?php
		}
		elseif($jumlah_ratings == '2')
		{
			//dd("masuk else if 2");
			?>
				<img src='<?php echo url('images/stars-2.png'); ?>' style='margin-bottom:10px;'> Ratings
			<?php
		}
		elseif($jumlah_ratings == '3')
		{
			//dd("masuk else if 3");
			?>
			<img src='<?php echo url('images/stars-3.png'); ?>' style='margin-bottom:10px;'> Ratings
			<?php

		}
		elseif($jumlah_ratings == '4')
		{
			//dd("masuk else if 4");
			?>
			<img src='<?php echo url('images/stars-4.png'); ?>' style='margin-bottom:10px;'> Ratings
			<?php
		}
		elseif($jumlah_ratings == '5')
		{
			//dd("masuk else if 5");
			?>
			<img src='<?php echo url('images/stars-5.png'); ?>' style='margin-bottom:10px;'> Ratings
			<?php
		}
		else{
			echo "<br>No Rating for this Product";
		}
	}
	elseif($product_count)
	{
		$product_divide_count = $product_total_count / $product_count;
	}
	else
	{
		echo "<br>No Rating for this Product";
	}
?>
<br>
				<hr class="soft">

	<?php foreach ($product_details_by_id as $row) {

	}
     //echo $row->sold_status;

	?>
	<!-- //////////////////////////////////////////////////////////////// -->
	<?php if(empty($promopFlashId)){
		$url = 'addtocart';
	}else{
		$url = 'addtocart_flash';
	}

	?>
	{!! Form :: open(array('url' => $url,'class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}

	<div class="control-group">
	<?php
	if(empty($promopFlashId)){
		if($pro_details_by_id->pro_disprice != 0)
		{
		?>
			<label class="control-label" style="font-size:18px;width:auto;">
			<strike style="font-size:18px;color:#aaa;font-weight:bold;">
			{{$get_cur}}<?php echo number_format($pro_details_by_id->pro_price); ?>
			</strike>
			<span class="bold"></span>
			</label>
			<label class="control-label srt" style="">
			<span>
			{{$get_cur}}<b><?php echo number_format($pro_details_by_id->pro_disprice); ?></b>
			</span>
			</label>
		<?php
		}
		else
		{
		?>
			<label class="control-label srt" style="font-size:18px;width:auto;margin-left:-4%">
			{{$get_cur}}<?php echo number_format($pro_details_by_id->pro_price); ?>
			<span class="bold"></span>
			</label>
		<?php
		}
	}else{

		if($promo_flash_time_by_id[0]->promop_original_disprice != 0)
		{
		?>
			<label class="control-label" style="font-size:18px;width:auto;">
			<strike style="font-size:18px;color:#aaa;font-weight:bold;">
			{{$get_cur}}<?php echo number_format($promo_flash_time_by_id[0]->promop_original_disprice); ?>
			</strike>
			<span class="bold"></span>
			</label>
			<label class="control-label srt" style="">
			<span>
			{{$get_cur}}<b><?php echo number_format($promo_flash_time_by_id[0]->promop_discount_price); ?></b>
			</span>
			</label>
		<?php
		}
		else
		{
		?>
			<label class="control-label" style="font-size:18px;width:auto;">
			<strike style="font-size:18px;color:#aaa;font-weight:bold;">
			{{$get_cur}}<?php echo number_format($promo_flash_time_by_id[0]->promop_original_price); ?>
			</strike>
			<span class="bold"></span>
			</label>
			<label class="control-label srt" style="">
			<span>
			{{$get_cur}}<b><?php echo number_format($promo_flash_time_by_id[0]->promop_discount_price); ?></b>
			</span>
			</label>
		<?php
		}
	}
	?>

	<!-- <p style="font-size:17px;font-family:lato;margin-top:6px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Delivery within: <strong>{!!$pro_details_by_id->pro_delivery!!} days</strong></p> -->
	</div>
	<?php

	if($get_all_same_group != "")
	{
	?>
		<div class="control-group ">
		<label class="control-label" style="font-size:18px;">Pilih Warna: </label>

		<select class="span1" name="pro_color_cb" id="pro_color_cb" style="width:100px;" onchange="//ganti_warna(this.value)">

		<?php

		foreach ($get_all_same_group as $row) {
		?>
			<option <?php if($product_details_by_id[0]->co_name == $row->co_name ){ echo "selected";}?> value="<?php echo $row->pro_id;?>"><?php echo $row->co_name?></option>
		<?php
		}

		?>
		<!-- <option selected value="<?php echo $product_details_by_id[0]->pro_id;?>"><?php echo $product_details_by_id[0]->co_name;?></option> -->
		</select>
		</div>
	<?php
	}

	?>
	<div class="control-group" style="display:none;">
	<label class="control-label" style="font-size:18px;">Poin Product: <?php echo $product_details_by_id[0]->pro_poin; ?></label>


	</div>

	<?php if(empty($promopFlashId)){?>
		<h4><span style="color:green;"><?php echo  $pro_details_by_id->pro_qty - $pro_details_by_id->pro_no_of_purchase; ?> In Stock</span> </h4> <div id="size_color_error" style="color:#D4222A;font-weight:800;" ></div><hr class="soft" style="width:30%;"/>
	<?php } else { ?>
		<h4><span style="color:green;"><?php echo  $promo_flash_time_by_id[0]->promop_qty; ?> In Stock</span> </h4> <div id="size_color_error" style="color:#D4222A;font-weight:800;" ></div><hr class="soft" style="width:30%;"/>
	<?php }?>
	<div class="controls">
	<label class="control-label" style="font-size:18px;">Quantity: </label>
	<input type="number" name="addtocart_qty" id="addtocart_qty" class="span1" placeholder="Qty." value="1" min="1" <?php if(empty($promopFlashId)){ ;?> max="<?php echo $pro_details_by_id->pro_qty - $pro_details_by_id->pro_no_of_purchase; ?>" <?php }else{?> max = "<?php echo $promo_flash_time_by_id[0]->promop_qty; ?>" <?php }?> required/>
	<input type="hidden" name="addtocart_weight" id="addtocart_weight" value="<?php echo $pro_details_by_id->pro_weight ?>">
	<input type="hidden" name="addtocart_length" id="addtocart_length" value="<?php echo $pro_details_by_id->pro_length ?>">
	<input type="hidden" name="addtocart_width" id="addtocart_width" value="<?php echo $pro_details_by_id->pro_width ?>">
	<input type="hidden" name="addtocart_height" id="addtocart_height" value="<?php echo $pro_details_by_id->pro_height ?>">
	<?php if(!empty($promopFlashId)){ //dd($promo_flash_time_by_id[0]);?>
		<input type="hidden" name="addtocart_flashPromoId" id="addtocart_flashPromoId" value="<?php echo $promo_flash_time_by_id[0]->promop_schedp_id; ?>">
		<input type="hidden" name="addtocart_promop_pro_id" id="addtocart_promop_pro_id" value="<?php echo $promo_flash_time_by_id[0]->promop_pro_id; ?>">
	<?php } ?>
	<input type="hidden" name="addtocart_grade" id="addtocart_grade" value="<?php echo $pro_details_by_id->grade_id ?>">
	<input type="hidden" name="addtocart_color" id="addtocart_color" value="<?php echo $pro_details_by_id->co_id ?>">

	<span id="addtocart_qty_error" style="color:red;" ></span>
	<input type="hidden" name="addtocart_pro_id" value="<?php echo $pro_details_by_id->pro_id; ?>" />
	<input type="hidden" name="return_url" value="<?php echo $pro_details_by_id->mc_name.'/'.base64_encode(base64_encode(base64_encode($pro_details_by_id->pro_mc_id))); ?>" />
	<div class="container">
	<?php
	if(Session::has('customerid'))
	{?>
		@if($row->pro_no_of_purchase >= $row->pro_qty)
		<div id="buton-outofstock-login">
		<span style="" class="out-of-stock">Out Of Stock</span>
		</div>
		@else
		<div id="buton-cart-login">
		<button type="submit" class="btn btn-large btn-primary pull-right me_btn cart-res" style="" id="add_to_cart_session" formtarget="_self">
		<i class="icon-shopping-cart"></i>&nbsp;Add to cart</button>
		</div>
		@endif
	<?php } elseif (Session::has('customerid')=="")
	{

	?>
		@if($row->pro_no_of_purchase >= $row->pro_qty)
		<div id="buton-outofstock-login">
		<span style="" class="out-of-stock">Out Of Stock</span>
		</div>
		@else
		<div id="buton-cart">
		<a href="#login" id="cart-but" role="button" data-toggle="modal" target="_self">
		<button class="btn btn-large btn-primary pull-right me_btn cart-res" id="login_a_click" value="Login" formtarget="_self">
		<i class="icon-shopping-cart" aria-hidden="true" style="padding-top: 3px;"></i>&nbsp; Add to Cart</button>
		</a>
		</div>
		@endif
	<?php
	}
	?>
	</div>
	<div class="basic" data-average="<?php // echo round($get_pro_rating_avg[$pro_details_by_id->pro_id]); ?>" data-id="1" style="padding-top:10px;" ></div>
	<input type="hidden" name="rate_myproduct" id="tate" />
	</div>

	<input type="hidden" name="addtocart_type" value="product" />
	</form>
	<?php
	if(Session::has('customerid'))
	{?>
		<div style="display:none;">
		<a href="#login" role="button" data-toggle="modal" target="_self">
		<button class="wish-but" id="login_a_click"  value="Login">
		<i class="icon-heart" aria-hidden="true" style="padding-top: 3px;"></i> Add to Wishlist</button>
		</a>
		</div>
	<?php } elseif (Session::has('customerid')=="")
	{

	?>
		<div class="container">
		<div id="buton-wish">
		<a href="#login" role="button" data-toggle="modal" target="_self">
		<button class="wish-but" id="login_a_click"  value="Login" formtarget="_self">
		<i class="icon-heart" aria-hidden="true" style="padding-top: 3px;"></i> Add to Wishlist</button>
		</a>
		</div>
		</div>
	<?php
	}
	?>
	<?php
	if(Session::has('customerid'))
	{
	?>
		{!! Form :: open(array('url' => 'addtowish','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}
		<input type="hidden" name="pro_id" value="<?php echo  $pro_details_by_id->pro_id; ?>">
		<input type="hidden" name="cus_id" value="<?php echo Session::get('customerid');?>">
		<!-- <input type="submit" style="padding-right:0; float:right;margin-top:-82px;" name="submit" class="wish-but"> -->
		<div class="container">
		<div id="buton-wish-login">
		<button type="submit" class="wish-but-login" id="login_a_click" name="submit" formtarget="_self">
		<i class="icon-heart" aria-hidden="true" style="padding-top: 3px;"></i> Add to Wishlist</button>
		</div>
		</div>
		</form>
	<?php
	}?>
	<div class="container">
	<?php	if(Session::has('customerid')) { ?>
		<div class="grid-bung-login">
		<div class="additional-text right kotak">
		<div class="span4">
		<div class="features features-icon-hover indent first">
		<span class="icons i-grade icon-color-productview">
		<div id="icon-grade"><?php echo substr($pro_details_by_id->grade_name,6);?></div>
		</span>
		<div class="addt-heading"><?php echo $pro_details_by_id->grade_name;?></div>
		<p class="no-margin"><?php echo $pro_details_by_id->grade_keterangan; ?></p>
		</div>
		<div class="features features-icon-hover indent first">
		<span class="icons i-garansi-bw icon-color-productview"></span>
		<div class="addt-heading">Garansi Kualitas Produk</div>
		<p class="no-margin">Kukuruyuk.com menjamin produk yang kami tawarkan memiliki kualitas terbaik karena kami hanya menerima produk dengan kondisi baik dan kami akan memperbaiki produk-produk tersebut untuk mencapai kualitas yang terbaik.</p>
		</div>
		<div class="features features-icon-hover indent first">
		<a href="<?php echo url('contactus')?>" target="_blank"><span class="icons i-telephone icon-color-productview"></span></a>
		<div class="addt-heading">Online Costumer Service</div>
		<p class="no-margin">Bila Anda menemukan kesulitan dalam berbelanja, silahkan hubungi kami, tim kami siap membantu anda.</p>
		</div>
		</div>
		</div>
		</div>
	<?php } elseif(Session::has('customerid')==""){?>
		<div class="grid-bung">
		<div class="square-no">
		<div class="additional-text right kotak-had">
		<div class="span4">
		<div class="features features-icon-hover indent first">
		<span class="icons i-grade icon-color-productview">
		<div id="icon-grade"><?php echo substr($pro_details_by_id->grade_name,6);?></div>
		</span>
		<div class="addt-heading"><?php echo $pro_details_by_id->grade_name;?></div>
		<p class="no-margin"><?php echo $pro_details_by_id->grade_keterangan; ?></p>
		</div>
		<div class="features features-icon-hover indent first">
		<span class="icons i-garansi-bw icon-color-productview"></span>
		<div class="addt-heading">Garansi Kualitas Produk</div>
		<p class="no-margin">Kukuruyuk.com menjamin produk yang kami tawarkan memiliki kualitas terbaik karena kami hanya menerima produk dengan kondisi baik dan kami akan memperbaiki produk-produk tersebut untuk mencapai kualitas yang terbaik.</p>
		</div>
		<div class="features features-icon-hover indent first">
		<a href="<?php echo url('contactus')?>" target="_blank"><span class="icons i-telephone icon-color-productview"></span></a>
		<div class="addt-heading">Online Costumer Service</div>
		<p class="no-margin">Bila Anda menemukan kesulitan dalam berbelanja, silahkan hubungi kami, tim kami siap membantu anda.</p>
		</div>
		</div>
		</div>
		</div>
		</div>
	<?php } ?>
</div>

<hr class="soft clr" style="margin-top:-30px;"/>

</div>
<div class="span9 pull-left table-responsive tab-land-wid"><br>


<table class="table table-bordered" style="font-size:12px;">
<thead>
<tr class="techSpecRow" style="background: #1D84C1;color: white;">
<th width="50%">Product Description</th>
<th width="50%">Product Specification</th>
</tr>
</thead>
<tbody>
<tr>
<td><?php echo  $pro_details_by_id->pro_desc; ?></td>
<td>
<?php if($product_spec_details){ ?>
<table class="table table-bordered" style="font-size:12px;">
<thead>
<tr>
<th>Spec Name</th>
<th>Spec Value</th>
</tr>
</thead>
<tbody>
<?php foreach ($product_spec_details as $product_spec) {?>
<tr>
<td><?php echo $product_spec->sp_name; ?></td>
<td><?php echo $product_spec->spc_value; ?></td>
</tr>
<?php } ?>
</tbody>
</table>
<?php } ?>
</td>
</tr>
</tbody>
</table>

<table class="table table-bordered" style="display:none;">
<tbody>
<tr class="techSpecRow" style="background: #1D84C1;
color: white;"><th colspan="2">Store Details</th></tr>
<tr><td class="hide-mob"><div class="span3">
<h4></h4>
<div id="us3" style="width: 100% !important; height: 240px;margin-bottom:10px;"></div>
</div></td>
<td><div class="span4">
<?php
//  dd($get_store);
foreach($get_store as $storerow)
{
$store_name = $storerow->stor_name;

$store_address = $storerow->stor_address1;
$store_address2 = $storerow->stor_address2;

$store_zip = $storerow->stor_zipcode;
$store_phone = $storerow->stor_phone;
$store_web = $storerow->stor_website;
$store_lat = $storerow->stor_latitude;
$store_lan = $storerow->stor_longitude;
?>


<a title="View Store" target="_blank" href="<?php echo url('storeview/'.base64_encode(base64_encode(base64_encode($storerow->stor_id)))); ?>">
<h4>
<?php echo $store_name; ?></h4></a>
<?php echo $store_address; ?>.,<br>
<?php echo $store_address2; ?>.,<br>
<?php echo $store_zip; ?>,<br>
Mobile: <?php echo $store_phone; ?><br>

Website:<?php echo $store_web; ?>
<?php }

?>
</div></td>
</tr>
</tbody>
</table>



				<!-- <h4>Reviews & Ratings</h4>
				<div class="row">
				<div class="span6">
					<main>
  <section>
    <ul class="style-1">
      <li>
        <em>5 Star</em>
        <span>127</span>
      </li>
      <li>
        <em>4 Star</em>
        <span>98</span>
      </li>
      <li>
        <em>3 Star</em>
        <span>34</span>
      </li>
      <li>
        <em>2 Star</em>
        <span>148</span>
      </li>
      <li>
        <em>1 Star</em>
        <span>10</span>
      </li>
    </ul>
  </section>

</main>
				</div>

				</div> -->
				<?php

				if(Session::has('customerid'))
		{

			?>


<div style="border-radius:3px; border:1px solid #ccc;display:none">
<h4 style="padding-left:13px;">Write a post comments</h4>
                             	<div class="row"><div class="span5">

									 {!! Form::open(array('url'=>'productcomments','class'=>'form-horizontal loginFrm')) !!}
									 <input type="hidden" name="customer_id" value="<?php echo Session::get('customerid');?>">
									 <input type="hidden" name="mcid" id="mcid" value="<?php echo $mcid;?>">
									 <input type="hidden" name="scid" id="scid" value="<?php echo $scid;?>">
									 <input type="hidden" name="sbid" id="sbid" value="<?php echo $sbid;?>">
									 <input type="hidden" name="ssbid" id="ssbid" value="<?php echo $ssbid;?>">
                                            <input type="hidden" name="product_id" value="{!!$pro_details_by_id->pro_id!!}">

        <div class="form-group">
          <div class="control-group">

              <input type="text" placeholder="Enter Comment Title" name="title" class="input-xlarge" style="width:100%"required/>

          </div>
          </div>


          <div class="control-group">
              <textarea rows="5"  name="comments" class="input-xlarge" style="width:100%" placeholder="Enter Comments Queries" required></textarea>

          </div>
		  <div class="control-group">
 <input type="radio" name="ratings" value="1" required>1 Star
                                                            <input type="radio" name="ratings" value="2" required>2 Star
                                                            <input type="radio" name="ratings" value="3" required>3 Star
                                                            <input type="radio" name="ratings" value="4" required>4 Star
                                                            <input type="radio" name="ratings" value="5" required>5 Star<br><br>
															</div>
				<div class="control-group">
    		<input type="submit" class="btn btn-large me_btn btnb-success" value="Post Comments" style="height:auto;background: #2F3234;
    										border-radius: 0px;" formtarget="_self"></div>



      </form>

	  </div></div></div>
	  <br class="clr">
	  <!-- start tampil komentar kalo udah login -->
									<h4>Reviews</h4>
<?php
if($product_count>=1)
{

											foreach($customer_details as $col)
			{
				$customer_name = $col->cus_name;
				$customer_mail = $col->cus_email;
				$customer_img = $col->cus_pic;
				$customer_comments = $col->comments;
				$customer_date = $col->created_date;
				$customer_product = $col->product_id;
				$change_format = date('d/m/Y', strtotime($col->created_date) );
				$customer_title = $col->title;
				$customer_name_arr = str_split($customer_name);
				$start_letter = $customer_name_arr[0];
				$customer_ratings = $col->ratings;
				$date_exp=explode('/',$change_format);
				$date_date = $date_exp[0];
				$date_month = $date_exp[1];
				$date_year = $date_exp[2];
			}

			if($customer_product==$pro_details_by_id->pro_id)
											{
			?>
			<div class="commentlist">
			<?php if($start_letter =='a')
			{
				echo "<div class='userimg'><span class='reviewer-imgName' style='background:#fba565; text-transform:capitalize;'>$customer_name_arr[0]</span>";
			}
			elseif($start_letter=='b')
			{
				echo "<div class='userimg'><span class='reviewer-imgName' style='background:#fba565; text-transform:capitalize;'>$customer_name_arr[0]</span>";
			}
			elseif($start_letter=='c')
			{
				echo "<div class='userimg'><span class='reviewer-imgName' style='background:#fba565; text-transform:capitalize;'>$customer_name_arr[0]</span>";
			}
			elseif($start_letter=='d')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='e')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='f')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='g')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='h')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='i')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='j')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='k')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='l')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='m')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='n')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='o')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='p')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='q')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='r')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='s')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='t')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='u')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='v')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='w')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='x')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='y')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='z')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			else{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			?>

                        <center><span class="_reviewUserName" title="Prateek" style="text-transform:capitalize;"><?php echo $customer_name; ?></span></center>

                        </div>
                        <div class="text">
                        <div class="user-review">
						<?php
						if($date_month=='01')
						{
							$month = 'Jan';
						}
						elseif($date_month=='02')
						{
							$month = 'Feb';
						}
						elseif($date_month=='03')
						{
							$month = 'March';
						}
						elseif($date_month=='04')
						{
							$month = 'April';
						}
						elseif($date_month=='05')
						{
							$month = 'May';
						}
						elseif($date_month=='06')
						{
							$month = 'June';
						}
						elseif($date_month=='07')
						{
							$month = 'July';
						}
						elseif($date_month=='08')
						{
							$month = 'Agu';
						}
						elseif($date_month=='09')
						{
							$month = 'Sep';
						}
						elseif($date_month=='10')
						{
							$month = 'Oct';
						}
						elseif($date_month=='11')
						{
							$month = 'Nov';
						}
						elseif($date_month=='12')
						{
							$month = 'Dec';
						}
						else{

						}

						?>
                            <div class="date LTgray"><b><?php echo $month; ?> - <?php echo $date_date; ?> - <?php echo $date_year; ?></b></div>
							<?php
							if($customer_ratings=='1')
							{
								?>
								<img src='<?php echo url('images/stars-1.png'); ?>' style='margin-bottom:10px;'>Ratings


								<?php
							}
							elseif($customer_ratings=='2')
							{
								?>
								<img src='<?php echo url('images/stars-2.png'); ?>' style='margin-bottom:10px;'>Ratings
								<?php
							}
							elseif($customer_ratings=='3')
							{
								?>
								<img src='<?php echo url('images/stars-3.png'); ?>' style='margin-bottom:10px;'>Ratings
								<?php
							}
							elseif($customer_ratings=='4')
							{
								?>
								<img src='<?php echo url('images/stars-4.png'); ?>' style='margin-bottom:10px;'>Ratings
								<?php
							}
							elseif($customer_ratings=='5')
							{
								?>
								<img src='<?php echo url('images/stars-5.png'); ?>' style='margin-bottom:10px;'>Ratings
								<?php
							}
							else {}
							?>

                            <div class="head"><?php echo $customer_title; ?></div>

<span><?php echo $customer_comments; ?></span></p>
                        </div>

                    </div>
			</div></div>
<?php } }else{?>No Review Ratings.<br>
<!-- end tampil komentar kalo udah login -->

<?php
				 if(Session::has('customerid'))
		{?>
				<div style="display:none;"><a href="#login" class="btn btn-orange  btn-line rippleWhite js-userReviewed" role="button" data-toggle="modal" style="padding-right:0;" id="login_a_click"  value="Login">Write a Review Post</a></div>
		<?php } elseif (Session::has('customerid')=="")
		{

			?>
			<a href="#login" role="button" data-toggle="modal" style="padding-right:0; float:left;" id="login_a_click"  class="btn btn-orange  btn-line rippleWhite js-userReviewed" value="Login">Write a Review Post</a>
			<?php
		}
		?>
<?php }?>
<br>
                                    <?php } else {
			?>
		<!-- <h4>Reviews</h4><h4><span style="margin-top:-33px; float:right;"><a href="#login" role="button" data-toggle="modal" id="login_a_click"  value="Login" class="btn btn-orange  btn-line rippleWhite js-userReviewed">Write a Review Post</a></span></h4> -->
<?php
if($product_count>=1)
{

											foreach($customer_details as $col)
			{
				$customer_name = $col->cus_name;
				$customer_mail = $col->cus_email;
				$customer_img = $col->cus_pic;
				$customer_comments = $col->comments;
				$customer_date = $col->created_date;
				$customer_product = $col->product_id;
				$change_format = date('d/m/Y', strtotime($col->created_date) );
				$customer_title = $col->title;
				$customer_name_arr = str_split($customer_name);
				$start_letter = $customer_name_arr[0];
				$customer_ratings = $col->ratings;
				$date_exp=explode('/',$change_format);
				$date_date = $date_exp[0];
				$date_month = $date_exp[1];
				$date_year = $date_exp[2];
			if($customer_product==$pro_details_by_id->pro_id)
											{
			?>
			<div class="commentlist">
			<?php if($start_letter =='a')
			{
				echo "<div class='userimg'><span class='reviewer-imgName' style='background:#fba565; text-transform:capitalize;'>$customer_name_arr[0]</span>";
			}
			elseif($start_letter=='b')
			{
				echo "<div class='userimg'><span class='reviewer-imgName' style='background:#fba565; text-transform:capitalize;'>$customer_name_arr[0]</span>";
			}
			elseif($start_letter=='c')
			{
				echo "<div class='userimg'><span class='reviewer-imgName' style='background:#fba565; text-transform:capitalize;'>$customer_name_arr[0]</span>";
			}
			elseif($start_letter=='d')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='e')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='f')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='g')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='h')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='i')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='j')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='k')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='l')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='m')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='n')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='o')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='p')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='q')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='r')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='s')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='t')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='u')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='v')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='w')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='x')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='y')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			elseif($start_letter=='z')
			{
				echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			else{
					echo "<div class='userimg'><center><span class='reviewer-imgName' style='background:#191d86; text-transform:capitalize;'>$customer_name_arr[0]</center></span>";
			}
			?>

                        <center><span class="_reviewUserName" title="Prateek" style="text-transform:capitalize;"><?php echo $customer_name; ?></span></center>

                        </div>
                        <div class="text">
                        <div class="user-review">
						<?php
						if($date_month=='01')
						{
							$month = 'Jan';
						}
						elseif($date_month=='02')
						{
							$month = 'Feb';
						}
						elseif($date_month=='03')
						{
							$month = 'March';
						}
						elseif($date_month=='04')
						{
							$month = 'April';
						}
						elseif($date_month=='05')
						{
							$month = 'May';
						}
						elseif($date_month=='06')
						{
							$month = 'June';
						}
						elseif($date_month=='07')
						{
							$month = 'July';
						}
						elseif($date_month=='08')
						{
							$month = 'Agu';
						}
						elseif($date_month=='09')
						{
							$month = 'Sep';
						}
						elseif($date_month=='10')
						{
							$month = 'Oct';
						}
						elseif($date_month=='11')
						{
							$month = 'Nov';
						}
						elseif($date_month=='12')
						{
							$month = 'Dec';
						}
						else{

						}

						?>
                            <div class="date LTgray"><b><?php echo $month; ?> - <?php echo $date_date; ?> - <?php echo $date_year; ?></b></div>
							<?php
							if($customer_ratings=='1')
							{
								?>
								<img src='<?php echo url('images/stars-1.png'); ?>' style='margin-bottom:10px;'>Ratings


								<?php
							}
							elseif($customer_ratings=='2')
							{
								?>
								<img src='<?php echo url('images/stars-2.png'); ?>' style='margin-bottom:10px;'> Ratings
								<?php
							}
							elseif($customer_ratings=='3')
							{
								?>
								<img src='<?php echo url('images/stars-3.png'); ?>' style='margin-bottom:10px;'> Ratings
								<?php
							}
							elseif($customer_ratings=='4')
							{
								?>
								<img src='<?php echo url('images/stars-4.png'); ?>' style='margin-bottom:10px;'> Ratings
								<?php
							}
							elseif($customer_ratings=='5')
							{
								?>
								<img src='<?php echo url('images/stars-5.png'); ?>' style='margin-bottom:10px;'> Ratings
								<?php
							}
							else {}
							?>

                            <div class="head"><?php echo $customer_title; ?></div>

<span><?php echo $customer_comments; ?></span></p>
                        </div>

                    </div>
			</div>
<?php } } }else{?>No Review Ratings.<br>

<?php }?>
<br>
									<?php } ?>

</div>




</div>
</div>
</div>
</div>
</div>
<!-- MainBody End ============================= -->

<!-- Footer ================================================================== -->
{!! $footer !!}
<!-- Footer Ends here ================================================================== -->
<!-- Placed at the end of the document so the pages load faster ============================================= -->

<script type="text/javascript">
$(document).ready(function(){

$('#add_to_cart_session').click(function(){
	var pro_purchase1 = '<?php echo $pro_details_by_id->pro_no_of_purchase; ?>' ;
	var pro_purchase = parseInt($('#addtocart_qty').val()) + parseInt(pro_purchase1);
	var pro_qty = '<?php echo $pro_details_by_id->pro_qty; ?>';
	if(pro_purchase > parseInt(pro_qty))
	{
		$('#addtocart_qty').focus();
		$('#addtocart_qty').css('border-color', 'red');
		$('#addtocart_qty_error').html('Limited Quantity Available');
		return false;
	}
	else
	{
		$('#addtocart_qty').css('border-color', '');
		$('#addtocart_qty_error').html('');
	}
	if($('#addtocart_color').val() ==0)
	{
		$('#addtocart_color').focus();
		$('#addtocart_color').css('border-color', 'red');
		$('#size_color_error').html('Select Color');
		return false;
	}
	else
	{
		$('#addtocart_color').css('border-color', '');
		$('#size_color_error').html('');
	}
	if($('#addtocart_size').val() ==0)
	{
		$('#addtocart_size').focus();
		$('#addtocart_size').css('border-color', 'red');
		$('#size_color_error').html('Select Size');
		return false;
	}
	else
	{
		$('#addtocart_size').css('border-color', '');
		$('#size_color_error').html('');
	}
});
// $("#searchbox").keyup(function(e)
// {
//
// var searchbox = $(this).val();
// var dataString = 'searchword='+ searchbox;
// if(searchbox=='')
// {
// $("#display").html("").hide();
// }
// else
// {
// 	var passData = 'searchword='+ searchbox;
// 	 $.ajax( {
// 			      type: 'GET',
// 				  data: passData,
// 				  url: '<?php //echo url('autosearch'); ?>',
// 				  success: function(responseText){
// 				  $("#display").html(responseText).show();
// }
// });
// }
// return false;
//
// });
});


</script>

<script type="text/javascript">
	// function ganti_warna(id)
	// {
	// 	//alert(id);
	// 	console.log("fungsi ganti warna terpanggil")
	// 	$.ajax({
	// 		method: "post",
	// 		url: "pilih_produk_warna_cb",
	// 		datatype: "json",
	// 		data: {
	// 			aId: id,
	// 		},
	// 		success: function(d){
	// 			alert(d);
	// 		}
	// 	});
	// 	console.log("fungsi ajax selesai");
	// }

	$("#pro_color_cb").on("change",function(){
		var id = $("#pro_color_cb").val();
		//alert(id);

		$.ajax({
			method: "get",
			url: "<?php echo url('pilih_produk_warna_cb'); ?>",
			datatype: "json",
			data: {
				aId: id,
			},
			success: function(d){

				window.location="<?php echo URL::to("");?>"+d;
			}
		});

	});

	$('img').bind('contextmenu', function(e) {
	    return false;
	});

</script>

<script src="<?php echo url(); ?>/themes/js/magiczoomplus.js" type="text/javascript"></script>
<script type="text/javascript">
	function setBarWidth(dataElement, barElement, cssProperty, barPercent) {
  var listData = [];
  $(dataElement).each(function() {
    listData.push($(this).html());
  });
  var listMax = Math.max.apply(Math, listData);
  $(barElement).each(function(index) {
    $(this).css(cssProperty, (listData[index] / listMax) * barPercent + "%");
  });
}
setBarWidth(".style-1 span", ".style-1 em", "width", 100);
setBarWidth(".style-2 span", ".style-2 span", "width", 55);


</script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDoY1OPjAqu1PlQhH3UljYsfw-81bLkI&libraries=places&signed_in=true"></script>
    <script src="<?php echo url(); ?>/assets/js/locationpicker.jquery.js"></script>
        <script>$('#us3').locationpicker({

        location: {latitude: <?php echo $store_lat; ?>, longitude: <?php echo $store_lan; ?>},

        radius: 200,
        inputBinding: {
            latitudeInput: $('#us3-lat'),
            longitudeInput: $('#us3-lon'),
            radiusInput: $('#us3-radius'),
            locationNameInput: $('#us3-address')
        },
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    });
        </script>

</body>
</html>
