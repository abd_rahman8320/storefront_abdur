<style media="screen">
.form-horizontal .form-group:before,
.form-horizontal .form-group:after{
    display: table !important;
    content: " ";
}
.form-horizontal .form-group:after{
    clear: both !important;
}
.form-control1 {
  display: block !important;
  width: 100% !important;
  height: 34px !important;
  padding: 6px 12px !important;
  font-size: 14px !important;
  line-height: 1.42857143 !important;
  color: #555 !important;
  background-color: #fff!important;
  background-image: none !important;
  border: 1px solid #ccc;
  border-radius: 4px !important;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s !important;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
}
.form-control1:focus {
  border-color: #66afe9;
  outline: 0 !important;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6) !important;
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6) !important;
}
.form-control1::-moz-placeholder {
  color: #999 !important;
  opacity: 1 !important;
}
.form-control1:-ms-input-placeholder {
  color: #999 !important;
}
.form-control1::-webkit-input-placeholder {
  color: #999 !important;
}
.form-control1::-ms-expand {
  background-color: transparent !important;
  border: 0 !important;
}
.form-control1[disabled],
.form-control1[readonly],
fieldset[disabled] .form-control1 {
  background-color: #eee !important;
  opacity: 1 !important;
}
.form-control1[disabled],
fieldset[disabled] .form-control1 {
  cursor: not-allowed !important;
}
textarea.form-control1 {
  height: auto !important;
}
.form-group {
  margin-bottom: 15px !important;
}
.form-horizontal .radio,
.form-horizontal .checkbox,
.form-horizontal .radio-inline,
.form-horizontal .checkbox-inline {
  padding-top: 7px !important;
  margin-top: 0 !important;
  margin-bottom: 0 !important;
}
.form-horizontal .radio,
.form-horizontal .checkbox {
  min-height: 27px !important;
}
.form-horizontal .form-group {
  margin-right: -15px !important;
  margin-left: -15px !important;
}
@media (min-width: 768px) {
  .form-horizontal .control-label {
    padding-top: 7px !important;
    margin-bottom: 0 !important;
    text-align: right !important;
  }
}
.form-horizontal .has-feedback .form-control1-feedback {
  right: 15px !important;
}
@media (min-width: 768px) {
  .form-horizontal .form-group-lg .control-label {
    padding-top: 11px !important;
    font-size: 18px !important;
  }
}
@media (min-width: 768px) {
  .form-horizontal .form-group-sm .control-label {
    padding-top: 6px !important;
    font-size: 12px !important;
  }
}
.loader {
  border: 2px solid #f3f3f3;
  border-radius: 50%;
  border-top: 2px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 0.5s linear infinite; /* Safari */
  animation: spin 0.5s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<section class="center slider" style="margin:0px 0px 0px 10px;padding-right:30px;" >
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#voucher" id="mobile_button">Pulsa dan Paket Data</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#voucher" id="electricity_button" onclick="get_product('type=electricity')">Token Listrik</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#tagihan" id="electricity_postpaid_button" onclick="get_product_tagihan('type=electricity_postpaid')">Tagihan Listrik</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#tagihan" id="bpjs_button" onclick="get_product_tagihan('type=bpjs_kesehatan')">BPJS Kesehatan</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#voucher" id="game_button" onclick="get_product('type=game')">Voucher Game</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#tagihan" id="multi_button" onclick="get_product_tagihan('type=multi')">Angsuran Kredit</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#tagihan" id="telkom_button" onclick="get_product_tagihan('type=telkom_postpaid')">Tagihan Telkom</button>
    </div>
    <div>
        <button class="button" type="button" style="width:200px" data-toggle="modal" data-target="#tagihan" id="pdam_button" onclick="get_product_tagihan('type=pdam')">Air PDAM</button>
    </div>
</section>
<input type="hidden" name="session" id="session" value="{{$customerid}}">
<div id="voucher" class="modal fade" role="dialog" aria-hidden="true" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                <h3 id="mobile">PULSA DAN PAKET DATA</h3>
                <h3 id="electricity">TOKEN LISTRIK</h3>
                <h3 id="game">VOUCHER GAME</h3>
            </div>

            <div class="modal-body">

                <form class="form-horizontal" action="{{url('sepulsa_checkout_process')}}" method="post">
                    <div class="form-group">
                        <label class="control-label span2" title="Nomor Telepon" id="mobile_label">Nomor Telepon</label>
                        <label class="control-label span2" title="Nomor yang tertera pada kartu pelanggan Anda" id="electricity_label">Nomor Pelanggan</label>
                        <label class="control-label span2" title="Nomor Telepon" id="game_label">Nomor Telepon</label>
                        <div class="span3">
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Contoh: 081234567890" id="mobile_input" title="Nomor Telepon">
                            <input class="form-control1" type="text" name="meter_number" value="" placeholder="Contoh: 1122334455" id="electricity_input" title="Nomor yang tertera pada kartu pelanggan Anda">
                            <p id="error_msg1" style="display:none;color:red;"></p>
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Contoh: 081234567890" id="game_input" title="Nomor Telepon">
                        </div>
                    </div>
                    <div class="form-group" id="electricity_div">
                        <label class="control-label span2" title="Nomor Telepon untuk notifikasi SMS" id="electricity_label2">Nomor Telepon</label>
                        <div class="span3">
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Contoh: 081234567890" id="electricity_input2" title="Nomor Telepon untuk notifikasi SMS">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label span2">Product</label>
                        <div class="span3">
                            <select class="form-control1" name="pro_id" id="product_sepulsa">
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="control-label span2">Harga</label>
                        <div class="span3">
                            <b id="harga_sepulsa">Rp 0</b>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label span2">Payment Method</label>
                        <div class="span3">
                            <select class="form-control1" name="payment_type" id="payment_type">
                                @foreach($get_payment_method as $payment)
                                <option value="{{$payment->kode_pay_detail}}" nilai="{{$payment->channel_pay_header}}">{{$payment->nama_pay_header}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="payment_channel" id="payment_channel" value="{{$get_payment_method[0]->channel_pay_header}}">
                        </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-success btn-sm btn-grad" type="submit" name="submit" id="voucher_submit">Place Order</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="tagihan" class="modal fade" role="dialog" aria-hidden="true" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                <h3 id="electricity_postpaid">TAGIHAN LISTRIK</h3>
                <h3 id="bpjs_kesehatan">BPJS KESEHATAN</h3>
                <h3 id="multifinance">ANGSURAN KREDIT</h3>
                <h3 id="telkom">TAGIHAN TELKOM</h3>
                <h3 id="pdam">AIR PDAM</h3>
                <div class="" style="color:red;display:none;" id="not_active_message">
                </div>
            </div>

            <div class="modal-body">

                <form class="form-horizontal" action="{{url('sepulsa_checkout_process')}}" method="post">
                    <div class="form-group">
                        <label class="control-label span2" title="Nomor yang tertera pada kartu pelanggan Anda" id="electricity_postpaid_label">Nomor Pelanggan</label>
                        <label class="control-label span2" title="Nomor virtual account keluarga Anda" id="bpjs_label">Nomor Kepesertaan BPJS</label>
                        <label class="control-label span2" title="Nomor Kontrak atau Nomor Perjanjian Anda" id="multi_label">Nomor Kontrak</label>
                        <label class="control-label span2" title="Nomor Telepon" id="telkom_label">Nomor Telepon</label>
                        <label class="control-label span2" title="Nomor yang tertera pada kartu pelanggan Anda" id="pdam_label">Nomor Pelanggan</label>
                        <div class="span3">
                            <input class="form-control1" type="text" name="meter_number" value="" placeholder="Contoh: 1122334455" id="electricity_postpaid_input" title="Meter Number adalah nomor yang tertera pada kartu pelanggan">
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Contoh: 0000001234567890" id="bpjs_input" title="Nomor virtual account keluarga Anda">
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Masukkan Nomor Kontrak" id="multi_input" title="Nomor Kontrak atau Nomor Perjanjian Anda">
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Contoh: 0211234567" id="telkom_input" title="Nomor Telepon">
                            <input class="form-control1" type="text" name="customer_number" value="" placeholder="Contoh: 1234567890" id="pdam_input" title="Nomor yang tertera pada kartu pelanggan Anda">
                            <input type="hidden" name="pro_id" value="" id="product_id">
                            <p id="error_msg2" style="display:none;color:red;"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label span2" title="Bayar Untuk" id="bpjs_label2">Bayar Untuk</label>
                        <label class="control-label span2" title="Operator" id="pdam_label2">Operator</label>
                        <div class="span3">
                            <div class="" id="bpjs_input2">
                                <select class="form-control1" name="payment_period" id="payment_period">
                                    <option value="01">1 Bulan</option>
                                    <option value="02">2 Bulan</option>
                                    <option value="03">3 Bulan</option>
                                    <option value="04">4 Bulan</option>
                                    <option value="05">5 Bulan</option>
                                    <option value="06">6 Bulan</option>
                                    <option value="07">7 Bulan</option>
                                    <option value="08">8 Bulan</option>
                                    <option value="09">9 Bulan</option>
                                    <option value="10">10 Bulan</option>
                                    <option value="11">11 Bulan</option>
                                    <option value="12">12 Bulan</option>
                                </select>
                            </div>
                            <input class="form-control1" type="text" name="pdam_input2" value="" placeholder="pdam palyja" id="pdam_input2">
                            <input class="form-control1" type="hidden" name="operator_code" value="" id="pdam_input3">
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="control-label span2">Biaya Premi</label>
                        <div class="span3">
                            <b id="premi">Rp 0</b>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label span2">Biaya Admin</label>
                        <div class="span3">
                            <b id="biaya_admin">Rp 0</b>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label span2">Total</label>
                        <div class="span3">
                            <b id="total">Rp 0</b>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label span2">Payment Method</label>
                        <div class="span3">
                            <select class="form-control1" name="payment_type" id="payment_type">
                                @foreach($get_payment_method as $payment)
                                <option value="{{$payment->kode_pay_detail}}" nilai="{{$payment->channel_pay_header}}">{{$payment->nama_pay_header}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="payment_channel" id="payment_channel" value="{{$get_payment_method[0]->channel_pay_header}}">
                        </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-info btn-sm btn-grad" type="button" name="button" id="tagihan_button" onclick="get_tagihan()">Cek Tagihan</button>
                <div class="loader" style="display:none;float:right;" id="loading"></div>
                <button class="btn btn-success btn-sm btn-grad" type="submit" name="submit" id="tagihan_submit" style="display:none;" disabled>Bayar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function number_format(number,decimals,dec_point,thousands_sep) {
        number  = number*1;//makes sure `number` is numeric value
        var str = number.toFixed(decimals?decimals:0).toString().split('.');
        var parts = [];
        for ( var i=str[0].length; i>0; i-=3 ) {
            parts.unshift(str[0].substring(Math.max(0,i-3),i));
        }
        str[0] = parts.join(thousands_sep?thousands_sep:',');
        return str.join(dec_point?dec_point:'.');
    }
    function get_product(data)
    {
        $.ajax({
            type:'get',
            data:data,
            url: '{{url("sepulsa_product_details")}}',
            success:function(e)
            {
                $('#product_sepulsa').empty();
                for (var i = 0; i < e.length; i++) {
                    if (e[i].pro_disprice == 0) {
                        var price = number_format(e[i].pro_price,0,'.',',');
                    }else {
                        var price = number_format(e[i].pro_disprice,0,'.',',');
                    }
                    if (i == 0) {
                        $('#harga_sepulsa').html('Rp '+price);
                    }
                    $('#product_sepulsa').append('<option value="'+e[i].pro_id+'" harga="'+price+'">'+e[i].pro_title+'</option>')
                }

            }
        });
    }
    function get_product_tagihan(data)
    {
        $.ajax({
            type:'get',
            data:data,
            url: '{{url("sepulsa_product_details")}}',
            success:function(e)
            {
                $('#product_id').val('');
                if (e.length > 0) {
                    for (var i = 0; i < e.length; i++) {
                        $('#product_id').val(e[i].pro_id);
                    }
                }else {
                    $('#not_active_message').html('Maaf, untuk sementara fitur ini tidak tersedia');
                    $('#not_active_message').show();
                }
            }
        });
    }
    function get_tagihan()
    {
        $('#tagihan_button').hide();
        $('#loading').show();
        var input1 = '';
        var input2 = '';
        var input3 = $('#product_id').val();
        if ($('#electricity_postpaid_input').is(':visible')) {
            input1 = $('#electricity_postpaid_input').val();
        }else if ($('#bpjs_input').is(':visible')) {
            input1 = $('#bpjs_input').val();
            input2 = $('#payment_period').val();
        }else if ($('#multi_input').is(':visible')) {
            input1 = $('#multi_input').val();
        }else if ($('#telkom_input').is(':visible')) {
            input1 = $('#telkom_input').val();
        }else if ($('#pdam_input').is(':visible')) {
            input1 = $('#pdam_input').val();
            input2 = $('#pdam_input3').val();
        }
        $.ajax({
            url:'{{url("inquire_sepulsa")}}',
            method:'get',
            data:'input1='+input1+'&input2='+input2+'&input3='+input3,
            success:function(dataSuccess){
                if (dataSuccess['data_inquire'] != undefined) {
                    if (dataSuccess['data_inquire']['status'] == true) {
                        var premi = parseFloat(dataSuccess['tagihan']);
                        $('#premi').html('Rp '+number_format(premi,0,'.',','));
                        $('#biaya_admin').html('Rp '+number_format(dataSuccess['data_product'].pro_price,0,'.',','));
                        var total = premi + parseFloat(dataSuccess['data_product'].pro_price);
                        $('#total').html('Rp '+number_format(total,0,'.',','));
                        $('#tagihan_button').hide();
                        $('#loading').hide();
                        $('#tagihan_submit').show();
                        $('#tagihan_submit').removeAttr('disabled');
                        $('#error_msg2').hide();
                    }else {
                        $('#error_msg2').show();
                        $('#error_msg2').html(dataSuccess['data_inquire']['message']);
                        $('#tagihan_button').show();
                        $('#loading').hide();
                        $('#tagihan_submit').hide();
                    }
                }else {
                    $('#tagihan_button').show();
                    $('#loading').hide();
                    $('#tagihan_submit').hide();
                }


            },
            error:function(){
                alert('Terjadi Sebuah Kesalahan. Silahkan Coba Lagi!');
                $('#tagihan_button').show();
                $('#loading').hide();
            }
        });
    }
    $('#pdam_input2').autocomplete({
        source:function(request, response){
            $.ajax({
                url:'{{url("auto_get_pdam_operators")}}',
                method:'get',
                data:{
                    product_id:'87'
                },
                success:function(dataSuccess){
                    response(dataSuccess);
                }
            });
        },
        minLength:0,
        focus:function(event, ui){
            $('#pdam_input2').val(ui.item.label);
            return false;
        },
        select:function(event, ui){
            $('#pdam_input2').val(ui.item.label);
            $('#pdam_input3').val(ui.item.value);
        },
        change:function(event, ui){
            if (ui.item===null) {
                $('#pdam_input2').val('');
                $('#pdam_input3').val('');
                alert('Silahkan pilih operator yang tersedia');
            }
        }
    }).bind('focus', function(){
        $(this).autocomplete("search");
    });
    $('#electricity_input').on('blur', function(){
        var data = 'pro_sepulsa_id='+$('#product_sepulsa').val()+'&customer_number='+$('#electricity_input').val();
        $.ajax({
            type:'get',
            data:data,
            url: '{{url("inquire_sepulsa")}}',
            success:function(e)
            {
                if (e.status == true) {
                    $('#error_msg1').hide();
                }else if (e.status == false) {
                    $('#error_msg1').show();
                    $('#error_msg1').html(e.message);
                }
            },
            error:function()
            {
                alert('Connection Error');
            }
        });
    });
    $('#mobile_input').on('keypress', function(eventObject){
        if (eventObject.which == 8 || eventObject.which == 46) {
            var phone = $('#mobile_input').val().slice(0, -1);
        }else {
            var phone = $('#mobile_input').val() + eventObject.key;
        }
        if (phone.length == 4) {
            var str = phone.match(/.{1,4}/g);
            var data = 'type=mobile&number='+str[0];
            get_product(data);
        }else if (phone.length < 4) {
            $('#product_sepulsa').empty();
        }
    });
    $('#payment_type').on('change', function(){
        var channel = this.options[this.selectedIndex].getAttribute('nilai');
        $('#payment_channel').val(channel);
    });
    $('#product_sepulsa').on('change', function(){
        var harga = this.options[this.selectedIndex].getAttribute('harga');
        $('#harga_sepulsa').html('Rp '+harga);
    });
    $('#mobile_button').on('click', function(){
        $('#error_msg1').hide();
        $('not_active_message').hide();
        $('#mobile').show();
        $('#mobile_input').attr('type', 'text');
        $('#mobile_input').removeAttr('disabled');
        $('#mobile_label').show();
        $('#electricity').hide();
        $('#electricity_label').hide();
        $('#electricity_label2').hide();
        $('#electricity_input').attr('type', 'hidden');
        $('#electricity_input').attr('disabled', 'disabled');
        $('#electricity_input2').attr('type', 'hidden');
        $('#electricity_input2').attr('disabled', 'disabled');
        $('#electricity_div').hide();
        $('#game').hide();
        $('#game_label').hide();
        $('#game_input').attr('type', 'hidden');
        $('#game_input').attr('disabled', 'disabled');
    });
    $('#electricity_button').on('click', function(){
        $('#error_msg1').hide();
        $('not_active_message').hide();
        $('#mobile').hide();
        $('#mobile_input').attr('type', 'hidden');
        $('#mobile_input').attr('disabled', 'disabled');
        $('#mobile_label').hide();
        $('#electricity').show();
        $('#electricity_label').show();
        $('#electricity_label2').show();
        $('#electricity_input').attr('type', 'text');
        $('#electricity_input').removeAttr('disabled');
        $('#electricity_input2').attr('type', 'text');
        $('#electricity_input2').removeAttr('disabled');
        $('#electricity_div').show();
        $('#game').hide();
        $('#game_label').hide();
        $('#game_input').attr('type', 'hidden');
        $('#game_input').attr('disabled', 'disabled');
    });
    $('#game_button').on('click', function(){
        $('#error_msg1').hide();
        $('not_active_message').hide();
        $('#mobile').hide();
        $('#mobile_input').attr('type', 'hidden');
        $('#mobile_input').attr('disabled', 'disabled');
        $('#mobile_label').hide();
        $('#electricity').hide();
        $('#electricity_label').hide();
        $('#electricity_label2').hide();
        $('#electricity_input').attr('type', 'hidden');
        $('#electricity_input').attr('disabled', 'disabled');
        $('#electricity_input2').attr('type', 'hidden');
        $('#electricity_input2').attr('disabled', 'disabled');
        $('#electricity_div').hide();
        $('#game').show();
        $('#game_label').show();
        $('#game_input').attr('type', 'text');
        $('#game_input').removeAttr('disabled');
    });
    $('#electricity_postpaid_button').on('click', function(){
        $('#error_msg2').hide();
        $('not_active_message').hide();
        $('#electricity_postpaid').show();
        $('#electricity_postpaid_label').show();
        $('#electricity_postpaid_input').attr('type', 'text');
        $('#electricity_postpaid_input').removeAttr('disabled');
        $('#bpjs_kesehatan').hide();
        $('#bpjs_label').hide();
        $('#bpjs_label2').hide();
        $('#bpjs_input').attr('type', 'hidden');
        $('#bpjs_input').attr('disabled', 'disabled');
        $('#bpjs_input2').hide();
        $('#bpjs_input2').attr('disabled', 'disabled');
        $('#multifinance').hide();
        $('#multi_label').hide();
        $('#multi_input').attr('type', 'hidden');
        $('#multi_input').attr('disabled', 'disabled');
        $('#telkom').hide();
        $('#telkom_label').hide();
        $('#telkom_input').attr('type', 'hidden');
        $('#telkom_input').attr('disabled', 'disabled');
        $('#pdam').hide();
        $('#pdam_label').hide();
        $('#pdam_label2').hide();
        $('#pdam_input').attr('type', 'hidden');
        $('#pdam_input').attr('disabled', 'disabled');
        $('#pdam_input2').attr('type', 'hidden');
        $('#pdam_input2').attr('disabled', 'disabled');
        $('#pdam_input3').attr('disabled', 'disabled');
    });
    $('#bpjs_button').on('click', function(){
        $('#error_msg2').hide();
        $('not_active_message').hide();
        $('#electricity_postpaid').hide();
        $('#electricity_postpaid_label').hide();
        $('#electricity_postpaid_input').attr('type', 'hidden');
        $('#electricity_postpaid_input').attr('disabled', 'disabled');
        $('#bpjs_kesehatan').show();
        $('#bpjs_label').show();
        $('#bpjs_label2').show();
        $('#bpjs_input').attr('type', 'text');
        $('#bpjs_input').removeAttr('disabled');
        $('#bpjs_input2').show();
        $('#bpjs_input2').removeAttr('disabled');
        $('#multifinance').hide();
        $('#multi_label').hide();
        $('#multi_input').attr('type', 'hidden');
        $('#multi_input').attr('disabled', 'disabled');
        $('#telkom').hide();
        $('#telkom_label').hide();
        $('#telkom_input').attr('type', 'hidden');
        $('#telkom_input').attr('disabled', 'disabled');
        $('#pdam').hide();
        $('#pdam_label').hide();
        $('#pdam_label2').hide();
        $('#pdam_input').attr('type', 'hidden');
        $('#pdam_input').attr('disabled', 'disabled');
        $('#pdam_input2').attr('type', 'hidden');
        $('#pdam_input2').attr('disabled', 'disabled');
        $('#pdam_input3').attr('disabled', 'disabled');
    });
    $('#multi_button').on('click', function(){
        $('#error_msg2').hide();
        $('not_active_message').hide();
        $('#electricity_postpaid').hide();
        $('#electricity_postpaid_label').hide();
        $('#electricity_postpaid_input').attr('type', 'hidden');
        $('#electricity_postpaid_input').attr('disabled', 'disabled');
        $('#bpjs_kesehatan').hide();
        $('#bpjs_label').hide();
        $('#bpjs_label2').hide();
        $('#bpjs_input').attr('type', 'hidden');
        $('#bpjs_input').attr('disabled', 'disabled');
        $('#bpjs_input2').hide();
        $('#bpjs_input2').attr('disabled', 'disabled');
        $('#multifinance').show();
        $('#multi_label').show();
        $('#multi_input').attr('type', 'text');
        $('#multi_input').removeAttr('disabled');
        $('#telkom').hide();
        $('#telkom_label').hide();
        $('#telkom_input').attr('type', 'hidden');
        $('#telkom_input').attr('disabled', 'disabled');
        $('#pdam').hide();
        $('#pdam_label').hide();
        $('#pdam_label2').hide();
        $('#pdam_input').attr('type', 'hidden');
        $('#pdam_input').attr('disabled', 'disabled');
        $('#pdam_input2').attr('type', 'hidden');
        $('#pdam_input2').attr('disabled', 'disabled');
        $('#pdam_input3').attr('disabled', 'disabled');
    });
    $('#telkom_button').on('click', function(){
        $('#error_msg2').hide();
        $('not_active_message').hide();
        $('#electricity_postpaid').hide();
        $('#electricity_postpaid_label').hide();
        $('#electricity_postpaid_input').attr('type', 'hidden');
        $('#electricity_postpaid_input').attr('disabled', 'disabled');
        $('#bpjs_kesehatan').hide();
        $('#bpjs_label').hide();
        $('#bpjs_label2').hide();
        $('#bpjs_input').attr('type', 'hidden');
        $('#bpjs_input').attr('disabled', 'disabled');
        $('#bpjs_input2').hide();
        $('#bpjs_input2').attr('disabled', 'disabled');
        $('#multifinance').hide();
        $('#multi_label').hide();
        $('#multi_input').attr('type', 'hidden');
        $('#multi_input').attr('disabled', 'disabled');
        $('#telkom').show();
        $('#telkom_label').show();
        $('#telkom_input').attr('type', 'text');
        $('#telkom_input').removeAttr('disabled');
        $('#pdam').hide();
        $('#pdam_label').hide();
        $('#pdam_label2').hide();
        $('#pdam_input').attr('type', 'hidden');
        $('#pdam_input').attr('disabled', 'disabled');
        $('#pdam_input2').attr('type', 'hidden');
        $('#pdam_input2').attr('disabled', 'disabled');
        $('#pdam_input3').attr('disabled', 'disabled');
    });
    $('#pdam_button').on('click', function(){
        $('#error_msg2').hide();
        $('not_active_message').hide();
        $('#electricity_postpaid').hide();
        $('#electricity_postpaid_label').hide();
        $('#electricity_postpaid_input').attr('type', 'hidden');
        $('#electricity_postpaid_input').attr('disabled', 'disabled');
        $('#bpjs_kesehatan').hide();
        $('#bpjs_label').hide();
        $('#bpjs_label2').hide();
        $('#bpjs_input').attr('type', 'hidden');
        $('#bpjs_input').attr('disabled', 'disabled');
        $('#bpjs_input2').hide();
        $('#bpjs_input2').attr('disabled', 'disabled');
        $('#multifinance').hide();
        $('#multi_label').hide();
        $('#multi_input').attr('type', 'hidden');
        $('#multi_input').attr('disabled', 'disabled');
        $('#telkom').hide();
        $('#telkom_label').hide();
        $('#telkom_input').attr('type', 'hidden');
        $('#telkom_input').attr('disabled', 'disabled');
        $('#pdam').show();
        $('#pdam_label').show();
        $('#pdam_label2').show();
        $('#pdam_input').attr('type', 'text');
        $('#pdam_input').removeAttr('disabled');
        $('#pdam_input2').attr('type', 'text');
        $('#pdam_input2').removeAttr('disabled');
        $('#pdam_input3').removeAttr('disabled');
    });

    $('#voucher_submit').click(function(){
        if ($('#session').val() == '') {
            $('#error_msg1').html('Mohon Untuk Login Terlebih Dahulu');
            $('#error_msg1').show();
            return false;
        }else {
            $('error_msg1').hide();
        }
        if ($('#product_sepulsa').val() == '') {
            $('#product_sepulsa').css('border', '1px solid red');
            return false;
        }else {
            $('#product_sepulsa').css('border', '');
        }

        if ($('#mobile').is(':visible')) {
            if ($('#mobile_input').val() == '') {
                $('#mobile_input').css('border', '1px solid red');
                return false;
            }else {
                $('#mobile_input').css('border', '');
            }
        }
        if ($('#electricity').is(':visible')) {
            if ($('#electricity_input').val() == '') {
                $('#electricity_input').css('border', '1px solid red');
                return false;
            }else {
                $('#electricity_input').css('border', '');
            }
            if ($('#electricity_input2').val() == '') {
                $('#electricity_input2').css('border', '1px solid red');
                return false;
            }else {
                $('#electricity_input2').css('border', '');
            }
        }
        if ($('#game').is(':visible')) {
            if ($('#game_input').val() == '') {
                $('#game_input').css('border', '1px solid red');
                return false;
            }else {
                $('#game_input').css('border', '');
            }
        }
    });

    $('#tagihan_submit').click(function(){
        if ($('#session').val() == '') {
            $('#error_msg2').html('Mohon Untuk Login Terlebih Dahulu');
            $('#error_msg2').show();
            return false;
        }else {
            $('error_msg2').hide();
        }
        if ($('#product_id').val() == '') {
            $('#product_id').css('border', '1px solid red');
            return false;
        }else {
            $('#product_id').css('border', '');
        }

        if ($('#electricity_postpaid').is(':visible')) {
            if ($('#electricity_postpaid_input').val() == '') {
                $('#electricity_postpaid_input').css('border', '1px solid red');
                return false;
            }else {
                $('#electricity_postpaid_input').css('border', '');
            }
        }
        if ($('#bpjs_kesehatan').is(':visible')) {
            if ($('#bpjs_input').val() == '') {
                $('#bpjs_input').css('border', '1px solid red');
                return false;
            }else {
                $('#bpjs_input').css('border', '');
            }
        }
        if ($('#multifinance').is(':visible')) {
            if ($('#multi_input').val() == '') {
                $('#multi_input').css('border', '1px solid red');
                return false;
            }else {
                $('#multi_input').css('border', '');
            }
        }
        if ($('#telkom').is(':visible')) {
            if ($('#telkom_input').val() == '') {
                $('#telkom_input').css('border', '1px solid red');
                return false;
            }else {
                $('#telkom_input').css('border', '');
            }
        }
        if ($('#pdam').is(':visible')) {
            if ($('#pdam_input').val() == '') {
                $('#pdam_input').css('border', '1px solid red');
                return false;
            }else {
                $('#pdam_input').css('border', '');
            }
            if ($('pdam_input3').val() == '') {
                $('#pdam_input2').css('border', '1px solid red');
                return false;
            }else {
                $('#pdam_input2').css('border', '');
            }
        }
    });
</script>
