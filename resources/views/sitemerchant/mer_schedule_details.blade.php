<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
   // dd($metatitle);
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> | Product details</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


       <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

  <div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                          <ul class="breadcrumb">
                              <li class=""><a >Home</a></li>
                                <li class="active"><a >Deal details</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Deal details</h5>

        </header>
        <div class="row">
          <div class="col-lg-11 panel_marg" style="padding-bottom:10px;">

          <form >
            <div class="panel panel-default">
              <div class="panel-heading">
              Deal details
              </div>

              <div class="panel-body">
                  <div class="form-group">
                     <label class="control-label col-lg-2" for="text1">Title<span class="text-sub">*</span></label>
                     <div class="col-lg-4">
                      <?php echo $get_schedules[0]->schedp_title; ?>
                     </div>
                 </div>
               </div>
             <div class="panel-body">
                 <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Start Date<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                     <?php echo $get_schedules[0]->schedp_start_date;//echo date('M-d-Y h:m:s',strtotime($startdate)); ?>
                    </div>
                </div>
              </div>
             <div class="panel-body">
                 <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">End Date<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo $get_schedules[0]->schedp_end_date;//echo date('M-d-Y h:m:s',strtotime($enddate)); ?>
                    </div>
                </div>
            </div>
            <?php
                if ($get_schedules[0]->schedp_RecurrenceType == 2) {
                  $time = "Daily";
              }elseif($get_schedules[0]->schedp_RecurrenceType == 3) {
                  $time = "Monthly";
              }else {
                  $time = 'Every Time';
              }
             ?>
             <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Recurring<span class="text-sub">*</span></label>
                    <div class="col-lg-3">
                      <?php echo $time ?>
                    </div>
                </div>

                <?php if ($time == "Daily") {?>
                <div class="col-lg-3 " id="dialy_time" style="display:none;">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      Recurr Every
                      <?php
                        $days = $get_schedules[0]->schedp_Int1;
                        if ($days == 1) {
                          $days = $days." day";
                        }else {
                          $days = $days ." days";
                        }
                        echo "<br> $days";
                      ?>
                    </div>
                  </div>
                </div>
                <?php }if ($time == "Daily") {
                  $checked =  explode(',', $get_schedules[0]->schedp_Int2);
                ?>
                <div class="col-lg-4 " id="weekly_time" >
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <p>Recurr Every</p>
                      <?php
                        // $week = $get_schedules[0]->schedp_Int1;
                        // if ($week == 1) {
                        //   $week = $week." week";
                        // }else {
                        //   $week = $week ." weeks";
                        // }
                        // echo "<br> $week <br>";
                      ?>

                      <div class="col-lg-6">
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="monday_checkbox" name="rec_week_every[]" value="0" <?php foreach ($checked as $value) { if ($value == 0) {
                            echo "checked";}} ?>>Monday</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="tuesday_checkbox" name="rec_week_every[]" value="1" <?php foreach ($checked as $value) { if ($value == 1) {
                            echo "checked";}} ?>>Tuesday</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="wednesday_checkbox" name="rec_week_every[]" value="2" <?php foreach ($checked as $value) { if ($value == 2) {
                            echo "checked";}} ?>>Wednesday</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="thurshday_checkbox" name="rec_week_every[]" value="3" <?php foreach ($checked as $value) { if ($value == 3) {
                            echo "checked";}} ?>>Thursday</label>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="friday_checkbox" name="rec_week_every[]" value="4" <?php foreach ($checked as $value) { if ($value == 4) {
                            echo "checked";}} ?>>Friday</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="saturday_checkbox" name="rec_week_every[]" value="5" <?php foreach ($checked as $value) { if ($value == 5) {
                            echo "checked";}} ?>>Saturday</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_week_every[]" value="6" <?php foreach ($checked as $value) { if ($value == 6) {
                            echo "checked";}} ?>>Sunday</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="time" class="col-lg-3 input-group bootstrap-timepicker timepicker">
                    <label>Start Time</label>
                    <input disabled readonly id="timepicker4" name="start_time" type="text" class="form-control input-small" placeholder="00:00:00" value="{{$get_schedules[0]->schedp_recurr_start_time}}">
                    <label>End Time</label>
                    <input disabled readonly id="timepicker5" name="end_time" type="text" class="form-control input-small" placeholder="00:00:00" value="{{$get_schedules[0]->schedp_recurr_end_time}}">
                </div>
                <?php }elseif ($time == 'Monthly') {
                    $checked =  explode(',', $get_schedules[0]->schedp_Int2);
                ?>
                <div class="col-lg-4 " id="monthly_time" >
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <p>Recurr Every</p>

                      <div class="col-lg-6">
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="monday_checkbox" name="rec_month_every[]" value="0" <?php foreach ($checked as $value) { if ($value == 0) {
                            echo "checked";}} ?>>January</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="tuesday_checkbox" name="rec_month_every[]" value="1" <?php foreach ($checked as $value) { if ($value == 1) {
                            echo "checked";}} ?>>February</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="wednesday_checkbox" name="rec_month_every[]" value="2" <?php foreach ($checked as $value) { if ($value == 2) {
                            echo "checked";}} ?>>March</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="thurshday_checkbox" name="rec_month_every[]" value="3" <?php foreach ($checked as $value) { if ($value == 3) {
                            echo "checked";}} ?>>April</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="friday_checkbox" name="rec_month_every[]" value="4" <?php foreach ($checked as $value) { if ($value == 4) {
                            echo "checked";}} ?>>May</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="saturday_checkbox" name="rec_month_every[]" value="5" <?php foreach ($checked as $value) { if ($value == 5) {
                            echo "checked";}} ?>>June</label>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_month_every[]" value="6" <?php foreach ($checked as $value) { if ($value == 6) {
                            echo "checked";}} ?>>July</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_month_every[]" value="7" <?php foreach ($checked as $value) { if ($value == 7) {
                            echo "checked";}} ?>>August</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_month_every[]" value="8" <?php foreach ($checked as $value) { if ($value == 8) {
                            echo "checked";}} ?>>September</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_month_every[]" value="9" <?php foreach ($checked as $value) { if ($value == 9) {
                            echo "checked";}} ?>>October</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_month_every[]" value="10" <?php foreach ($checked as $value) { if ($value == 10) {
                            echo "checked";}} ?>>November</label>
                        </div>
                        <div class="checkbox">
                          <label><input disabled readonly type="checkbox" id="sunday_checkbox" name="rec_month_every[]" value="11" <?php foreach ($checked as $value) { if ($value == 11) {
                            echo "checked";}} ?>>December</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php } ?>
            </div>

            <div class="panel-body">
                <div class="form-group">
                   <label class="control-label col-lg-2" for="text1">Tipe Promo<span class="text-sub">*</span></label>
                   <div class="col-lg-4">
                     <?php
                     if($get_schedules[0]->schedp_simple_action=='free_shipping'){
                         $tipe = 'Free Shipping';
                     }elseif($get_schedules[0]->schedp_simple_action=='by_percent'){
                         $tipe = 'Discounted By Percent';
                     }elseif ($get_schedules[0]->schedp_simple_action=='by_fixed') {
                         $tipe = 'Discounted By Fixed Amount';
                     }elseif ($get_schedules[0]->schedp_simple_action=='buy_x_get_y') {
                         $tipe = 'Buy X Get Y';
                     }elseif ($get_schedules[0]->schedp_simple_action=='flash_promo') {
                         $tipe = 'Flash Promo';
                     }else {
                         $tipe = '';
                     }
                     echo $tipe;
                     ?>
                   </div>
               </div>
           </div>

           <?php if ($get_schedules[0]->schedp_simple_action!='flash_promo'):?>
            <div class="panel-body">
                <div class="form-group">
                   <label class="control-label col-lg-2" for="text1">Voucher Code<span class="text-sub">*</span></label>
                   <div class="col-lg-4">
                     <?php echo $get_promo_coupons->promoc_voucher_code;
                     ?>
                   </div>
               </div>
           </div>
         
           <div class="panel-body">
               <div class="form-group">
                  <label class="control-label col-lg-2" for="text1">Minimal Transaction<span class="text-sub">*</span></label>
                  <div class="col-lg-4">
                    {{$get_cur}}. <?php echo number_format($get_promo_coupons->promoc_minimal_transaction,2,',','.');
                    ?>
                  </div>
              </div>
          </div>

          <div class="panel-body">
              <div class="form-group">
                 <label class="control-label col-lg-2" for="text1">Voucher Usage Limit<span class="text-sub">*</span></label>
                 <div class="col-lg-4">
                   <?php echo $get_promo_coupons->promoc_usage_limit;
                   ?>
                 </div>
             </div>
         </div>

         <div class="panel-body">
             <div class="form-group">
                <label class="control-label col-lg-2" for="text1">Voucher Usage Limit Per Customer<span class="text-sub">*</span></label>
                <div class="col-lg-4">
                  <?php echo $get_promo_coupons->promoc_usage_per_customer;
                  ?>
                </div>
            </div>
        </div>
          <?php endif;?>
        <div class="panel-body" <?php if($get_schedules[0]->schedp_simple_action != 'free_shipping') { ?> style="display:none;"<?php } ?>>
            <div class="form-group">
               <label class="control-label col-lg-2" for="text1">Maximal Shipping<span class="text-sub">*</span></label>
               <div class="col-lg-4">
                 <?php echo $get_schedules[0]->maximum_free_shipping;
                 ?>
               </div>
           </div>
       </div>

            <div class="panel-body" id="discounted_percentage" <?php if($get_schedules[0]->schedp_simple_action != 'by_percent') { ?> style="display:none;"<?php } ?>>
                <div class="form-group">
                   <label class="control-label col-lg-2" for="text1">Discounted Percentage<span class="text-sub">*</span></label>
                   <div class="col-lg-4">
                     <?php if($get_promo_product!=null){ echo $get_promo_product[0]->promop_discount_percentage." %"; }
                     ?>
                   </div>
               </div>
           </div>

           <div class="panel-body" id="discounted_price" <?php if($get_schedules[0]->schedp_simple_action != 'by_fixed') { ?> style="display:none;"<?php } ?>>
               <div class="form-group">
                  <label class="control-label col-lg-2" for="text1">Discounted Amount<span class="text-sub">*</span></label>
                  <div class="col-lg-4">
                    <?php if($get_promo_product!=null){ echo "Rp. ".number_format($get_promo_product[0]->promop_saving_price,2,',','.'); }
                    ?>
                  </div>
              </div>
          </div>
          <?php if($get_postal_services != ''){ ?>
          <div class="panel-body" id="jenis_pengiriman" <?php if($get_schedules[0]->schedp_simple_action != 'free_shipping') { ?> style="display:none;"<?php } ?>>
              <div class="form-group">
                 <label class="control-label col-lg-2" for="text1">Jenis Pengiriman<span class="text-sub">*</span></label>
                 <div class="col-lg-4">
                   <?php echo $get_postal_services->ps_code;
                   ?>
                 </div>
             </div>
         </div>
         <?php }?>

         <div class="panel-body" id="nilai_x" <?php if($get_schedules[0]->schedp_simple_action != 'buy_x_get_y') { ?> style="display:none;"<?php } ?>>
             <div class="form-group">
                <label class="control-label col-lg-2" for="text1">Discount Qty Step (Buy X)<span class="text-sub">*</span></label>
                <div class="col-lg-4">
                  <?php echo $get_schedules[0]->schedp_x;
                  ?>
                </div>
            </div>
        </div>

        <div class="panel-body" id="nilai_y" <?php if($get_schedules[0]->schedp_simple_action != 'buy_x_get_y') { ?> style="display:none;"<?php } ?>>
            <div class="form-group">
               <label class="control-label col-lg-2" for="text1">Discount Amount (Get Y)<span class="text-sub">*</span></label>
               <div class="col-lg-4">
                 <?php echo $get_schedules[0]->schedp_y;
                 ?>
               </div>
           </div>
       </div>

           <div class="panel-body" id="product" <?php if($get_schedules[0]->schedp_simple_action == 'free_shipping') { ?> style="display:none;"<?php } ?>>
               <div class="form-group">
              <label for="text1" class="control-label col-lg-2">Product<span class="text-sub">*</span></label>

                <div class="col-lg-8 ">
                  <div class="row panel panel-default" style="height:300px; overflow: scroll;" >
                      <div class="panel-body row">
                        <div id="prodCont">

                          <?php if($get_products!=''){ foreach ($get_products as $value): ?>

                            <?php if($value!=null){ $product_get_img = explode("/**/",$value->pro_Img); ?>

                            <div class="col-lg-3">
                              <div class="thumbnail" style="height:280px;">
                                <a href="">
                                  <img src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>" alt="Lights" style="width:100%; height: 50%;">
                                  <div class="caption">
                                    <p>{{$value->pro_title}}</p>
                                  </div>
                                </a>
                              </div>
                            </div>
                        <?php } endforeach; }?>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
          </div>

          </div>

          <div class="form-group">
                    <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-2">
                    <a style="color:#fff" href="<?php echo url('mer_manage_schedule'); ?>" class="btn btn-warning btn-sm btn-grad">Back</a>
                    </div>

                </div>

                </form>
                </div>

        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->
  <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

</body>
     <!-- END BODY -->
</html>
