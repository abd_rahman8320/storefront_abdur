<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <?php
       $metatitle = DB::table('nm_generalsetting')->get();
        if($metatitle){
        foreach($metatitle as $metainfo) {
            $metaname=$metainfo->gs_metatitle;
             $metakeywords=$metainfo->gs_metakeywords;
             $metadesc=$metainfo->gs_metadesc;
             }
             }
        else
        {
             $metaname="";
             $metakeywords="";
              $metadesc="";
        }
        ?>
        <title><?php echo $metaname  ;?>| Withdraw Fund Request</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/theme.css" />
	<link rel="stylesheet" href="<?php echo url(); ?>/assets/css/plan.css" />
<link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="<?php echo url(); ?>/assets/css/datepicker.css" rel="stylesheet">
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<?php echo url(); ?>/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/timeline/timeline.css" />



</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
          {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        {!! $adminleftmenus !!}


        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <?php foreach($commison_amt as $commision) { }
				$comminsion = $commision->mer_commission;

				$deal =  $deal_count * $deal_discount_count ;
				$commision_amt_deal = $deal * ($comminsion / 100);
				$deal_withdraw = $deal - $commision_amt_deal;
			 	$product =  $product_no_count * $product_discount_count ;
				$commision_amt_pro = $product * ($comminsion / 100);
				$product_withdraw = $product -$commision_amt_pro;
				 ?>

    <div class="inner">
      <div class="row">
        <div class="col-lg-12">
          <ul class="breadcrumb">
            <li class=""><a href="<?php echo url('sitemerchant_dashboard'); ?>">Home</a></li>
            <li class="active"><a href="#">Withdraw Fund Request</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="box dark"> <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Withdraw Fund Request</h5>
            </header>
            <div class="row">
                <div class="col-lg-10 panel_marg">
                    <form class="" action="{{url('withdraw_submit')}}" method="post">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nomor Transaksi</th>
                                    <th>Nama Barang</th>
                                    <th>Quantity</th>
                                    <th>Jumlah Harga - Komisi ({{$commison_amt[0]->mer_commission}}%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total=0; $i=1;
                                if(!empty($fund_details)){
                                    foreach ($fund_details as $fund_detail) {
                                        $order_amt = $fund_detail->order_amt - ($fund_detail->order_amt * $commison_amt[0]->mer_commission/100);
                                        $order_shipping_price = $fund_detail->order_shipping_price;
                                        ?>
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$fund_detail->transaction_id}}</td>
                                    <td>{{$fund_detail->pro_title}}</td>
                                    <td>{{$fund_detail->order_qty}}</td>
                                    <td style="text-align:right;">{{$get_cur}}{{number_format($order_amt + $order_shipping_price,0,',','.')}}</td>
                                </tr>
                                <?php $i++; $total+=$order_amt + $order_shipping_price; } ?>
                                <tr>
                                    <td colspan="2" style="text-align:right;"><b>Total</b></td>
                                    <td colspan="3" style="text-align:right;"><b>{{$get_cur}}{{number_format($total,0,',','.')}}</b></td>
                                </tr>
                            </tbody>
                            <input type="hidden" name="total" value="{{$total}}">
                            <input type="hidden" name="merchant_id" value="{{$fund_detail->pro_mr_id}}">
                        <?php } ?>
                        </table>
                        <div class="form-group">
                            <label for="" class="control-label col-lg-2">Faktur Pajak<span class="text-sub">*</span></label>

                            <div class="col-lg-6">
                                <input type="text" name="faktur_pajak" placeholder="Faktur Pajak" value="" class="form-control" required>
                            </div>

                        </div>
                        <button class="btn btn-warning btn-sm btn-grad col-lg-offset-2" type="submit" id="send_request" style="color:#fff">Send Request</button>
                    </form>
                </div>

            </div>

          </div>
        </div>


      </div>

    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $adminfooter !!}
    <!--END FOOTER -->




     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url(); ?>/assets/plugins/jquery-2.0.3.min.js"></script>
    <script>
	$(document).ready(function() {
		$('#send_request').click(function() {
			var fund_detail = '<?php echo $i;?>';
            if(fund_detail==1){
                alert('Data Kosong');
                return false;
            }
            else if(fund_detail>1){
                return true;
            }
		});
	});
	</script>
     <script src="<?php echo url(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <script  src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.resize.js"></script>
	<script  src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo url(); ?>/assets/js/pie_chart.js"></script>




<script src="<?php echo url(); ?>/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo url(); ?>/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="<?php echo url(); ?>/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="<?php echo url(); ?>/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>



	<script src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo url(); ?>/assets/plugins/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo url(); ?>/assets/js/bar_chart.js"></script>




</body>
     <!-- END BODY -->
</html>
