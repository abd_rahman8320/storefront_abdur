<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
     <?php
         $metatitle = DB::table('nm_generalsetting')->get();
          if($metatitle){
          foreach($metatitle as $metainfo) {
              $metaname=$metainfo->gs_metatitle;
               $metakeywords=$metainfo->gs_metakeywords;
               $metadesc=$metainfo->gs_metadesc;
               }
               }
          else
          {
               $metaname="";
               $metakeywords="";
                $metadesc="";
          }
      ?>

      <title><?php echo $metaname  ;?> | Add Promo</title>
      <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />
         <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link href="<?php echo url('')?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES -->
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/Markdown.Editor.hack.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/jquery.cleditor-hack.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/bootstrap-wysihtml5-hack.css" />

    <style>
        ul.wysihtml5-toolbar > li {
            position: relative;
        }

        .dialog-wrapper{
      	  position: fixed;

      		top:0;
      		left: 0;
      		right: 0;
      		bottom: 0;
      		overflow: auto;
      		background: rgba(50,50,50,.4);
      		display: none;
      		padding: 15px;
          z-index: 9999;
	       }

      	.dialog{
      	  width: 600px;
      		background: #fff;
      		margin: auto;
      		padding: 15px;
      		min-width: 200px;
      		border: 1px solid #eee;
      		position: relative;
      		animation: span ease-out 300ms;
      	}

      	.dialog-head{
      		margin-bottom: 10px;

      	}

      	.dialog-head h4 {
      		margin: 0;
      		border-bottom: thin solid #ccc;
      		color: red;
      		margin-bottom: 10px;
      	}

      	.dialog-body{
      		overflow:auto;
      	}

      	@keyframes span{
      		0% {
      			transform: scaleX(0) scaleY(0);
      		}

      		100% {
      			transform: scaleX(1) scaleY(1);
      		}
	}
    </style>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >
    <!-- MAIN WRAPPER -->
    <div id="wrap" class="wrap">
         <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
         {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

         <!--PAGE CONTENT -->
        <div id="content">
                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            	<ul class="breadcrumb">
                                	<li class=""><a >Home</a></li>
                                  <li class="active"><a >Add Promo</a></li>
                              </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons"><i class="icon-edit"></i></div>
                                    <h5>Add Promo</h5>
                                </header>
                                <div id="div-1" class="accordion-body collapse in body">
                                <?php
                                      if(isset($action) && $action == 'save') { $process = 'save'; $button = 'Add Plan Timer'; $form_action = 'add_plan_timer_submit'; }
                        		          else if(isset($action) && $action == 'update') { $process = 'update'; $button = 'Update Deals'; }  ?>
                                      {!! Form::open(array('url'=>'mer_add_plan_timer_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}

                              			   <input type="hidden" name="action" value="<?php echo $process; ?>" />

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2"></label>

                                            <div class="col-lg-8">
                                                <div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="random_code" value="<?php echo(rand(10,100));?>">

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Title<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input name="title" type="text" class="form-control"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Start Date<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div id="datetimepicker1" class=" date input-group">
                                                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="startdate" name="startdate" class="form-control"></input>
                                                    <span class="add-on input-group-addon">
                                                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">End Date<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div id="datetimepicker2" class=" date input-group">
                                                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="enddate" name="enddate" class="form-control"></input>
                                                    <span class="add-on input-group-addon">
                                                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group" id="recurring">
                                            <label for="text1" class="control-label col-lg-2">Recurring<span class="text-sub">*</span></label>
                                                <div class="col-lg-3">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="radio">
                                                          <label><input type="radio" class="every_time"  name="opt_time" value="0" checked>Every Time</label>
                                                        </div>
                                                        <div class="radio ">
                                                            <label><input type="radio" class="dialy" name="opt_time" value="2">Daily</label>
                                                        </div>
                                                        <div class="radio">
                                                            <label><input type="radio" class="weekly"  name="opt_time" value="3">Monthly</label>
                                                        </div>

                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-lg-3 " id="dialy_time" style="display:none">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                      Recurr Every
                                                      <input type="number" name="rec_dialy_time" placeholder=" Day(s)">
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="col-lg-3 " id="monthly_time" style="display:none">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                      <p>Recurr Every</p>
                                                      <input style="display:none;" type="number" name="rec_monthly_time" placeholder=" Month(s)">
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="0">January</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="1">February</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="2">March</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="3">April</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="4">May</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="5">June</label>
                                                        </div>
                                                      </div>
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="6">July</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="7">August</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="8">September</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="9">October</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="10">November</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="11">December</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="col-lg-3 " id="weekly_time" style="display:none">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                      <p>Recurr Every</p>
                                                      <input style="display:none;" type="number" name="rec_weekly_time" placeholder=" Week(s)">
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="0">Monday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="1">Tuesday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="2">Wednesday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="3">Thursday</label>
                                                        </div>
                                                      </div>
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="4">Friday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="5">Saturday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_week_every[]" value="6">Sunday</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                                <div id="time" class="col-lg-3 input-group bootstrap-timepicker timepicker" style="display:none;">
                                                    <label>Start Time</label>
                                                    <input id="timepicker4" name="start_time" type="text" class="form-control input-small" placeholder="00:00:00">
                                                    <label>End Time</label>
                                                    <input id="timepicker5" name="end_time" type="text" class="form-control input-small" placeholder="00:00:00">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Tipe Promo<span class="text-sub">*</span></label>
                                            <div class="col-lg-3">
                                                <select class="form-control" id="simple_action" name="simple_action">
                                                    <!-- <option value="free_shipping">Free Shipping</option> -->
                                                    <option value='by_fixed'>Discounted By Fixed Amount</option>
                                                    <option value='by_percent'>Discounted By Percent</option>
                                                    <option value="buy_x_get_y">Buy x Get y</option>
                                                 </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Voucher Code<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input name="voucher_code" type="text" class="form-control"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="shipping_list" style="display:none">
                                            <label for="text1" class="control-label col-lg-2">Jenis Pengiriman<span class="text-sub">*</span></label>
                                            <div class="col-lg-3" >
                                                <div >
                                                    <select class="form-control" name="ps_id">
                                                        <option value="0">Semua Pengiriman</option>
                                                        <?php
                                                                foreach($list_postal_sevices as $list_postal_sevices2){
                                                            ?>
                                                                <option value='<?php echo $list_postal_sevices2 -> ps_id;?>'><?php echo $list_postal_sevices2 -> ps_code;?></option>
                                                            <?php
                                                                }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="minimal_transaction">
                                            <label for="text1" class="control-label col-lg-2">Minimal Transaction<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="minimal_transaction" name="minimal_transaction" type="number" class="form-control" placeholder="Rp"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="voucher_limit">
                                            <label for="text1" class="control-label col-lg-2">Voucher Usage Limit<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="voucher_limit" name="voucher_limit" type="number" class="form-control" placeholder="0"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="customer_voucher_limit">
                                            <label for="text1" class="control-label col-lg-2">Voucher Usage Limit Per Customer<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="customer_voucher_limit" name="customer_voucher_limit" type="number" class="form-control" placeholder="0"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="discounted_percentage" style="display:none;">
                                            <label for="text1" class="control-label col-lg-2">Discounted Percentage<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="discounted_percentage_input" name="discounted_percentage" type="number" class="form-control" placeholder="%"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="discounted_price" style="">
                                            <label for="text1" class="control-label col-lg-2">Discounted Amount<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="discounted_price_input" name="discounted_price" type="number" class="form-control" placeholder="Rp"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="nilai_x" style="display:none;">
                                            <label for="text1" class="control-label col-lg-2">Discount Qty Step (Buy X)<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input name="nilai_x" type="number" class="form-control" placeholder=""></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="nilai_y" style="display:none;">
                                            <label for="text1" class="control-label col-lg-2">Discount Amount (Get Y)<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input name="nilai_y" type="number" class="form-control" placeholder=""></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id='div_all_products'>
                                            <label for="text1" class="control-label col-lg-2">All Products<span class="text-sub">*</span></label>
                                            <div class="col-lg-3" >
                                                <div >
                                                    <input type="checkbox" id="all_products" name="all_products">
                                                    <?php foreach ($get_products as $value){ ?>
                                                    <input type="hidden" name="product[]" class="pilih"  value="<?php echo $value->pro_id;?>" />
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="product">
                                          <label for="text1" class="control-label col-lg-2">Product<span class="text-sub">*</span></label>

                                            <div class="col-lg-8 ">
                                              <div style="margin-bottom:5px;">
                                                <button type="button" class="btn btn-default"><a style="color:blue">Add Product</a></button>
                                              </div>

                                              <div class="row panel panel-default" style="height:300px; overflow: scroll;" >
                                                  <div class="panel-body row">
                                                    <div class="col-lg-12" >
                                                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="header_table">
                                                          <thead>
                                                            <tr role="row">
                                                                <th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0"  aria-sort="ascending">S.No</th>
                                                                <th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Promo Name</th>
                                                                <th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Store Name</th>
                                                                <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Original Price</th>
                                                                <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0"> Deal Image </th>
                                                                <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Actions</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody id="prodCont">

                                                          </tbody>
                                                        </table>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                          </div>

                                        <div class="form-group">
                                                <label for="pass1" class="control-label col-lg-2"><span  class="text-sub"></span></label>

                                                <div class="col-lg-8">
                                                   <button class="btn btn-warning btn-sm btn-grad" id="submit_deal" ><a style="color:#fff"  ><?php echo $button; ?></a></button>
                                                   <button class="btn btn-default btn-sm btn-grad" type="reset" ><a style="color:#000">Reset</a></button>
                                                </div>
                                            </div>
                                      </form>
                                  </div>
                              </div>
                        </div>
                    </div>
                </div>
        </div>
            <!--END PAGE CONTENT -->

    </div>

    <div class="dialog-wrapper">
        <div class="dialog">
            <div class="dialog-head">
                <h4 class="">Pilih Product</h4>
            </div>
            <div class="dialog-body row">
              <div class="thumbnail">
                <input type="search" id="input_search" name="search" placeholder="Search Me Please !!"><input id="btn_search" type="submit" name="btn_search" value="cari">

                <button type="button" name="button" id="prev">Prev</button>
                <button type="button" name="button" id="next">Next</button>

                <input type="checkbox" class="select_all">Select All

                <select name="select_cat" class="kategori" id="kategori">
                <option value="">-Semua Kategory-</option>
                <?php foreach($category as $category_merchant){?>
                    <option value="<?php echo $category_merchant->mc_name; ?>"><?php echo $category_merchant->mc_name; ?></option>
                <?php  } ?>
                </select>

                <div class="content">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr role="row">
                            <th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0"  aria-sort="ascending">S.No</th>
                            <th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Product Name</th>
                            <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Original Price</th>
                            <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Image </th>
                            <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Actions</th>
                            <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Stock</th>
                        </tr>
                    </thead>
                    <tbody id="prodCont_dialog">
                        <tr id="tr_dialog">
                            <td class= "no_dialog"></td>
                            <td class="title_dialog"></td>
                            <td class="original_price_dialog"></td>
                            <td ><img style="height:40px;" src="" class="deals_image_dialog" alt=""></td>
                            <td><input class="checkbox_id" type="checkbox"></td>
                            <td class="stock"></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
              </div>


            </div>
        </div>
    </div>

     <!--END MAIN WRAPPER -->

  <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->

 	<script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
  <script type="text/javascript">
  $('#header_table').hide();
  var tr_dialog = $('#tr_dialog');
    var skip = 0;
    var count = 10;
    var daftar_produk_terpilih = [];

    function reload_product(){

      var prodCont = $('#prodCont_dialog');
      var inputan = $('#input_search').val();
       var kategori = $('.kategori').val();

      $.get("mer_search_product", {
                                pro_title:inputan,
                                kategori:kategori,
                                skip:skip,
                                count:count,
                              }).then(function(result){
        prodCont.empty();

        if(result.data.length > 0){
            $('.table-product').show();
            $('.text_not_found').remove();

            for(i=0;i<result.data.length;i++){
                if(result.data[i].pro_qty > 0){
                    var clone = tr_dialog.clone(); // mengklone tr
                    clone.find('.no_dialog').html(skip+i+1);
                    clone.find('.title_dialog').html(result.data[i].pro_title);
                    clone.find('.shop_dialog').html(result.data[i].pro_sh_id);
                    clone.find('.original_price_dialog').html(result.data[i].pro_price);
                    // clone.find('.checkbox_id').val(result.data[i].pro_id);
                    if (daftar_produk_terpilih.indexOf(result.data[i].pro_id) >= 0) {
                    clone.find('.checkbox_id').attr('checked','checked');
                    }
                    clone.find('.checkbox_id').val(result.data[i].pro_id);

                    var img_urls = (result.data[i].pro_Img).split('/**/');
                    if (img_urls.length > 0) {
                    clone.find('.deals_image_dialog').attr('src', 'assets/product/'+img_urls[0]);
                    }
                    //memasukan data ke html elemen td
                    clone.find('.stock').html(result.data[i].pro_qty);

                    prodCont.append(clone); // memasukan td_clone kedalam prodCont
                }
                else{
                    $('.table-product').hide();
                    var text = "<p class='text_not_found' style='text-align:center; margin-top: 30px;margin-bottom:30px;'><strong>Data Not found</strong></p>";

                    if($('.text_not_found').length == 0){
                    $('.content').append(text);
                    }
                }
            }

        } else {
              $('.table-product').hide();
              var text = "<p class='text_not_found' style='text-align:center; margin-top: 30px;margin-bottom:30px;'><strong>Data Not found</strong></p>";

             if($('.text_not_found').length == 0){
                $('.content').append(text);
             }
            }

        if (skip <= 0) {
          $('#prev').prop('disabled', true);
        }else{
          $('#prev').prop('disabled', false);
        }

        if (skip >= result.total-count) {
          $('#next').prop('disabled', true);
        }else{
          $('#next').prop('disabled', false);
        }
      });
    }

    $('#btn_search').on('click', function(){
        reload_product();
    });
    $('#input_search').keyup(function() {
        reload_product();
    });
    $('select[name=select_cat]').change(function() { //alert($(this).val());
        reload_product();
    });
    $('.select_all').change(function() {
        //reload_product();
         $(".checkbox_id").prop('checked', $(this).prop("checked"));
    });
    $('#next').on('click', function(){
      skip = skip + count;
      reload_product();
    });
    $('#prev').on('click', function(){
      skip = skip - count;
      reload_product();
    });
  </script>
    <script>
      $(document).ready(function(){
          var radio_dialy = $('.dialy');
          var radio_weekly = $('.weekly');
          var radio_every = $('.every_time');
           if (radio_dialy.is(':checked')) {
            console.log("Nongol nih");
            $("#weekly_time").fadeIn();
            $('#time').fadeIn();
          }
          if (radio_weekly.is(':checked')) {
            $("#monthly_time").fadeIn();
          }

          $(".every_time").click(function(){
              $("#weekly_time").hide();
              $("#monthly_time").hide();
              $('#time').hide();
          });
          $(".dialy").click(function(){
              $("#weekly_time").fadeIn();
              $("#monthly_time").hide();
              $('#time').show();
          });
          $(".weekly").click(function(){
              $("#monthly_time").fadeIn();
              $("#weekly_time").hide();
              $('#time').hide();
          });
          $(".btn-default").click(function(){
              $(".dialog-wrapper").show();
              reload_product();
          });
          // $("#dialy_time").hide();
          // $("#weekly_time").hide();
          // $(".dialy").click(function(){
          //     $("#dialy_time").fadeIn();
          //     $("#weekly_time").hide();
          // });
          // $(".weekly").click(function(){
          //     $("#weekly_time").fadeIn();
          //     $("#dialy_time").hide();
          // });
          // $(".btn-default").click(function(){
          //     $(".dialog-wrapper").show();
          //     reload_product();

          // });

          $(".dialog-wrapper").click(function(){
              //reload_product();
              $(".dialog-wrapper").hide();
              $('#header_table').show();
              var checks_inputs =   $('.dialog .checkbox_id:checked');
              console.log(checks_inputs);
              var prodCont = $('#prodCont');

              prodCont.empty();
              for (var i = 0; i < checks_inputs.length; i++) {
                var check_input = checks_inputs[i];
                var thumb = $(check_input.closest('#tr_dialog'));
                var thumbClone = thumb.clone();
                var id_prod = check_input.value;

                if (daftar_produk_terpilih.indexOf(id_prod) < 0) {
                  daftar_produk_terpilih.push(parseInt(id_prod));
                }


                thumbClone.find('.checkbox_id').attr('name', 'products[]');

                prodCont.append(thumbClone);

              }
              console.log(daftar_produk_terpilih);
          });
          $(".dialog").click(function(e){
              e.stopPropagation();
          });
      });
    </script>
    <script>
        $('#all_products').on('click', function(){
            var all_products = document.getElementById('all_products');
            if(all_products.checked){
                $('#product').hide();
            }else {
                $('#product').show();

            }
        });

        $('#simple_action').on('change', function(){
            var value = $('#simple_action').val()
            if(value=='by_fixed'){
                $('#discounted_percentage').hide();
                $('#discounted_price').show();
                $('#shipping_list').hide();
                $('#div_all_products').show();
                $('#product').show();
                $('#nilai_x').hide();
                $('#nilai_y').hide();
            }else if(value=='by_percent'){
                $('#discounted_percentage').show();
                $('#discounted_price').hide();
                $('#shipping_list').hide();
                $('#div_all_products').show();
                $('#product').show();
                $('#nilai_x').hide();
                $('#nilai_y').hide();
            }else if(value=='free_shipping'){
                $('#discounted_percentage').hide();
                $('#discounted_price').hide();
                $('#shipping_list').show();
                $('#div_all_products').hide();
                $('#product').hide();
                $('#nilai_x').hide();
                $('#nilai_y').hide();
            }else if (value=='buy_x_get_y') {
                $('#discounted_percentage').hide();
                $('#discounted_price').hide();
                $('#shipping_list').hide();
                $('#div_all_products').hide();
                $('#product').show();
                $('#nilai_x').show();
                $('#nilai_y').show();
            }
        });
		$( document ).ready(function() {

    	var startdate 		   = $('#startdate');
    	var enddate 		     = $('#enddate');

    	var wysihtml5 		   = $('#wysihtml5');

    	var metakeyword	  	 = $('#metakeyword');
    	var metadescription	 = $('#metadescription');


        $('#originalprice').keypress(function (e){
            if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57))
        		{
              originalprice.css('border', '1px solid red');
        			$('#error_msg').html('Numbers Only Allowed');
        			originalprice.focus();
        			return false;
            }else
        		{
              originalprice.css('border', '');
        			$('#error_msg').html('');
        		}
        });

		$('#submit_deal').click(function() {

		if(startdate.val() == '')
		{
			startdate.css('border', '1px solid red');
			$('#error_msg').html('Please Select Start Date');
			startdate.focus();
			return false;
		}else{
      startdate.css('border', '');
      $('#error_msg').html('');
    }

		if(enddate.val() == '')
		{
			enddate.css('border', '1px solid red');
			$('#error_msg').html('Please Select End Date');
			enddate.focus();
			return false;
		}else if(enddate.val() < startdate.val() )
		{
			enddate.css('border', '1px solid red');
			$('#error_msg').html('End Date sholud be greater than start date');
			enddate.focus();
			return false;
		}else
    {
      enddate.css('border', '');
      $('#error_msg').html('');
    }

		var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == "")
 		{
 		file.focus();
		file.css('border', '1px solid red');
		$('#error_msg').html('Please choose image');
		return false;
		}
		else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		file.focus();
		file.css('border', '1px solid red');
		$('#error_msg').html('Please choose valid image');
		return false;
		}
		else
		{
		file.css('border', '');
		$('#error_msg').html('');
		}

		});
    });
	</script>

    <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/timepicker/css/bootstrap-timepicker.min.css"></script>
    <script src="<?php echo url('')?>/assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
  <script type="text/javascript">
  $('#timepicker4').timepicker({
    minuteStep: 1,
    template: 'modal',
    appendWidgetTo: 'body',
    showSeconds: true,
    showMeridian: false,
    defaultTime: false
});
$('#timepicker5').timepicker({
  minuteStep: 1,
  template: 'modal',
  appendWidgetTo: 'body',
  showSeconds: true,
  showMeridian: false,
  defaultTime: false
});</script>
   <script src="<?php echo url('')?>/assets/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });

			$(function () {
                $('#datetimepicker2').datetimepicker();
            });

			$(function () {
                $('#datetimepicker3').datetimepicker();
            });
       $(function(){
                $('#timepicker4').timepicker();
            });

            $(function(){
                $('#timepicker5').timepicker();
            });
    </script>

         <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo url('')?>/assets/js/editorInit.js"></script>
    <script>
       $(function () { formWysiwyg(); });
    </script>
   <script type="text/javascript">
    function addFormField() {
      var id = document.getElementById("aid").value;
	  var count_id = document.getElementById("count").value;
	  if(count_id < 4){
	  document.getElementById('count').value = parseInt(count_id)+1;
      jQuery.noConflict()
      jQuery("#divTxt").append("<tr style='height:5px;' > <td> </td> </tr><tr id='row" + id + "' style='width:100%'><td width='20%'><input type='file' name='file_more"+count_id+"' /></td><td>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + id + "\"); return false;' style='color:#F60;' >Remove</a></td></tr>");
         jQuery('#row' + id).highlightFade({    speed:1000 });
     id = (id - 1) + 2;
     document.getElementById("aid").value = id;
	}
	if(count_id == 4)
	{
		alert("You can add maximum 5 images");;
	}
    }

      function removeFormField(id) {
var count_id = document.getElementById("count").value;
 document.getElementById('count').value = parseInt(count_id)-1;
        jQuery(id).remove();
    }
    </script>
    <!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>
</body>
     <!-- END BODY -->
</html>
