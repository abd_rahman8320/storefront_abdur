<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <?php
       $metatitle = DB::table('nm_generalsetting')->get();
        if($metatitle){
        foreach($metatitle as $metainfo) {
            $metaname=$metainfo->gs_metatitle;
             $metakeywords=$metainfo->gs_metakeywords;
             $metadesc=$metainfo->gs_metadesc;
             }
             }
        else
        {
             $metaname="";
             $metakeywords="";
              $metadesc="";
        }
        ?>
        <title><?php echo $metaname  ;?>| Products All Orders</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
<link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
	 <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
{!!$merchantheader!!}
        <!-- MENU SECTION -->
{!!$merchantleftmenus!!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
<div id="content">

    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                	<li class=""><a href="<?php echo url('sitemerchant_dashboard'); ?>">Home</a></li>
                    <li class="active"><a href="#">Products All Orders</a></li>
                </ul>
            </div>
        </div>

		<center>
		<form  action="{!!action('MerchantTransactionController@product_all_orders')!!}" method="POST">

            <input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">

            <div class="row">
				<br>

                <div class="col-sm-3">
					<div class="item form-group">
						<div class="col-sm-6">
                        From Date
                        </div>
						<div class="col-sm-6">
					          <input type="text" name="from_date"  class="form-control" id="datepicker-8" required>

						</div>
					</div>
				</div>

                <div class="col-sm-3">
					<div class="item form-group">
						<div class="col-sm-6">To Date</div>
							<div class="col-sm-6">
                                <input type="text" name="to_date"  id="datepicker-9" class="form-control">
                            </div>
				    </div>
				</div>

				<div class="form-group">
					<div class="col-sm-2">
					       <input type="submit" name="submit" class="btn btn-block btn-success" value="Search">
					</div>
				</div>

                            <input type="hidden" id="return_url" value="<?php echo 'https://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];?>" />

		</form>
	    </center>

<!-- Start Box Data Table -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box dark">
                <header>
                    <div class="icons"><i class="icon-edit"></i></div>
                    <h5>Products All Orders</h5>

                </header>

                <div id="tabel_merchant">
                <!-- Start Data Table Detail -->
                <div id="div-1" class="accordion-body collapse in body">

                    <form class="form-horizontal">

                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>

                        <div class="col-lg-8">
    					   <div class="col-lg-4"></div>
                           <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>
					    <div class="col-lg-4"></div></div>
                </div>

                <div class="form-group col-lg-12">
                    	<div id="div-1" class="accordion-body collapse in body">
           <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label>

		   </label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter">
		   </div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
					<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 30px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending">Pembeli</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending">ID Transaksi</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 158px;" aria-label="Email: activate to sort column ascending">Nama Product</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 158px;" aria-label="Email: activate to sort column ascending">Jumlah Barang</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending">Tanggal</th>
					<th class="sorting center" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 138px;" aria-label="Status: activate to sort column ascending">Alamat Shipping</th>
                    <th class="sorting center" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Status: activate to sort column ascending">Status</th>

					</tr>
                                    </thead>
                                    <tbody>
    <?php

        $i = 1 ;

		if(isset($_POST['submit']))
		{
			foreach($get_all_product_merchant_paid_by_date as $allorders_list)
            {
                // status outbound
                $status_outbound;

                if($allorders_list->status_outbound == 1)
                {
                    $status_outbound = "-";
                }
                else if($allorders_list->status_outbound == 2)
                {
                    $status_outbound = 'Pick';
                }
                else if($allorders_list->status_outbound == 3)
                {
                    $status_outbound = 'Packing';
                }
                else if($allorders_list->status_outbound == 4)
                {
                    $status_outbound = 'Shipping';
                }
                else if($allorders_list->status_outbound == 5)
                {
                    $status_outbound = 'Receiving';
                }
                else if($allorders_list->status_outbound == 6)
                {
                    $status_outbound = 'Barang Hilang';
                }

				?>


                <tr class="gradeA odd">
                    <td class="sorting_1"><?php echo $i;?></td>
                    <td class="     "><?php echo $allorders_list->cus_name;?></td>
                    <td class="     "><?php echo $allorders_list->transaction_id;?></td>
                    <td class="     "><?php echo $allorders_list->pro_title;?></td>
                    <td class="center     "><?php echo $allorders_list->order_qty;?></td>
                    <td class="center     "><?php echo $allorders_list->order_date;?></td>
                    <td class="center     "><?php echo $allorders_list->order_shipping_add;?></td>
                    <td>
                        <?php
                        if($allorders_list->status_outbound==5){
                        ?>
                        <select class="btn btn-default" disabled readonly>
                            <option> Received </option>
                        </select>
                        <?php
                        }else{
                        ?>
                        <select class="btn btn-default" id="ubah_status_outbound" name="ubah_status_outbound" onchange="update_outbound_merchant(this.value,'<?php echo $allorders_list->order_id;?>') ">
                            <option value="1" <?php if($allorders_list->status_outbound==1){?> selected <?php } ?> > - </option>
                            <option value="2" <?php if($allorders_list->status_outbound==2){?> selected <?php } ?> >Pick</option>
                            <option value="3" <?php if($allorders_list->status_outbound==3){?> selected <?php } ?> >Packing</option>
                            <option value="4" <?php if($allorders_list->status_outbound==4){?> selected <?php } ?> >Shipping</option>
                            <option value="6" <?php if($allorders_list->status_outbound==6){?> selected <?php } ?> >Barang Tidak Ada</option>
                        </select>
                        <?php
                        }
                        ?>
                    </td>
                </tr>

        <?php
            $i++;
            }

		}
        else
        {
			foreach($get_all_product_merchant_paid as $allorders_list)
            {
				// status outbound
                $status_outbound;

                if($allorders_list->status_outbound == 1)
                {
                    $status_outbound = "-";
                }
                else if($allorders_list->status_outbound == 2)
                {
                    $status_outbound = 'Pick';
                }
                else if($allorders_list->status_outbound == 3)
                {
                    $status_outbound = 'Packing';
                }
                else if($allorders_list->status_outbound == 4)
                {
                    $status_outbound = 'Shipping';
                }
                else if($allorders_list->status_outbound == 6)
                {
                    $status_outbound = 'Barang Hilang';
                }

                ?>


				<tr class="gradeA odd">
                    <td class="sorting_1"><?php echo $i;?></td>
                    <td class="     "><?php echo $allorders_list->cus_name;?></td>
                    <td class="     "><?php echo $allorders_list->transaction_id;?></td>
                    <td class="     "><?php echo $allorders_list->pro_title;?></td>
                    <td style="text-align: center;" class="center     "><?php echo $allorders_list->order_qty;?></td>
                    <td class="center     "><?php echo $allorders_list->order_date;?></td>
                    <td class="center     "><?php echo $allorders_list->order_shipping_add;?></td>
                    <td>
                        <?php
                        if($allorders_list->status_outbound==5){
                        ?>
                        <select class="btn btn-default" disabled readonly>
                            <option value="1" <?php if($allorders_list->status_outbound==1){?> selected <?php } ?> > - </option>
                            <option value="2" <?php if($allorders_list->status_outbound==2){?> selected <?php } ?> >Pick</option>
                            <option value="3" <?php if($allorders_list->status_outbound==3){?> selected <?php } ?> >Packing</option>
                            <option value="4" <?php if($allorders_list->status_outbound==4){?> selected <?php } ?> >Shipping</option>
                            <option value="" <?php if($allorders_list->status_outbound==5){?> selected <?php } ?> >Receiving</option>
                            <option value="6" <?php if($allorders_list->status_outbound==6){?> selected <?php } ?> >Barang Tidak Ada</option>
                        </select>
                        <?php
                        }else{
                        ?>
                        <select class="btn btn-default" id="ubah_status_outbound" name="ubah_status_outbound" onchange="update_outbound_merchant(this.value,'<?php echo $allorders_list->order_id;?>') ">
                            <option value="1" <?php if($allorders_list->status_outbound==1){?> selected <?php } ?> > - </option>
                            <option value="2" <?php if($allorders_list->status_outbound==2){?> selected <?php } ?> >Pick</option>
                            <option value="3" <?php if($allorders_list->status_outbound==3){?> selected <?php } ?> >Packing</option>
                            <option value="4" <?php if($allorders_list->status_outbound==4){?> selected <?php } ?> >Shipping</option>
                            <option value="6" <?php if($allorders_list->status_outbound==6){?> selected <?php } ?> >Barang Tidak Ada</option>
                        </select>
                        <?php
                        }
                        ?>
                    </td>
                </tr>

    <?php $i++; } }?>
					</tbody>
                                </table><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"></div></div></div></div><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"><ul class="pagination"></ul></div></div></div></div><div class="row">

								<div class="col-sm-6">
								<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
								<ul class="pagination">
								<li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">
								</li>
								</ul></div></div></div></div>
        </div>
                </div>

         </form>
        </div>
       <!-- End Data Table -->
       </div>
       <!-- End div table_merchant -->

       <!-- Start nomor resi -->
       <div id="nomor_resi" style="display: none;">
           <div class="panel panel-default">
        {!! Form::open(array('url'=>'input_nomor_resi_merchant','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
        <div class="panel-heading"><b>
            Silahkan Input Nomor Resi </b>
        </div>

        <div class="panel-body">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="col-lg-2">
                        <label class="control-label" for="text1">Nomor Resi<span class="text-sub">*</span></label>
                    </div>

                    <div class="col-lg-3">
                        <input type="text" class="form-control" placeholder="" id="nomor_resi" name="nomor_resi" >
                    </div>

                    <div class="col-lg-2">
                        <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" >Submit</a></button>
                    </div>

                    <input type="hidden" name="id_order_no_resi" id="id_order_no_resi" name="id_order_no_resi">
                    <input type="hidden" name="status_outbound_no_resi" id="status_outbound_no_resi" name="status_outbound_no_resi">

                </div>
            </div>
        </div>

                </form>
                </div>

        </div>
       </div>
       <!-- End Nomor Resi -->

    </div>
</div>

    </div>
    <!-- End Data Table -->

                    </div>



                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   {!!$merchantfooter!!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('');?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('');?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('');?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });

        function update_outbound_merchant(status_outbound,order_id)
        {
            var returl =$('#return_url').val();

            if(status_outbound == 4)
            {
                $("#nomor_resi").show();
                $("#id_order_no_resi").val(order_id);
                $("#status_outbound_no_resi").val(status_outbound);
                $("#tabel_merchant").hide();
            }
            else
            {
                window.location.href = "{{url('update_outbound_merchant_submit')}}?orderid="+order_id+"&status_outbound="+status_outbound;
                // $.ajax({
                //     method: "get",
                //     url: "{{url('update_outbound_merchant_submit')}}",
                //     datatype: "json",
                //     data: {
                //         status_outbound: $status_outbound,
                //         orderid: $order_id,
                //     },
                //     success: function(d){
                //         window.location = returl;
                //         }
                // });
            }
        }
      </script>

      <!-- kocak -->

      <script type="text/javascript">

      </script>
</body>
     <!-- END BODY -->
</html>
