<?php header("Access-Control-Allow-Origin: *"); ?>
﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?>| Manage specification</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
	<!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->
	<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a >Manage Specification</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Manage Specification</h5>

        </header>


 <div class="row">

    <div class="col-lg-11 panel_marg">

	@if (Session::has('updated_result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('updated_result') !!}</div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		@endif
        @if (Session::has('insert_result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('insert_result') !!}</div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		@endif
        @if (Session::has('delete_result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('delete_result') !!}</div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		@endif
        <div class="form-group col-lg-3">
            <label>Specification Group Name</label>
            <select class="form-control" id="specification_group">
                @foreach($spgroups as $spgroup)
                <option value="<?php echo $spgroup->spg_id?>"><?php echo $spgroup->spg_name ?></option>
                @endforeach
            </select>
        </div>

        <!-- button -->
        <div class="col-lg-3 col-lg-offset-9" style="margin-top:-50px;">
            <a href="<?php echo url('add_specification'); ?>"><button type="button" name="button" class="btn btn-info">Add Specification</button></a>
        </div>
        <p></p>
        
        <!-- table -->
    	<table class="table table-bordered">
              <thead>
		 <tr>
                  <th style="width:10%;"class="text-center">S.No</th>
                  <th style="text-align:center;">Specification Name</th>
				  <th style="text-align:center;">Sort Order</th>

				  <th style="text-align:center;">Edit</th>
				  <th style="text-align:center;">Delete</th>
                </tr>
              </thead>
              <tbody id="specification">

 <?php $i=1;

?>

                @foreach ($specificationresult as $info)

                <tr>
                  <td class="text-center">{!!$i!!}</td>
                  <td class="text-center">{!!$info->sp_name!!}</td>
				   <td class="text-center">{!!$info->sp_order!!}</td>
				   <td class="text-center">
                       <a href="<?php echo url('edit_specification/'.$info->sp_id); ?>"><i class="icon icon-edit icon-2x"></i></a></td>
				   <td class="text-center">
                   <a href="<?php echo url('delete_specification/'.$info->sp_id); ?>"><i class="icon icon-trash icon-2x"></i></a></td>

                </tr>

<?php $i=$i+1; ?>
		@endforeach


              </tbody>
            </table>
    </div>

   </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->

         {!! $adminfooter !!}

    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
    $('#specification_group').on('change', function(){
        jQuery('#specification').empty();
        $.ajax({
            type: 'get',
            datatype: 'json',
            url: '{{url("get_specification")}}',
            data: {
                spg_id: $('#specification_group').val()
            },
            success: function(response){
                // alert('test');
                for (var i = 0; i < response.length; i++) {
                    j = i+1;
                    jQuery('#specification').append('<tr><td class="text-center">'+j+'</td><td class="text-center">'+response[i].sp_name+'</td><td class="text-center">'+response[i].sp_order+'</td><td class="text-center"><a href="<?php echo url('edit_specification'); ?>/'+response[i].sp_id+'"><i class="icon icon-edit icon-2x"></i></a></td><td class="text-center"><a href="<?php echo url('delete_specification'); ?>/'+response[i].sp_id+'"><i class="icon icon-trash icon-2x"></i></a></td></tr>');
                }
            }
        });
    });
    </script>
</body>
     <!-- END BODY -->
</html>
