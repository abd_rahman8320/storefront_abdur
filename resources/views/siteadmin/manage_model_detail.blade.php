<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |Add Grouping</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
   <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
   <link href="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


        <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->
    <div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                          <ul class="breadcrumb">
                              <li class=""><a>Home</a></li>
                                <li class="active"><a >Manage Grouping Detail</a></li>
                            </ul>
                    </div>
                </div>
<div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Manage Grouping Model</h5>

         </header>

         <!-- pilihan untuk manage groping -->
        <div id="judul_grop">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12">
                    <div id="div-1" class="accordion-body collapse in body">
                        <div class="form-group">
                            <label class="control-label col-lg-6"><b><h4><?php echo "List Product Model ".$detail_header[0]->nama_grouping_model?></h4></b></label>
                            <div class="col-lg-3" style="float: right;">
                            <button type="submit" id="btn_add_product" class="btn btn-success btn-sm btn-grad" style="color:#fff">Tambahkan Product</button>
                        </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <!-- end of combo box -->

        <!-- Spesifik -->
<div id="spesifik">
           @if (Session::has('block_message'))
    <div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
    @endif
        <div id="div-1" class="accordion-body collapse in body">
           <div class="accordion-body collapse in body" id="div-1">
           <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label>

       </label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter">
       </div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div>

           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 25px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending">Product Name</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending">Store Name</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Original Price(Rp): activate to sort column ascending">Original Price(Rp)</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Discounted Price(Rp): activate to sort column ascending">Discounted Price(Rp)</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Warna </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Grade </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Product Image </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Status </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Action</th>

                            <!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 74px;" aria-label="Preview: activate to sort column ascending">Preview</th> -->
                        </tr>
                    </thead>
                <tbody>
                  <?php $i = 1 ;
                          foreach($details as $row) {

                            $product_get_img = explode("/**/",$row->pro_Img);
                  ?>
                      <tr class="gradeA odd">
                        <td class="sorting_1"><?php echo $i; ?></td>
                        <td class="  "><?php echo substr($row->pro_title,0,45); ?></td>
                        <td class="center  "><?php echo $row->stor_name;?></td>
                        <td class="center  "><?php echo number_format($row->pro_price,2,",","."); ?></td>
                        <td class="center  "><?php echo number_format($row->pro_disprice,2,",","."); ?></td>
                        <td class="center  "><?php echo $row->co_name; ?></td>
                        <td class="center  "><?php echo $row->grade_name; ?></td>
                        <td class="center  "><a><img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>"></a></td>
                        <?php
                            $kocak;

                            if(floatval($row->pro_no_of_purchase) <= floatval($row->pro_qty) )
                            {
                               $kocak = "Stok Tersedia";
                            }
                            else
                            {
                                $kocak = "Stok Habis";
                            }

                        ?>
                        <td class="center  "><?php echo $kocak; ?></td>

                        <td class="center  "><a href="<?php echo url('keluarkan_model')."/".$row->id."/".$row->id_grouping_product_model_header; ?>"><button  class="btn btn-info btn-sm btn-grad" style="color:#fff">Keluarkan Product</button></a></td>
                      </tr>

                  <?php $i++;
                        }
                  ?>

                    </tbody>
                                </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"></div></div></div></div><div class="row">

                <div class="col-sm-6">
                <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination">
                <li id="dataTables-example_previous" tabindex="0" aria-controls="dataTables-example" class="paginate_button previous disabled">
                </li>
                </ul></div></div></div></div>
        </div>
        </div>
        <!-- Data Table -->
</div>
<!-- Akhir Spesifik -->

<div id="all" style="display: none;">

        @if (Session::has('block_message'))
    <div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
    @endif
        <div id="div-1" class="accordion-body collapse in body">
           <div class="accordion-body collapse in body" id="div-1">
           <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label>
           <hr>
           <h3>Tambahkan Product Model</h3>
           <hr>
       </label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter">
       </div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div>

           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example1">
                <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">S.No</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending">Product Name</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending">Store Name</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Original Price(Rp): activate to sort column ascending">Original Price(Rp)</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Discounted Price(Rp): activate to sort column ascending">Discounted Price(Rp)</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Warna </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Grade </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Product Image </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Status </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Action</th>

                            <!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 74px;" aria-label="Preview: activate to sort column ascending">Preview</th> -->
                        </tr>
                    </thead>
                <tbody>
                  <?php $i = 1 ;
                          foreach($all_product as $row) {

                            $product_get_img = explode("/**/",$row->pro_Img);
                  ?>
                      <tr class="gradeA odd">
                        <td class="sorting_1"><?php echo $i; ?></td>
                        <td class="  "><?php echo substr($row->pro_title,0,45); ?></td>
                        <td class="center  "><?php echo $row->stor_name;?></td>
                        <td class="center  "><?php echo number_format($row->pro_price,2,",","."); ?></td>
                        <td class="center  "><?php echo number_format($row->pro_disprice,2,",","."); ?></td>
                        <td class="center  "><?php echo $row->co_name; ?></td>
                        <td class="center  "><?php echo $row->grade_name; ?></td>
                        <td class="center  "><a><img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>"></a></td>
                        <?php
                            $kocak;

                            if(floatval($row->pro_no_of_purchase) <= floatval($row->pro_qty) )
                            {
                               $kocak = "Stok Tersedia";
                            }
                            else
                            {
                                $kocak = "Stok Habis";
                            }                        ?>
                        <td class="center  "><?php echo $kocak; ?></td>

                        <td class="center  "><a href="<?php echo url('tambahkan_model')."/".$row->pro_id."/".$detail_header[0]->id; ?>"><button  class="btn btn-info btn-sm btn-grad" style="color:#fff">Tambahkan Product</button></a></td>
                      </tr>

                  <?php $i++;
                        }
                  ?>

                    </tbody>
                                </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"></div></div></div></div><div class="row">

                <div class="col-sm-6">
                <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination">
                <li id="dataTables-example_previous" tabindex="0" aria-controls="dataTables-example" class="paginate_button previous disabled">
                </li>
                </ul></div></div></div></div>
        </div>
        </div>
    </div>
</div>
</div>
<!-- Akhir All -->
    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

   <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example1').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
     <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });

         $("#btn_add_product").on("click", function(){
          $("#all").show();
          $("#spesifik").hide();
          $("#judul_grop").hide();
         });
      </script>
</body>
     <!-- END BODY -->
</html>
