<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |    Manage Products                   </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->


        <div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li class=""><a >Home</a></li>
                                <li class="active"><a >  Product Tanpa Gambar                   </a></li>
                            </ul>
                    </div>
                </div>
                <center></center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>  Manage Product Tanpa Gambar                  </h5>
        </header>

        <!-- pilihan untuk manage groping -->
        <div class="row" style="margin-top: 25px;">
            <div class="col-lg-12">
                <div id="div-1" class="accordion-body collapse in body">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-2">Pilih Group Product<span class="text-sub">*</span></label>
                        <div class="col-lg-3">
                            <select class="form-control" id="cb_grouping" name="cb_grouping">
                                <?php
                                    if($id_group == null)
                                    {
                                        ?>
                                        <option value="pilih">Pilih Grouping</option>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $detail_ket_group->group_id;?>"><?php echo $detail_ket_group->group_nama?></option>
                                        <?php
                                    }
                                ?>
                                <?php
                                    foreach ($groping_main as $group_op) {
                                        ?>
                                        <option value="<?php echo $group_op->group_id;  ?>"><?php echo $group_op->group_nama;?></option>
                                        <?php
                                    }
                                ?>
                             </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of combo box -->

        <div id="judul_grop">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12">
                    <div id="div-1" class="accordion-body collapse in body">
                        <div class="form-group">
                            <label class="control-label col-lg-3"><b><h4><?php echo "List ".$detail_ket_group->group_nama." Product"?></h4></b></label>
                            <div class="col-lg-3" style="float: right;">
                            <button type="submit" id="btn_add_product" class="btn btn-success btn-sm btn-grad" style="color:#fff">Tambahkan Product</button>
                        </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>

        <div id="silahkan" style="display: none;">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12">
                    <div id="div-1" class="accordion-body collapse in body">
                        <div class="form-group">
                            <label class="control-label col-lg-6"><b><h4><?php echo "Tambahkan product ke dalam list ".$detail_ket_group->group_nama?></h4></b></label>
                        </div>
                    </div>
                </div>


            </div>
        </div>

<!-- start spesifikasi product -->
<div id="list_all_pro"  style="display: none;">
    <div id="div-1" class="accordion-body collapse in body">
        <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
            <div class="row">
                <div class="col-sm-6">
                    <div class="dataTables_length" id="dataTables-example_length">
                    <label></label>
                </div>
            </div>

            <div class="col-sm-6">
                <div id="dataTables-example_filter" class="dataTables_filter">

                </div>
            </div>
        </div>

        <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <div class="row">
                <div class="col-sm-6">
                    <div id="dataTables-example_length" class="dataTables_length">
                        <label></label>
                    </div>
                </div>

                <div class="col-sm-6">
                        <div class="dataTables_filter" id="dataTables-example_filter">

                        </div>
                </div>
            </div>

            <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_length" id="dataTables-example_length">
                                <label></label>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div id="dataTables-example_filter" class="dataTables_filter">

                        </div>
                    </div>
                </div>

                <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">S.No</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending">Product Name</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending">Store Name</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Original Price(Rp): activate to sort column ascending">Original Price(Rp)</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Discounted Price(Rp): activate to sort column ascending">Discounted Price(Rp)</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Product Image </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                        </tr>
                    </thead>

                    <tbody>
<?php $i = 1 ;
    if(isset($_POST['submit']))
    {
        foreach($productrep as $row)
        {
            if($row->pro_no_of_purchase < $row->pro_qty)
            {
                $product_get_img = explode("/**/",$row->pro_Img);

                if($row->pro_status == 1)
                {
                    $process = "<a href='".url('block_product/'.$row->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
                }

                else if($row->pro_status == 0)
                {
                    $process = "<a href='".url('block_product/'.$row->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
                }

?>
        <tr class="gradeA odd">
            <td class="sorting_1"><?php echo $i; ?></td>
            <td class="  "><?php echo substr($row->pro_title,0,45); ?></td>
            <td class="center  "><?php echo $row->stor_name;?></td>
            <td class="center  "><?php echo $row->pro_price; ?></td>
            <td class="center  "><?php echo $row->pro_disprice; ?></td>
            <td class="center  "><a><img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>"></a></td>
            <td class="center  "><a href="<?php echo url('product_details')."/".$row->pro_id; ?>">Tambahkan</a></td>
        </tr>

<?php
        $i++;
        }
    }
}

else{
        foreach($product_details_all as $product_list) {

        if(true /*bypass agar dapat menampilkan semua produk*/ || $product_list->pro_no_of_purchase < $product_list->pro_qty)
        {
            $product_get_img = explode("/**/",$product_list->pro_Img);
            if($product_list->pro_status == 1)
            {
                $process = "<a href='".url('block_product/'.$product_list->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
            }

            else if($product_list->pro_status == 0)
            {
                $process = "<a href='".url('block_product/'.$product_list->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
            }
?>

        <tr class="gradeA odd">
            <td class="sorting_1"><?php echo $i; ?></td>
            <td class="  "><?php echo substr($product_list->pro_title,0,45); ?></td>
            <td class="center  "><?php echo $product_list->stor_name;?></td>
            <td class="center  "><?php echo number_format($product_list->pro_price,2,",","."); ?></td>
            <td class="center  "><?php echo number_format($product_list->pro_disprice,2,",",".");?></td>
            <td class="center  "><a><img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>"></a></td>

            <td class="center  "><a href="<?php echo url('tambah_grop')."/".$product_list->pro_id."/".$detail_ket_group->group_id; ?>"><button  class="btn btn-info btn-sm btn-grad" style="color:#fff">Tambahkan Product</button></a></td>
        </tr>
<?php
        $i++;
            }
        }
    }
?>
    </tbody>
</table>

        <div class="row">
            <div class="col-sm-6">
                <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">

                </div>
            </div>

            <div class="col-sm-6">
                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info">

            </div>
        </div>

        <div class="col-sm-6">
            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination"></ul>
        </div>

        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                <ul class="pagination">
                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

</div>
<!-- end spesifikasi product -->


<!-- start all pro -->
<div id="list_spec_pro">
    <div id="div-1" class="accordion-body collapse in body">
        <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
            <div class="row">
                <div class="col-sm-6">
                    <div class="dataTables_length" id="dataTables-example_length">
                    <label></label>
                </div>
            </div>

            <div class="col-sm-6">
                <div id="dataTables-example_filter" class="dataTables_filter">

                </div>
            </div>
        </div>

        <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <div class="row">
                <div class="col-sm-6">
                    <div id="dataTables-example_length" class="dataTables_length">
                        <label></label>
                    </div>
                </div>

                <div class="col-sm-6">
                        <div class="dataTables_filter" id="dataTables-example_filter">

                        </div>
                </div>
            </div>

            <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_length" id="dataTables-example_length">
                                <label></label>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div id="dataTables-example_filter" class="dataTables_filter">

                        </div>
                    </div>
                </div>

                <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">Sort Number</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Deals Name: activate to sort column ascending">Product Name</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Store Name: activate to sort column ascending">Store Name</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Original Price(Rp): activate to sort column ascending">Original Price(Rp)</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Discounted Price(Rp): activate to sort column ascending">Discounted Price(Rp)</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Product Image </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label=" Deal Image : activate to sort column ascending"> Status </th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Action</th>

                            <!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 74px;" aria-label="Preview: activate to sort column ascending">Preview</th> -->
                        </tr>
                    </thead>

                    <tbody>
<?php $i = 1 ;
    if(isset($_POST['submit']))
    {
        foreach($productrep as $row)
        {
            if($row->pro_no_of_purchase < $row->pro_qty)
            {
                $product_get_img = explode("/**/",$row->pro_Img);

                if($row->pro_status == 1)
                {
                    $process = "<a href='".url('block_product/'.$row->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
                }

                else if($row->pro_status == 0)
                {
                    $process = "<a href='".url('block_product/'.$row->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
                }

?>
        <tr class="gradeA odd">
            <td class="sorting_1"><?php echo $i; ?></td>
            <td class="  "><?php echo substr($row->pro_title,0,45); ?></td>
            <td class="center  "><?php echo $row->stor_name;?></td>
            <td class="center  "><?php echo $row->pro_price; ?></td>
            <td class="center  "><?php echo $row->pro_disprice; ?></td>
            <td class="center  "><a><img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>"></a></td>
            <!-- <td class="center  "><?php echo $row->status; ?></td> -->
            <?php
                $kocak;

                if(floatval($row->pro_no_of_purchase) >= floatval($row->pro_qty) )
                {
                   $kocak = "Stok Tersedia";
                }
                else
                {
                    $kocak = "Stok Habis";
                }
            ?>
            <td class="center  "><?php echo $kocak; ?></td>

            <td class="center  "><a href="<?php echo url('keluarkan_group')."/".$product_list->pro_id."/".$detail_ket_group->group_id;; ?>"><button  class="btn btn-info btn-sm btn-grad" style="color:#fff">Keluarkan Product</button></a></td>

        </tr>

<?php
        $i++;
        }
    }
}

else{
        foreach($product_details as $product_list) {

        if(true /*bypass agar dapat menampilkan semua produk*/ || $product_list->pro_no_of_purchase < $product_list->pro_qty)
        {
            $product_get_img = explode("/**/",$product_list->pro_Img);
            if($product_list->status == "aktif")
            {
                $process = "<a href='".url('block_product/'.$product_list->pro_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
            }

            else if($product_list->status == "tidak aktif")
            {
                $process = "<a href='".url('block_product/'.$product_list->pro_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
            }
?>

        <tr class="gradeA odd">
            <form id="form-sort" class="" action="{{url('sort_product')}}/{{$product_list->group_detail_id_product}}/{{$product_list->group_detail_id_grouping}}" method="post">
            <td class="sorting_1"><input class="form-control" id="sort" type="number" name="sort" value="<?php echo $i; ?>" onkeypress="myFunction(event)"></td>
            </form>
            <td class="  "><?php echo substr($product_list->pro_title,0,45); ?></td>
            <td class="center  "><?php echo $product_list->stor_name;?></td>
            <td class="center  "><?php echo number_format($product_list->pro_price,2,",","."); ?></td>
            <td class="center  "><?php echo number_format($product_list->pro_disprice,2,",",".");?></td>
            <td class="center  "><a><img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>"></a></td>
            <?php
                $kocak;

                if(floatval($product_list->pro_no_of_purchase) >= floatval($product_list->pro_qty) )
                {
                   $kocak = "Stok Habis";
                }
                else
                {
                    $kocak = "Stok Tersedia";
                }
            ?>
            <td class="center  "><b><?php echo $kocak; ?></b></td>

            <td class="center  ">
                <a href="<?php echo url('keluarkan_group')."/".$product_list->pro_id."/".$detail_ket_group->group_id; ?>"><button  class="btn btn-info btn-grad" style="color:#fff">Keluarkan Product</button></a>
            </td>

        </tr>
<?php
        $i++;
            }
        }
    }
?>
    </tbody>
</table>

        <div class="row">
            <div class="col-sm-6">
                <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">

                </div>
            </div>

            <div class="col-sm-6">
                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info">

            </div>
        </div>

        <div class="col-sm-6">
            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination"></ul>
        </div>

        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                <ul class="pagination">
                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

</div>
<!-- end all pro -->



        </div> <!-- dark box -->
    </div>


</div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
     <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
       <script>
        function myFunction(event)
        {
            var x = event.which;
            if(x==13){
                document.getElementById("form-sort").submit();
            }
        }

         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>
      <script type="text/javascript">

        // redirect ke grouping yang dituju
        $("#cb_grouping").on("change", function(){
            window.location.href = 'halaman_product?grop='+$("#cb_grouping").val();
        });

        // saat tombol tambah product di tekan

        $("#btn_add_product").on("click", function(){
            $("#list_all_pro").show();
            $("#silahkan").show();
            $("#list_spec_pro").hide();
            $("#judul_grop").hide();
        });

      </script>
</body>
     <!-- END BODY -->
</html>
