<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <?php
            $metatitle = DB::table('nm_generalsetting')->get();
            if($metatitle){
                foreach($metatitle as $metainfo) {
                    $metaname=$metainfo->gs_metatitle;
                    $metakeywords=$metainfo->gs_metakeywords;
                    $metadesc=$metainfo->gs_metadesc;
                }
            }
            else
            {
                $metaname="";
                $metakeywords="";
                $metadesc="";
            }
        ?>
        <title><?php echo $metaname  ;?> | Add User</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />

        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
        <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--END GLOBAL STYLES -->

    </head>
    <!-- END HEAD -->

    <body class="padTop53">
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            {!! $adminheader !!}
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            {!! $adminleftmenus !!}
            <!--END MENU SECTION -->
            <div></div>

            <!-- PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <!-- START Users & Access Management / Users -->
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li><a >Users & Access Management</a></li>
                                <li><a >Users</a></li>
                                <li class="active"><a>Add</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Users & Access Management / Users -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons">
                                        <i class="icon-edit"></i>
                                    </div>
                                    <h5>Add User</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif

                                <div id="div-1" class="accordion-body collapse in body">
                                    <form class="form-horizontal" action="{{url('add_user_submit')}}" method="post" enctype="multipart/form-data">
                                        <div id="error_msg"  style="color:#F00;font-weight:800">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">User Name</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" name="name" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Full Name</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" name="full_name" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Email</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="email" name="email" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2"></label>
                                            <div class="col-lg-4">
                                                <label for=""><input type="checkbox" name="" value="" disabled read-only> Account Active</label>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for=""><input type="checkbox" name="" value="" disabled read-only> Password Expire</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Roles</label>
                                            <div class="col-lg-8">
                                                <select multiple class="form-control" name="roles[]">
                                                    @foreach($roles_list as $role)
                                                    <option value="{{$role->roles_name}}">{{$role->roles_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2"></label>
                                            <div class="col-lg-8">
                                                <button class="btn btn-success btn-sm btn-grad" type="button" data-toggle="modal" data-target="#submit">Submit</button>
                                                <a href="{{url('users')}}"><button class="btn btn-default btn-sm btn-grad" type="button" name="button">Cancel</button></a>
                                            </div>
                                        </div>

                                        <div id="submit" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <p>Are you sure?</p>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="#"><button class="btn btn-success btn-sm btn-grad" type="submit">Yes</button></a>
                                                        <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- FOOTER -->
        {!! $adminfooter !!}
        <!--END FOOTER -->

        <!-- GLOBAL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

    </body>
</html>
