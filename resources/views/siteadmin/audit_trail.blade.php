<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <?php
            $metatitle = DB::table('nm_generalsetting')->get();
            if($metatitle){
                foreach($metatitle as $metainfo) {
                    $metaname=$metainfo->gs_metatitle;
                    $metakeywords=$metainfo->gs_metakeywords;
                    $metadesc=$metainfo->gs_metadesc;
                }
            }
            else
            {
                $metaname="";
                $metakeywords="";
                $metadesc="";
            }
        ?>
        <title><?php echo $metaname  ;?> | Audit Trail</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />

        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
        <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--END GLOBAL STYLES -->
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    </head>
    <!-- END HEAD -->

    <body class="padTop53">
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            {!! $adminheader !!}
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            {!! $adminleftmenus !!}
            <!--END MENU SECTION -->
            <div></div>

            <!-- PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <!-- START Users & Access Management / Users -->
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li><a >Users & Access Management</a></li>
                                <li class="active"><a >Audit Trail</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Users & Access Management / Users -->
                    <!-- Start Search By Date -->
                    <center>
                        <form  action="{!!action('UserManagementController@audit_trail')!!}" method="POST">
                            <input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
                            <div class="row">
                                <br>
                                <div class="col-sm-3">
                                    <div class="item form-group">
                                        <div class="col-sm-6">From Date</div>
                                        <div class="col-sm-6">
                                            <input type="text" name="from_date"  class="form-control" id="datepicker-8">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="item form-group">
                                        <div class="col-sm-6">To Date</div>
                                        <div class="col-sm-6">
                                            <input type="text" name="to_date"  id="datepicker-9" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <input type="submit" name="submit" class="btn btn-block btn-success" value="Search">
                                    </div>
                                </div>

                            </div>
                        </form>
                    </center>
                                 <!-- End Search By Date -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons">
                                        <i class="icon-edit"></i>
                                    </div>
                                    <h5>Audit Trail</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('error') !!}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif

                                <div id="div-1" class="accordion-body collapse in body">
                                    <p></p>
                                    <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
                                        <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 10px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No.</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="User Name: activate to sort column ascending">Table</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Full Name: activate to sort column ascending">Action</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Email: activate to sort column ascending">Alteration (Column: Value)</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Account Active?: activate to sort column ascending">Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Password Expire: activate to sort column ascending">User Name</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php $i=1 ; ?>
                                                @foreach($audittrail as $audit)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$audit->aud_table}}</td>
                                                    <td>{{$audit->aud_action}}</td>
                                                    <td>
                                                        <?php
                                                        $alteration = json_decode($audit->aud_detail);

                                                        ?>
                                                        @foreach($alteration as $key => $value)
                                                        {{$key}}: {{$value}} <br>
                                                        @endforeach
                                                    </td>
                                                    <td>{{$audit->aud_date}}</td>
                                                    <td>{{$audit->aud_user_name}}</td>
                                                </tr>
                                                <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END MAIN WRAPPER -->

        <!-- FOOTER -->
        {!! $adminfooter !!}
        <!--END FOOTER -->

        <!-- GLOBAL SCRIPTS -->
        <script src="assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
   	    <script>
            $(function() {
               $( "#datepicker-8" ).datepicker({
                  prevText:"click for previous months",
                  nextText:"click for next months",
                  showOtherMonths:true,
                  selectOtherMonths: false
               });
               $( "#datepicker-9" ).datepicker({
                  prevText:"click for previous months",
                  nextText:"click for next months",
                  showOtherMonths:true,
                  selectOtherMonths: true
               });
            });
         </script>
    </body>
</html>
