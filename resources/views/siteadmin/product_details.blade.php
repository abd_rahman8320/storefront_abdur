<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?>| Product details</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo url('')?>/assets/css/plan.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


       <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

	<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a >Product details</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Product details</h5>

        </header>
      <?php
      $title = '';
      $category_get = '';
      $maincategory = '';
      $subcategory = '';
      $secondsubcategory = '';
      $originalprice  = '';
      $discountprice  = '';
      $inctax = '';
      $shippingamt = '';
      $description = '';
      $deliverydays = '';
      $metakeyword = '';
      $metadescription = '';
      $file_get = '';
      $file_get_path[0] = '';
      $img_count = '';
      $weight = '';
      $length = '';
      $width = '';
      $height = '';
      $grade = '';
      $color = '';
      if($product_list){
      foreach($product_list as $products)
		{  }
        $title 		 = $products->pro_title;
        if($products->pro_sku=='' || $products->pro_sku==null){
            $sku = $products->pro_sku_merchant;
        }else {
            $sku = $products->pro_sku;
        }
        if(!empty($products->pro_mc_id)){
            $category_get	 = $products->pro_mc_id;
        }
        if(!empty($products->pro_smc_id)){
            $maincategory 	 = $products->pro_smc_id;
        }
        if(!empty($products->pro_sb_id)){
            $subcategory 	 = $products->pro_sb_id;
        }
        if(!empty($products->pro_ssb_id)){
            $secondsubcategory= $products->pro_ssb_id;
        }
        $originalprice  = $products->pro_price;
        $discountprice  = $products->pro_disprice;
        $inctax=$products->pro_inctax;
        $shippingamt=$products->pro_shippamt;
        $description 	 = $products->pro_desc;
        $deliverydays=$products->pro_delivery;
        $metakeyword	 = $products->pro_mkeywords;
        $metadescription= $products->pro_mdesc;
        $file_get  = $products->pro_Img;
        $file_get_path =  explode("/**/",$file_get);
        $img_count		 = $products->pro_image_count;
        $weight = $products->pro_weight;
        $length = $products->pro_length;
        $width = $products->pro_width;
        $height = $products->pro_height;
        if(!empty($products->grade_name)){
            $grade = $products->grade_name;
        }
        if(!empty($products->co_name)){
            $color = $products->co_name;
        }

        $point = $products->pro_poin;

        $pro_metatitle = $products->pro_mtitle;
        $pro_metakeywords = $products->pro_mkeywords;
        $pro_metadescription = $products->pro_mdesc;
        $warranty = $products->have_warranty;
        $pro_id = $products->pro_id;
        $mer_fname = $products->mer_fname;
    }


?>

        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">

                    <form >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        Product details
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Title<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                       <?php echo $title ; ?>
                    </div>
                </div>
                        </div>
                    <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product SKU<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $sku ; ?>
                    </div>
                </div>
            </div>
					 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Top Category<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo $products->mc_name; ?>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Main Category<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      <?php echo $products->smc_name; ?>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Sub Category<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo $products->sb_name; ?>
                    </div>
                </div>
                </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Second Sub Category<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                    <?php echo $products->ssb_name; ?>
                    </div>
                </div>


                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Description<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                            <?php echo $description; ?>
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                   <label class="control-label col-lg-2" for="text1">Product Specification<span class="text-sub">*</span></label>
                   <div class="col-lg-4">
                       <table class="table table-bordered">
                           <thead>
                               <tr>
                                   <th>Spec Name</th>
                                   <th>Spec Value</th>
                               </tr>
                           </thead>
                           <tbody id="tbody_spec">
                               <?php $i=0; foreach($productspecification as $product_spec){?>
                               <tr>
                                   <td>{{$product_spec->sp_name}}</td>
                                   <td>{{$product_spec->spc_value}}</td>
                               </tr>
                               <?php $i++; } ?>
                           </tbody>
                       </table>
                   </div>
               </div>
                       </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Price<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                      {{$get_cur}}. <?php echo number_format($originalprice,2,',','.'); ?>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Discount Price<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                           {{$get_cur}}. <?php echo number_format($discountprice,2,',','.'); ?>
                    </div>
                </div>
                        </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Grade<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $grade ; ?>
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Extended Warranty<span class="text-sub">*</span></label>
                    <div class="col-lg-8">
                        <input type="checkbox" name="extended_warranty" id="extended_warranty" value="{{$warranty}}" <?php if($warranty == 1){echo "checked";}?>> <label class="sample"><?php echo $get_setting_extended[0]->besaran_warranty?> %</label>
                   </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Color<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $color ; ?>
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Weight<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $weight ; ?> kg
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Length<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $length ; ?> cm
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Width<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $width ; ?> cm
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Height<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $height ; ?> cm
                    </div>
                </div>
                </div>
                <?php
                  if($mer_fname == "Kukuruyuk"){
                ?>
                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Meta Title<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $pro_metatitle ; ?>
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Meta Keywords<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $pro_metakeywords ; ?>
                    </div>
                </div>
                </div>

                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Meta Description<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <?php echo $pro_metadescription ; ?>
                    </div>
                </div>
                </div>
                <?php
                  }
                ?>

						 <div class="panel-body" style="display:none;">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Shipping Amount<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                     <?php echo  $shippingamt ?>
                    </div>
                </div>
                        </div>







			 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Product Image<span class="text-sub">*</span></label>
                    <div class="col-lg-4">


                            <img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $file_get_path[0]; ?>">
                            <?php
					 for($j=0 ; $j< $img_count; $j++)
					 { ?>
                     <img style="height:40px;" src="<?php echo url(''); ?>/assets/product/<?php echo $file_get_path[$j+1]; ?>">
                     <?php } ?>
                    </div>
                </div>
                        </div>
                        </div>
		<div class="form-group">
                    <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-2">
                    <a style="color:#fff" href="javascript:history.go(-1)" class="btn btn-success btn-sm btn-grad">Back</a>
                    </div>

                </div>

                </form>
                </div>

        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->
  <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <script type="text/javascript">
        $("#extended_warranty").on("change", function(){
            if($("#extended_warranty").prop('checked'))
            {
                $("#extended_warranty").val(1);

                $.ajax({
                  method: "get",
                  url: "<?php echo url('update_warranty')?>",
                  datatype: "json",
                  data: {
                    id: <?php echo $pro_id;?>,
                    warranty: $("#extended_warranty").val(),
                  },
                  success: function(id){
                    alert("Warranty Updated");
                  }
                });
            }
            else
            {
                $("#extended_warranty").val(0);

                $.ajax({
                  method: "get",
                  url: "<?php echo url('update_warranty')?>",
                  datatype: "json",
                  data: {
                    id: <?php echo $pro_id;?>,
                    warranty: $("#extended_warranty").val(),
                  },
                  success: function(id){
                    alert("Warranty Updated");
                  }
                });
            }
        });
    </script>
    <!-- END GLOBAL SCRIPTS -->

</body>
     <!-- END BODY -->
</html>
