<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
     <?php
         $metatitle = DB::table('nm_generalsetting')->get();
          if($metatitle){
          foreach($metatitle as $metainfo) {
              $metaname=$metainfo->gs_metatitle;
               $metakeywords=$metainfo->gs_metakeywords;
               $metadesc=$metainfo->gs_metadesc;
               }
               }
          else
          {
               $metaname="";
               $metakeywords="";
                $metadesc="";
          }
      ?>

      <title><?php echo $metaname  ;?> | Add Promo</title>
      <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />
         <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link href="<?php echo url('')?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES -->
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/Markdown.Editor.hack.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/jquery.cleditor-hack.css" />
        <link rel="stylesheet" href="<?php echo url('')?>/assets/css/bootstrap-wysihtml5-hack.css" />

    <style>
         ul.wysihtml5-toolbar > li {
            position: relative;
        }

        .dialog-wrapper{
          position: fixed;

          top:0;
          left: 0;
          right: 0;
          bottom: 0;
          overflow: auto;
          background: rgba(50,50,50,.4);
          display: none;
          padding: 15px;
          z-index: 9999;
         }

        .dialog{
          width: 800px;
          background: #fff;
          margin: auto;
          padding: 15px;
          min-width: 200px;
          border: 1px solid #eee;
          position: relative;
          animation: span ease-out 300ms;
        }

        .dialog-head{
          margin-bottom: 10px;

        }

        .dialog-head h4 {
          margin: 0;
          border-bottom: thin solid #ccc;
          color: red;
          margin-bottom: 10px;
        }

        .dialog-body{
          overflow:auto;
        }

        @keyframes span{
          0% {
            transform: scaleX(0) scaleY(0);
          }

          100% {
            transform: scaleX(1) scaleY(1);
          }
         }
    </style>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >
    <!-- MAIN WRAPPER -->
    <div id="wrap" class="wrap">
         <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
         {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

         <!--PAGE CONTENT -->
        <div id="content">
                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            	<ul class="breadcrumb">
                                	<li class=""><a >Home</a></li>
                                  <li class="active"><a >Add Promo</a></li>
                              </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons"><i class="icon-edit"></i></div>
                                    <h5>Add Promo</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('error') !!}
                                </div>
                                @endif
                                <div id="div-1" class="accordion-body collapse in body">
                                <?php
                                      if(isset($action) && $action == 'save') { $process = 'save'; $button = 'Add Plan Timer'; $form_action = 'add_plan_timer_submit'; }
                        		          else if(isset($action) && $action == 'update') { $process = 'update'; $button = 'Update Promo'; }

                                      foreach($get_schedules as $get_schedule)
                                      		{  }
                                           $schedp_title =  $get_schedule->schedp_title;
                                      		 $schedp_start_date = $get_schedule->schedp_start_date;
                                           $schedp_end_date = $get_schedule->schedp_end_date;


                                ?>


                                      {!! Form::open(array('url'=>'edit_plan_timer_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}

                              			   <input type="hidden" name="action" value="<?php echo $process; ?>" />
                                           <input type="hidden" name="id" value="{{$id}}"/>
                                           <input type="hidden" name="url_redirect" value="{{redirect()->back()->getTargetUrl()}}">
                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2"></label>

                                            <div class="col-lg-8">
                                                <div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
                                            </div>
                                        </div>

                                        <?php  ?>
                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Title<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input type="text" class="form-control" name="title" value="<?php echo $schedp_title; ?>"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Start Date<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div id="datetimepicker1" class=" date input-group">
                                                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="startdate" name="startdate" class="form-control" value="<?php echo $schedp_start_date;?>"></input>
                                                    <span class="add-on input-group-addon">
                                                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">End Date<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div id="datetimepicker2" class=" date input-group">
                                                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="enddate" name="enddate" class="form-control" value="<?php echo $schedp_end_date;?>"></input>
                                                    <span class="add-on input-group-addon">
                                                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Recurring<span class="text-sub">*</span></label>
                                                <div class="col-lg-3">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                      <?php if($get_schedule->schedp_RecurrenceType == 2):
                                                              $checked = "checked";
                                                            elseif ($get_schedule->schedp_RecurrenceType == 3):
                                                              $checked = "checked";
                                                            endif;
                                                      ?>
                                                      <div class="radio ">
                                                          <label><input type="radio" class="every_time" id="radio_check" name="opt_time" value="0" <?php if($get_schedule->schedp_RecurrenceType != 2 && $get_schedule->schedp_RecurrenceType != 3){
                                                                  $checked = "checked";
                                                                  echo $checked; }?>>Every Time</label>
                                                      </div>
                                                      <div class="radio ">
                                                          <label><input type="radio" class="dialy" id="radio_check" name="opt_time" value="2" <?php if($get_schedule->schedp_RecurrenceType == 2){
                                                                  $checked = "checked";
                                                                  echo $checked; }?>>Daily</label>
                                                      </div>
                                                      <div class="radio">
                                                        <label><input type="radio" class="weekly" id="radio_check" name="opt_time" value="3" <?php if($get_schedule->schedp_RecurrenceType == 3){
                                                                $checked = "checked";
                                                                echo $checked; }?>>Monthly</label>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-lg-3 " id="dialy_time" style="display:none">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                      Recurr Every
                                                      <input type="number" name="rec_dialy_time" placeholder=" Day(s)" value="<?php
                                                      if($get_schedule->schedp_RecurrenceType == 1){
                                                          echo $get_schedule->schedp_Int1;
                                                        }?>">
                                                    </div>
                                                  </div>
                                                </div>
                                                <?php
                                                if($get_schedule->schedp_RecurrenceType == 2){
                                                    $checked_weekly =  explode( ',', $get_schedule->schedp_Int2);
                                                    $checked_monthly = array(100);
                                                }elseif ($get_schedule->schedp_RecurrenceType == 3) {
                                                    $checked_monthly =  explode( ',', $get_schedule->schedp_Int2);
                                                    $checked_weekly = array(100);
                                                }else {
                                                    $checked_weekly = array(100);
                                                    $checked_monthly = array(100);
                                                }


                                                //dd($checked);
                                                ?>
                                                <div class="col-lg-3 " id="monthly_time" style="display:none">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                      <p>Recurr Every</p>
                                                      <input style="display:none;" type="number" name="rec_monthly_time" placeholder=" Month(s)">
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="0" <?php foreach ($checked_monthly as $value) { if ($value == 0) {
                                                            echo "checked";}} ?>>January</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="1" <?php foreach ($checked_monthly as $value) { if ($value == 1) {
                                                            echo "checked";}} ?>>February</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="2" <?php foreach ($checked_monthly as $value) { if ($value == 2) {
                                                            echo "checked";}} ?>>March</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="3" <?php foreach ($checked_monthly as $value) { if ($value == 3) {
                                                            echo "checked";}} ?>>April</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="4" <?php foreach ($checked_monthly as $value) { if ($value == 4) {
                                                            echo "checked";}} ?>>May</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="5" <?php foreach ($checked_monthly as $value) { if ($value == 5) {
                                                            echo "checked";}} ?>>June</label>
                                                        </div>
                                                      </div>
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="6" <?php foreach ($checked_monthly as $value) { if ($value == 6) {
                                                            echo "checked";}} ?>>July</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="7" <?php foreach ($checked_monthly as $value) { if ($value == 7) {
                                                            echo "checked";}} ?>>August</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="8" <?php foreach ($checked_monthly as $value) { if ($value == 8) {
                                                            echo "checked";}} ?>>September</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="9" <?php foreach ($checked_monthly as $value) { if ($value == 9) {
                                                            echo "checked";}} ?>>October</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="10" <?php foreach ($checked_monthly as $value) { if ($value == 10) {
                                                            echo "checked";}} ?>>November</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" name="rec_month_every[]" value="11" <?php foreach ($checked_monthly as $value) { if ($value == 11) {
                                                            echo "checked";}} ?>>December</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="col-lg-3 " id="weekly_time" style="display:none">
                                                  <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <p>Recurr Every</p>
                                                      <input style="display:none;" type="number" name="rec_weekly_time" placeholder=" Week(s)" value="<?php
                                                      if($get_schedule->schedp_RecurrenceType == 1){
                                                          echo $get_schedule->schedp_Int1;
                                                        }?>">


                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="monday_checkbox" name="rec_week_every[]" value="0" <?php foreach ($checked_weekly as $value) { if ($value == 0) {
                                                            echo "checked";}} ?>>Monday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="tuesday_checkbox" name="rec_week_every[]" value="1" <?php foreach ($checked_weekly as $value) { if ($value == 1) {
                                                            echo "checked";}} ?>>Tuesday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="wednesday_checkbox" name="rec_week_every[]" value="2" <?php foreach ($checked_weekly as $value) { if ($value == 2) {
                                                            echo "checked";}} ?>>Wednesday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="thurshday_checkbox" name="rec_week_every[]" value="3" <?php foreach ($checked_weekly as $value) { if ($value == 3) {
                                                            echo "checked";}} ?>>Thursday</label>
                                                        </div>
                                                      </div>
                                                      <div class="col-lg-6">
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="friday_checkbox" name="rec_week_every[]" value="4" <?php foreach ($checked_weekly as $value) { if ($value == 4) {
                                                            echo "checked";}} ?>>Friday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="saturday_checkbox" name="rec_week_every[]" value="5" <?php foreach ($checked_weekly as $value) { if ($value == 5) {
                                                            echo "checked";}} ?>>Saturday</label>
                                                        </div>
                                                        <div class="checkbox">
                                                          <label><input type="checkbox" id="sunday_checkbox" name="rec_week_every[]" value="6" <?php foreach ($checked_weekly as $value) { if ($value == 6) {
                                                            echo "checked";}} ?>>Sunday</label>
                                                        </div>
                                                      </div>

                                                    </div>
                                                  </div>
                                                </div>

                                                <div id="time" class="col-lg-3 input-group bootstrap-timepicker timepicker" style="display:none;">
                                                    <label>Start Time</label>
                                                    <input id="timepicker4" name="start_time" type="text" class="form-control input-small" placeholder="00:00:00" value="{{$get_schedule->schedp_recurr_start_time}}">
                                                    <label>End Time</label>
                                                    <input id="timepicker5" name="end_time" type="text" class="form-control input-small" placeholder="00:00:00" value="{{$get_schedule->schedp_recurr_end_time}}">
                                                </div>

                                        </div>

                                        <div class="form-group"<?php if($get_schedules[0]->schedp_simple_action == 'flash_promo') { ?> style="display:none;" <?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Picture (200 x 200 pixels)<span class="text-sub">*</span></label>
                                            <div class="col-lg-3" >
                                                <input type="file" name="picture" value="">
                                                <img src="{{url('assets/promo')}}/{{$get_schedule->schedp_picture}}" alt="gambar promo" height="200px" width="200px">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text1" class="control-label col-lg-2">Tipe Promo<span class="text-sub">*</span></label>
                                            <div class="col-lg-3">
                                                <?php if($get_schedule->schedp_simple_action=='free_shipping') {
                                                        $simple_action = 'Free Shipping';
                                                    }
                                                    if($get_schedule->schedp_simple_action=='by_fixed') {
                                                        $simple_action = 'Discounted By Fixed Amount';
                                                    }
                                                    if($get_schedule->schedp_simple_action=='by_percent') {
                                                        $simple_action = 'Discounted By Percent';
                                                    }
                                                    if($get_schedule->schedp_simple_action=='buy_x_get_y') {
                                                        $simple_action = 'Buy x Get y';
                                                    }
                                                    if($get_schedule->schedp_simple_action=='flash_promo') {
                                                        $simple_action = 'Flash Promo';
                                                    }
                                                    ?>
                                                <input type="text" name="simple_action_text" value="{{$simple_action}}" class="form-control" readonly disabled>
                                                <input id="simple_action" name="simple_action" type="hidden" class="form-control" value="<?php echo $get_schedule->schedp_simple_action;?>" readonly></input>

                                            </div>
                                        </div>
                                        <?php
                                          if($get_schedule->jenis_voucher == "statis")
                                          {
                                            ?>
                                              <div class="form-group">
                                                  <label for="text1" class="control-label col-lg-2">Voucher Code<span class="text-sub">*</span></label>

                                                  <div class="col-lg-3" >
                                                      <div >
                                                          <input name="voucher_code" type="text" class="form-control" value="<?php echo $get_promo_coupons->promoc_voucher_code;?>"></input>
                                                      </div>
                                                  </div>
                                              </div>
                                            <?php
                                          }
                                          else
                                          {
                                            ?>
                                              <input type="hidden" name="voucher_code" value="">
                                            <?php
                                          }

                                        ?>

                                        <div class="form-group" id="shipping_list" <?php if($get_schedules[0]->schedp_simple_action != 'free_shipping') { ?> style="display:none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Jenis Pengiriman<span class="text-sub">*</span></label>
                                            <div class="col-lg-3" >
                                                <div >
                                                    <select class="form-control" name="ps_id">
                                                        <option value="0">Semua Pengiriman</option>
                                                        <?php
                                                                foreach($list_postal_services as $list_postal_sevices2){
                                                            ?>
                                                                <option <?php if($get_schedule->schedp_ps_id==$list_postal_sevices2->ps_id) { ?> selected <?php } ?>value='<?php echo $list_postal_sevices2 -> ps_id;?>'><?php echo $list_postal_sevices2 -> ps_code;?></option>
                                                            <?php
                                                                }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="minimal_transaction" style="display:none;">
                                            <label for="text1" class="control-label col-lg-2">Minimal Transaction<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="minimal_transaction" name="minimal_transaction" type="number" class="form-control" placeholder="Rp" value="{{$get_promo_coupons->promoc_minimal_transaction}}"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="voucher_limit" <?php if($get_schedules[0]->schedp_simple_action == 'flash_promo') { ?> style="display: none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Voucher Usage Limit<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="voucher_limit" name="voucher_limit" type="number" class="form-control" placeholder="" value="{{$get_promo_coupons->promoc_usage_limit}}"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="customer_voucher_limit" <?php if($get_schedules[0]->schedp_simple_action == 'flash_promo') { ?> style="display: none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Voucher Usage Limit Per Customer<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="customer_voucher_limit" name="customer_voucher_limit" type="number" class="form-control" placeholder="" value="{{$get_promo_coupons->promoc_usage_per_customer}}"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="maximum_shipping_div" <?php if($get_schedules[0]->schedp_simple_action != 'free_shipping') { ?> style="display:none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Maximum Shipping<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="maximum_shipping" name="maximum_shipping" type="number" class="form-control" placeholder="Rp" value="{{$get_schedule->maximum_free_shipping}}"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="discounted_percentage" <?php if($get_schedules[0]->schedp_simple_action != 'by_percent') { ?> style="display:none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Discounted Percentage<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="discounted_percentage_input" name="discounted_percentage" type="number" class="form-control" placeholder="%" value="<?php if($get_promo_product!=null){ echo $get_promo_product[0]->promop_discount_percentage;}?>"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="discounted_price" <?php if($get_schedules[0]->schedp_simple_action != 'by_fixed') { ?> style="display:none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Discounted Amount<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input id="discounted_price_input" name="discounted_price" type="number" class="form-control" placeholder="Rp" value="<?php if($get_promo_product!=null){ echo $get_promo_product[0]->promop_saving_price;}?>"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="nilai_x" <?php if($get_schedules[0]->schedp_simple_action != 'buy_x_get_y') { ?> style="display:none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Discount Qty Step (Buy X)<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input name="nilai_x" type="number" class="form-control" placeholder="" value="<?php echo $get_schedules[0]->schedp_x;?>"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="nilai_y" <?php if($get_schedules[0]->schedp_simple_action != 'buy_x_get_y') { ?> style="display:none;"<?php } ?>>
                                            <label for="text1" class="control-label col-lg-2">Discount Amount (Get Y)<span class="text-sub">*</span></label>

                                            <div class="col-lg-3" >
                                                <div >
                                                    <input name="nilai_y" type="number" class="form-control" placeholder="" value="<?php echo $get_schedules[0]->schedp_y;?>"></input>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="product" <?php if($get_schedules[0]->schedp_simple_action == 'free_shipping') { ?> style="display:none;"<?php } ?>>
                                          <label for="text1" class="control-label col-lg-2">Product<span class="text-sub">*</span></label>

                                            <div class="col-lg-8 ">
                                              <div style="margin-bottom:5px;">
                                                <button data-toggle="tooltip" title="Add Product" type="button" class="btn btn-default add-btn-default <?php if($get_schedules[0]->schedp_simple_action == 'flash_promo') { ?> disabled <?php } ?>"><a style="color:blue">Add Product</a></button>
                                              </div>

                                              <div class="row panel panel-default" id="product_2" style="height:300px; overflow: scroll;" >
                                                  <div class="panel-body row">
                                                        <div class="col-lg-12">
                                                          <table class="table table-striped table-bordered table-hover dataTable no-footer" id="header_table">
                                                            <thead>
                                                              <tr role="row">
                                                                    <th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0"  aria-sort="ascending">S.No</th>
                                                                    <th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Product Name</th>
                                                                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Original Price({{$get_cur}})</th>
                                                                    <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" > Image </th>
                                                                    <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Actions</th>
                                                                    <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Stock</th>
                                                                </tr>
                                                            </thead>
                                                            <?php //dd($get_products); ?>


                                                  <tbody id="prodCont" class="tbody_ori">
                                                    <tr class="tr_ori_bgt">
                                                    <td class="no_tr_ori_bgt"></td>
                                                    <td class="nama_tr_ori_bgt"></td>
                                                    <td class="oriPrice_tr_ori_bgt"></td>
                                                    <td class="img_tr_ori_bgt"><img src="<?php echo url('/assets/product/');?>" alt="Lights" style="height:40px;"></td>
                                                    <td><button type="button" name="button" class="btn btn-default btn-remove">Remove</button>
                                                    <input style="display:none;" type="checkbox" name="product[]" class="pilih pilih_ori" checked></td>
                                                    <td class="stock_ori"></td>
                                                  </tr>

                                                              <?php $j=0; if($get_products!=''){ foreach ($get_products as $value): ?>

                                                                <?php if($value!=null){ $product_get_img = explode("/**/",$value->pro_Img); ?>
                                                              <tr class="tr_ori">
                                                                <td></td>
                                                                <td><?php echo $value->pro_title; ?></td>
                                                                <td><?php echo number_format($value->pro_price,2,',','.'); ?></td>
                                                                <td><img src="<?php echo url(''); ?>/assets/product/<?php echo $product_get_img[0]; ?>" alt="Lights" style="height:40px;"></td>
                                                                <td><button type="button" name="button" class="btn btn-default btn-remove-ori" data-id="{{$value->pro_id}}">Remove</button></td>
                                                                <input style="display:none;" type="checkbox" name="product[]" class="pilih" id="products_input" value="{{$value->pro_id}}" checked>
                                                                <input style="display:none;" type="hidden" name="promoPrice[]" class="pilih" value="{{$get_promo_product[$j]->promop_saving_price}}">
                                                                <input style="display:none;" type="hidden" name="qty[]" class="pilih" value="{{$get_promo_product[$j]->promop_qty}}">
                                                                <td><?php echo $value->pro_qty - $value->pro_no_of_purchase; ?></td>
                                                              </tr>
                                                          <?php $j++; } endforeach; } ?>
                                                            </tbody>

                                                          </table>
                                                        </div>

                                                  </div>
                                                </div>
                                            </div>
                                          </div>

                                        <div class="form-group">
                                                <label for="pass1" class="control-label col-lg-2"><span  class="text-sub"></span></label>

                                                <div class="col-lg-8">
                                                   <button class="btn btn-warning btn-sm btn-grad" id="submit_deal" ><a style="color:#fff"  ><?php echo $button; ?></a></button>
                                                   <a style="color:#000" href="{{redirect()->back()->getTargetUrl()}}"><button class="btn btn-default btn-sm btn-grad" type="button">Cancel</button></a>
                                                </div>
                                            </div>
                                      </form>
                                  </div>
                              </div>
                        </div>
                    </div>
                </div>
        </div>
            <!--END PAGE CONTENT -->
    </div>


    <div class="dialog-wrapper">
        <div class="dialog">
            <div class="dialog-head">
                <h4 class="">Pilih Product</h4>
            </div>
            <div class="dialog-body row">
              <div>
                <input type="search" id="input_search" name="search" placeholder="Search Me Please !!"><input id="btn_search" type="submit" name="btn_search" value="cari">

                <button type="button" name="button" id="prev">Prev</button>
                <button type="button" name="button" id="next">Next</button>

                <input type="checkbox" class="select_all">Select All

                <select name="select_cat" class="kategori" id="kategori">
                <option value="">-Semua Kategory-</option>
                <?php foreach($productcategory as $category){?>
                    <option value="<?php echo $category->mc_name; ?>"><?php echo $category->mc_name; ?></option>
                <?php  } ?>
                </select>

                <select name="select_mer" class="select_mer" id="select_mer">
                <option value="">-Semua Merchant-</option>
                <?php foreach($productmerchant as $merchant){?>
                    <option value="<?php echo $merchant->mer_id; ?>"><?php echo $merchant->mer_fname; ?></option>
                <?php  } ?>
                </select>

                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                  <thead >
                    <tr role="row">
                        <th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0"  aria-sort="ascending">S.No</th>
                        <th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Product Name</th>
                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Original Price({{$get_cur}})</th>
                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" > Image </th>
                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Actions</th>
                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 40px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" >Stock</th>
                        <th class="head_promoPrice" aria-label="CSS grade: activate to sort column ascending" style="width: 180px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Promo Price</th>
                        <th class="head_qty" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0">Qty</th>
                        <th class="head_mer" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" aria-controls="dataTables-example" tabindex="0">Merchant</th>
                      </tr>
                  </thead>
                  <tbody id="prodCont_dialog">
                    <tr class="tr_dialog">
                      <td class="no_dialog"></td>
                      <td class="title_dialog"></td>
                      <td class="original_price_dialog"></td>
                      <td class="deals_image_dialog"><img style="height:40px;" src=""  alt=""></td>
                      <td><input class="checkbox_id" type="checkbox"></td>
                      <td class="stock"></td>
                      <td class="promoPrice" style="width:100px;"><input type="number" name="promoPrice[]"/></td>
                      <td class="qty" style="width:100px;"><input type="number" style="width:100%;" name="qty[]"/></td>
                      <td class="name_mer"></td>
                    </tr>
                  </tbody>
                </table>

              </div>


            </div>
        </div>
    </div>

     <!--END MAIN WRAPPER -->

  <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->

 	<script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/timepicker/css/bootstrap-timepicker.min.css"></script>
    <script src="<?php echo url('')?>/assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
  <script type="text/javascript">
  $('.disable').click(function(e){
    e.preventDefault();
  });

  $(function(){
      $('#timepicker4').timepicker();
  });

  $(function(){
      $('#timepicker5').timepicker();
  });
  $('#timepicker4').timepicker({
    minuteStep: 1,
    template: 'modal',
    appendWidgetTo: 'body',
    showSeconds: true,
    showMeridian: false,
    defaultTime: false
});
$('#timepicker5').timepicker({
  minuteStep: 1,
  template: 'modal',
  appendWidgetTo: 'body',
  showSeconds: true,
  showMeridian: false,
  defaultTime: false
});
  function number_format(number,decimals,dec_point,thousands_sep) {
      number  = number*1;//makes sure `number` is numeric value
      var str = number.toFixed(decimals?decimals:0).toString().split('.');
      var parts = [];
      for ( var i=str[0].length; i>0; i-=3 ) {
          parts.unshift(str[0].substring(Math.max(0,i-3),i));
      }
      str[0] = parts.join(thousands_sep?thousands_sep:',');
      return str.join(dec_point?dec_point:'.');
  }

  $('#all_products').on('click', function(){
      var all_products = document.getElementById('all_products');
      if(all_products.checked){
          $('#product').hide();
      }else {
          $('#product').show();

      }
  });
  $('#submit_deal').on('click', function(){
    //  alert('test');
     if($('#simple_action').val()!='free_shipping'){
         if(typeof $('#products_input').val() == 'undefined'){
            $('#product_2').css('border', '1px solid red');
 			$('#error_msg').html('Product Must Be Fill');
 			$('#product_2').focus();
 			return false;
        }else {
            $('#product_2').css('border', '');
            $('#error_msg').html('');
            return true;
        }
     } else {
         $('#product_2').css('border', '');
         $('#error_msg').html('');
         return true;
     }
  });
  $('#simple_action').on('change', function(){
      var value = $('#simple_action').val()
      if(value=='by_fixed'){
          $('#discounted_percentage').hide();
          $('#discounted_price').show();
          $('#shipping_list').hide();
          $('#div_all_products').show();
          $('#product').show();
          $('#nilai_x').hide();
          $('#nilai_y').hide();
          $('#maximum_shipping_div').hide();
      }else if(value=='by_percent'){
          $('#discounted_percentage').show();
          $('#discounted_price').hide();
          $('#shipping_list').hide();
          $('#div_all_products').show();
          $('#product').show();
          $('#nilai_x').hide();
          $('#nilai_y').hide();
          $('#maximum_shipping_div').hide();
      }else if(value=='free_shipping'){
          $('#discounted_percentage').hide();
          $('#discounted_price').hide();
          $('#shipping_list').show();
          $('#div_all_products').hide();
          $('#product').hide();
          $('#nilai_x').hide();
          $('#nilai_y').hide();
          $('#maximum_shipping_div').show();
      }else if (value=='buy_x_get_y') {
          $('#discounted_percentage').hide();
          $('#discounted_price').hide();
          $('#shipping_list').hide();
          $('#div_all_products').hide();
          $('#product').show();
          $('#nilai_x').show();
          $('#nilai_y').show();
          $('#maximum_shipping_div').hide();
      }else if (value=='flash_promo') {
          $('#voucher').hide();
          $('#minimal_transaction').hide();
          $('#voucher_limit').hide();
          $('#customer_voucher_limit').hide();
          $('#shipping_list').hide();
          $('#maximal_shipping_div').hide();
          $('#maximal_shipping_div').hide();
          $('#discounted_price').hide();
          $('#product').show();
          $('#nilai_x').hide();
          $('#nilai_y').hide();
      }
  });
  //$('#header_table').hide();
  var tr_ori_bgt    = $('.tr_ori_bgt');
  $('.tr_ori_bgt').remove();
  var tr_dialog = $('.tr_dialog');

    var skip = 0;
    var count = 10;
    var daftar_produk_terpilih = [];
    var ori_checkeds = $('.pilih:checked');
    for (var i = 0; i < ori_checkeds.length; i++) {
      //if (daftar_produk_terpilih[i] != ori_checkeds[i].value ) {
        daftar_produk_terpilih.push(parseInt(ori_checkeds[i].value));
      //}

    }
    $('.btn-remove-ori').click(function(){
        var id_remove = parseInt(this.getAttribute('data-id'));
        console.log(id_remove);
        this.closest('tr').remove();
        $(".pilih[value='"+id_remove+"']").remove();
        daftar_produk_terpilih.splice(daftar_produk_terpilih.indexOf(id_remove),1);
    });

    function reload_product(){

      var prodCont = $('#prodCont_dialog');
      var inputan = $('#input_search').val();
      var kategori = $('.kategori').val();
      var value = $('#simple_action').val();
      var merchant = $('#select_mer').val();

      $.get("../search_product", {
                                pro_title:inputan,
                                kategori:kategori,
                                skip:skip,
                                merchant:merchant,
                                count:count,
                              }).then(function(result){
        prodCont.empty();
         if(result.data.length > 0){
            $('.table-product').show();
            $('.text_not_found').remove();

                for(i=0;i<result.data.length;i++)
                {
                    if(result.data[i].pro_qty > 0){
                        var clone = tr_dialog.clone(); // mengklone tr
                        clone.find('.no_dialog').html(skip+i+1);
                        clone.find('.title_dialog').html(result.data[i].pro_title);
                        clone.find('.shop_dialog').html(result.data[i].pro_sh_id);
                        if(result.data[i].pro_disprice != 0){
                            clone.find('.original_price_dialog').html(number_format(result.data[i].pro_disprice, 2, ',', '.'));
                        }else{
                            clone.find('.original_price_dialog').html(number_format(result.data[i].pro_price, 2, ',', '.'));
                        }
                        if (daftar_produk_terpilih.indexOf(result.data[i].pro_id) >= 0) {
                          clone.find('.checkbox_id').attr('checked','checked');
                        }
                        clone.find('.checkbox_id').val(result.data[i].pro_id);

                        var img_urls = (result.data[i].pro_Img).split('/**/');
                        if (img_urls.length > 0) {
                          clone.find('.deals_image_dialog img').attr('src', '../assets/product/'+img_urls[0]);
                        }
                        clone.find('.stock').html(result.data[i].pro_qty);
                        clone.find('.name_mer').html(result.data[i].mer_fname);
                        //memasukan data ke html elemen td

                        if(value != 'flash_promo'){
                           clone.find('.promoPrice').remove();
                           clone.find('.qty').remove();
                           $('.head_promoPrice').css('display', 'none');
                           $('.head_qty').css('display', 'none');
                         }else{
                           $('.head_promoPrice').removeAttr('style');
                           $('.head_qty').removeAttr('style');
                         }
                        prodCont.append(clone); // memasukan td_clone kedalam prodCont
                    }else{
                            $('.table-product').hide();
                            var text = "<p class='text_not_found' style='text-align:center; margin-top: 30px;margin-bottom:30px;'><strong>Data Not found</strong></p>";

                            if($('.text_not_found').length == 0){
                                $('.content').append(text);
                            }
                        }
                    }
                }else{
                    $('.table-product').hide();
                    var text = "<p class='text_not_found' style='text-align:center; margin-top: 30px;margin-bottom:30px;'><strong>Data Not found</strong></p>";

                    if($('.text_not_found').length == 0){
                        $('.content').append(text);
                    }
                }

        if (skip <= 0) {
          $('#prev').prop('disabled', true);
        }else{
          $('#prev').prop('disabled', false);
        }

        if (skip >= result.total-count) {
          $('#next').prop('disabled', true);
        }else{
          $('#next').prop('disabled', false);
        }
      });
    }

    $('#btn_search').on('click', function(){
        reload_product();
    });
    $('#input_search').keyup(function() {
        reload_product();
    });
    $('select[name=select_cat]').change(function() { //alert($(this).val());
        reload_product();
    });
    $('.select_all').change(function() {
        //reload_product();
         $(".checkbox_id").prop('checked', $(this).prop("checked"));
    });
    $('#next').on('click', function(){
      skip = skip + count;
      reload_product();
    });
    $('#prev').on('click', function(){
      skip = skip - count;
      reload_product();
    });
    $("#select_mer").on("change", function(){
      reload_product();
    });
  </script>
    <script>
      $(document).ready(function(){

        var tbody_ori = $('.tbody_ori');

        $("#dialy_time").hide();
        $("#weekly_time").hide();
        $('#monthly_time').hide();

        $(".dialog-wrapper").click(function(){
            //reload_product();
            $(".dialog-wrapper").hide();
            $('#header_table').show();
            var checks_inputs =   $('.dialog .checkbox_id:checked');
            // console.log(checks_inputs);
            // prodCont.empty();
            //daftar_produk_terpilih=[];
            for (var i = 0; i < checks_inputs.length; i++) {

              var check_input = checks_inputs[i];
              var id_prod = parseInt(check_input.value);
              var tr_dialog_closest = $(check_input.closest('.tr_dialog'));
              var no_dialog = tr_dialog_closest.find('.no_dialog');
              var colom_title = tr_dialog_closest.find('.title_dialog');
              var colom_shop = tr_dialog_closest.find('.shop_dialog');
              var colom_original = tr_dialog_closest.find('.original_price_dialog');
              var colom_img = tr_dialog_closest.find('.deals_image_dialog img') ;
              var stock = tr_dialog_closest.find('.stock');




              if (daftar_produk_terpilih.indexOf(id_prod) < 0) {
                daftar_produk_terpilih.push(id_prod);
                var clone_tr_ori_bgt = tr_ori_bgt.clone();
                var clone_no = clone_tr_ori_bgt.find('.no_tr_ori_bgt');
                var clone_title = clone_tr_ori_bgt.find('.nama_tr_ori_bgt');
                var clone_store = clone_tr_ori_bgt.find('.store_name_tr_ori_bgt');
                var clone_oriPrice = clone_tr_ori_bgt.find('.oriPrice_tr_ori_bgt');
                var clone_img = clone_tr_ori_bgt.find('.img_tr_ori_bgt img');
                var clone_pilih_ori = clone_tr_ori_bgt.find('.pilih_ori');
                var clone_stock_ori = clone_tr_ori_bgt.find('.stock_ori');

                clone_no.html(no_dialog.html());
                clone_title.html(colom_title.html());
                clone_store.html(colom_shop.html());
                clone_oriPrice.html(colom_original.html());
                clone_img.attr('src',colom_img.attr('src'));
                clone_pilih_ori.val(id_prod);
                clone_stock_ori.html(stock.html());
                tbody_ori.append(clone_tr_ori_bgt);

                var clone_remove = clone_tr_ori_bgt.find('.btn-remove');
                clone_remove.attr('data-id', id_prod);
                clone_remove.click(function(){
                    var id_remove = parseInt(this.getAttribute('data-id'));
                    console.log(id_remove);
                    this.closest('tr').remove();
                    daftar_produk_terpilih.splice(daftar_produk_terpilih.indexOf(id_remove),1);
                });
              }


            }


            console.log(daftar_produk_terpilih);
        });

        $(".dialog").click(function(e){
            e.stopPropagation();
        });

          var radio_dialy = $('.dialy');
          var radio_weekly = $('.weekly');
          var radio_every = $('.every_time');
          if (radio_dialy.is(':checked')) {
            console.log("Nongol nih");
            $("#weekly_time").fadeIn();
            $('#time').fadeIn();
          }
          if (radio_weekly.is(':checked')) {
            $("#monthly_time").fadeIn();
          }

          $(".every_time").click(function(){
              $("#weekly_time").hide();
              $("#monthly_time").hide();
              $('#time').hide();
          });
          $(".dialy").click(function(){
              $("#weekly_time").fadeIn();
              $("#monthly_time").hide();
              $('#time').show();
          });
          $(".weekly").click(function(){
              $("#monthly_time").fadeIn();
              $("#weekly_time").hide();
              $('#time').hide();
          });
          $(".add-btn-default").click(function(){
              $(".dialog-wrapper").show();
              reload_product();
          });
      });
    </script>


    <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

   <script src="<?php echo url('')?>/assets/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">


            $(function () {
                $('#datetimepicker1').datetimepicker();
            });

			$(function () {
                $('#datetimepicker2').datetimepicker();
            });

			$(function () {
                $('#datetimepicker3').datetimepicker();
            });
    </script>

         <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo url('')?>/assets/js/editorInit.js"></script>
    <script>
       $(function () { formWysiwyg(); });
    </script>
    <!---F12 Block Code---->

</body>
     <!-- END BODY -->
</html>
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>
