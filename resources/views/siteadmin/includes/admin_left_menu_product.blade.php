   <?php $current_route = Route::getCurrentRoute()->getPath(); ?>
    <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading">PRODUCTS</h5>

                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'products_dashboard')
                <li <?php if($current_route == 'product_dashboard' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('product_dashboard');?>">
                        <i class="icon-dashboard"></i>&nbsp;Products Dashboard</a>
                </li>
                <?php break; ?> @endif
                @endforeach

  <li <?php if($current_route == 'add_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> style="display:none;" >

                    <a href="<?php echo url('add_product');?>" >
                        <i class=" icon-plus-sign"></i>&nbsp;Add Products
	                </a>
                </li>

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_products')
                <li <?php if($current_route == 'manage_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('manage_product');?>" >
                        <i class=" icon-edit"></i>&nbsp; Manage Products
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                <li <?php if($current_route == 'sepulsa_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('sepulsa_product');?>" >
                        <i class=" icon-edit"></i>&nbsp; Sepulsa Products
                   </a>
                </li>

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'approval_products')
                <li <?php if($current_route == 'approval_product'){ ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('approval_product');?>">
                        <i class="icon-ok"></i>&nbsp; Approval Products
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'sold_products')
	 <li <?php if($current_route == 'sold_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('sold_product');?>" >
                        <i class="icon-tag"></i>&nbsp; Sold Products
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

				 <li style="display:none;" <?php if( $current_route == "manage_product_shipping_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('manage_product_shipping_details');?>" >
                        <i class="icon-ambulance"></i>&nbsp;Shipping And Delivery
                   </a>
                </li>
			<?php $general=DB::table('nm_generalsetting')->get(); foreach($general as $gs) {} if($gs->gs_payment_status == 'COD') { ?> 	<li style="display:none;" <?php if( $current_route == "manage_cashondelivery_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('manage_cashondelivery_details');?>" >
                        <i class="icon-money"></i>&nbsp;Cash On Delivery
                   </a>
                </li> <?php } ?>

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_product_reviews')
                <li <?php if($current_route == 'manage_review' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('manage_review');?>" >
                        <i class=" icon-edit"></i>&nbsp; Manage Product Reviews
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'add_grouping_view')
                <li <?php if($current_route == 'add_groping_page' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('add_groping_page');?>" >
                        <i class=" icon-edit"></i>&nbsp; Add Grouping View
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_grouping_view')
                <li <?php if($current_route == 'halaman_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('halaman_product');?>" >
                        <i class=" icon-edit"></i>&nbsp; Manage Grouping View
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_grouping_model')
                <li <?php if($current_route == 'manage_model' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('manage_model');?>" >
                        <i class=" icon-edit"></i>&nbsp; Manage Grouping Model
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'product_without_picture')
                <li <?php if($current_route == 'manage_product_pic' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('manage_product_pic');?>" >
                        <i class=" icon-edit"></i>&nbsp; Uncompleted Product
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'excel_data_product')
                <li <?php if($current_route == 'upload_excel_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?>>
                    <a href="<?php echo url('upload_excel_product');?>">
                        <i class="icon-arrow-up"></i>&nbsp; Download / Upload Data Product
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

            </ul>

        </div>
