<?php  $current_route = Route::getCurrentRoute()->getPath(); ?>
<div id="left">
    @foreach($privileges as $priv)
    <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
    @if($priv_name[1] == 'electronic_payment' || $priv_name[1] == 'cicilan_columbia' || $priv_name[1] == 'transfer_bank' || $priv_name[1] == 'merchant_outbound')
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading"> TRANSACTIONS </h5>

                </div>
                <br />
            </div>
            <?php break; ?> @endif
            @endforeach

            <ul id="menu" class="collapse">

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'electronic_payment')
                <li <?php if($current_route=="product_all_orders" || $current_route=="product_success_orders" || $current_route=="product_completed_orders" || $current_route=="product_failed_orders" || $current_route=="product_hold_orders" ){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav2">
                        <i class="icon-dropbox"></i>&nbsp;Electronic Payment
                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
	                </a>
                     <ul class="collapse" id="form-nav2">
                         <li class=""><a href="<?php echo url('product_all_orders');?>"><i class="icon-angle-right"></i> Semua Transaksi </a></li>
                      <li class=""><a href="<?php echo url('product_success_orders');?>"><i class="icon-angle-right"></i> Sudah Dibayar</a></li>
                    <?php /*?><li class=""><a href="<?php echo url('product_completed_orders');?>"><i class="icon-angle-right"></i>Completed Orders </a></li><?php */?>
                      <!-- <li class=""><a href="<?php //echo url('product_hold_orders');?>"><i class="icon-angle-right"></i> Hold Orders</a></li> -->
                      <li class=""><a href="<?php echo url('product_failed_orders');?>"><i class="icon-angle-right"></i> Expired</a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

    <?php $general=DB::table('nm_generalsetting')->get();
    foreach($general as $gs) {} ?>
    @foreach($privileges as $priv)
    <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
    @if($priv_name[1] == 'cicilan_columbia')
            <li <?php

            if($current_route=="columbia_all_orders" || $current_route=="columbia_success_orders" || $current_route=="columbia_completed_orders" || $current_route=="columbia_failed_orders" || $current_route=="columbia_hold_orders" )
                {
                    echo 'class="panel active"';
                }
            else
                {
                    echo 'class="panel"';
                } ?>>

            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav4">

            <i class="icon-money"></i>&nbsp;Cicilan Columbia
                <span class="pull-right">
                    <i class="icon-angle-right"></i>
                </span>
            </a>
            <ul class="collapse" id="form-nav4">
                <li class=""><a href="<?php echo url('columbia_all_orders');?>"><i class="icon-angle-right"></i> Semua Pesanan</a></li>
                       <!--li class=""><a href="<?php //echo url('cod_completed_orders');?>"><i class="icon-angle-right"></i> Completed Orders </a></li-->
                <li class=""><a href="<?php echo url('columbia_hold_orders');?>"><i class="icon-angle-right"></i> Belum Dibayar</a></li>
                <li class=""><a href="<?php echo url('columbia_success_orders');?>"><i class="icon-angle-right"></i> Sudah Dibayar</a></li>
                <li class=""><a href="<?php echo url('columbia_failed_orders');?>"><i class="icon-angle-right"></i> Expired</a></li>

            </ul>

            </li>
            <?php break; ?> @endif
            @endforeach

            <?php $general=DB::table('nm_generalsetting')->get();
    foreach($general as $gs) {} ?>
    @foreach($privileges as $priv)
    <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
    @if($priv_name[1] == 'transfer_bank')
            <li <?php

            if($current_route=="tfr_bank_all_orders" || $current_route=="tfr_bank_success_orders" || $current_route=="tfr_bank_failed_orders" || $current_route=="columbia_failed_orders" || $current_route=="tfr_bank_hold_orders" )
                {
                    echo 'class="panel active"';
                }
            else
                {
                    echo 'class="panel"';
                } ?>>

            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav5">

            <i class="icon-money"></i>&nbsp;Transfer Bank
                <span class="pull-right">
                    <i class="icon-angle-right"></i>
                </span>
            </a>
            <ul class="collapse" id="form-nav5">
                <li class=""><a href="<?php echo url('tfr_bank_all_orders');?>"><i class="icon-angle-right"></i> Semua Pesanan</a></li>
                       <!--li class=""><a href="<?php //echo url('cod_completed_orders');?>"><i class="icon-angle-right"></i> Completed Orders </a></li-->
                <li class=""><a href="<?php echo url('tfr_bank_hold_orders');?>"><i class="icon-angle-right"></i> Belum Dibayar</a></li>
                <li class=""><a href="<?php echo url('tfr_bank_success_orders');?>"><i class="icon-angle-right"></i> Sudah Dibayar</a></li>

                <li class=""><a href="<?php echo url('tfr_bank_failed_orders');?>"><i class="icon-angle-right"></i> Expired</a></li>

            </ul>

            </li>
            <?php break; ?> @endif
            @endforeach

            @foreach($privileges as $priv)
            <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
            @if($priv_name[1] == 'cash_payment')
                    <li <?php

                    if($current_route=="cash_all_orders" || $current_route=="cash_success_orders" || $current_route=="cash_failed_orders" || $current_route=="cash_hold_orders" )
                        {
                            echo 'class="panel active"';
                        }
                    else
                        {
                            echo 'class="panel"';
                        } ?>>

                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav6">

                    <i class="icon-money"></i>&nbsp;Cash Payment
                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="form-nav6">
                        <li class=""><a href="<?php echo url('cash_all_orders');?>"><i class="icon-angle-right"></i> Semua Pesanan</a></li>
                               <!--li class=""><a href="<?php //echo url('cod_completed_orders');?>"><i class="icon-angle-right"></i> Completed Orders </a></li-->
                        <li class=""><a href="<?php echo url('cash_hold_orders');?>"><i class="icon-angle-right"></i> Belum Dibayar</a></li>
                        <li class=""><a href="<?php echo url('cash_success_orders');?>"><i class="icon-angle-right"></i> Sudah Dibayar</a></li>

                        <li class=""><a href="<?php echo url('cash_failed_orders');?>"><i class="icon-angle-right"></i> Expired</a></li>

                    </ul>

                    </li>
                    <?php break; ?> @endif
                    @endforeach

            @foreach($privileges as $priv)
            <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
            @if($priv_name[1] == 'merchant_outbound')
            <li <?php if($current_route=="merchant_all_transaction" || $current_route=="product_completed_orders" || $current_route=="product_failed_orders" || $current_route=="product_hold_orders" ){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="<?php echo url('merchant_all_transaction');?>" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav6">
                        <i class="icon-dropbox"></i>&nbsp;Outbound Status
                         <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

    </ul>
    @foreach($privileges as $priv)
    <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
    @if($priv_name[1] == 'all_fund_requests' || $priv_name[1] == 'unpaid_fund_requests' || $priv_name[1] == 'paid_fund_requests' || $priv_name[1] == 'confirmed_fund_requests')
             <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading"> FUND REQUESTS</h5>

                </div>
                <br />
            </div>
            <?php break; ?> @endif
            @endforeach

            <ul id="menu" class="collapse">
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'all_fund_requests')
                <li <?php if($current_route=="all_fund_request"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="<?php echo url('all_fund_request'); ?>" >
                        <i class="icon-arrow-right"></i>&nbsp; All Fund requests</a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'unpaid_fund_requests')
                <li <?php if($current_route=="unpaid_fund_request"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                   <a href="<?php echo url('unpaid_fund_request');?>">
                       <i class="icon-ban-circle"></i>&nbsp;Unpaid Fund requests
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'paid_fund_requests')
                <li <?php if($current_route=="paid_fund_request"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="<?php echo url('paid_fund_request');?>">
                        <i class="icon-mail-reply-all"></i>&nbsp;Paid Fund requests
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'confirmed_fund_requests')
                   <li <?php if($current_route=="confirmed_fund_request"){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="<?php echo url('confirmed_fund_request');?>">
                        <i class="icon-ok"></i>&nbsp;Confirmed Fund requests
	                </a>
                </li>
                <?php break; ?> @endif
                @endforeach




            </ul>


        </div>
