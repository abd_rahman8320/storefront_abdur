   <?php  $current_route = Route::getCurrentRoute()->getPath(); ?>
  <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading">Promo</h5>

                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'promo_dashboard')
                <li <?php if($current_route == 'deals_dashboard' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?>>
                    <a href="<?php echo url('deals_dashboard'); ?>">
                    <i class="icon-dashboard"></i>&nbsp;Promo Dashboard</a>
                </li>
                <?php break; ?> @endif
                @endforeach

                <li style="display:none;" <?php if($current_route == 'add_deals' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?> >
                  <a href="<?php echo url('add_deals'); ?>" >
                      <i class=" icon-plus-sign"></i>&nbsp;Add Promo
                  </a>
                </li>

                <li style="display:none;" <?php if($current_route == 'manage_deals' || $current_route == 'deal_details/{id}' || $current_route == 'edit_deals/{id}' )  { ?> class="panel active" <?php } else { echo 'class="panel"';  } ?>>
                  <a href="<?php echo url('manage_deals'); ?>" >
                    <i class=" icon-edit"></i>&nbsp;Manage Promo
                  </a>
                </li>

				        <li <?php if($current_route == 'expired_deals' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?>>
                    <a href="<?php echo url('expired_deals'); ?>" >
                        <i class="icon-check-sign"></i>&nbsp;Expired Promo
                    </a>
                </li>

                <li  style="display:none;" <?php if($current_route == 'manage_deal_review' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?>>
                  <a href="<?php echo url('manage_deal_review'); ?>" >
                        <i class="icon-check-sign"></i>&nbsp;Manage Deal Reviews
                  </a>
                </li>

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'add_schedule_promo')
                <li <?php if($current_route == 'add_plan_timer' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?> >
                  <a href="<?php echo url('add_plan_timer'); ?>" >
                      <i class=" icon-plus-sign"></i>&nbsp;Add Schedule Promo
                  </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_schedule_promo')
                <li <?php if($current_route == 'manage_schedule' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?> >
                  <a href="<?php echo url('manage_schedule'); ?>" >
                      <i class=" icon-edit"></i>&nbsp;Manage Schedule Promo
                  </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_kupon')
                <li <?php if($current_route == 'manage_coupons' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?> >
                  <a href="<?php echo url('manage_coupons'); ?>" >
                      <i class=" icon-edit"></i>&nbsp;Manage Kupon
                  </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'report_promo')
                <li <?php if($current_route == 'report_promo' ) { ?> class="panel active" <?php } else { echo 'class="panel"';} ?> >
                  <a href="<?php echo url('report_promo'); ?>" >
                      <i class="icon-arrow-up"></i>&nbsp;Report Promo
                  </a>
                </li>
                <?php break; ?> @endif
                @endforeach

				<!-- <li  <?php //if($current_route == 'validate_coupon_code' ) { ?> class="panel active" <?php //} else { echo 'class="panel"';} ?>>
                    <a href="<?php //echo url('validate_coupon_code'); ?>" >
                        <i class="icon-barcode"></i>&nbsp;Validate Coupon Code
                   </a>
                </li>
				 <li  <?php //if($current_route == 'redeem_coupon_list' ) { ?> class="panel active" <?php //} else { echo 'class="panel"';} ?>>
                    <a href="<?php //echo url('redeem_coupon_list'); ?>" >
                        <i class="icon-asterisk"></i>&nbsp;Redeem Coupon List
                   </a>
                </li>-->
            </ul>

        </div>
