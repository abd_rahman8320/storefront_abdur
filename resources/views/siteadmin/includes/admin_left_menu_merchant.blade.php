  <?php  $current_route = Route::getCurrentRoute()->getPath(); ?>
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading"> MERCHANTS </h5>

                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                 <li style="display: none;"> <?php if($current_route == 'merchant_dashboard'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?> >
                    <a href="<?php echo url('merchant_dashboard'); ?>" >
                        <i class="icon-dashboard"></i>&nbsp; Merchants Dashboard</a>
                </li>
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'add_merchants_account')
                   <li <?php if($current_route == 'add_merchant'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?> >
                    <a href="<?php echo url('add_merchant'); ?>" >
                        <i class="icon-user"></i>&nbsp;Add Merchant Account
	                </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'manage_merchant_accounts')
                 <li <?php if($current_route == 'manage_merchant'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="<?php echo url('manage_merchant'); ?>" >
                        <i class="icon-ok-sign"></i>&nbsp;Manage Merchant Accounts
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'approval_merchant')
				<li <?php if($current_route == 'approval_merchant'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="<?php echo url('approval_merchant'); ?>" >
                        <i class="icon-ok-sign"></i>&nbsp;Approval Merchant
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach
            </ul>


        </div>
