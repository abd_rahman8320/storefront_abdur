 <?php $current_route = Route::getCurrentRoute()->getPath(); ?>
  <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading">Users & Access Management</h5>

                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'privileges')
                <li style="display:none;" <?php if( $current_route == "privileges" || $current_route == "view_privileges/{id}" || $current_route == "add_privileges" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('privileges'); ?>">
                        <i class="icon-cog"></i>&nbsp;Privileges
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'roles')
                <li <?php if( $current_route == "roles" || $current_route == "view_roles/{id}" || $current_route == "add_roles" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('roles'); ?>">
                        <i class="icon-cog"></i>&nbsp;Roles
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'users')
                <li <?php if( $current_route == "users" || $current_route == "view_users/{id}" || $current_route == "add_users") { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('users'); ?>">
                        <i class="icon-cog"></i>&nbsp;Users
                    </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'audit_trail')
                <li <?php if($current_route == 'audit_trail'){ ?> class="panel active" <?php } else{ echo 'class="panel"'; }?>>
                    <a href="{{url('audit_trail')}}">
                        <i class="icon-cog"></i>&nbsp;Audit Trail
                    </a>
                </li>
                <?php break; ?>@endif
                @endforeach

            </ul>

        </div>
<!---Right Click Block Code---->
<script language="javascript">
document.onmousedown=disableclick;
status="Cannot Access for this mode";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;
   }
}
</script>


<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>
