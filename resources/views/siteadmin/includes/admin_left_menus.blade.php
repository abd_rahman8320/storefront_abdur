 <?php $current_route = Route::getCurrentRoute()->getPath(); ?>
  <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading">SETTINGS</h5>

                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'general')
                <li <?php if( $current_route == "general_setting" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('general_setting'); ?>">
                        <i class="icon-cog"></i>&nbsp;General Settings</a>
                </li>
                <?php break; ?>
                @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'email_contact')
                   <li <?php if( $current_route == "email_setting" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('email_setting'); ?>" >
                        <i class="icon-envelope"></i>&nbsp;Email & Contact Settings
	                </a>
                </li>
                <?php break; ?>
                @endif
                @endforeach
                 <!--li <?php //if( $current_route == "smtp_setting" ) //{ ?> class="panel active"  <?php //} else { echo 'class="panel"';  }?>>
                    <a href="<?php //echo url('smtp_setting'); ?>" >
                        <i class="icon-mail-reply"></i>&nbsp;SMTP Mailer Settings
                   </a>
                </li-->
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'social_media')
				 <li <?php if( $current_route == "social_media_settings" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('social_media_settings'); ?>" >
                        <i class="icon-facebook"></i>&nbsp;Social Media Page Settings
                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'payment')
				<li <?php if( $current_route == "payment_settings" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('payment_settings'); ?>" >
                        <i class="icon-credit-card"></i>&nbsp;Payment settings


                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'extended_warranty')
                <li <?php if( $current_route == "extended_warranty" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('extended_warranty'); ?>" >
                        <i class="icon-credit-card"></i>&nbsp;Extended Warranty


                   </a>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'bank_akun')
                <li <?php if( $current_route == "add_bank_akun" || $current_route == "bank_akun_kuku" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >

                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#bank-nav">

                    <i class=" icon-globe"></i> Bank Akun Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>

                    <ul <?php if( $current_route == "add_bank_akun" || $current_route == "bank_akun_kuku" )
                    { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="bank-nav">

                        <li <?php if( $current_route == "add_bank_akun" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_bank_akun'); ?>"><i class="icon-angle-right"></i>Add Bank Akun </a></li>

                        <li <?php if( $current_route == "bank_akun_kuku" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('bank_akun_kuku'); ?>"><i class="icon-angle-right"></i> Manage Bank Akun </a></li>

                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

				<?php /*?> <li  <?php if( $current_route == "module_settings" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="<?php echo url('module_settings'); ?>" >
                        <i class="icon-table"></i>&nbsp;Modules Setting
                   </a>
                </li><?php */?>
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'no_image' || $priv_name[1] == 'favicon' || $priv_name[1] == 'logo')
                <li class="panel">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                        <i class="icon-picture"> </i> Image Settings
                        <span class="pull-right">
                          <i class="icon-angle-right"></i>
                        </span>
                       <!--&nbsp; <span class="label label-default">3</span>&nbsp;-->
                    </a>
                    <ul class="collapse" id="component-nav">

                        <li class=""><a href="<?php echo url('add_logo'); ?>"><i class="icon-angle-right"></i> Logo Settings </a></li>
                         <li class=""><a href="<?php echo url('add_favicon'); ?>"><i class="icon-angle-right"></i> Favicon Settings </a></li>
                        <li class=""><a href="<?php echo url('add_noimage'); ?>"><i class="icon-angle-right"></i> No-Image Settings </a></li>
                        <!--   <li class=""><a href="tabs_panels.html"><i class="icon-angle-right"></i> Image zoom settings </a></li>
                      <li class=""><a href="notifications.html"><i class="icon-angle-right"></i> Notification </a></li>
                         <li class=""><a href="more_notifications.html"><i class="icon-angle-right"></i> More Notification </a></li>
                        <li class=""><a href="modals.html"><i class="icon-angle-right"></i> Modals </a></li>
                          <li class=""><a href="wizard.html"><i class="icon-angle-right"></i> Wizard </a></li>
                         <li class=""><a href="sliders.html"><i class="icon-angle-right"></i> Sliders </a></li>
                        <li class=""><a href="typography.html"><i class="icon-angle-right"></i> Typography </a></li> -->
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'popup_image')
                <li <?php if( $current_route == "add_popup_image" || $current_route == "manage_popup_image" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pop-nav">
                        <i class="icon-camera"></i> Pop Up Image Settings

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>
                    <ul <?php if( $current_route == "add_popup_image" || $current_route == "manage_popup_image" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="pop-nav">
                        <li <?php if( $current_route == "add_popup_image" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_popup_image'); ?>"><i class="icon-angle-right"></i> Add PopUp Image </a></li>
                        <li <?php if( $current_route == "manage_popup_image" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_popup_image'); ?>"><i class="icon-angle-right"></i> Manage PopUp Images </a></li>

                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'banner_image')
                <li <?php if( $current_route == "add_banner_image" || $current_route == "manage_banner_image" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#form-nav">
                        <i class="icon-camera"></i> Banner Image Settings

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>
                    <ul <?php if( $current_route == "add_banner_image" || $current_route == "manage_banner_image" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="form-nav">
                        <li <?php if( $current_route == "add_banner_image" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_banner_image'); ?>"><i class="icon-angle-right"></i> Add Banner Image </a></li>
                        <li <?php if( $current_route == "manage_banner_image" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_banner_image'); ?>"><i class="icon-angle-right"></i> Manage Banner Images </a></li>

                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'active_payment')
                <li <?php if( $current_route == "manage_payment_aktif_setting" || $current_route == "manage_payment_aktif_setting" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pay-aktif">
                        <i class="icon-camera"></i> Payment Active Settings

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>
                    <ul <?php if( $current_route == "manage_payment_aktif_setting" || $current_route == "manage_payment_aktif_setting" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="pay-aktif">
                        <li <?php if( $current_route == "manage_payment_aktif_setting" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_payment_aktif_setting'); ?>"><i class="icon-angle-right"></i> Manage Active Payment </a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'payment_method_image')
                <li <?php if( $current_route == "add_payment_method_image" || $current_route == "manage_payment_method_image" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pay-nav">
                        <i class="icon-camera"></i> Payment Method Image Settings

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>
                    <ul <?php if( $current_route == "add_payment_method_image" || $current_route == "manage_payment_method_image" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="pay-nav">
                        <li <?php if( $current_route == "add_payment_method_image" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_payment_method_image'); ?>"><i class="icon-angle-right"></i> Add Payment Method Images </a></li>
                        <li <?php if( $current_route == "manage_payment_method_image" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_payment_method_image'); ?>"><i class="icon-angle-right"></i> Manage Payment Method Images </a></li>

                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'attributes')
                <li  <?php if( $current_route == "add_size" || $current_route == "manage_size" || $current_route == "add_color" || $current_route == "manage_color" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>  >
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pagesr-nav">
                        <i class="icon-anchor"></i> Attributes Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>
                    <ul <?php if( $current_route == "add_size" || $current_route == "manage_size" || $current_route == "add_color" || $current_route == "manage_color" || $current_route == "add_grade" || $current_route == "manage_grade") { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="pagesr-nav">
                        <li <?php if( $current_route == "add_color" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_color'); ?>"><i class="icon-angle-right"></i> Add Color </a></li>
                        <li <?php if( $current_route == "manage_color" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_color'); ?>"><i class="icon-angle-right"></i> Manage Colors </a></li>
                        <li style="display:none;" <?php if( $current_route == "add_size" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_size'); ?>"><i class="icon-angle-right"></i> Add Size </a></li>
                        <li style="display:none;" <?php if( $current_route == "manage_size" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_size'); ?>"><i class="icon-angle-right"></i> Manage Sizes </a></li>
                        <li <?php if( $current_route == "add_grade" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_grade'); ?>"><i class="icon-angle-right"></i> Add Grade </a></li>
                        <li <?php if( $current_route == "manage_grade" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_grade'); ?>"><i class="icon-angle-right"></i> Manage Grade </a></li>

                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'specification')
  <li <?php if( $current_route == "add_specification" || $current_route == "manage_specification" || $current_route == "add_specification_group" || $current_route == "manage_specification_group" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav">
                        <i class="icon-bar-chart"></i> Spec Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>

			 <ul class="collapse" id="chart-nav">
                        <li><a href="<?php echo url('add_specification_group'); ?>"><i class="icon-angle-right"></i> Add specification group </a></li>
                        <li><a href="<?php echo url('manage_specification_group'); ?>"><i class="icon-angle-right"></i> Manage specification group</a></li>
                        <li><a href="<?php echo url('add_specification'); ?>"><i class="icon-angle-right"></i> Add specification </a></li>
                        <li><a href="<?php echo url('manage_specification'); ?>"><i class="icon-angle-right"></i> Manage specification</a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'country')
 <li <?php if( $current_route == "add_country" || $current_route == "manage_country" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >


                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#DDL-nav">
                        <i class=" icon-globe"></i> Countries Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>

		  <ul <?php if( $current_route == "add_country" || $current_route == "manage_country" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="DDL-nav">

                        <li <?php if( $current_route == "add_country" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_country'); ?>"><i class="icon-angle-right"></i>Add Country </a></li>
                        <li <?php if( $current_route == "manage_country" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_country'); ?>"><i class="icon-angle-right"></i> Manage Country </a></li>
                       <!--  <li><a href="#"><i class="icon-angle-right"></i> Demo Link 4 </a></li> -->
                    </ul>
		 </li>
         <?php break; ?> @endif
         @endforeach

         @foreach($privileges as $priv)
         <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
         @if($priv_name[1] == 'city')
                <li <?php if( $current_route == "add_city" || $current_route == "manage_city" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#DDL4-nav">
                        <i class=" icon-building"></i> Cities Management

                    <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>
                    <ul <?php if( $current_route == "add_city" || $current_route == "manage_city" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="DDL4-nav">

                        <li <?php if( $current_route == "add_city" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_city'); ?>"><i class="icon-angle-right"></i>Add City </a></li>
                        <li <?php if( $current_route == "manage_city" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_city'); ?>"><i class="icon-angle-right"></i> Manage Cities </a></li>
                       <!--  <li><a href="#"><i class="icon-angle-right"></i> Demo Link 4 </a></li> -->
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'category')
                <li <?php if( $current_route == "add_category" || $current_route == "manage_category" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#error-nav">
                        <i class="icon-plus"></i> Category Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>

                    </a>
                    <ul <?php if( $current_route == "add_category" || $current_route == "manage_category" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?> id="error-nav">
                        <li <?php if( $current_route == "add_category" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_category'); ?>"><i class="icon-angle-right"></i> Add Category </a></li>
                        <li <?php if( $current_route == "manage_category" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_category'); ?>"><i class="icon-angle-right"></i> Manage Categories </a></li>

                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'cms')
                   <li <?php if( $current_route == "add_cms_page" || $current_route == "manage_cms_page" || $current_route == "aboutus_page" || $current_route == "terms" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav2">
                        <i class="icon-pencil"></i> CMS Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                    <ul  <?php if( $current_route == "add_cms_page" || $current_route == "manage_cms_page" || $current_route == "aboutus_page" || $current_route == "terms" ) { ?> class="in"  <?php } else { echo 'class="collapse"';  }?>  id="chart-nav2">

                        <li <?php if( $current_route == "add_cms_page" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('add_cms_page'); ?>"  ><i class="icon-angle-right"></i> Add Page</a></li>
                        <li  <?php if( $current_route == "manage_cms_page" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('manage_cms_page'); ?>" ><i class="icon-angle-right"></i> Manage CMS Pages</a></li>
                        <li  <?php if( $current_route == "aboutus_page" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('aboutus_page'); ?>"><i class="icon-angle-right"></i> About Us</a></li>
                         <li  <?php if( $current_route == "terms" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('terms'); ?>"><i class="icon-angle-right"></i> Terms & Conditions</a></li>
                         <li  <?php if( $current_route == "mer_terms" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('mer_terms'); ?>"><i class="icon-angle-right"></i> Terms & Conditions Merchant</a></li>
                         <li  <?php if( $current_route == "mer_thanks_reg" ) { ?> class="active"  <?php } else { echo 'class=""';  }?>><a href="<?php echo url('mer_thanks_reg'); ?>"><i class="icon-angle-right"></i> Register Success Merchant</a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'ads')
				<li <?php if( $current_route == "add_ad" || $current_route == "manage_ad" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> style="display:none;" >
                    <a  data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav1">
                    <i class="icon-external-link-sign"></i> Ads Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                    <ul class="collapse" id="chart-nav1">
                        <li><a href="<?php echo url('add_ad'); ?>"><i class="icon-angle-right"></i> Add Ads</a></li>
                        <li><a href="<?php echo url('manage_ad'); ?>"><i class="icon-angle-right"></i> Manage Ads</a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                <!--- Partners --->
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'partners')
                <li <?php if( $current_route == "add_partners" || $current_route == "manage_partners" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >
                    <a  href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#test1">
                    <i class="icon-external-link-sign"></i> Partners Management

                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                    <ul class="collapse" id="test1">
                        <li><a href="<?php echo url('add_partners'); ?>"><i class="icon-angle-right"></i> Add Partners</a></li>
                        <li><a href="<?php echo url('manage_partners'); ?>"><i class="icon-angle-right"></i> Manage Partners</a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach
                <!-- End Partners -->

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'faq')
				<li <?php if( $current_route == "add_faq" || $current_route == "manage_faq" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >
                  <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav3">
                        <i class="icon-question-sign"></i> FAQ

                  <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                  <ul class="collapse" id="chart-nav3">
                    <li><a href="<?php echo url('add_faq'); ?>"><i class="icon-angle-right"></i> Add FAQ</a></li>
                        <li><a href="<?php echo url('manage_faq'); ?>"><i class="icon-angle-right"></i> Manage FAQ</a></li>
                    </ul>
                </li>
                <?php break; ?> @endif
                @endforeach

                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[1] == 'newsletter')
				<li <?php if( $current_route == "manage_newsletter_subscribers" || $current_route == "send_newsletter" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?> >
                    <a  data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav4">
                        <i class="icon-signin"></i> News Letter
                        <span class="pull-right">
                            <i class="icon-angle-right"></i>
                        </span>
                          &nbsp; <span class="label label-danger"></span>&nbsp;
                    </a>
                    <ul class="collapse" id="chart-nav4">
                        <li><a href="<?php echo url('send_newsletter'); ?>"><i class="icon-angle-right"></i> Send Newsletter</a></li>
                        <li><a href="<?php echo url('manage_newsletter'); ?>"><i class="icon-angle-right"></i> Manage Newsletter</a></li>
                        <li><a href="<?php echo url('manage_newsletter_subscribers'); ?>"><i class="icon-angle-right"></i> Manage subscribed users</a></li>

                    </ul>
              </li>
              <?php break; ?> @endif
              @endforeach

               <!--<li <?php //if( $current_route == "add_estimated_zipcode" ) { ?> class="panel active"  <?php //} else { echo 'class="panel"';  }?>>
                    <a href="<?php //echo url('add_estimated_zipcode');?>" >
                        <i class="icon-circle-arrow-right"></i>&nbsp;Add Estimated Zipcode
                   </a>
                </li>
                <li <?php //if( $current_route == "estimated_zipcode" ) { ?> class="panel active"  <?php //} else { echo 'class="panel"';  }?>>
                    <a href="<?php //echo url('estimated_zipcode');?>" >
                        <i class="icon-circle-arrow-right"></i>&nbsp;Estimated Zipcode
                   </a>
                </li>-->


            </ul>

        </div>
<!---Right Click Block Code---->
<script language="javascript">
document.onmousedown=disableclick;
status="Cannot Access for this mode";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;
   }
}
</script>


<!---F12 Block Code---->
<script type='text/javascript'>
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){
      return false;  //Prevent from ctrl+shift+i
   }
});
</script>
