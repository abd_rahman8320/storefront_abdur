<?php  $current_route = Route::getCurrentRoute()->getPath();
   ?>
  <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
                </a> -->

                <div class="media-body">
                    <h5 class="media-heading">USER MANAGEMENT</h5>

                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                <li <?php if($current_route == 'manage_admin'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?> >
                    <a href="<?php echo url('manage_admin'); ?>" >
                    <i class="icon-dashboard"></i>&nbsp; Manage Admin</a>
                </li>
                <li <?php if($current_route == 'privileges'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?> >
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                        <i class="icon-picture"> </i> Privileges
                        <span class="pull-right">
                          <i class="icon-angle-right"></i>
                        </span>                       
                    </a>
                </li>
            </ul>

        </div>
