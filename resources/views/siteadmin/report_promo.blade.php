<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
         $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> | Export/Import Data Products</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
	<link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
	<link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/bootstrap-wysihtml5-hack.css" />
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
	<style>
		ul.wysihtml5-toolbar > li {
			position: relative;
		}
	</style>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->


		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a>Download / Upload Data Products</a></li>
                            </ul>
                    </div>
                </div>
<div class="row">
    <div class="col-lg-12">
        <div class="box dark">
            <header>
                <div class="icons"><i class="icon-edit"></i></div>
                <h5>Download / Upload Data Products</h5>

            </header>

            <div class="row" style="margin-top: 25px; margin-bottom: 15px;">
                <div class="col-lg-12">
                    <div id="div-1" class="accordion-body collapse in body" style="margin-left: 20px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Report
                            </div>
                            <br>
                            <label for="" style="margin-left: 20px;">Report Promo</label>
                            <p></p>
                            <form class="form-horizontal" action="{{url('report_promo_download')}}" method="post" style="margin-left: 20px;">
                                <div class="form-group">
                                    <label for="" class="control-label col-lg-4">Tanggal</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="from_date" placeholder="From"  class="form-control" id="datepicker-8">
                                    </div>

                                    <div class="col-sm-2">
                                        <input type="text" name="to_date" placeholder="To"  id="datepicker-9" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 10px;">
                                    <button type="submit" name="submit" class="btn btn-warning btn-grad">Download</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     {!! $adminfooter !!}
    <!--END FOOTER -->

    <!-- script -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript">
        $('#top_category').on('change', function(){
            var mc_id = $('#top_category').val();
            $.ajax({
                type: 'get',
                data: {
                    mc_id: mc_id
                },
                url: '{{url("get_secmaincategory_ajax")}}',
                success: function(data){
                    $('#main_category').empty();
                    $('#main_category').append("<option value=''>All</option>");
                    for(var i=0;i<data.length;i++)
                    {
                        $('#main_category').append("<option value='"+data[i].smc_id+"'>"+data[i].smc_name+"</option>");
                    }
                }
            });
        });
        $('#main_category').on('change', function(){
            var smc_id = $('#main_category').val();
            $.ajax({
                type: 'get',
                data: {
                    smc_id: smc_id
                },
                url: '{{url("get_subcategory_ajax")}}',
                success: function(data){
                    $('#sub_category').empty();
                    $('#sub_category').append("<option value=''>All</option>");
                    for(var i=0;i<data.length;i++)
                    {
                        $('#sub_category').append("<option value='"+data[i].sb_id+"'>"+data[i].sb_name+"</option>");
                    }
                }
            });
        });
        $(function() {
           $( "#datepicker-8" ).datepicker({
              prevText:"click for previous months",
              nextText:"click for next months",
              showOtherMonths:true,
              selectOtherMonths: false
           });
           $( "#datepicker-9" ).datepicker({
              prevText:"click for previous months",
              nextText:"click for next months",
              showOtherMonths:true,
              selectOtherMonths: true
           });
        });
    </script>

</body>
</html>
