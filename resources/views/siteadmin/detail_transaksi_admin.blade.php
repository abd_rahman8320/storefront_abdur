<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |  Dashboard</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
         <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo url('');?>/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo url('');?>/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo url('');?>/stylesheet" href="assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo url(); ?>/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	{!!$adminheader!!}
        <!-- END HEADER SECTION -->



        <!-- MENU SECTION -->
       <div id="left" >


        </div>
        <!--END MENU SECTION -->
		<div class="container">
        	<div class="row">


                </div>

        </div>


        <!--PAGE CONTENT -->
        <div class=" container" >
            <div class="inner" style="min-height: 700px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                        	<header>
                <div class="icons"><i class="icon-dashboard"></i></div>
                <h5>Detail Transaksi</h5>
            </header>
              <?php
$sold_cnt=0;
 foreach($soldproductscnt as $soldres){
	if($soldres->pro_no_of_purchase	>=$soldres->pro_qty)
		{
		$sold_cnt++;
		}


	}
?>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <b>Detail Transaksi <?php echo $transaction_header->transaction_id;?></b>
                            </div>
                            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Product Name</th>
                                            <th>Product Quantity</th>
                                            <th>Amount</th>
                                            <th>Shipping</th>
                                            <th>Extended Warranty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $i = 1;
                                            $total_transaksi = 0;
                                            foreach ($transaction_detail as $row) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td>
                                                            <?php
                                                            if ($row->pro_title == 'Jasa Pengiriman' || $row->pro_title == 'Biaya Pengiriman') {
                                                                echo 'Total Biaya Pengiriman';
                                                            }else {
                                                                if ($row->order_is_cicilan == 1) {
                                                                    echo $row->pro_title.' <b style="color:green;">(Cicilan)</b>';
                                                                }else {
                                                                    echo $row->pro_title;
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <?php
                                                            if($row->pro_title == "Jasa Pengiriman" || $row->pro_title == "Diskon" || $row->pro_title == "Biaya Pengiriman")
                                                            {
                                                                ?>
                                                                  <td></td>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                    <td><?php echo $row->order_qty;?></td>
                                                                <?php
                                                            }
                                                        ?>
                                                        @if($row->pro_title == 'Jasa Pengiriman' || $row->pro_title == 'Biaya Pengiriman')
                                                        <td></td>
                                                        @else
                                                        <td style="text-align: right;"><span style="float: left;">Rp.</span><?php echo number_format($row->order_amt);?></td>
                                                        @endif
                                                        <?php
                                                            if($row->pro_title == "Diskon")
                                                            {
                                                                ?>
                                                                  <td></td>
                                                                <?php
                                                            }
                                                            elseif ($row->pro_title == "Jasa Pengiriman" || $row->pro_title == "Biaya Pengiriman") {
                                                                ?>
                                                                    <td style="text-align: right;"><span style="float: left;">Rp.</span><?php echo number_format($row->order_amt);?></td>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                    <td style="text-align: right;"><span style="float: left;">Rp.</span><?php echo number_format($row->order_shipping_price);?></td>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if($row->pro_title == "Jasa Pengiriman" || $row->pro_title == "Diskon" || $row->pro_title == "Biaya Pengiriman")
                                                            {
                                                                ?>
                                                                  <td></td>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                    <td style="text-align: right;"><span style="float: left;">Rp.</span><?php echo number_format($row->order_jumlah_warranty);?></td>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tr>
                                                <?php
                                                $i++;
                                                $total_transaksi = $total_transaksi + floatval($row->order_amt) + floatval($row->order_jumlah_warranty);
                                            }
                                        ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td>Poin</td>
                                            <td></td>
                                            <td style="text-align: right;"><span style="float: left;">Rp.</span><?php echo number_format($transaction_header->poin_digunakan);?></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Jumlah</td>
                                            <td colspan="3  " style="text-align: right;"><span style="float: left;">Rp.</span><?php echo number_format(floatval($total_transaksi) - floatval($transaction_header->poin_digunakan));?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                            </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <b>Transaksi <?php echo $transaction_header->transaction_id;?></b>
                            </div>
                            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Total Belanja</th>
                                            <th>Diskon</th>
                                            <th>Shipping</th>
                                            <th>Metode Pembayaran</th>
                                            <th>Status Pembayaran</th>
                                            <th>Status Outbound</th>
                                            <th>Poin yang Didapat</th>
                                            <th>Poin yang Digunakan</th>
                                            <?php
                                                if($transaction_header->metode_pembayaran == "Columbia")
                                                {
                                                    ?>
                                                    <th>Uang Muka</th>
                                                    <th>Lama Cicilan</th>
                                                    <th>Admin Fee</th>
                                                    <th>Cicilan Perbulan</th>
                                                    <th>Approval</th>
                                                    <?php
                                                }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $status_outbound = "";

                                            if($transaction_header->status_outbound_transaksi == 1)
                                            {
                                                $status_outbound = "Belum Dilihat";
                                            }
                                            elseif($transaction_header->status_outbound_transaksi == 2)
                                            {
                                                $status_outbound = "Picking";
                                            }
                                            elseif($transaction_header->status_outbound_transaksi == 3)
                                            {
                                                $status_outbound = "Packing";
                                            }
                                            elseif($transaction_header->status_outbound_transaksi == 4)
                                            {
                                                $status_outbound = "Shipping";
                                            }
                                            elseif($transaction_header->status_outbound_transaksi == 5)
                                            {
                                                $status_outbound = "Receiving";
                                            }
                                        ?>
                                        <tr>
                                            <td style="text-align: right;"><?php echo number_format($transaction_header->total_belanja);?></td>
                                            <td style="text-align: right;"><?php echo number_format($transaction_header->diskon);?></td>
                                            <td style="text-align: right;"><?php echo number_format($transaction_header->shipping);?></td>
                                            <td style="text-align: center;"><?php echo $metode_pembayaran;?></td>
                                            <td style="text-align: center;"><?php echo $transaction_header->status_pembayaran;?></td>
                                            <td style="text-align: center;"><?php echo $status_outbound;?></td>
                                            <td style="text-align: center;"><?php echo $transaction_header->total_poin;?></td>
                                            <td style="text-align: center;"><?php echo $transaction_header->poin_digunakan;?></td>

                                            <?php
                                                if($transaction_header->metode_pembayaran == "Columbia")
                                                {
                                                    ?>
                                                        <td style="text-align: right;"><?php echo number_format($transaction_header->transaksi_dp);?></td>
                                                        <td style="text-align: center;"><?php echo $transaction_header->lama_cicilan;?></td>
                                                        <td style="text-align: right;"><?php echo number_format($transaction_header->adminfee);?></td>
                                                        <td style="text-align: right;"><?php echo number_format($transaction_header->cicilan_perbulan);?></td>
                                                        <td style="text-align: center;"><?php if($transaction_header->approval_columbia == 1){echo "Disetujui";}elseif($transaction_header->status_pembayaran == "expired"){echo "Tidak Disetujui";} else{echo "Menunggu Persetujuan";}?></td>
                                                    <?php
                                                }
                                            ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            </div>
                            </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <b>Outbound Status Transaksi <?php echo $transaction_header->transaction_id;?></b>
                            </div>
                            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nama Product</th>
                                            <th>Jumlah Barang</th>
                                            <th>Nama Penjual</th>
                                            <th>Telphone Penjual</th>
                                            <th>Pesanan Dibuat</th>
                                            <th>Pesanan Dibayarkan</th>
                                            <th>Pesanan Diproses</th>
                                            <th>Pesanan Dikemas</th>
                                            <th>Pesanan Dikirim</th>
                                            <th>Metode Pengiriman</th>
                                            <th>Nomor Resi</th>
                                            <th>Pesanan Diterima</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                            foreach ($get_history_transaksi as $history) {
                                                if ($history->pro_title != 'Jasa Pengiriman' && $history->pro_title != 'Biaya Pengiriman' && $history->pro_title != 'Diskon') {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $history->pro_title;?></td>
                                                        <td style="text-align: center;"><?php echo $history->order_qty?></td>
                                                        <td><?php echo $history->mer_fname;?></td>
                                                        <td><?php echo $history->mer_phone;?></td>
                                                        <td><?php echo $history->order_date;?></td>
                                                        <td><?php echo $history->order_tgl_pesanan_dibayarkan;?></td>
                                                        <td><?php echo $history->order_tgl_pesanan_diproses;?></td>
                                                        <td><?php echo $history->order_tgl_pesanan_dikemas;?></td>
                                                        <td><?php echo $history->order_tgl_pesanan_dikirim;?></td>
                                                        <td><?php echo $history->postalservice_code?></td>
                                                        <td><?php echo $history->order_nomor_resi;?></td>
                                                        <td><?php echo $history->order_tgl_konfirmasi_penerimaan_barang;?></td>
                                                    </tr>
                                                <?php
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>


                            </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; Kukuruyuk <?php echo date('Y'); ?> &nbsp;</p>
    </div>
    <!--END FOOTER -->
    <script>
	$(document).ready(function(){

	plot6 = $.jqplot('chart6', [[<?php echo $get_chart6_details; ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer} } );
		});
	</script>
      <script>
	$(document).ready(function(){

	plot10 = $.jqplot('chart10', [[<?php echo $activeproductscnt; ?>,<?php echo $sold_cnt; ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer} });
		});
	</script>


    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;

		<?php $s1 = "[" .$cus_count. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });

        $('#chart1').bind('jqplotDataClick',
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;

		<?php $s1 = "[" .$transaction_chart. "]"; ?>
        var s1 = <?php echo $s1; ?>;
        var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        plot1 = $.jqplot('chart5', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });

        $('#chart5').bind('jqplotDataClick',
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    <script class="include" type="text/javascript" src="<?php echo url(); ?>/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo url(); ?>/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo url(); ?>/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo url(); ?>/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo url(); ?>/assets/js/chart/jqplot.pointLabels.min.js"></script>


    <!-- GLOBAL SCRIPTS -->

     <script src="<?php echo url('');?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('');?>/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url('');?>/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo url('');?>/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo url('');?>/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo url('');?>/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo url('');?>/assets/plugins/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo url('');?>/assets/js/bar_chart.js"></script>

    <!-- END PAGE LEVEL SCRIPTS -->


</body>

    <!-- END BODY -->
</html>
