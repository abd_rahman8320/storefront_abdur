<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if ($metatitle) {
    	foreach($metatitle as $metainfo) {
			$metaname=$metainfo->gs_metatitle;
			$metakeywords=$metainfo->gs_metakeywords;
			$metadesc=$metainfo->gs_metadesc;
		}
	}
    else {
		$metaname="";
		$metakeywords="";
		$metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> | Edit Products</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
	<link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
	<link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/bootstrap-wysihtml5-hack.css" />
	<style>
		ul.wysihtml5-toolbar > li {
			position: relative;
		}
	</style>
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->


		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a>Home</a></li>
                                <li class="active"><a >Edit Products</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Edit Products</h5>

        </header>
@if ($errors->any())
         <br>
		 <ul style="color:red;">
		{!! implode('', $errors->all('<li>:message</li>')) !!}
		</ul>
		@endif
        @if (Session::has('message'))
		<p style="background-color:green;color:#fff;">{!! Session::get('message') !!}</p>
		@endif

<?php
foreach($product_list as $products)
{
	$title 		 = $products->pro_title;
	$category_get	 = $products->pro_mc_id;
	$maincategory 	 = $products->pro_smc_id;
	$subcategory 	 = $products->pro_sb_id;
	$secondsubcategory= $products->pro_ssb_id;
	$originalprice  = $products->pro_price;
	$discountprice  = $products->pro_disprice;
	$inctax=$products->pro_inctax;
	$shippingamt=$products->pro_shippamt;
	$description 	 = $products->pro_desc;
	$deliverydays=$products->pro_delivery;
	$merchant		 = $products->pro_mr_id;
	$shop			 = $products->pro_sh_id;
	$metakeyword	 = $products->pro_mkeywords;
	$metadescription= $products->pro_mdesc;
	$file_get		 = $products->pro_Img;
	$img_count		 = $products->pro_image_count;
	$productspec=$products->pro_isspec;
	$pqty=$products->pro_qty;
	$SKU = $products->pro_sku;

	$weight   = $products->pro_weight;
	$length   = $products->pro_length;
	$widht    = $products->pro_width;
	$height   = $products->pro_height;

	$color_id = $products->co_id;
	$color_name = $products->co_name;
	$grade_id    = $products->grade_id;
	$grade_name    = $products->grade_name;

	$point = $products->pro_poin;

	$pro_metatitle = $products->pro_mtitle;
	$pro_metakeywords = $products->pro_mkeywords;
	$pro_metadescription = $products->pro_mdesc;

	$ws1_min = $products->wholesale_level1_min ? $products->wholesale_level1_min : null;
	$ws1_max = $products->wholesale_level1_max ? $products->wholesale_level1_max : null;
	$ws1_price = $products->wholesale_level1_price!=0 ? $products->wholesale_level1_price : null;
	$ws2_min = $products->wholesale_level2_min ? $products->wholesale_level2_min : null;
	$ws2_max = $products->wholesale_level2_max ? $products->wholesale_level2_max : null;
	$ws2_price = $products->wholesale_level2_price!=0 ? $products->wholesale_level2_price : null;
	$ws3_min = $products->wholesale_level3_min ? $products->wholesale_level3_min : null;
	$ws3_max = $products->wholesale_level3_max ? $products->wholesale_level3_max : null;
	$ws3_price = $products->wholesale_level3_price!=0 ? $products->wholesale_level3_price : null;
	$ws4_min = $products->wholesale_level4_min ? $products->wholesale_level4_min : null;
	$ws4_max = $products->wholesale_level4_max ? $products->wholesale_level4_max : null;
	$ws4_price = $products->wholesale_level4_price!=0 ? $products->wholesale_level4_price : null;
	$ws5_min = $products->wholesale_level5_min ? $products->wholesale_level5_min : null;
	$ws5_max = $products->wholesale_level5_max ? $products->wholesale_level5_max : null;
	$ws5_price = $products->wholesale_level5_price!=0 ? $products->wholesale_level5_price : null;

    $warranty = $products->have_warranty;
    $cicilan_3bulan = $products->jumlah_cicilan_3bulan;
    $cicilan_6bulan = $products->jumlah_cicilan_6bulan;
    $cicilan_12bulan = $products->jumlah_cicilan_12bulan;
}
?>

        <div id="div-1" class="accordion-body collapse in body">
               {!! Form::open(array('url'=>'edit_product_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}


		<input type="hidden" name="product_edit_id" value="<?php echo $products->pro_id; ?>" />
        <input type="hidden" name="url" value="{{redirect()->back()->getTargetUrl()}}">
		<div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
        <?php $yes = 0; ?>
        @foreach($privileges as $priv)
        <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
        @if(($priv_name[3] == 'content' && $priv_name[4] == 'edit_product') || ($priv_name[3] == 'all' && $priv_name[4] == 'edit_product'))
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Title<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Title" placeholder="" name="Product_Title" class="form-control" type="text" value="<?php echo $title; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product SKU<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly disabled id="Product_SKU" placeholder="" name="Product_SKU" class="form-control" type="text" value="<?php echo $SKU; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Weight<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Weight" placeholder="kg" name="Product_Weight" class="form-control" type="text" value="<?php echo $weight?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Length<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Length" placeholder="cm" name="Product_Length" class="form-control" type="text" value="<?php echo $length?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Widht<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Width" placeholder="cm" name="Product_Width" class="form-control" type="text" value="<?php echo $widht?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Height<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input id="Product_Height" placeholder="cm" name="Product_Height" class="form-control" type="text" value="<?php echo $height?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-2">Category<span class="text-sub">*</span></label>

                    <div class="col-lg-8">

			 <select class="form-control" name="category" id="category" onChange="select_main_cat(this.value)" >
                        <option value="0">--- Select ---</option>
                       <?php foreach($category as $cat_list)  { ?>
              			<option value="<?php echo $cat_list->mc_id; ?>" <?php if($cat_list->mc_id ==  $category_get) { ?> selected <?php } ?> ><?php echo $cat_list->mc_name; ?></option>
                        <?php } ?>
			 </select>

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Select Main Category<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <select class="form-control" name="maincategory" id="maincategory" onChange="select_sub_cat(this.value)" >
           			  <option value="0">---Select---</option>
      				  </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Select Sub Category<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                         <select class="form-control" name="subcategory" id="subcategory" onChange="select_second_sub_cat(this.value)" >
           			    <option value="0">---Select---</option>
				        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-2">Select Second Sub Category<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                    <select class="form-control" name="secondsubcategory" id="secondsubcategory"  >
		               <option value="0">---Select---</option>
        			   </select>
                    </div>
                </div>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Quantity<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input placeholder="Enter Quantity of Product" class="form-control" type="number" id="Quantity_Product" name="Quantity_Product" value="<?php echo $pqty; ?>" readonly>
                    </div>
                </div>
                <?php $yes = 1; break; ?>
                @endif
                @endforeach

                @if($yes == 0)
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Title<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly id="Product_Title" placeholder="" name="Product_Title" class="form-control" type="text" value="<?php echo $title; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product SKU<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly disabled id="Product_SKU" placeholder="" name="Product_SKU" class="form-control" type="text" value="<?php echo $SKU; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Weight<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly id="Product_Weight" placeholder="kg" name="Product_Weight" class="form-control" type="text" value="<?php echo $weight?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Length<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly id="Product_Length" placeholder="cm" name="Product_Length" class="form-control" type="text" value="<?php echo $length?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Widht<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly id="Product_Width" placeholder="cm" name="Product_Width" class="form-control" type="text" value="<?php echo $widht?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Height<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly id="Product_Height" placeholder="cm" name="Product_Height" class="form-control" type="text" value="<?php echo $height?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-2">Category<span class="text-sub">*</span></label>

                    <div class="col-lg-8">

			 <select readonly class="form-control" name="category" id="category" onChange="select_main_cat(this.value)" >
                        <option value="0">--- Select ---</option>
                       <?php foreach($category as $cat_list)  { ?>
              			<option value="<?php echo $cat_list->mc_id; ?>" <?php if($cat_list->mc_id ==  $category_get) { ?> selected <?php } ?> ><?php echo $cat_list->mc_name; ?></option>
                        <?php } ?>
			 </select>

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Select Main Category<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <select readonly class="form-control" name="maincategory" id="maincategory" onChange="select_sub_cat(this.value)" >
           			  <option value="0">---Select---</option>
      				  </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Select Sub Category<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                         <select readonly class="form-control" name="subcategory" id="subcategory" onChange="select_second_sub_cat(this.value)" >
           			    <option value="0">---Select---</option>
				        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-2">Select Second Sub Category<span class="text-sub"></span></label>

                    <div class="col-lg-8">
                    <select readonly class="form-control" name="secondsubcategory" id="secondsubcategory"  >
		               <option value="0">---Select---</option>
        			   </select>
                    </div>
                </div>
				<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Quantity<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input placeholder="Enter Quantity of Product" class="form-control" type="text" id="Quantity_Product" name="Quantity_Product" value="<?php echo $pqty; ?>" readonly>
                    </div>
                </div>
                @endif

                <!-- Start Pricing Admin Privileges -->
                <?php $yes = 0; ?>
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']) ?>
                @if(($priv_name[3] == 'price' && $priv_name[4] == 'edit_product') || ($priv_name[3] == 'all' && $priv_name[4] == 'edit_product'))
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Original Price<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input   placeholder="Numbers Only" class="form-control" type="number" id="Original_Price" value="<?php echo $originalprice; ?>" name="Original_Price">
                    </div>
                </div>
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Discounted Price<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input placeholder="Numbers Only" class="form-control" type="number" id="Discounted_Price" name="Discounted_Price" value="<?php echo $discountprice; ?>">
                    </div>
                </div>
                <div class="form-group" id="div_warranty" style="display: block;">
                    <label for="text2" class="control-label col-lg-2">Cicilan SNP 3 Bulan</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="number" name="cicilan_3bulan" id="cicilan_3bulan" value="{{$cicilan_3bulan}}">
                   </div>
                </div>
                <div class="form-group" id="div_warranty" style="display: block;">
                    <label for="text2" class="control-label col-lg-2">Cicilan SNP 6 Bulan</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="number" name="cicilan_6bulan" id="cicilan_6bulan" value="{{$cicilan_6bulan}}">
                   </div>
                </div>
                <div class="form-group" id="div_warranty" style="display: block;">
                    <label for="text2" class="control-label col-lg-2">Cicilan SNP 12 Bulan</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="number" name="cicilan_12bulan" id="cicilan_12bulan" value="{{$cicilan_12bulan}}">
                   </div>
                </div>

				<!-- Mulai Block Harga Grosir -->
				<div class="form-group">
					<div id="grosirbox" class="mt-10">
						<div class="col-lg-2"></div>
						<div class="col-lg-10">
							<strong><span class="green">+ Add wholesale price</span></strong>
						</div>
					</div>

					<div class="addProductbox__form custom-view-grosir row-fluid" style="display: block;">
						<div class="col-lg-2"></div>
						<div class="col-lg-7">
							<div class="control-group">
								<div class="controls">
									<input type="hidden" value="0" id="wholesale-exist">
									<table class="wholesale-price-table">
										<thead>
											<tr>
												<th></th>
												<th>Total Produk (buah)</th>
												<th>Harga Barang / buah</th>
											</tr>
										</thead>
										<tbody class="grosir-list">
											<tr>
												<td>1</td>
												<td>
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws1_min; ?>" id="qty-min-1" name="qty_min_1">
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws1_max; ?>" id="qty-max-1" name="qty_max_1"></td>
												<td>
													<input type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws1_price; ?>" id="prd-prc-1" name="prd_prc_1">
													<span id="error-grosir-1" class="error_grosir ml-5"><i class="mr-5 green"></i><small class="msg"></td>
											</tr>
											<tr>
												<td>2</td>
												<td>
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws2_min; ?>" id="qty-min-2" name="qty_min_2" disabled="disabled">
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws2_max; ?>" id="qty-max-2" name="qty_max_2" disabled="disabled"></td>
												<td>
													<input type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws2_price; ?>" id="prd-prc-2" name="prd_prc_2" disabled="disabled">
													<span id="error-grosir-2" class="error_grosir ml-5"><i class="mr-5 green"></i><small class="msg"></small></span></td>
											</tr>
											<tr>
												<td>3</td>
												<td>
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws3_min; ?>" id="qty-min-3" name="qty_min_3" disabled="disabled">
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws3_max; ?>" id="qty-max-3" name="qty_max_3" disabled="disabled"></td>
												<td>
													<input type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws3_price; ?>" id="prd-prc-3" name="prd_prc_3" disabled="disabled">
													<span id="error-grosir-3" class="error_grosir ml-5"><i class="mr-5 green"></i><small class="msg"></small></span></td>
											</tr>
											<tr>
												<td>4</td>
												<td>
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws4_min; ?>" id="qty-min-4" name="qty_min_4" disabled="disabled">
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws4_max; ?>" id="qty-max-4" name="qty_max_4" disabled="disabled">
												</td>
												<td>
													<input type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws4_price; ?>" id="prd-prc-4" name="prd_prc_4" disabled="disabled">
													<span id="error-grosir-4" class="error_grosir ml-5"><i class="mr-5"></i><small class="msg"></small></span>
												</td>
											</tr>
											<tr>
												<td>5</td>
												<td>
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws5_min; ?>" id="qty-min-5" name="qty_min_5" disabled="disabled">
													<input type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws5_max; ?>" id="qty-max-5" name="qty_max_5" disabled="disabled"></td>
												<td>
													<input type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws5_price; ?>" id="prd-prc-5" name="prd_prc_5" disabled="disabled">
													<span id="error-grosir-5" class="error_grosir ml-5"><i class="mr-5"></i><small class="msg"></small></span></td>
											</tr>
										</tbody>
									</table>
									<div class="alert-warning alert-box fs-11 mt-10" id="wholesale-price-alert" hidden="hidden">
										<div class="alert-box__left"><img src="https://ecs7.tokopedia.net/img/icon-bulb.png" class="alert-icon__center" alt="" width="20"></div>
										<div class="alert-box__content pl-10">
											<p class="m-0 gray fs-10">Selisih harga grosir lebih dari 30% harga normal. Mohon cek kembali harga grosir anda.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-2">
							<p class="form-info">Jumlah dan Harga tanpa tanda koma dan titik<br>Contoh:<br>Jumlah: 2 - 10, Harga: 15000<br>Jumlah: 11 - 20, Harga: 10000</p>
						</div>
					</div>
                </div>
				<!-- Akhir Block Harga Grosir -->
                <?php $yes = 1; break; ?>
                @endif
                @endforeach
                <!-- End Pricing Admin Privileges -->

                @if($yes == 0)
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Original Price<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly placeholder="Numbers Only" class="form-control" type="number" id="Original_Price" value="<?php echo $originalprice; ?>" name="Original_Price">
                    </div>
                </div>
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Discounted Price<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input readonly placeholder="Numbers Only" class="form-control" type="number" id="Discounted_Price" name="Discounted_Price" value="<?php echo $discountprice; ?>">
                    </div>
                </div>
                <div class="form-group" id="div_warranty" style="display: block;">
                    <label for="text2" class="control-label col-lg-2">Cicilan SNP 3 Bulan</label>
                    <div class="col-lg-8">
                        <input readonly class="form-control" type="number" name="cicilan_3bulan" id="cicilan_3bulan" value="{{$cicilan_3bulan}}">
                   </div>
                </div>
                <div class="form-group" id="div_warranty" style="display: block;">
                    <label for="text2" class="control-label col-lg-2">Cicilan SNP 6 Bulan</label>
                    <div class="col-lg-8">
                        <input readonly class="form-control" type="number" name="cicilan_6bulan" id="cicilan_6bulan" value="{{$cicilan_6bulan}}">
                   </div>
                </div>
                <div class="form-group" id="div_warranty" style="display: block;">
                    <label for="text2" class="control-label col-lg-2">Cicilan SNP 12 Bulan</label>
                    <div class="col-lg-8">
                        <input readonly class="form-control" type="number" name="cicilan_12bulan" id="cicilan_12bulan" value="{{$cicilan_12bulan}}">
                   </div>
                </div>

				<!-- Mulai Block Harga Grosir -->
				<div class="form-group">
					<div id="grosirbox" class="mt-10">
						<div class="col-lg-2"></div>
						<div class="col-lg-10">
							<strong><span class="green">+ Add wholesale price</span></strong>
						</div>
					</div>

					<div class="addProductbox__form custom-view-grosir row-fluid" style="display: block;">
						<div class="col-lg-2"></div>
						<div class="col-lg-7">
							<div class="control-group">
								<div class="controls">
									<input type="hidden" value="0" id="wholesale-exist">
									<table class="wholesale-price-table">
										<thead>
											<tr>
												<th></th>
												<th>Total Produk (buah)</th>
												<th>Harga Barang / buah</th>
											</tr>
										</thead>
										<tbody class="grosir-list">
											<tr>
												<td>1</td>
												<td>
													<input readonly type="text" class="med-grosir mr-10 qty-1" value="<?php echo $ws1_min; ?>" id="qty-min-1" name="qty_min_1">
													<input readonly type="text" class="med-grosir mr-10 qty-2" value="<?php echo $ws1_max; ?>" id="qty-max-1" name="qty_max_1"></td>
												<td>
													<input readonly type="text" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws1_price; ?>" id="prd-prc-1" name="prd_prc_1">
													<span id="error-grosir-1" class="error_grosir ml-5"><i class="mr-5 green"></i><small class="msg"></td>
											</tr>
											<tr>
												<td>2</td>
												<td>
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws2_min; ?>" id="qty-min-2" name="qty_min_2" disabled="disabled">
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws2_max; ?>" id="qty-max-2" name="qty_max_2" disabled="disabled"></td>
												<td>
													<input disable readonly type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws2_price; ?>" id="prd-prc-2" name="prd_prc_2" disabled="disabled">
													<span id="error-grosir-2" class="error_grosir ml-5"><i class="mr-5 green"></i><small class="msg"></small></span></td>
											</tr>
											<tr>
												<td>3</td>
												<td>
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws3_min; ?>" id="qty-min-3" name="qty_min_3" disabled="disabled">
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws3_max; ?>" id="qty-max-3" name="qty_max_3" disabled="disabled"></td>
												<td>
													<input disable readonly type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws3_price; ?>" id="prd-prc-3" name="prd_prc_3" disabled="disabled">
													<span id="error-grosir-3" class="error_grosir ml-5"><i class="mr-5 green"></i><small class="msg"></small></span></td>
											</tr>
											<tr>
												<td>4</td>
												<td>
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws4_min; ?>" id="qty-min-4" name="qty_min_4" disabled="disabled">
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws4_max; ?>" id="qty-max-4" name="qty_max_4" disabled="disabled">
												</td>
												<td>
													<input disable readonly type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws4_price; ?>" id="prd-prc-4" name="prd_prc_4" disabled="disabled">
													<span id="error-grosir-4" class="error_grosir ml-5"><i class="mr-5"></i><small class="msg"></small></span>
												</td>
											</tr>
											<tr>
												<td>5</td>
												<td>
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-1" value="<?php echo $ws5_min; ?>" id="qty-min-5" name="qty_min_5" disabled="disabled">
													<input disable readonly type="text" data-input="enable" class="med-grosir mr-10 qty-2" value="<?php echo $ws5_max; ?>" id="qty-max-5" name="qty_max_5" disabled="disabled"></td>
												<td>
													<input disable readonly type="text" data-input="enable" class="lg-grosir price-grosir-new price-input-idr" value="<?php echo $ws5_price; ?>" id="prd-prc-5" name="prd_prc_5" disabled="disabled">
													<span id="error-grosir-5" class="error_grosir ml-5"><i class="mr-5"></i><small class="msg"></small></span></td>
											</tr>
										</tbody>
									</table>
									<div class="alert-warning alert-box fs-11 mt-10" id="wholesale-price-alert" hidden="hidden">
										<div class="alert-box__left"><img src="https://ecs7.tokopedia.net/img/icon-bulb.png" class="alert-icon__center" alt="" width="20"></div>
										<div class="alert-box__content pl-10">
											<p class="m-0 gray fs-10">Selisih harga grosir lebih dari 30% harga normal. Mohon cek kembali harga grosir anda.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-2">
							<p class="form-info">Jumlah dan Harga tanpa tanda koma dan titik<br>Contoh:<br>Jumlah: 2 - 10, Harga: 15000<br>Jumlah: 11 - 20, Harga: 10000</p>
						</div>
					</div>
                </div>
				<!-- Akhir Block Harga Grosir -->
                @endif

				<!-- <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">

                                <input type="checkbox" <?php if($inctax > 0) { ?> checked="checked" <?php } ?> onclick="if(this.checked){$('#inctax').show();}else{$('#inctax').hide();$('#inctax').val(0)}"> ( Including tax amount )
                                <input placeholder="Numbers Only" <?php if($inctax == 0) { ?> style="display:none;" <?php } ?>  class="form-control" type="text" id="inctax" name="inctax" value="<?php echo $inctax;?>">
                    </div>
                </div> -->
		<!-- <div class="form-group"  >
                    <label for="text2"  class="control-label col-lg-2">Shipping Amount<span class="text-sub">*</span></label>

                   <div class="col-lg-8">
<input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'none');" value="1" <?php if($shippingamt<1){?>checked<?php } ?> > <label class="sample">Free</label>
<input type="radio" id="shipamt" name="shipamt" onClick="setshipVisibility('showship', 'inline');" value="2" <?php if($shippingamt>0){?>checked<?php } ?>  ><label class="sample">Amount</label>


						<label class="sample"></label>
                    </div>
                </div> -->
<?php
if($shippingamt<1)
{
$showship="display:block";
}
else
{

$showship="display:none";
}

?>

				  <!-- <div class="form-group" id="showship" style="<?php echo $showship;?>" >
                    <label for="text1" class="control-label col-lg-2">Shipping Amount<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input  placeholder="" class="form-control" type="text" id="Shipping_Amount" name="Shipping_Amount" value="<?php echo $shippingamt; ?>">
                    </div>
                </div> -->

                <?php $yes = 0; ?>
                @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']) ?>
                @if(($priv_name[3] == 'content' && $priv_name[4] == 'edit_product') || ($priv_name[3] == 'all' && $priv_name[4] == 'edit_product'))
				 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Description<span class="text-sub">*</span></label>

                    <div class="col-lg-9">
						<textarea id="wysihtml5" class="form-control" rows="10" id="Description" name="Description"><?php echo $description; ?></textarea>
                    </div>
                </div>




<?php
$resultspec="";
$resultspectext="";
// dd($existingspecification);
  ?>
	@foreach ($existingspecification as $existingspecification1)
	<?php
	$resultspec=$existingspecification1->spc_sp_id.",".$resultspec;
    if($resultspectext==''){
        $resultspectext=$existingspecification1->spc_value;
    }else{
	    $resultspectext=$resultspectext.','.$existingspecification1->spc_value;
    }
	?>
	@endforeach

<?php
if(strlen($resultspec)>0)
{
$trimmedspec=trim($resultspec,",");
$specarray=explode(",",$trimmedspec);
$speccount=count($specarray)-1;
$trimmedtext=trim($resultspectext,",");
$textarray=explode(",",$trimmedtext);
$specidvalue=$speccount+1;
}
else
{
 $speccount=0;
}
?>


         <div class="form-group" id="sub4" >
                    <label for="text2" class="control-label col-lg-2">Specification</label>

                    <div class="col-lg-8" style="display:none;">
                    <label>Text ( More custom specification <a href="<?php echo url('')?>/manage_specification">ADD</a> )</label>
			</div>

			<div class="col-lg-8 control-label">
                <select class="form-control" id="spec_contoh" style="display:none"></br></br>
        </br>
                          <option  value="0">--select specification--</option>
              		@foreach ($productspecification as $specification)
             		 <option  value="<?php echo $specification->sp_id;?>" >{!!$specification->sp_name!!}</option>
            		 @endforeach
          		</select>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Spec Name</th>
                            <th>Spec Value</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_spec">
                        <?php $i=0;$j=0; foreach($productspecification as $product_spec){?>

                        <tr>

                            <td><input type="hidden" name="spec{{$i}}" value="{{$product_spec->sp_id}}"> {{$product_spec->sp_name}}</td>

                           <?php if ($product_spec->sp_type == "Text") { ?>
 <td><input class="form-control" type="text" name="spectext{{$i}}" <?php if(!empty($textarray[$i])){ ?> value="{{$textarray[$i]}}" <?php } ?>></td>

<?php
}

elseif($product_spec->sp_type == "Selection"){        ?>

            <td>
            <select class="form-control"  name="spectext{{$i}}">

         @foreach ($values[$i] as $value)
            @if(!empty($textarray[$i]))
                @if($value->short_desc == $textarray[$i])
                <option value="<?php echo $value->short_desc;?>" selected><?php echo $value->short_desc ?></option>
                @else
                <option value="<?php echo $value->short_desc;?>"><?php echo $value->short_desc ?></option>
                @endif
            @else
                <option value="<?php echo $value->short_desc;?>"><?php echo $value->short_desc ?></option>
            @endif
         @endforeach
            </select>
            </td>

            <?php $j++; } ?>


                    </tr>
                        <?php $i++; } ?>


                </tbody>
            </table>
                    </div>


                    <input type="hidden" id="specificationid1" value="<?php if(strlen($resultspec)>0) { echo $specidvalue; } ?>">

		    <input type="hidden" id="specificationcount" name="specificationcount" value="<?php echo count($productspecification);?>">
                </div>


        <div class="col-lg-12 col-lg-offset-2">
     		<div  id="divspecificationTxt">
                </br>
    		</div>
        </div>

		 <div class="form-group"  >

		 <div class="col-lg-3">

                    </div>

 		<p id="addmore" style="display:none;"><a onClick="addspecificationFormField();"  style="cursor:pointer;color:#F60; ">Add more</a> </p>
		</div>


<?php
$resultsize="";
$sizename="";
 ?>
	@foreach ($existingsize as $existingsize1)
	<?php
		$resultsize=$existingsize1->ps_si_id.",".$resultsize;
		$sizename=$existingsize1->si_name.",".$sizename;
	?>
	@endforeach

<?php
if(strlen($resultsize)>0)
{

$trimmedsizes=trim($resultsize,",");
$sizearray=explode(",",$trimmedsizes);
$sizecount=count($sizearray);
$trimsizename=explode(",",$sizename);
}
else
{
$resultsize="0,";
$sizecount=0;
}

?>




<div class="form-group" id="sizediv" <?php if($sizecount>0){ ?> style="display:block;" <?php } else {?>style="display:none;" <?php } ?>>

 <input type="hidden" id="productsizecount" name="productsizecount" value="<?php echo $sizecount;?> ">
		 <label class="control-label col-lg-2" for="text1">Your Select size<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showsize" >

		<?php for($i=0;$i<$sizecount;$i++) { ?>

 		<span style="padding-right:10px;">Select Size:</span><?php echo $trimsizename[$i];?><span style="color:#ff0099;padding-right:90px"><input type="checkbox"  name="<?php echo 'sizecheckbox'.$sizearray[$i];?>"style="padding-left:30px;" checked="checked" value="1" ></span>
		 <?php } ?>
		</p>


                       <input type="hidden"  name="si"  value="<?php echo $resultsize;?>" id="si" />
                    </div>
</div>
<div class="form-group" id="quantitydiv" <?php if($sizecount>0){ ?> style="display:none;" <?php }else { ?> style="display:none;" <?php } ?>>
  		 <label class="control-label col-lg-2" for="text1">Quantity<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showquantity" >

		<?php for($i=0;$i<$sizecount;$i++) { ?>
	 <input type="text" name="<?php echo 'quantity'.$sizearray[$i];?>" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input>  <?php } ?>
</p>
                       <input type="hidden"   value="0," id="sq" />
                    </div>
</div>

<?php
$resultcolor="";
$colorname="";
$colorcode="";
 ?>
	@foreach ($existingcolor as $existingcolor1)
	<?php
		$resultcolor=$existingcolor1->pc_co_id.",".$resultcolor;
		$colorname=$existingcolor1->co_name.",".$colorname;
		$colorcode=$existingcolor1->co_code.",".$colorcode;
	?>
	@endforeach

<?php


if(strlen($resultcolor)>0)
{

$trimmedcolor=trim($resultcolor,",");
$colorarray=explode(",",$trimmedcolor);
$colorcount=count($colorarray);
$colornamearray=explode(",",$colorname);
$colorcodearray=explode(",",$colorcode);
}
else
{
$colorcount=0;
$resultcolor="0,";
}

?>

<div class="form-group" id="sub-grade" style="display: block;">
    <label for="text2" class="control-label col-lg-2">Product Grade</label>
    <div class="col-lg-3">
        <select class="form-control" id="selectprograde" name="selectprograde" onchange="getgradename(this.value)" readonly>
            <option value="0">-Select product grade-</option>
            @foreach ($productgrade as $grade)
            <option <?php if($product_list[0]->pro_grade_id==$grade->grade_id){ ?> selected <?php } ?> value="<?php echo $grade->grade_id;?>">{!!$grade->grade_name!!}</option>
            @endforeach
        </select>
    </div>
</div>
@foreach ($productgrade as $grade)
<?php if($product_list[0]->pro_grade_id==$grade->grade_id){ ?>
<input type="hidden" name="selectprograde" value="{{$grade->grade_id}}">
<?php } ?>
@endforeach
<div class="form-group" id="div_warranty" style="display: block;">
    <label for="text2" class="control-label col-lg-2">Extended Warranty</label>
    <div class="col-lg-8">
        <input type="checkbox" name="extended_warranty" id="extended_warranty" value="{{$warranty}}" <?php if($warranty == 1){echo "checked";}?>> <label class="sample"><?php echo $get_setting_extended[0]->besaran_warranty?> %</label>
   </div>
</div>
<!-- <div class="form-group" style="display:none;">
	 <label for="text2" class="control-label col-lg-2">Add Color Field<span class="text-sub">*</span></label>
	 <div class="col-lg-8">  <input type="radio" name="productcolor" onClick="setVisibility1('sub3', 'none');" value="1" checked> <label class="sample">yes                  </label>
					<input type="radio" name="productcolor" onClick="setVisibility1('sub3', 'inline');" value="2" ><label class="sample">No</label>
						<label class="sample"></label>
                    </div>
                </div> -->

  <div class="form-group" id="sub3">
      <label for="text2" class="control-label col-lg-2">Product Colour</label>

      <div class="col-lg-3">
			  <select class="form-control" id="select_color" name="select_color" onchange="//getcolorname(this.value)">
        <option value="<?php echo $color_id?>"><?php echo $color_name?></option>

			    @foreach ($productcolor as $color)
           	<option style="background:<?php echo $color->co_code;?>;" value="<?php echo $color->co_id;?>">{!!$color->co_name!!}</option>
          @endforeach

      	</select>


      </div>
  </div>

  <div class="form-group" style="display:none;">
      <label for="text1" class="control-label col-lg-2">Product Point<span class="text-sub">*</span></label>

      <div class="col-lg-8">
          <input id="product_point" placeholder="" name="product_point" class="form-control" type="text" value="<?php echo $point; ?>">
      </div>
  </div>

  <div class="form-group">
     <label for="text1" class="control-label col-lg-2">Product Meta Title<span class="text-sub">*</span></label>

     <div class="col-lg-8">
         <input placeholder="Product Meta Title" class="form-control" type="text" id="Meta_Title" name="Meta_Title" value="{{$pro_metatitle}}">
     </div>
 </div>
 <div class="form-group">
    <label for="text1" class="control-label col-lg-2">Product Meta Keywords<span class="text-sub">*</span></label>

    <div class="col-lg-8">
        <input placeholder="Product Meta Keywords" class="form-control" type="text" id="Meta_Keywords" name="Meta_Keywords" value="{{$pro_metakeywords}}">
    </div>
</div>
<div class="form-group">
   <label for="text1" class="control-label col-lg-2">Product Meta Description<span class="text-sub">*</span></label>

   <div class="col-lg-8">
       <input placeholder="Product Meta Description" class="form-control" type="text" id="Meta_Description" name="Meta_Description" value="{{$pro_metadescription}}">
   </div>
</div>


		<!-- <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Delivery days<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <input type="text" class="form-control" placeholder="" id="Delivery_Days" name="Delivery_Days" value="<?php echo $deliverydays; ?>">
                    </div>
                </div> -->
				 <!-- <div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                      Eg : ( 2 - 5 )
                    </div>
                </div> -->
				   <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Select Merchant<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <select class="form-control" name="Select_Merchant" id="Select_Merchant" onchange="getshop_ajax(this.value);">
			@foreach ($merchantdetails as $merchantval)
	 <option  value="<?php echo $merchantval->mer_id;?>" <?php if($merchantval->mer_id==$merchant) { ?>  selected <?php } ?> >{!!$merchantval->mer_fname!!}</option>
          		 @endforeach
        </select>
                    </div>
                </div>
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Select Shop<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                         <select class="form-control" name="Select_Shop" id="Select_Shop" >
			 </select>
                    </div>
                </div>


				  <!-- <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Meta keywords<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Keywords" name="Meta_Keywords"><?php echo $metakeyword;?></textarea>
                    </div>
                </div> -->

				 <!-- <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Meta description<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Description" name="Meta_Description"><?php echo $metadescription ; ?></textarea>
                    </div>
                </div> -->

<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Image<span class="text-sub">*</span><br><span  style="color:#999">(max 5)</span></label>
					<?php $file_get_path =  explode("/**/",$file_get); ?>
                    <div class="col-lg-8" id="img_upload">
                    	<div style="float:left; max-width:219px"><input type="hidden" name="file_new" value="<?php echo  $file_get_path[0]; ?>"  >
                       <input type="file"  id="file" name="file"><span><img src="<?php echo url(''); ?>/assets/product/<?php echo $file_get_path[0]; ?>" height="50" width="50" > </span>                        </div>
                     <?php
					 for($j=0 ; $j< $img_count; $j++)
					 { ?>
                     <div style="float:left; max-width:219px" id="row_exist{{$j}}">
                         <input type="hidden" id="file_more_new<?php echo $j; ?>" name="file_more_new<?php echo $j; ?>" value="<?php echo  $file_get_path[$j+1]; ?>" >
                         <input type='file' id="file_more<?php echo $j; ?>" name='file_more<?php echo $j; ?>' style="padding-bottom:5px;" /> <img src="<?php echo url(''); ?>/assets/product/<?php echo $file_get_path[$j+1]; ?>" height="50" width="50" >
                         <a href='#' onClick='removeFormField("#row_exist{{$j}}", {{$j}}); return false;' style='color:#F60;' >Remove</a>
                     </div>

					 <?php } ?>
                       <div id="divTxt"></div>
                      <p style="clear:both"><a onClick="addimgFormField(); return false;" style="cursor:pointer;color:#F60;" >Add</a></p>
                       <input type="hidden" id="count" name="count" value="<?php echo $img_count; ?>">
                       <input type="hidden" id="aid" value="1">
                    </div>


                 </div>
                 <?php $yes = 1; break; ?>
                 @endif
                 @endforeach

                 @if($yes == 0)
                 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Description<span class="text-sub">*</span></label>

                    <div class="col-lg-9">
						<textarea readonly id="" class="form-control" rows="10" id="Description" name="Description"><?php echo $description; ?></textarea>
                    </div>
                </div>




<?php
$resultspec="";
$resultspectext="";
// dd($existingspecification);
  ?>
	@foreach ($existingspecification as $existingspecification1)
	<?php
	$resultspec=$existingspecification1->spc_sp_id.",".$resultspec;
    if($resultspectext==''){
        $resultspectext=$existingspecification1->spc_value;
    }else{
	    $resultspectext=$resultspectext.','.$existingspecification1->spc_value;
    }
	?>
	@endforeach

<?php
if(strlen($resultspec)>0)
{
$trimmedspec=trim($resultspec,",");
$specarray=explode(",",$trimmedspec);
$speccount=count($specarray)-1;
$trimmedtext=trim($resultspectext,",");
$textarray=explode(",",$trimmedtext);
$specidvalue=$speccount+1;
}
else
{
 $speccount=0;
}
?>


         <div class="form-group" id="sub4" >
                    <label for="text2" class="control-label col-lg-2">Specification</label>

                    <div class="col-lg-8" style="display:none;">
                    <label>Text ( More custom specification <a href="<?php echo url('')?>/manage_specification">ADD</a> )</label>
			</div>

			<div class="col-lg-8 control-label">
                <select class="form-control" id="spec_contoh" style="display:none"></br></br>
        </br>
                          <option  value="0">--select specification--</option>
              		@foreach ($productspecification as $specification)
             		 <option  value="<?php echo $specification->sp_id;?>" >{!!$specification->sp_name!!}</option>
            		 @endforeach
          		</select>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Spec Name</th>
                            <th>Spec Value</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_spec">
                        <?php $i=0; foreach($productspecification as $product_spec){?>
                        <tr>
                            <td><input type="hidden" name="spec{{$i}}" value="{{$product_spec->sp_id}}"> {{$product_spec->sp_name}}</td>
                            <td><input readonly class="form-control" type="text" name="spectext{{$i}}" <?php if(!empty($textarray[$i])){ ?> value="{{$textarray[$i]}}" <?php } ?>></td>
                        </tr>
                        <?php $i++; } ?>
                    </tbody>
                </table>
                    </div>


                    <input type="hidden" id="specificationid1" value="<?php if(strlen($resultspec)>0) { echo $specidvalue; } ?>">
		    <input type="hidden" id="specificationcount" name="specificationcount" value="<?php echo count($productspecification);?>">
                </div>
        <div class="col-lg-12 col-lg-offset-2">
     		<div  id="divspecificationTxt">
                </br>
    		</div>
        </div>

		 <div class="form-group"  >

		 <div class="col-lg-3">

                    </div>

 		<p id="addmore" style="display:none;"><a onClick="addspecificationFormField();"  style="cursor:pointer;color:#F60; ">Add more</a> </p>
		</div>


<?php
$resultsize="";
$sizename="";
 ?>
	@foreach ($existingsize as $existingsize1)
	<?php
		$resultsize=$existingsize1->ps_si_id.",".$resultsize;
		$sizename=$existingsize1->si_name.",".$sizename;
	?>
	@endforeach

<?php
if(strlen($resultsize)>0)
{

$trimmedsizes=trim($resultsize,",");
$sizearray=explode(",",$trimmedsizes);
$sizecount=count($sizearray);
$trimsizename=explode(",",$sizename);
}
else
{
$resultsize="0,";
$sizecount=0;
}

?>




<div class="form-group" id="sizediv" <?php if($sizecount>0){ ?> style="display:block;" <?php } else {?>style="display:none;" <?php } ?>>

 <input type="hidden" id="productsizecount" name="productsizecount" value="<?php echo $sizecount;?> ">
		 <label class="control-label col-lg-2" for="text1">Your Select size<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showsize" >

		<?php for($i=0;$i<$sizecount;$i++) { ?>

 		<span style="padding-right:10px;">Select Size:</span><?php echo $trimsizename[$i];?><span style="color:#ff0099;padding-right:90px"><input type="checkbox"  name="<?php echo 'sizecheckbox'.$sizearray[$i];?>"style="padding-left:30px;" checked="checked" value="1" ></span>
		 <?php } ?>
		</p>


                       <input type="hidden"  name="si"  value="<?php echo $resultsize;?>" id="si" />
                    </div>
</div>
<div class="form-group" id="quantitydiv" <?php if($sizecount>0){ ?> style="display:none;" <?php }else { ?> style="display:none;" <?php } ?>>
  		 <label class="control-label col-lg-2" for="text1">Quantity<span class="text-sub">:</span></label>

                    <div class="col-lg-8">
                       <p id="showquantity" >

		<?php for($i=0;$i<$sizecount;$i++) { ?>
	 <input type="text" name="<?php echo 'quantity'.$sizearray[$i];?>" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input>  <?php } ?>
</p>
                       <input type="hidden"   value="0," id="sq" />
                    </div>
</div>

<?php
$resultcolor="";
$colorname="";
$colorcode="";
 ?>
	@foreach ($existingcolor as $existingcolor1)
	<?php
		$resultcolor=$existingcolor1->pc_co_id.",".$resultcolor;
		$colorname=$existingcolor1->co_name.",".$colorname;
		$colorcode=$existingcolor1->co_code.",".$colorcode;
	?>
	@endforeach

<?php


if(strlen($resultcolor)>0)
{

$trimmedcolor=trim($resultcolor,",");
$colorarray=explode(",",$trimmedcolor);
$colorcount=count($colorarray);
$colornamearray=explode(",",$colorname);
$colorcodearray=explode(",",$colorcode);
}
else
{
$colorcount=0;
$resultcolor="0,";
}

?>

<div class="form-group" id="sub-grade" style="display: block;">
    <label for="text2" class="control-label col-lg-2">Product Grade</label>
    <div class="col-lg-3">
        <select readonly class="form-control" id="selectprograde" name="selectprograde" onchange="getgradename(this.value)">
            <option value="0">-Select product grade-</option>
            @foreach ($productgrade as $grade)
            <option <?php if($product_list[0]->pro_grade_id==$grade->grade_id){ ?> selected <?php } ?> value="<?php echo $grade->grade_id;?>">{!!$grade->grade_name!!}</option>
            @endforeach
        </select>
    </div>
</div>
@foreach ($productgrade as $grade)
<?php if($product_list[0]->pro_grade_id==$grade->grade_id){ ?>
<input type="hidden" name="selectprograde" value="{{$grade->grade_id}}">
<?php } ?>
@endforeach
<div class="form-group" id="div_warranty" style="display: block;">
    <label for="text2" class="control-label col-lg-2">Extended Warranty</label>
    <div class="col-lg-8">
        <input disabled type="checkbox" name="extended_warranty" id="extended_warranty" value="{{$warranty}}" <?php if($warranty == 1){echo "checked";}?>> <label class="sample"><?php echo $get_setting_extended[0]->besaran_warranty?> %</label>
        <input type="hidden" name="extended_warranty" id="extended_warranty" value="{{$warranty}}">
   </div>
</div>
<!-- <div class="form-group" style="display:none;">
	 <label for="text2" class="control-label col-lg-2">Add Color Field<span class="text-sub">*</span></label>
	 <div class="col-lg-8">  <input type="radio" name="productcolor" onClick="setVisibility1('sub3', 'none');" value="1" checked> <label class="sample">yes                  </label>
					<input type="radio" name="productcolor" onClick="setVisibility1('sub3', 'inline');" value="2" ><label class="sample">No</label>
						<label class="sample"></label>
                    </div>
                </div> -->

  <div class="form-group" id="sub3">
      <label for="text2" class="control-label col-lg-2">Product Colour</label>

      <div class="col-lg-3">
			  <select readonly class="form-control" id="select_color" name="select_color" onchange="//getcolorname(this.value)">
        <option value="<?php echo $color_id?>"><?php echo $color_name?></option>

			    @foreach ($productcolor as $color)
           	<option style="background:<?php echo $color->co_code;?>;" value="<?php echo $color->co_id;?>">{!!$color->co_name!!}</option>
          @endforeach

      	</select>


      </div>
  </div>

  <div class="form-group" style="display:none;">
      <label for="text1" class="control-label col-lg-2">Product Point<span class="text-sub">*</span></label>

      <div class="col-lg-8">
          <input readonly id="product_point" placeholder="" name="product_point" class="form-control" type="text" value="<?php echo $point; ?>">
      </div>
  </div>

  <div class="form-group">
     <label for="text1" class="control-label col-lg-2">Product Meta Title<span class="text-sub">*</span></label>

     <div class="col-lg-8">
         <input readonly placeholder="Product Meta Title" class="form-control" type="text" id="Meta_Title" name="Meta_Title" value="{{$pro_metatitle}}">
     </div>
 </div>
 <div class="form-group">
    <label for="text1" class="control-label col-lg-2">Product Meta Keywords<span class="text-sub">*</span></label>

    <div class="col-lg-8">
        <input readonly placeholder="Product Meta Keywords" class="form-control" type="text" id="Meta_Keywords" name="Meta_Keywords" value="{{$pro_metakeywords}}">
    </div>
</div>
<div class="form-group">
   <label for="text1" class="control-label col-lg-2">Product Meta Description<span class="text-sub">*</span></label>

   <div class="col-lg-8">
       <input readonly placeholder="Product Meta Description" class="form-control" type="text" id="Meta_Description" name="Meta_Description" value="{{$pro_metadescription}}">
   </div>
</div>


		<!-- <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Delivery days<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <input type="text" class="form-control" placeholder="" id="Delivery_Days" name="Delivery_Days" value="<?php echo $deliverydays; ?>">
                    </div>
                </div> -->
				 <!-- <div class="form-group">
                    <label for="text2" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                      Eg : ( 2 - 5 )
                    </div>
                </div> -->
				   <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Select Merchant<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <select readonly class="form-control" name="Select_Merchant" id="Select_Merchant" onchange="getshop_ajax(this.value);">
			@foreach ($merchantdetails as $merchantval)
	 <option  value="<?php echo $merchantval->mer_id;?>" <?php if($merchantval->mer_id==$merchant) { ?>  selected <?php } ?> >{!!$merchantval->mer_fname!!}</option>
          		 @endforeach
        </select>
                    </div>
                </div>
				  <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Select Shop<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                         <select readonly class="form-control" name="Select_Shop" id="Select_Shop" >
			 </select>
                    </div>
                </div>


				  <!-- <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Meta keywords<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Keywords" name="Meta_Keywords"><?php echo $metakeyword;?></textarea>
                    </div>
                </div> -->

				 <!-- <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">Meta description<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                       <textarea class="form-control" id="Meta_Description" name="Meta_Description"><?php echo $metadescription ; ?></textarea>
                    </div>
                </div> -->

<div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Product Image<span class="text-sub">*</span><br><span  style="color:#999">(max 5)</span></label>
					<?php $file_get_path =  explode("/**/",$file_get); ?>
                    <div class="col-lg-8" id="img_upload">
                    	<div style="float:left; max-width:219px"><input type="hidden" name="file_new" value="<?php echo  $file_get_path[0]; ?>"  >
                       <input disabled readonly type="file"  id="file" name="file"><span><img src="<?php echo url(''); ?>/assets/product/<?php echo $file_get_path[0]; ?>" height="50" width="50" > </span>                        </div>
                     <?php
					 for($j=0 ; $j< $img_count; $j++)
					 { ?>
                     <div style="float:left; max-width:219px" id="row_exist{{$j}}">
                         <input type="hidden" id="file_more_new<?php echo $j; ?>" name="file_more_new<?php echo $j; ?>" value="<?php echo  $file_get_path[$j+1]; ?>" >
                         <input disabled readonly type='file' id="file_more<?php echo $j; ?>" name='file_more<?php echo $j; ?>' style="padding-bottom:5px;" /> <img src="<?php echo url(''); ?>/assets/product/<?php echo $file_get_path[$j+1]; ?>" height="50" width="50" >
                     </div>

					 <?php } ?>
                       <div id="divTxt"></div>
                       <input type="hidden" id="count" name="count" value="<?php echo $img_count; ?>">
                       <input type="hidden" id="aid" value="1">
                    </div>


                 </div>
                 @endif

		<div class="form-group">
                    <label for="pass1" class="control-label col-lg-2"><span  class="text-sub"></span></label>

                    <div class="col-lg-8">
                   <button class="btn btn-warning btn-sm btn-grad" id="submit_product" ><a style="color:#fff">Update Product</a></button>
                     <a href="{{redirect()->back()->getTargetUrl()}}" class="btn btn-default btn-sm btn-grad" style="color:#000">Cancel</a>

                    </div>

                </div>


         </form>
        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     {!! $adminfooter !!}
    <!--END FOOTER -->


       <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
<script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>



 <script language="JavaScript">
function setVisibility(id, visibility) {


document.getElementById(id).style.display = visibility;
document.getElementById('addmore').style.display =visibility;

}
function setVisibility1(id, visibility) {


document.getElementById(id).style.display = visibility;
document.getElementById('colordiv').style.display =visibility;

}
function setshipVisibility(id, visibility)
{
document.getElementById(id).style.display = visibility;

}
</script>
<script type='text/javascript'>

$(document).ready(function(){

    var counter = 2;
    $('#del_file').hide();
    $('img#add_file').click(function(){
        $('#file_tools').before('<br><div class="col-lg-8" id="f'+counter+'"><input name="file[]" type="file"></div>');
        $('#del_file').fadeIn(0);
    counter++;
    });
    $('img#del_file').click(function(){
        if(counter==3){
            $('#del_file').hide();
        }
        counter--;
        $('#f'+counter).remove();
    });
});
</script>
<script type="text/javascript">
 function addspecificationFormField()
	{
		var count_id = document.getElementById("specificationcount").value;

		var selectspec=$("#spec"+count_id+" option:selected").val();
		var spectext=$("#spectext"+count_id).val();
		if(selectspec!=0  && spectext!="")
		{


	 var id = document.getElementById("specificationid1").value;
	  var count_id = document.getElementById("specificationcount").value;
	 var nameid=parseInt(count_id)+1;

     document.getElementById('specificationcount').value = parseInt(count_id)+1;
     jQuery("#divspecificationTxt").append(" <div class='form-group ' id='spec"+ nameid + "' '><div class='col-lg-3'><select name='spec"+ nameid  + "' id='spec_option"+ nameid  + "' class='form-control'><option  value='0'>-- select specification--</option></select></div><div class='col-lg-3'><input type='text' class='form-control' name='spectext"+ nameid  + "'  /></div><div class='col-lg-3' ><a href='#' onClick='removespecFormField(\"#spec" + nameid + "\"); return false;' style='color:#F60;' >Remove</a></div></div>");
     jQuery('#spec_option'+nameid).html($('#spec_contoh').html());
     id = (id - 1) + 1;
     document.getElementById("specificationid1").value = id;
		}
		else
		{
			alert("Please select specification and provide text then click Add more");
		}




	}
function removespecFormField(id) {
	var count_id = document.getElementById("specificationcount").value;
	 count_id=count_id-1;

document.getElementById("specificationcount").value=count_id;

        jQuery(id).remove();
    }


</script>

 <script type="text/javascript">


    function addimgFormField() {
      var id = document.getElementById("aid").value;
	  var count_id = document.getElementById("count").value;
	  if(count_id < 4){

		document.getElementById('count').value = parseInt(count_id)+1;
		jQuery.noConflict()
        jQuery("#divTxt").append("<input type='hidden' id='file_more_new"+count_id+"' name='file_more_new"+count_id+"' value='' />");
		jQuery("#divTxt").append("<tr style='height:5px;' > <td> </td> </tr><tr id='row" + id + "' style='width:100%'><td width='20%'><input type='file' id='file_more"+count_id+"' name='file_more"+count_id+"' /></td><td>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + id + "\", "+count_id+"); return false;' style='color:#F60;' >Remove</a></td></tr>");
			jQuery('#row' + id).highlightFade({    speed:1000 });
		id = (id - 1) + 2;
		document.getElementById("aid").value = id;
		}
    }

      	function removeFormField(id, id_number)
		{
            var count_id = document.getElementById("count").value;
            for (var i = id_number; i < count_id; i++) {
                j = i - 1;
                document.getElementById('file_more_new'+i).setAttribute('name', 'file_more_new'+j);
                document.getElementById('file_more_new'+i).id = 'file_more_new'+j;
                document.getElementById('file_more'+i).setAttribute('name', 'file_more'+j);
                document.getElementById('file_more'+i).id = 'file_more'+j;
            }
			document.getElementById('count').value = parseInt(count_id)-1;
			jQuery(id).remove();
		}





   </script>

 <script type='text/javascript'>

$(document).ready(function(){
    	var counter = 2;
	$('#add_spec').click(function(){
        $('#file_tools').before('<br><div class="col-lg-8" id="f'+counter+'"><input name="file[]" type="file"></div>');
        $('#del_file').fadeIn(0);
    counter++;
    });
    $('img#del_file').click(function(){
        if(counter==3){
            $('#del_file').hide();
        }
        counter--;
        $('#f'+counter).remove();
    });
});
</script>
<script>
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;

      }
	 function getshop_ajax(id)
	{

		var passmerchantid = 'id='+id;
		   $.ajax( {
			      type: 'get',
				  data: passmerchantid,
				  url: '<?php echo url('product_getmerchantshop_ajax'); ?>',
				  success: function(responseText){
				   if(responseText)
				   { 	//alert(responseText);
					$('#Select_Shop').html(responseText);
				   }
				}
			});
	}



	function getcolorname(id)
	{

		 var passcolorid = 'id='+id+"&co_text_box="+$('#co').val();

		   $.ajax( {
			      type: 'get',
				  data: passcolorid,
				  url:'<?php echo url('product_getcolor'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {

					var get_result = responseText.split(',');
					if(get_result[3]=="success")
					{
						$('#colordiv').css('display', 'block');

					$('#showcolor').append('<span style="width:220px;padding:10px;border:5px solid '+get_result[2]+';margin:0px;">'+get_result[0]+'<input type="checkbox"  name="colorcheckbox'+get_result[1]+'"style="padding-left:30px;" checked="checked" value="1" ></span>&nbsp;&nbsp;');
					var co_name_js = $('#co').val();
					$('#co').val(get_result[1]+","+co_name_js);

					}
					else if(get_result[3]=="failed")
					{
						alert("Already color is choosed.");
					}
					else
					{
							alert("something went wrong .");
					}

				   }
				}
			});

	}
	function getsizename(id)
	{

		 var passsizeid = 'id='+id+"&si_text_box="+$('#si').val();

		   $.ajax( {
			      type: 'get',
				  data: passsizeid,
				  url: '<?php echo url('product_getsize'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {

					var get_result = responseText.split(',');
					if(get_result[3]=="success")
					{
                                  		var count=parseInt($('#productsizecount').val())+1;
						$("#productsizecount").val(count);
						$('#sizediv').css('display', 'block');
						//$('#quantitydiv').css('display', 'block');

					$('#showsize').append('<span style="padding-right:10px;">Select Size:</span><span style="color:#ff0099;padding-right:90px">'+get_result[2]+'<input type="checkbox"  name="sizecheckbox'+get_result[1]+'"style="padding-left:30px;" checked="checked" value="1" ></span>');
					$('#showquantity').append('<input type="text" name="quantity'+get_result[1]+'" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input> ');

					var co_name_js = $('#si').val();
					$('#si').val(get_result[1]+","+co_name_js);


					}
					else if(get_result[3]=="failed")
					{
						alert("Already size is choosed.");
					}
					else
					{
							alert("something went wrong .");
					}

				   }
				}
			});

	}





	function select_main_cat(id)
	{
		   var passData = 'id='+id;
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('product_getmaincategory'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {
				   		// alert(responseText);
					$('#maincategory').html(responseText);
				   }
				}
			});
	}

	function select_sub_cat(id)
	{
		var passData = 'id='+id;
        $('#tbody_spec').empty();
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('product_getsubcategory'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {
					$('#subcategory').html(responseText);
				   }
				}
			});
            $.ajax({
                type: 'get',
                data: passData,
                url: '<?php echo url('product_getspecification'); ?>',
                success: function(specification){
                    $('#specificationcount').val(specification.length);
                    for(var i=0; i<specification.length; i++){
                        $('#tbody_spec').append('<tr><td><input type="hidden" name="spec'+i+'" value="'+specification[i].sp_id+'">'+specification[i].sp_name+'</td><td><input class="form-control" type="text" name="spectext'+i+'" value=""></td></tr>');
                    }
                }
            });
	}

	function select_second_sub_cat(id)
	{
		var passData = 'id='+id;
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('product_getsecondsubcategory'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {
					$('#secondsubcategory').html(responseText);
				   }
				}
			});
	}
	// Onload to get selected category
	$( document ).ready(function() {

		var passmerchantid = 'id=<?php echo $merchant; ?>';
		$.ajax( {
			      type: 'get',
				  data: passmerchantid,
				  url: '<?php echo url('product_getmerchantshop'); ?>',
				  success: function(responseText){
				   if(responseText)
				   { 	//alert(responseText);
					$('#Select_Shop').html(responseText);
				   }
				}
			});







	var passData = 'edit_id=<?php echo $maincategory; ?>';
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('product_edit_getmaincategory'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {
				   		// alert(responseText);
					$('#maincategory').html(responseText);
				   }
				}
			});
	var passData = 'edit_sub_id=<?php echo $subcategory; ?>';
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('product_edit_getsubcategory'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {
				   		// alert(responseText);
					$('#subcategory').html(responseText);
				   }
				}
			});
	var passData = 'edit_second_sub_id=<?php echo $secondsubcategory; ?>';
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('product_edit_getsecondsubcategory'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {

				   	//alert(responseText);
					$('#secondsubcategory').html(responseText);
				   }
				}
			});
	});


	$( document ).ready(function() {

			$('#specificationdetails').hide();
			var title 		 = $('#Product_Title');
			var category		 = $('#category');
			var maincategory 	 = $('#maincategory');
			var subcategory 	 = $('#subcategory');
			var secondsubcategory	 = $('#secondsubcategory');
			var originalprice 	 = $('#Original_Price');
			var inctax = $('#inctax');
			var discountprice 	 = $('#Discounted_Price');
			var shippingamt          = $('#Shipping_Amount');
			var description          = $('#Description');
			var wysihtml5 		 = $('#wysihtml5');
			 	var merchant		 = $('#Select_Merchant');
			var shop		 = $('#Select_Shop');
            var metatitle = $('#Meta_Title');
			var metakeyword		 = $('#Meta_Keywords');
			var metadescription	 = $('#Meta_Description');
			var file		 = $('#file');
 var pquantity		 =$('#Quantity_Product');

	 $('#Original_Price').keypress(function (e){
         if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57))
			{
		    originalprice.css('border', '1px solid red');
				$('#error_msg').html('Numbers Only Allowed');
				originalprice.focus();
                originalprice.val('');
		}
        else if (parseInt(originalprice.val()) < 0) {
            originalprice.css('border', '1px solid red');
 			$('#error_msg').html('Original Price should be positive integer');
 			originalprice.focus();
 			originalprice.val('');
        }
		else
		{
            originalprice.css('border', '');
			$('#error_msg').html('');
		}
        });
	$('#inctax').keypress(function (e){
		if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57))
			{
		    inctax.css('border', '1px solid red');
				$('#error_msg').html('Numbers Only Allowed');
				inctax.focus();
				return false;
		}
		else
		{
            inctax.css('border', '');
			$('#error_msg').html('');
		}
        });
	$('#Discounted_Price').keyup(function (e){
		if(parseInt(discountprice.val()) >= parseInt(originalprice.val()) )
		{
			discountprice.css('border', '1px solid red');
			$('#error_msg').html('Discount price should be less than original price');
			discountprice.focus();
			discountprice.val(originalprice.val() - 1);
		}
        else if(parseInt(discountprice.val()) < 0)
        {
            discountprice.css('border', '1px solid red');
			$('#error_msg').html('Discount Price should be positive integer');
			discountprice.focus();
			discountprice.val('');
        }
		else
		{
            discountprice.css('border', '');
			$('#error_msg').html('');
		}
        });


        $('#submit_product').click(function() {

	 var checkedoptioncolor = $('input:radio[name=productcolor]:checked').val();
	 var colorid=$("#selectprocolor option:selected").val();
         var checkspec = $('input:radio[name=specification]:checked').val();
	 var  sizecnt=$("#productsizecount").val();
  var shipamtchecked = $('input:radio[name=shipamt]:checked').val();

		// if($.trim(title.val()) == "")
		// {
		// 	title.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Title');
		// 	title.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	title.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
		// if(category.val() == 0 || category.val() == null || category.val() == '')
		// {
		// 	category.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Select Category');
		// 	category.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	category.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
		// if(maincategory.val() == 0 || maincategory.val() == null || maincategory.val() == '')
		// {
		// 	maincategory.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Select Main Category');
		// 	maincategory.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	maincategory.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
		// if(subcategory.val() == 0)
		// {
		// 	subcategory.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Select Sub Category');
		// 	subcategory.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	subcategory.css('border', '');
    	// 	$('#error_msg').html('');
		// }
		// if(secondsubcategory.val() == 0)
		// {
		// 	secondsubcategory.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Select Sub Category');
		// 	secondsubcategory.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	secondsubcategory.css('border', '');
    	// 	$('#error_msg').html('');
		// }
		// if(pquantity.val() == 0)
		// {
    	// 	pquantity.css('border', '1px solid red');
    	// 	$('#error_msg').html('Please Enter Product Quantity');
    	// 	pquantity.focus();
    	// 	return false;
		// }
		// else
		// {
    	// 	pquantity.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
		// if(originalprice.val() == 0)
		// {
		// 	originalprice.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Original Price');
		// 	originalprice.focus();
		// 	return false;
		// }
		// else if(isNaN(originalprice.val()) == true)
		// {
		// 	originalprice.css('border', '1px solid red');
		// 	$('#error_msg').html('Numbers Only Allowed');
		// 	originalprice.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	originalprice.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
		// if(isNaN(discountprice.val()) == true)
		// {
		// 	discountprice.css('border', '1px solid red');
		// 	$('#error_msg').html('Numbers Only Allowed');
		// 	discountprice.focus();
		// 	return false;
		// }
		// else if(parseInt(discountprice.val()) > parseInt(originalprice.val()) )
		// {
		// 	discountprice.css('border', '1px solid red');
		// 	$('#error_msg').html('Discount price sholud be less than original price');
		// 	discountprice.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	discountprice.css('border', '');
    	// 	$('#error_msg').html('');
		// }
		// if($.trim(wysihtml5.val()) == '')
		// {
		// 	wysihtml5.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Description');
		// 	wysihtml5.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	wysihtml5.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        // if($('#selectprograde').val()==0 || $('#selectprograde').val()=='' || $('#selectprograde').val()==null)
        // {
        //     $('#selectprograde').css('border', '1px solid red');
		// 	$('#error_msg').html('Please Select Grade');
		// 	$('#selectprograde').focus();
		// 	return false;
        // }else {
        //     $('#selectprograde').css('border', '');
    	// 	$('#error_msg').html('');
        // }
        //
        // if($('#select_color').val() == '')
        // {
        //     $('#select_color').css('border', '1px solid red');
		// 	$('#error_msg').html('Please Choose Color');
		// 	$('#select_color').focus();
		// 	return false;
        // }
        // else
        // {
        //     $('#select_color').css('border', '');
        //     $('#error_msg').html('');
        // }
        //
        // if($.trim(metatitle.val()) == "")
		// {
		// 	metatitle.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Metatitle');
		// 	metatitle.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	metatitle.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
        // if($.trim(metakeyword.val()) == "")
		// {
		// 	metakeyword.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Metakeyword');
		// 	metakeyword.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	metakeyword.css('border', '');
    	// 	$('#error_msg').html('');
		// }
        //
		// if($.trim(metadescription.val()) == "")
		// {
		// 	metadescription.css('border', '1px solid red');
		// 	$('#error_msg').html('Please Enter Metadescription');
		// 	metadescription.focus();
		// 	return false;
		// }
		// else
		// {
    	// 	metadescription.css('border', '');
    	// 	$('#error_msg').html('');
		// }

	    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        <?php if($file_get_path[0] == ''){ ?>
            if ($('#file').val() != '') {
                if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            		file.focus();
            		file.css('border', '1px solid red');
            		$('#error_msg').html('Please choose valid image');
            		return false;
        		}
        		else
        		{
            		file.css('border', '');
            		$('#error_msg').html('');
        		}
            }
        <?php }else{ ?>
            file.css('border', '');
    		$('#error_msg').html('');
        <?php } ?>
	});

	});


	</script>

    <script type="text/javascript">
        $("#selectprograde").on('change', function(){

            if($("#selectprograde").val() == 1 || $("#selectprograde").val() == 2)
            {
                $("#div_warranty").show();
            }
            else
            {
                $("#div_warranty").hide();
                $("#extended_warranty").val(0);
            }
        });

        $("#extended_warranty").on("change", function(){
            if($("#extended_warranty").prop('checked'))
            {
                $("#extended_warranty").val(1);
            }
            else
            {
                $("#extended_warranty").val(0);
            }
        });
    </script>


  	<script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

	<!-- PAGE LEVEL SCRIPTS -->
	<script src="<?php echo url('')?>/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo url('')?>/assets/js/editorInit.js"></script>
    <script src="<?php echo url('')?>/assets/js/wholesale.js"></script>
    <script>
        $(function () { formWysiwyg(); });
	</script>

</body>
<!-- END BODY -->
</html>
