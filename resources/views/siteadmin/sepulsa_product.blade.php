<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |    Sepulsa Products                   </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->


		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a >  Sepulsa Products                   </a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>  Sepulsa Products                    </h5>

        </header>
        @if (Session::has('error'))
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {!! Session::get('error') !!}
        </div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {!! Session::get('success') !!}
        </div>
        @endif
        <div id="div-1" class="accordion-body collapse in body">
           <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
           <div class="form-group form-horizontal">
             <label>Show</label>
             <ul id="managepro" class="nav nav-tabs" style="padding-bottom:10px;">
               <li class="active"><a data-toggle="tab" href="#mobile">Mobile</a></li>
               <li><a data-toggle="tab" href="#electricity">Electricity</a></li>
               <li><a data-toggle="tab" href="#electricity_postpaid">Electricity Postpaid</a></li>
               <li><a data-toggle="tab" href="#bpjs_kesehatan">BPJS Kesehatan</a></li>
               <li><a data-toggle="tab" href="#game">Game</a></li>
               <li><a data-toggle="tab" href="#multifinance">Multifinance</a></li>
               <li><a data-toggle="tab" href="#telkom_postpaid">Telkom Postpaid</a></li>
               <li><a data-toggle="tab" href="#pdam">PDAM</a></li>
             </ul>
            </div>
            <p></p>
            <div class="tab-content">
                <div id="mobile" class="tab-pane active">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Paket Data: activate to sort column ascending">Paket Data</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($mobile_products)
                            <?php $i = 1; ?>
                            @foreach($mobile_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="  ">
                                    @if($product['field_paket_data'] == true)
                                    Ya
                                    @else
                                    Tidak
                                    @endif
                                </td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="field_paket_data" value="{{$product['field_paket_data']}}">
                                        <input type="hidden" name="field_denom" value="{{$product['field_denom']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="electricity" class="tab-pane">
                    <table id="dataTables-example1" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($electricity_products)
                            <?php $i = 1; ?>
                            @foreach($electricity_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})">
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="electricity_postpaid" class="tab-pane">
                    <table id="dataTables-example2" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($electricity_postpaid_products)
                            <?php $i = 1; ?>
                            @foreach($electricity_postpaid_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="bpjs_kesehatan" class="tab-pane">
                    <table id="dataTables-example3" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($bpjs_kesehatan_products)
                            <?php $i = 1; ?>
                            @foreach($bpjs_kesehatan_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="game" class="tab-pane">
                    <table id="dataTables-example4" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($game_products)
                            <?php $i = 1; ?>
                            @foreach($game_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="multifinance" class="tab-pane">
                    <table id="dataTables-example5" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($multi_products)
                            <?php $i = 1; ?>
                            @foreach($multi_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="telkom_postpaid" class="tab-pane">
                    <table id="dataTables-example6" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($telkom_postpaid_products)
                            <?php $i = 1; ?>
                            @foreach($telkom_postpaid_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div id="pdam" class="tab-pane">
                    <table id="dataTables-example7" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Tipe: activate to sort column ascending">Tipe</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Nama Produk: activate to sort column ascending">Nama Produk</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Operator: activate to sort column ascending">Operator</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Harga: activate to sort column ascending">Harga(Rp)</th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Tersedia : activate to sort column ascending">Tersedia</th>

                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 71px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="product_filter">
                            @if($pdam_products)
                            <?php $i = 1; ?>
                            @foreach($pdam_products as $product)
                            <tr class="gradeA odd">
                                <td class="sorting_1">{{$i}}</td>
                                <td>{{$product['type']}}</td>
                                <td class="center  ">{{$product['label']}}</td>
                                <td class="center  ">{{$product['operator']}}</td>
                                <td class="center  ">{{$product['price']}}</td>
                                <td class="center  ">
                                    @if($product['enabled'] == 1)
                                    <p style="color:green">Tersedia</p>
                                    @else
                                    <p style="color:red">Tidak Tersedia</p>
                                    @endif
                                </td>

                                <td class="center  ">
                                    <form action="{{url('add_sepulsa_product')}}" method="post" id="form{{$product['product_id']}}">
                                        <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                        <input type="hidden" name="type" value="{{$product['type']}}">
                                        <input type="hidden" name="label" value="{{$product['label']}}">
                                        <input type="hidden" name="operator" value="{{$product['operator']}}">
                                        <input type="hidden" name="nominal" value="{{$product['nominal']}}">
                                        <input type="hidden" name="price" value="{{$product['price']}}">
                                        <input type="hidden" name="enabled" value="{{$product['enabled']}}">
                                        <i class="icon icon-plus icon-2x" style="margin-left:15px;color:green;" title="Tambahkan" onclick="submitForm({{$product['product_id']}})"></i>
                                    </form>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -->
			</div>
        </div>
								</div>
								</div>
                </div>
          </div>
              </div>
              </div>
        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
     <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <!-- <script>$(document).ready(function() {
    $('table.display').DataTable();
} );</script> -->
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example1').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example2').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example3').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example4').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example5').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example6').dataTable();
         });
         $(document).ready(function () {
             $('#dataTables-example7').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
        function number_format(number,decimals,dec_point,thousands_sep) {
            number  = number*1;//makes sure `number` is numeric value
            var str = number.toFixed(decimals?decimals:0).toString().split('.');
            var parts = [];
            for ( var i=str[0].length; i>0; i-=3 ) {
                parts.unshift(str[0].substring(Math.max(0,i-3),i));
            }
            str[0] = parts.join(thousands_sep?thousands_sep:',');
            return str.join(dec_point?dec_point:'.');
        }
        function submitForm(product_id)
        {
            document.getElementById('form'+product_id).submit();
        }
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>
</body>
     <!-- END BODY -->
</html>
