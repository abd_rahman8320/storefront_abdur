<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |Manage FAQ </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->




		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a >Manage FAQ</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Manage FAQ</h5>

        </header>


 <div class="row">

    <div class="col-lg-11 panel_marg">
    @if (Session::has('updated_result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('updated_result') !!}</div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		@endif
        @if (Session::has('insert_result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('insert_result') !!}</div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		@endif
        @if (Session::has('delete_result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('delete_result') !!}</div>
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
           <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
                <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 59px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">S.No</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Question: activate to sort column ascending">Question</th>

                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Answer: activate to sort column ascending">Answer</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 73px;" aria-label="Edit: activate to sort column ascending">Edit</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Status: activate to sort column ascending">Status</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 76px;" aria-label="Delete: activate to sort column ascending">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach ($faqresult as $info)
                            <tr>
                                <td class="text-center">{!!$i!!}</td>
                                <td class="text-center">{!!$info->faq_name!!}</td>
                                <td class="text-center">{!!$info->faq_ans!!}</td>
                                <td class="text-center">
                                    <a href="<?php echo url('edit_faq/'.$info->faq_id); ?>">
                                        <i class="icon icon-edit icon-2x"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?php if($info->faq_status== 0){ ?>
                                        <a href="{!! url('status_faq_submit').'/'.$info->faq_id.'/'.'1'!!}">
                                            <i class="icon icon-ban-circle icon-2x"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a href="{!! url('status_faq_submit').'/'.$info->faq_id.'/'.'0'!!}">
                                            <i class="icon icon-ban-circle icon-2x icon-me"></i>
                                        </a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo url('delete_faq/'.$info->faq_id); ?>">
                                        <i class="icon icon-trash icon-2x"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $i=$i+1; ?>
                        @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                            <ul class="pagination"></ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                            <ul class="pagination">
                                <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                            </ul>
                        </div>
                    </div>
                </div>
           </div>
       </div>
    	<!-- <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width:10%;"class="text-center">S.No</th>
                  <th class="text-center">Question</th>
				   <th style="text-align:center;">Answer</th>
				  <th style="text-align:center;">Edit</th>
				   <th style="text-align:center;">Status </th>
				  <th style="text-align:center;">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
	  @foreach ($faqresult as $info)
                <tr>
                  <td class="text-center">{!!$i!!}</td>
                  <td class="text-center">{!!$info->faq_name!!}</td>
				     <td class="text-center">{!!$info->faq_ans!!}</td>
				<td class="text-center"><a href="<?php echo url('edit_faq/'.$info->faq_id); ?>"><i class="icon icon-edit icon-2x"></i></a></td>



  		<td class="text-center"><?php if($info->faq_status== 0){ ?><a href="{!! url('status_faq_submit').'/'.$info->faq_id.'/'.'1'!!}"><i class="icon icon-ban-circle icon-2x"></i>
                  </a> <?php } else { ?> <a href="{!! url('status_faq_submit').'/'.$info->faq_id.'/'.'0'!!}">
                   <i class="icon icon-ban-circle icon-2x icon-me"></i></a> <?php } ?></td>
		 <td class="text-center"><a href="<?php echo url('delete_faq/'.$info->faq_id); ?>"><i class="icon icon-trash icon-2x"></i></a></td>
                </tr>


	<?php $i=$i+1; ?>
		@endforeach


              </tbody>
            </table> -->
    </div>

   </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
  {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <!-- PAGE LEVEL SCRIPTS -->
<script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#dataTables-example').dataTable();
});
</script>

</body>
     <!-- END BODY -->
</html>
