<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD -->
    <head>
         <meta charset="UTF-8" />
    <?php
        $metatitle = DB::table('nm_generalsetting')->get();
        if($metatitle){
        foreach($metatitle as $metainfo) {
                $metaname=$metainfo->gs_metatitle;
                $metakeywords=$metainfo->gs_metakeywords;
                $metadesc=$metainfo->gs_metadesc;
             }
        }
        else
        {
             $metaname="";
             $metakeywords="";
             $metadesc="";
        }
        ?>
        <title><?php echo $metaname  ;?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />
        <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); foreach($favi as $fav) {} ?>
        <link rel="shortcut icon" href="<?php echo url(); ?>/assets/favicon/<?php echo $fav->imgs_name; ?>">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo url(); ?>/assets/favicon/<?php echo $fav->imgs_name; ?>">
         <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- PAGE LEVEL STYLES -->
        <link rel="stylesheet" href="{{url('')}}/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="{{url('')}}/assets/css/login.css" />
        <link rel="stylesheet" href="{{url('')}}/assets/plugins/magic/magic.css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END HEAD -->

    <body>
        <!-- PAGE CONTENT -->
        <div class="container">
            <div class="text-center">
                <img src="<?php echo url()."/"; ?>assets/logo/logo-web-baru.png" alt="Logo">
            </div>
            <form class="form-signin form-horizontal" action="{{url('create_password')}}" method="post">
                <p class="text-muted text-center btn-block btn-primary disabled">Create Password</p>
                <div id="error_msg"  style="color:#F00;font-weight:800"  > </div>
                <input type="hidden" name="user_name" value="{{$user_name}}">
                <input type="password" name="" value="" style="display:none">
                <input placeholder="New Password" class="form-control" type="password" name="password_new" value="" id="password_new" required>
                <input placeholder="Confirm New Password" class="form-control" type="password" name="password_confirm" value="" id="password_confirm" required>
                <center><button class="btn text-muted text-center btn-warning" type="submit" name="button" id="submit">Submit</button></center>
            </form>

        </div>

        <script src="{{url('')}}/assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="{{url('')}}/assets/plugins/bootstrap/js/bootstrap.js"></script>
        <script src="{{url('')}}/assets/js/login.js"></script>
        <script type="text/javascript">
        $('#submit').click(function(){
            if ($('#password_new').val() != $('#password_confirm').val()) {
                $('#password_new').css('border', '1px solid red');
                $('#password_confirm').css('border', '1px solid red');
                $('#error_msg').html('Password Is Not Match');
                return false;
            }
            if ($('#password_new').val() == '') {
                $('#password_new').css('border', '1px solid red');
                return false;
            }
            if ($('#password_confirm').val() == '') {
                $('#password_confirm').css('border', '1px solid red');
                return false;
            }
        })
        </script>

    </body>
</html>
