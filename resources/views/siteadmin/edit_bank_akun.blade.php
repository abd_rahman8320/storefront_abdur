<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?>| Payment Settings</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/theme.css" />
	  <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/plan.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/MoneAdmin.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
       {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a>Payment Settings</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Payment Settings</h5>

        </header>

         @if ($errors->any())
		<div class="alert alert-warning alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

      {!! implode('', $errors->all('<li>:message</li>')) !!}
    </div>

		@endif
         @if (Session::has('success'))
		<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{!! Session::get('success') !!}</div>
		@endif
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">

   {!! Form::open(array('url'=>'edit_bank_akun_submit','class'=>'form-horizontal')) !!}

   <?php
    foreach ($detail_bank_akun as $row ) {}
   ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Tambah Akun Bank
        </div>
        <!-- input hide -->
        <input type="hidden" name="id_bank" id="id_bank" value="<?php echo $row->id;?>">


        <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-lg-3" for="text1">Nama Bank<span class="text-sub">*</span></label>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="nama_bank" placeholder="Nama Bank" id="nama_bank" value="<?php echo $row->nama_bank;?>">
                </div>
            </div>
        </div>

        <div id="whole_currency_div">
    		<div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-3" for="text1">Nomor Rekening<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="nomor_rekening" placeholder="Nama Rekening" id="nomor_rekening" value="<?php echo $row->nomor_rekening;?>">
                    </div>
                </div>
            </div>
    		<div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-3" for="text1">Nama Pemilik Rekening<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="nama_pemilik_rekening" placeholder="Atas Nama" id="nama_pemilik_rekening" value="<?php echo $row->nama_pemilik_rekening;?>">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="form-group">

        <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>

        <div class="col-lg-1">
            <button class="btn btn-info btn-sm btn-grad"><a style="color:#fff">Update</a></button>
        </div>
        <div class="col-lg-1">
            <a id="btn_reset" class="btn btn-danger btn-sm btn-grad" style="color:#fff">Reset</a>
        </div>

    </div>

                </form>
                </div>

        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
  {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url(); ?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script>

	function select_cur_val(id)
	{

		var passData = 'id='+id;

		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('select_currency_value_ajax'); ?>',
				  success: function(responseText){
				   if(responseText)
				   {
				   	//alert(responseText);
					$('#whole_currency_div').html(responseText);
				   }
				}
			});
	}

  $("#btn_reset").on('click', function(){
    $("#nama_bank").val("<?php echo $row->nama_bank;?>");
    $("#nomor_rekening").val("<?php echo $row->nomor_rekening;?>");
    $("#nama_pemilik_rekening").val("<?php echo $row->nama_pemilik_rekening;?>");
  });

	</script>
    <!-- END GLOBAL SCRIPTS -->

</body>
     <!-- END BODY -->
</html>
