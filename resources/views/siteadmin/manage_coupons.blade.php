<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |Manage Schedule</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
   <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
	 <link href="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


        <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a>Home</a></li>
                                <li class="active"><a >Manage Schedule</a></li>
                            </ul>
                    </div>
                </div>

				<center>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Manage Promo</h5>

         </header>
           @if (Session::has('block_message'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
           <div class="accordion-body collapse in body" id="div-1">
           <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label>

		   </label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter">
		   </div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div>
           <div class="form-group form-horizontal">
             <label>Show</label>
             <select class="form-control" id="status">
               <option value="2">All</option>
               <option value="1">Active</option>
               <option value="0">Non-Active</option>
             </select>
            </div>
            <p></p>

           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                <thead>
                    <tr role="row">
    										<th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending">No</th>
    										<th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Kode Coupons</th>
                        <th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Status Coupons</th>
                        <th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Judul Promo</th>
  									</tr>
                </thead>
                <tbody id="time_schedule">
                  <?php $i = 1 ;
              									  foreach($get_coupons_h as $get_time) {
                             ?>
                                    <tr class="gradeA odd">
                                            <td class="sorting_1"><?php echo $i; ?></td>
                                            <td class="center  "><?php echo substr($get_time->promoc_voucher_code,0,45); ?></td>
                                            <td class="center  "><?php echo $get_time->status_coupons; ?></td>
                                            <td class="center  "><?php echo $get_time-> schedp_title; ?></td>
                                        </tr>
									                      <?php $i++;
                                        }
			                         ?>

										</tbody>
                                </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"></div></div></div></div><div class="row">

								<div class="col-sm-6">
								<div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
								<ul class="pagination">
								<li id="dataTables-example_previous" tabindex="0" aria-controls="dataTables-example" class="paginate_button previous disabled">
								</li>
								</ul></div></div></div></div>
        </div>
        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

   <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
       $('#status').on('change', function(){
           jQuery('#time_schedule').empty();
           $.ajax({
               type: 'get',
               datatype: 'json',
               url: "{{url('get_schedule')}}",
               data: {
                   status: $('#status').val()
               },
               success: function(response){
                //    alert('test');
                   for(var i=0;i<response.length;i++)
                   {
                       var j = i+1;
                       if(response[i].schedp_RecurrenceType==1){
                           var RecurrenceType = 'Daily';
                       }else if(response[i].schedp_RecurrenceType==2){
                           var RecurrenceType = 'Weekly';
                       }
                       var url_active_deactive = "<?php echo url('block_deals');?>/"+response[i].schedp_id;
                       if (response[i].schedp_status==1) {
                           var active_deactive = "<a href='"+url_active_deactive+"/0' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>";
                       }else if (response[i].schedp_status==0) {
                           var active_deactive = "<a href='"+url_active_deactive+"/1' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>";
                       }
                       var url_edit = '<?php echo url('edit_plan_timer'); ?>/'+response[i].schedp_id;
                       var url_delete = '<?php echo url('delete_schedule'); ?>/'+response[i].schedp_id;
                       var url_details = '<?php echo url('schedule_details'); ?>/'+response[i].schedp_id;
                       jQuery('#time_schedule').append("<tr class='gradeA odd'> <td class='sorting_1'>"+j+"</td> <td class='center'>"+response[i].schedp_title+"</td> <td class='center'>"+response[i].schedp_start_date+"</td> <td class='center'>"+response[i].schedp_end_date+"</td> <td class='center'>"+RecurrenceType+"</td> <td class='center'><a href='"+url_edit+"' > <i class='icon icon-edit' style='margin-left:15px;'></i></a>"+active_deactive+" <a href="+url_delete+" ><i class='icon icon-trash icon-1x' style='margin-left:15px;'></i></a></td> <td class='center'><a href='"+url_details+"'>View details</a></td> </tr>");
                        // alert('test');
                        // jQuery('#time_schedule').append("<tr> <td class='sorting_1'>"+j+"</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> </tr>");
                   }
               }
           });
       });
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>
</body>
     <!-- END BODY -->
</html>
