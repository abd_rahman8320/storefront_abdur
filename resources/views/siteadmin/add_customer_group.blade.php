<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <?php
            $metatitle = DB::table('nm_generalsetting')->get();
            if($metatitle){
                foreach($metatitle as $metainfo) {
                    $metaname=$metainfo->gs_metatitle;
                    $metakeywords=$metainfo->gs_metakeywords;
                    $metadesc=$metainfo->gs_metadesc;
                }
            }
            else
            {
                $metaname="";
                $metakeywords="";
                $metadesc="";
            }
        ?>
        <title><?php echo $metaname  ;?> | Add Customer Group</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />

        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
        <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--END GLOBAL STYLES -->

    </head>
    <!-- END HEAD -->

    <body class="padTop53">
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            {!! $adminheader !!}
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            {!! $adminleftmenus !!}
            <!--END MENU SECTION -->
            <div></div>

            <!-- PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <!-- START Example breadcrumb -->
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li><a >Customers</a></li>
                                <li class="active"><a>Add Customer Group</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Example breadcrumb -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons">
                                        <i class="icon-edit"></i>
                                    </div>
                                    <h5>View Users</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('error') !!}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif

                                <div class="row" style="margin-top: 25px; margin-bottom: 15px;">
                                    <div class="col-lg-12">
                                        <div id="div-1" class="accordion-body collapse in body" style="margin-left:20px">
                                            <form class="form-horizontal" action="{{url('add_customer_group_submit')}}" method="post">
                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">Customer Group Name</label>
                                                    <div class="col-lg-4">
                                                        <input class="form-control" type="text" name="customer_group" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-lg-3"></label>
                                                    <button type="submit" name="button" class="btn btn-warning btn-sm btn-grad">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div id="div-1" class="accordion-body collapse in body">
                                    <div class="dataTables_wrapper form-inline" role="grid" id="dataTables-example_wrapper">
                                        <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 10px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="User Name: activate to sort column ascending">Nama Customer Group</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Full Name: activate to sort column ascending">Edit</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Email: activate to sort column ascending">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; ?>
                                                @foreach($get_customer_group as $customer_group)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$customer_group->cus_group_desc}}</td>
                                                    <td><a href="{{url('edit_customer_group')}}/{{$customer_group->cus_group_id}}"><button type="button" name="button" class="btn btn-success btn-sm btn-grad">Edit</button></a></td>
                                                    <td><a href="{{url('delete_customer_group')}}/{{$customer_group->cus_group_id}}"><button type="button" name="button" class="btn btn-danger btn-sm btn-grad">Delete</button></a></td>
                                                </tr>
                                                <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- FOOTER -->
        {!! $adminfooter !!}
        <!--END FOOTER -->

        <!-- GLOBAL SCRIPTS -->
        <script src="assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
    </body>
</html>
