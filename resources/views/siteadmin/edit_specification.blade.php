 <?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?>| Add specification</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
	  <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->
	<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a >Edit specification</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Edit specification</h5>

        </header>

        <div id="div-1" class="accordion-body collapse in body">
@if ($errors->any())
         <br>
		 <ul style="color:red;">
		{!! implode('', $errors->all('<li>:message</li>')) !!}
		</ul>
		@endif
        @if (Session::has('message'))
		<p style="background-color:green;color:#fff;">{!! Session::get('message') !!}</p>
		@endif

		 {!! Form::open(array('url'=>'update_specification_submit','class'=>'form-horizontal')) !!}
@foreach ($specificationresult as $info)
<input type="hidden" name="id" value="<?php echo $id; ?>">

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-3">Specification name<span class="text-sub">*</span></label>

                    <div class="col-lg-7">
			{!! Form::text('spedit_name',$info->sp_name,array('id'=>'spedit_name','class'=>'form-control')) !!}

                    </div>
                </div>
<!--
                <div class="form-group">
                    <label for="limiter" class="control-label col-lg-3">Specification group name<span class="text-sub">*</span></label>

                    <div class="col-lg-7">
                        <select class="form-control"  id="speditgroup_name" name="speditgroup_name">


		<?php $groupid=$info->sp_spg_id;
		 foreach ($groupresult as $group) { ?>
            <option value="<?php echo $group->spg_id;?>" <?php if($groupid==$group->spg_id){ ?> selected <?php } ?>>{!!$group->spg_name!!}
</option>
          	<?php }?>

        </select>
                    </div>
                </div>
 -->
               <div class="form-group">
                    <label for="text1" class="control-label col-lg-3" id="editsortorder" name="editsortorder">Sort order<span class="text-sub">*</span></label>

                    <div class="col-lg-1">

		{!! Form::text('spedit_order',$info->sp_order,array('id'=>'spedit_order','class'=>'form-control')) !!}
                    </div>
                </div>

<!--  Type  -->
                    <div class="form-group">
                    <label for="limiter" class="control-label col-lg-3">Type<span class="text-sub">*</span></label>

                    <div class="col-lg-7">
                        <select class="form-control"  name="type" id="mySelect" onchange="myFunction()">


  <option value="Text" <?php if($info->sp_type == 'Text'){ ?>selected<?php } ?>>Teks
  <option value="Selection" <?php if($info->sp_type == 'Selection'){ ?>selected<?php } ?>>Selection

        </select>

<?php $i=1; ?>
        @foreach ($values as $value)

       <div id='Option{{$i}}'><input type='text' name='value[]' value="{{$value->short_desc}}" />
          <span class="btn btn-default " style="color:#000" id='Delete' onclick='deleteInput({{$i}})'>Delete</span>
      </div>

       <?php $i++; ?>

        @endforeach

        <div id="demo"></div>
        @if($info->sp_type == 'Text')
        <span class="btn btn-warning btn-sm btn-grad" style="color:#fff;display:none;" id='Add'>Add Option</span>
        @else
        <span class="btn btn-warning btn-sm btn-grad" style="color:#fff;" id='Add'>Add Option</span>
        @endif
        <input type="hidden" id="i" value="0" name="">

        <!-- <p id="demo"><div class='Option'><input type='text' name='txtTest'/> <span class='Delete'>Delete</span></div>
<span class='Add'>Add Option</span></p> -->

                    </div>
                </div>

                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-3"><span  class="text-sub"></span></label>

                    <div class="col-lg-8">
                     <button type="submit" class="btn btn-warning btn-sm btn-grad" style="color:#fff">Update</button>
                      <a href="<?php echo url('manage_specification');?>" class="btn btn-default btn-sm btn-grad" style="color:#000">Cancel</a>

                    </div>

                </div>


           @endforeach
         </form>

        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

<script>
  function deleteInput(i)
  {
    //alert('test delete');
     $('#Option'+i).remove();
  }
    $('#Delete').on('click',function(e){
      $(this).parent().remove();
    });
    $('#Add').on('click',function(e){
        //alert('test');
        // $('#Option:last').after($('#Option:first').clone());
        $('#i').val($('#i').val() + 1);
        var i = $('#i').val();
        $('#demo').append("<div id='Option"+i+"'><input type='text' name='value[]'/> <span class='btn btn-default' id='Delete' onclick='deleteInput("+i+")'>Delete</span></div>");

    });

var lastValue;
<?php $j=1; ?>
$("#mySelect").bind("click", function(e){
    lastValue = $(this).val();
}).bind("change", function(e){
    changeConfirmation = confirm("Really?");
    if (changeConfirmation) {
      <?php
for($j=1; $j<=$i; $j++)
{
  ?> $('#Option{{$j}}').hide(); <?php
}
?>
       // $('#Option{{$j}}').hide()
        // Delete Query di sini
    } else {
        $(this).val(lastValue);
    }

});


function myFunction() {
    var x = document.getElementById("mySelect").value;
    if (x == "Text") {

      document.getElementById("demo").innerHTML='';
      $('#Add').hide();

    }
    else
        if ( x == "Selection") {
          $('#Add').show();
          $('#i').val(); //di val harusnya awal (1)
          var i = $('#i').val();
          document.getElementById("demo").innerHTML= "<div id='Option"+i+"'><input type='text' name='value[]'/> <span class='btn btn-default' id='Delete' onclick='deleteInput("+i+")'>Delete</span></div>" ;
                  }
}

</script>

</body>
     <!-- END BODY -->
</html>
