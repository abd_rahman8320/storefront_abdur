<?php header("Access-Control-Allow-Origin: *"); ?>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <?php
            $metatitle = DB::table('nm_generalsetting')->get();
            if($metatitle){
                foreach($metatitle as $metainfo) {
                    $metaname=$metainfo->gs_metatitle;
                    $metakeywords=$metainfo->gs_metakeywords;
                    $metadesc=$metainfo->gs_metadesc;
                }
            }
            else
            {
                $metaname="";
                $metakeywords="";
                $metadesc="";
            }
        ?>
        <title><?php echo $metaname  ;?> | Update Sepulsa Product</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />

        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
        <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--END GLOBAL STYLES -->

    </head>
    <!-- END HEAD -->

    <body class="padTop53">
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            {!! $adminheader !!}
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            {!! $adminleftmenus !!}
            <!--END MENU SECTION -->
            <div></div>

            <!-- PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <!-- START Example breadcrumb -->
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li><a >Product</a></li>
                                <li class="active"><a>Update Sepulsa Product</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Example breadcrumb -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons">
                                        <i class="icon-edit"></i>
                                    </div>
                                    <h5>Update Sepulsa Product</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('error') !!}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif

                                <div id="div-1" class="accordion-body collapse in body">
                                    <form class="form-horizontal" action="{{url('update_sepulsa_product_process')}}" method="post">
                                        <div id="error_msg"  style="color:#F00;font-weight:800">
                                        </div>
                                        <input type="hidden" name="enabled" value="{{$enabled}}">

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product ID</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="number" name="product_id" value="{{$product_id}}" readonly required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product Type</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" name="type" value="{{$type}}" readonly required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product Title</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" name="title" value="{{$label}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product Operator</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" name="operator" value="{{$operator}}" readonly required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product Nominal</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="number" name="nominal" value="{{$nominal}}" readonly required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product Price</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="number" name="price" value="{{$price}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2">Product Discount Price</label>
                                            <div class="col-lg-8">
                                                @if(isset($disprice))
                                                <input class="form-control" type="number" name="disprice" value="{{$disprice}}">
                                                @else
                                                <input class="form-control" type="number" name="disprice" value="0">
                                                @endif

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label col-lg-2"></label>
                                            <div class="col-lg-8">
                                                <button class="btn btn-success btn-sm btn-grad" type="submit" name="submit">Submit</button>
                                                <a href="{{url('')}}/{{$url_cancel}}"><button class="btn btn-default btn-sm btn-grad" type="button" name="button">Cancel</button></a>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- FOOTER -->
        {!! $adminfooter !!}
        <!--END FOOTER -->

        <!-- GLOBAL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

    </body>
</html>
