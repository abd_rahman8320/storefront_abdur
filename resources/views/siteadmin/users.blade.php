<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <?php
            $metatitle = DB::table('nm_generalsetting')->get();
            if($metatitle){
                foreach($metatitle as $metainfo) {
                    $metaname=$metainfo->gs_metatitle;
                    $metakeywords=$metainfo->gs_metakeywords;
                    $metadesc=$metainfo->gs_metadesc;
                }
            }
            else
            {
                $metaname="";
                $metakeywords="";
                $metadesc="";
            }
        ?>
        <title><?php echo $metaname  ;?> | Users</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />

        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
        <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--END GLOBAL STYLES -->

    </head>
    <!-- END HEAD -->

    <body class="padTop53">
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            {!! $adminheader !!}
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            {!! $adminleftmenus !!}
            <!--END MENU SECTION -->
            <div></div>

            <!-- PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <!-- START Users & Access Management / Users -->
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li><a >Users & Access Management</a></li>
                                <li class="active"><a >Users</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Users & Access Management / Users -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons">
                                        <i class="icon-edit"></i>
                                    </div>
                                    <h5>Users</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('error') !!}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif

                                <div id="div-1" class="accordion-body collapse in body">
                                    <form class="" action="{{url('delete_users')}}" method="post">
                                    <a href="{{url('add_users')}}"><button class="btn btn-info btn-sm btn-grad" type="button" name="button">Add</button></a>
                                    <button class="btn btn-danger btn-sm btn-grad" type="button" name="button" id="delete_button" data-toggle="modal" data-target="#deletes_modal">Delete Selected</button>
                                    <p></p>
                                    <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
                                        <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 10px;" aria-label=""><input type="checkbox" name="" value="" id="check_all"></th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 10px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">S.No</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="User Name: activate to sort column ascending">User Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Full Name: activate to sort column ascending">Full Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Email: activate to sort column ascending">Email</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Account Active?: activate to sort column ascending">Account Active?</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Password Expire: activate to sort column ascending">Password Expire?</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php $i=1; ?>
                                                @foreach($users_list as $user)
                                                <tr>
                                                    <td><input type="checkbox" name="id[]" value="{{$user->user_id}}" id="checkbox{{$i}}"></td>
                                                    <td>{{$i}}</td>
                                                    <td>{{$user->user_name}}</td>
                                                    <td>{{$user->user_profile_full_name}}</td>
                                                    <td>{{$user->user_profile_email}}</td>
                                                    @if($user->user_active == 1)
                                                    <td><input disabled readonly type="checkbox" name="" value="" checked></td>
                                                    @else
                                                    <td><input disabled readonly type="checkbox" name="" value=""></td>
                                                    @endif
                                                    @if($user->user_pwd_expire == 1)
                                                    <td><input disabled readonly type="checkbox" name="" value="" checked></td>
                                                    @else
                                                    <td><input disabled readonly type="checkbox" name="" value=""></td>
                                                    @endif
                                                    <td><a href="" data-toggle="modal" data-target="#delete_modal{{$user->user_id}}">Delete</a> | <a href="{{url('view_users')}}/{{$user->user_id}}">Views</a></td>
                                                </tr>
                                                <?php $i++; ?>
                                                <div id="delete_modal{{$user->user_id}}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">

                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                                                            </div>

                                                            <div class="modal-body">
                                                                <p>Are you sure?</p>
                                                                <p>You can't use this user name again and you can't restore it</p>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <a href="{{url('delete_user')}}/{{$user->user_id}}"><button class="btn btn-success btn-sm btn-grad" type="button">Yes</button></a>
                                                                <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        <div id="deletes_modal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <p>Are you sure?</p>
                                                        <p>You can't use this users name again and you can't restore it</p>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button class="btn btn-success btn-sm btn-grad" type="submit">Yes</button>
                                                        <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END MAIN WRAPPER -->

        <!-- FOOTER -->
        {!! $adminfooter !!}
        <!--END FOOTER -->

        <!-- GLOBAL SCRIPTS -->
        <script src="assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
            $('#check_all').click(function(){
                if ($('#check_all').is(':checked')) {
                    for (var i = 1; i < {{$i}}; i++) {
                        $('#checkbox'+i).prop('checked', true);
                    }
                }else {
                    for (var i = 1; i < {{$i}}; i++) {
                        $('#checkbox'+i).prop('checked', false);
                    }
                }
            });
            $('#delete_button').click(function(){
                var success = 0;
                for (var i = 0; i < {{$i}}; i++) {
                    if ($('#checkbox'+i).is(':checked')) {
                        var success = 1;
                    }
                }
                if (success == 0) {
                    return false;
                }
            });
        </script>
    </body>
</html>
