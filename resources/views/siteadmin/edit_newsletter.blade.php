<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?>| Edit Newsletter</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
     <link href="<?php echo url('')?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <script src="<?php echo url(); ?>/assets/plugins/ckeditor/ckeditor.js"></script>
    <!-- <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/Markdown.Editor.hack.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/jquery.cleditor-hack.css" />
    <link rel="stylesheet" href="<?php echo url(); ?>/assets/css/bootstrap-wysihtml5-hack.css" /> -->
     <style>
                        /*ul.wysihtml5-toolbar > li {
                            position: relative;
                        }*/
                    </style>
    <!-- END PAGE LEVEL  STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

  		<!-- HEADER SECTION -->
        {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}

        <!--END MENU SECTION -->
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a>Home</a></li>
                                <li class="active"><a >Edit Newsletter</a></li>
                            </ul>
                    </div>
                </div>


    <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Edit Newsletter</h5>

        </header>
            @if ($errors->any())
		<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{!! implode('', $errors->all('<li>:message</li>')) !!}</div>
		@endif
        @if (Session::has('error'))
		<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{!! Session::get('error') !!}</div>
		@endif
          @if (Session::has('success'))
		<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{!! Session::get('success') !!}</div>
		@endif
           {!! Form::open(array('url'=>'edit_newsletter_submit','class'=>'form-horizontal')) !!}
           @foreach($banner_detail as $banner_det)
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">
              <input type="hidden" name="id_emb" id="id_emb" value="{!!$banner_det->id_emb!!}"/>

                <div class="form-group">
                          <label for="text1" class="control-label col-lg-2">Judul<span class="text-sub">*</span></label>

                          <div class="col-lg-8">
                           <input type="text" class="form-control" placeholder="Title" value="{!! $banner_det->judul !!}" name="judul" id="judul">
                          </div>
                </div>
 				        <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Subject<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                     <input type="text" class="form-control" placeholder="News Letter Subject" value="{!! $banner_det->subject !!}" name="subject" id="subject">
                    </div>
                </div>
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Start Date<span class="text-sub">*</span></label>
                    <div class="col-lg-3">
                        <div id="datetimepicker1" class="date input-group">
                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="startdate" name="startdate" value="{!! $banner_det->tanggal_kirim !!}" class="form-control"></input>
                            <span class="add-on input-group-addon">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2">Message<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                                    <div class="tab-pane fade active in" >
                                        <form>
                                        <textarea id="editor1" name="message" class="form-control" rows="10"><?php echo $banner_det->isi ?></textarea>
                                        <script>
                                                        // Replace the <textarea id="editor1"> with a CKEditor
                                                        // instance, using default configuration.
                                                        CKEDITOR.replace('editor1');
                                                    </script>
                                        <div class="form-actions">
                                            <br />
                                           <button type="submit" class="btn btn-warning btn-sm btn-grad"><a style="color:#fff">Save</a></button>
                     <button type="reset" class="btn btn-default btn-sm btn-grad"><a style="color:#000">Reset</a></button>
                                        </div>
                                    </form>
                                    </div>
                    </div>
                </div>






         </form>
         @endforeach

          {!! Form::close() !!}
        </div>
    </div>
</div>

    </div>


</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   {!! $adminfooter !!}
    <!--END FOOTER -->


   <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url(); ?>/assets/plugins/jquery-2.0.3.min.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- END GLOBAL SCRIPTS -->

         <!-- PAGE LEVEL SCRIPTS -->
     <script src="<?php echo url(); ?>/assets/plugins/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/bootstrap-wysihtml5-hack.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/CLEditor1_4_3/jquery.cleditor.min.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/pagedown/Markdown.Converter.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/pagedown/Markdown.Sanitizer.js"></script>
    <script src="<?php echo url(); ?>/assets/plugins/Markdown.Editor-hack.js"></script>
    <script src="<?php echo url(); ?>/assets/js/editorInit.js"></script>
    <script src="<?php echo url('');?>/assets/js/bootstrap-datetimepicker.min.js"></script>
     <script type="text/javascript">
         $(function() {
             $('#datetimepicker1').datetimepicker();
         });
         $(function (){
           formWysiwyg();
         });
     </script>
</body>
     <!-- END BODY -->
</html>
