<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <?php
            $metatitle = DB::table('nm_generalsetting')->get();
            if($metatitle){
                foreach($metatitle as $metainfo) {
                    $metaname=$metainfo->gs_metatitle;
                    $metakeywords=$metainfo->gs_metakeywords;
                    $metadesc=$metainfo->gs_metadesc;
                }
            }
            else
            {
                $metaname="";
                $metakeywords="";
                $metadesc="";
            }
        ?>
        <title><?php echo $metaname  ;?> | Cash Failed Orders</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    	<meta content="" name="description" />
    	<meta content="" name="author" />

        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
        <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
        <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
        <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--END GLOBAL STYLES -->
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    </head>
    <!-- END HEAD -->

    <body class="padTop53">
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            {!! $adminheader !!}
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            {!! $adminleftmenus !!}
            <!--END MENU SECTION -->
            <div></div>

            <!-- PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <!-- START Transactions / Cash All Orders -->
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumb">
                                <li><a >Transactions</a></li>
                                <li class="active"><a >Cash Failed Orders</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Transactions / Cash All Orders -->

                    <center>
                        <form  action="{!!action('TransactionController@product_all_orders')!!}" method="POST">
                            <input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
                            <div class="row">
                            <br>
                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <div class="col-sm-6">From Date</div>
                                    <div class="col-sm-6">
                                        <input type="text" name="from_date"  class="form-control" id="datepicker-8">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="item form-group">
                                    <div class="col-sm-6">To Date</div>
                                    <div class="col-sm-6">
                                        <input type="text" name="to_date"  id="datepicker-9" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2">
                                    <input type="submit" name="submit" class="btn btn-block btn-success" value="Search">
                                </div>
                            </div>

                        </form>
                    </center>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box dark">
                                <header>
                                    <div class="icons">
                                        <i class="icon-edit"></i>
                                    </div>
                                    <h5>Cash Failed Orders</h5>
                                </header>
                                @if (Session::has('error'))
                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('error') !!}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::get('success') !!}
                                </div>
                                @endif

                                <div id="div-1" class="accordion-body collapse in body">
                                    <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">
                                        <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 10px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">S.No</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="User Name: activate to sort column ascending">ID Transaksi</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Full Name: activate to sort column ascending">Customers</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Email: activate to sort column ascending">Product Title</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Account Active?: activate to sort column ascending">Amount(Rp)</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Password Expire: activate to sort column ascending">Status</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Actions: activate to sort column ascending">Transaction Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Actions: activate to sort column ascending">Cicilan SNP</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Actions: activate to sort column ascending">Bukti Pembayaran</th>
                                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 67px;" aria-label="Actions: activate to sort column ascending">View Details</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1; ?>
                                                @if(isset($_POST['submit']))
                                                    @foreach($allproductrep as $allorders_list)
                                                    <?php $amount = floatval($allorders_list->total_belanja) +floatval($allorders_list->shipping) - floatval($allorders_list->diskon) + floatval($allorders_list->warranty); ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$allorders_list->transaction_id}}</td>
                                                        <td>{{$allorders_list->cus_name}}</td>
                                                        <td>{{$allorders_list->pro_title}}</td>
                                                        <td class="center">{{$amount}}</td>

                                                        @if($allorders_list->order_status == 1)
                                                            <td class="center"><span class="colr3">Success</span></td>
                                                        @elseif($allorders_list->order_status == 2)
                                                            <td class="center"><span class="colr3">Completed</span></td>
                                                        @elseif($allorders_list->order_status == 3)
                                                            <td class="center"><span class="colr3">Hold</span></td>
                                                        @elseif($allorders_list->order_status == 4)
                                                            <td class="center"><span class="colr3">Failed</span></td>
                                                        @else
                                                            <td class="center"></td>
                                                        @endif

                                                        <td class="center">{{$allorders_list->order_date}}</td>

                                                        @if($allorders_list->is_cicilan == 1)
                                                            <td class="center"><span class="colr2">Yes</span></td>
                                                        @else
                                                            <td class="center"><span class="colr2">No</span></td>
                                                        @endif

                                                        @if($allorders_list->bukti_transfer == '')
                                                            <td>Belum Dibayar</td>
                                                        @else
                                                            <td class="center"><a href="{{url('download_file_bukti/'.$allorders_list->bukti_transfer)}}" class="btn btn-info btn-sm btn-grad" target="_self">Download</a></td>
                                                        @endif

                                                        <td style="text-align: center;"><a href="<?php echo url('detail_transaksi_admin')."/".$allorders_list->transaction_id;?>">View Detail</a></td>
                                                    </tr>
                                                    @endforeach
                                                @else
                                                    @foreach($allorders as $allorders_list)
                                                    <?php $amount = floatval($allorders_list->total_belanja) +floatval($allorders_list->shipping) - floatval($allorders_list->diskon) + floatval($allorders_list->warranty); ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$allorders_list->transaction_id}}</td>
                                                        <td>{{$allorders_list->cus_name}}</td>
                                                        <td>{{$allorders_list->pro_title}}</td>
                                                        <td class="center">{{$amount}}</td>

                                                        @if($allorders_list->order_status == 1)
                                                            <td class="center"><span class="colr3">Success</span></td>
                                                        @elseif($allorders_list->order_status == 2)
                                                            <td class="center"><span class="colr3">Completed</span></td>
                                                        @elseif($allorders_list->order_status == 3)
                                                            <td class="center"><span class="colr3">Hold</span></td>
                                                        @elseif($allorders_list->order_status == 4)
                                                            <td class="center"><span class="colr3">Failed</span></td>
                                                        @else
                                                            <td class="center"></td>
                                                        @endif

                                                        <td class="center">{{$allorders_list->order_date}}</td>

                                                        @if($allorders_list->is_cicilan == 1)
                                                            <td class="center"><span class="colr2">Yes</span></td>
                                                        @else
                                                            <td class="center"><span class="colr2">No</span></td>
                                                        @endif

                                                        @if($allorders_list->bukti_transfer == '')
                                                            <td>Belum Dibayar</td>
                                                        @else
                                                            <td class="center"><a href="{{url('download_file_bukti/'.$allorders_list->bukti_transfer)}}" class="btn btn-info btn-sm btn-grad" target="_self">Download</a></td>
                                                        @endif

                                                        <td style="text-align: center;"><a href="<?php echo url('detail_transaksi_admin')."/".$allorders_list->transaction_id;?>">View Detail</a></td>
                                                    </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END MAIN WRAPPER -->

        <!-- FOOTER -->
        {!! $adminfooter !!}
        <!--END FOOTER -->

        <!-- GLOBAL SCRIPTS -->
        <script src="assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script>
            $(function() {
                $( "#datepicker-8" ).datepicker({
                    prevText:"click for previous months",
                    nextText:"click for next months",
                    showOtherMonths:true,
                    selectOtherMonths: false
                });
                $( "#datepicker-9" ).datepicker({
                    prevText:"click for previous months",
                    nextText:"click for next months",
                    showOtherMonths:true,
                    selectOtherMonths: true
                });
            });
        </script>
    </body>
</html>
