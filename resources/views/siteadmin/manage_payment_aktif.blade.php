<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?> |Add Grouping</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
   <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
	 <link href="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


        <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a>Home</a></li>
                                <li class="active"><a >Manage Payment Active</a></li>
                            </ul>
                    </div>
                </div>
<div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Manage Payment Active</h5>

         </header>
         <input type="hidden" id="return_url" value="<?php echo 'https://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];?>" />

         <!-- pilihan untuk manage groping -->
        <div class="row" style="margin-top: 25px; margin-bottom: 15px;">
            <div class="col-lg-12">
                <div id="div-1" class="accordion-body collapse in body" style="margin-left: 20px;">
                  {!! Form::open(array('url'=>'add_header_payment','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Tambahkan Jenis Pembayaran<span class="text-sub">*</span></label>

                        <div class="col-lg-3" >
                            <div >
                                <input name="nama_header" type="text" class="form-control"></input>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass1" class="control-label col-lg-4"><span  class="text-sub"></span></label>

                        <div class="col-lg-2">
                           <button class="btn btn-warning btn-sm btn-grad"><a style="color:#fff"  >Simpan</a></button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
        <!-- end of combo box -->

           @if (Session::has('block_message'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
           <div class="accordion-body collapse in body" id="div-1">
           <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label>

		   </label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter">
		   </div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div>

           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                <thead>
                    <tr role="row">
    										<th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending">No</th>
    										<th aria-label="Browser: activate to sort column ascending" style="width: 500px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Payment Header Name</th>
                        <th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Choose Payment Active</th>
                        <th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Choose Payment Channel</th>
                        <th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Choose Customer Group</th>
                        <th aria-label="Engine version: activate to sort column ascending" style="width: 50px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Action</th>
  									</tr>
                </thead>
                <tbody>
                  <?php $i = 1 ;
                          foreach($list_payment_header as $row) {
                  ?>
                                <tr class="gradeA odd">
                                  <td class="sorting_1"><?php echo $i; ?></td>
                                  <td class="center  "><?php echo $row->nama_pay_header; ?></td>
                                  <td>
                                    <select name="pilihan_payment" id="pilihan_payment" class="btn btn-default" onchange="update_payment_aktif(this.value,'<?php echo $row->id_pay_header;?>')">
                                    <option value="0" <?php if($row->id_pay_detail==null){?> selected <?php } ?>></option>
                                    <?php
                                      foreach($list_payment_details as $row1) {
                                        ?>
                                          <option value="<?php echo $row1->id_pay_detail?>" <?php if($row->id_pay_detail==$row1->id_pay_detail){?> selected <?php } ?>><?php echo $row1->nama_pay_detail?></option>
                                        <?php
                                      }
                                    ?>
                                    </select>
                                  </td>
                                  <td>
                                      <?php if($row->id_pay_detail == 1){ ?>
                                        <select class="btn btn-default" name="pilihan_channel" id="pilihan_channel" onchange="update_channel(this.value, '<?php echo $row->id_pay_header;?>')">
                                            <option value="0" <?php if($row->channel_pay_header==null){?> selected <?php } ?>></option>
                                            <option value="01" <?php if($row->channel_pay_header=='01'){?> selected <?php } ?>>Credit Card Visa/Master IDR</option>
                                            <option value="02" <?php if($row->channel_pay_header=='02'){?> selected <?php } ?>>Mandiri ClickPay</option>
                                            <option value="03" <?php if($row->channel_pay_header=='03'){?> selected <?php } ?>>KlikBCA</option>
                                            <option value="04" <?php if($row->channel_pay_header=='04'){?> selected <?php } ?>>Doku Wallet</option>
                                            <option value="05" <?php if($row->channel_pay_header=='05'){?> selected <?php } ?>>ATM Permata VA LITE</option>
                                            <option value="06" <?php if($row->channel_pay_header=='06'){?> selected <?php } ?>>BRI e-Pay</option>
                                            <option value="07" <?php if($row->channel_pay_header=='07'){?> selected <?php } ?>>ATM Permata VA</option>
                                            <option value="08" <?php if($row->channel_pay_header=='08'){?> selected <?php } ?>>Mandiri Multipayment LITE</option>
                                            <option value="09" <?php if($row->channel_pay_header=='09'){?> selected <?php } ?>>Mandiri Multipayment</option>
                                            <option value="10" <?php if($row->channel_pay_header=='10'){?> selected <?php } ?>>ATM Mandiri VA LITE</option>
                                            <option value="11" <?php if($row->channel_pay_header=='11'){?> selected <?php } ?>>ATM Mandiri VA</option>
                                            <option value="12" <?php if($row->channel_pay_header=='12'){?> selected <?php } ?>>PayPal</option>
                                            <option value="13" <?php if($row->channel_pay_header=='13'){?> selected <?php } ?>>BNI Debit Online (VCN)</option>
                                            <option value="14" <?php if($row->channel_pay_header=='14'){?> selected <?php } ?>>Alfamart</option>
                                            <option value="15" <?php if($row->channel_pay_header=='15'){?> selected <?php } ?>>Credit Card Visa/Master Multi Currency</option>
                                            <option value="16" <?php if($row->channel_pay_header=='16'){?> selected <?php } ?>>Tokenization</option>
                                            <option value="17" <?php if($row->channel_pay_header=='17'){?> selected <?php } ?>>Recur</option>
                                            <option value="18" <?php if($row->channel_pay_header=='18'){?> selected <?php } ?>>KlikPayBCA</option>
                                            <option value="19" <?php if($row->channel_pay_header=='19'){?> selected <?php } ?>>CIMB Clicks</option>
                                            <option value="20" <?php if($row->channel_pay_header=='20'){?> selected <?php } ?>>PTPOS</option>
                                            <option value="21" <?php if($row->channel_pay_header=='21'){?> selected <?php } ?>>Sinarmas VA Full</option>
                                            <option value="22" <?php if($row->channel_pay_header=='22'){?> selected <?php } ?>>Sinarmas VA Lite</option>
                                            <option value="23" <?php if($row->channel_pay_header=='23'){?> selected <?php } ?>>MOTO</option>

                                        </select>
                                        <?php }elseif($row->id_pay_detail == 2){ ?>
                                            <select class="btn btn-default" name="pilihan_channel" id="pilihan_channel" onchange="update_channel(this.value, '<?php echo $row->id_pay_header;?>')">
                                                <option value="" <?php if($row->channel_pay_header==null){?> selected <?php } ?>></option>
                                                <option value="01" <?php if($row->channel_pay_header=='01'){?> selected <?php } ?>>Transfer Bank</option>
                                                <option value="02" <?php if($row->channel_pay_header=='02'){?> selected <?php } ?>>Cash</option>
                                            </select>
                                        <?php }elseif($row->id_pay_detail == 3){ ?>
                                            <select class="btn btn-default" name="pilihan_channel" id="pilihan_channel" onchange="update_channel(this.value, '<?php echo $row->id_pay_header;?>')">
                                                <option value="" <?php if($row->channel_pay_header==null){?> selected <?php } ?>></option>
                                                <option value="00" <?php if($row->channel_pay_header=='00'){?> selected <?php } ?>>BCA KlikPay</option>
                                                <option value="01" <?php if($row->channel_pay_header=='01'){?> selected <?php } ?>>BCA Virtual Account</option>
                                                <option value="02" <?php if($row->channel_pay_header=='02'){?> selected <?php } ?>>Permata Virtual Account</option>
                                                <option value="03" <?php if($row->channel_pay_header=='03'){?> selected <?php } ?>>Kredivo</option>
                                            </select>
                                        <?php } ?>
                                  </td>
                                  <td>
                                      <select class="btn btn-default" name="cus_group_id" id="cus_group_id">
                                          <option value="0" idpay="{{$row->id_pay_header}}"></option>
                                          @if($list_customer_group != null)
                                              @foreach($list_customer_group as $customer_group)
                                                  @if($row->cus_group_id == $customer_group->cus_group_id)
                                                    <option value="{{$customer_group->cus_group_id}}" idpay="{{$row->id_pay_header}}" selected>{{$customer_group->cus_group_desc}}</option>
                                                  @else
                                                    <option value="{{$customer_group->cus_group_id}}" idpay="{{$row->id_pay_header}}">{{$customer_group->cus_group_desc}}</option>
                                                  @endif
                                              @endforeach
                                          @endif
                                      </select>
                                  </td>
                                  <td class="center     ">
                                    <a href="<?php echo url('hapus_payment_header/'.$row->id_pay_header);?>" class="btn btn-danger btn-sm btn-grad" target="_self">Hapus</a></td>
                                  </td>

                                </tr>

                  <?php $i++;
                        }
                  ?>

										</tbody>
                                </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"></div></div></div></div><div class="row">

								<div class="col-sm-6">
								<div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers">
								<ul class="pagination">
								<li id="dataTables-example_previous" tabindex="0" aria-controls="dataTables-example" class="paginate_button previous disabled">
								</li>
								</ul></div></div></div></div>
        </div>
        </div>
    </div>
</div>

    </div>

                    </div>




                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

   <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->
     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('')?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('')?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->
      <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
       $('#cus_group_id').on('change', function(){
           var returl = $('#return_url').val();
           var idpay = this.options[this.selectedIndex].getAttribute('idpay');
           $.ajax({
              method:'get',
              url:'{{url("update_payment_customer_group")}}',
              data: {
                  cus_group_id: $("#cus_group_id").val(),
                  id_pay_header: idpay
              },
              success:function(){
                  location.reload();
              }
           });
       });
        function update_channel(id_channel, id_header)
        {
            //alert(id_trans);
            var returl =$('#return_url').val();
            //alert(id_header+" | "+id_detail);
            $.ajax({
                method: "get",
                url: "{{url('update_channel')}}",
                datatype: "json",
                data: {
                    aid_channel: id_channel,
                    aid_header: id_header
                },
                success: function(d){
                    location.reload();
                    }
            });
        }
        function update_payment_aktif(id_detail, id_header)
        {
          //alert(id_trans);
          var returl =$('#return_url').val();
          //alert(id_header+" | "+id_detail);
          $.ajax({
              method: "get",
              url: "{{url('update_payment_aktif_x')}}",
              datatype: "json",
              data: {
                  aid_detail:id_detail,
                  aid_header: id_header
              },
              success: function(d){
                  location.reload();
                  }
          });
        }

         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });


      </script>
</body>
     <!-- END BODY -->
</html>
