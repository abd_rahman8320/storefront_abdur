<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
<?php
   $metatitle = DB::table('nm_generalsetting')->get();
    if($metatitle){
    foreach($metatitle as $metainfo) {
        $metaname=$metainfo->gs_metatitle;
         $metakeywords=$metainfo->gs_metakeywords;
         $metadesc=$metainfo->gs_metadesc;
         }
         }
    else
    {
         $metaname="";
         $metakeywords="";
          $metadesc="";
    }
    ?>
    <title><?php echo $metaname  ;?>|Products All Orders</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/MoneAdmin.css" />
     <link rel="shortcut icon" href="<?php echo url(''); ?>/themes/images/favicon.png">
    <link rel="stylesheet" href="<?php echo url('');?>/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo url('');?>/assets/css/success.css" />
	 <link href="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
{!!$adminheader!!}
        <!-- MENU SECTION -->
{!!$adminleftmenus!!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">

                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >Home</a></li>
                                <li class="active"><a >Products All Orders</a></li>
                            </ul>
                    </div>
                </div>
				<center>
		 <form  action="{!!action('TransactionController@merchant_all_transaction')!!}" method="POST">
							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
							 <div class="row">
							 <br>


							   <div class="col-sm-3">
							    <div class="item form-group">
							<div class="col-sm-6">From Date</div>
							<div class="col-sm-6">
							 <input type="text" name="from_date"  class="form-control" id="datepicker-8">

							  </div>
							  </div>
							   </div>
							    <div class="col-sm-3">
							    <div class="item form-group">
							<div class="col-sm-6">To Date</div>
							<div class="col-sm-6">
							 <input type="text" name="to_date"  id="datepicker-9" class="form-control">

							  </div>
							  </div>
							   </div>

							   <div class="form-group">
							   <div class="col-sm-2">
							 <input type="submit" name="submit" class="btn btn-block btn-success" value="Search">
							 </div>
							</div>

							 </form>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>Products All Orders</h5>

        </header>
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">




				 <div class="form-group">
                    <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
					 <div class="col-lg-4">

		</div><label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>
					  <div class="col-lg-4"></div>

                    </div>
                </div>

                <div class="form-group col-lg-12">
                    	<div id="div-1" class="accordion-body collapse in body">
           <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label>

		   </label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter">
		   </div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
					<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 69px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">No</th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending">ID Transaksi</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending">Merchant</th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending">Customer</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 158px;" aria-label="Email: activate to sort column ascending">Product Title</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="City: activate to sort column ascending">Price(Rp)</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending">Transaction Date</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending">Warranty Expired</th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending">Outbound Status</th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending">View Details</th>
				</tr>
      </thead>

      <tbody>

      <?php $i = 1 ;

        if(isset($_POST['submit']))
			  {

          foreach($allproductrep as $allorders_list)
          {
            $status_outbound;

            if($allorders_list->status_outbound == 1)
            {
              $status_outbound = 'Belum Diproses';
            }
            else if($allorders_list->status_outbound == 2)
            {
              $status_outbound = 'Pick';
            }
            else if($allorders_list->status_outbound == 3)
            {
              $status_outbound = 'Packing';
            }
            else if($allorders_list->status_outbound == 4)
            {
              $status_outbound = 'Shipping';
            }
            else if($allorders_list->status_outbound == 5)
            {
              $status_outbound = 'Receiving';
            }
            else if($allorders_list->status_outbound == 6)
            {
              $status_outbound = 'Barang Tidak Ada';
            }

					?>

					<tr class="gradeA odd">
            <td class="sorting_1"><?php echo $i;?></td>
            <td class="     "><?php echo $allorders_list->transaction_id;?></td>
            <td class="     "><a href="" data-toggle="modal" data-target="#merchant{{$i}}"> <?php echo $allorders_list->mer_fname;?> </a></td>
            <?php $flag_customer = 0; ?>
            @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[3] == 'customer' && $priv_name[4] == 'merchant_all_transaction')
                    <td class="     "><a href="" data-toggle="modal" data-target="#customer{{$i}}"> <?php echo $allorders_list->ship_name;?> </a></td>
                    <?php $flag_customer = 1; break; ?>
                @endif
            @endforeach
            @if($flag_customer == 0)
                <td class="     "><?php echo $allorders_list->ship_name;?></td>
            @endif
            <td class="     "><?php echo $allorders_list->pro_title;?></td>
            <td class="center"><?php echo number_format(($allorders_list->order_amt),2,",",".");?></td>
            <td class="center"><?php echo $allorders_list->order_date;?></td>
            <?php
            $order_received = strtotime($allorders_list->order_tgl_konfirmasi_penerimaan_barang);
            $expired = strtotime('+'.$warranty_settings[0]->lama_garansi_dasar.' days', $order_received);
            $now = strtotime('now');
            $substract = $expired - $now;
            $day = round($substract/86400); ?>
            @if($allorders_list->mer_fname == 'Kukuruyuk')
                @if($allorders_list->order_tgl_konfirmasi_penerimaan_barang == 0)
                    <td class="center">Not yet received</td>
                @elseif($day > 0)
                    <td class="center" style="color:green;">{{$day}} Days Left</td>
                @else
                    <td class="center" style="color:red;">Expired</td>
                @endif
            @else
                <td class="center">The Product Has No Warranty</td>
            @endif
            <td class="center"><b><span class="colr2" style="color: green;"><?php echo $status_outbound;?></span></b></td>
            <td style="text-align: center;"><a href="<?php echo url('detail_transaksi_admin')."/".$allorders_list->transaction_id;?>">View Detail</a></td>
          </tr>
          <div id="merchant{{$i}}" class="modal fade" role="dialog">
              <div class="modal-dialog">
                  <div class="modal-content">

                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                      </div>

                      <div class="modal-body">
                          <b>Merchant Contact</b>
                          <p></p>
                          <div class="row">
                              <div class="col-lg-3">
                                  Name
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->mer_fname}} {{$allorders_list->mer_lname}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Phone
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->mer_phone}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Email
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->mer_email}}
                              </div>
                          </div>
                      </div>

                      <div class="modal-footer">
                          <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>
          <div id="customer{{$i}}" class="modal fade" role="dialog">
              <div class="modal-dialog">
                  <div class="modal-content">

                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                      </div>

                      <div class="modal-body">
                          <b>Customer Contact</b>
                          <p></p>
                          <div class="row">
                              <div class="col-lg-3">
                                  Name
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->ship_name}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Address
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->ship_address1}}, {{$allorders_list->sdis_name}}, {{$allorders_list->dis_name}}, {{$allorders_list->ci_name}}, {{$allorders_list->ship_postalcode}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Phone
                              </div>
                              <div class="col-lg-3">
                                  :{{$allorders_list->ship_phone}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Email
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->ship_email}}
                              </div>
                          </div>
                      </div>

                      <div class="modal-footer">
                          <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>
      <?php $i++; }

			  }
        else{

					foreach($allorders as $allorders_list) {

					$status_outbound;

          if($allorders_list->status_outbound == 1)
          {
            $status_outbound = 'Belum Diproses';
          }
          else if($allorders_list->status_outbound == 2)
          {
            $status_outbound = 'Pick';
          }
          else if($allorders_list->status_outbound == 3)
          {
            $status_outbound = 'Packing';
          }
          else if($allorders_list->status_outbound == 4)
          {
            $status_outbound = 'Shipping';
          }
          else if($allorders_list->status_outbound == 5)
          {
            $status_outbound = 'Receiving';
          }
          else if($allorders_list->status_outbound == 6)
          {
            $status_outbound = 'Barang Tidak Ada';
          }
					?>


					<tr class="gradeA odd">
            <td class="sorting_1"><?php echo $i;?></td>
            <td class="     "><?php echo $allorders_list->transaction_id;?></td>
            <td class="     "><a href="" data-toggle="modal" data-target="#merchant{{$i}}"> <?php echo $allorders_list->mer_fname;?> </a></td>
            <?php $flag_customer = 0; ?>
            @foreach($privileges as $priv)
                <?php $priv_name = explode(' ', $priv['rp_priv_name']); ?>
                @if($priv_name[3] == 'customer' && $priv_name[4] == 'merchant_all_transaction')
                    <td class="     "><a href="" data-toggle="modal" data-target="#customer{{$i}}"> <?php echo $allorders_list->ship_name;?> </a></td>
                    <?php $flag_customer = 1; break; ?>
                @endif
            @endforeach
            @if($flag_customer == 0)
                <td class="     "><?php echo $allorders_list->ship_name;?></td>
            @endif
            <td class="     "><?php echo $allorders_list->pro_title;?></td>
            <td class="center"><?php echo number_format(($allorders_list->order_amt),2,",",".");?></td>
            <td class="center"><?php echo $allorders_list->order_date;?></td>
            <?php
            $order_received = strtotime($allorders_list->order_tgl_konfirmasi_penerimaan_barang);
            $expired = strtotime('+'.$warranty_settings[0]->lama_garansi_dasar.' days', $order_received);
            $now = strtotime('now');
            $substract = $expired - $now;
            $day = round($substract/86400); ?>
            @if($allorders_list->mer_fname == 'Kukuruyuk')
                @if($allorders_list->order_tgl_konfirmasi_penerimaan_barang == 0)
                    <td class="center">Not yet received</td>
                @elseif($day > 0)
                    <td class="center" style="color:green;">{{$day}} Days Left</td>
                @else
                    <td class="center" style="color:red;">Expired</td>
                @endif
            @else
                <td class="center">The Product Has No Warranty</td>
            @endif
            <td class="center" style="color: green;"><b><?php echo $status_outbound;?></b></td>
            <td style="text-align: center;"><a href="<?php echo url('detail_transaksi_admin')."/".$allorders_list->transaction_id;?>">View Detail</a></td>
          </tr>
          <div id="merchant{{$i}}" class="modal fade" role="dialog">
              <div class="modal-dialog">
                  <div class="modal-content">

                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                      </div>

                      <div class="modal-body">
                          <b>Merchant Contact</b>
                          <p></p>
                          <div class="row">
                              <div class="col-lg-3">
                                  Name
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->mer_fname}} {{$allorders_list->mer_lname}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Phone
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->mer_phone}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Email
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->mer_email}}
                              </div>
                          </div>
                      </div>

                      <div class="modal-footer">
                          <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>
          <div id="customer{{$i}}" class="modal fade" role="dialog">
              <div class="modal-dialog">
                  <div class="modal-content">

                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
                      </div>

                      <div class="modal-body">
                          <b>Customer Contact</b>
                          <p></p>
                          <div class="row">
                              <div class="col-lg-3">
                                  Name
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->ship_name}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Address
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->ship_address1}}, {{$allorders_list->sdis_name}}, {{$allorders_list->dis_name}}, {{$allorders_list->ci_name}}, {{$allorders_list->ship_postalcode}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Phone
                              </div>
                              <div class="col-lg-3">
                                  :{{$allorders_list->ship_phone}}
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-3">
                                  Email
                              </div>
                              <div class="col-lg-9">
                                  :{{$allorders_list->ship_email}}
                              </div>
                          </div>
                      </div>

                      <div class="modal-footer">
                          <button class="btn btn-default btn-sm btn-grad" type="button" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>
      <?php $i++; } }?>
					</tbody>

      </table><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"></div></div></div></div><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all"></div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"></div></div></div></div><div class="row"><div class="col-sm-6"><div aria-relevant="all" aria-live="polite" role="alert" id="dataTables-example_info" class="dataTables_info"></div></div><div class="col-sm-6"><div id="dataTables-example_paginate" class="dataTables_paginate paging_simple_numbers"><ul class="pagination"></ul></div></div></div></div><div class="row">

								<div class="col-sm-6">
								<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">
								<ul class="pagination">
								<li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">
								</li>
								</ul></div></div></div></div>
        </div>
                </div>

         </form>
        </div>
    </div>
</div>

    </div>

                    </div>



                </div>
            <!--END PAGE CONTENT -->

        </div>

     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   {!!$adminfooter!!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url('');?>/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="<?php echo url('');?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo url('');?>/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo url('');?>/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
     <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"click for previous months",
               nextText:"click for next months",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script>
</body>
     <!-- END BODY -->
</html>
