<?php header("Access-Control-Allow-Origin: *"); ?>
<html>
    <body>
        <form action="{{$redirectURL}}" id="Payment" method="post">
            <input type="hidden" name="klikPayCode" value="{{$klikPayCode}}"/>
            <input type="hidden" name="transactionNo" value="{{$transactionNo}}"/>
            <input type="hidden" name="totalAmount" value="{{$totalAmount}}"/>
            <input type="hidden" name="currency" value="{{$currency}}"/>
            <input type="hidden" name="payType" value="{{$payType}}"/>
            <input type="hidden" name="callback" value="{{$callback}}"/>
            <input type="hidden" name="transactionDate" value="{{$transactionDate}}"/>
            <input type="hidden" name="descp" value="{{$descp}}"/>
            <input type="hidden" name="miscFee" value="{{$miscFee}}"/>
            <input type="hidden" name="signature" value="{{$signature}}"/>
        </form>
        <script type="text/javascript">
    		document.getElementById('Payment').submit();
    	</script>
    </body>
</html>
