<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar end=============================================== -->

	<div class="span12">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active">Checkout</li>
    </ul>

    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Modal header</h3>
        </div>
        <div class="modal-body">
            <p>One fine body…</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary">Save changes</button>
        </div>
    </div>

	<h3>CHECKOUT</h3>

    <div class="row">
    <?php
        if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){  $item_count_header1 = count($_SESSION['cart']); } else { $item_count_header1 = 0; }
	    if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){	 $item_count_header2 = count($_SESSION['deal_cart']); } else { $item_count_header2 = 0; }

        $count = $item_count_header1 + $item_count_header2;

        if($count !=0) {
	?>
        <div class="col-lg-2">
        </div>

        {!! Form::Open(array('url' => 'payment_checkout_process')) !!}

        <div class="span6">
	        <?php if($count > 1)
                {
            ?>
            <!-- <label class="control-label col-lg-8" for="text1">
       Multiple Shipping Address
       &nbsp;<input type="radio" name="mul_shipping_addr" id="mulshipping_addr_1rad" value="yes" style="margin-top:-2px;"> Yes
       &nbsp;<input type="radio" name="mul_shipping_addr" id="mulshipping_addr_2rad" value="no"  style="margin-top:-2px;" checked="true"> No
       <span class="text-sub"></span></label>-->

            <?php
            }

            else {
            ?>

		<!--<input type="radio" style="display:none;" name="mul_shipping_addr" id="mulshipping_addr_2rad" value="no"  style="margin-top:-2px;" checked="true" > -->
		<?php } ?>

		<?php // Shipping address starts here ?>
<div style="border: 1px solid #202020; padding: 14px;margin-bottom:25px;" id="shipping_addr_div"   class="shipping_addr_class">

    <h5 class="panel-deal">SHIPPING ADDRESS<?php //echo $z;?></h5>

    <label class="control-label col-lg-8" for="text1">

       Load Shipping Address For The Profile Details

       &nbsp;<input type="radio" name="shipping_addr<?php //echo $z;?>" id="shipping_addr_1rad<?php //echo $z;?>" onClick="shipping_addr_func(1)" value="yes" style="margin-top:-2px;"> Yes
       &nbsp;<input type="radio" name="shipping_addr<?php //echo $z;?>" id="shipping_addr_2rad<?php //echo $z;?>" onClick="shipping_addr_func(0)" value="no"  style="margin-top:-2px;" checked="true"> No

       <span class="text-sub"></span></label>

    <legend></legend>

    <div class="row">
        <div class="span">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2">Name<span class="text-sub">*</span></label>

                    <div class="col-lg-8">
                        <input type="text" id="fname<?php //echo $z;?>" name="fname<?php //echo $z;?>" placeholder="" required class="form-control">
                         <input type="hidden" id="load_ship<?php //echo $z;?>" name="load_ship<?php //echo $z;?>" value="0">
                    </div>
            </div>
        </div>
    <div class="span">
        <div class="form-group">
            <label for="text1" class="control-label col-lg-2">Address Line1<span class="text-sub">*</span></label>

            <div class="col-lg-8">
                <input type="text" id="addr_line<?php //echo $z;?>" name="addr_line<?php //echo $z;?>" placeholder="" required class="form-control">
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="span">
        <div class="form-group">
            <label for="text1" class="control-label col-lg-2">Address Line2</label>
            <div class="col-lg-8">
                <input type="text" id="addr1_line<?php //echo $z;?>" name="addr1_line<?php //echo $z;?>" placeholder="" class="form-control">
            </div>
        </div>
    </div>
	<div class="span">
		<div class="row-fluid">
			<div class="span4">
				<label class="control-label col-lg-2" for="text1">Country<span class="text-sub">*</span></label>
				<!--<input type="text" id="country<?php //echo $z;?>" name="country<?php //echo $z;?>" placeholder="" required class="form-control">-->

				<select name="country" id="country" required>
				<option value="">Choose Country</option>
				<?php
					foreach($list_country as $list_country2){
				?>
					<option value='<?php echo $list_country2 -> co_id;?>'><?php echo $list_country2 -> co_name;?></option>
				<?php
					}
				?>
			</select>

			<input type="text" id="country_popbox_input" name="country_popbox_input" style="display: none;" >
			</div>
		</div>
	</div>
    </div>

    <div class="row">
        <div class="span">
            <div class="row-fluid">
                <div class="span4">
                    <label class="control-label col-lg-2" for="text1">State<span class="text-sub">*</span></label>
                    <input type="text" id="state" name="state" placeholder="" required class="form-control">
					<input type="hidden" id="state_id" name="state_id" value="">
                </div>
            </div>
        </div>
		<div class="span">
	        <div class="form-group">
	            <label for="text1" class="control-label col-lg-2">City<span class="text-sub">*</span></label>

	            <div class="col-lg-8">
					<input type="text" value="" class="city" id="city" required>
	                <!-- <select name="city" id="city" class="city" required>
	                    <option value="">Choose City </option>

	                </select> -->
	                <!-- H -->
	                <input type="hidden" id="id_city_ha" name="id_city_ha" value="">
					<input type="hidden" name="city" value="" class="city" id="city_code" required>

	                <input type="text" id="city_popbox_input" name="city_popbox_input" style="display: none;" >
	                <input type="hidden" id="shipping_reg_number" name="shipping_reg_number">
	            </div>
	        </div>
	    </div>

    </div>

    <div class="row">
        <div class="span">
            <div class="form-group">
                <label class="control-label col-lg-2" for="text1">Phone Number<span class="text-sub">*</span></label>

                <div class="col-lg-8">
                    <input type="text" class="form-control span"  placeholder="" name="phone1_line<?php //echo $z;?>" required id="phone1_line<?php //echo $z;?>">
                </div>
            </div>
        </div>
		<div id="district_div" class="span">
	        <div class="form-group">
	            <label for="text1" class="control-label col-lg-2">District<span class="text-sub">*</span></label>

	            <div class="col-lg-8">
					<input type="text" value="" class="district" id="district" required>
	                <!-- <select name="district" id="district" class="district" required>
	                    <option value="">Choose District </option>
	                </select> -->
					<input type="hidden" name="district" value="" id="district_code">
	            </div>
	        </div>
	    </div>
	</div>
	<div class="row">
        <div class="span">
            <label class="control-label col-lg-2" for="text1">Email<span class="text-sub">*</span></label>

            <div class="col-lg-8">
                <input type="text" class="form-control span" placeholder=""  required id="email<?php //echo $z;?>" name="email<?php //echo $z;?>" value="">
            </div>
        </div>
		<div id="subdistrict_div" class="span">
	        <div class="form-group">
	            <label for="text1" class="control-label col-lg-2">Sub District<span class="text-sub">*</span></label>

	            <div class="col-lg-8">
					<input type="text" value="" id="subdistrict" class="subdistrict" required>
	                <!-- <select name="subdistrict" id="subdistrict" class="subdistrict" required>
	                    <option value="">Choose Sub District</option>
	                </select> -->
					<input type="hidden" name="subdistrict" value="" id="subdistrict_code">
	            </div>
	        </div>
	    </div>
    </div>
	<div class="row">
		<div class="span">
            <label class="control-label col-lg-2" for="text1">Zip Code<span class="text-sub">*</span></label>

            <div class="col-lg-8">
                <input type="text" class="form-control span" placeholder="Choose Sub District" required readonly id="zipcode<?php //echo $z;?>" name="zipcode<?php //echo $z;?>">
            </div>
        </div>
	</div>

</div> <!-- todo -->

<!-- todo///////////////////////////////// Untuk POBOX ////////////////////////////////////////////////////////////////-->




<!-- todo///////////////////////////////// Untuk POBOX ////////////////////////////////////////////////////////////////-->

<?php // shipping address ends here ?>
   <?php  $z = 1;
   if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){

				//$max=count($_SESSION['cart']);

				$max = 1;
				$overall_total_price='';
				$overall_shipping_price='';
				$overall_tax_price='';



				for($i=0;$i<$max;$i++){
					$pid=$_SESSION['cart'][$i]['productid'];
					$q=$_SESSION['cart'][$i]['qty']; ?>




<?php /*
    <div style="border: 1px solid #202020;
    padding: 14px;margin-bottom:25px;" id="shipping_addr_div<?php echo $pid;?>"  <?php if($z > 1){ ?> class="shipping_addr_class" style="display:none;"<?php } ?>>

        <h5 class="panel-deal">SHIPPING ADDRESS<?php //echo $z;?></h5>



       <label class="control-label col-lg-8" for="text1">

       Load Shipping Address For The Profile Details

       &nbsp;<input type="radio" name="shipping_addr<?php //echo $z;?>" id="shipping_addr_1rad<?php //echo $z;?>" onClick="shipping_addr_func(1)" value="yes" style="margin-top:-2px;"> Yes

       &nbsp;<input type="radio" name="shipping_addr<?php //echo $z;?>" id="shipping_addr_2rad<?php //echo $z;?>" onClick="shipping_addr_func(0)" value="no"  style="margin-top:-2px;" checked="true"> No

       <span class="text-sub"></span></label>

    <legend></legend>

    <div class="row">

    <div class="span">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Name<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="fname<?php //echo $z;?>" name="fname<?php //echo $z;?>" placeholder="" class="form-control">

                         <input type="hidden" id="load_ship<?php //echo $z;?>" name="load_ship<?php //echo $z;?>" value="0">

                    </div>

                </div>

    </div>

 <!--    <div class="span3">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Last Name<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="lname<?php //echo $z;?>" name="lname<?php //echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div> -->
   <div class="span">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Address Line1<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="addr_line<?php //echo $z;?>" name="addr_line<?php //echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>
    </div>

    <div class="row">

    <div class="span">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Address Line2<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="addr1_line<?php //echo $z;?>" name="addr1_line<?php //echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>

    <div class="span">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">City<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="city<?php //echo $z;?>" name="city<?php //echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>

    </div>

    <div class="row">



    <div class="span">

    <div class="row-fluid">

    <div class="span4">

    <label class="control-label col-lg-2" for="text1">State<span class="text-sub">*</span></label>

   <input type="text" id="state<?php //echo $z;?>" name="state<?php //echo $z;?>" placeholder="" class="form-control">

    </div>



    </div>

    </div>

    <div class="span">

    <div class="row-fluid">

    <div class="span4">

    <label class="control-label col-lg-2" for="text1">Country<span class="text-sub">*</span></label>

   <input type="text" id="country<?php //echo $z;?>" name="country<?php //echo $z;?>" placeholder="" class="form-control">

    </div>



    </div>

    </div>

    </div>

    <div class="row">

    <div class="span">

    <div class="form-group">

                    <label class="control-label col-lg-2" for="text1">Phone Number<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" class="form-control span"  placeholder="" name="phone1_line<?php //echo $z;?>" id="phone1_line<?php //echo $z;?>" maxlength="10">
*/ ?>
                        <?php /*?> <input type="text" class="form-control span1" onKeyup=" if($(this).val().length > 3){ $('#phone3_line<?php echo $z;?>').focus();}" placeholder="" id="phone2_line<?php echo $z;?>" maxlength="4">

                          <input type="text" class="form-control span1" placeholder="" id="phone3_line<?php echo $z;?>" maxlength="4">
<?php */?>
        <?php /*

                    </div>

                </div>

    </div>

    <div class="span">



                    <label class="control-label col-lg-2" for="text1">Zip Code<span class="text-sub">*</span></label>

                    <div class="col-lg-8">

<input type="text" class="form-control span" placeholder="" id="zipcode<?php //echo $z;?>" name="zipcode<?php //echo $z;?>">

                    </div>


    </div>

        <div class="span">



                    <label class="control-label col-lg-2" for="text1">Email<span class="text-sub">*</span></label>

                    <div class="col-lg-8">

<input type="text" class="form-control span" placeholder=""  required id="email<?php //echo $z;?>" name="email<?php //echo $z;?>" value="">

                    </div>


    </div>

    </div>

    </div>
*/ ?>
    <?php $z++;
	} } ?>

    <?php   if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){
				$max=count($_SESSION['deal_cart']);
				$overall_total_price='';
				$overall_shipping_price='';
				$overall_tax_price='';

				for($i=0;$i<$max;$i++){
					$pid=$_SESSION['deal_cart'][$i]['productid'];
					$q=$_SESSION['deal_cart'][$i]['qty'];
    ?>
    <?php
    /*
    <div style="border: 1px solid #202020;
    padding: 14px;" id="shipping_addr_div<?php echo $pid;?>"  <?php if($z > 1){ ?> class="shipping_addr_class" style="display:block;"<?php } ?>>

        <h5 class="panel-deal">DEAL SHIPPING ADDRESS <?php echo $z;?></h5>



       <label class="control-label col-lg-8" for="text1">

       Load Shipping Address For The Profile Details

       &nbsp;<input type="radio" name="shipping_addr<?php echo $z;?>" id="shipping_addr_1rad<?php echo $z;?>" onClick="shipping_addr_func(<?php echo $z;?>,1)" value="yes" style="margin-top:-2px;"> Yes

       &nbsp;<input type="radio" name="shipping_addr<?php echo $z;?>" id="shipping_addr_2rad<?php echo $z;?>" onClick="shipping_addr_func(<?php echo $z;?>,0)" value="no"  style="margin-top:-2px;" checked="true"> No

       <span class="text-sub"></span></label>

    <legend></legend>

    <div class="row">

    <div class="span3 width-deal">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">First Name<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="fname<?php echo $z;?>" name="fname<?php echo $z;?>" placeholder="" class="form-control">

                         <input type="hidden" id="load_ship<?php echo $z;?>" name="load_ship<?php echo $z;?>" value="0">

                    </div>

                </div>

    </div>

    <div class="span3 width-deal">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Last Name<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="lname<?php echo $z;?>" name="lname<?php echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>

    </div>

    <div class="row">

    <div class="span3 width-deal">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Address Line1<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="addr_line<?php echo $z;?>" name="addr_line<?php echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>

    <div class="span3 width-deal">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Address Line2<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="addr1_line<?php echo $z;?>" name="addr1_line<?php echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>

    </div>

    <div class="row">

    <div class="span3 width-deal">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">City<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" id="city<?php echo $z;?>" name="city<?php echo $z;?>" placeholder="" class="form-control">

                    </div>

                </div>

    </div>

    <div class="span3 width-deal">

    <div class="row-fluid">

    <div class="span4">

    <label class="control-label col-lg-2" for="text1">State<span class="text-sub">*</span></label>

   <input type="text" id="state<?php echo $z;?>" name="state<?php echo $z;?>" placeholder="" class="form-control">

    </div>

    <div class="span4">





    </div>

    </div>

    </div>

    </div>

    <div class="row">

    <div class="span3 width-deal">

    <div class="form-group">

                    <label class="control-label col-lg-2" for="text1">Phone Number<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" class="form-control"  placeholder="" name="phone1_line<?php echo $z;?>" id="phone1_line<?php echo $z;?>" maxlength="10">
*/ ?>
                         <?php /*?><input type="text" class="form-control span1" onKeyup=" if($(this).val().length > 3){ $('#phone3_line<?php echo $z;?>').focus();}" placeholder="" id="phone2_line<?php echo $z;?>" maxlength="4">

                          <input type="text" class="form-control span1" placeholder="" id="phone3_line<?php echo $z;?>" maxlength="4"><?php */ ?>

             <?php /*

                    </div>

                </div>

    </div>

    <div class="span3 width-deal">



                    <label class="control-label col-lg-2" for="text1">Zip Code<span class="text-sub">*</span></label>



<input type="text" class="form-control" placeholder="" id="zipcode<?php echo $z;?>" name="zipcode<?php echo $z;?>">




    </div>

        <div class="span3 width-deal">



                    <label class="control-label col-lg-2" for="text1">Email<span class="text-sub">*</span></label>



<input type="text" class="form-control" placeholder="example@gmail.com" required id="email<?php echo $z;?>" name="email<?php echo $z;?>" value="123">




    </div>

    </div>

    </div>
<?php */ ?>
    <?php $z++;} } ?>
    <?php } if($count > 1) { ?>
        <div class="row">
        <div class="span5">

    <!--<div class="form-group">

                    <label class="control-label col-lg-8" for="text1">

                                             Same Shipping Address For The Other Products

                                     &nbsp;<input type="radio" name="shipping_addr" id="shipping_addr_rad1" value="yes" checked=""style="margin-top:-2px;"> Yes

                                              &nbsp;<input type="radio" name="shipping_addr" id="shipping_addr_rad2" value="no" checked="" style="margin-top:-2px;"> No



                    <span class="text-sub"></span></label>

                    <div class="col-lg-2">

                    </div>

                </div>-->

    </div>

    <div class="span3 width-deal">
        <div class="form-group">
            <label class="control-label col-lg-8" for="text1">
            <span class="text-sub"></span></label>

            <div class="col-lg-2">
            </div>
        </div>
    </div>
</div>

<?php } ?>

    <?php

        foreach($shipping_addr_details as $ship_addr_det){}
        if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){	 $item_count_header1 = count($_SESSION['cart']); } else { $item_count_header1 = 0; }
        if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){	 $item_count_header2 = count($_SESSION['deal_cart']); } else { $item_count_header2 = 0; }

        $count = $item_count_header1 + $item_count_header2;

        if($count !=0) {
	?>

            <input type="hidden" name="count_pid" id="count_pid" value="<?php echo $count; ?>" />
    <?php  if($shipping_addr_details)
        {
    ?>
            <input type="hidden" name="ship_name" id="ship_name" value="<?php echo $ship_addr_det->ship_name; ?>" />
            <input type="hidden" name="ship_address1" id="ship_address1" value="<?php echo $ship_addr_det->ship_address1; ?>" />
            <input type="hidden" name="ship_address2" id="ship_address2" value="<?php echo $ship_addr_det->ship_address2; ?>" />
            <input type="hidden" name="ship_city" id="ship_city" value="<?php echo $ship_addr_det->ci_code; ?>" />
			<input type="hidden" name="ship_district" id="ship_district" value="<?php echo $ship_addr_det->dis_id; ?>" />
			<input type="hidden" name="ship_subdistrict" id="ship_subdistrict" value="<?php echo $ship_addr_det->sdis_id; ?>" />
			<input type="hidden" name="ship_city_label" id="ship_city_label" value="<?php echo $ship_addr_det->ci_name; ?>" />
			<input type="hidden" name="ship_city_id" id="ship_city_id" value="<?php echo $ship_addr_det->ci_id; ?>" />
			<input type="hidden" name="ship_district_label" id="ship_district_label" value="<?php echo $ship_addr_det->dis_name; ?>" />
			<input type="hidden" name="ship_subdistrict_label" id="ship_subdistrict_label" value="<?php echo $ship_addr_det->sdis_name; ?>" />
            <input type="hidden" name="ship_state" id="ship_state" value="<?php echo $ship_addr_det->ship_state; ?>" />
            <input type="hidden" name="ship_postalcode" id="ship_postalcode" value="<?php echo $ship_addr_det->ship_postalcode; ?>" />
            <input type="hidden" name="ship_phone" id="ship_phone" value="<?php echo $ship_addr_det->ship_phone; ?>" />
            <input type="hidden" name="ship_email" id="ship_email" value="<?php echo $ship_addr_det->ship_email; ?>" />
            <input type="hidden" name="ship_country" id="ship_country" value="<?php echo $ship_addr_det->co_id; ?>" />
	<?php
        }
    ?>
	        <!-- <label for="text1" class="control-label col-lg-8" style="margin-top:20px;"> -->

            <!-- Select Payment Method -->
            <!-- &nbsp; -->

<!-- Toto 20170207 additional payment method : Doku -->
            <!-- <input type="radio" checked value="2" id="paypal_radio" name="select_payment_type"style="margin-top:-2px;"> Doku -->
            <!-- <input type="radio" value="1" id="paypal_radio" name="select_payment_type"style="margin-top:-2px;"> Paypal -->
            <!-- <input type="radio" value="4" id="paypal_radio" name="select_payment_type"style="margin-top:-2px;"> Columbia -->
            <!-- <input type="radio" value="5" id="paypal_radio" name="select_payment_type"style="margin-top:-2px;"> Transfer Bank -->
            <!-- <?php
            //foreach($general as $gs) {}
               // if($gs->gs_payment_status == 'COD')
                    { ?> &nbsp;<input type="radio"  value="0" id="cod_radio" name="select_payment_type" style="margin-top:-2px;"> Cash On Delivery <?php
                } ?>

            <span class="text-sub"></span></label>

            <div class="col-lg-2">
            </div>
 -->
     <!--<div id="total_select_pay_iformation">

    <h5>PAYMENT INFORMATION</h5>

    <legend></legend>

    <div class="row">

    <div class="span3">

    <div class="form-group">

                    <label class="control-label col-lg-2" for="text1">Name on Card<span class="text-sub">*</span></label>



                    <div class="col-lg-8">

                        <input type="text" class="form-control" placeholder="" id="text1">

                    </div>

                </div>

    </div>

    <div class="span3">

    <div class="form-group">

                    <label class="control-label col-lg-2" for="text1">Card No<span class="text-sub">*</span> <img src="themes/images/b3.png"></label>



                    <div class="col-lg-8">

                        <input type="text" class="form-control" placeholder="" id="text1">

                  </div>

                </div>

    </div>

    </div>



    <div class="row">

    <div class="span3">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2">Expiration<span class="text-sub">*</span></label>



                   <div class="col-lg-8">

                       <select class="form-control span1">

            <option>Default</option>

            <option>English</option>

            <option>3</option>

            <option>4</option>

            <option>5</option>

        </select>

                        <select class="form-control span1">

            <option>Default</option>

            <option>English</option>

            <option>3</option>

            <option>4</option>

            <option>5</option>

        </select>



                    </div>

                </div>



    </div>

    <div class="span3">

    <div class="form-group">

                   <br>



                    <div class="col-lg-8">

                       <input type="checkbox" checked="" value="option1" id="optionsRadios1" name="optionsRadios">&nbsp;Save card

                    </div>

                </div>

    </div>

    </div>

    </div>-->



    <!-- <div class="row hide" id="cod_div">
        <div class="span3">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2"><img src="themes/images/cod_delivery.png" width="150" height="81">

                    <span class="text-sub"></span></label>

                    <div class="col-lg-8">



                    </div>

                </div>

    </div>

    <div class="span3">

    <div class="form-group">

                    <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>



                    <div class="col-lg-8">



                    </div>

                </div>

    </div>

    </div> -->



    <!-- <div class="row" id="paypal_div">
        <div class="span3">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2"><img src="themes/images/bn2.png" width="150" height="100">
                <span class="text-sub"></span></label>

                <div class="col-lg-8">
                </div>

            </div>
        </div>
        <div class="span3">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2"><span class="text-sub"></span></label>
                    <div class="col-lg-8">
                    </div>
            </div>
        </div>
    </div> -->

    <legend></legend>

    <div class="row">
        <div class="span3">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-8">
                <span class="text-sub"></span></label>

                <div class="col-lg-2">
                </div>
            </div>
        </div>
    </div>

<?php } ?>

</div>









    <?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){	 $item_count_header1 = count($_SESSION['cart']); } else { $item_count_header1 = 0; }

			if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){	 $item_count_header2 = count($_SESSION['deal_cart']); } else { $item_count_header2 = 0; }

			 $count = $item_count_header1 + $item_count_header2;



	if($count != 0) {

	?>



     <div class="span6">


<div style="border: 1px solid #202020;
    padding: 14px;" id="shipping_addr_div">
     <h5 class="panel-deal">ORDER SUMMARY [ <small style="color:white !important"><?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){	 $item_count_header1 = count($_SESSION['cart']); } else { $item_count_header1 = 0; }

			if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){	 $item_count_header2 = count($_SESSION['deal_cart']); } else { $item_count_header2 = 0; }

			 $item_count_header = $item_count_header1 + $item_count_header2;

			 if($item_count_header != 0) { echo $item_count_header; } else { echo 0; } ?> Item(s) </small>]</h5>

     <legend></legend>



     <?php
        $allItems = [4,5,7,2];
		$z = 1;
		$overall_total_price='';
		$overall_shipping_price='';
        $overall_tax_price='';
		if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
            $max=count($_SESSION['cart']);

			for($i=0;$i<$max;$i++){

                $pid=$_SESSION['cart'][$i]['productid'];
                $q=$_SESSION['cart'][$i]['qty'];
                $size=$size_result[$_SESSION['cart'][$i]['size']];
                $color=$color_result[$_SESSION['cart'][$i]['color']];
                // if(isset($_SESSION['cart'][$i]['flashPromoId'])){
				// 		$flashPromoId = $_SESSION['cart'][$i]['flashPromoId'];
				// 	}
                $pname="Have to get";
                //dd($result_cart);
					foreach($result_cart[$pid] as $session_cart_result) {
                      // dd($session_cart_result);
						$product_img=explode('/**/',$session_cart_result->pro_Img);
                        if(isset($session_cart_result->promop_schedp_id) || !empty($session_cart_result->promop_schedp_id)){
							// dd('test4');
                            $item_total_price = ($_SESSION['cart'][$i]['qty']) * ($session_cart_result->promop_discount_price);
                            $overall_total_price +=$session_cart_result->promop_discount_price * $q;
                        }else if(!empty($session_cart_result->wholesale_level1_min)){
							// dd('test3');
                                if($q < $session_cart_result->wholesale_level1_min){
									if($session_cart_result->pro_disprice != 0){
										//echo "Haha";
										$item_total_price = $q * ($session_cart_result->pro_disprice);
										$overall_total_price +=$session_cart_result->pro_disprice * $q;
										}else{
										$item_total_price = $q * ($session_cart_result->pro_price);
										$overall_total_price +=$session_cart_result->pro_price * $q;
									}
								}else if($session_cart_result->wholesale_level1_min>1){
                                if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q):
                                    $item_total_price = $q * ($session_cart_result->wholesale_level5_price);
                                    $overall_total_price += $q * $session_cart_result->wholesale_level5_price;
                                elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q):
                                    $item_total_price = $q * ($session_cart_result->wholesale_level4_price);
                                    $overall_total_price += $q * $session_cart_result->wholesale_level4_price;
                                elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q):
                                    $item_total_price = $q * ($session_cart_result->wholesale_level3_price);
                                    $overall_total_price += $q * $session_cart_result->wholesale_level3_price;
                                elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q):
                                    $item_total_price = $q * ($session_cart_result->wholesale_level2_price);
                                    $overall_total_price += $q * $session_cart_result->wholesale_level2_price;
                                elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q):
                                    $item_total_price = $q * ($session_cart_result->wholesale_level1_price);
                                    $overall_total_price += $q * $session_cart_result->wholesale_level1_price;
                                endif;
                            }
                        }
                        else if($session_cart_result->pro_disprice != 0){
                            $item_total_price = $q * ($session_cart_result->pro_disprice);
                            $overall_total_price +=$session_cart_result->pro_disprice * $q;
                        }else{
                            $item_total_price = $q * ($session_cart_result->pro_price);
                            $overall_total_price +=$session_cart_result->pro_price * $q;
                        }

						$overall_shipping_price +=$session_cart_result->pro_shippamt;
						$overall_tax_price = 10;
	   					$delivery_date[$z] = '+'.$session_cart_result->pro_delivery.'days';

    ?>

<h4>Product Details </h4>
    <legend></legend>

    <div class="row product_select" style="margin:0px auto" id="product_select_div<?php echo $pid;?>">
        <div class="span2 check-wid">
            <img src="<?php echo url('/assets/product/').'/'.$product_img[0]; ?>" alt="<?php echo $session_cart_result->pro_title; ?>" width="100" height="auto" style="padding:10px">
        </div>
        <!-- Id Barang -->
        <input type="hidden" name="pro_id_h{{$i}}" value="{{$pid}}">

        <div class="span3">
            <label><b><?php echo $session_cart_result->pro_title; ?></b></label>
            <legend></legend>

                <label><b>Harga &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>
                <?php
                if(isset($session_cart_result->promop_schedp_id) || !empty($session_cart_result->promop_schedp_id)){
                ?>
                <span class="pull-right"><b><?php echo $get_cur; ?><?php echo number_format($session_cart_result->promop_discount_price);?></b></span>
                <?php
                }else if(!empty($session_cart_result->wholesale_level1_min)){
                    if($q < $session_cart_result->wholesale_level1_min){
						if($session_cart_result->pro_disprice != 0){ //dd($session_cart_result); //echo "gue disini";?>
                            <span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_disprice);?></b></span>
						<?php }else{ ?>
							<span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_price);?></b></span>
						<?php 	} ?>
				<?php }else if ($session_cart_result->wholesale_level1_min>1) {
						?>
						<?php if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q): ?>
							<span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level5_price);?></b></span>
						<?php elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q): ?>
							<span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level4_price);?></b></span>
						<?php elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q): ?>
							<span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level3_price);?></b></span>
						<?php elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q): ?>
							<span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level2_price);?></b></span>
						<?php elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q): ?>
							<span class="pull-right"><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level1_price);?></b></span>
                        <?php endif; ?>
						<?php
                    }
                }
                else if($session_cart_result->pro_disprice != 0){
                        ?>
                            <span class="pull-right"><b><?php echo $get_cur; ?><?php echo number_format($session_cart_result->pro_disprice);?></b></span>
                        <?php
                    }else {
                        ?>
                             <span class="pull-right"><b><?php echo $get_cur; ?><?php echo number_format($session_cart_result->pro_price);?></b></span>
                        <?php
                    }
                ?>
                </label>

                <label><b>Quantity &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>
                <span class="pull-right"><b><?php echo $q;?></b></span>
                </label>
				<label>
					<b>Shipping &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>
                    <span class="pull-right"><b id="shipping_price_per_item{{$i}}"></b></span>
                </label>
                <?php
                //dd($session_cart_result);
                    if(!empty($session_cart_result->have_warranty) || !isset($session_cart_result->have_warranty)  || $session_cart_result->have_warranty == 1)
                    {
                        $tot_warranty = 0;

                        if($session_cart_result->pro_disprice == 0 || $session_cart_result->pro_disprice =="")
                        {
                            $tot_warranty = floatval($q)*floatval($session_cart_result->pro_price)* floatval($get_setting_extended[0]->besaran_warranty/100);
                        }
                        else
                        {
                            $tot_warranty = floatval($q)*floatval($session_cart_result->pro_disprice)* floatval($get_setting_extended[0]->besaran_warranty/100);
                        }

                        ?>
                    <label>
                            <b>Ext. Warranty &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                            <input type="checkbox" name="extended_warranty_cb{{$i}}" id="extended_warranty_cb{{$i}}" value="0">&nbsp;<?php echo $get_setting_extended[0]->besaran_warranty?>%</b></b>
                            <span class="pull-right" id="extended_warranty_span{{$i}}"><b><?php echo $get_cur; ?><?php echo number_format($tot_warranty);?></span>
                            <input type="hidden" name="extended_warranty_span_hidden{{$i}}" id="extended_warranty_span_hidden{{$i}}" value="{{$tot_warranty}}">
                    </label>

                    </b>
                    </span>
                    </label>
                        <?php
                    }
					if($session_cart_result->jumlah_cicilan_3bulan > 0 || $session_cart_result->jumlah_cicilan_6bulan > 0 || $session_cart_result->jumlah_cicilan_12bulan > 0)
                    {
                ?>
				<label>
						<b>Gunakan Cicilan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
						<input type="checkbox" id="cicilan{{$i+1}}" class="cicilan" value="0">Yes</b>
				</label>
				<?php
				}
				?>
				<input type="hidden" name="cicilan[{{$i+1}}]" id="cicilan2{{$i+1}}" value="0">
                <legend></legend>
                <label style="margin-top: -15px;">
                    <b>Total &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>
                    <span class="pull-right"><b id="total_price_span{{$i}}"><?php echo $get_cur; ?><?php echo number_format($item_total_price);?></b></span>
					<input type="hidden" name="total_price_input_hidden{{$i}}" value="{{$item_total_price}}" id="total_price_input_hidden{{$i}}">
                </label>
                <label style="display:none;"><b>Size &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>
					<span class="pull-right"><?php echo $size;?></span>
                </label>
                <label style="display:none;"><b>Color &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>
					<span class="pull-right"><?php echo $color;?></span>
                </label>
                <!-- <label><b>Tax : </b>
                    <span class="pull-right"><b>10 %</b></span>
                </label> -->
        </div>
    </div>
<?php //dd($session_cart_result);?>
    <br>
    <?php if(isset($session_cart_result->promop_schedp_id) || !empty($session_cart_result->promop_schedp_id)){?>
        <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="<?php echo $session_cart_result->promop_schedp_id; ?>" />
        <input type="hidden" name="item_pro_id[<?php echo $z;?>]" value="<?php echo $session_cart_result->promop_pro_id; ?>" />

        <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ($session_cart_result->promop_saving_price * $q); ?>" />
        <input type="hidden" name="item_dis_price[<?php echo $z;?>]" value="<?php echo ($session_cart_result->promop_discount_price * $q); ?>" />
    <?php }else if(!empty($session_cart_result->wholesale_level1_min)){
                    if($q < $session_cart_result->wholesale_level1_min){
						if($session_cart_result->pro_disprice != 0){ //dd($session_cart_result); //echo "gue disini";?>
                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->pro_disprice)) * $q); ?>" />
						<?php }else{ ?>
                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
							<input type="hidden" name="item_diskon[<?php echo $z;?>]" value="0" />
						<?php 	} ?>
				<?php }else {
						    if($session_cart_result->wholesale_level1_min > 1){?>
                            <?php if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q): ?>
                                <?php if($session_cart_result->pro_disprice != 0){?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_disprice) - ($session_cart_result->wholesale_level5_price)) * $q); ?>" />
                                <?php }else{ ?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->wholesale_level5_price)) * $q); ?>" />
                                <?php } ?>
							<?php elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q): ?>
								<?php if($session_cart_result->pro_disprice != 0){?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_disprice) - ($session_cart_result->wholesale_level4_price)) * $q); ?>" />
                                <?php }else{ ?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->wholesale_level4_price)) * $q); ?>" />
                                <?php } ?>
							<?php elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q): ?>
								<?php if($session_cart_result->pro_disprice != 0){?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_disprice) - ($session_cart_result->wholesale_level3_price)) * $q); ?>" />
                                <?php }else{ ?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->wholesale_level3_price)) * $q); ?>" />
                                <?php } ?>
							<?php elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q): ?>
								<?php if($session_cart_result->pro_disprice != 0){?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_disprice) - ($session_cart_result->wholesale_level2_price)) * $q); ?>" />
                                <?php }else{ ?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->wholesale_level2_price)) * $q); ?>" />
                                <?php } ?>
							<?php elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q): ?>
								<?php if($session_cart_result->pro_disprice != 0){?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_disprice) - ($session_cart_result->wholesale_level1_price)) * $q); ?>" />
                                <?php }else{ ?>
                                            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
                                            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->wholesale_level1_price)) * $q); ?>" />
                                <?php } ?>
							<?php endif; ?>
						<?php } ?>
                <?php } ?>
    <?php }else if($session_cart_result->pro_disprice != 0){?>
            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
             <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ((($session_cart_result->pro_price) - ($session_cart_result->pro_disprice)) * $q); ?>" />
    <?php }else{ ?>
            <input type="hidden" name="item_schedp_id[<?php echo $z;?>]" value="0" />
            <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="0" />
    <?php } ?>

    <input type="hidden" name="item_warranty_val[<?php echo $z;?>]" id="item_warranty_val{{$z}}" value="0">

    <input type="hidden" name="item_name[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_title; ?>" />

    <input type="hidden" name="item_type[<?php echo $z;?>]" value="1" />

	<input type="hidden" name="item_code[<?php echo $z;?>]" value="<?php echo $pid; ?>" />

	<input type="hidden" name="item_desc[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_desc; ?>" />

	<input type="hidden" name="item_qty[<?php echo $z;?>]" value="<?php echo $q; ?>" />

    <input type="hidden" name="item_color[<?php echo $z;?>]" value="<?php echo $_SESSION['cart'][$i]['color']; ?>" />

    <input type="hidden" name="item_size[<?php echo $z;?>]" value="<?php echo $_SESSION['cart'][$i]['size']; ?>" />

    <input type="hidden" name="item_color_name[<?php echo $z;?>]" value="<?php echo $color; ?>" />

    <input type="hidden" name="item_size_name[<?php echo $z;?>]" value="<?php echo $size; ?>" />

    <input type="hidden" name="item_poin[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_poin; ?>" />

    <?php
   //dd($session_cart_result);
       // if(!isset($session_cart_result->promop_schedp_id) || empty($session_cart_result->promop_schedp_id)){
            if($session_cart_result->pro_disprice != 0)
            {
                ?>
                    <input type="hidden" name="item_price[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_price; ?>" />
                     <?php if(isset($session_cart_result->promop_schedp_id) || !empty($session_cart_result->promop_schedp_id)){?>
                        <!-- <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ($session_cart_result->promop_saving_price * $q); ?>" />
                        <input type="hidden" name="item_dis_price[<?php echo $z;?>]" value="<?php echo ($session_cart_result->promop_discount_price * $q); ?>" /> -->
                    <?php } ?>
                <?php
            }
            else
            {
                ?>
                    <input type="hidden" name="item_price[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_price; ?>" />
                     <?php if(isset($session_cart_result->promop_schedp_id) || !empty($session_cart_result->promop_schedp_id)){?>
                        <!-- <input type="hidden" name="item_diskon[<?php echo $z;?>]" value="<?php echo ($session_cart_result->promop_saving_price * $q); ?>" />
                        <input type="hidden" name="item_dis_price[<?php echo $z;?>]" value="<?php echo ($session_cart_result->promop_discount_price * $q); ?>" /> -->
                    <?php } ?>
                <?php

            }


        //dd($session_cart_result);
    ?>

    <input type="hidden" name="item_tax[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_inctax; ?>" />

    <input type="hidden" name="item_shipping[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_shippamt; ?>" />

    <input type="hidden" name="item_totprice[<?php echo $z;?>]" value="<?php echo $item_total_price; ?>" />

	<input type="hidden" name="item_merchant[<?php echo $z;?>]" value="<?php echo $session_cart_result->pro_mr_id; ?>" />


   <?php $no_item_found = 1; $z++;} }
        }
    ?>


    <!-- barang deal tadinya -->
    <?php

        $overall_deal_total_price='';
        $overall_deal_shipping_price='';
        $overall_deal_tax_price='';

        if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){

        $max=count($_SESSION['deal_cart']);

		for($i=0;$i<$max;$i++){

		    $pid=$_SESSION['deal_cart'][$i]['productid'];
            $q=$_SESSION['deal_cart'][$i]['qty'];
            $pname="Have to get";

			foreach($result_cart_deal[$pid] as $session_deal_cart_result) {
				// dd($session_deal_cart_result);
			$product_img=explode('/**/',$session_deal_cart_result->pro_Img);
			$item_total_price = ($_SESSION['deal_cart'][$i]['qty']) * ($session_deal_cart_result->promop_discount_price);
            $overall_deal_total_price +=$session_deal_cart_result->promop_discount_price * $q;
            $overall_deal_shipping_price +=0;
            $overall_deal_tax_price = 10;

	?>

<h4>Deal Details </h4>
     <div class="row">

    <div class="span3">

    <div class="form-group">

                    <label class="control-label col-lg-2" for="text1">Shipment <?php echo $z;?> <span class="text-sub">*</span></label>

                </div>

    </div>

    </div>

    <div class="row">
        <div class="span3">

    <!--<div class="form-group">



                    <label class="control-label col-lg-2" for="text1">Estimated Delivery<span class="text-sub"> <?php // echo date('D,M,Y', strtotime($delivery_date[$z]));?></span></label>

                </div>-->

    </div>

    </div>



    <legend></legend>

    <div class="row product_select" style="margin:0px auto" id="product_select_div<?php echo $pid;?>">

    <div class="span2">

    <img src="<?php echo url('/assets/product/').'/'.$product_img[0]; ?>" alt="<?php echo $session_deal_cart_result->pro_title; ?>" width="100" height="auto" style="padding:10px"></div>

    <div class="span3">

    <label><?php echo $session_deal_cart_result->pro_title; ?></label>

    <?php /*?><label>Size:<?php //echo $size;?> - Color: <?php //echo $color;?> <span class="pull-right">Qty:<?php //echo $q;?></span></label><?php */?>

    <label>Qty:<?php echo $q;?></label>

    <?php /*?><label>Shipping:$<?php echo $session_cart_result->pro_shippamt;?> <span class="pull-right">Tax:$<?php echo $session_cart_result->pro_inctax.'.00';?></span></label><?php */?>

    <label>Price: <?php echo $get_cur; ?><?php echo $session_deal_cart_result->promop_discount_price;?><br><span class="">Total: <?php echo $get_cur; ?><?php echo $item_total_price.'.00';?></span></label>



    </div>

    </div>

    <br>

    <input type="hidden" name="item_warranty_val[<?php echo $z;?>]" id="item_warranty_val[<?php echo $z;?>]" value="">

    <input type="hidden" name="kode_voucher" id="kode_voucher">

    <input type="hidden" name="item_name[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->pro_title; ?>" />

    <input type="hidden" name="item_type[<?php echo $z;?>]" value="2" />

	<input type="hidden" name="item_code[<?php echo $z;?>]" value="<?php echo $pid; ?>" />

	<input type="hidden" name="item_desc[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->pro_desc; ?>" />

	<input type="hidden" name="item_qty[<?php echo $z;?>]" value="<?php echo $q; ?>" />

    <input type="hidden" name="item_color[<?php echo $z;?>]" value="" />

    <input type="hidden" name="item_size[<?php echo $z;?>]" value="" />

    <input type="hidden" name="item_color_name[<?php echo $z;?>]" value="" />

    <input type="hidden" name="item_size_name[<?php echo $z;?>]" value="" />

    <input type="hidden" name="item_price[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->promop_discount_price; ?>" />

   <input type="hidden" name="item_tax[<?php echo $z;?>]" value="" />

    <input type="hidden" name="item_shipping[<?php echo $z;?>]" value="" />

    <input type="hidden" name="item_totprice[<?php echo $z;?>]" value="<?php echo $item_total_price; ?>" />

    <input type="hidden" name="item_merchant[<?php echo $z;?>]" value="<?php echo $session_deal_cart_result->pro_mr_id; ?>" />



   <?php $no_item_found = 1; $z++;}
                }
            }
    ?>
    <legend></legend>
    <div class="row center">
        <div class="span3 width-deal">
            <div class="form-group">
                <label class="control-label col-lg-2" for="text1"><b>Total Belanja:</b></label>

                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
            <div class="form-group">
                <label class="control-label col-lg-2" for="text1"><b id="total_belanja">

                <?php echo $get_cur; ?><?php echo number_format($overall_total_price + $overall_deal_total_price);?>


                </b></label>
                <input type="hidden" name="subtotal" id="subtotal" value="<?php echo ($overall_total_price + $overall_deal_total_price); ?>" />
                    <div class="col-lg-8">
                    </div>
            </div>
        </div>
    </div>
    <div class="row" style="display:none;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2"><b>Pajak</b><span class="text-sub">*</span></label>

                <div class="col-lg-8">
                </div>
            </div>
        </div>
        <div class="span3 width-deal">
            <div class="form-group">
                <?php
                    $overall_per_pro_tax = $overall_total_price *($overall_tax_price/100);
                    $overall_per_deal_tax = $overall_deal_total_price *($overall_deal_tax_price/100);
                ?>

                <label for="text1" class="control-label col-lg-2"><b>10 %</b></label>

                <input type="hidden" name="tax_price" id="persen_pajak_h" value="10" />

                <div class="col-lg-8">
                </div>
            </div>
            <legend></legend>
        </div>
    </div>

    <div class="row" style="display: none;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2 me_color"><b>Order Total Belanja:</b><span class="text-sub">*</span></label>

                <div class="col-lg-8">
                </div>
            </div>
        </div>

    <div class="span3 width-deal">
        <div class="form-group">
            <?php
                $overall_pro_price = $overall_total_price ;//+ ($overall_total_price *($overall_tax_price/100));
                $overall_deal_price = $overall_deal_total_price ;//+ ($overall_deal_total_price *($overall_deal_tax_price/100));
				$total_pajak = ($overall_pro_price+$overall_deal_price)*0.1;
			?>

                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2 me_color" id="order_total_h" value="<?php  echo round($overall_pro_price+$overall_deal_price);?>"><b><?php echo $get_cur; ?>  <?php  echo number_format(round($overall_pro_price+$overall_deal_price));?></b></label>

                <input type="hidden" name="total_price" id="total_price" value="<?php  echo round($overall_pro_price+$overall_deal_price);?>" />

                <div class="col-lg-8">
                </div>
        </div>
    </div>
    </div>

    <?php } ?>

     </div>

     <!-- todo///////////////////////////////// Untuk Pengiriman dan Pembayaran ////////////////////////////////////////////////////////////////-->


<div style="border: 1px solid #202020; padding: 14px;margin-bottom:25px; margin-top: 25px;" id="shipping_popbox">

    <h5 class="panel-deal">Pengiriman dan Pembayaran<?php //echo $z;?></h5>

    <div class="row" style="margin-top: 25px;">
    <!--pilih pengiriman-->

        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" class="control-label col-lg-2" for="text1">Pilih Pengiriman  :<span class="text-sub">*</span></label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

    <div class="span3 width-deal">
        <div class="form-group">
            <select required name="pengiriman_oke" id="pengiriman_oke">
                <option value="kocak">Pilih Pengiriman</option>
                <?php
                        foreach($list_postal_sevices as $list_postal_sevices2){
                    ?>
                        <option value='<?php echo $list_postal_sevices2 -> ps_code;?>'><?php echo $list_postal_sevices2 ->ps_code;?></option>
                    <?php
                        }
                    ?>
            </select>
                <div class="col-lg-8">
                </div>
        </div>
    </div>

    </div>

    <div class="row" style="margin-top:5px;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2">Biaya Pengiriman<span class="text-sub">*</span></label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
            <div class="form-group">
                <label doFormatMoney2(isNaN($this) ? 0 : res,3, '.', ',') style="font-weight: bold;" for="text1" id="shipping_price_label" name="shipping_price_label" class="control-label col-lg-2"></label> <?php //(yang lama)echo ($overall_shipping_price + $overall_deal_shipping_price).'.00';?>
                <input type="hidden" name="shipping_price" value="<?php echo ($overall_shipping_price + $overall_deal_shipping_price); ?>" />
                <input type="hidden" name="shipping_price_label_hidden" id="shipping_price_label_hidden">
                <div class="col-lg-8">
                </div>
            </div>
        </div>
    </div>

    <!-- DIV ini muncul setelah ambil langsung -->
    <div id="setelah_action_ambil_langsung" style="display: none;margin-top: 10px;">

        <div class="row">
            <div class="span3 width-deal">
            </div>

            <div class="span3 width-deal">

                <div class="form-group">
                    <label for="text1" id="ambil_langsung_txt" class="control-label col-lg-2" style="font-weight: bold;">Anda memilih pengiriman dengan metode "Ambil Langsung"</label>
                    <div class="col-lg-8">
                    </div>
                </div>
                <legend ></legend>
            </div>
        </div>

        <div class="row">
            <div class="span3 width-deal">
            </div>

            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2" style="font-weight: bold;">Alamat Kantor & Gudang Kukuruyuk.com: Ruko Grand Mall Bekasi Blok D No 12 Jl. Jend Sudirman Bekasi, Jawa Barat Indonesia 17143</label>

                    <div class="col-lg-8">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End off hidden ambil langsung -->

    <legend ></legend>

    <!-- Diskon Div -->
    <div class="row" style="margin-top:5px; margin-bottom: 10px;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label for="text1" class="control-label col-lg-2" style="font-weight: bold;">Masukkan Kupon</label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
            <div class="form-group">
                <input type="text" name="input_kupon" id="input_kupon" />
                <div class="col-lg-8">
                <button type="button" id="button_kupon" class="btn btn-success btn-sm btn-grad" style="color:#fff">Kupon</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End of diskon div -->
    <!-- DIV ini muncul setelah kupon di eksekusi -->
    <div id="setelah_action_kupon" style="display: none;margin-top: 10px;">

        <!-- Judul Promo -->
        <div class="row">
            <div class="span3 width-deal">
            </div>

            <div class="span3 width-deal">

                <div class="form-group">
                    <label for="text1" id="judul_diskon_amt" class="control-label col-lg-2" style="font-weight: bold;"></label>
                    <!-- Hidden untuk diskon ammount -->
                    <input type="hidden" name="judul_diskon_amt_hidden" id="judul_diskon_amt_hidden">

                    <div class="col-lg-8">
                    </div>
                </div>
                <legend ></legend>
            </div>
        </div>

        <!-- Diskon Kupon -->
        <div class="row">
            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2" style="font-weight: bold;">Diskon Kupon</label>
                    <div class="col-lg-8">
                    </div>
                </div>
            </div>

            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" id="persen_kupon_angka" class="control-label col-lg-2" style="font-weight: bold;"></label>
                    <!-- Hidden untuk diskon ammount -->
                    <input type="hidden" name="persen_kupon_angka_hidden" id="persen_kupon_angka_hidden">

                    <div class="col-lg-8">
                    </div>
                </div>
            </div>
        </div>

        <!-- Total Kupon -->
        <div class="row">
            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2" style="font-weight: bold;">Total Diskon</label>
                    <div class="col-lg-8">
                    </div>
                </div>
            </div>

            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" id="total_diskon_kupon" class="control-label col-lg-2" style="font-weight: bold;"></label>

                    <!-- Hidden untuk diskon ammount -->
                    <input type="hidden" name="total_diskon_kupon_hidden" id="total_diskon_kupon_hidden">

                    <div class="col-lg-8">
                    </div>
                    <legend ></legend>
                </div>
            </div>
        </div>

        <!-- Total Belanja -->
        <div class="row" style="display:none;">
            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" class="control-label col-lg-2" style="font-weight: bold;">Total Setelah Diskon</label>
                    <div class="col-lg-8">
                    </div>
                </div>
            </div>

            <div class="span3 width-deal">
                <div class="form-group">
                    <label for="text1" id="total_belanja_kupon" class="control-label col-lg-2" style="font-weight: bold;"></label>

                     <!-- Hidden untuk diskon ammount -->
                    <input type="hidden" name="total_belanja_kupon_hidden" id="total_belanja_kupon_hidden">

                    <div class="col-lg-8">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End off hidden diskon -->

<legend></legend>
<div class="row" style="margin-top:5px;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2">Total Pembayaran</label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" id="total_belanja_ditambah_shipping" name="total_belanja_ditambah_shipping" class="control-label col-lg-2"><b><?php echo $get_cur; ?><?php  echo number_format(round($overall_pro_price+$overall_deal_price));?></b></label> <?php //(yang lama)echo ($overall_shipping_price + $overall_deal_shipping_price).'.00';?>

                <input type="hidden" name="shipping_price" value="<?php echo ($overall_shipping_price + $overall_deal_shipping_price); ?>" />
                <input type="hidden" name="total_belanja_ditambah_shipping_hidden" id="total_belanja_ditambah_shipping_hidden" value="<?php echo $overall_pro_price+$overall_deal_price; ?>">
                <div class="col-lg-8">
                </div>
            </div>
        </div>
    </div>
    <legend></legend>
    <div class="row" style="margin-top:5px;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2">Dapatkan Poin Sebanyak</label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" id="poin_yang_didapatkan" name="poin_yang_didapatkan" class="control-label col-lg-2"><b><?php  echo number_format(round($overall_pro_price+$overall_deal_price)/1000);?> Poin</b></label> <?php //(yang lama)echo ($overall_shipping_price + $overall_deal_shipping_price).'.00';?>

                <input type="hidden" name="poin_yang_didapatkan_hidden" id="poin_yang_didapatkan_hidden" value="<?php echo ($overall_pro_price+$overall_deal_price)/1000; ?>">
                <div class="col-lg-8">
                </div>
            </div>
        </div>
    </div>
    <legend></legend>

    <div class="row" style="margin-top:5px;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2">Gunakan Poin Kukuruyuk</label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" id="total_poin_cus" value="<?php echo $get_data_cus->total_poin; ?>" class="control-label col-lg-2"><b>(<?php echo number_format($get_data_cus->total_poin); ?>)</b></label>
                <input type="checkbox" id="chk_poin" name="chk_poin" value="0"> Yes

                <input type="hidden" name="total_poin_cus_hidden" id="total_poin_cus_hidden" value="<?php echo $get_data_cus->total_poin; ?>" />
                <div class="col-lg-2">

                </div>
            </div>
        </div>
    </div>

    <legend></legend>

<div class="row" style="margin-top:5px; margin-bottom: 15px;">
        <div class="span3 width-deal">
            <div class="form-group">
                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2">Pilih Metode Pembayaran<span class="text-sub">*</span></label>
                <div class="col-lg-8">
                </div>
            </div>
        </div>

        <div class="span3 width-deal">
        <div class="form-group">
            <select name="select_payment_type" id="paypal_radio">
                <?php
                    foreach ($get_metode_pembayaran as $key) {
                        ?>
                            <option value="<?php echo $key->kode_pay_detail;?>" nilai="{{$key->nama_pay_header}}" nilai2="{{$key->channel_pay_header}}"><?php echo $key->nama_pay_header;?></option>
                        <?php
                    }
                ?>

            </select>
			<input type="hidden" name="payment_channel" id="payment_channel" value="{{$get_metode_pembayaran[0]->channel_pay_header}}">
                <div class="col-lg-8">
                </div>
        </div>
    </div>
    </div>
    <legend></legend>
	<!-- <div class="row" style="margin-top:5px; margin-bottom: 15px;" id="payment_channel_doku">
	        <div class="span3 width-deal">
	            <div class="form-group">
	                <label style="font-weight: bold;" for="text1" class="control-label col-lg-2">Payment Channel<span class="text-sub">*</span></label>
	                <div class="col-lg-8">
	                </div>
	            </div>
	        </div>

	        <div class="span3 width-deal">
	        <div class="form-group">
				<select name="payment_channel_doku">
					<option value="04">Dokuwallet</option>
					<option value="15">Credit Card</option>
	                <option value="02">Mandiri Clickpay </option>
              	</select>
	                <div class="col-lg-8">
	                </div>
	        </div>
	    </div>
	    </div> -->

	<div class="row" style="margin-top:5px; margin-bottom: 15px;" id="">
		<div class="span3 width-deal">
			<div class="form-group">
				<label style="font-weight: bold;" for="text1" class="control-label col-lg-2"></label>
				<div class="col-lg-8">
				</div>
			</div>
		</div>

		<div class="span3 width-deal">
		<div class="form-group">
			<?php if($get_start_pm_img!=null){
				$src = "assets/paymentmethodimage/{$get_start_pm_img->pm_image}";
			}
			else
			{
				$src = "";
			} ?>
			<img id="paymentmethod_image" src="{{$src}}" width="50" height="50" <?php if($src=='') {?> style="display:none;"<?php } ?>>
				<div class="col-lg-8">
				</div>
		</div>
	   </div>
	</div>
	<div class="row">
		<div class="span12 checkbox">
			<label class="control-label"><input type="checkbox" name="term" value="1" required>By clicking, you agree to <a href="{{url('termsandconditons')}}" style="text-decoration:underline;color:blue;" target="_blank">Terms And Conditions</a></label>
		</div>
	</div>
    <div class="form-group">
        <br>
        <label class="control-label col-lg-3" for="pass1"><span class="text-sub" id="error_div" style="color:red;"></span></label>

        <div class="col-lg-8">
            <?php
                if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){   $item_count_header1 = count($_SESSION['cart']); } else { $item_count_header1 = 0; }
                if(isset($_SESSION['deal_cart'])&& !empty($_SESSION['deal_cart'])){  $item_count_header2 = count($_SESSION['deal_cart']); } else { $item_count_header2 = 0; }
                $count = $item_count_header1 + $item_count_header2;
                if($count != 0) {
            ?>
                <input type="hidden" name="count_session" id="count_session" value="<?php echo $count; ?>" />
                <button type="submit" id="place_order_submit" class="btn btn-warning btn-sm btn-grad" style="color:#fff" formtarget="_self">Place Order</button>
             <?php
                }
             ?>
        </div>
    </div>

</div> <!-- todo -->

<!-- todo///////////////////////////////// Untuk Pengiriman dan Pembayaran ////////////////////////////////////////////////////////////////-->
    </div>
     </form>

      <?php if(empty($item_count_header)){ ?>

     <div class="span6">

     <h5>ORDER SUMMARY [ <small ><?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){	 $item_count_header1 = count($_SESSION['cart']); } else { $item_count_header1 = 0; }

			if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){	 $item_count_header2 = count($_SESSION['deal_cart']); } else { $item_count_header2 = 0; }

			 $item_count_header = $item_count_header1 + $item_count_header2;

			 if($item_count_header != 0) { echo $item_count_header; } else { echo 0; } ?> Item(s) </small>]</h5>

     <legend></legend>

     <div class="row">

    <div class="span3">

    <div class="form-group">

                    <label class="control-label col-lg-2" for="text1">No Order's Placed </label>

                </div>

    </div>

    </div>

     </div> <!-- todo -->

     <?php } ?>

    </div>

	<?php
        $a= ['a','b'];
    ?>



</div>

</div>

</div>

</div>
<!-- MainBody End ============================= -->

<!-- Footer ================================================================== -->

	{!! $footer !!}

<!-- Placed at the end of the document so the pages load faster ============================================= -->

	<!-- <script src="<?php echo url(); ?>/themes/js/jquery.js" type="text/javascript"></script> -->

	<script src="<?php echo url(); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>

	<script src="<?php echo url(); ?>/themes/js/google-code-prettify/prettify.js"></script>

	<script src="<?php echo url(); ?>/themes/js/bootshop.js"></script>

    <script src="<?php echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>


	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDoY1OPjAqu1PlQhH3UljYsfw-81bLkI&libraries=places&signed_in=true"></script>
	<script type="text/javascript">

    console.log('tes');

    // -- fungsi penggunaan poin (Screen) --
    $("#chk_poin").on("change", function(){
        if($("#chk_poin").is(':checked'))
        {
            // -- perhitungan poin --
            var tot_poin            = $("#total_poin_cus_hidden").val();
            var tot_pembayaran      = $("#total_belanja_ditambah_shipping_hidden").val();
            var tot_hasil           = tot_pembayaran - tot_poin;

            if(tot_hasil < 0)
            {
                alert("Poin Tidak Dapat Digunakan");
                $("#chk_poin").prop('checked', false);
            }
            else
            {
                // -- memberikan poin ke label --
                $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(tot_hasil) ? 0 : tot_hasil,0,3, ',', '')+" (Poin Digunakan)");
                $("total_belanja_ditambah_shipping_hidden").val(tot_hasil);

                // -- perubahan nilai cekbok --
                $("#chk_poin").val(1);

                // -- perubahan poin
                $("#poin_yang_didapatkan").text(doFormatMoney2(isNaN(tot_hasil/1000) ? 0 : tot_hasil/1000,0,3, ',', '')+" Poin");
                $("#poin_yang_didapatkan_hidden").val(tot_hasil/1000);

                alert("Anda menggunakan poin anda");
            }
        }
        else
        {
            // -- kembalikan proses if
            alert("Anda tidak menggunkan poin anda");
            var tot_poin            = parseInt($("#total_poin_cus_hidden").val());
            var tot_pembayaran      = parseInt($("#total_belanja_ditambah_shipping_hidden").val());
            var tot_hasil           = tot_pembayaran;
            console.log(tot_hasil);
            // -- memberikan poin ke label --
            $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(tot_hasil) ? 0 : tot_hasil,0,3, ',', ''));
            console.log("line 1");
            $("total_belanja_ditambah_shipping_hidden").val(tot_hasil);
            console.log("selesai perhitungan");
            // -- perubahan nilai cekbok --
            $("#chk_poin").val(0);
            console.log("selesai pengaturan chk");
            // -- perubahan poin
            $("#poin_yang_didapatkan").text(doFormatMoney2(isNaN(tot_hasil/1000) ? 0 : tot_hasil/1000,0,3, ',', '')+" Poin");
            $("#poin_yang_didapatkan_hidden").val(tot_hasil/1000);
        }
    });

	function test(locations)
	{
	    var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 10,
	      <?php foreach($get_default_city as $gd) { ?>
	      center: new google.maps.LatLng(locations[0][1], locations[0][2]),
	      <?php } ?>
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    });

	    var infowindow = new google.maps.InfoWindow();

	    var marker, i;

	    for (i = 0; i < locations.length; i++) {
	      marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	        map: map
	      });

	      google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
	          infowindow.setContent(locations[i][0]);
	          infowindow.open(map, marker);
	        }
	      })(marker, i));

		  google.maps.event.addListener(marker, 'dblclick', (function(marker, i) {
			  return function() {
				  document.getElementById('popbox_location').value = locations[i][3];
				  var city = '';
				  if ($('#popbox_city').val()!='') {
					  city = $('#popbox_city').val();
				  }

				  $.ajax({
		               method: "get",
		               url: "<?php echo url('get_address_location')?>",
		               datatype: "json",
		               data: {
			                aCity : city,
			   				aCountry : 'Indonesia',
			   				aLocation : $("#popbox_location").val(),
		               },
		               success: function (d) {
						   document.getElementById('popbox_city').value = d.city;
						   $("#address").text(d.address);
						   $('#address_2').text(d.address_2);

		                   //form
		                   $('#fname').val(d.name);
		                   $('#addr_line').val(d.address);
		                   $('#addr1_line').val(d.address_2);
		                   $('#state').val(d.province);
		                   $('#zipcode').val(d.zip_code);
		                   $('#city_popbox_input').val(d.city);
		                   $('#country_popbox_input').val(d.country);
		                   $('#shipping_reg_number').val(d.id);
						   var zipcode = $('#zipcode').val();
						   var city_popbox = $('#city_popbox_input').val();
						   request_shipping_popbox(zipcode, city_popbox);
		                   if(d.error != null)
		                   {
		                       alert(d.error);
		                   }
		               }
		           });
			  }
		  })(marker, i));
	    }
	}
	</script>

    <script>
	function request_shipping_popbox(zipcode, city_popbox){
		$("#shipping_price_label").text("Loading...");
		$.ajax({
				method: "get",
				url: "<?php echo url('request_shipping');?>",
				datatype: "json",
				data: {
					aKode :  $("#pengiriman_oke").val(),
					aItems : [<?php
					if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
					{
						$max=count($_SESSION['cart']);
					}

					if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
					{
						$max=count($_SESSION['deal_cart'])-1;
					}

					for($i=0;$i<$max;$i++)
					{
						if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
						{
							$pid      = $_SESSION['cart'][$i]['productid'];
							$q        = $_SESSION['cart'][$i]['qty'];
							$weight   = $_SESSION['cart'][$i]['weight'];
							$length   = $_SESSION['cart'][$i]['length'];
							$width    = $_SESSION['cart'][$i]['width'];
							$height   = $_SESSION['cart'][$i]['height'];
						}

						if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
						{
							$pid=$_SESSION['deal_cart'][$i]['productid'];
							$q=$_SESSION['deal_cart'][$i]['qty'];
						}


						foreach($result_cart[$pid] as $session_cart_result)
						{
							$id        = $session_cart_result->pro_id;
							$weight2    = $session_cart_result->pro_weight;
							$length2    = floatval($session_cart_result->pro_length);
							$width2     = floatval($session_cart_result->pro_width);
							$height2    = floatval($session_cart_result->pro_height);

							$volume     = $length2 * $width2 * $height2;

							echo '{id:"'.$id.'",qty:'.$q.',weight:"'.$weight2.'",volume:"'.$volume.'"},';
						}
					}

					?>],
					aToZip : zipcode,
					aCity : city_popbox,
				},
				success: function (d) {
					if(d.error != null)
					{
						total_dan_shipping = ship_price+total;

						$("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
						$("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);

						alert(d.error);

						$("#shipping_price_label").val("");
						$("#shipping_price_label_hidden").val("");
						$("#order_total_h").val("");
						$("#shipping_price_label").text("");
						$("#input_kupon").text("");
						$("#pengiriman_oke").val('kocak');
						$('#zipcode').val('');

						$("#city").show();
						$("#country").prop('style', 'display:block;');
						$("#district_div").show();
						$("#subdistrict_div").show();
						$("#city_popbox_input").hide();
						$("#country_popbox_input").hide();
						$('#shipping_popbox').hide();
					}
					$("#shipping_price_label").text("Rp"+doFormatMoney2(isNaN(d.price) ? 0 : d.price,0,3, ',', ''));
					$("#shipping_price_label_hidden").val(d.price);


					var ship_price = parseInt(d.price);
					var total = parseInt($("#subtotal").val());
					var total_dan_shipping = ship_price+total;

					$("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
					$("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);

					$("#total_belanja").text("Rp"+number_format(total_dan_shipping, 0, '.', ','));
					for(var i=0;i<d.items.length;i++)
					{
						var total_price = +$('#total_price_input_hidden'+i).val() + +d.items[i].cost;
						$("#shipping_price_per_item"+i).text("Rp"+doFormatMoney2(isNaN(d.items[i].cost) ? 0 : d.items[i].cost,0,3, ',', ''));
						$('#total_price_span'+i).text("Rp"+number_format(total_price, 0, '.', ','));
					}

					//cek_kupon();
				},
				error: function(){
					$("#shipping_price_label").text("");
					alert("Tidak dapat terhubung ke Jasa Pengiriman");
					$('#pengiriman_oke').val('kocak');
					$("#city").show();
					$("#country").prop('style', 'display:block;');
					$("#district_div").show();
					$("#subdistrict_div").show();
					$("#city_popbox_input").hide();
					$("#country_popbox_input").hide();
					$('#shipping_popbox').hide();
					$('#zipcode').val('');
				}
			});
	}

	function number_format(number,decimals,dec_point,thousands_sep) {
        number  = number*1;//makes sure `number` is numeric value
        var str = number.toFixed(decimals?decimals:0).toString().split('.');
        var parts = [];
        for ( var i=str[0].length; i>0; i-=3 ) {
            parts.unshift(str[0].substring(Math.max(0,i-3),i));
        }
        str[0] = parts.join(thousands_sep?thousands_sep:',');
        return str.join(dec_point?dec_point:'.');
    }

    function doFormatMoney2(value, n, x, s, c)
    {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')', num = value.toFixed(Math.max(0, ~~n));
        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    }

	$('#paypal_radio').on('change', function(){
		var payment = this.options[this.selectedIndex].getAttribute('nilai');
		var channel = this.options[this.selectedIndex].getAttribute('nilai2');
		$('#payment_channel').val(channel);
		$('#paymentmethod_image').hide();

		$.ajax({
			method: "get",
			url: "<?php echo url('get_paymentmethod_image')?>",
			datatype: "json",
			data: {
				name: payment,
			},
			success: function(image){
				$('#paymentmethod_image').show();
				$('#paymentmethod_image').attr('src', "<?php echo 'assets/paymentmethodimage';?>/"+image);
			},
			error: function(){
				$('#paymentmethod_image').attr('src', "");
			}
		});

	});
	$("#country").on("change", function(){
        //alert("country");
        console.log("call jquery");
		var country = '';
		if($("#country").val()!='')
		{
			state = $("#state_id").val();
		}

		$.ajax({
			method: "get",
			url: "<?php echo url('api/get_city')?>",
			datatype: "json",
			data: {
				aState: state,
			},
			success: function(city){
				$("#city").empty();
				$('#city').append('<option value= >Choose City</option>');
				$("#district").empty();
				$('#district').append('<option value= >Choose District</option>');
				$("#subdistrict").empty();
				$('#subdistrict').append('<option value= >Choose Sub District</option>');
				$('#zipcode').val('');
				for (var i = 0; i < city.length; i++) {
					$("#city").append('<option id_city_h='+city[i].ci_id+' value='+city[i].ci_code+' >'+city[i].ci_name+'</option>');
				}
			},
            error: function(){
                console.log('error');
			}
		});

        console.log("end call jquery");
	});

	$('#state').autocomplete({
		source:function(request, response){
			$.ajax({
				url:"{{url('auto_provinsi')}}",
				method:'get',
				data:{
					term: request.term,
					country: $('#country').val()
				},
				success:function(dataSuccess){
					response(dataSuccess);
				}
			});
		},
		minLength:0,
		focus:function(event, ui){
			$('#state').val(ui.item.value);
			return false;
		},
		select:function(event, ui){
			$('#state').val(ui.item.value);
			$('#state_id').val(ui.item.id);
		},
		change:function(event, ui){
			if (ui.item===null) {
				$('#state').val('');
				$('#state_id').val('');
				alert('Silahkan Pilih Provinsi Yang Tersedia');
			}
		}
	}).bind('focus', function(){
		$(this).autocomplete("search");
	});
	$("#city").autocomplete({
		source:function(request, response){
			$.ajax({
				url:"{{url('auto_get_city')}}",
				method:'get',
				data:{
					term: request.term,
					state: $('#state_id').val()
				},
				success:function(dataSuccess){
					response(dataSuccess);
				}
			});
		},
		minLength:0,
		focus:function(event, ui){
			$('#city').val(ui.item.value);
			return false;
		},
		select:function(event, ui){
			$('#city').val(ui.item.value);
			$('#id_city_ha').val(ui.item.id);
			$('#city_code').val(ui.item.code);
		},
		change:function(event, ui){
			if (ui.item===null) {
				$('#city').val('');
				$('#id_city_ha').val('');
				$('#city_code').val('');
				alert('Silahkan pilih City yang tersedia');
			}
		}
	}).bind('focus', function(){
		$(this).autocomplete("search");
	});
	$("#district").autocomplete({
		source:function(request, response){
			$.ajax({
				url:"{{url('auto_get_district')}}",
				method:'get',
				data:{
					term: request.term,
					city: $('#id_city_ha').val()
				},
				success:function(dataSuccess){
					response(dataSuccess);
				}
			});
		},
		minLength:0,
		focus:function(event, ui){
			$('#district').val(ui.item.value);
			return false;
		},
		select:function(event, ui){
			$('#district').val(ui.item.value);
			$('#district_code').val(ui.item.id);
		},
		change:function(event, ui){
			if (ui.item===null) {
				$('#district').val('');
				$('#district_code').val('');
				alert('Silahkan pilih District yang tersedia');
			}
		}
	}).bind('focus', function(){
		$(this).autocomplete("search");
	});
	$("#subdistrict").autocomplete({
		source:function(request, response){
			$.ajax({
				url:"{{url('auto_get_subdistrict')}}",
				method:'get',
				data:{
					term: request.term,
					district: $('#district_code').val()
				},
				success:function(dataSuccess){
					response(dataSuccess);
				}
			});
		},
		minLength:0,
		focus:function(event, ui){
			$('#subdistrict').val(ui.item.value);
			return false;
		},
		select:function(event, ui){
			$('#subdistrict').val(ui.item.value);
			$('#subdistrict_code').val(ui.item.id);
			$('#zipcode').val(ui.item.zip_code);
		},
		change:function(event, ui){
			if (ui.item===null) {
				$('#subdistrict').val('');
				$('#subdistrict_code').val('');
				$('#zipcode').val('');
				alert('Silahkan pilih Sub-District yang tersedia');
			}
		}
	}).bind('focus', function(){
		$(this).autocomplete("search");
	});
    // $("#city").on("change", function () {
    //     //alert("kocak");
    //     var a = this.options[this.selectedIndex].getAttribute('id_city_h');
    //     //alert(a);
	//
    //     $("#id_city_ha").val(a);
	// 	var city = '';
	// 	if($("#city").val()!='')
	// 	{
	// 		city = $("#city").val();
	// 	}
	//
	// 	$.ajax({
	// 		method: "get",
	// 		url: "<?php echo url('api/get_district')?>",
	// 		datatype: "json",
	// 		data: {
	// 			aCity: city,
	// 		},
	// 		success: function(district){
	// 			$("#district").empty();
	// 			$('#district').append('<option value= >Choose District</option>');
	// 			$("#subdistrict").empty();
	// 			$('#subdistrict').append('<option value= >Choose Sub District</option>');
	// 			for (var i = 0; i < district.length; i++) {
	// 				$("#district").append('<option value='+district[i].dis_code+' >'+district[i].dis_name+'</option>');
	// 			}
	// 		}
	// 	});
    // });
	// $("#district").on("change", function(){
	// 	var district = '';
	// 	if($("#district").val()!='')
	// 	{
	// 		district = $("#district").val();
	// 	}
	//
	// 	$.ajax({
	// 		method: "get",
	// 		url: "<?php echo url('api/get_subdistrict')?>",
	// 		datatype: "json",
	// 		data: {
	// 			aDistrict: district,
	// 		},
	// 		success: function(subdistrict){
	// 			$("#subdistrict").empty();
	// 			$('#subdistrict').append('<option value= >Choose Sub District</option>');
	// 			for (var i = 0; i < subdistrict.length; i++) {
	// 				$("#subdistrict").append('<option zip_code='+subdistrict[i].sdis_zip_code+' value='+subdistrict[i].sdis_code+' >'+subdistrict[i].sdis_name+'</option>');
	// 			}
	// 		}
	// 	});
	// });
	// $("#subdistrict").on("change", function(){
	// 	var x = this.options[this.selectedIndex].getAttribute('zip_code');
	// 	$('#zipcode').val(x);
	//
	// });

    function request_harga_shipping_kondisi_else()
    {
        // checking null
        if($("#pengiriman_oke").val() == "kocak")
        {

        }
		// else if($('#pengiriman_oke').val() == 'AL'){
		// 	// alert("Anda memilih mengambil langsung barang anda");
		//
		// 	$("#shipping_price_label").text("Rp 0");
		// 	$("#shipping_price_label_hidden").val(0);
		//
		// 	var ship_price = parseInt(0);
		// 	var total = parseInt($("#total_price").val());
		// 	var total_dan_shipping = ship_price+total;
		//
		// 	$("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
		// 	$("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);
		//
		// 	$("#setelah_action_ambil_langsung").show();
		// }
        else
        {
            console.log("manggil fungsi request_harga_shipping_kondisi_else");
    		$("#shipping_price_label").text("Loading...");
            $.ajax({
                    method: "get",
                    url: "<?php echo url('request_shipping');?>",
                    datatype: "json",
                    data: {
                        aKode :  $("#pengiriman_oke").val(),
                        aItems : [<?php
                        if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
                        {
                            $max=count($_SESSION['cart']);
                        }

                        if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
                        {
                            $max=count($_SESSION['deal_cart'])-1;
                        }

                        for($i=0;$i<$max;$i++)
                        {
                            if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
                            {
                                $pid      = $_SESSION['cart'][$i]['productid'];
                                $q        = $_SESSION['cart'][$i]['qty'];
                                $weight   = $_SESSION['cart'][$i]['weight'];
                                $length   = $_SESSION['cart'][$i]['length'];
                                $width    = $_SESSION['cart'][$i]['width'];
                                $height   = $_SESSION['cart'][$i]['height'];
                            }

                            if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
                            {
                                $pid=$_SESSION['deal_cart'][$i]['productid'];
                                $q=$_SESSION['deal_cart'][$i]['qty'];
                            }


                            foreach($result_cart[$pid] as $session_cart_result)
                            {
                                $id        = $session_cart_result->pro_id;
                                $weight2    = $session_cart_result->pro_weight;
                                $length2    = floatval($session_cart_result->pro_length);
                                $width2     = floatval($session_cart_result->pro_width);
                                $height2    = floatval($session_cart_result->pro_height);

                                $volume     = $length2 * $width2 * $height2;

                                echo '{id:"'.$id.'",qty:'.$q.',weight:"'.$weight2.'",volume:"'.$volume.'"},';
                            }
                        }

                        ?>],
                        aToZip : $("#zipcode").val(),
                        aCity : $("#city").val(),
                    },
                    success: function (d) {
                        if(d.error != null)
                        {
                            total_dan_shipping = ship_price+total;

                            $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
                            $("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);

                            alert(d.error);

                            $("#shipping_price_label").val("");
                            $("#shipping_price_label_hidden").val("");
                            $("#order_total_h").val("");
                            $("#shipping_price_label").text("");
                            $("#input_kupon").text("");
                            $("#pengiriman_oke").val('kocak');
                        }
                        $("#shipping_price_label").text("Rp"+doFormatMoney2(isNaN(d.price) ? 0 : d.price,0,3, ',', ''));
                        $("#shipping_price_label_hidden").val(d.price);


                        var ship_price = parseInt(d.price);
                        var total = parseInt($("#subtotal").val());
                        var total_dan_shipping = ship_price+total;

                        $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
                        $("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);

                        $("#total_belanja").text("Rp"+number_format(total_dan_shipping, 0, '.', ','));
                        for(var i=0;i<d.items.length;i++)
                        {
                            var total_price = +$('#total_price_input_hidden'+i).val() + +d.items[i].cost;
                            $("#shipping_price_per_item"+i).text("Rp"+doFormatMoney2(isNaN(d.items[i].cost) ? 0 : d.items[i].cost,0,3, ',', ''));
                            $('#total_price_span'+i).text("Rp"+number_format(total_price, 0, '.', ','));
                        }

						if(d.kode == 'AL'){
							$("#setelah_action_ambil_langsung").show();
						}else {
							$("#setelah_action_ambil_langsung").hide();
						}

                        //cek_kupon();
                    },
                    error: function(){
                        $("#shipping_price_label").text("");
                        alert("Tidak dapat terhubung ke Jasa Pengiriman");
						$('#pengiriman_oke').val('kocak');
                    }
                });
        }
    }

    function cek_kupon()
    {
        console.log('memanggil fungsi cek kupon');

        if($("#pengiriman_oke").val() != "kocak")
        {
            // memanggil kembali fungsi mendapatkan harga shipping
            request_harga_shipping_kondisi_else();
        }
        // memberi nilai pada inputan hide code voucher u/ mengganti status_pemakaian_voucher
        //$("#kode_voucher").val($("#input_kupon").val());

        $.ajax({
            method: "get",
            url: "<?php echo url('cek_kupon')?>",
            datatype: "json",
            data: {
                nomor_kupon :  $("#input_kupon").val(),
                total_belanja_awal: <?php echo round($overall_pro_price+$overall_deal_price);?>,
                harga_shipping : $("#shipping_price_label_hidden").val(),
                today : new Date(),
                postal_code : $("#pengiriman_oke").val(),
                pajak : 0,

                cart : [<?php
                    if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
                        $max=count($_SESSION['cart']);
                    }
                    if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){
                        $max=count($_SESSION['deal_cart'])-1;
                    }
                    for($i=0;$i<$max;$i++){
                    if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
                        $pid=$_SESSION['cart'][$i]['productid'];
                        $q=$_SESSION['cart'][$i]['qty'];
						$weight   = $_SESSION['cart'][$i]['weight'];
		                $length   = $_SESSION['cart'][$i]['length'];
		                $width    = $_SESSION['cart'][$i]['width'];
		                $height   = $_SESSION['cart'][$i]['height'];
                    }
                    if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart'])){
                        $pid=$_SESSION['deal_cart'][$i]['productid'];
                        $q=$_SESSION['deal_cart'][$i]['qty'];
                    }


                        foreach($result_cart[$pid] as $session_cart_result)
                        {
							$id        = $session_cart_result->pro_id;
			                $weight2    = $session_cart_result->pro_weight;
			                $length2    = floatval($session_cart_result->pro_length);
			                $width2     = floatval($session_cart_result->pro_width);
			                $height2    = floatval($session_cart_result->pro_height);
                            $sku =  $session_cart_result->pro_sku ;

							if ($session_cart_result->wholesale_level1_min>1) {
			                    if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q):
			                        $price = floatval($session_cart_result->wholesale_level5_price);
			                    elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q):
			                        $price = floatval($session_cart_result->wholesale_level4_price);
			                    elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q):
			                        $price = floatval($session_cart_result->wholesale_level3_price);
			                    elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q):
			                        $price = floatval($session_cart_result->wholesale_level2_price);
			                    elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q):
			                        $price = floatval($session_cart_result->wholesale_level1_price);
			                    else:
			                        if ($session_cart_result->pro_disprice==0 || $session_cart_result->pro_disprice=='' || $session_cart_result->pro_disprice==null):
			                            $price = floatval($session_cart_result->pro_price);
			                        else:
			                            $price = floatval($session_cart_result->pro_disprice);
			                        endif;
			                    endif;
			                } else
			                if(!isset($session_cart_result->promop_schedp_id) || empty($session_cart_result->promop_schedp_id)){
			                    if ($session_cart_result->pro_disprice==0 || $session_cart_result->pro_disprice=='' || $session_cart_result->pro_disprice==null){
			                        $price = floatval($session_cart_result->pro_price);
			                    } else {
			                        $price = floatval($session_cart_result->pro_disprice);
			                    }
			                }else{
			                    if ($session_cart_result->pro_disprice==0 || $session_cart_result->pro_disprice=='' || $session_cart_result->pro_disprice==null){
			                        $price = floatval($session_cart_result->promop_discount_price);
			                    } else {
			                        $price = floatval($session_cart_result->promop_discount_price);
			                    }
			                }

			                $volume     = $length2 * $width2 * $height2;

                            echo '{sku:"'.$pid.'",qty:'.$q.',weight:'.$weight2.',volume:'.$volume.',price:'.$price.'}, ';
                        }
                    }

                    ?>],
            },
            success: function (d) {
                //dd(d);
				$("#setelah_action_kupon").hide();
                if(d.error != null)
                {
                    alert(d.error);
                    $("#input_kupon").val("");
                }
                else
                {
                    // Show hidden div
                    $("#setelah_action_kupon").show();
                    $("#judul_diskon_amt").text("Selamat Anda Mendapatkan Promo "+d.schedp_title);

                    if(d.schedp_simple_action == "by_percent")
                    {
                        $("#persen_kupon_angka").text(d.schedp_discount_amount+" %");
                    }
                    else if(d.schedp_simple_action == "by_percent")
                    {
                        $("#persen_kupon_angka").text("Rp"+doFormatMoney2(isNaN(d.schedp_discount_amount) ? 0 : d.schedp_discount_amount,0,3, ',', ''));
                    }
                    else
                    {
                        $("#persen_kupon_angka").text(d.schedp_discount_amount);
                    }

                    $("#persen_kupon_angka_hidden").val(d.schedp_discount_amount);

                    // end


                    $("#total_belanja_kupon").text("Rp"+doFormatMoney2(isNaN(d.total_belanja_akhir) ? 0 : d.total_belanja_akhir,0,3, ',', ''));
                    $("#total_belanja_kupon_hidden").val(d.total_belanja_akhir);

                    $("#total_diskon_kupon").text("Rp"+doFormatMoney2(isNaN(d.total_diskon) ? 0 : d.total_diskon,0,3, ',', ''));
                    $("#total_diskon_kupon_hidden").val(d.total_diskon);

                    $("#order_total_h").text("Rp"+doFormatMoney2(isNaN(d.total_belanja_setelah_pajak) ? 0 : d.total_belanja_setelah_pajak,0,3, ',', ''));
                    $("#order_total_h").val(d.total_belanja_setelah_pajak);
                    $("#total_price").val(d.total_belanja_setelah_pajak);


                    $("#total_belanja_ditambah_shipping").text("Rp"+ doFormatMoney2(isNaN(d.total_belanja_plus_shipping) ? 0 : d.total_belanja_plus_shipping,0,3, ',', ''));
                    $("#total_belanja_ditambah_shipping_hidden").val(d.total_belanja_plus_shipping);
                }

            }
        });
    }

    $("#button_kupon").on("click", function ()
    {
        if($("#input_kupon").val() == "")
        {
            alert("masukkan kode Kupon anda");
        }
        else
        {
			$("#setelah_action_kupon").show();
			$("#judul_diskon_amt").text("Loading...");
            cek_kupon();
        }
    });

    $("#pengiriman_oke").on("change", function () {
        console.log("masuk action pengiriman");
        $('#setelah_action_ambil_langsung').hide();

        // mereset nilai kupon jadi kosong. pengguna di minta memasukan kembali kupon
        if($("#input_kupon").val() != "")
        {
            // $("#input_kupon").val("");
            $("#setelah_action_kupon").hide();
        }
        // checking null
        if($("#pengiriman_oke").val() == "kocak")
        {
            alert("Silahkan Pilih Pengiriman");
        }
        else if($('#pengiriman_oke').val() == "AL")
        {
            // alert("Anda memilih mengambil langsung barang anda");
		
            $("#shipping_price_label").text("Rp 0");
            $("#shipping_price_label_hidden").val(0);
		
            var ship_price = parseInt(0);
            var total = parseInt($("#total_price").val());
            var total_dan_shipping = ship_price+total;
		
			for (var i = 0; i < {{count($_SESSION['cart'])}}; i++) {
				$("#shipping_price_per_item"+i).text("Rp"+doFormatMoney2(isNaN(d.items[i].cost) ? 0 : d.items[i].cost,0,3, ',', ''));
				$('#total_price_span'+i).text("Rp"+number_format(total_price, 0, '.', ','));
			}
		
            $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
            $("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);
		
            $("#setelah_action_ambil_langsung").show();
        }
        else if($("#pengiriman_oke").val() == "POPBOX")
        {
            $("#city").hide();
            $("#country").prop('style', 'display:none;');
			$("#district_div").hide();
			$("#subdistrict_div").hide();

            $("#city_popbox_input").show();
            $("#country_popbox_input").show();

			$('#zipcode').val('');

            $('#shipping_popbox').show();
			$.ajax({
				 method: "get",
				 url: "<?php echo url('get_location_popbox')?>",
				 datatype: "json",
				 data: {
					 aCity : '',
					 aCountry : 'Indonesia',
				 },
				 success: function (d) {
					 $('#popbox_location').empty();
					 $('#popbox_location').append('<option value=null>Pilih Location</option>');

					 for (var i = 0; i < d['total_data']; i++) {
						 $("#popbox_location").append('<option value='+d.data[i].id+'>'+d.data[i].name+'</option>');
					 }
					 if(d.error != null)
					 {
						 alert(d.error);

                         $("#shipping_price_label").val("");
                         $("#shipping_price_label_hidden").val("");
                         $("#order_total_h").val("");
                         $("#shipping_price_label").text("");
                         //$("#order_total_h").text(<?php  echo round($overall_pro_price+$overall_deal_price);?>);
					 }
				 }
			});
			initialize();
        }
        else
        {
            console.log("masuk else selain PopBox");
			$("#shipping_price_label").text("Loading...");
			$("#city").show();
            $("#country").prop('style', 'display:block;');;
			$("#district_div").show();
			$("#subdistrict_div").show();

			$("#city_popbox_input").hide();
            $("#country_popbox_input").hide();

			$('#shipping_popbox').hide();
            //alert("Dia Bukan PopBox");
            $.ajax({
                method: "get",
                url: "<?php echo url('request_shipping');?>",
                datatype: "json",
                data: {
                    aKode :  $("#pengiriman_oke").val(),
                    aItems : [<?php
					if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
                    {
                    	$max=count($_SESSION['cart']);
					}

					if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
                    {
						$max=count($_SESSION['deal_cart'])-1;
					}

                    for($i=0;$i<$max;$i++)
                    {
    					if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
                        {
    						$pid      = $_SESSION['cart'][$i]['productid'];
    	                    $q        = $_SESSION['cart'][$i]['qty'];
                            $weight   = $_SESSION['cart'][$i]['weight'];
                            $length   = $_SESSION['cart'][$i]['length'];
                            $width    = $_SESSION['cart'][$i]['width'];
                            $height   = $_SESSION['cart'][$i]['height'];
    					}

    					if(isset($_SESSION['deal_cart']) && !empty($_SESSION['deal_cart']))
                        {
    						$pid=$_SESSION['deal_cart'][$i]['productid'];
    	                    $q=$_SESSION['deal_cart'][$i]['qty'];
    					}


                        foreach($result_cart[$pid] as $session_cart_result)
                        {
                            $id        = $session_cart_result->pro_id;
                            $weight2    = $session_cart_result->pro_weight;
                            $length2    = floatval($session_cart_result->pro_length);
                            $width2     = floatval($session_cart_result->pro_width);
                            $height2    = floatval($session_cart_result->pro_height);

                            $volume     = $length2 * $width2 * $height2;

                            echo '{id:"'.$id.'",qty:'.$q.',weight:"'.$weight2.'",volume:"'.$volume.'"},';
                        }
                    }

                    ?>],
                    aToZip : $("#zipcode").val(),
                    aCity : $("#city").val(),
                },
                success: function (d) {
					if(d.error != null)
                    {
                        total_dan_shipping = ship_price+total;

                        $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
                        $("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);

                        alert(d.error);

                        $("#shipping_price_label").val("");
                        $("#shipping_price_label_hidden").val("");
                        $("#order_total_h").val("");
                        $("#shipping_price_label").text("");
                        $("#input_kupon").text("");
						$("#pengiriman_oke").val('kocak');
                    }
                    $("#shipping_price_label").text("Rp"+doFormatMoney2(isNaN(d.price) ? 0 : d.price,0,3, ',', ''));
                    $("#shipping_price_label_hidden").val(d.price);


                    var ship_price = parseInt(d.price);
                    var total = parseInt($("#subtotal").val());
                    var total_dan_shipping = ship_price+total;

                    $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(total_dan_shipping) ? 0 : total_dan_shipping,0,3, ',', ''));
                    $("#total_belanja_ditambah_shipping_hidden").val(total_dan_shipping);

					$("#total_belanja").text("Rp"+number_format(total_dan_shipping, 0, '.', ','));
					for(var i=0;i<d.items.length;i++)
					{
						var total_price = +$('#total_price_input_hidden'+i).val() + +d.items[i].cost;
						$("#shipping_price_per_item"+i).text("Rp"+doFormatMoney2(isNaN(d.items[i].cost) ? 0 : d.items[i].cost,0,3, ',', ''));
						$('#total_price_span'+i).text("Rp"+number_format(total_price, 0, '.', ','));
					}

					if(d.kode == 'AL'){
						$("#setelah_action_ambil_langsung").show();
					}else {
						$("#setelah_action_ambil_langsung").hide();
					}

                    //cek_kupon();
                },
				error: function(){
					$("#shipping_price_label").text("");
					alert("Tidak dapat terhubung ke Jasa Pengiriman");
					$('#pengiriman_oke').val('kocak');
				}
            });
       }
    });

	$("#popbox_city").on("change", function () {
	   var locations = [];
	   var city = '';
	   if($("#popbox_city").val()!='')
	   {
		   city = $("#popbox_city").val();
	   }
       $.ajax({
            method: "get",
            url: "<?php echo url('get_location_popbox')?>",
            datatype: "json",
            data: {
                aCity : city,
				aCountry : 'Indonesia',
            },
            success: function (d) {
				$('#popbox_location').empty();
				$('#popbox_location').append('<option value=null>Pilih Location</option>');

				$("#address").text('');
				$('#address_2').text('');

				//form
                $('#fname').val('');
                $('#addr_line').val('');
                $('#addr1_line').val('');
                $('#state').val('');
                $('#zipcode').val('');
                $('#city_popbox_input').val('');
                $('#country_popbox_input').val('');
                $('#shipping_reg_number').val('');
				for (var i = 0; i < d['total_data']; i++) {
					$("#popbox_location").append('<option value='+d.data[i].id+'>'+d.data[i].name+'</option>');
					locations.push(["<b>Store Name:</b>&nbsp;"+ d.data[i].name +",<br/><b>Address:</b>&nbsp;"+ d.data[i].address +",<br/>"+ d.data[i].address_2, d.data[i].latitude, d.data[i].longitude, d.data[i].id]);
				}
				test(locations);
                if(d.error != null)
                {
                    alert(d.error);
                }
            }
        });
    });

	$("#popbox_location").on("change", function () {
	   var locations = [];
       $.ajax({
            method: "get",
            url: "<?php echo url('get_address_location')?>",
            datatype: "json",
            data: {
                aCity : $("#popbox_city").val(),
				aCountry : 'Indonesia',
				aLocation : $("#popbox_location").val(),
            },
            success: function (d) {

				$("#address").text(d.address);
				$('#address_2').text(d.address_2);

                //form
                $('#fname').val(d.name);
                $('#addr_line').val(d.address);
                $('#addr1_line').val(d.address_2);
                $('#state').val(d.province);
                $('#zipcode').val(d.zip_code);
                $('#city_popbox_input').val(d.city);
                $('#country_popbox_input').val(d.country);
                $('#shipping_reg_number').val(d.id);

				var zipcode = $('#zipcode').val();
				var city_popbox = $('#city_popbox_input').val();
				request_shipping_popbox(zipcode, city_popbox);

				locations.push(["<b>Store Name:</b>&nbsp;"+ d.name +",<br/><b>Address:</b>&nbsp;"+ d.address +",<br/>"+ d.address_2, d.latitude, d.longitude, d.data[i].id]);
				test(locations);
                if(d.error != null)
                {
                    alert(d.error);
                }
            }
        });
    });
	$('#cari').click(function(){
		if($('#popbox_location').val()!='' && $('#popbox_city').val()!='')
		{
			var locations = [];
			$.ajax({
	             method: "get",
	             url: "<?php echo url('get_address_location')?>",
	             datatype: "json",
	             data: {
	                aCity : $("#popbox_city").val(),
	 				aCountry : 'Indonesia',
	 				aLocation : $("#popbox_location").val(),
	             },
	             success: function (d) {
					 locations.push(["<b>Store Name:</b>&nbsp;"+ d.name +",<br/><b>Address:</b>&nbsp;"+ d.address +",<br/>"+ d.address_2, d.latitude, d.longitude, d.id]);
					 test(locations);
	                 if(d.error != null)
	                 {
	                     alert(d.error);
	                 }
	             }
	         });
		}
		if ($('#popbox_city').val()!='' && $('#popbox_location').val()=='') {
			var locations = [];
			$.ajax({
		         method: "get",
		         url: "<?php echo url('get_location_popbox');?>",
		         datatype: "json",
		         data: {
		             aCity : $("#popbox_city").val(),
		             aCountry : 'Indonesia',
		         },
		         success: function (d) {
		             for (var i = 0; i < d['total_data']; i++) {
		                 locations.push(["<b>Store Name:</b>&nbsp;"+ d.data[i].name +",<br/><b>Address:</b>&nbsp;"+ d.data[i].address +",<br/>"+ d.data[i].address_2, d.data[i].latitude, d.data[i].longitude, d.data[i].id]);
		             }
		             test(locations);
		             if(d.error != null)
		             {
		                 alert(d.error);
		             }
		         }
		     });
		}
		if ($('#popbox_city').val()=='' && $('#popbox_location').val()==''){
			initialize();
		}
	});

    $("#button_cek").on("click", function () {
        // $.ajax({
        //     method: "get",
        //     url: "get_provincy_popbox",
        //     datatype: "json",
        //     data: {
        //     },
        //     success: function (d) {
        //         alert("Sukses");
        //         alert(d);
        //     }
        // });
        alert("oke");
        $('#MyModal').show();
    });

	function shipping_addr_func(val)

    {

		//alert(pid+'-'+val);

		var ship_name = $('#ship_name').val();

		var ship_address1 = $('#ship_address1').val();

		var ship_address2 = $('#ship_address2').val();

		var ship_city = $('#ship_city').val();

		var ship_state =  $('#ship_state').val();

		var ship_postalcode = $('#ship_postalcode').val();

		var ship_phone =  $('#ship_phone').val();

        var ship_email =  $('#ship_email').val();

		var ship_country = $('#ship_country').val();
		var ship_city_label = $('#ship_city_label').val();
		var ship_city_id = $('#ship_city_id').val();
		var ship_district_label = $('#ship_district_label').val();
		var ship_district = $("#ship_district").val();
		var ship_subdistrict_label = $('#ship_subdistrict_label').val();
		var ship_subdistrict = $("#ship_subdistrict").val();

		if( val > 0)

		{
			$.ajax({
				method: "get",
				url: "<?php echo url('api/get_city')?>",
				datatype: "json",
				data: {
					aCountry: ship_country,
				},
				success: function(city){
					for (var i = 0; i < city.length; i++) {
						$("#city").append('<option id_city_h='+city[i].ci_id+' value='+city[i].ci_code+' >'+city[i].ci_name+'</option>');
					}

					$('#city option[value= '+ship_city+']').attr('selected', true);
                    //$("#gate option[value='Gateway 2']").prop('selected', true);

                    var a = $("#city option:selected").attr('id_city_h');
                    $("#id_city_ha").val(a);

				}
			});
			 //alert(ship_city);
			$('#fname').val(ship_name);

			$('#lname').val('');

			$('#addr_line').val(ship_address1);

			$('#addr1_line').val(ship_address2);

			$('#state').val(ship_state);

			$('#country').val(ship_country);

			$('#city_code').val(ship_city);

			$('#city').val(ship_city_label);

			$('#id_city_ha').val(ship_city_id);

			$('#district').val(ship_district_label);

			$("#district_code").val(ship_district);

			$('#subdistrict').val(ship_subdistrict_label);

			$("#subdistrict_code").val(ship_subdistrict);

			$('#phone1_line').val(ship_phone);

			$('#zipcode').val(ship_postalcode);

			$('#email').val(ship_email);



			$('#load_ship').val(1);
			// reload_district();
		}

		else

		{

			$('#fname').val('');

			$('#lname').val('');

			$('#addr_line').val('');

			$('#addr1_line').val('');

			$("#city").val('');

			$("#district").val('');

			$("#subdistrict").val('');

			$("#city_code").val('');

			$("#id_city_ha").val('');

			$("#district_code").val('');

			$("#subdistrict_code").val('');

			$('#state').val('');

            $('#country').val('');

			$('#phone1_line').val('');

			$('#zipcode').val('');

            $('#email').val('');

			$('#load_ship').val(0);

		}

	}

	function reload_district()
	{
		var ship_city = $('#ship_city').val();
		var ship_district = $('#ship_district').val();
	    $.ajax({
	        method: "get",
	        url: "<?php echo url('api/get_district');?>",
	        datatype: "json",
	        data: {
	            aCity: ship_city,
	        },
	        success: function(district){
	            for (var i = 0; i < district.length; i++) {
	                //alert('district');
	                $("#district").append('<option value='+district[i].dis_code+' >'+district[i].dis_name+'</option>');
	            }
	            $('#district').val(ship_district);
	            reload_subdistrict();
	        }
	    });
	}

	function reload_subdistrict()
	{
		var ship_district = $('#ship_district').val();
		var ship_subdistrict = $('#ship_subdistrict').val();
	    $.ajax({
	        method: "get",
	        url: "<?php echo url('api/get_subdistrict');?>",
	        datatype: "json",
	        data: {
	            aDistrict: ship_district,
	        },
	        success: function(subdistrict){
	            for (var i = 0; i < subdistrict.length; i++) {
	                $("#subdistrict").append('<option zip_code='+subdistrict[i].sdis_zip_code+' value='+subdistrict[i].sdis_code+' >'+subdistrict[i].sdis_name+'</option>');
	            }
	            $('#subdistrict').val(ship_subdistrict);
	            reload_zipcode();
	        }
	    });
	}

	function reload_zipcode()
	{
	    var x = document.getElementById('subdistrict').options[document.getElementById('subdistrict').selectedIndex].getAttribute('zip_code');
	    $('#zipcode').val(x);
	}

	$(document).ready(function(){

    var tot_all_new = parseInt($("#subtotal").val()); // total belanja hanya barang
    for(i = 0; i < <?php echo count($result_cart);?>;i++)
    {
        (function (i) {
            $("#extended_warranty_cb"+i).on('change', function(){
                if($("#extended_warranty_cb"+i).prop('checked'))
                {
                    // total extended warranty + subtotal barang yg sama
                    var tot_new = parseInt($("#extended_warranty_span_hidden"+i).val())+parseInt($("#total_price_input_hidden"+i).val());

                    // layer sub per barang
                    $("#total_price_input_hidden"+i).val(tot_new); // subtotal barang yg sama diubah
                    $("#total_price_span"+i).text("Rp"+doFormatMoney2(isNaN(tot_new) ? 0 : tot_new,0,3, ',', '')); // sama tapi no hidden
                    $("#extended_warranty_cb"+i).val(1); // cb di kasih nilai 1

                    // layer keseluruhan
                    tot_all_new = tot_all_new + parseInt($("#extended_warranty_span_hidden"+i).val()); // total hanya belanja di tambah setiap warranty
                    $("#subtotal").val(tot_all_new); // memberi nilai ke subtotal
                    $("#total_belanja").text("Rp"+doFormatMoney2(isNaN(tot_all_new) ? 0 : tot_all_new,0,3, ',', '')); // memberi nilai ke subtotal
                    $("#total_belanja_ditambah_shipping_hidden").val(tot_all_new); // memberi nilai total + shipping
                    $("#total_belanja_ditambah_shipping").text("Rp"+doFormatMoney2(isNaN(tot_all_new) ? 0 : tot_all_new,0,3, ',', '')); // memberi nilai ke total + shipping

                    var x = i+1;
                    $("#item_warranty_val"+x).val(1); // hidden warranty untuk post

                    // panggil kembali shipping
                    if($("#pengiriman_oke").val != "" || $("#pengiriman_oke").val != 0)
                    {
                        request_harga_shipping_kondisi_else();
                    }
                    alert("Extended Warranty Digunakan");
                }
                else
                {
                    var tot_new = parseInt($("#total_price_input_hidden"+i).val())-parseInt($("#extended_warranty_span_hidden"+i).val());

                    $("#total_price_input_hidden"+i).val(tot_new);
                    $("#total_price_span"+i).text("Rp"+doFormatMoney2(isNaN(tot_new) ? 0 : tot_new,0,3, ',', ''));
                    $("#extended_warranty_cb"+i).val(0);

                    tot_all_new = tot_all_new - parseInt($("#extended_warranty_span_hidden"+i).val());
                    $("#subtotal").val(tot_all_new);
                    $("#total_belanja").text("Rp"+doFormatMoney2(isNaN(tot_all_new) ? 0 : tot_all_new,0,3, ',', ''));

                    var x = i+1;
                    $("#item_warranty_val"+x).val(0);

                    // panggil kembali shipping
                    if($("#pengiriman_oke").val != "" || $("#pengiriman_oke").val != 0)
                    {
                        request_harga_shipping_kondisi_else();
                    }
                    alert("Extended Warranty Tidak Digunakan");
                }
            });
 			var j = i+1;
			$("#cicilan"+j).on("change", function(){
				if($('.cicilan').is(':checked'))
				{
					$('option[value!=5]').hide();
					$('#paypal_radio').val(5).trigger('change');
				}
				else
				{
					$('option[value!=5]').show();
				}

	            if($("#cicilan"+j).prop('checked'))
	            {
	                $("#cicilan"+j).val(1);
					$("#cicilan2"+j).val(1);
	            }
	            else
	            {
	                $("#cicilan"+j).val(0);
					$("#cicilan2"+j).val(0);
	            }
	        });
        })(i);
    }


		$('#paypal_radio').click(function(){

				$('#paypal_div').show();

				$('#cod_div').hide();

		});

		$('#cod_radio').click(function(){

				$('#paypal_div').hide();

				$('#cod_div').show();

		});

		$('#mulshipping_addr_1rad').click(function(){

			$('.shipping_addr_class').css("display","block");

			});

		$('#mulshipping_addr_2rad').click(function(){

				$('.shipping_addr_class').css("display","none");

				$('#shipping_addr_div1').css("display","block");

			});



		var count = $('#count_pid').val();

		var zip_regex =  /[0-9-()+]{6,7}/;

		var phone_regex =  /[0-9-()+]{8,10}/;

		$('#place_order_submit').click(function(){
			//alert($('input:radio[name=mul_shipping_addr]:checked').val());
			if($('input:radio[name=mul_shipping_addr]:checked').val()=="yes")
			{
			for(var i=1;i<=count;i++)
			{
				if($('#fname'+i).val() == '')
				{
					$('#fname'+i).css("border","1px solid red");
					$('#fname'+i).focus();
					$('#error_div').html('Enter First Name');

					return false;
				}
				else
				{
					$('#fname'+i).css("border","");
					$('#error_div').html('');
				}

				if($('#lname'+i).val() == '')
				{
					$('#lname'+i).css("border","1px solid red");
					$('#lname'+i).focus();
					$('#error_div').html('Enter Last Name');

					return false;
				}
				else
				{
					$('#lname'+i).css("border","");
					$('#error_div').html('');
				}
				if($('#addr_line'+i).val() == '')
				{
					$('#addr_line'+i).css("border","1px solid red");
					$('#addr_line'+i).focus();
					$('#error_div').html('Enter Address Line1');

					return false;
				}
				else
				{
					$('#addr_line'+i).css("border","");
					$('#error_div').html('');
				}
				if($('#addr1_line'+i).val() == '')
				{
					$('#addr1_line'+i).css("border","1px solid red");
					$('#addr1_line'+i).focus();
					$('#error_div').html('Enter Address Line2');

					return false;
				}
				else
				{
					$('#addr1_line'+i).css("border","");
					$('#error_div').html('');
				}
				if($('#city'+i).val() == '')
				{
					$('#city'+i).css("border","1px solid red");
					$('#city'+i).focus();
					$('#error_div').html('Enter your city');

					return false;
				}
				else
				{
					$('#city'+i).css("border","");
					$('#error_div').html('');
				}
				if($('#state'+i).val() == '')
				{
					$('#state'+i).css("border","1px solid red");
					$('#state'+i).focus();
					$('#error_div').html('Enter your state');

					return false;
				}
				else
				{
					$('#state'+i).css("border","");
					$('#error_div').html('');
				}
				if($('#phone1_line'+i).val() == '')
				{
					$('#phone1_line'+i).css("border","1px solid red");
					$('#error_div').html('Enter your phone no');
					$('#phone1_line'+i).focus();

					return false;
				}
				else if(!phone_regex.test($('#phone1_line'+i).val()))
				{
					$('#phone1_line'+i).css("border","1px solid red");
					$('#phone1_line'+i).focus();
					$('#error_div').html('Enter your valid phone no');

					return false;
				}
				else
				{
					$('#phone1_line'+i).css("border","");
					$('#error_div').html('');
				}
				if($('#zipcode'+i).val() == '')
				{
					$('#zipcode'+i).css("border","1px solid red");
					$('#zipcode'+i).focus();
					$('#error_div').html('Enter your zipcode');

					return false;
				}
				else if(!zip_regex.test($('#zipcode'+i).val()))
				{
					$('#zipcode'+i).css("border","1px solid red");
					$('#zipcode'+i).focus();
					$('#error_div').html('Enter your valid zipcode');

					return false;
				}
				else
				{
					$('#zipcode'+i).css("border","");
					$('#error_div').html('');
				}
			}
			}

			else if($('input:radio[name=mul_shipping_addr]:checked').val()=="no")
			{		//alert('asd');
				if($('#fname1').val() == '')
				{
					$('#fname1').css("border","1px solid red");
					$('#fname1').focus();
					$('#error_div').html('Enter your first name');

					return false;
				}
				else
				{
					$('#fname1').css("border","");
					$('#error_div').html('');
				}
				if($('#lname1').val() == '')
				{
					$('#lname1').css("border","1px solid red");
					$('#lname1').focus();
					$('#error_div').html('Enter your last name');

					return false;
				}
				else
				{
					$('#lname1').css("border","");
					$('#error_div').html('');
				}
				if($('#addr_line1').val() == '')
				{
					$('#addr_line1').css("border","1px solid red");
					$('#addr_line1').focus();
					$('#error_div').html('Enter your address line1');
					return false;
				}
				else
				{
					$('#addr_line1').css("border","");
					$('#error_div').html('');
				}
				if($('#addr1_line1').val() == '')
				{
					$('#addr1_line1').css("border","1px solid red");
					$('#addr1_line1').focus();
					$('#error_div').html('Enter your address line2');

					return false;
				}
				else
				{
					$('#addr1_line1').css("border","");
					$('#error_div').html('');
				}
				if($('#city1').val() == '')
				{
					$('#city1').css("border","1px solid red");
					$('#city1').focus();
					$('#error_div').html('Enter your city');

					return false;
				}
				else
				{
					$('#city1').css("border","");
					$('#error_div').html('');
				}
				if($('#state1').val() == '')
				{
					$('#state1').css("border","1px solid red");
					$('#state1').focus();
					$('#error_div').html('Enter your state');

					return false;
				}
				else
				{
					$('#state1').css("border","");
					$('#error_div').html('');
				}

				if($('#phone1_line1').val() == '')
				{
					$('#phone1_line1').css("border","1px solid red");
					$('#phone1_line1').focus();
					$('#error_div').html('Enter your phone no');
					return false;
				}
				else if(!phone_regex.test($('#phone1_line1').val()))
				{
					$('#phone1_line1').css("border","1px solid red");
					$('#phone1_line1').focus();
					$('#error_div').html('Enter your valid phone no');

					return false;
				}
				else
				{
					$('#phone1_line1').css("border","");
					$('#error_div').html('');
				}

				if($('#zipcode1').val() == '')
				{
					$('#zipcode1').css("border","1px solid red");
					$('#zipcode'+i).focus();
					$('#error_div').html('Enter your zipcode');
					return false;
				}

				else if(!zip_regex.test($('#zipcode1').val()))
				{
					$('#zipcode1').css("border","1px solid red");
					$('#zipcode1').focus();
					$('#error_div').html('Enter your valid zipcode');
					return false;
				}

				else
				{
					$('#zipcode1').css("border","");
					$('#error_div').html('');
				}
			}
			if($('#pengiriman_oke').val()=='kocak'){
				$('#pengiriman_oke').css('border', '1px solid red');
				alert("Silahkan Pilih Pengiriman");
				$('#pengiriman_oke').focus();
				return false;
			}
			else if ($('#pengiriman_oke').val()=='POPBOX' && $('#zipcode').val()=='') {
				$('#pengiriman_oke').css('border', '1px solid red');
				alert("Silahkan Pilih Lokasi Popbox");
				$('#pengiriman_oke').focus();
				return false;
			}
			else
			{
				$('#pengiriman_oke').css('border', '');
			}

			});



		});
    </script>


<script>
	//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>


</body>

</html>
