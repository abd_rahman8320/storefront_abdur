<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->


<!-- Banner Start ================================================== -->
<div class="container">
	@if (Session::has('payment_cancel'))
	<div class="alert alert-danger alert-dismissable">{!! Session::get('payment_cancel') !!}
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	</div>
	@endif
	@if (Session::has('success'))
	<div class="alert alert-success alert-dismissable">{!! Session::get('success') !!}
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	</div>
	@endif
	@include('includes/banner')
</div>
<!-- Banner End ================================================== -->

<!-- Ad Promo Start ================================================== -->
<!-- @include('includes/adpromo') -->
<!-- Ad Promo End ================================================== -->

<!-- Main Content Start ================================================== -->
<div id="mainBody" >
	@include('includes/content')
</div>
<?php if (isset($get_popup)): ?>
<center>
	<?php foreach($get_popup as $banner)
		{
			$i = 1;
			$bannerimagepath="assets/PopUp/".$banner->pop_image;

			date_default_timezone_set('Asia/Jakarta');
						$today = date('Y-m-d H:i:s');

			if($banner->start_date <= $today && $banner->end_date >= $today)
			{
				?>
				<div id="pop1" class="simplePopup" style="display:none">
					<div <?php if($i==1){ ?><?php } else { ?><?php } ?>>
						<a href="<?php echo url('get_countpopup_click/'.$banner->pop_id); ?>">
							<img width="auto" src="<?php echo url('').'/'.$bannerimagepath; ?>" />
						</a>
					</div>
					<br/>
					<input type="button" id="pop-close" class="pop-close" value="Close" />
				</div>

				<?php
			} else { ?>
									<!-- <div id="pop1" class="simplePopup" style="display:none"></div> -->

		<?php	}}?>
</br>
</center>
<?php endif; ?>

<!-- Main Content End ================================================== -->

<!-- Features Start ================================================== -->
@include('includes/features')
<!-- Features End ================================================== -->

<!-- Footer ================================================================== -->
	{!! $footer !!}
	<script src="<?php echo url(); ?>/themes/js/simplePopup.js"></script>
	<script src="<?php echo url(); ?>/themes/js/jquery.cookie.js"></script>
	<script type="text/javascript">
		function close_pop() {
				$('.simplePopup, .simplePopupBackground').animate({
						'opacity': '0'
				}, 300, 'linear', function () {
						$('.simplePopup, .simplePopupBackground').css('display', 'none');
				});

				setCookie('hide', true, 1);
				return false;
			}

			function getCookie(c_name) {
				var c_value = document.cookie;
				var c_start = c_value.indexOf(" " + c_name + "=");
				if (c_start == -1) {
						c_start = c_value.indexOf(c_name + "=");
				}
				if (c_start == -1) {
						c_value = null;
				} else {
						c_start = c_value.indexOf("=", c_start) + 1;
						var c_end = c_value.indexOf(";", c_start);
						if (c_end == -1) {
								c_end = c_value.length;
						}
						c_value = unescape(c_value.substring(c_start, c_end));
				}
				return c_value;
			}

			function setCookie(c_name, value, exdays) {
				var exdate = new Date();
				exdate.setDate(exdate.getDate() + exdays);
				var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toGMTString());
				document.cookie = c_name + "=" + c_value;
			}

		window.onload=function () {
			if (!getCookie('hide')) {
					window.setTimeout(function() {
							$('.simplePopup').load(function(){
							$('#pop1').simplePopup();
							});
							$('#pop1').simplePopup();
							$('#pop1').ready(function () {
									$('.simplePopup, .simplePopupBackground');
									$('.simplePopup, .simplePopupBackground').css('display', 'block');
							});

							$('.pop-close').click(function () {
									close_pop();
							});

							$('.simplePopupClose').click(function () {
									close_pop();
							});
							//change 1000 to 20000 for 20 seconds
					}, 1000);
			}
		}
		// $(window).load(function(){
		// 	$('#pop1').simplePopup();
		// });
		//
		// $('#pop-close').click(function(){
		// 	$('.simplePopupBackground').hide();
		// 	$('.simplePopup').hide();
		// });

	  //   // $('.show1').click(function(){
		// 	// 	$('#pop1').simplePopup();
	  //   // });
		// });
	</script>
	<script type="text/javascript">
		(function($) {
	    	var elem = $('#pop1');
			if (elem.offset()!=undefined) {
				originalY = elem.offset().top;
				// Space between element and top of screen (when scrolling)
				var topMargin = 20;
				// Should probably be set in CSS; but here just for emphasis
				elem.css('position', 'relative');
				// add scroll event
				$(window).on('scroll', function(event) {
					var scrollTop = $(window).scrollTop();
					elem.stop(false, false).animate({
						top: scrollTop < originalY
								? 0
								: scrollTop - originalY + topMargin
					}, 300);
				});
			}
		})(jQuery);
	</script>

<script src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.js"></script>
	<script>
$(document).ready( function() {
	$('.disabled').click(function(e){
		e.preventDefault();
	});

	$('.slider1').bxSlider({
	mode: 'fade',
	auto: true,
	pager: false,
	pause: 3000
	});

	$('.slider2').bxSlider({
	auto: true,
	minSlides: 4,
	maxSlides: 4,
	pager: false,
	slideWidth: 170,
	slideMargin: 10
	});

	$('.slider3').bxSlider({
		auto: true,
		pager: false,
});

  // Handler for .ready() called.
	$(function(){
		$('[data-inprogressdate]').each(function() {
			var $this = $(this), finalDate = $(this).data('enddate');
			var today = new Date();

			$this.countdown(finalDate)
			.on('update.countdown', function(e){
				$(this).html('End at : ' + e.strftime('%D days %H:%M:%S'));
			})
			.on('finish.countdown', function(e){
				$this.html('Expired');
			})
		});
	});

	$(function(){
		$('[data-goingtodate]').each(function() {
			var $this = $(this), finalDate = $(this).data('goingtodate');
			var today = new Date();

			$this.countdown(finalDate)
			.on('update.countdown', function(e){
				$(this).html('Start at : ' + e.strftime('%D days %H:%M:%S'));
			})
			.on('finish.countdown', function(e){
				// $this.html('Expired');
			})
		});
	});

  });
</script>

	<script type="text/javascript">
	if(screen.width==600)
		document.getElementById('side-menu').className = 'span3 side-men';
	</script>

</body>
</html>
