<?php header("Access-Control-Allow-Origin: *"); ?>
<body style="display:none;">

<div style="clear:both"></div>
<h1>OneCheckout - Payment Page - Tester</h1>
<form action="{{$URL}}" id="MerchatPaymentPage" name="MerchatPaymentPage" method="post" >
<table width="600" cellspacing="1" cellpadding="5" border="0">
  <tbody><tr>
    <td class="field_label" width="100">BASKET</td>
    <td class="field_input" width="500"><input name="BASKET" id="BASKET" value="{{$BASKET}}" size="100" type="text"></td>
  </tr>
  <tr>
    <td class="field_label" width="100">MALLID</td>
    <td class="field_input" width="500"><input name="MALLID" id="MALLID" value="{{$MALLID}}" size="12" type="text"> --&gt; Mall ID OCO</td>
  </tr>
  <tr>
    <td class="field_label" width="100">CHAINMERCHANT</td>
    <td class="field_input" width="500"><input name="CHAINMERCHANT" id="CHAINMERCHANT" value="{{$CHAINMERCHANT}}" size="12" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">CURRENCY</td>
    <td class="field_input"><input name="CURRENCY" id="CURRENCY" value="{{$CURRENCY}}" size="3" maxlength="3" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">PURCHASECURRENCY</td>
    <td class="field_input"><input name="PURCHASECURRENCY" id="PURCHASECURRENCY" value="{{$PURCHASECURRENCY}}" size="3" maxlength="3" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">AMOUNT</td>
    <td class="field_input"><input name="AMOUNT" id="AMOUNT" value="{{$AMOUNT}}" size="12" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">PURCHASEAMOUNT</td>
    <td class="field_input"><input name="PURCHASEAMOUNT" id="PURCHASEAMOUNT" value="{{$PURCHASEAMOUNT}}" size="12" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">TRANSIDMERCHANT</td>
    <td class="field_input"><input name="TRANSIDMERCHANT" id="TRANSIDMERCHANT" size="16" value="{{$TRANSIDMERCHANT}}" type="text"></td>
  </tr>
    <tr>
    <td class="field_label">PAYMENTCHANNEL</td>
    <td class="field_input"><input name="PAYMENTCHANNEL" id="PAYMENTCHANNEL" size="16" value="{{$PAYMENTCHANNEL}}" type="text">
    </td>

  </tr>

    <tr><td class="hidden"><input name="SHAREDKEY" id="SHAREDKEY" value="{{$SHAREDKEY}}" size="15" maxlength="12" type="hidden"></td>

  </tr><tr>
    <td class="field_label">WORDS</td>
    <td class="field_input"><input id="WORDS" name="WORDS" size="60" value="{{$WORDS}}" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">REQUESTDATETIME</td>
    <td class="field_input"><input name="REQUESTDATETIME" id="REQUESTDATETIME" size="14" maxlength="14" value="{{$REQUESTDATETIME}}" type="text">
      (YYYYMMDDHHMMSS)</td>
  </tr>

  <tr>
    <td class="field_label">SESSIONID</td>
    <td class="field_input"><input id="SESSIONID" name="SESSIONID" value="{{$SESSIONID}}" type="text"></td>
  </tr>
  <tr>
    <td class="field_label" width="100">EMAIL</td>
    <td class="field_input" width="500"><input name="EMAIL" id="EMAIL" value="{{$EMAIL}}" size="12" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">NAME</td>
    <td class="field_input"><input name="NAME" id="NAME" value="{{$NAME}}" size="30" maxlength="50" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">ADDRESS</td>
    <td class="field_input"><input name="ADDRESS" id="ADDRESS" value="{{$ADDRESS}}" size="50" maxlength="50" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">COUNTRY</td>
    <td class="field_input"><input name="COUNTRY" id="COUNTRY" value="{{$COUNTRY}}" size="50" maxlength="50" type="text"></td>
  </tr>
  <tr>
    <td class="field_label">MOBILEPHONE</td>
    <td class="field_input"><input name="MOBILEPHONE" id="MOBILEPHONE" value="{{$MOBILEPHONE}}" size="12" maxlength="20" type="text"></td>
  </tr>
  <tr>
  	<td class="field_input" colspan="2">&nbsp;</td>
  </tr>

</tbody></table><br><br>
</form>
<script type="text/javascript">
    document.getElementById("MerchatPaymentPage").submit();
</script>


</body>
