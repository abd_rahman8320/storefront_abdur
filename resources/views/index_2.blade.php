<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    *{
      padding: 0;
      margin: 0;
    }
    body{
      width: 80%;
      margin: 20px auto;
    }
      table{
        border-collapse: collapse;
        width: 100%;
      }
      table th, td{
        padding: 10px;
        text-align: center;
      }
      div.average{
        width: 50%;
        margin: 0 auto;
        box-shadow: 3px 3px 5px rgb(167, 166, 164);
        border-radius: 5px;
        margin-bottom: 20px;
      }
      div.chart{
        margin: 0 auto;
        padding: 10px;
        box-shadow: 3px 3px 8px rgb(167, 166, 164);
        border-radius: 5px;
        width: 100%;
      }
      div.chart table#summary{
        width: 80%;
        margin: 10px auto;
        border: 1px solid grey;
      }
      div.latest_five_order, div.latest_five_term, div.top_five_term{
        width: 50%;
        margin: 20px auto;
        padding: 10px;
        box-shadow: 3px 3px 8px rgb(167, 166, 164);
        border-radius: 5px;
      }
      div.latest_five table tr#title{
        background-color: black;
        color: white;
      }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js" type="text/javascript"></script>
  </head>
  <body>
    <?php
    foreach ($sum_purchases as $sum_purchase) {
      $revenue = $sum_purchase->revenue;
      $tax = $sum_purchase->tax;
    }

    $date = [];
    $qty  = [];
    $qty_total = 0;

    foreach ($get_order_dates as $dates) {
      $date[] = $dates->time_key;
      $qty[] = $dates->qty;
      $qty_total += $dates->qty;
    }

    $date_result= "\"".implode("\",\"", $date)."\"";
    $qty_result= "\"".implode("\",\"", $qty)."\"";
    ?>
    <div class="average">
      <table border="1">
        <tr>
          <th>AVERAGE ORDERS</th>
        </tr>
        <tr>
          <td>{{"\$ ".number_format($revenue/$qty_total, 2)}}</td>
        </tr>
      </table>
    </div>

    <div class="chart" style="width: 50%; height: 350px;">
      <canvas id="avg_order" ></canvas>
      <table id="summary" border="1">
        <tr>
          <td>
            <b>REVENUE</b>
            <p>{{"\$ ".number_format($revenue, 2)}}</p>
          </td>
          <td>
            <b>TAX</b>
            <p>{{"\$ ".number_format($tax, 2)}}</p>
          </td>
          <td>
            <b>SHIPPING</b>
            <p></p>
          </td>
          <td>
            <b>QTY</b>
            <p>{{$qty_total}}</p>
          </td>
        </tr>
      </table>
    </div>

    <div class="latest_five_order">
      <table border="1">
        <legend><b>LAST 5 ORDERS</b></legend>
        <tr id="title">
          <th>Customer</th>
          <th>Qty</th>
          <th>Grand Total</th>
        </tr>
        <?php /*dd($latest_orders);*/for($i=0; $i < sizeof($latest_orders) ; $i++) { ?>
        <tr>
          <td>{{$latest_orders[$i]->cus_name}}</td>
          <td>{{$latest_orders[$i]->order_qty}}</td>
          <td style="text-align:right;">{{"\$ ". number_format($latest_orders[$i]->order_qty * $latest_orders[$i]->order_amt, 2)}}</td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <div class="latest_five_term">
      <table border="1">
        <legend><b>LATEST 5 TERM SEARCH</b></legend>
        <tr id="title">
          <th>Search Term</th>
          <th>Results</th>
          <th>Number of Uses</th>
        </tr>
        <?php for($i=0; $i < sizeof($latest_searches); $i++) { ?>
        <tr>
          <td>{{$latest_searches[$i]->term}}</td>
          <td>{{$latest_searches[$i]->search_result}}</td>
          <td style="text-align:right;">{{$latest_searches[$i]->search_term_uses_count}}</td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <div class="top_five_term">
      <table border="1">
        <legend><b>TOP 5 TERM SEARCH</b></legend>
        <tr id="title">
          <th>Search Term</th>
          <th>Results</th>
          <th>Number of Uses</th>
        </tr>
        <?php for($i=0; $i < sizeof($top_search); $i++) { ?>
        <tr>
          <td>{{$top_search[$i]->term}}</td>
          <td>{{$top_search[$i]->search_result}}</td>
          <td style="text-align:right;">{{$top_search[$i]->search_term_uses_count}}</td>
        </tr>
        <?php } ?>
      </table>
    </div>



    <!-- javascript style DOM -->
    <script type="text/javascript">
    var chart =  document.getElementById('avg_order');
    var lineChart = new Chart(chart, {
      type:'line',
      data : {
                labels: [<?php echo "$date_result";?>],
                datasets: [
                    {
                        label: "Dialy Order",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [<?php echo "$qty_result";?>],
                        spanGaps: false,
                    }
                ]
            }
    });
    </script>
  </body>
</html>
