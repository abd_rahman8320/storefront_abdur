<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->
	<div id="sidebar" class="span3 pull-right">
		<div class="well well-small btn-warning"><strong>Related Promo</strong></div>
		<div class="row">
		<ul class="thumbnails">

					<?php if($get_related_product){ foreach($get_related_product as $product_det){
	$product_image = explode('/**/',$product_det->deal_image);


	$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
			    $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
                $res = base64_encode($product_det->deal_id);
	?>
			<li class="span3">
			  <div class="thumbnail" style="width:100%;">
				 <!-- <i class="tag"><?php //echo round($product_det->deal_discount_percentage);?>%</i>-->



					<a href="<?php echo url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res); ?>"><img alt="" src="<?php echo url('assets/deals/').'/'.$product_image[0];?>" style="height:215px;" ></a>
					<div class="caption product_show">
					<h3 class="prev_text"><?php echo $product_det->deal_title;?></h3>
				<h4 class="top_text dolor_text">{{$get_cur}}  <?php echo $product_det->deal_discount_price;?></h4>

					  <h4><a href="<?php echo url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res); ?>" class="btn align_brn btnb-success"><i class="icon-large icon-shopping-cart icon_me"></i></a></h4>
					</div>
				  </div>
			</li>
            <?php } }else{ ?>
         <li class="span3">
			  <div class="thumbnail">
					No Deal's Available

				  </div>
			</li>
             <?php } ?>




				  </ul>
				  </div>
		<br>
		  <div class="clearfix"></div>
		<br/>

	</div>
<!-- Sidebar end=============================================== -->
	<div class="span9 pull-left tab-land-wid">
 <div class="clearfix"></div>
    <ul class="breadcrumb">
    <?php foreach($promo_product as $promo) { }
  $product_img= $promo->schedp_picture;
  $img_count = count($product_img);
  $date2 = $promo->schedp_end_date;
  $deal_end_year = date('Y',strtotime($date2));
  $deal_end_month = date('m',strtotime($date2));
  $deal_end_date = date('d',strtotime($date2));
  $deal_end_hours = date('H',strtotime($date2));
  $deal_end_minutes = date('i',strtotime($date2));
  $deal_end_seconds = date('s',strtotime($date2));
 ?>
    <li><a href="<?php echo url('index');?>">Home</a> <span class="divider">/</span></li>
    <li><a href="<?php echo url('deals');?>">Promo</a> <span class="divider">/</span></li>

    <li class="active"><?php echo  $promo->schedp_title; ?></li>
    </ul>

	<div class="row">

      <div class="span3 tabland-deal-img">
            	<a id="Zoomer3"  href="{!! url('assets/promo/').'/'.$product_img!!}" class="MagicZoomPlus" rel="selectors-effect: fade; selectors-change: mouseover; " title="<?php echo  $promo->schedp_title; ?>"><img src="{!! url('assets/promo/').'/'.$product_img!!}"/></a> <br/>
            </div>


			<div class="deal-wid left_bor">
				<h3 style="padding: 10px 0 0 10px;"><?php echo $promo->schedp_title; ?></h3>
				<h5 style="padding: 10px 0 0 10px;color:red;border-style:solid;width:20%;margin-left:10px;">Voucher Code: {{$promo->promoc_voucher_code}}
				</h5>
				<div id="countdown" class="countdownHolder span12"></div>

							<?php $date = date('Y-m-d H:i:s');
                             //echo $date;
							foreach ($promo_product as $row) {}
								//echo $row->deal_end_date;

             if($row->schedp_end_date < $date){?>
              </h1>

                <div class="row-fluid" style="border-bottom:1px solid #ddd;">
                <div class="span7">
               <br>
                <p><strike class="srt">{{$get_cur}}<?php echo number_format($promo->promop_original_price); ?></strike>  <span class="bold" style="#026EA5 !important">{{$get_cur}}<?php echo number_format($promo->promop_discount_price); ?></span></p>
                </div>

                {!! Form :: open(array('url' => 'addtocart_deal','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}
                <input type="hidden" name="addtocart_deal_id" value="<?php echo $promo->pro_id; ?>" />
                <input type="hidden" name="addtocart_type" value="deals" />

                <div class="span8 rank" style="margin-bottom:10px">

                <span class="out-of-stock"> Out Of Stock</span>

                </div>
                </form>

<?php } else{ ?>
                <!-- ///////////////////////////////////// -->

                <label><span style="color:green;padding: 10px 0 0 10px;">Start: {{$promo->schedp_start_date}}</span></label>
				<label><span style="color:green;padding: 10px 0 0 10px;">End: {{$promo->schedp_end_date}}</span></label>
                 <span class="ang_text">Minimal Transaction: {{$get_cur}}{{number_format($promo->promoc_minimal_transaction,0,'.',',')}}</span>
                <div class="row-fluid" style="border-bottom:1px solid #ddd;">
					@if($promo->schedp_simple_action == 'free_shipping')
					<div class="span7">
						<br>
						<p><span class="srt" style="#026EA5 !important">Free Shipping Discount: {{$get_cur}}{{number_format($promo->maximum_free_shipping,0,'.',',')}}</span></p>
					</div>
					@endif
					@if($promo->schedp_simple_action == 'by_fixed')
					<div class="span7">
						<br>
						<p><span class="srt" style="#026EA5 !important">Discount: {{$get_cur}}{{number_format($promo->schedp_discount_amount,0,'.',',')}}</span></p>
					</div>
					@endif
					@if($promo->schedp_simple_action == 'by_percent')
					<div class="span7">
						<br>
						<p><span class="srt" style="#026EA5 !important">Discount: {{number_format($promo->schedp_discount_amount,0)}}%</span></p>
					</div>
					@endif
					@if($promo->schedp_simple_action == 'buy_x_get_y')
					<div class="span7">
						<br>
						<p><span class="srt" style="#026EA5 !important">BUY {{$promo->schedp_x}} GET {{$promo->schedp_y}} FREE</span></p>
					</div>
					@endif

                {!! Form :: open(array('url' => 'addtocart_deal','class'=>'form-horizontal qtyFrm','enctype'=>'multipart/form-data')) !!}
                <input type="hidden" name="addtocart_deal_id" value="<?php echo $promo->pro_id; ?>" />
                <input type="hidden" name="addtocart_type" value="deals" />

                <div class="span8 rank" style="margin-bottom:10px">
                <label class="control-label" style="font-size:18px;display:none;">Quantity: </label>
              	<select class="form-control span3 res-qty" name="addtocart_qty" id="addtocart_qty" style="display:none;margin-left: -42px;
    margin-right: 18px;
    margin-bottom: 10px;">

                <?php
				$cont_qty1 = $promo->pro_qty - $promo->pro_no_of_purchase;
				if($cont_qty1 > 10 ) { $cont_qty = 10; } else { $cont_qty = $cont_qty1; }
				for($k=1; $k <= $cont_qty; $k++)
				{
				?>
                	<option value="<?php echo $k; ?>" ><?php echo $k; ?></option>
                  <?php } ?>
                </select>              <span id="addtocart_qty_error" style="color:red;display:none;" ></span>
                <button class="btn btn-large btn-primary  me_btn btnb-success" type="submit" id="add_to_cart_session" style="display:none;"> Add to cart <i class=" icon-shopping-cart"></i></button>



                </div>
                </form>
                </div>
<?php }?>



                    <span class="dolor_text"></span>

			</div>




<div class="span9 pull-left tab-land-wid" style="margin-top:20px;">
<div class="table-responsive">
	<table class="table table-bordered" style="font-size:12px;">
	<thead>
		<tr class="techSpecRow" style="background: #1D84C1;color: white;">
			<th colspan="4">PRODUCT</th>
		</tr>
	</thead>
	<tbody>
		@if($promo->schedp_coupon_type == 2)
			<?php $i = 1; ?>
			@foreach($promo_product as $product)
				<?php $pro_img = explode('/**/', $product->pro_Img); ?>
				@if($i==1)
					<tr>
				@endif
					<td width="25%" style="text-align:center;">
						<img src="{{url('assets/product')}}/{{$pro_img[0]}}" alt="{{$product->pro_title}}" style="height:200px;display:block;margin:auto;width:200px;">
						<h3 class="prev_text">{{$product->pro_title, 0, 20}}</h3>
						<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px;text-align:center;">
							@if($product->pro_disprice != 0)
							<span style="color: red;">{{$get_cur}}{{number_format($product->pro_disprice,0,'.',',')}}</span>
							@else
							<span style="color: red;">{{$get_cur}}{{number_format($product->pro_price,0,'.',',')}}</span>
							@endif
						</p>
						<a href="{{url('productview_spesial')}}/{{base64_encode($product->pro_mc_id)}}/{{base64_encode($product->pro_smc_id)}}/{{base64_encode($product->pro_sb_id)}}/{{base64_encode($product->pro_ssb_id)}}/{{base64_encode($product->pro_id)}}/{{$product->pro_title}}">
							<button type="button" name="button" class="action action--button action--buy">
								<span class="action__text">See Details</span>
							</button>
						</a>
					</td>
				@if($i==4)
					</tr>
					<?php $i = 0; ?>
				@endif
				<?php $i++; ?>
			@endforeach
			@if($i!=1)
				</tr>
			@endif
		@else
		<tr>
			<td><h1>All Products</h1></td>
		</tr>
		@endif
	</tbody>
</table>
</div>


	</div>
</div>
</div> </div>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
	  <script src="themes/js/jquery-1.10.0.min.js"></script>
	{!! $footer !!}

<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="<?php echo url(); ?>/themes/js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo url(); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo url(); ?>/themes/js/google-code-prettify/prettify.js"></script>

	<script src="<?php echo url(); ?>/themes/js/bootshop.js"></script>
    <script src="<?php echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>

      <script type="text/javascript" src="<?php echo url(''); ?>/themes/js/jquery.js"></script>
<script src="<?php echo url(''); ?>/themes/js/magiczoomplus.js" type="text/javascript"></script>
	<!-- Themes switcher section ============================================================================================= -->
<div id="secectionBox">
<link rel="stylesheet" href="<?php echo url(); ?>/themes/switch/themeswitch.css" type="text/css" media="screen" />
<script src="<?php echo url(); ?>/themes/switch/theamswitcher.js" type="text/javascript" charset="utf-8"></script>
<script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" href="<?php echo url(); ?>/themes/css/jquery.countdown.css" />
		<script src="<?php echo url(); ?>/themes/js/jquery.countdown.js"></script>


       <!-- Count Down Coding -->
		<script type="text/javascript">

		year = <?php echo $deal_end_year;?>; month = <?php echo $deal_end_month;?>; day = <?php echo $deal_end_date;?>;hour= <?php echo $deal_end_hours;?>; min= <?php echo $deal_end_minutes;?>; sec= <?php echo $deal_end_seconds;?>;

     <?php $date = date('Y-m-d H:i:s');
	  foreach($promo_product as $store_deal_by_id) {

		 if($date > $store_deal_by_id->schedp_end_date) {
		 $sold_deal_error = 1;
	  	 //$dealdiscount_percentage = $store_deal_by_id->deal_discount_percentage;
		 //$deal_img= explode('/**/', $store_deal_by_id->deal_image);
             $mcat = strtolower(str_replace(' ','-',$store_deal_by_id->mc_name));
             $smcat = strtolower(str_replace(' ','-',$store_deal_by_id->smc_name));
	     $sbcat = strtolower(str_replace(' ','-',$store_deal_by_id->sb_name));
             $ssbcat = strtolower(str_replace(' ','-',$store_deal_by_id->ssb_name));
	     $res = base64_encode($store_deal_by_id->pro_id);
	 ?>






       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
<h4> <a href="{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" class="s_detail"><?php echo substr($store_deal_by_id->pro_title,0,20);  ?></a></h4>
       <?php } ?>





        <?php } }?>

$(function(){
    countProcess();
});


var timezone = new Date()
month        = --month;
dateFuture   = new Date(year,month,day,hour,min,sec);

function countProcess(){

        dateNow = new Date();
        amount  = dateFuture.getTime() - dateNow.getTime()+5;
        delete dateNow;

        /* time is already past */
        if(amount < 0){
                output ="<span class='countDays'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='countDiv countDiv0'></span><span class='countHours'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span><span class='countDiv countDiv1'></span><span class='countMinutes'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span><span class='countDiv countDiv2'></span><span class='countSeconds'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>0</span></span></span>" ;
                $('#countdown').html(output);
                 <?php $date = date('Y-m-d H:i:s');
	  foreach($promo_product as $store_deal_by_id) {

		 if($date > $store_deal_by_id->schedp_end_date) {
		 $sold_deal_error = 1;
	  	 //$dealdiscount_percentage = $store_deal_by_id->deal_discount_percentage;
		 //$deal_img= explode('/**/', $store_deal_by_id->deal_image);
             $mcat = strtolower(str_replace(' ','-',$store_deal_by_id->mc_name));
             $smcat = strtolower(str_replace(' ','-',$store_deal_by_id->smc_name));
	     $sbcat = strtolower(str_replace(' ','-',$store_deal_by_id->sb_name));
             $ssbcat = strtolower(str_replace(' ','-',$store_deal_by_id->ssb_name));
	     $res = base64_encode($store_deal_by_id->pro_id);
	 ?>
                <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
				window.location='{!! url('dealview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}';
				<?php } ?>
				<?php } }?>
        }
        /* date is still good */
        else{
                days=0; hours=0; mins=0; secs=0; output="";

                amount = Math.floor(amount/1000); /* kill the milliseconds */

                days   = Math.floor(amount/86400); /* days */
                amount = amount%86400;

                hours  = Math.floor(amount/3600); /* hours */
                amount = amount%3600;

                mins   = Math.floor(amount/60); /* minutes */
                amount = amount%60;


                secs   = Math.floor(amount); /* seconds */

				fdays = parseInt(days/10);
				sdays = days%10;
				fhours = parseInt(hours/10);
				shours = hours%10;
				fmins = parseInt(mins/10);
				smins = mins%10;
				fsecs = parseInt(secs/10);
				ssecs = secs%10;
                output="<span class='countDays'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>" + fdays +"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>" + sdays +"</span></span><span class='countDiv countDiv0'></span><span class='countHours'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fhours+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+shours+"</span></span></span><span class='countDiv countDiv1'></span><span class='countMinutes'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fmins+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+smins+"</span></span></span><span class='countDiv countDiv2'></span><span class='countSeconds'><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+fsecs+"</span></span><span class='position'><span class='digit static' style='top: 0px; opacity: 1;'>"+ssecs+"</span></span></span>" ;

                $('#countdown').html(output);


                setTimeout("countProcess()", 1000);
        }

}
</script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDoY1OPjAqu1PlQhH3UljYsfw-81bLkI&libraries=places&signed_in=true"></script>
    <script src="<?php echo url(); ?>/assets/js/locationpicker.jquery.js"></script>



</div>
<span id="themesBtn"></span>
</body>
</html>
