<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')

{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End====================================================================== -->
<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->
	<div class="span3" id="sidebar">
		<div class="well well-small btn-warning"><strong>Categories </strong></div>
				<ul id="css3menu1" class="topmenu">
<input type="checkbox" id="css3menu-switcher" class="switchbox"><label onclick="" class="switch" for="css3menu-switcher"></label>
<?php foreach($main_category as $fetch_main_cat) { $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>

<li class="topfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>"><?php echo $fetch_main_cat->mc_name; ?> </a>
<?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0) { ?>
	<ul>
    <?php foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)  { $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>
    <?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0) { ?>
			 <li class="subfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>"><?php echo $fetch_sub_main_cat->smc_name ; ?></a>

		<ul>
                <?php  foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat) { $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id;?>                  <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0) { ?>
					<li class="subsecond"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>"><?php echo  $fetch_sub_cat->sb_name; ?></a>

                    <ul style="display:none;">
                    <?php  foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat) { $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>
                        <li class="subthird"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>"><?php echo $fetch_secsub_cat->ssb_name ?></a></li>
                     <?php } ?>
                      </ul>
                      <?php } ?>
                    </li>
				<?php } ?>
				</ul>
                <?php } ?>
        </li>
        <?php } ?>
	</ul>
    <?php } ?>
    </li>
      <?php } ?>
</ul><br/>
		  <div class="clearfix"></div>
		<br/>
         <div class="well well-small btn-warning"><strong>Specials</strong></div>
          <?php foreach($most_visited_product as $fetch_most_visit_pro) {
			  if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty){
			 $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;
			 $mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);
			 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);
             $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));
             $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));
             $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));
             $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name));
             $res = base64_encode($fetch_most_visit_pro->pro_id);
			  ?>
          <div class="thumbnail" style="width:95%" >
			<img  src="<?php echo url(''); ?>/assets/product/<?php echo $mostproduct_img[0]; ?>" alt="<?php echo $fetch_most_visit_pro->pro_title; ?>"/>
			<div class="caption product_show">
				<h4 class="top_text dolor_text">Rp. <?php if($fetch_most_visit_pro->pro_disprice!=0){ echo number_format($fetch_most_visit_pro->pro_disprice, 2, ',', '.'); }else{ echo number_format($fetch_most_visit_pro->pro_price, 2, ',', '.'); } ?></h4>
					  <h5 class="prev_text"><?php echo substr($fetch_most_visit_pro->pro_title,0,50);  ?>...</h5>
					 <h4><a class="btn btn-warning" href="{!! url('productview_spesial/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$fetch_most_visit_pro->pro_title)!!}"><i class="icon-large icon-shopping-cart icon_me"></i></a>
					 </h4>
					</div>
		  </div><br/>
			<?php } }?>

	</div>
<!-- Sidebar end=============================================== -->
	<div class="span9" id="paymentresult-page">
    <ul class="breadcrumb">
		<li><a href="<?php echo url();?>">Home</a> <span class="divider">/</span></li>
		<li class="active">Payment Result </li>
    </ul>
	<div class="span4" style="margin:0px">

    @if (Session::has('result'))
    <h4>  Selamat! Anda akan menerima pesanan</h5>
    @endif
       @if (Session::has('fail'))
    <h4>  Your Payment Process failed..</h5>
    @endif
	 @if (Session::has('error'))
    <h4>  Your Payment Process  has been stopped Due to Some error...</h5>
    @endif

    </div>


    <div class="clearfix"></div>
    <hr class="soft"/>
      <h5 style="text-align: center;">Informasi Pembayaran</h5>
	  <hr class="soft"/>
		  <h5>Hi ! <?php echo $cus_details[0]->cus_name?></h5>
			<h5>Terima kasih telah berbelanja di Kukuruyuk.</h5>
      <table class="table table-bordered" style="background-color:rgba(138,138, 142, 0.15);">
        <thead>
          <tr>
            <th>Nomor Transaksi</th>
				  </tr>
        </thead>
        <tbody>
          <tr>
              <td><b><?php echo $transaction_header->transaction_id;?><b></td>
        	</tr>
        </tbody>
      </table>
      <!-- <p>Transaksi Anda dengan Nomor Transaksi : "<b><?php //echo $transaction_header->transaction_id;?><b>"</p> -->
      <hr class="soft"/>

      <?php
        if($transaction_header->approval_columbia == 0 && $transaction_header->status_pembayaran == "belum dibayar")
        {
          ?>
            <h5>Transaksi anda sedang diajukan, anda dapat menghubungi CS kami untuk keterangan lebih lanjut di : <?php echo $email_settings[0]->es_phone1;?></h5>

            <hr class="soft"/>
          <?php
        }
        elseif($transaction_header->approval_columbia == 1 )
        {
          ?>
            <h5>Transaksi anda telah disetujui, anda akan di hubungi oleh CS kami. Atau anda bisa menghubungi CS kami di : <?php echo $email_settings[0]->es_phone1;?> </h5>

            <hr class="soft"/>
          <?php
        }
        elseif($transaction_header->status_pembayaran == "expired")
        {
          ?>
            <h5 style="color:red;">Transaksi anda tidak disetujui, anda akan di hubungi oleh CS kami. Atau anda bisa menghubungi CS kami di : <?php echo $email_settings[0]->es_phone1;?></h5>

            <hr class="soft"/>
          <?php
        }
        else
        {

        }
      ?>
      <?php
        if($transaction_header->status_pembayaran == "sudah dibayar" )
        {
          ?>
            <a style="float: right;" href="<?php echo url('download_invoice/'.$transaction_header->transaction_id);?>" class="btn btn-info btn-sm btn-grad" target="_self">Download Invoice</a>
          <?php
        }
      ?>
    <hr style="margin-top: 60px;" class="soft"/>

  <h5> Rincian Pemesanan Anda </h5>
  <!-- Start Tabel list product -->
  <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Product Quantity</th>
                  <th>Amount</th>
  				        <th>Shipping</th>
                  <th>Extended Warranty</th>
				        </tr>
              </thead>
              <tbody>
				<?php

        $total_belanja = 0;
        if($transaction_detail)
				{
				  foreach($transaction_detail as $orderdet)
          {
  					if($orderdet->order_type == 1)
  					{
  						$shipamt = $orderdet->pro_shippamt;
  						$taxamount=($orderdet->order_amt*$orderdet->pro_inctax)/100;
  					}
  					else
  					{
  						$shipamt = 0;
  						$taxamount=0;
  					}

					if($orderdet->pro_title != 'Jasa Pengiriman' && $orderdet->pro_title != 'Biaya Pengiriman'){
  					 ?>

  				      <tr>
                 	  <td><?php echo $orderdet->pro_title  ;?></td>
                    <?php
                      if($orderdet->pro_title == "Biaya Pengiriman" || $orderdet->pro_title == "Diskon" || $orderdet->pro_title == "Biaya Pengiriman")
                      {
                        ?>
                          <td></td>
                        <?php
                      }
                      else
                      {
                        ?>
                          <td><?php echo number_format(floatval($orderdet->order_qty),0,",",".");?> </td>
                        <?php
                      }
                    ?>
					<?php
					if($orderdet->pro_title == 'Diskon'){
					?>
						<td style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format(floatval($getdetailtransaksi[0]->diskon),0,".",",");?></td>
					<?php
					}else{
					?>
                    	<td style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format(floatval($orderdet->order_amt),0,".",",");?></td>
					<?php
					}
					?>
					<?php if($orderdet->pro_title == "Jasa Pengiriman" || $orderdet->pro_title == "Diskon" || $orderdet->pro_title == "Biaya Pengiriman")
					{
					  // dd($orderdet);
					  ?>
						<td></td>

					  <?php
				  }else { ?>
				  	<td style="text-align: right;"><span style="float:left">Rp </span>{{number_format(floatval($orderdet->order_shipping_price))}}</td>
					<?php
				  }
					?>

          <?php if($orderdet->pro_title == "Jasa Pengiriman" || $orderdet->pro_title == "Diskon" || $orderdet->pro_title == "Biaya Pengiriman")
          {
            // dd($orderdet);
            ?>
            <td></td>

            <?php
          }else { ?>
            <td style="text-align: right;"><span style="float:left">Rp </span>{{number_format(floatval($orderdet->order_jumlah_warranty))}}</td>
          <?php
          }
          ?>

					 </tr>
  				<?php

          $total_belanja = $total_belanja + floatval($orderdet->order_amt + $shipamt + $taxamount) + floatval($orderdet->order_jumlah_warranty);
	  		}
          }


				}
        ?>
            <tr>
              <td>Poin</td>
              <td></td>
              <td style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format($transaction_header->poin_digunakan,0,".",",")?></td>
      	      <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td><b>Jumlah</b></td>
              <td colspan="3" style="text-align: right;"><b><span style="float:left">Rp</span><?php echo number_format((floatval($total_belanja)-($transaction_header->poin_digunakan)+$getdetailtransaksi[0]->shipping),0,".",",");?></b></td>
            </tr>


                	</tbody>
            </table>
            </div>
            <!-- End tabel list product -->

  <hr class="soft"/>
  <h5> Rincian Pembayaran Anda Melalui Cicilan Columbia</h5>
  <!-- Start tabel pembayaran kolumbia -->
  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th style="text-align: center;">Total Belanja Anda</th>
          <th style="text-align: center;">Biaya Admin</th>
          <th style="text-align: center;">DP (Down Payment)</th>
          <th style="text-align: center;">Lama Cicilan</th>
          <th style="text-align: center;">Cicilan Perbulan</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <th style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format((floatval($transaction_header->total_belanja)+floatval($transaction_header->shipping)-floatval($transaction_header->diskon)),0,".",",");?></th>
          <th style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format(floatval($transaction_header->adminfee),0,".",",");?></th>
          <th style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format(floatval($transaction_header->transaksi_dp),0,".",",");?></th>
          <th style="text-align: center;"><?php echo $transaction_header->lama_cicilan;?></th>
          <th style="text-align: right;"><span style="float:left">Rp</span><?php echo number_format(floatval($transaction_header->cicilan_perbulan),0,".",",");?></th>
        </tr>
      </tbody>

    </table>
  </div>
  <!-- End tabel pembayaran kolumbia -->
  <hr class="soft"/>
  <h5> Total Pembayaran yang harus di bayar adalah : <?php echo "Rp ". number_format(floatval($transaction_header->transaksi_dp),2,",",".");?> + <?php echo "Rp ". number_format(floatval($transaction_header->adminfee),2,",",".");?> = <?php echo "Rp ". number_format(floatval(floatval($transaction_header->adminfee)+floatval($transaction_header->transaksi_dp)),2,",",".");?></h5>
  <hr class="soft"/>
  <legend></legend>
    <?php
// kondisi belum dibayar
if($transaction_header->status_pembayaran == "belum dibayar" || $transaction_header->status_pembayaran == "expired")
{

    ?>
        <b>
          <p>1. Gunakan ATM Bank / setor tunai untuk transfer ke rekening KUKURUYUK berikut :</p>
        </b>
  <legend></legend>
  <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nama Bank</th>
            <th>Nomor Rekening</th>
            <th>Atas Nama</th>
          </tr>
        </thead>

        <tbody>


        <?php
        if($get_pay_settings_new)
        {
          foreach($get_pay_settings_new as $pay_setting_new)
          {
             ?>
                <tr>
                    <td><?php echo $pay_setting_new->nama_bank;?></td>
                    <td><?php echo $pay_setting_new->nomor_rekening;?></td>
                    <td><?php echo $pay_setting_new->nama_pemilik_rekening;?></td>
               </tr>
          <?php
          }
        }
      ?>
          </tbody>
        </table>
      </div>
      <legend></legend>
      <b>
        <p>2. Silahkan upload bukti transfer anda :</p>
      </b>

      <?php
        // status tidak Expired
        if($transaction_header->status_pembayaran != "expired")
        {
          // status belum upload bukti transfer
          if($transaction_header->bukti_transfer == "")
          {
            ?>

            {!! Form::open(array('url'=>'upload_bukti_transfer_bank_file','class'=>'form-horizontal loginFrm','enctype'=>'multipart/form-data')) !!}
              <div class="form-group">
                <div class="col-lg-8">

                  <input type="file" name="upload_bukti" id="upload_bukti">
                  <input type="hidden" name="transaksi_id" id="transaksi_id" value="<?php echo $orderdet->transaction_id;?>">

                  <button type="submit" id="place_order_submit" class="btn btn-warning btn-sm btn-grad" style="color:#fff">Upload</button>
                </div>
                </form>
              </div>
              <b>
        <p>3. Setelah Anda melakukan pembayaran, proses cicilan akan melalui proses peninjauan oleh tim Columbia Cash & Credit.
</p>
      </b>
            <b>
        <p>4. Bila pengajuan cicilan disetujui, Anda akan dihubungi langsung oleh tim Columbia Cash & Credit.</p>
      </b>
      <b>
        <p>5. Bila pengajuan cicilan tidak disetujui, maka Kukuruyuk akan memproses pengembalian dana ke rekening Anda.</p>
      </b>
            <?php
          }
          else
          {
            ?>

            <div class="form-group">
              <div class="col-lg-6">
                <div class="col-lg-3">
                  <a href="<?php echo url('download_file_bukti/'.$transaction_header->bukti_transfer);?>" class="btn btn-info btn-sm btn-grad" target="_self">Download</a>

                  <a href="<?php echo url('hapus_file_bukti/'.$transaction_header->transaction_id);?>" class="btn btn-warning btn-sm btn-grad" target="_self">Hapus</a>
                </div>
                </div>
              </div></br>
              <b>
        <p>3. Setelah Anda melakukan pembayaran, proses cicilan akan melalui proses peninjauan oleh tim Columbia Cash & Credit.
</p>
      </b>
            <b>
        <p>4. Bila pengajuan cicilan disetujui, Anda akan dihubungi langsung oleh tim Columbia Cash & Credit.</p>
      </b>
      <b>
        <p>5. Bila pengajuan cicilan tidak disetujui, maka Kukuruyuk akan memproses pengembalian dana ke rekening Anda.</p>
      </b>
            <?php
          }
        }
        else
        {
          ?>
            <div class="form-group">
              <div class="col-lg-6">
                <div class="col-lg-3">
                  <b>Transaksi Anda Sudah Expired</b>
                </div>
              </div>
            </div>
          <?php
        }
      }
      // tutup if transaksi belum dibayar
      // kondisi sudah di bayar
      else
      {
        ?>

        <hr class="soft"/>
          <b>
            <p style="text-align: center;">   History Transaksi</p>
          </b>
        <hr class="soft"/>

        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td style="font-weight: bold; text-align: center;">Nama Product</td>
                <td style="font-weight: bold; text-align: center;">Jumlah Barang</td>
                <td style="font-weight: bold; text-align: center;">Pesanan Dibuat</td>
                <td style="font-weight: bold; text-align: center;">Pesanan Dibayarkan</td>
                <td style="font-weight: bold; text-align: center;">Pesanan Diproses</td>
                <td style="font-weight: bold; text-align: center;">Pesanan Dikemas</td>
                <td style="font-weight: bold; text-align: center;">Pesanan Dikirim</td>
                <td style="font-weight: bold; text-align: center;">Metode Pengiriman</td>
                <td style="font-weight: bold; text-align: center;">Nomor Resi Pengiriman</td>
                <td style="font-weight: bold; text-align: center;">Konfirmasi Barang</td>
                <td style="font-weight: bold; text-align: center;">Pesanan Diterima</td>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($get_history_transaksi as $history)
                {
                  $status_outbound;

                  if($history->status_outbound == 2){
                    $status_outbound = 'Pick';
                  }
                  else if($history->status_outbound == 3){
                    $status_outbound = 'Packing';
                  }
                  else if($history->status_outbound == 4){
                    $status_outbound = 'Shipping';
                  }
                  else if($history->status_outbound == 5){
                    $status_outbound = 'Recieving';
                  }

                   ?>
                      <tr>
                          <td style="text-align: center; "><?php echo $history->pro_title;?></td>
                          <td style="text-align: center; "><?php echo $history->order_qty;?></td>
                          <td style="text-align: center; "><?php echo $history->order_date;?></td>
                          <?php
                            if($history->order_tgl_pesanan_dibayarkan != "0000-00-00 00:00:00")
                            {
                              ?>
                                <td style="text-align: center; "><?php echo $history->order_tgl_pesanan_dibayarkan;?></td>
                              <?php
                            }
                            else
                            {
                              ?>
                                <td style="text-align: center; "> - </td>
                              <?php
                            }
                          ?>
                          <?php
                            if($history->order_tgl_pesanan_diproses != "0000-00-00 00:00:00")
                            {
                              ?>
                                <td style="text-align: center; "><?php echo $history->order_tgl_pesanan_diproses;?></td>
                              <?php
                            }
                            else
                            {
                              ?>
                                <td style="text-align: center; "> - </td>
                              <?php
                            }
                          ?>
                          <?php
                            if($history->order_tgl_pesanan_dikemas != "0000-00-00 00:00:00")
                            {
                              ?>
                                <td style="text-align: center; "><?php echo $history->order_tgl_pesanan_dikemas;?></td>
                              <?php
                            }
                            else
                            {
                              ?>
                                <td style="text-align: center; "> - </td>
                              <?php
                            }
                          ?>
                          <?php
                            if($history->order_tgl_pesanan_dikirim != "0000-00-00 00:00:00")
                            {
                              ?>
                                <td style="text-align: center; "><?php echo $history->order_tgl_pesanan_dikirim;?></td>
                              <?php
                            }
                            else
                            {
                              ?>
                                <td style="text-align: center; "> - </td>
                              <?php
                            }
                          ?>

                          <td style="text-align: center; "><?php echo $history->postalservice_code;?></td>
                          <td style="text-align: center; "><?php echo $history->order_nomor_resi;?></td>
                          <?php
                            if($history->order_tgl_pesanan_dikirim != "0000-00-00 00:00:00")
                            {
                              ?>
                                  <td class="center     "><a href="<?php echo url('update_status_konfirmasi/'.$history->order_id.'/'.$history->transaction_id);?>"  class="btn btn-info btn-sm btn-grad" target="_self">Terima</a></td>
                              <?php
                            }
                            else
                            {
                              ?>
                                <td class="center     ">-</td>
                              <?php
                            }
                          ?>

                          <?php
                            if($history->order_tgl_konfirmasi_penerimaan_barang != "0000-00-00 00:00:00")
                            {
                              ?>
                                <td style="text-align: center; "><?php echo $history->order_tgl_konfirmasi_penerimaan_barang;?></td>
                              <?php
                            }
                            else
                            {
                              ?>
                                <td style="text-align: center; "> - </td>
                              <?php
                            }
                          ?>
                     </tr>
                <?php
                }
              ?>
            </tbody>
          </table>
        </div>

        <?php

        if($transaction_detail[0]->postalservice_code == 'Ambil Langsung')
          {
            ?>
              <hr class="soft"/>
                <b>
                  <p style="text-align: center;">  Alamat Ambil Langsung</p>
                </b>
              <hr class="soft"/>
                <b>
                  <p style="text-align: center;">  Ruko Grand Mall Bekasi Blok D No 12 Jl. Jend Sudirman Bekasi, Jawa Barat Indonesia 17143</p>
                </b>
              <hr class="soft"/>
            <?php
          }
      }
      ?>
  <hr class="soft"/>
</div>
</div></div>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->

    	{!! $footer !!}
<!-- Placed at the end of the document so the pages load faster ============================================= -->
		<script src="<?php echo url('');?>/themes/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo url('');?>/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo url('');?>/themes/js/google-code-prettify/prettify.js"></script>

	<script src="<?php echo url('');?>/themes/js/bootshop.js"></script>
    <script src="<?php echo url('');?>/themes/js/jquery.lightbox-0.5.js"></script>

    <script src="<?php echo url('');?>/plug-k/demo/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="<?php echo url('');?>/plug-k/js/bootstrap-typeahead.js" type="text/javascript"></script>
    <script src="<?php echo url('');?>/plug-k/demo/js/demo.js" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo url('');?>/themes/js/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="<?php echo url('');?>/themes/js/scriptbreaker-multiple-accordion-1.js"></script>
    <script language="JavaScript">
    function defaultPage(){
        if(screen.width<=1200 && screen.width>=980)
        {
            document.getElementById("paymentresult-page").className = "span8";
        }
    }
    $(document).ready(function() {
        $(".topnav").accordion({
            accordion:true,
            speed: 500,
            closedSign: '<span class="icon-chevron-right"></span>',
            openedSign: '<span class="icon-chevron-down"></span>'
        });
    });

    </script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
	//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
</body>
</html>
