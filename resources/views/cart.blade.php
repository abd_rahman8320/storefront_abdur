<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')

<body style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody">
	<div class="container">
	<div class="row">

<!-- Sidebar ================================================== -->

<!-- Sidebar end=============================================== -->
	<div class="span12">
    <ul class="breadcrumb">
		<li><a href="index">Home</a> <span class="divider">/</span></li>
		<li class="active"> Shopping Cart</li>
    </ul>



    <?php
	if(isset($_SESSION['cart'])){
		$count2 =  count($_SESSION['cart']);
	}else{
		$count2 =  0;
	}



	if(isset($_SESSION['deal_cart'])){
		 $count1 = count($_SESSION['deal_cart']);
	}else{
		$count1 = 0;
		}
		$count = $count1 + $count2;
		?>
		<!-- Koding Lama -->
	<!-- <h3>  Shopping Cart [ <small><?php //echo $count;  ?> Item(s) </small>]<a href="<?php //echo url('index'); ?>" class="btn btn-large pull-right me_btn res-cont1 hidden-phone"><i class="icon-arrow-left"></i> <span style="font-weight:normal;" >Continue Shopping</span> </a> -->

	<h3>  Shopping Cart [ <small><?php echo $count;  ?> Item(s) </small>]<a href="javascript:history.go(-1)" class="btn btn-large pull-right me_btn res-cont1 hidden-phone"><i class="icon-arrow-left"></i> <span style="font-weight:normal;" >Continue Shopping</span> </a></h3>
	<hr class="soft hide-mob"/>

	<?php if($count != 0) { ?>
		 <?php if($session_result != ''){ ?>
 		 <div class="alert alert-danger alert-dismissable"><?php echo $session_result;?>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		<?php } ?>

		@if (Session::has('session_result'))
		<div class="alert alert-danger alert-dismissable">{!! Session::get('session_result') !!}
	   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
	   @endif

    <div class="table-responsive">
	<table class="table table-bordered">
        <thead>
            <tr>
				<th>No.</th>
				<th>Product</th>
				<th>Description</th>
				<th>Color</th>
                <th style="display:none;">Size</th>
                <th>Quantity/Update</th>
				<th>Price</th>
                <th>Total</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$z = 1;
			$overall_total_price=0;

			if($count != 0) {
				if(isset($_SESSION['cart'])){
					$cart_error = 1;
					$max=count($_SESSION['cart']);
					$overall_total_price='';
					$z = 1;

					for($i=0;$i<$max;$i++){
						$pid=$_SESSION['cart'][$i]['productid'];
						//dd(isset($_SESSION['cart'][$i]['flashPromoId']));
						if(isset($_SESSION['cart'][$i]['flashPromoId'])){
							$flashPromoId = $_SESSION['cart'][$i]['flashPromoId'];
						}else{
							$flashPromoId = null;
						}
						$q=$_SESSION['cart'][$i]['qty'];
						$pname="Have to get";
						//dd($result_cart);
						foreach($result_cart[$pid] as $session_cart_result) {
							$product_img=explode('/**/',$session_cart_result->pro_Img);
							if(isset($flashPromoId) && !empty($session_cart_result->promop_discount_price)){
								//dd("hehe");
								//echo "here";
								if(isset($session_cart_result->promop_discount_price) || !empty($session_cart_result->promop_discount_price)){
									$item_total_price = $q * ($session_cart_result->promop_discount_price);
								}else if($session_cart_result->pro_disprice != 0){
									$item_total_price = $q * ($session_cart_result->pro_disprice);
									//dd($item_total_price);
								}else{
									$item_total_price = $q * ($session_cart_result->pro_price);
								}
							}else if (!empty($session_cart_result->wholesale_level1_min)){

								if($q < $session_cart_result->wholesale_level1_min){

									if($session_cart_result->pro_disprice != 0){
										//echo "Haha";
										$item_total_price = $q * ($session_cart_result->pro_disprice);
										}else{
										$item_total_price = $q * ($session_cart_result->pro_price);
									}
								}else{
									if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q):
											$item_total_price = $q * ($session_cart_result->wholesale_level5_price);
											//$overall_total_price += $q * $session_cart_result->wholesale_level5_price;
										elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q):
											$item_total_price = $q * ($session_cart_result->wholesale_level4_price);
											//$overall_total_price += $q * $session_cart_result->wholesale_level4_price;
										elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q):
											$item_total_price = $q * ($session_cart_result->wholesale_level3_price);
											//$overall_total_price += $q * $session_cart_result->wholesale_level3_price;
										elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q):
											$item_total_price = $q * ($session_cart_result->wholesale_level2_price);
											//$overall_total_price += $q * $session_cart_result->wholesale_level2_price;
										elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q):
											$item_total_price = $q * ($session_cart_result->wholesale_level1_price);
											//$overall_total_price += $q * $session_cart_result->wholesale_level1_price;
										//elseif($q < $session_cart_result->wholesale_level1_min):
										endif;
								}

							}else if($session_cart_result->pro_disprice != 0){
									$item_total_price = $q * ($session_cart_result->pro_disprice);
							}else{
									$item_total_price = $q * ($session_cart_result->pro_price);
							}
							$max_qty =  $session_cart_result->pro_qty - $session_cart_result->pro_no_of_purchase; ?>
			<tr>
				<td><?php echo $z;?> </td>
				<td><?php echo $session_cart_result->pro_title; ?></td>
				<td><img width="60" src="<?php echo url('/assets/product/').'/'.$product_img[0]; ?>" alt="<?php echo $session_cart_result->pro_title; ?>"/></td>
				<td><?php echo $session_cart_result->co_name;?> </td>
				<td style="display:none;"><?php echo $size_result[$_SESSION['cart'][$i]['size']];?> </td>
				<td>
					<?php if(empty($_SESSION['cart'][$i]['flashPromoId']) && !isset($_SESSION['cart'][$i]['flashPromoId'])){ ?>
						<div class="input-append">
							<button class="btn" type="button" onClick="minus(<?php echo $z; ?>,<?php echo $pid; ?>)">
								<i class="icon-minus"></i>
							</button>
							<input class="span1" style="max-width:5%;height:20px;" id="pro_qty<?php echo $z; ?>" placeholder=""  size="16" value="<?php echo $q; ?>" type="text">

							<button class="btn" type="button" onClick="add(<?php echo $z; ?>,<?php echo $pid; ?>,<?php echo $max_qty;?>)">
								<i class="icon-plus"></i>
							</button>
						</div>
					<?php }else{ //echo "haha";?>
						<div class="input-append">
							<button class="btn" type="button" onClick="minusFlash(<?php echo $z; ?>,<?php echo $pid; ?>)">
								<i class="icon-minus"></i>
							</button>

							<input class="span1" style="max-width:5%;height:20px;" id="pro_qty<?php echo $z; ?>" placeholder=""  size="16" value="<?php echo $q; ?>" type="text">
							<!--<p>Hahaha</p>-->
							<button class="btn" type="button" onClick="addFlash(<?php echo $z; ?>,<?php echo $pid; ?>,<?php echo $max_qty;?>)">
								<i class="icon-plus"></i>
							</button>
						</div>
					<?php }?>
				</td>
					<?php
					// dd($q);
					//dd($session_cart_result);
				if(isset($flashPromoId) && !empty($session_cart_result->promop_discount_price)){
					if(isset($session_cart_result->promop_discount_price)){?>
						<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->promop_discount_price)?></b></td>
			<?php 	}else if($session_cart_result->pro_disprice != 0){ ?>
						<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_disprice)?></b></td>
			<?php	}else{ ?>
						<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_price);?></b></td>
			<?php	}?>
					<?php
				}else if (!empty($session_cart_result->wholesale_level1_min)) {
						if($q < $session_cart_result->wholesale_level1_min){
							if($session_cart_result->pro_disprice != 0){ //dd($session_cart_result); //echo "gue disini";?>
							<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_disprice); //dd("gue disini");?></b></td>
						<?php }else{ ?>
							<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_price);?></b></td>
						<?php 	} ?>
						<?php }else{
							if($session_cart_result->wholesale_level1_min > 1){
						?>
							<?php if ($session_cart_result->wholesale_level5_min>1 && $session_cart_result->wholesale_level5_min<=$q): ?>
								<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level5_price);?></b></td>
							<?php elseif ($session_cart_result->wholesale_level4_min>1 && $session_cart_result->wholesale_level4_min<=$q): ?>
								<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level4_price);?></b></td>
							<?php elseif ($session_cart_result->wholesale_level3_min>1 && $session_cart_result->wholesale_level3_min<=$q): ?>
								<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level3_price);?></b></td>
							<?php elseif ($session_cart_result->wholesale_level2_min>1 && $session_cart_result->wholesale_level2_min<=$q): ?>
								<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level2_price);?></b></td>
							<?php elseif ($session_cart_result->wholesale_level1_min>1 && $session_cart_result->wholesale_level1_min<=$q): ?>
								<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->wholesale_level1_price);?></b></td>
							<?php endif; ?>
						<?php }
						} ?>
					<?php
				}else if($session_cart_result->pro_disprice != 0){ ?>
						<td><b>{{$get_cur}}<?php echo number_format($session_cart_result->pro_disprice);?></b></td>
					<?php
				}else { ?>
						<td><b>{{$get_cur}}<?php  echo number_format($session_cart_result->pro_price);?></b></td>
					<?php
				} ?>
				<td><b>{{$get_cur}}<?php   echo number_format($item_total_price);?></b></td>
					<?php
				if(empty($flashPromoId) && !isset($flashPromoId)){ ?>
					<td><button class="btn btn-danger" type="button" onClick="del(<?php echo $pid;?>)"><i class="icon-remove icon-white"></i></button></td>
					<?php }else{?>
					<td><button class="btn btn-danger" type="button" onClick="delFlash(<?php echo $pid;?>)"><i class="icon-remove icon-white"></i></button></td>
					<?php
				}?>
			</tr>

		<?php
						$z++;
					}
				}
			}
		?>
				<?php

				$y = 0;

				$overall_total_price1 = 0;
				if(isset($_SESSION['deal_cart'])){
				$dealcart_error = 1;
				$max=count($_SESSION['deal_cart']);
				$overall_total_price1='';

				for($i=0;$i<$max;$i++){
					$pid=$_SESSION['deal_cart'][$i]['productid'];
					$q=$_SESSION['deal_cart'][$i]['qty'];
					$pname="Have to get";
						foreach($result_cart_deal[$pid] as $session_cart_result) {
						$product_img=explode('/**/',$session_cart_result->pro_Img);
						$item_total_price = ($_SESSION['deal_cart'][$i]['qty']) * ($session_cart_result->promop_discount_price);
						$overall_total_price1 +=$session_cart_result->promop_discount_price * $q;
						$max_deal_qty =  $session_cart_result->pro_qty - $session_cart_result->pro_no_of_purchase;
				?>
                 <tr>
                  <td><?php echo $z;?> </td>
                  <td><?php echo $session_cart_result->pro_title; ?></td>
                  <td><img width="60" src="<?php echo url('/assets/product/').'/'.$product_img[0]; ?>" alt="<?php echo $session_cart_result->pro_title; ?>"/></td>
                  <td>N/A </td>
                  <td>N/A </td>
				  <td>
					<div class="input-append"><input class="span1" style="max-width:5%;height:5%;" id="pro_qty<?php echo $z; ?>" placeholder=""  size="16" value="<?php echo $q; ?>" type="text"><button class="btn" type="button" onClick="minus_dealcart(<?php echo $z; ?>,<?php echo $pid; ?>)"><i class="icon-minus"></i></button><button class="btn" type="button" onClick="add_dealcart(<?php echo $z; ?>,<?php echo $pid; ?>,<?php echo $max_deal_qty;?>)"><i class="icon-plus"></i></button><button class="btn btn-danger" type="button" onClick="del_dealcart(<?php echo $pid?>)"><i class="icon-remove icon-white"></i></button>				</div>
				  </td>
                  <td>{{$get_cur}}<?php echo $session_cart_result->promop_discount_price;?></td>
                  <td>{{$get_cur}}<?php echo $item_total_price.'.00';?></td>
                </tr>
                <?php $z++;} }
				?>

				 <tr>
                  <td colspan="7" style="text-align:right"><strong>Total =</strong></td>
                  <td colspan="8" align="center"> <strong> $<?php  echo $overall_total_price + $overall_total_price1;?> </strong></td>
                </tr>
                <?php }  } ?>

				</tbody>
            </table>
</div>


			<table class="table table-bordered" style="display:none;">
			 <tr><th style="background: #1D84C1;color:white">ESTIMATE YOUR SHIPPING </th></tr>
				<tr><td>
				<form class="form-horizontal">
				  <div class="control-group"  >
					<label class="control-label" for="inputPost"  style="width:440px;">Check product availability at your location(Post Code/ Zipcode) </label>
					<div class="controls" style="margin-left:458px;">
					  <input type="text" id="estimate_check_val" placeholder="ex: 641041">
					</div>



				  </div>



				  <div class="control-group">



					<div class="controls" style="margin-left:425px;">



					  <button type="button" class="btn" id="estimate_check" style="background:#424542;color:white;text-shadow:none">Verify </button><br>



                      <div id="result_zip_code" style="margin-top:10px;"> </div>



					</div>



				  </div>



				</form>



			  </td>



			  </tr>



            </table>



            <?php } else { ?>



            <table class="table table-bordered">



              <thead>



              <tr><td>



              No Items in cart...



              </td></tr>



              </thead>



              </table>







            <?php } ?>



	<a href="javascript:history.go(-1)" class="btn btn-large me_btn res-cont2"><i class="icon-arrow-left"></i> Continue Shopping </a>



     <?php if(Session::has('customerid')){ ?>
	<?php if($count2 > 0 || $count1 > 0){ ?>  <a href="<?php echo url('checkout');?>" class="btn btn-large pull-right res-proc1" style="background:#424542;color:white;text-shadow:none;" target="_self"> Proceed to Checkout <i class="icon-arrow-right"></i></a> <?php } ?>
     <?php } else { ?>
     <?php if($count2 > 0 || $count1 > 0){  ?>  <a href="#login" role="button" data-toggle="modal" class="btn btn-large pull-right res-proc2" style="background: #424542;
    text-shadow: none;
    border: none;
    color: white;" target="_self"> Proceed to Checkout <i class="icon-arrow-right"></i></a> <?php } ?>
     <?php } ?>
</div>
</div></div>
</div>
<!-- MainBody End ============================= -->



<!-- Footer ================================================================== -->

  <script src="<?php echo url(); ?>/plug-k/demo/js/jquery-1.8.0.min.js" type="text/javascript"></script>

	{!!$footer; !!}

<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="<?php echo url(); ?>/themes/js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo url(); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo url(); ?>/themes/js/google-code-prettify/prettify.js"></script>
    <script src="<?php echo url(); ?>/themes/js/bootshop.js"></script>
    <script src="<?php echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){

			$('#estimate_check').click(function(){
			var estimate_check_val = $('#estimate_check_val').val();

			if(estimate_check_val == "")
			{
				$('#estimate_check_val').css("border-color", "red");
				$('#estimate_check_val').focus();
			}

			else if(isNaN(estimate_check_val))
			{
				$('#estimate_check_val').css("border-color", "red");
				$('#estimate_check_val').focus();
			}

			else
			{
				$('#estimate_check_val').css("border-color", "");
				$('#result_zip_code');

				var passData = 'estimate_check_val='+ estimate_check_val;
				//alert(passData);
				$.ajax( {
						type: 'GET',
						data: passData,
						url: '<?php echo url('check_estimate_zipcode'); ?>',

						success: function(responseText)
						{
							if(responseText != 0 )
							{
								$('#result_zip_code').html('<span style="color:green;margin-top:10px;" ><b>Product can be dispatched at your location in '+responseText+ ' bussiness days</b></span>' );
							}
							else
							{
								$('#result_zip_code').html('<span style="color:red;margin-top:10px;" ><b>Sorry!! No dispatched item for your location</b></span>' );
							}
						}
					});
				}
				return false;
			});


			// $("#searchbox").keyup(function(e){
			//
			// 	var searchbox = $(this).val();
			// 	var dataString = 'searchword='+ searchbox;
			//
			// 	if(searchbox ==''){
			// 		$("#display").html("").hide();
			// 	}else{
			// 		var passData = 'searchword='+ searchbox;
			// 		$.ajax({
			// 					type: 'GET',
			// 					data: passData,
			//
			// 					url: '<?php //echo url('autosearch'); ?>',
			// 					success: function(responseText){
			// 						$("#display").html(responseText).show();
			// 						}
			// 				});
			// 			}
			// 		return false;
			// });
		});
	</script>
	<script>
		function add(sno,pid,max_qty)
		{
			var id = document.getElementById('pro_qty'+sno).value;

			if(id < max_qty)
			{
			document.getElementById('pro_qty'+sno).value = parseInt(id) + 1;
			var new_id = document.getElementById('pro_qty'+sno).value;
			var passData = 'id='+new_id+'&pid='+pid;

			//alert(passData);
			$.ajax({
					type: 'GET',
					data: passData,

					url: '<?php echo url('set_quantity_session_cart'); ?>',

					success: function(responseText){
					//alert(responseText);
					//alert('here');
					location.reload();
						window.location.href = 'addtocart';
						}

				});
				return false;
			}
		}

		function minus(sno,pid)
		{
			var id = document.getElementById('pro_qty'+sno).value;

			if(id <= 10 && id > 0)
			{
			document.getElementById('pro_qty'+sno).value = parseInt(id) - 1;

			var new_id = document.getElementById('pro_qty'+sno).value;
			var passData = 'id='+new_id+'&pid='+pid;

			//alert(passData);

			$.ajax({
					type: 'GET',
					data: passData,

					url: '<?php echo url('set_quantity_session_cart'); ?>',

					success: function(responseText){
					//alert(responseText);
					location.reload();
					window.location.href = 'addtocart';
						}
				});

				return false;
			}
		}

		function addFlash(sno,pid,max_qty)
		{
			var id = document.getElementById('pro_qty'+sno).value;

			if(id < max_qty)
			{
			document.getElementById('pro_qty'+sno).value = parseInt(id) + 1;
			var new_id = document.getElementById('pro_qty'+sno).value;
			var passData = 'id='+new_id+'&pid='+pid;

			//alert(passData);
			$.ajax({
					type: 'GET',
					data: passData,

					url: '<?php echo url('set_quantity_session_cart'); ?>',

					success: function(responseText){
						//alert('there');
						location.reload();
						window.location.href = 'addtocart_flash';
						}

				});
				return false;
			}
		}

		function minusFlash(sno,pid)
		{
			var id = document.getElementById('pro_qty'+sno).value;

			if(id <= 10 && id > 0)
			{
			document.getElementById('pro_qty'+sno).value = parseInt(id) - 1;

			var new_id = document.getElementById('pro_qty'+sno).value;
			var passData = 'id='+new_id+'&pid='+pid;

			//alert(passData);

			$.ajax({
					type: 'GET',
					data: passData,

					url: '<?php echo url('set_quantity_session_cart'); ?>',

					success: function(responseText){
					//alert(responseText);
					//location.reload();
					window.location.href ='{{url("addtocart_flash")}}';
					//window.location.href = 'addtocart_flash';
						}
				});

				return false;
			}
		}

		function del(id)
		{
			var passData = 'id='+id;

				$.ajax( {
					type: 'GET',
					data: passData,
					url: '<?php echo url('remove_session_cart_data'); ?>',

					success: function(responseText){
					//alert('Haha');
					//location.reload();
					window.location.href ='{{url("addtocart")}}';
					//window.location.href = 'addtocart';

											}

				});

				return false;
		}

		function delFlash(id)
		{
			//alert(id);
			var passData = 'id='+id;

				$.ajax( {
					type: 'GET',
					data: passData,
					url: '<?php echo url('remove_session_cart_data'); ?>',

					success: function(responseText){
						//alert("haha Berhasil");
						window.location.href ='{{url("addtocart_flash")}}';
					 	window.location.href = "addtocart_flash";
						 //location.reload();
						//location.reload();
											}

				});

				return false;
		}

		function add_dealcart(sno,pid,max_deal_qty)
		{
			var id = document.getElementById('pro_qty'+sno).value;
			if(id < max_deal_qty)
			{
			document.getElementById('pro_qty'+sno).value = parseInt(id) + 1;
			var new_id = document.getElementById('pro_qty'+sno).value;
			var passData = 'id='+new_id+'&pid='+pid;

			//alert(passData);
			$.ajax({
					type: 'GET',
					data: passData,

					url: '<?php echo url('set_quantity_session_dealcart'); ?>',

					success: function(responseText){

					//alert(responseText);
						window.location.href = 'addtocart';
						}

				});

				return false;
			}
		}

		function minus_dealcart(sno,pid)
		{
			var id = document.getElementById('pro_qty'+sno).value;

			if(id <= 10 && id > 0)
			{
				document.getElementById('pro_qty'+sno).value = parseInt(id) - 1;
				var new_id = document.getElementById('pro_qty'+sno).value;
				var passData = 'id='+new_id+'&pid='+pid;
				//alert(passData);
				$.ajax({
						type: 'GET',
						data: passData,

						url: '<?php echo url('set_quantity_session_dealcart'); ?>',

						success: function(responseText){

						//alert(responseText);
							window.location.href = 'addtocart';
						}
					});
					return false;
			}
		}

		function del_dealcart(id)
		{
			//alert(id);
			var passData = 'id='+id;
				$.ajax({
					type: 'GET',
					data: passData,
					url: '<?php echo url('remove_session_dealcart_data'); ?>',

					success: function(responseText)
					{

						window.location.reload = 'addtocart';

					}
				});
				return false;
		}

	</script>
	<script src="<?php echo url(); ?>/plug-k/js/bootstrap-typeahead.js" type="text/javascript"></script>
    <script src="<?php echo url(); ?>/plug-k/demo/js/demo.js" type="text/javascript"></script>
</body>
</html>
