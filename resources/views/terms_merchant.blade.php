<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body id="product_page" style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody">
	<div class="container">
	<div class="row">
	<?php if($cms_result){ foreach($cms_result as $cms) { } $cms_desc = stripslashes($cms->tr_description); } else { $cms_desc = 'Yet To be Filled';} ?>
	<div class="span12">
    <ul class="breadcrumb">
		<li><a href="<?php echo url('index');?>">Home</a> <span class="divider">/</span></li>
		<li><a href="<?php echo url('termsandconditons');?>">Merchant Terms & Conditions</a> <span class="divider">/</span></li>
    </ul>
	<h3> <?php echo 'Terms & Conditions'; ?></h3>
	<hr class="soft"/>
	<div id="legalNotice">
		<?php echo $cms_desc; ?>
	</div>
    <div id="cekboxs">
      <input type="checkbox" id="cekbox" name="agree" value="yes" class="cekbox1" >   I Agree To The Terms & Condition
    </br>
  </br>
  </div>
    <input type="submit" value="Next" name="submit" class="btn btn-large me_btn btnb-success" style="height:auto;margin-top:1%;">
		<a href="<?php echo url('/')?>" role="button" target="_self" style="height:auto;padding-left:10px;"><button class="wish-but" style="margin-top:-16%">Cancel</button></a>
	</div>
</div>
</div>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
	{!! $footer  !!}
<!-- Placed at the end of the document so the pages load faster ============================================= -->
<script type="text/javascript">
  var button_confirm = $('.btnb-success');
  // var checked = $('#cekbox');
  $("#cekbox").on("click", function(){
      //$('#cekbox').prop('checked',true)
  });
  button_confirm.click(function(){
    if($('#cekbox').is(':checked')) {
      window.location = '<?php echo url('merchant_signup');?>';
    }else{
      alert('it is necessary to agree with terms and conditions');
    }
  });


</script>
	<script src="<?php echo url(); ?>/themes/js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo url(); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo url(); ?>/themes/js/google-code-prettify/prettify.js"></script>

	<script src="<?php echo url(); ?>/themes/js/bootshop.js"></script>
    <script src="<?php echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>
<!--	<script src="<?php //echo url(); ?>/plug-k/demo/js/jquery-1.8.0.min.js" type="text/javascript"></script>-->

<script src='https://code.jquery.com/jquery-1.9.1.min.js'></script>

   <script type="text/javascript" src="<?php echo url(''); ?>/themes/js/jquery.js"></script>

<script src="<?php echo url(''); ?>/themes/js/magiczoomplus.js" type="text/javascript"></script>
<script type="text/javascript">
	function setBarWidth(dataElement, barElement, cssProperty, barPercent) {
  var listData = [];
  $(dataElement).each(function() {
    listData.push($(this).html());
  });
  var listMax = Math.max.apply(Math, listData);
  $(barElement).each(function(index) {
    $(this).css(cssProperty, (listData[index] / listMax) * barPercent + "%");
  });
}
setBarWidth(".style-1 span", ".style-1 em", "width", 100);
setBarWidth(".style-2 span", ".style-2 span", "width", 55);


</script>


</body>
</html>
