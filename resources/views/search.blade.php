<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body id="product_page" style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}

@if (Session::has('error'))
<div  >{!! Session::get('error') !!}</div>
@endif

@if (Session::has('result_success'))
  <div>{!! Session::get('result_success') !!}
  </div>
@endif

@if (Session::has('result_cancel'))
  <div>{!! Session::get('result_cancel') !!}
  </div>
@endif
<!-- Header End====================================================================== -->

<div id="mainBody">
  <div class="container">
    <div class="row">



<div class="span9" style="margin-left:20px;background-color:white;">
    <h4 style="margin-left:10px;">Hasil pencarian untuk "{{$q}}"</h4>
      <div class="view">
        <div class="list" style="margin-left:-5px">
            <section class="grid">
          <!--Buat diclone-->

          <div class="list-item product item_clone hidden" style="style="margin-right:5px!important;width:198px;"">
            <div class="product__info ">
              <!--Img-->
              <div class="img" style="width:100%;">
                <a id='a_clone' href="#">
                  <img class="product__image" src="">
                </a>
              </div>

              <!--Info Product-->
              <div class="block" style="width:100%;">
                <p class="title product__title tab-title"></p>
                <p style="font-size: 16px; font-weight: bold; margin-top: -5px;">
                  <span class="harga_diskon" style="color: red; text-align: bold;"></span>
                </p>
                <p class="disprice_present" style="font-weight: bold;margin-top: -10px;">
                  <strike>
                    <span class="harga_awal" style="color:#aaa; font-size: 13;"></span>
                  </strike>
                  &nbsp&nbsp&nbspDiskon <span class="persen_diskon" style="color: black; font-weight: bold;"></span></p>

                </p>
              </div>
              <!--Buat Tombol-->
              <a class="align_brn">
                <button class="action action--button action--buy">
                  <i class="fa fa-shopping-cart"></i><span class="action__text">Details</span>
                </button>
              </a>
            </div>
          </div>

          <!--Akhir Clone-->
        <?php if($searchTerms) { ?>
          <!--List Product-->
          <?php
              // dd($searchTerms);
            foreach($searchTerms as $fetch_product) {
              if($fetch_product->pro_no_of_purchase < $fetch_product->pro_qty){
                $product_saving_price = $fetch_product->pro_price - $fetch_product->pro_disprice;

                if($fetch_product->pro_disprice==0 || $fetch_product->pro_disprice==null){
                    $product_discount_percentage = 0;
                }else {
                    $product_discount_percentage = round(($product_saving_price/ $fetch_product->pro_price)*100,2);
                }

                $product_img = explode('/**/', $fetch_product->pro_Img);
                //dd($product_img);
                $mcat = strtolower(str_replace(' ','-',$fetch_product->mc_name));
                $smcat = strtolower(str_replace(' ','-',$fetch_product->smc_name));
                $sbcat = strtolower(str_replace(' ','-',$fetch_product->sb_name));
                $ssbcat = strtolower(str_replace(' ','-',$fetch_product->ssb_name));
                $res = base64_encode($fetch_product->pro_id);
          ?>
          <div class="list-item product" style="margin-right:5px!important;width:198px;">
            <div class="product__info">
              <!--Img-->
              <div class="img" style="width:100%;">
                <a href="<?php echo url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res);?>">
                  <img class="product__image" src="<?php echo url('').'/assets/product/'.$product_img[0] ?>">
                </a>
              </div>

              <!--Info Product-->
              <div class="">
                <p class="title product__title tab-title"><?php echo substr($fetch_product->pro_title,0,25);?></p>
                <p style="font-size: 16px; font-weight: bold; margin-top: -5px;">
                  <span style="color: red; text-align: bold;"><?php if($fetch_product->pro_disprice!=0){ echo "Rp ".number_format($fetch_product->pro_disprice,0,".",","); }else{ echo "Rp ".number_format($fetch_product->pro_price,0,".",","); } ?></span>
                </p>
                <?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
                <i class="tag-dis">
                  <div id="dis-val">
                      <?php echo round($product_discount_percentage);?>%
                  </div>
                </i>
                <?php } ?>
                <p class="" style="font-weight: bold;margin-top: -10px;">
                  <strike>
                    <span style="color:#aaa; font-size: 13;"><?php if($fetch_product->pro_disprice!=0){ echo "Rp ". number_format($fetch_product->pro_price,0,".",","); }?></span>
                  </strike>
                </p>

              </div>

              <!--Buat Tombol-->
              <a href="<?php echo url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res);?>">
                <button class="action action--button action--buy">
                  <i class="fa fa-shopping-cart"></i><span class="action__text">Details</span>
                </button>
              </a>
            </div>
          </div>


          <?php
               }
            }//endforeach?>
            </section>
        </div>
      </div>
    <?php } ?>

    <?php if($searchTermss) { ?>
    <?php }?>
</div>
</div>
</div>
</div>



<!-- Footer ================================================================== -->

	{!! $footer !!}

  <script>
  function number_format(number,decimals,dec_point,thousands_sep) {
      number  = number*1;//makes sure `number` is numeric value
      var str = number.toFixed(decimals?decimals:0).toString().split('.');
      var parts = [];
      for ( var i=str[0].length; i>0; i-=3 ) {
          parts.unshift(str[0].substring(Math.max(0,i-3),i));
      }
      str[0] = parts.join(thousands_sep?thousands_sep:',');
      return str.join(dec_point?dec_point:'.');
  }

  var list = $('.grid');
  var list_item_clone = $('.item_clone');
  var link = $('.align_brn');
  list_item_clone.remove();
  list_item_clone.removeClass('hidden');

  //merubah count kategori
  var right_badge = $('.right-badge');
  var badge_clone = $('.badge_clone');
  badge_clone.remove();
  badge_clone.removeClass('hidden');

    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 10000000,
      values: [0, <?php echo 0;?>],
      slide: function( event, ui ) {
        var min_price = ui.values[ 0 ];
        var max_price =  ui.values[ 1 ] ;
        min_price = number_format(min_price,0,'.',',');
        max_price = number_format(max_price,0,'.',',');
        $( "#amount" ).val( "Rp. " + min_price + " - Rp. " + max_price);

      },
      stop: function( event, ui ) {
        show_result();
      }
    });

    $( "#amount" ).val( "Rp. " + number_format($("#slider-range").slider("values", 0),0,'.',',') + " - Rp. " + number_format($("#slider-range").slider("values", 1),0,'.',','));


  function show_result(){
    var min_price = $("#slider-range").slider("values", 0);
    var max_price = $("#slider-range").slider("values", 1);
    var grade = [];
    var spc_value = [];
    for (var i = 0; i < 1; i++) {
        if ($('#filter_checkbox_grade'+i).is(':checked')) {
            grade.push($("#filter_checkbox_grade"+i).val());
        }
    }
    for (var j = 0; j < 1; j++) {
        if ($('#filter_checkbox_spec'+j).is(':checked')) {
            spc_value.push($("#filter_checkbox_spec"+j).val());
        }
    }
    var param = {
      pro_title: '<?php echo $q;?>',
      min_price: min_price,
      max_price: max_price,
      mc_id : '<?php echo $mc_id;?>',
      smc_id : '<?php echo $smc_id;?>',
      sb_id :'<?php echo $sb_id;?>',
      ssb_id : '<?php echo $ssb_id;?>',
      grade : grade,
      spc_value : spc_value,
    };
    console.log(param);
    $.ajax({
        type: 'get',
        data: param,
        url: '{{url("search_filter")}}',
        success: function(respon){
            console.log(respon);
            list.empty();
            $('#spc_result').empty();
            // $('#table_grade').empty();
            // right_badge.empty();
            // badge_clone.html(respon.total);
            // right_badge.append(badge_clone);
            // var spc_result = Object.keys(respon.spc_result);
            // var count_spc_result = spc_result.length;
            var i = 0;
            var l = 0;
            for(var key in respon.spc_result){
                $("#spc_result").append('<div id="list5" class="div'+i+'" style="margin-top:20px; "></div>');
                $(".div"+i).append('<div id="div_div'+i+'"></div>');
                $("#div_div"+i).append('<hr><p style="font-size: 15px; font-weight: bold;">'+key+'</p>');
                $("#div_div"+i).append('<div style="width:100%;" id="div_div_div'+i+'"></div>');
                $("#div_div_div"+i).append('<table style="width:100%" id="table'+i+'"></table>');
                for (var j = 0; j < respon.spc_result[key].length; j++) {
                    // console.log(respon.spc_result[key]);
                    var yes = 0;
                    for (var k = 0; k < spc_value.length; k++) {
                        if (spc_value[k] == respon.spc_result[key][j].spc_value) {
                            $("#table"+i).append('<tr><td style="width:80%;"><label><input checked id="filter_checkbox_spec'+l+'" type="checkbox" value="'+respon.spc_result[key][j].spc_value+'" onchange="show_result()">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.spc_result[key][j].spc_value+'</span></label></td><td style="display:none;"><span class="badge">'+respon.spc_result[key][j].spc_value+'</span></td></tr>');
                            yes = 1;
                            break;
                        }
                    }
                    if (yes != 1) {
                        $("#table"+i).append('<tr><td style="width:80%;"><label><input id="filter_checkbox_spec'+l+'" type="checkbox" value="'+respon.spc_result[key][j].spc_value+'" onchange="show_result()">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.spc_result[key][j].spc_value+'</span></label></td><td style="display:none;"><span class="badge">'+respon.spc_result[key][j].spc_value+'</span></td></tr>');
                    }
                    l++;
                }
                i++;
            }

            // for (i = 0; i < respon.grade_result.length; i++) {
            //     var yes = 0;
            //     for (var j = 0; j < grade.length; j++) {
            //         if (grade[j] == respon.grade_result[j].grade_id) {
            //             $('#table_grade').append('<tr><td style="width:80%;"><label><input checked id="filter_checkbox_grade'+i+'" type="checkbox" value="'+respon.grade_result[i].grade_id+'" onchange="show_result()">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.grade_result[i].grade_name+'</span></label></td></tr>')
            //             yes = 1;
            //             break;
            //         }
            //     }
            //     if (yes != 1) {
            //         $('#table_grade').append('<tr><td style="width:80%;"><label><input id="filter_checkbox_grade'+i+'" type="checkbox" value="'+respon.grade_result[i].grade_id+'" onchange="show_result()">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.grade_result[i].grade_name+'</span></label></td></tr>')
            //     }
            // }

            for(i=0;i<respon.data.length;i++)
            {
              var list_item_clone_2 = list_item_clone.clone();
              var harga_awal = respon.data[i].pro_price;
              var persen_diskon = Math.round(((respon.data[i].pro_price-respon.data[i].pro_disprice)/(respon.data[i].pro_price))*100);
              var img_urls = (respon.data[i].pro_Img).split('/**/');
              var harga_diskon = respon.data[i].pro_disprice;
              var judul_product = respon.data[i].pro_title;
              var pro_id = respon.data[i].pro_id;
              var pro_name = respon.data[i].mc_name;

              var smc_name = respon.data[i].smc_name;

              var sb_name = respon.data[i].sb_name;

              var ssb_name = respon.data[i].ssb_name;

              if (pro_name == null) {
                pro_name = "";
              }
              if (smc_name == null) {
                smc_name = "";
              }
              if (sb_name == null) {
                sb_name = "";
              }
              if (ssb_name == null) {
                ssb_name = "";
              }


              if(harga_diskon == 0){
                  list_item_clone_2.find('.disprice_present').hide();
                  list_item_clone_2.find('.persen_diskon').hide();
                  list_item_clone_2.find('.harga_awal').hide();
                  list_item_clone_2.find('.harga_diskon').html('Rp ' +Number(harga_awal).toLocaleString('id')+ ',00');
              }else {
                  list_item_clone_2.find('.persen_diskon').html(persen_diskon + '%');
                  list_item_clone_2.find('.harga_awal').html('Rp ' + Number(harga_awal).toLocaleString('id') + ',00');
                  list_item_clone_2.find('.harga_diskon').html('Rp ' +Number(harga_diskon).toLocaleString('id')+ ',00');
              }
              list_item_clone_2.find('#a_clone').attr('href', '{{url('')}}/productview'+'/'+pro_name.replace(' ', '-').toLowerCase()+'/'+smc_name.replace(' ', '-').toLowerCase()+'/'+sb_name.replace(' ', '-').toLowerCase()+'/'+ssb_name.replace(' ', '-').toLowerCase()+'/'+btoa(pro_id));
              list_item_clone_2.find('img').attr('src', '{{ url('') }}/assets/product/'+ img_urls[0]);
              list_item_clone_2.find('.product__title').html(judul_product);
              list_item_clone_2.find('.align_brn').attr('href', '{{url('')}}/productview'+'/'+pro_name.replace(' ', '-').toLowerCase()+'/'+smc_name.replace(' ', '-').toLowerCase()+'/'+sb_name.replace(' ', '-').toLowerCase()+'/'+ssb_name.replace(' ', '-').toLowerCase()+'/'+btoa(pro_id));
              //url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)
              // <a href="'. url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res).'">
              list.append(list_item_clone_2);
            }
        },
        error: function(){
            alert('Server Error');
        }
    });
    // $.get('search_filter', param).then(function(respon){
    //   console.log(respon);
    //   list.empty();
    //   right_badge.empty();
    //   badge_clone.html(respon.total);
    //   right_badge.append(badge_clone);
    //
    //   for(i=0;i<respon.data.length;i++)
    //   {
    //     var list_item_clone_2 = list_item_clone.clone();
    //     var harga_awal = respon.data[i].pro_price;
    //     var persen_diskon = Math.round(((respon.data[i].pro_price-respon.data[i].pro_disprice)/(respon.data[i].pro_price))*100);
    //     var img_urls = (respon.data[i].pro_Img).split('/**/');
    //     var harga_diskon = respon.data[i].pro_disprice;
    //     var judul_product = respon.data[i].pro_title;
    //     var pro_id = respon.data[i].pro_id;
    //     var pro_name = respon.data[i].mc_name;
    //
    //     var smc_name = respon.data[i].smc_name;
    //
    //     var sb_name = respon.data[i].sb_name;
    //
    //     var ssb_name = respon.data[i].ssb_name;
    //
    //     if (pro_name == null) {
    //       pro_name = "";
    //     }
    //     if (smc_name == null) {
    //       smc_name = "";
    //     }
    //     if (sb_name == null) {
    //       sb_name = "";
    //     }
    //     if (ssb_name == null) {
    //       ssb_name = "";
    //     }
    //
    //
    //     if(harga_diskon == 0){
    //         list_item_clone_2.find('.disprice_present').hide();
    //         list_item_clone_2.find('.persen_diskon').hide();
    //         list_item_clone_2.find('.harga_awal').hide();
    //         list_item_clone_2.find('.harga_diskon').html('Rp ' +Number(harga_awal).toLocaleString('id')+ ',00');
    //     }else {
    //         list_item_clone_2.find('.persen_diskon').html(persen_diskon + '%');
    //         list_item_clone_2.find('.harga_awal').html('Rp ' + Number(harga_awal).toLocaleString('id') + ',00');
    //         list_item_clone_2.find('.harga_diskon').html('Rp ' +Number(harga_diskon).toLocaleString('id')+ ',00');
    //     }
    //     list_item_clone_2.find('#a_clone').attr('href', '{{url('')}}/productview'+'/'+pro_name.replace(' ', '-').toLowerCase()+'/'+smc_name.replace(' ', '-').toLowerCase()+'/'+sb_name.replace(' ', '-').toLowerCase()+'/'+ssb_name.replace(' ', '-').toLowerCase()+'/'+btoa(pro_id));
    //     list_item_clone_2.find('img').attr('src', '{{ url('') }}/assets/product/'+ img_urls[0]);
    //     list_item_clone_2.find('.product__title').html(judul_product);
    //     list_item_clone_2.find('.align_brn').attr('href', '{{url('')}}/productview'+'/'+pro_name.replace(' ', '-').toLowerCase()+'/'+smc_name.replace(' ', '-').toLowerCase()+'/'+sb_name.replace(' ', '-').toLowerCase()+'/'+ssb_name.replace(' ', '-').toLowerCase()+'/'+btoa(pro_id));
    //     //url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)
    //     // <a href="'. url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res).'">
    //     list.append(list_item_clone_2);
    //   }
    //
    //
    // });
  }
  </script>
</body>
</html>
