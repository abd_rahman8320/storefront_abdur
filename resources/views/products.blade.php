<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body id="product_page" style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody">
	<div class="container">
		@if(Session::has('wish'))
			<p class="alert {!! Session::get('alert-class', 'alert-success') !!}">{!! Session::get('wish') !!}</p>
		@endif
		<div class="row">
			<!-- Sidebar ================================================== -->
			<div class="span3" id="sidebar" style="margin-top:10px;">
				<div class="side-menu-head"><strong>Categories</strong></div>
				<ul id="css3menu1" class="topmenu">
					<input type="checkbox" id="css3menu-switcher" class="switchbox"><label onclick="" class="switch" for="css3menu-switcher"></label>
					<?php
					foreach($main_category as $fetch_main_cat) {
						$pass_cat_id1 = "1,".$fetch_main_cat->mc_id;
						if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0) { ?>
							<li class="topfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>" target="_self"><?php echo $fetch_main_cat->mc_name; ?> </a>

								<ul>
									<?php
									foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat){
										$pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id;

										if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0) { ?>
											<li class="subfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>" target="_self"><?php echo $fetch_sub_main_cat->smc_name ; ?> </a>
												<ul>
													<?php  foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat) { $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id;?>  <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0) { ?>
														<li class="subsecond"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>" target="_self"><?php echo  $fetch_sub_cat->sb_name; ?> </a>
															<ul style="display:none;">
															<?php  foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat) { $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>
																<li class="subthird"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>" target="_self"><?php echo $fetch_secsub_cat->ssb_name ?></a></li>
															<?php } ?>
															</ul>
														</li>
														<?php } ?>
													<?php } ?>
												</ul>

											</li>
								<?php 	} ?>
								<?php } ?>
								</ul>

							</li>

					<?php } ?>
				<?php } ?>
				</ul>
				<br>
				<div class="clearfix"></div>
				<br/>

				<div class="side-menu-head" style="margin-top:10px;"><strong>Filter</strong></div>
				<?php
			      //get range harga
			      $max_price = $search_max_min['max_price'];
			      $min_price = $search_max_min['min_price'];

			      // dd($max_price." - ".$min_price);
			    ?>
			    <div style="margin-top:20px;">
			      <strong style="font-size:15px;">Range Harga</strong>
			      <p>
			        <label for="amount">Price range:</label>
			        <input style="width:100%;" type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
			      </p>

			      <div id="slider-range"></div>
			    </div>
			    <div id="list5" style="margin-top:20px; ">
			      <div>
			          <hr><p style="font-size: 15px; font-weight: bold;">Grade</p>

			          <div style="width:100%;">
			              <table style="width:100%" id="table_grade">
			              <?php $i=1; if($get_grade){ foreach($get_grade as $grade){?>
			                <tr>
			                  <td style="width:80%;">
			                      <label>
			                          <input id="filter_checkbox_grade{{$i}}" type="checkbox" value="{{$grade->grade_id}}" onchange="show_result()">
			                          &nbsp&nbsp&nbsp
			                          <span style="font-size: 14px; font-weight: bold;"><?php echo $grade->grade_name;?></span>
			                      </label>
			                  </td>
			                </tr>
			                <?php $i++; } } ?>
			              </table>
			          </div>

			      </div>

			    </div>
			    <div id="spc_result">
			    <?php $j=1; foreach($spc_result as $key => $value){ ?>
			    <div id="list5" style="margin-top:20px; ">
			      <div>
			          <hr><p style="font-size: 15px; font-weight: bold;"><?php echo $key; ?></p>

			          <div style="width:100%;">
			              <table style="width:100%">
			              <?php foreach($value as $valueitem){?>
			                <tr>
			                  <td style="width:80%;">
			                      <label>
			                          <input id="filter_checkbox_spec{{$j}}" type="checkbox" value="{{$valueitem->spc_value}}" onchange="show_result()">
			                          &nbsp&nbsp&nbsp
			                          <span style="font-size: 14px; font-weight: bold;"><?php echo $valueitem->spc_value;?></span>
			                      </label>
			                  </td>
			                  <td style="display:none;"><span class="badge"><?php echo $valueitem->qty; ?></span></td>
			                </tr>
			                <?php $j++; } ?>
			              </table>
			          </div>

			      </div>

			    </div>
			    <?php } ?>
			    </div>

				<br>
				<div class="clearfix"></div>
				<br/>

				<?php
				if($most_visited_product) { ?>
				<div class="side-menu-head" style="margin-top:10px;"><strong>Most Visited Products</strong></div>
				<?php
					foreach($most_visited_product as $fetch_most_visit_pro) {
						$mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;
						if($fetch_most_visit_pro->pro_price==0 || $fetch_most_visit_pro->pro_price==null){
								$mostproduct_discount_percentage = 0;
								}else{
								$mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);
							}

								$mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);
							if($fetch_most_visit_pro->pro_no_of_purchase < $fetch_most_visit_pro->pro_qty) {
							?>
								<div class="thumbnail" style="width:95%;border:none">
									<img  src="<?php echo url(''); ?>/assets/product/<?php echo $mostproduct_img[0]; ?>" alt="<?php echo $fetch_most_visit_pro->pro_title; ?>"/ style="height:150px">
									<div class="caption product_show">
									<h3 class="prev_text"><?php echo substr($fetch_most_visit_pro->pro_title,0,20);  ?>...</h3>
									<h4 class="top_text dolor_text">{{$get_cur}}<?php if($fetch_most_visit_pro->pro_disprice!=0){ echo number_format($fetch_most_visit_pro->pro_disprice); }else{ echo number_format($fetch_most_visit_pro->pro_price); } ?></h4>

									<?php
									if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty) { ?>
										<h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a>
										<?php
										}else{
										$mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));
										$smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));
										$sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));
										$ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name));
										$res = base64_encode($fetch_most_visit_pro->pro_id);
										?>
										<div class="product-info">
											<div class="product-info-price" style="padding-bottom:10px">
												<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
												<a href="<?php echo url('productview_spesial/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$fetch_most_visit_pro->pro_title); ?>" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
												<?php } ?>
												<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
												<a href="<?php echo url('productview1/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res); ?>" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
												<?php } ?>
												<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
												<a href="<?php echo url('productview2/'.$mcat.'/'.$smcat.'/'.$res); ?>" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
												<?php } ?>
											</div>
										</div>
										<?php
									} ?>
								</div>
			</div>
						<?php
					} } } ?>

		</div>
		<!-- Sidebar end=============================================== -->
				<div id="demo" class="box jplist jplist-grid-view span9 tab-land-wid" style="margin-left:20px;background:white">
					<!-- ios button: show/hide panel -->
					<div class="jplist-ios-button">
						<i class="fa fa-sort"></i>
						More Filters
					</div>

					<!-- panel -->
					<div class="jplist-panel box panel-top">

						<!-- reset button -->
						<button type="button" class="jplist-reset-btn" data-control-type="reset" data-control-name="reset" data-control-action="reset">
							Reset &nbsp;<i class="fa fa-share"></i>
						</button>

						<div class="jplist-drop-down" data-control-type="drop-down" data-control-name="paging" data-control-action="paging"><div class="jplist-dd-panel"> 10 per page </div>
							<ul style="display: none;">
								<li class=""><span data-number="5"> 5 per page </span></li>
								<li class="active"><span data-number="10" data-default="true"> 10 per page </span></li>
								<li><span data-number="15"> 15 per page </span></li>
								<li><span data-number="all"> view all </span></li>
							</ul>
						</div>

						<div class="jplist-drop-down" data-control-type="drop-down" data-control-name="sort" data-control-action="sort"><div class="jplist-dd-panel">Likes asc</div>
							<ul style="display: none;">
								<li class=""><span data-path="default" data-default="true">Sort by</span></li>
								<li class="active"><span data-path=".like" data-order="asc" data-type="number">Price low - high</span></li>
								<li><span data-path=".like" data-order="desc" data-type="number">Price high -low</span></li>
								<li><span data-path=".title" data-order="asc" data-type="text">Title A-Z</span></li>
								<li><span data-path=".title" data-order="desc" data-type="text">Title Z-A</span></li>
							</ul>
						</div>

						<!-- filter by title -->
						<div class="text-filter-box">
							<i class="fa fa-search  jplist-icon"></i>
							<input data-path=".title" value="" placeholder="Filter by Title" data-control-type="textbox" data-control-name="title-filter" data-control-action="filter" type="text" class="filt">
						</div>

						<!-- filter by description -->


						<!-- views -->
						<div class="jplist-views" data-control-type="views" data-control-name="views" data-control-action="views" data-default="jplist-grid-view" style="visibility:hidden;">
							<button type="button" class="jplist-view jplist-list-view" data-type="jplist-list-view"></button>
							<button type="button" class="jplist-view jplist-grid-view" data-type="jplist-grid-view"></button>
							<button type="button" class="jplist-view jplist-thumbs-view" data-type="jplist-thumbs-view"></button>
						</div>

						<!-- pagination results -->
						<div class="jplist-label" data-type="Page {current} of {pages}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging" style="visibility:hidden;"></div>

						<!-- pagination control -->
						<!-- pagination results -->
					</div>
					<!-- Compare basket -->
					<div class="compare-basket">
						<button class="action action--button action--compare"><i class="fa fa-check"></i><span class="action__text">Compare</span></button>
					</div>
		<div class="view">
			<div class="list" style="margin-left:-5px">
				
				<?php
			if(count($product_details) != 0){
				// if(isset($get_schedule_flash)){
				// 	$product_details = $get_schedule_flash;
				// }

				//dd($product_details);
				foreach($product_details as $product_det){
					date_default_timezone_set('Asia/Jakarta');
					$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
					$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
					$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
					$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
					$product_title = str_replace('', '-', $product_det->pro_title);
					$res = base64_encode($product_det->pro_id);
					$product_image = explode('/**/',$product_det->pro_Img);
					$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

					if($product_det->pro_price==0 || $product_det->pro_price==null)
					{
						$product_discount_percentage = 0;
					}else {
						$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
					}
					if($product_det->pro_no_of_purchase < $product_det->pro_qty) {
						?>
					<!-- <section class="grid"> -->
						<!-- item 1 -->
						<div class="list-item product" style="margin-right:5px!important;width:198px;">
							<!-- img -->
							<div class="product__info">
								<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
								<div class="img">
									<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product_title!!}" target="_self"><img class="product__image" src="<?php echo url('assets/product/').'/'.$product_image[0];?>" alt="" title=""/></a>
								<?php } ?>
								<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
								<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product_title!!}" target="_self"><img class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
								<?php } ?>
								<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
								<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product_title !!}" target="_self"><img class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
								<?php
									}
									if(floatval($product_det->pro_disprice) != 0 )
									{
										$total_diskon = ceil(100 - (floatval($product_det->pro_disprice)/floatval($product_det->pro_price) * 100));
									}

								?>
								</div>
							<!-- data -->
							<div class="block">

								<p class="title product__title tab-title"><?php echo substr($product_det->pro_title,0,25); ?></p>
								<?php
									if(floatval($product_det->pro_disprice) != 0 )
									{
										?>
										<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px;">	<span  style="color: red; text-align: bold;">{{$get_cur}}<?php echo number_format($product_det->pro_disprice);?></span>
										</p>
										<p style="display:none;" class="like tab-like">{{$product_det->pro_disprice}}</p>
										<?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
										<i class="tag-dis-prod">
											<div id="dis-val">
												<?php echo round($product_discount_percentage);?>%
											</div>
										</i>
										<?php } ?>
										<p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color:#aaa; font-size: 13;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></span></strike>

										<?php
									}
									else
									{
										?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px;margin-bottom:14.5%;">	<span  style="color: red; text-align: bold;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></span></p>
											<p style="display:none;" class="like tab-like">{{$product_det->pro_price}}</p>
										<?php
									}
								?>

								<?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty) { ?>
                                    <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a>
                                    <?php } else { ?></h4>

							</div>
							<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
							<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product_title!!}" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
							<?php } ?>
							<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
							<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product_title !!}" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
							<?php } ?>
							<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
							<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product_title !!}" target="_self"><button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php } ?>
						</div>
						<label class="action action--compare-add"><input class="check-hidden" type="checkbox" /><i class="fa fa-plus"></i><i class="fa fa-check"></i><span class="action__text action__text--invisible">Add to compare</span></label>
						<?php } ?>

						</div>
							<?php
							//if(isset($get_schedule_flash)){
								}
							//}
			}
			}elseif(count($product_details) == 0)
			{
				?>
				<div class="box jplist-no-results text-shadow align-center jplist-hidden">
				<p style="margin-top:20px;color: rgb(54, 160, 222); margin-top: 20px; font-weight: bold; padding-left: 8px;">No products available</p>						</div>
				<?php
			}
							?>



					</div>
				<!-- </section> -->
</div>
<section class="compare">
			<button class="action action--close"><i class="fa fa-remove"></i><span class="action__text action__text--invisible">Close comparison overlay</span></button>
		</section>

		<!--//////////////////////////////////////////////////  -->
		<!-- Main view -->

			<!-- Product grid -->


		<!-- data -->





						<!-- ios button: show/hide panel -->
						<div class="jplist-ios-button">
							<i class="fa fa-sort"></i>
							More Filters
						</div>
						<div class="clearfix"></div>
						<!-- panel -->
						<div class="jplist-panel box panel-bottom" style="display:none;">

							<div class="jplist-drop-down" data-control-type="drop-down" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="true"><div class="jplist-dd-panel"> 10 per page </div>
								<ul style="display: none;">
									<li class=""><span data-number="5"> 5 per page </span></li>
									<li class="active"><span data-number="10" data-default="true"> 10 per page </span></li>
									<li><span data-number="15"> 15 per page </span></li>
									<li><span data-number="all"> view all </span></li>
								</ul>
							</div>
							<div class="jplist-drop-down" data-control-type="drop-down" data-control-name="sort" data-control-action="sort" data-control-animate-to-top="true"><div class="jplist-dd-panel">Likes asc</div>
								<ul style="display: none;">
									<li class=""><span data-path="default">Sort by</span></li>
                                    <li class="active"><span data-path=".like" data-order="asc" data-type="number" data-default="true">Price low - high</span></li>
									<li><span data-path=".like" data-order="desc" data-type="number">Price high -low</span></li>
									<li><span data-path=".title" data-order="asc" data-type="text">Title A-Z</span></li>
									<li><span data-path=".title" data-order="desc" data-type="text">Title Z-A</span></li>
									<li><span data-path=".desc" data-order="asc" data-type="text">Description A-Z</span></li>
									<li><span data-path=".desc" data-order="desc" data-type="text">Description Z-A</span></li>								</ul>
							</div>

							<!-- pagination results -->
							<div class="jplist-label" data-type="{start} - {end} of {all}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging">1 - 10 of 32</div>

							<!-- pagination -->
							<div class="jplist-pagination" data-control-type="pagination" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="true"><div class="jplist-pagingprev jplist-hidden" data-type="pagingprev"><button type="button" class="jplist-first" data-number="0" data-type="first">«</button><button data-number="0" type="button" class="jplist-prev" data-type="prev">‹</button></div><div class="jplist-pagingmid" data-type="pagingmid"><div class="jplist-pagesbox" data-type="pagesbox"><button type="button" data-type="page" class="jplist-current" data-active="true" data-number="0">1</button> <button type="button" data-type="page" data-number="1">2</button> <button type="button" data-type="page" data-number="2">3</button> <button type="button" data-type="page" data-number="3">4</button> </div></div><div class="jplist-pagingnext" data-type="pagingnext"><button data-number="1" type="button" class="jplist-next" data-type="next">›</button><button data-number="3" type="button" class="jplist-last" data-type="last">»</button></div></div>
						</div>
					</div>
</div>
</div>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
{!! $footer !!}
<script src="<?php echo url(); ?>/themes/js/compare-products/main.js"></script>
<script type="text/javascript">

function number_format(number,decimals,dec_point,thousands_sep) {
	number  = number*1;//makes sure `number` is numeric value
	var str = number.toFixed(decimals?decimals:0).toString().split('.');
	var parts = [];
	for ( var i=str[0].length; i>0; i-=3 ) {
		parts.unshift(str[0].substring(Math.max(0,i-3),i));
	}
	str[0] = parts.join(thousands_sep?thousands_sep:',');
	return str.join(dec_point?dec_point:'.');
}

// var list = $('.list');
// var list_item_clone = $('.item_clone');
// var link = $('.align_brn');
// list_item_clone.remove();
// list_item_clone.removeClass('hidden');
//
// //merubah count kategori
// var right_badge = $('.right-badge');
// var badge_clone = $('.badge_clone');
// badge_clone.remove();
// badge_clone.removeClass('hidden');

  $( "#slider-range" ).slider({
	range: true,
	min: 0,
	max: 10000000,
	values: [0, <?php echo $max_price;?>],
	slide: function( event, ui ) {
	  var min_price = ui.values[ 0 ];
	  var max_price =  ui.values[ 1 ] ;
	  min_price = number_format(min_price,0,'.',',');
	  max_price = number_format(max_price,0,'.',',');
	  $( "#amount" ).val( "Rp. " + min_price + " - Rp. " + max_price);

	},
	stop: function( event, ui ) {
	  show_result();
	}
  });

  $( "#amount" ).val( "Rp. " + number_format($("#slider-range").slider("values", 0),0,'.',',') + " - Rp. " + number_format($("#slider-range").slider("values", 1),0,'.',','));


function show_result(){
  var min_price = $("#slider-range").slider("values", 0);
  var max_price = $("#slider-range").slider("values", 1);
  var grade = [];
  var spc_value = [];
  for (var i = 0; i < {{$i}}; i++) {
	  if ($('#filter_checkbox_grade'+i).is(':checked')) {
		  grade.push($("#filter_checkbox_grade"+i).val());
	  }
  }
  for (var j = 0; j < {{$j}}; j++) {
	  if ($('#filter_checkbox_spec'+j).is(':checked')) {
		  spc_value.push($("#filter_checkbox_spec"+j).val());
	  }
  }
  var param = {
	min_price: min_price,
	max_price: max_price,
	mc_id : '{{$mc_id}}',
	smc_id: '{{$smc_id}}',
	sb_id: '{{$sb_id}}',
	ssb_id: '{{$ssb_id}}',
	grade : grade,
	spc_value : spc_value,
  };
  console.log(param);
  $('#demo').jplist({
	 itemsBox: '.list',
	 itemPath: '.list-item',
	 panelPath: '.jplist-panel',
	 dataSource:{
		 type:'server',
		 server:{
			 ajax:{
				type: 'get',
				data: param,
				dataType: 'html',
				url: '{{url("jplist_filter")}}',
			},
			serverOkCallback: function(serverData, statuses, ajax, response){
			}
		 }
	 }
  });
  $.ajax({
   type: 'get',
   data: param,
   url: '{{url("search_filter")}}',
   success: function(respon){
  //   console.log(respon);
  //   list.empty();
    $('#spc_result').empty();
    // $('#table_grade').empty();
    // right_badge.empty();
    // badge_clone.html(respon.total);
    // right_badge.append(badge_clone);
    // var spc_result = Object.keys(respon.spc_result);
    // var count_spc_result = spc_result.length;
    var i = 0;
    var l = 0;
    for(var key in respon.spc_result){
  	  $("#spc_result").append('<div id="list5" class="div'+i+'" style="margin-top:20px; "></div>');
  	  $(".div"+i).append('<div id="div_div'+i+'"></div>');
  	  $("#div_div"+i).append('<hr><p style="font-size: 15px; font-weight: bold;">'+key+'</p>');
  	  $("#div_div"+i).append('<div style="width:100%;" id="div_div_div'+i+'"></div>');
  	  $("#div_div_div"+i).append('<table style="width:100%" id="table'+i+'"></table>');
  	  for (var j = 0; j < respon.spc_result[key].length; j++) {
  		  // console.log(respon.spc_result[key]);
  		  var yes = 0;
  		  for (var k = 0; k < spc_value.length; k++) {
  			  if (spc_value[k] == respon.spc_result[key][j].spc_value) {
  				  $("#table"+i).append('<tr><td style="width:80%;"><label><input checked id="filter_checkbox_spec'+l+'" type="checkbox" value="'+respon.spc_result[key][j].spc_value+'" onchange="show_result()" data-path=".'+respon.spc_result[key][j].spc_value+'">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.spc_result[key][j].spc_value+'</span></label></td><td style="display:none;"><span class="badge">'+respon.spc_result[key][j].spc_value+'</span></td></tr>');
  				  yes = 1;
  				  break;
  			  }
  		  }
  		  if (yes != 1) {
  			  $("#table"+i).append('<tr><td style="width:80%;"><label><input id="filter_checkbox_spec'+l+'" type="checkbox" value="'+respon.spc_result[key][j].spc_value+'" onchange="show_result()" data-path=".'+respon.spc_result[key][j].spc_value+'">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.spc_result[key][j].spc_value+'</span></label></td><td style="display:none;"><span class="badge">'+respon.spc_result[key][j].spc_value+'</span></td></tr>');
  		  }
  		  l++;
  	  }
  	  i++;
    }

    // for (i = 0; i < respon.grade_result.length; i++) {
 //  	  var yes = 0;
 //  	  for (var j = 0; j < grade.length; j++) {
 //  		  if (grade[j] == respon.grade_result[j].grade_id) {
 //  			  $('#table_grade').append('<tr><td style="width:80%;"><label><input checked id="filter_checkbox_grade'+i+'" type="checkbox" value="'+respon.grade_result[i].grade_id+'" onchange="show_result()">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.grade_result[i].grade_name+'</span></label></td></tr>')
 //  			  yes = 1;
 //  			  break;
 //  		  }
 //  	  }
 //  	  if (yes != 1) {
 //  		  $('#table_grade').append('<tr><td style="width:80%;"><label><input id="filter_checkbox_grade'+i+'" type="checkbox" value="'+respon.grade_result[i].grade_id+'" onchange="show_result()">&nbsp&nbsp&nbsp<span style="font-size: 14px; font-weight: bold;">'+respon.grade_result[i].grade_name+'</span></label></td></tr>')
 //  	  }
	//
    // }

  //   for(i=0;i<respon.data.length;i++)
  //   {
  // 	var list_item_clone_2 = list_item_clone.clone();
  // 	var harga_awal = respon.data[i].pro_price;
  // 	var persen_diskon = Math.round(((respon.data[i].pro_price-respon.data[i].pro_disprice)/(respon.data[i].pro_price))*100);
  // 	var img_urls = (respon.data[i].pro_Img).split('/**/');
  // 	var harga_diskon = respon.data[i].pro_disprice;
  // 	var judul_product = respon.data[i].pro_title;
  // 	var pro_id = respon.data[i].pro_id;
  // 	var pro_name = respon.data[i].mc_name;
    //
  // 	var smc_name = respon.data[i].smc_name;
    //
  // 	var sb_name = respon.data[i].sb_name;
    //
  // 	var ssb_name = respon.data[i].ssb_name;
    //
  // 	if (pro_name == null) {
  // 	  pro_name = "";
  // 	}
  // 	if (smc_name == null) {
  // 	  smc_name = "";
  // 	}
  // 	if (sb_name == null) {
  // 	  sb_name = "";
  // 	}
  // 	if (ssb_name == null) {
  // 	  ssb_name = "";
  // 	}
    //
  // 	if(harga_diskon == 0){
  // 		list_item_clone_2.find('.disprice_present').hide();
  // 		list_item_clone_2.find('.persen_diskon').hide();
  // 		list_item_clone_2.find('.harga_awal').hide();
  // 		list_item_clone_2.find('.tag-dis-prod').hide();
  // 		list_item_clone_2.find('.harga_diskon').html('Rp ' +Number(harga_awal).toLocaleString('id')+ ',00');
  // 		list_item_clone_2.find('.tab-like').html(Number(harga_awal));
  // 	}else {
  // 		list_item_clone_2.find('.persen_diskon').html(persen_diskon + '%');
  // 		list_item_clone_2.find('.harga_awal').html('Rp ' + Number(harga_awal).toLocaleString('id') + ',00');
  // 		list_item_clone_2.find('.harga_diskon').html('Rp ' +Number(harga_diskon).toLocaleString('id')+ ',00');
  // 		list_item_clone_2.find('.tab-like').html(Number(harga_diskon));
  // 	}
  // 	list_item_clone_2.find('#a_clone').attr('href', '{{url('')}}/productview'+'/'+pro_name.replace(' ', '-').toLowerCase()+'/'+smc_name.replace(' ', '-').toLowerCase()+'/'+sb_name.replace(' ', '-').toLowerCase()+'/'+ssb_name.replace(' ', '-').toLowerCase()+'/'+btoa(pro_id));
  // 	list_item_clone_2.find('img').attr('src', '{{ url('') }}/assets/product/'+ img_urls[0]);
  // 	list_item_clone_2.find('.product__title').html(judul_product);
  // 	list_item_clone_2.find('.align_brn').attr('href', '{{url('')}}/productview'+'/'+pro_name.replace(' ', '-').toLowerCase()+'/'+smc_name.replace(' ', '-').toLowerCase()+'/'+sb_name.replace(' ', '-').toLowerCase()+'/'+ssb_name.replace(' ', '-').toLowerCase()+'/'+btoa(pro_id));
  // 	//url('dealview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res)
  // 	// <a href="'. url('productview/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res).'">
  // 	list.append(list_item_clone_2);
  //   }
   },
   error: function(){
    alert('Server Error');
   }
  });
}
</script>
</body>
</html>
