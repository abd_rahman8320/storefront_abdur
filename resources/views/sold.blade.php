<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body id="product_page" style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody">
	<div class="container">

<!-- Sidebar ================================================== -->

<!-- Sidebar end=============================================== -->

<h4>Sold products</h4>
      <legend></legend>
<div class="row">
<?php
$sold_product_error = "";
if($get_store_product_by_id)
{
  $sold_product_error = "";	?>
     <?php
     foreach($get_store_product_by_id as $fetch_most_visit_pro)
     {
	       if($fetch_most_visit_pro->pro_no_of_purchase >= $fetch_most_visit_pro->pro_qty)
         {
             $mcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->mc_name));
             $smcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->smc_name));
	           $sbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->sb_name));
             $ssbcat = strtolower(str_replace(' ','-',$fetch_most_visit_pro->ssb_name));
	           $res = base64_encode($fetch_most_visit_pro->pro_id);
	           $sold_product_error = 1;
	           $mostproduct_saving_price = $fetch_most_visit_pro->pro_price - $fetch_most_visit_pro->pro_disprice;
	           //$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

  	if($fetch_most_visit_pro->pro_price==0 || $fetch_most_visit_pro->pro_price==null)
  	{
  		$mostproduct_discount_percentage = 0;
  	}else {
  		$mostproduct_discount_percentage = round(($mostproduct_saving_price/ $fetch_most_visit_pro->pro_price)*100,2);
  	}
			 $mostproduct_img = explode('/**/', $fetch_most_visit_pro->pro_Img);
  ?>
        <div class="span3  thumbnail tab-sold-wid" style="width:240px;margin-bottom:20px;height:290px;">
        <span class="sold"></span>
  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
  <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$fetch_most_visit_pro->pro_title!!}" target="_self"><img alt="" src="<?php echo url(''); ?>/assets/product/<?php echo $mostproduct_img[0]; ?>" height="215px"></a>
          <?php } ?>
       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
  <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self"><img alt="" src="<?php echo url(''); ?>/assets/product/<?php echo $mostproduct_img[0]; ?>" height="215px"></a>
      <?php } ?>
      <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
  <a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self"><img alt="" src="<?php echo url(''); ?>/assets/product/<?php echo $mostproduct_img[0]; ?>" height="215px"></a>
 <?php } ?>

  <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '') { ?>
<h4><a class="s_detail" href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$fetch_most_visit_pro->pro_title!!}" target="_self"><?php echo substr($fetch_most_visit_pro->pro_title,0,20);  ?>...</a></h4>
       <?php } ?>
       <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '') { ?>
<h4><a class="s_detail" href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self"><?php echo substr($fetch_most_visit_pro->pro_title,0,20);  ?>...</a></h4>
      <?php } ?>
      <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '') { ?>
<h4><a class="s_detail" href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self"><?php echo substr($fetch_most_visit_pro->pro_title,0,20);  ?>...</a></h4>
 <?php } ?>



        </div>
		<?php } } }

    else { ?>
     <h5 style="color:#933;" >No records found under products.</h5>
        <?php } ?>
         <?php if($sold_product_error != 1) { ?>
          <h5 style="color:#933;" >No records found under promo.</h5>
          <?php } ?>
	  </div>
</div>

</div>
</div>
<!-- Footer ================================================================== -->

	{!! $footer !!}
<!-- Placed at the end of the document so the pages load faster ============================================= -->




    <script src="plug-k/js/bootstrap-typeahead.js" type="text/javascript"></script>
    <script src="plug-k/demo/js/demo.js" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo url(); ?>/themes/js/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="<?php echo url(); ?>/themes/js/scriptbreaker-multiple-accordion-1.js"></script>
    <script language="JavaScript">

    $(document).ready(function() {
        $(".topnav").accordion({
            accordion:false,
            speed: 500,
            closedSign: '<span class="icon-chevron-right"></span>',
            openedSign: '<span class="icon-chevron-down"></span>'
        });
    });

    </script>
</body>
</html>
