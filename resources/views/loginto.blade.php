<?php header("Access-Control-Allow-Origin: *"); ?>
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link href="<?php echo url(); ?>/themes/css/green-theme.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="<?php echo url('');?>/themes/css/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo url('');?>/themes/css/component.css" />
<script src="<?php echo url('');?>/themes/js/modernizr.custom.js"></script>
</head>
<body class="loginto-body">
  <div class="container">
      <header>
        <h1 style="color:white">Login To</h1>
      </header>
      <div class="konten">
  			<ul class="grid cs-style-4">
  				<li>
  					<figure>
  						<div><img src="<?php echo url('');?>/assets/sf_icon5.png" alt="img05"></div>
  						<figcaption>
  							<h3>Kukuruyuk Admin</h3>
  							<a href="https://devkukuruyuk.com/sf/siteadmin">Login</a>
  						</figcaption>
  					</figure>
  				</li>
  				<li>
  					<figure>
  						<div><img src="<?php echo url('');?>/assets/wms_icon.png" alt="img06"></div>
  						<figcaption>
  							<h3>Wms</h3>
  							<a href="https://devkukuruyuk.com/wms">Login</a>
  						</figcaption>
  					</figure>
  				</li>
  			</ul>
      </div>
  		</div>
  <footer class="loginto-footer">Kukuruyuk &copy; <?php echo date('Y') ?></footer>
  </div>
  <script src="<?php echo url('');?>/themes/js/toucheffects.js"></script>
	</body>
	</html>
