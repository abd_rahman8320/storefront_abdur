<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<!-- Navbar ================================================== -->
{!! $navbar !!}
{!! $header !!}
<!-- Header End====================================================================== -->
<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->

<!-- Sidebar end=============================================== -->
	<div class="span12">
    <ul class="breadcrumb">
		<li><a href="index">Home</a> <span class="divider">/</span></li>
		<li class="active">Merchant Sign up</li>
    </ul>

        @if ($errors->any())
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>')) !!}
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
        </div>
		</ul>
		@endif
         @if (Session::has('mail_exist'))
		<div class="alert alert-warning alert-dismissable">{!! Session::get('mail_exist') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button></div>
		@endif
         @if (Session::has('result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('result') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button></div>
		@endif
	<h3 style="margin-bottom:0px;">Welcome to merchant sign up!</h3>
    <p style="padding-bottom:20px;">You will now be guided through a few steps to create your own personal online store!</p>
<div class="boks">
{!! Form::open(array('url'=>'add_merchant_submit_signup','class'=>'form-horizontal','id'=>'testform','enctype'=>'multipart/form-data')) !!}
 <div class="row">
	 <div class="col-lg-eleven panel_marg" style="padding-bottom:10px;">
		 					{!! Form::open(array('url'=>'add_merchant_submit_signup','class'=>'form-horizontal','id'=>'testform','enctype'=>'multipart/form-data')) !!}
						 <!-- {!! Form::open(array('url'=>'add_merchant_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!} -->
<div class="panel panel-default">
			<div class="panel-heading">
									Merchant Account
			</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
					<div class="form-grup">
						<div class="col-lg-two">
								<label class="control-labels" for="text1">First Name<span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
								<input type="text" class="form-controlss" placeholder="" id="first_name" name="first_name" value="{!! Input::old('first_name') !!}" required>
						</div>
						<div class="col-lg-two">
								<label class="control-labels" for="text1">Last Name<span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
								<input type="text" class="form-controlss" placeholder="" id="last_name"  name="last_name" value="{!! Input::old('last_name') !!}" required>
						</div>
					</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
						<div class="col-lg-two">
							<label class="control-labels" for="text1">E-mail<span class="text-sub">*</span></label>
						 </div>
						<div class="col-lg-three">
							<input type="email" class="form-controlss" placeholder="" id="email_id" name="email_id" value="{!! Input::old('email_id') !!}" required>
						 </div>
						<div class="col-lg-two">
							<label class="control-labels" for="text1">Phone<span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
							<input type="text" class="form-controlss" placeholder="" id="phone_no" name="phone_no" value="{!! Input::old('phone_no') !!}" required>
						</div>
				</div>
			</div>
		</div>

		 <div class="panel-body">
			 <div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Select Country<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<select required name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" >
							<option value="">-- Select --</option>
										 <?php foreach($country_details as $country_fetch){ ?>
							<option value="<?php echo $country_fetch->co_id; ?>"><?php echo $country_fetch->co_name; ?></option>
											<?php } ?>
						</select>
					</div>

					<div class="col-lg-two">
						<label class="control-labels">Province <span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="mer_province" id="mer_province" value="" class="form-controlss">
						<input type="hidden" name="mer_province_id" id="mer_province_id" value="">
					</div>
				</div>
			 </div>
		 </div>

		 <div class="panel-body">
			 <div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">City<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="mer_city" id="mer_city" value="" class="form-controlss">
						<input type="hidden" name="mer_city_id" id="mer_city_id" value="">
					</div>

					<div class="col-lg-two">
						<label class="control-labels">District <span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="mer_district" id="mer_district" value="" class="form-controlss">
						<input type="hidden" name="mer_district_id" id="mer_district_id" value="">
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="col-lg-twelve">
			   <div class="form-grup">
				   <div class="col-lg-two">
					   <label class="control-labels">Subdistrict <span class="text-sub">*</span></label>
				   </div>
				   <div class="col-lg-three">
					   <input type="text" name="mer_subdistrict" id="mer_subdistrict" value="" class="form-controlss">
					   <input type="hidden" name="mer_subdistrict_id" id="mer_subdistrict_id" value="">
				   </div>
			   </div>
		   </div>
	   </div>



		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Address1<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="addreess_one" name="addreess_one" value="{!! Input::old('addreess_one') !!}" required>
					</div>
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Address2</label>
					</div>
					<div class="col-lg-three">
					    <input type="text" class="form-controlss" placeholder="" id="address_two" name="address_two" value="{!! Input::old('address_two') !!}"  >
					 </div>
				</div>
			</div>
		</div>

		<!-- <div class="panel-body">
		<div class="col-lg-twelve"
				 <div class="form-grup">
						 <label class="control-labels" for="text1">Payment Account<span class="text-sub"></span></label>

						 <div class="col-lg-4">
								 <input type="text" class="form-controlss" placeholder="" id="payment_account" name="payment_account" value="{!!Input::old('payment_account') !!}"  >
						 </div>
				 </div>
				 </div>
		</div>
		</div> -->
		 <!--  -->

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Nama Bank<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="nama_bank" name="nama_bank" value="{!!Input::old('nama_bank') !!}" required>
					</div>
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Nomor Rekening<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="nomor_rekening" name="nomor_rekening" value="{!!Input::old('nomor_rekening') !!}" required>
					</div>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
						<div class="col-lg-two">
							<label class="control-labels" for="text1">Nama Pemilik Rekening<span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
							<input type="text" class="form-controlss" placeholder="" id="nama_pemilik_rekening" name="nama_pemilik_rekening" value="{!!Input::old('nama_pemilik_rekening') !!}" required>
						</div>
						<div class="col-lg-two">
							<label class="control-labels" for="text1">SIUP<span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
							<input type="file" required id="file_siup" name="file_siup" placeholder="SIUP"> Image upload size 455 X 378
						</div>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Tanda Daftar Perusahaan<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="file" required id="file_tanda_daftar_perusahaan" name="file_tanda_daftar_perusahaan" placeholder="TDP"> Image upload size 455 X 378
					</div>
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Surat Domisili<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="file" required id="file_surat_domisili" name="file_surat_domisili" placeholder="Surat Domisili"> Image upload size 455 X 378
					</div>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">NPWP<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="file" required id="file_npwp" name="file_npwp" placeholder="NPWP"> Image upload size 455 X 378
					</div>
					<div class="col-lg-two">
						<label class="control-labels" for="text1">KTP Direksi<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="file" required id="file_ktp" name="file_ktp" placeholder="KTP"> Image upload size 455 X 378
					</div>
				</div>
			</div>
		</div>
 <!--  -->
</div>
<br>
<br>
<div class="panel panel-default">
 <div class="panel-heading">
		 Store Details
 </div>
		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
						<div class="col-lg-two">
							<label class="control-labels" for="text1">Store Name<span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
							<input type="text" class="form-controlss" placeholder="" id="store_name" name="store_name" value="{!! Input::old('store_name') !!}"required>
						</div>
						<div class="col-lg-two">
							<label class="control-labels" for="text1">Phone <span class="text-sub">*</span></label>
						</div>
						<div class="col-lg-three">
							<input type="text" class="form-controlss" placeholder="" id="store_pho" name="store_pho" value="{!! Input::old('store_pho') !!}" required>
						</div>
				</div>
			 </div>
		 </div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Address1<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="store_add_one" name="store_add_one" value="{!! Input::old('store_add_one') !!}" required>
					</div>
					<div class="col-lg-two">
						<label class="control-labels" for="text1">Address2</label>
					</div>
					<div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="store_add_two" name="store_add_two" value="{!! Input::old('store_add_two') !!}"   >
					</div>
			    </div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					 <div class="col-lg-two">
							 <label class="control-labels" for="text1">Select Country<span class="text-sub">*</span></label>
					 </div>
					<div class="col-lg-three">
						<select required class="form-controlss" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
							<option value="">-- Select --</option>
									<?php foreach($country_details as $country_fetch){?>
							<option value="<?php echo $country_fetch->co_id; ?>"><?php echo $country_fetch->co_name; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-lg-two">
						<label class="control-labels">Province <span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="select_province" id="select_province" value="" class="form-controlss">
						<input type="hidden" name="select_province_id" id="select_province_id" value="">
					</div>

				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels" for="text1">City<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="select_city" id="select_city" value="" class="form-controlss">
						<input type="hidden" name="select_city_id" id="select_city_id" value="">
					</div>

					<div class="col-lg-two">
						<label class="control-labels">District <span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="select_district" id="select_district" value="" class="form-controlss">
						<input type="hidden" name="select_district_id" id="select_district_id" value="">
					</div>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				<div class="form-grup">
					<div class="col-lg-two">
						<label class="control-labels">Subdistrict <span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input type="text" name="select_subdistrict" id="select_subdistrict" value="" class="form-controlss">
						<input type="hidden" name="select_subdistrict_id" id="select_subdistrict_id" value="">
					</div>

					<div class="col-lg-two">
					   <label class="control-labels" for="text1">Kode Pos<span class="text-sub">*</span></label>
					</div>
					<div class="col-lg-three">
						<input readonly type="text" class="form-controlss" placeholder="" id="zip_code" name="zip_code" value="{!! Input::old('zip_code') !!}" required>
					</div>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				 <div class="form-grup">
					 <div class="col-lg-two">
						<label class="control-labels" for="text1">Website</label>
					 </div>
					 <div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="website" name="website" value="{!! Input::old('website') !!}"  >
					 </div>
				 </div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				 <div class="form-grup">
					 <div class="col-lg-two">
							 <label class="control-labels" for="text1"><span class="text-sub"></span></label>
					 </div>
					 <div class="col-lg-three">
							 <input id="pac-input" class="form-controlss" type="text" placeholder="Type your location here (Auto-complete)" required>
					 </div>
				 </div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				 <div class="form-grup">
						 <div class="col-lg-six">
								 <label class="control-labels" for="text1">Map Search Location<span class="text-sub">*</span><br><span  style="color:#999">(Drag Marker to get latitude & longitude )</span></label>
						 </div>
						 <div class="col-lg-four">
								 <div id="map_canvas" style="width:300px;height:250px;" ></div>
						 </div>

				 </div>
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<div class="panel-body">
			<div class="col-lg-twelve">
				 <div class="form-grup">
						 <div class="col-lg-five">
							<label class="control-labels" for="text1">Latitude<span class="text-sub">*</span></label>
						 </div>
						 <div class="col-lg-three">
							<input type="text" class="form-controlss" placeholder="" id="latitude" name="latitude" readonly  value="{!! Input::old('latitude') !!}"  >
						 </div>
						 <div class="col-lg-two">
							<label class="control-labels" for="text1">Longitude<span class="text-sub">*</span></label>
						 </div>
						 <div class="col-lg-three">
							<input type="text" class="form-controlss" placeholder="" id="longtitude" name="longtitude" value="{!! Input::old('longtitude') !!}"  readonly >
						 </div>
				 </div>
			</div>
		</div>

		<div class="panel-body">
			<div class="col-lg-twelve">
				 <div class="form-grup">
					 <div class="col-lg-two">
						<label class="control-labels" for="text1">Commission<span class="text-sub">*</span></label>
					 </div>
					 <div class="col-lg-three">
						<input type="text" class="form-controlss" placeholder="" id="commission" name="commission" value="{!! Input::old('commission') !!}" required><span> %</span>
					 </div>
					 <div class="col-lg-two">
						<label class="control-labels" for="text1">Stores Image</label>
					 </div>
					 <div class="col-lg-three">
						<input type="file" id="file" name="file" placeholder="Fruit ball"> Image upload size 455 X 378
					 </div>
				 </div>
			</div>
		</div>
										 <!-- Meta Hide -->

				 <!-- <div class="panel-body">
												<div class="form-group">
								 <label class="control-labels col-lg-two" for="text1">Meta keywords<span class="text-sub">*</span></label>

								 <div class="col-lg-4">
										<textarea  class="form-controlss" name="meta_keyword" id="meta_keyword" >{!! Input::old('meta_keyword') !!}</textarea>
								 </div>
						 </div>
										 </div> -->

				 <!-- <div class="panel-body">
												<div class="form-group">
								 <label class="control-labels col-lg-two" for="text1">Meta description<span class="text-sub">*</span></label>

								 <div class="col-lg-4">
										<textarea id="meta_description"  name="meta_description" class="form-controlss">{!! Input::old('meta_description') !!}</textarea>
								 </div>
						 </div>
										 </div> -->

			<div class="form-grup">
				<div class="col-lg-twelve">
					<div class="col-lg-eight">
						<label class="control-labels col-lg-three" for="pass1"><span class="text-sub"></span></label>
						<button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" formtarget="_self"><a style="color:#fff" >Submit</a></button>
						<button class="btn btn-default btn-sm btn-grad" type="reset" ><a style="color:#000">Reset</a></button>
					</div>
				</div>
			</div>
			</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
	{!! $footer !!}
<!-- Placed at the end of the document so the pages load faster ============================================= -->
 <!-- <script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script> -->
<script src="<?php //echo url();?>/themes/js/jquery.steps.js"></script>
	<script src="<?php echo url(); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script>

	<script src="<?php echo url(); ?>/themes/js/bootshop.js"></script>


   <!-- <script src="<?php echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script> -->


    <script>
	$( document ).ready(function() {


		$('.close').click(function() {
			$('.alert').hide();
		});
	// $('#submit').click(function() {
  //   var file		 	 = $('#file_siup');
	// var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
  //     	if(file.val() == "")
 // 		{
 // 		file.focus();
	// 	file.css('border', '1px solid red');
	// 	return false;
	// 	}
	// 	else if ($.inArray($('#file_siup').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
	// 	file.focus();
	// 	file.css('border', '1px solid red');
	// 	return false;
	// 	}
	// 	else
	// 	{
	// 	file.css('border', '');
	// 	}
	// });
	});
	</script>
     <script>
	 $("#mer_province").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_provinsi')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 country: $('#mer_select_country').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#mer_province').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#mer_province').val(ui.item.value);
	         $('#mer_province_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#mer_province').val('');
	             $('#mer_province_id').val('');
	             alert('Silahkan Pilih Provinsi/State yang Tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete('search');
	 });

	 $("#mer_city").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_get_city')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 state: $('#mer_province_id').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#mer_city').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#mer_city').val(ui.item.value);
	         $('#mer_city_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#mer_city').val('');
	             $('#mer_city_id').val('');
	             alert('Silahkan pilih City yang tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete("search");
	 });

	 $("#mer_district").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_get_district')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 city: $('#mer_city_id').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#mer_district').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#mer_district').val(ui.item.value);
	         $('#mer_district_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#mer_district').val('');
	             $('#mer_district_id').val('');
	             alert('Silahkan pilih District yang tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete("search");
	 });

	 $("#mer_subdistrict").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_get_subdistrict')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 district: $('#mer_district_id').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#mer_subdistrict').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#mer_subdistrict').val(ui.item.value);
	         $('#mer_subdistrict_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#mer_subdistrict').val('');
	             $('#mer_subdistrict_id').val('');
	             alert('Silahkan pilih Sub District yang tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete("search");
	 });

	 //for store autocomplete
	 $("#select_province").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_provinsi')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 country: $('#select_country').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#select_province').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#select_province').val(ui.item.value);
	         $('#select_province_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#select_province').val('');
	             $('#select_province_id').val('');
	             alert('Silahkan Pilih Provinsi/State yang Tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete('search');
	 });

	 $("#select_city").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_get_city')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 state: $('#select_province_id').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#select_city').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#select_city').val(ui.item.value);
	         $('#select_city_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#select_city').val('');
	             $('#select_city_id').val('');
	             alert('Silahkan pilih City yang tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete("search");
	 });

	 $("#select_district").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_get_district')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 city: $('#select_city_id').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#select_district').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#select_district').val(ui.item.value);
	         $('#select_district_id').val(ui.item.id);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#select_district').val('');
	             $('#select_district_id').val('');
	             alert('Silahkan pilih District yang tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete("search");
	 });

	 $("#select_subdistrict").autocomplete({
	     source:function(request, response){
	         $.ajax({
	             url:"{{url('auto_get_subdistrict')}}",
	             method:'get',
	             data:{
	                 term: request.term,
	                 district: $('#select_district_id').val()
	             },
	             success:function(dataSuccess){
	                 response(dataSuccess);
	             }
	         });
	     },
	     minLength:0,
	     focus:function(event, ui){
	         $('#select_subdistrict').val(ui.item.value);
	         return false;
	     },
	     select:function(event, ui){
	         $('#select_subdistrict').val(ui.item.value);
	         $('#select_subdistrict_id').val(ui.item.id);
			 $('#zip_code').val(ui.item.zip_code);
	     },
	     change:function(event, ui){
	         if (ui.item===null) {
	             $('#select_subdistrict').val('');
	             $('#select_subdistrict_id').val('');
				 $('#zip_code').val('');
	             alert('Silahkan pilih Sub District yang tersedia');
	         }
	     }
	 }).bind('focus', function(){
	     $(this).autocomplete("search");
	 });

	function select_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){
				 // alert(responseText);
				   if(responseText)
				   {
					$('#select_city').html(responseText);
				   }
				}
			});
	}

	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){
				 // alert(responseText);
				   if(responseText)
				   {
					$('#select_mer_city').html(responseText);
				   }
				}
			});
	}
	</script>

 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script>
	 <script src="<?php echo url(); ?>/themes/js/simpleform.min.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDoY1OPjAqu1PlQhH3UljYsfw-81bLkI&libraries=places&signed_in=true"></script>
   <script src="<?php echo url(); ?>/themes/js/jquery-gmaps-latlon-picker.js"></script>

   <script type="text/javascript">
    var map;

    function initialize() {
    var myLatlng = new google.maps.LatLng(-6.228461975960522,106.98351461534423);
        var mapOptions = {

           zoom: 10,
                center: myLatlng,
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	 		  var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				visible: false,
				draggable:true,
            });
		google.maps.event.addListener(marker, 'dragend', function(e) {

			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longtitude').val(lng);
			});
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
				var myLatlng = place.geometry.location;
				//alert(place.geometry.location);
				var marker = new google.maps.Marker({
                 position: myLatlng,
				 visible: true,
                map: map,
				draggable:true,
            });
			google.maps.event.addListener(marker, 'dragend', function(e) {

			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longtitude').val(lng);
			});
            } else {
                map.setCenter(place.geometry.location);

                map.setZoom(17);
            }
        });



    }


    google.maps.event.addDomListener(window, 'load', initialize);
	</script>

	<!-- Themes switcher section ============================================================================================= -->
