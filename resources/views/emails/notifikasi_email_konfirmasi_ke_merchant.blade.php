<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
  <title>Kukuruyuk Newsletter</title>

  <style type="text/css">
body {
  margin: 0;
  padding: 0;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

table {
  border-spacing: 0;
}

table td {
  border-collapse: collapse;
}

.ExternalClass {
  width: 100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
  line-height: 100%;
}

.ReadMsgBody {
  width: 100%;
  background-color: #ebebeb;
}

table {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

img {
  -ms-interpolation-mode: bicubic;
}

.yshortcuts a {
  border-bottom: none !important;
}

@media screen and (max-width: 599px) {
  .force-row,
  .container {
    width: 100% !important;
    max-width: 100% !important;
  }
}
@media screen and (max-width: 400px) {
  .container-padding {
    padding-left: 12px !important;
    padding-right: 12px !important;
  }
}
.ios-footer a {
  color: #aaaaaa !important;
  text-decoration: underline;
}
</style>
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
  <tr>
    <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

      <br>

      <!-- 600px container (white background) -->
      <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
        <tr>
           <img src="{{$message->embed(public_path().'/assets/logo/logo-web-baru.png')}}" alt="" style="text-align: center;"/>
          </td>
        </tr>
        <tr>
          <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
            <br>
<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Notifikasi Transaksi</div>
<br>
    <table width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
      <tr>
        <td class="col" valign="top" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
        </td>
      </tr>
    </table>

<br>

<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
<p style="text-align: center;">Pembayaran telah berhasil untuk nomor transaksi <b><?php echo $trans_id;?></b></p>
  <table border="1" style="width:100%;">
  <tr>
      <th style="text-align: center;">Nama Produk</th>
      <th style="text-align: center;">Product SKU</th>
      <th style="text-align: center;">Qty</th>
      <th style="text-align: center;">Customer Name</th>
      <th style="text-align: center;">Phone Number</th>
      <th style="text-align: center;">Metode Pengiriman</th>
    <tr>
    <tr>
      <td style="text-align: left;"><?php echo $title; ?></td>
      <td style="text-align: center;"><?php echo $pro_sku_merchant; ?></td>
      <td style="text-align: center;"><?php echo $order_qty; ?></td>
      <td style="text-align: center;"><?php echo $cus_name; ?></td>
      <td style="text-align: center;"><?php echo $cus_phone; ?></td>
      <td style="text-align: center;"><?php echo $postalservice; ?></td>
    </tr>
    </table>
</div>
<br>
          </td>
        </tr>
      </table>
<!--/600px contain'
    </'td>
  </tr>
</table>
<!--/100% background wrapper-->

</body>
</html>
