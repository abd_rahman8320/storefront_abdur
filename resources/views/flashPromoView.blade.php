<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div class="container">
	@if (Session::has('payment_cancel'))
	<div class="alert alert-danger alert-dismissable">{!! Session::get('payment_cancel') !!}
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
   @endif
<div class="row">
<div class="span2 side-men" id="side-menu">
	<div class="side-menu-head"><strong>Categories</strong></div>
		<ul id="css3menu1" class="topmenu">
<input type="checkbox" id="css3menu-switcher" class="switchbox"><label onclick="" class="switch" for="css3menu-switcher"></label>
<?php foreach($main_category as $fetch_main_cat) { $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>
<?php if(count($sub_main_category[$fetch_main_cat->mc_id])!= 0) { ?>
<li class="topfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>" target="_self"><?php echo $fetch_main_cat->mc_name; ?> </a>

	<ul>
    <?php foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat)  { $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>
    		<?php if(count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0) { ?>
			 <li class="subfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>" target="_self"><?php echo $fetch_sub_main_cat->smc_name ; ?> </a>

		<ul>
                <?php  foreach($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat) { $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id;?>  <?php if(count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0) { ?>
					<li class="subsecond"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>" target="_self"><?php echo  $fetch_sub_cat->sb_name; ?> </a>

                    <ul style="display:none;">
                    <?php  foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat) { $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>
                        <li class="subthird"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>" target="_self"><?php echo $fetch_secsub_cat->ssb_name ?></a></li>
                     <?php } ?>
                      </ul>
                      <?php } ?>
                    </li>
				<?php } ?>
				</ul>
                <?php } ?>
        </li>
        <?php } ?>
	</ul>
    <?php } ?>
    </li>
      <?php } ?>
</ul>
		<br>
		  <div class="clearfix"></div>
		<br/>
</div>
<div class="span9">
	<div id="carouselBlk">
		<div id="myCarousel" class="carousel slide">
			<div class="carousel-inner">
				<?php
					if($bannerimagedetails)
					{
						$i=1;

						foreach($bannerimagedetails as $banner)
						{
							$bannerimagepath="assets/bannerimage/".$banner->bn_img;

							date_default_timezone_set('Asia/Jakarta');
							$today = date('Y-m-d H-i-s');

							if($banner->start_date <= $today && $banner->end_date >= $today)
							{
								?>
									<div <?php if($i==1){ ?>class="item active" <?php } else { ?> class="item" <?php } ?>>
										<a href="<?php echo url('getting_count_klick/'.$banner->bn_id); ?>">
											<img width="100%" src="<?php echo url('').'/'.$bannerimagepath; ?>"  alt=""/>

										</a>
									</div>
								<?php
							}
							$i++;
						}
					}
					else
					{ ?>
						<div class="item active">
							<a ><img style="width:100%" src="<?php echo url(''); ?>/themes/images/carousel/6.png" alt="special offers"/></a>
						</div>

						<div class="item">
							<div class="container">
								<a ><img style="width:100%" src="<?php echo url(''); ?>/themes/images/carousel/3.png" alt="special offers"/></a>
							</div>
						</div>
				<?php
					}
				?>
			</div>

			<a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
	  	</div>
	</div>
</div>
</div>
</div>

<div class="container">



	<div style="height:25px"></div>



</div>







<div class="container hide-mob hid-tab">



<div class="row">



<?php

if($noimagedetails)

{

foreach($noimagedetails as $noimage)

{

	$noimgpath="assets/noimage/".$noimage->imgs_name;

}

}

else

{

	$noimgpath="assets/noimage/nonamewbHfhyPG.jpg";

}

$count=1;

 foreach($addetails as $adinfo) {





$imgpath="assets/adimage/".$adinfo->ad_img;

$adurl=$adinfo->ad_redirecturl;



?>





        <div class="span4"><a href="<?php echo $adurl;?>" target="_self" ><img src="<?php echo url('').'/'.$imgpath; ?>" width="381px" height="215px" class="tab-add"></a></div>





        <?php  $count++;} if($count>3){ } else {



			for($i=$count;$i<=3;$i++){

			?>



        <div class="span4"><a href="<?php echo url(''); ?>" target="_self"><img src="<?php echo url('').'/'.$noimgpath; ?>" width="381px" height="215px"></a></div>



   <?php } } ?>



</div>



</div>

<div id="mainBody" >
	<div class="container home-pro" >
	@if(Session::has('wish'))
	<p class="alert {!! Session::get('alert-class', 'alert-success') !!}">{!! Session::get('wish') !!}</p>
	@endif
	<div class="row">
<!-- Sidebar ================================================== -->


<!-- Sidebar end=============================================== -->

<!-- Mulai Product tampil -->

		<div id="demo" class="span12">
			<div class="compare-basket">
				<button class="action action--button action--compare">
				<i class="fa fa-check"></i><span class="action__text">Compare</span></button>
		</div>
		<?php //dd($get_schedule_flash); ?>
		<div class="view">
			<section class="grid">

			<div class="flash_promo" style="width:100%; height:270px; margin-top:10px;">
				<p style="text-align: left; float:left; font-size: 16px; margin-left:15px;" ><strong>Flash Promo</strong></p>
				<a style="float:right; color:blue; margin-right:20px;" href="{!! url('flashPromoView') !!}" >Lihat semua</a>
				<div style="clear:both;"></div>
				<div style="float:left; width:18%; margin-left:15px;" >
					<ul class="slider1" style="padding:0; margin:0;">
					<!-- Wrapper for slides -->
						<?php $i=1;

						if($get_schedule_flash){
							foreach ($get_schedule_flash as $product_flash){
							$get_image = explode("/**/",$product_flash->pro_Img);

							$todays = date('Y-m-d H:i:s');
							$format = 'Y-m-d H:i:s';
							$dt = DateTime::createFromFormat($format, $todays);
							$today_sec = $dt->format('U');

							$start_date = $product_flash->schedp_start_date;
							$sd = DateTime::createFromFormat($format, $start_date);
							$start_sec = $sd->format('U');

							$end_date = $product_flash->schedp_end_date;
							$ed = DateTime::createFromFormat($format, $end_date);
							$end_sec = $ed->format('U'); ?>

							<?php
							if($today_sec > $start_sec && $today_sec <  $end_sec){ ?>
								<li >
									<img style="height: 78%;" src="<?php echo url().'/assets/product/'.$get_image[0]; ?>" alt="..." >
									<div style="font-size:13px;" data-inprogressdate="<?php echo $product_flash->schedp_start_date;?>" data-enddate="<?php echo $product_flash->schedp_end_date;?>"></div>
								</li>
							<?php } ?>
							<?php $i++; }?>
							<?php
							}
						?>
						</ul>

				</div>
				<div style="float:left; width:60%; margin-left:5px;">
					<ul class="bxslider">
					<?php $i=1;
					if($get_schedule_flash){
					foreach ($get_schedule_flash as $product_flash){
						$get_image = explode("/**/",$product_flash->pro_Img);

						$todays = date('Y-m-d H:i:s');
						$format = 'Y-m-d H:i:s';
						$dt = DateTime::createFromFormat($format, $todays);
						$today_sec = $dt->format('U');

						$start_date = $product_flash->schedp_start_date;
						$sd = DateTime::createFromFormat($format, $start_date);
						$start_sec = $sd->format('U');

						$end_date = $product_flash->schedp_end_date;
						$ed = DateTime::createFromFormat($format, $end_date);
						$end_sec = $ed->format('U'); ?>

						<?php if($today_sec < $start_sec){?>
						<?php
						if($today_sec < $start_sec){ ?>
							<li>
								<img style="height: 78%;" src="<?php echo url().'/assets/product/'.$get_image[0]; ?>" />
								<div style="font-size:13px;" data-goingtodate="<?php echo $product_flash->schedp_start_date;?>" data-enddate="<?php echo $product_flash->schedp_end_date;?>"></div>
							</li>
						<?php } ?>
						<?php } ?>
					<?php $i++; }
					} ?>
					</ul>
				</div>
				<div style="float:left; width:18%; height:280px; margin-left:5px;">
					<ul class="slider3" style="padding:0; margin:0; ">
					<?php $i=1;
					if($get_schedule_flash){
					foreach ($get_schedule_flash as $product_flash){
						$get_image = explode("/**/",$product_flash->pro_Img);

						$todays = date('Y-m-d H:i:s');
						$format = 'Y-m-d H:i:s';
						$dt = DateTime::createFromFormat($format, $todays);
						$today_sec = $dt->format('U');

						$start_date = $product_flash->schedp_start_date;
						$sd = DateTime::createFromFormat($format, $start_date);
						$start_sec = $sd->format('U');

						$end_date = $product_flash->schedp_end_date;
						$ed = DateTime::createFromFormat($format, $end_date);
						$end_sec = $ed->format('U'); ?>


						<?php
						if($today_sec > $end_sec){ ?>
							<li>
								<img style="height: 77%;" src="<?php echo url().'/assets/product/'.$get_image[0]; ?>" />
								<div style="font-size:13px;" data-goingtodate="<?php echo $product_flash->schedp_start_date;?>" data-enddate="<?php echo $product_flash->schedp_end_date;?>"></div>
							</li>
						<?php } ?>

					<?php $i++; }
					} ?>
					</ul>
				</div>

			</div>

			<div class="products">
				<h3 class="home-heading" style="font-weight: bold; margin-left: 40px; margin-top: 10px; clear:both;">New Products</h3>
				<a href="<?php echo url('products');?>"><h4 class="see-all">Lihat Semua</h4></a>
				<?php
				if(count($product_details) != 0)
					{
						foreach($product_details as $product_det)
						{
							$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
            				$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
							$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
             				$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
			  				$res = base64_encode($product_det->pro_id);
							$product_image = explode('/**/',$product_det->pro_Img);

							$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
							if($product_det->pro_price==0 || $product_det->pro_price==null)
							{
								$product_discount_percentage = 0;
							}else {
								$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
							}
							if($product_det->pro_no_of_purchase < $product_det->pro_qty)
							{
				?>
					<!-- item 1 -->
					<div class="product" style="margin-right:5px!important;width:204px;">
							<!-- img -->
						<div class="product__info">
							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
								{
									?>

									<div class="img">
										<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" target="_self"><img class="product__image"  src="<?php echo url('assets/product/').'/'.$product_image[0];?>" alt="" title=""/></a>
									<?php
								}
							?>

							<?php

								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
								{
							?>
									<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self"><img class="product__image"  alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
 							<?php
 								}
 							?>

 							<?php
 								if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
 									{
 							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>

 							<?php
 									}
 							?>
							</div>
							<?php

								if(floatval($product_det->pro_disprice) != 0 )
								{
								$total_diskon = ceil(100 - (floatval($product_det->pro_disprice)/floatval($product_det->pro_price) * 100));
								}

							?>
							<!-- data -->
							<div class="">
								<p class="title product__title tab-title"><?php echo substr($product_det->pro_title,0,25); ?></p>
								<?php
									if(floatval($product_det->pro_disprice) != 0 )
									{
										?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}.  <?php echo number_format($product_det->pro_disprice,2,",",".");?></p>
											<?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
										  <i class="tag-dis">
												<div id="dis-val">
													<?php echo round($product_discount_percentage);?>%
												</div>
											</i>
										  <?php } ?>
											<p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color:#aaa; font-size: 12;">{{$get_cur}}.  <?php echo number_format($product_det->pro_price,2,",",".");?></span></strike>
													<?php
									}
									else
									{
										?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}.  <?php echo number_format($product_det->pro_price,2,",",".");?></p>
										<?php
									}

								?>

							<?php
								if($product_det->pro_no_of_purchase >= $product_det->pro_qty)
								{
							?>
                                <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a>
                            <?php
                            	}
                            	else
                            	{
                            ?>
                            	</h4>

							</div>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php

								}

							?>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php
								}
							?>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php
								}
							?>
						</div>

						<label class="action action--compare-add">
							<input class="check-hidden" type="checkbox" /><i class="fa fa-plus"></i><i class="fa fa-check"></i>
								<span class="action__text action__text--invisible">Add to compare</span>
						</label>

						<?php
							}
						?>

					</div>


					<?php
						}
					}
				}

				elseif(count($product_details) == 0)
				{
				?>

				<div class="box jplist-no-results text-shadow align-center jplist-hidden">

					<p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">No products available</p>
				</div>

				<?php
				}
				?>
				</div>

				<?php

		foreach($groping_main as $main) {
	?>
	<!-- Mulai Product tampil -->
<center>
	<h3 class="home-heading" style="float: left; font-weight: bold; margin-left: 40px;"><?php echo $main->group_nama?></h3>

	<a href="<?php echo url('products');?>"><h4 class="see-all-group">Lihat Semua</h4></a>
</center>

				<div class="products">
				<?php
				if(array_key_exists($main->group_id, $grouping_product))
					{
						foreach($grouping_product[$main->group_id] as $product_det)
						{
							$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
            				$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
							$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
             				$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
			  				$res = base64_encode($product_det->pro_id);
							$product_image = explode('/**/',$product_det->pro_Img);

							$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
							if($product_det->pro_price==0 || $product_det->pro_price==null)
							{
								$product_discount_percentage = 0;
							}else {
								$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
							}
							if($product_det->pro_no_of_purchase < $product_det->pro_qty) {
				?>
					<!-- item 1 -->
					<div class="product" style="margin-right:5px!important;width:204px;">
							<!-- img -->
						<div class="product__info">
							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
									{
							?>

							<div class="img">
								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" target="_self"><img  class="product__image" src="<?php echo url('assets/product/').'/'.$product_image[0];?>" alt="" title=""/></a>

							<?php
								}
							?>

							<?php

								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
									{
							?>
									<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
 							<?php
 								}
 							?>

 							<?php
 								if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
 									{
 							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>

 							<?php
 									}
 							?>
							</div>
							<?php
								if(floatval($product_det->pro_disprice) != 0 )
								{

									$total_diskon = ceil(100 - (floatval($product_det->pro_disprice)/floatval($product_det->pro_price) * 100));
								}
							?>
							<!-- data -->
							<div class="">

								<p class="title product__title tab-title"><?php echo substr($product_det->pro_title,0,25); ?></p>
								<?php
									if(floatval($product_det->pro_disprice) != 0 )
									{
										?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}.<?php echo number_format($product_det->pro_disprice,2,",",".");?></p>
											<?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
										  <i class="tag-dis">
												<div id="dis-val">
													<?php echo round($product_discount_percentage);?>%
												</div>
											</i>
										  <?php } ?>
											<p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color: #aaa; font-size: 10;">{{$get_cur}}.<?php echo number_format($product_det->pro_price,2,",",".");?></span></strike>
													<?php
									}
									else
									{
										?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}.<?php echo number_format($product_det->pro_price,2,",",".");?></p>
										<?php
									}

								?>

							<?php
								if($product_det->pro_no_of_purchase >= $product_det->pro_qty)
								{
							?>
                                <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a>
                            <?php
                            	}
                            	else
                            	{
                            ?>
                            	</h4>

							</div>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php

								}

							?>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php
								}
							?>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php
								}
							?>
						</div>

						<label class="action action--compare-add">
							<input class="check-hidden" type="checkbox" /><i class="fa fa-plus"></i><i class="fa fa-check"></i>
								<span class="action__text action__text--invisible">Add to compare</span>
						</label>

						<?php
							}
						?>

					</div>
					<?php
						}
					}
				}

				elseif(count($product_details) == 0)
				{
				?>

				<div class="box jplist-no-results text-shadow align-center jplist-hidden">

					<p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">No products available</p>
				</div>

				<?php
				}
				?>
				</div>
	<?php
		}
	?>
		</section>
	</div>
		<section class="compare">
			<button class="action action--close"><i class="fa fa-remove"></i><span class="action__text action__text--invisible">Close comparison overlay</span></button>
		</section>
	</div>
	<!-- selesai tampilan product -->


	<!-- end -->
</div>
</div>
<br />
<br />
		<div class="container" style="background-color:#ffffff">
			<div class="grid-ytb">
			<!-- <div class="col-md-12"> -->
			<div class="ytb-embed">
				<div id="embed-vid">
					<iframe width="352" height="296" src="https://www.youtube.com/embed/yDw5iYkYTYs" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	<div class="grid-ytb">
		<!-- <div class="span5"> -->
		<div class="feature-icon-hover">
			<!-- <div class="trust-text"> -->
					<div class="span4">
						<span class="icon large">
							<img src="<?php echo url('');?>/themes/images/garansi.png" alt="pengiriman"/>
						</span>
					<div class="wrappin">
					<div class="above-heading">
						<h6>Produk Kami</h6>
					</div>
						<h5 style="color:black;">Jaminan Garansi</h5>
					<p style="text-align:justify;color:black">
						Semua produk Barang Jago bergaransi
						& terjamin kualitasnya. Kukuruyuk.com akan memberikan garansi terbatas untuk service GRATIS!
					</p>
				</div>
				</div>
			<!-- </div> -->
		</div>
		<div class="feature-icon-hover">
			<!-- <div class="trust-text"> -->
					<div class="span4">
						<span class="icon large">
							<img src="<?php echo url('');?>/themes/images/pengiriman.png" alt="distribusi"/>
						</span>
					<div class="wrappin">
					<div class="above-heading">
						<h6>Distribusi</h6></div>
						<h5 style="color:black;">Dikirim Ke Seluruh Indonesia</h5>
					<p style="text-align:justify;color:black">
						Bekerjasama dengan JNE dan Tim Ekspedisi Terbaik untuk mengirim barang pesanan Anda ke seluruh Indonesia.
					</p>
				</div>
			</div>
			<!-- </div> -->
		</div>
	<!-- </div> -->
	</div>
	<div class="grid-ytb">
		<div class="feature-icon-hover">
			<!-- <div class="trust-text"> -->
					<div class="span4">
						<span class="icon large">
							<img src="<?php echo url('');?>/themes/images/best-prices.png" alt="best-price"/>
						</span>
					<div class="wrappin">
					<div class="above-heading">
						<h6>Harga Terbaik</h6></div>
						<h5 style="color:black;">Paling Hemat</h5>
					<p style="text-align:justify;color:black">
						Hanya Kukuruyuk.com yang menawarkan produk Barang Jago dengan harga paling hemat dan terjangkau.
					</p>
				</div>
				</div>
			<!-- </div> -->
		</div>
		<div class="feature-icon-hover">
			<!-- <div class="trust-text"> -->
					<div class="span4">
						<span class="icon large">
							<img src="<?php echo url('');?>/themes/images/grade-list.png" alt="kualitas"/>
						</span>
					<div class="wrappin">
					<div class="above-heading">
						<h6>Kualitas Produk</h6></div>
						<h5 style="color:black;">Kategori Produk</h5>
					<p style="text-align:justify;color:black">
						Tim profesional Kukuruyuk.com menilai kategori kualitas Barang Jago berdasarkan kondisi fisiknya, Grade A, Grade B, Grade C dan Grade D. Apa artinya?
					</p>
					<a style="color:black" href="<?php echo url('');?>/kualitas_produk" target="_self" alt="grading"><u>Baca Selengkapnya..</u></a>
				</div>
			</div>
			<!-- </div> -->
		</div>
	</div>
	<!-- </div> -->
</div>
<div id="ac-wrapper" style='display:none'>
    <div id="popup">
        <center>
             <h2>Popup Content Here</h2>

            <input type="submit" name="submit" value="Submit" onClick="PopUp('hide')" />
        </center>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.js"></script>
<script src="<?php echo url(); ?>/themes/js/compare-products/main.js"></script>
<script>
$(document).ready( function() {
	$('.slider1').bxSlider({
     mode: 'fade',
  auto: true,
  pause: 2000
});

	$('.bxslider').bxSlider({
     auto: true,
  minSlides: 4,
  maxSlides: 4,
  slideWidth: 170,
  slideMargin: 10
});

$('.slider3').bxSlider({
     auto: true,

});

	//$('#tampil_coba').html('Hahaha');
  // Handler for .ready() called.
 $(function(){
    $('[data-inprogressdate]').each(function() {
   	var $this = $(this), finalDate = $(this).data('enddate');
	   var today = new Date();

	   $this.countdown(finalDate)
	   .on('update.countdown', function(e){
		   $(this).html('End at : ' + e.strftime('%D days %H:%M:%S'));
	   })
	   .on('finish.countdown', function(e){
		   $this.html('Expired');
	   })
 });
});

$(function(){
    $('[data-goingtodate]').each(function() {
   	var $this = $(this), finalDate = $(this).data('goingtodate');
	   var today = new Date();

	   $this.countdown(finalDate)
	   .on('update.countdown', function(e){
		   $(this).html('Start at : ' + e.strftime('%D days %H:%M:%S'));
	   })
	   .on('finish.countdown', function(e){
		   $this.html('Expired');
	   })
 });
});
	// $.get("{{ url('get_promo_flash') }}", {
    //                             pro_title:"",
	// 						  }).then(function(result){

							// 	  console.log(result.data.length);
							// 	  if(result.data.length >= 0){
							// 		  for(i=0;i<result.data.length;i++){
							// 			  		var ul_clone = $('show_promo').clone();


							// 					var countDownDate = new Date(result.data[i].schedp_start_date).getTime();
							// 					var now = new Date().getTime();
							// 					//console.log(countDownDate);

							// 					(function(i){
							// 						setInterval(function(){

							// 						var distance = countDownDate - now;
							// 						var days = Math.floor(distance / (1000 * 60 * 60 * 24));
							// 						var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
							// 						var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
							// 						var seconds = Math.floor((distance % (1000 * 60)) / 1000);
							// 						var time = days + "days" + hours + "hours" + minutes + "minutes" + seconds + "seconds";

							// 						if (distance < 0) {
							// 							clearInterval(x);
							// 							$('.flash_product_active').html("EXPIRED");
							// 						}
							// 						console.log(time);
							// 						}, 1000);

							// 					})(i)
							// 			  }
							// 	  }

							//   });


  });
</script>
</div>

<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
	{!! $footer !!}
</body>
</html>
