<?php header("Access-Control-Allow-Origin: *"); ?>
<div id="logoArea" class="navbar para-nav">
    <a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
    </a>
    <div class="container" style="width: 86%;">
        <?php if($logodetails){foreach($logodetails as $logo) { $logoimgpath="assets/logo/" .$logo->imgs_name; } } else { $logoimgpath="themes/images/logo2.png"; }?> <a target="_self" class="brand" href="<?php echo url('index'); ?>"><img src="<?php echo url('').'/'.$logoimgpath; ?>" alt="Storefront eCommerce Multivendor"/></a>
        <div class="pull-right mob-cart" style="padding-top:10px; padding-left:25px">
        <img src="<?php echo url(''); ?>/themes/images/cart.png"> <span class="text-info"><a href="<?php echo url('cart'); ?>" >
        <strong>
        <?php
        if(isset($_SESSION['cart']))
            {
                $item_count_header1 = count($_SESSION['cart']);
                }
        else {
            $item_count_header1 = 0;
            }
            if(isset($_SESSION['deal_cart']))
                {
                    $item_count_header2 = count($_SESSION['deal_cart']);
            }
            else {
                $item_count_header2 = 0;
                }
                $item_count_header = $item_count_header1 + $item_count_header2;
            if($item_count_header != 0) {
             echo $item_count_header;
             }
             else {
                echo 0;
                }
                ?>        </strong>  items in your cart</a></span>
        </div>
        <form class="form-inline navbar-search pull-right" style="margin-top: 17px;display:none;">
            <select class="srchTxt" onchange="window.location.href = '<?php echo url("category_list")."/ "; ?>' + this.value;">
                <option value="0">Select Category</option>
                <?php foreach($header_category as $category_list) { ?>
                <option value="<?php echo base64_encode($category_list->mc_id); ?>">
                    <?php echo $category_list->mc_name; ?></option>
                <?php } ?> </select>
            <div id="display" style="display: none; position:absolute; top:40px;width:12% important; z-index:10; background:#EEEEEE;color:black;filter:alpha(opacity=80); border:1px solid #ddd; border-radius:6px; max-height:150px; overflow:scroll; line-height:25px; padding:10px "
            class=" "></div>

        </form>

        <?php //SEARCH DI INDEX ?>
        <form class="form-inline navbar-search pull-right" action="{!!action('HomeController@search')!!}" style="margin-left: -5px;margin-top: 17px">
            <input type="text" id="searchbox" placeholder="Search Product Name" autocomplete="on" style="font-family:lato !important;border:1px solid #3479B3;z-index:999999;border-radius: 0px;height:auto;width:400px;" name="q" class="form-control"/>
            <button type="submit" name="" class="btn btn-danger"><i class="fa fa-search"></i></button>
            </form>
        <br> </div>
        <div class="row menu-bgg">
        <div class="container" style="width: 1170px;" >
    <ul id="topMenu" class="nav pull-right" style="width:100%">
        <li <?php if(Route::getCurrentRoute()->getPath() == 'index' || Route::getCurrentRoute()->getPath() == '/') { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a class="a-menu" href="<?php echo url('index'); ?>" target="_self">Home</a>
        </li>
        <li <?php if(Route::getCurrentRoute()->getPath() == 'products') { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a class="a-menu" href="<?php echo url('products'); ?>" target="_self">Products</a>
        </li>
        <li <?php if(Route::getCurrentRoute()->getPath() == 'deals') { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a class="a-menu" href="<?php echo url('deals'); ?>" target="_self">Promo</a>
        </li>
        <!-- turun harga -->
        <li <?php if(Route::getCurrentRoute()->getPath() == 'turun_harga') { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a class="a-menu" href="<?php echo url('turun_harga'); ?>" target="_self">Turun Harga!</a>
        </li>

        <li <?php if(Route::getCurrentRoute()->getPath() == 'sold') { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a class="a-menu" href="<?php echo url('sold'); ?>" target="_self">Sold Out</a>
        </li>

        <!--perubahan di menu store dan nearbystore-->
        <li style="display:none;" <?php if(Route::getCurrentRoute()->getPath() == 'stores') { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a href="<?php echo url('stores'); ?>" target="_self">Stores</a>
        </li>
        <li style="display:none;" <?php if(Route::getCurrentRoute()->getPath() == 'nearbystore' ) { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a href="<?php echo url('nearbystore'); ?>" target="_self">Near by store</a>
        </li>
         <li <?php if(Route::getCurrentRoute()->getPath() == 'contactus' ) { ?>class="active"
            <?php } else {?> class=""
            <?php } ?>><a class="a-menu" href="<?php echo url('contactus'); ?>" target="_self">Contact Us</a>
        </li>
    </ul>
    </div>
    </div>
</div>
<script type="text/javascript">
$('#searchbox').autocomplete({
    source:"{{url('autosearch')}}",
    select:function(event, ui){
        $('#searchbox').val(ui.item.value);
    }
});
</script>
