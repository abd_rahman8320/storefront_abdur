<?php
header('Access-Control-Allow-Origin: *');
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<base href="https://" target="_self">
    <meta charset="utf-8">

    <?php
		$metadetails = DB::table('nm_generalsetting')->get();
    if ($metadetails){
	    foreach ($metadetails as $metainfo) {
			$metaname = $metainfo->gs_metatitle;
			$metakeywords = $metainfo->gs_metakeywords;
			$metadesc = $metainfo->gs_metadesc;
		}
	}
    else {
        $metaname = "Nex eMerchant";
        $metakeywords = "Nex eMerchant";
        $metadesc = "Nex eMerchant";
    }

	if (isset($current_route)) {
		if ($current_route=='productview/{mc}/{sc}/{sb}/{ssb}/{id}'){
			if ($metaproduct){
				$metaname .= ' | '.$metaproduct->pro_mtitle;
				$metadesc .= ' '.$metaproduct->pro_mdesc;
				$metakeywords .= ' '.$metaproduct->pro_mkeywords;
			}
		}
	}
    ?>

	<title><?php echo $metaname  ;?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="_token" content="{!! csrf_token() !!}"/>
	<link id="callCss" rel="stylesheet" href="<?php echo url(); ?>/themes/bootshop/bootstrap.min.css" media="screen">
	<link href="<?php echo url(); ?>/themes/css/green-theme.css" rel="stylesheet" media="screen"/>
	<link href="<?php echo url(); ?>/themes/css/img-sprites.css" rel="stylesheet" media="screen"/>
	<!-- Bootstrap style -->

    <?php //foreach($general as $gs) {} if($gs->gs_themes == 'blue') //{ ?>
    <!--link href="<?php echo url(); ?>/themes/css/base.css" rel="stylesheet" media="screen"/-->
    <?php //} elseif($gs->gs_themes == 'green') //{ ?>
    <!--link href="<?php echo url(); ?>/themes/css/green-theme.css" rel="stylesheet" media="screen"/-->
    <?php //} ?>
	<link href="<?php echo url(); ?>/themes/css/jquery.colorpanel.css" rel="stylesheet" media="screen"/>

	<!-- validation (Register Page)(newsletter)-->
	<link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo url(); ?>/themes/css/simpleform.css">
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

	<!-- Bootstrap style responsive-->
    <link href="<?php echo url(); ?>/themes/css/planing.css" rel="stylesheet"/>
    <link href="<?php echo url(); ?>/themes/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="<?php echo url(); ?>/themes/css/font-awesome.css" rel="stylesheet" type="text/css">

	<!-- Google-code-prettify -->
    <link href="<?php echo url(); ?>/themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
	<!-- Google-code-prettify (Register Page)-->
    <link href="<?php echo url(); ?>/themes/css/jquery-gmaps-latlon-picker.css" rel="stylesheet"/>

	<!-- fav and touch icons -->
    <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo url(); ?>/assets/favicon/<?php echo $fav->imgs_name; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo url(); ?>/assets/favicon/<?php echo $fav->imgs_name; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo url(); ?>/themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo url(); ?>/themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo url(); ?>/themes/images/ico/apple-touch-icon-57-precomposed.png">
	<link rel="stylesheet" href="<?php echo url(); ?>/themes/css/normalize.css" />
	<link rel="stylesheet" href="<?php echo url(); ?>/themes/css/styles.min.css" />
	<link href="<?php echo url(); ?>/themes/css/jplist.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo url(''); ?>/themes/css/leftmenu.css" rel="stylesheet" media="screen"/>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<style type="text/css" id="enjects"></style>

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/themes/css/compare-products/demo.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/themes/css/compare-products/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/assets/css/kukuruyuk.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/themes/css/leftmenu.css"media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/themes/css/magiczoomplus.css" media="screen"/>

	<style media="screen">
		@media (min-width:768px) and (max-width:1023px){
			.table{
				font-size: 90%;
			}
		}
	</style>

	<style type="text/css" id="enject"></style>

	<style type="text/css">
		.gllpMap  { height: 228px; }
		input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0;
        }
	</style>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.css"></script>
	<style media="screen">
		@media (min-width:768px) and (max-width:1024px) {
			.img-row{
				width:100%;
			}
			.img-ad{
				width:200%;
			}
			.tab-add{
				width: 100%;
			}
		}
		@media (min-width:240px) and (max-width:767px){
			.img-row{
				display: none;
			}
			.img-ad{
				display: none;
			}
			#myCarousel{
				width: 95% !important;
			}
			body{
				width: 95%;
			}
		}
	</style>
	<style type="text/css">
		.products{
			width: 100%;
			float: left;
		}
		.products .product{
			float: left;
		}
		img {
            max-width: 100%;
            max-height: 100%;
    	}
		.portrait {
			height: 768px;
			width: 316px;
		}
	</style>
    <style media="screen">
        @media (min-width:768px) and (max-width:1024px){
            .a-menu{
                margin-left: 30px;
            }
        }
        @media (min-width:240px) and (max-width:480px){
            .navbar-search .srchTxt{
                margin-right: 0px !important;
            }
            .a-menu{
                margin-left: 10px;
            }
        }
    </style>

	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?php echo url(); ?>/themes/js/modernizr.min.js"></script>

</head>
