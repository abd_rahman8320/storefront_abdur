<?php header("Access-Control-Allow-Origin: *"); ?>
<div class="row">

	<!-- Side Menu Start ================================================== -->
	<div class="span3 side-men" id="side-menu">
		<div class="side-menu-head"><strong>Categories</strong></div>
        <ul id="css3menu1" class="topmenu">
        <input type="checkbox" id="css3menu-switcher" class="switchbox"><label onclick="" class="switch" for="css3menu-switcher"></label>
        <?php foreach ($main_category as $fetch_main_cat) { $pass_cat_id1 = "1,".$fetch_main_cat->mc_id; ?>
            <?php if (count($sub_main_category[$fetch_main_cat->mc_id])!= 0) { ?>
                <li class="topfirst"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id1); ?>" target="_self"><?php echo $fetch_main_cat->mc_name; ?> </a>
                <ul>
                    <?php
                    foreach($sub_main_category[$fetch_main_cat->mc_id] as $fetch_sub_main_cat):
                        $pass_cat_id2 = "2,".$fetch_sub_main_cat->smc_id; ?>
                        <?php if (count($second_main_category[$fetch_sub_main_cat->smc_id])!= 0): ?>
                            <li class="subfirst">
                            <a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id2); ?>" target="_self"><?php echo $fetch_sub_main_cat->smc_name ; ?> </a>
                            <ul>
                                <?php
                                foreach ($second_main_category[$fetch_sub_main_cat->smc_id] as $fetch_sub_cat):
                                    $pass_cat_id3 = "3,".$fetch_sub_cat->sb_id;
                                    if (count($second_sub_main_category[$fetch_sub_cat->sb_id])!= 0): ?>
                                        <li class="subsecond"><a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id3); ?>" target="_self"><?php echo  $fetch_sub_cat->sb_name; ?> </a>
                                        <ul style="display:none;">
                                            <?php
                                            foreach($second_sub_main_category[$fetch_sub_cat->sb_id] as $fetch_secsub_cat):
                                                $pass_cat_id4 = "4,".$fetch_secsub_cat->ssb_id; ?>
                                                <li class="subthird">
                                                <a href="<?php echo url('catproducts/viewcategorylist')."/".base64_encode($pass_cat_id4); ?>" target="_self"><?php echo $fetch_secsub_cat->ssb_name ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                </li>
            <?php } ?>
        <?php } ?>
        </ul>
        <div class="clearfix"></div>
    </div>
	<!-- Side Menu End ================================================== -->

	<div class="span9">
		<div id="carouselBlk">
		<div id="myCarousel" class="carousel slide">
				<div class="carousel-inner">
		<?php
		if ($bannerimagedetails):
			$i=1;
			foreach($bannerimagedetails as $banner):
				$bannerimagepath="assets/bannerimage/".$banner->bn_img;
				date_default_timezone_set('Asia/Jakarta');
				$today = date('Y-m-d H-i-s');
				if ($banner->start_date <= $today && $banner->end_date >= $today):
		?>
				<div <?php if($i==1){ ?>class="item active" <?php } else { ?> class="item" <?php } ?>>
					<a href="<?php echo url('getting_count_klick/'.$banner->bn_id); ?>">
						<img width="100%" src="<?php echo url('').'/'.$bannerimagepath; ?>"  alt=""/>
					</a>
				</div>
		<?php
				endif;
				$i++;
			endforeach;
		else:
		?>

		<div class="item active">
			<a ><img style="width:100%" src="<?php echo url(''); ?>/themes/images/carousel/6.png" alt="special offers"/></a>
		</div>

		<div class="item">
			<div class="container">
				<a ><img style="width:100%" src="<?php echo url(''); ?>/themes/images/carousel/3.png" alt="special offers"/></a>
			</div>
		</div>
		<?php
		endif;
		?>
				</div>

				<a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div>
            </div>
    </div>
    </div>
