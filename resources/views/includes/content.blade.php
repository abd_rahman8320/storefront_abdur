<?php header("Access-Control-Allow-Origin: *"); ?>
<link rel="stylesheet" type="text/css" href="{{url('')}}/themes/slick/slick.css">
<link rel="stylesheet" type="text/css" href="{{url('')}}/themes/slick/slick-theme.css">
<style type="text/css">
    html, body {
      margin: 0;
      padding: 0;
    }

    * {
      box-sizing: border-box;
    }

    .slider {
        width: 100%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }

    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }

    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }

	.button {
	    background-color: black;
	    border: none;
	    color: white;
		font-weight: bold;
	    padding: 5px 10px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;
	    margin: 10px 5px;
	    cursor: pointer;
		height: 50px;
	}
</style>
<script src="<?php echo url('')?>/assets/plugins/jquery-2.0.3.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{url('')}}/themes/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$(document).on('ready', function() {
	  $(".center").slick({
	    dots: true,
	    infinite: true,
	    centerMode: true,
	    slidesToShow: 5,
	    slidesToScroll: 3
	  });
	});
</script>
<script type="text/javascript">
// var count = 0;
// function show_table(i)
// {
// 	if(count<=3){
// 		count++;
// 		alert(count);
// 		$('#table_specification'+i).show();
// 	}else {
// 		alert(count);
// 	}
// }
// function hide_table(i)
// {
// 	alert(count);
// 	count--;
// 	$('#table_specification'+i).hide();
// }
</script>
	<div class="container home-pro" >
	@if(Session::has('wish'))
	<p class="alert {!! Session::get('alert-class', 'alert-success') !!}">{!! Session::get('wish') !!}</p>
	@endif
	<div class="row">
<!-- Sidebar ================================================== -->


<!-- Sidebar end=============================================== -->

<!-- Mulai Product tampil -->
<?php if(isset($_SESSION['cart'][0]['flashPromoId'])){
		//session_unset($_SESSION['cart'][0]['flashPromoId']);
		//session_destroy();
}?>

		<div id="demo" class="span12">
		<div class="compare-basket">
				<button class="action action--button action--compare">
				<i class="fa fa-check"></i><span class="action__text">Compare</span></button>
		</div>

		<div class="view">
		<section class="grid">
			<?php if(!empty($get_schedule_flash)): ?>
			<div class="flash_promo" style="width:100%; height:400px; margin-top:10px; margin-bottom:40px;<?php if($next_flash_display == 0 && $now_flash_display == 0) {?> display:none; <?php } ?>">
				<p style="text-align: left; float:left; font-size: 16px; margin-left:15px;" ><strong>Flash Promo</strong></p>
				<a style="float:right; color:blue; margin-right:20px;" href="{!! url('productsflash') !!}" >Lihat semua</a>
				<div style="clear:both;"></div>
				<div class="product__info" style="float:left; width:18%; margin-left:8px; height: 400px; box-shadow:0px 0px 5px grey;" >
					<div style="padding:10px;font-size:15px;"><strong>What's Next ?</strong></div>
					<ul class="slider1" style="padding:0; margin:0;">
					<?php $i=1;
					//dd($get_schedule_flash);
					if($get_schedule_flash){
						//dd($get_schedule_flash);
					foreach ($get_schedule_flash as $product_flash){
						$mcat = strtolower(str_replace(' ','-',$product_flash->mc_name));
						$smcat = strtolower(str_replace(' ','-',$product_flash->smc_name));
						$sbcat = strtolower(str_replace(' ','-',$product_flash->sb_name));
						$ssbcat = strtolower(str_replace(' ','-',$product_flash->ssb_name));
						$res = base64_encode($product_flash->pro_id);
						$get_image = explode("/**/",$product_flash->pro_Img);
						$promo_flash_id = $product_flash->schedp_id;

						$todays = date('Y-m-d H:i:s');
						$format = 'Y-m-d H:i:s';
						$dt = DateTime::createFromFormat($format, $todays);
						$today_sec = $dt->format('U');

						$start_date = $product_flash->schedp_start_date;
						$sd = DateTime::createFromFormat($format, $start_date);
						$start_sec = $sd->format('U');

						$end_date = $product_flash->schedp_end_date;
						$ed = DateTime::createFromFormat($format, $end_date);
						$end_sec = $ed->format('U'); ?>

						<?php
						if($today_sec < $start_sec && $today_sec < $end_sec){ ?>
							<li style="height: 350px;">
								<?php if($product_flash->promop_discount_percentage):?>
								<i class="tag-dis" style="margin-top:0px;">
									<div id="dis-val">
										<?php echo round($product_flash->promop_discount_percentage);?>%
									</div>
								</i>
								<?php endif;?>
								<img style="height: 150px; width:100%;" src="<?php echo url().'/assets/product/'.$get_image[0]; ?>" />
								<div name="details" style="height: 50px; margin-bottom:30px;">
									<p style="height: 30px;"><?php echo $product_flash->pro_title;?></p>
									<p style="font-size: 13px; font-weight: bold; color: red;">{{$get_cur}}.  <?php echo number_format($product_flash->promop_discount_price,2,",",".");?></p>
									<?php if($product_flash->promop_original_disprice != 0){?>
									<p style="font-weight: bold; padding: 0; margin-top:-5px;"><strike><span style="color:#aaa; font-size: 12;">{{$get_cur}}.  <?php echo number_format($product_flash->promop_original_disprice,2,",",".");?></span></strike></p>
									<?php }else{ ?>
									<p style="font-weight: bold; padding: 0; margin-top:-5px;"><strike><span style="color:#aaa; font-size: 12;">{{$get_cur}}.  <?php echo number_format($product_flash->promop_original_price,2,",",".");?></span></strike></p>
									<?php }?>
								</div>
								<a class="disabled" data-toggle="tooltip" title="Sorry not available for now!" href="{!! url('productview3').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$promo_flash_id !!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
								<div style="font-size:13px; font-weight: bold; margin-top:10px;" data-goingtodate="<?php echo $product_flash->schedp_start_date;?>" data-enddate="<?php echo $product_flash->schedp_end_date;?>"></div>
							</li>
						<?php } ?>
					<?php $i++; }
					} else { ?>
					<?php }?>
					</ul>
				</div>
				<div class="product__info" style="float:left; width:60%; margin-left:13px; height: 400px; box-shadow:0px 0px 5px grey;" >
					<div style="padding:10px;font-size:15px;"><strong>Promo Now !</strong></div>
					<ul class="slider2" style="padding:0; margin:0;">
					<!-- Wrapper for slides -->
						<?php $i=1;
						//dd($get_schedule_flash);
						if($get_schedule_flash){
							foreach ($get_schedule_flash as $product_flash){
							date_default_timezone_set('Asia/Jakarta');
							$mcat = strtolower(str_replace(' ','-',$product_flash->mc_name));
							$smcat = strtolower(str_replace(' ','-',$product_flash->smc_name));
							$sbcat = strtolower(str_replace(' ','-',$product_flash->sb_name));
							$ssbcat = strtolower(str_replace(' ','-',$product_flash->ssb_name));
							$res = base64_encode($product_flash->pro_id);
							$promo_flash_id = $product_flash->schedp_id;
							$get_image = explode("/**/",$product_flash->pro_Img);

							$todays = date('Y-m-d H:i:s');
							$format = 'Y-m-d H:i:s';
							$dt = DateTime::createFromFormat($format, $todays);
							$today_sec = $dt->format('U');

							$start_date = $product_flash->schedp_start_date;
							$sd = DateTime::createFromFormat($format, $start_date);
							$start_sec = $sd->format('U');
							//dd($start_date);
							$end_date = $product_flash->schedp_end_date;
							$ed = DateTime::createFromFormat($format, $end_date);
							$end_sec = $ed->format('U');
							//dd($today_sec > $start_sec && $today_sec <  $end_sec);
							?>

							<?php if($today_sec > $start_sec && $today_sec <  $end_sec){ ?>
								<li style="height: 350px;">
									<?php if($product_flash->promop_discount_percentage):?>
									<i class="tag-dis" style="margin-top:0px;">
										<div id="dis-val">
											<?php echo round($product_flash->promop_discount_percentage);?>%
										</div>
									</i>
									<?php endif;?>
									<img style="height: 185px; width:100%;" src="<?php echo url().'/assets/product/'.$get_image[0]; ?>" alt="..." >
									<div name="details" style="height: 50px; margin-bottom:30px;">
									<p style="height: 30px;"><?php echo $product_flash->pro_title;?></p>
									<p style="font-size: 13px; font-weight: bold; color: red;">{{$get_cur}}.  <?php echo number_format($product_flash->promop_discount_price,2,",",".");?></p>
									<?php if($product_flash->promop_original_disprice != 0){?>
									<p style="font-weight: bold; padding: 0; margin-top:-5px;"><strike><span style="color:#aaa; font-size: 12;">{{$get_cur}}.  <?php echo number_format($product_flash->promop_original_disprice,2,",",".");?></span></strike></p>
									<?php }else{ ?>
									<p style="font-weight: bold; padding: 0; margin-top:-5px;"><strike><span style="color:#aaa; font-size: 12;">{{$get_cur}}.  <?php echo number_format($product_flash->promop_original_price,2,",",".");?></span></strike></p>
									<?php }?>
								</div>
								<a href="{!! url('product_flashPromo').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$promo_flash_id !!}" target="_self"><button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
								<div style="font-size:13px; font-weight: bold; margin-top:10px;" data-inprogressdate="<?php echo $product_flash->schedp_start_date;?>" data-enddate="<?php echo $product_flash->schedp_end_date;?>"></div>
								</li>

							<?php } ?>
							<?php $i++; }?>
							<?php
							} else {
						?>
						<?php }?>
					</ul>

				</div>
				<div style="float:left; width:18%;margin-left:8px; height: 400px; box-shadow:0px 0px 5px grey;" >
					<div style="padding:10px;font-size:15px;"><strong>Expired</strong></div>
					<ul class="slider3" style="height: 100%; padding:0; margin:0;">
					<?php $i=1;
					if($get_schedule_flash){
					foreach ($get_schedule_flash as $product_flash){
						$get_image = explode("/**/",$product_flash->pro_Img);

						$todays = date('Y-m-d H:i:s');
						$format = 'Y-m-d H:i:s';
						$dt = DateTime::createFromFormat($format, $todays);
						$today_sec = $dt->format('U');

						$start_date = $product_flash->schedp_start_date;
						$sd = DateTime::createFromFormat($format, $start_date);
						$start_sec = $sd->format('U');

						$end_date = $product_flash->schedp_end_date;
						$ed = DateTime::createFromFormat($format, $end_date);
						$end_sec = $ed->format('U'); ?>


						<?php
						if($today_sec > $end_sec){ ?>
							<li style="height: 350px;">
								<img style="height: 200px; width:100%;" src="<?php echo url().'/assets/product/'.$get_image[0]; ?>" />
								<div style="font-size:13px; font-weight: bold; margin-top:10px;" data-goingtodate="<?php echo $product_flash->schedp_start_date;?>" data-enddate="<?php echo $product_flash->schedp_end_date;?>"></div>
							</li>
						<?php } ?>

					<?php $i++; }
					} else { ?> <?php }?>
					</ul>
				</div>
			</div>
			<?php
			endif;
			?>
			@include('sepulsa')

			<center>
			<div class="products">
	<h3 class="home-heading" style="float: left; font-weight: bold; margin-left: 40px; margin-top: 25px;">New Products</h3>
	<a href="<?php echo url('products');?>"><h4 class="see-all">Lihat Semua</h4></a>
	</div>
</center>
			<div class="products">

				<?php
				if(count($product_details) != 0)
					{
						$i=0;
						foreach($product_details as $product_det)
						{
							$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
            				$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
							$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
            				$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
							$res = base64_encode($product_det->pro_id);
							$product_image = explode('/**/',$product_det->pro_Img);
							$product__title = str_replace(' ','-',$product_det->pro_title);

							$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

							if($product_det->pro_price==0 || $product_det->pro_price==null)
							{
								$product_discount_percentage = 0;
							}else {
								$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
							}
							if($product_det->pro_no_of_purchase < $product_det->pro_qty)
							{
	?>
						<!-- item 1 -->
						<div class="product" style="margin-right:5px!important;width:204px;">
							<!-- img -->
							<div class="product__info">
							<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
								<div class="img">
									<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product__title!!}" target="_self"><img class="product__image"  src="<?php echo url('assets/product/').'/'.$product_image[0];?>" alt="" title=""/></a>
							<?php endif; ?>

							<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
									<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product__title!!}" target="_self"><img class="product__image"  alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
 							<?php endif; ?>

 							<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
								<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product__title!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
 							<?php endif; ?>
							</div>
							<?php
								if(floatval($product_det->pro_disprice) != 0 ) {
									$total_diskon = ceil(100 - (floatval($product_det->pro_disprice)/floatval($product_det->pro_price) * 100));
								}
							?>
							<!-- data -->
							<div class="">
								<p class="title product__title tab-title" title="{{$product_det->pro_title}}"><?php echo substr($product_det->pro_title,0,50); ?></p>
								<?php if (isset($get_cur)): ?>
								<?php if(floatval($product_det->pro_disprice) != 0 ): ?>
									<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}<?php echo number_format($product_det->pro_disprice);?></p>
									<?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
										  <i class="tag-dis">
												<div id="dis-val">
													<?php echo round($product_discount_percentage);?>%
												</div>
											</i>
										  <?php } ?>
											<p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color:#aaa; font-size: 13;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></span></strike>
								<?php else: ?>
									<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;margin-bottom:30px;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></p>
								<?php endif; ?>
								<?php endif; ?>

								<!-- <p class="title product__title tab-title"><?php echo substr($product_det->pro_title,0,25); ?></p> -->
								<?php if (isset($get_cur)): ?>
								<p class="like product__price"><?php //echo $product_det->pro_disprice;?></p>
								<?php endif; ?>

								<table class="table table-bordered" style="display:none;" id="table_specification">
									<thead>
										<th style="color:white;">Spec Name</th>
										<th style="color:white;">Spec Value</th>
									</thead>
									<tbody>
										<tr>

										</tr>
									</tbody>
								</table>

								<?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty): ?>
                                    <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a></h4>
								<?php else: ?>

									<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
										<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product__title!!}" target="_self">
											<button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
									<?php endif; ?>

									<?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
										<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product__title!!}" target="_self">
											<button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
									<?php endif; ?>

									<?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
										<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product__title!!}" target="_self">
											<button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
									<?php endif; ?>

									<label class="action action--compare-add">
										<input class="check-hidden" type="checkbox" /><i class="fa fa-plus" onclick="show_table({{$i}})"></i><i class="fa fa-check" onclick="hide_table({{$i}})"></i>
											<span class="action__text action__text--invisible">Add to compare</span>
									</label>

								<?php endif; ?>
							</div>

							</div>

						</div>


							<?php
								}
								$i++;
							 }
			}

			elseif(count($product_details) == 0)
			{
				?>

				<div class="box jplist-no-results text-shadow align-center jplist-hidden">

					<p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">No products available</p>
				</div>

				<?php
			}
							?>
				</div>

                <?php
                if(count($recommended_products[0]) != 0)
                    {
                        $k=0;
                        foreach ($neighbor_limit as $key => $value) {

                ?>
                <center>
                    <div class="products">
                        <h3 class="home-heading" style="float: left; font-weight: bold; margin-left: 40px; margin-top: 25px;">Recommended For You ({{$key}} Neighbors)</h3>
                        <a href="<?php echo url('all_products_neighbour/'.$value);?>"><h4 class="see-all">Lihat Semua</h4></a>
                    </div>
                </center>

                <div class="products">

                    <?php

                            $i=0;
                            foreach($recommended_products[$k] as $product_det)
                            {
                                $mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
                                $smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
                                $sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
                                $ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
                                $res = base64_encode($product_det->pro_id);
                                $product_image = explode('/**/',$product_det->pro_Img);
                                $product__title = str_replace(' ','-',$product_det->pro_title);

                                $product_saving_price = $product_det->pro_price - $product_det->pro_disprice;

                                if($product_det->pro_price==0 || $product_det->pro_price==null)
                                {
                                    $product_discount_percentage = 0;
                                }else {
                                    $product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
                                }
                                if($product_det->pro_no_of_purchase < $product_det->pro_qty)
                                {
        ?>
                            <!-- item 1 -->
                            <div class="product" style="margin-right:5px!important;width:204px;">
                                <!-- img -->
                                <div class="product__info">
                                <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                    <div class="img">
                                        <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product__title!!}" target="_self"><img class="product__image"  src="<?php echo url('assets/product/').'/'.$product_image[0];?>" alt="" title=""/></a>
                                <?php endif; ?>

                                <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                                        <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product__title!!}" target="_self"><img class="product__image"  alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
                                <?php endif; ?>

                                <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                                    <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product__title!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
                                <?php endif; ?>
                                </div>
                                <?php
                                    if(floatval($product_det->pro_disprice) != 0 ) {
                                        $total_diskon = ceil(100 - (floatval($product_det->pro_disprice)/floatval($product_det->pro_price) * 100));
                                    }
                                ?>
                                <!-- data -->
                                <div class="">
                                    <p class="title product__title tab-title" title="{{$product_det->pro_title}}"><?php echo substr($product_det->pro_title,0,50); ?></p>
                                    <?php if (isset($get_cur)): ?>
                                    <?php if(floatval($product_det->pro_disprice) != 0 ): ?>
                                        <p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}<?php echo number_format($product_det->pro_disprice);?></p>
                                        <?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
                                              <i class="tag-dis">
                                                    <div id="dis-val">
                                                        <?php echo round($product_discount_percentage);?>%
                                                    </div>
                                                </i>
                                              <?php } ?>
                                                <p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color:#aaa; font-size: 13;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></span></strike>
                                    <?php else: ?>
                                        <p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;margin-bottom:30px;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></p>
                                    <?php endif; ?>
                                    <?php endif; ?>

                                    <!-- <p class="title product__title tab-title"><?php echo substr($product_det->pro_title,0,25); ?></p> -->
                                    <?php if (isset($get_cur)): ?>
                                    <p class="like product__price"><?php //echo $product_det->pro_disprice;?></p>
                                    <?php endif; ?>

                                    <?php if($product_det->pro_no_of_purchase >= $product_det->pro_qty): ?>
                                        <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a></h4>
                                    <?php else: ?>

                                        <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != ''): ?>
                                            <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product__title!!}" target="_self">
                                                <button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
                                        <?php endif; ?>

                                        <?php if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == ''): ?>
                                            <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product__title!!}" target="_self">
                                                <button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
                                        <?php endif; ?>

                                        <?php if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == ''): ?>
                                            <a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product__title!!}" target="_self">
                                                <button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>
                                        <?php endif; ?>

                                        <label class="action action--compare-add">
                                            <input class="check-hidden" type="checkbox" /><i class="fa fa-plus" onclick="show_table({{$i}})"></i><i class="fa fa-check" onclick="hide_table({{$i}})"></i>
                                                <span class="action__text action__text--invisible">Add to compare</span>
                                        </label>

                                    <?php endif; ?>
                                </div>

                                </div>

                            </div>


                                <?php
                                    }
                                    $i++;
                                 }
                                 $k++;
                             }
                }

                elseif(count($recommended_products) == 0)
                {
                    ?>
                    <center>
                        <div class="products">
                            <h3 class="home-heading" style="float: left; font-weight: bold; margin-left: 40px; margin-top: 25px;">Recommended For You</h3>
                            <a href="<?php echo url('products');?>"><h4 class="see-all">Lihat Semua</h4></a>
                        </div>
                    </center>

                    <div class="products">
                    <div class="box jplist-no-results text-shadow align-center jplist-hidden">

                        <p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">No products available</p>
                    </div>

                    <?php
                }
                                ?>
                    </div>

<?php if (isset($groping_main)): ?>
				<?php
		foreach($groping_main as $main) {
	?>
	<!-- Mulai Product tampil -->
<center>
	<h3 class="home-heading" style="float: left; font-weight: bold; margin-left: 40px;"><?php echo $main->group_nama?></h3>

	<a href="<?php echo url('products');?>"><h4 class="see-all-group">Lihat Semua</h4></a>
</center>

				<div class="products">
				<?php
				if(array_key_exists($main->group_id, $grouping_product))
					{
						foreach($grouping_product[$main->group_id] as $product_det)
						{
							$mcat = strtolower(str_replace(' ','-',$product_det->mc_name));
            				$smcat = strtolower(str_replace(' ','-',$product_det->smc_name));
							$sbcat = strtolower(str_replace(' ','-',$product_det->sb_name));
             				$ssbcat = strtolower(str_replace(' ','-',$product_det->ssb_name));
			  				$res = base64_encode($product_det->pro_id);
							$product_image = explode('/**/',$product_det->pro_Img);

							$product_saving_price = $product_det->pro_price - $product_det->pro_disprice;
							if($product_det->pro_price==0 || $product_det->pro_price==null)
							{
								$product_discount_percentage = 0;
							}else {
								$product_discount_percentage = round(($product_saving_price/ $product_det->pro_price)*100,2);
							}
							if($product_det->pro_no_of_purchase < $product_det->pro_qty) {
				?>
					<!-- item 1 -->
					<div class="product" style="margin-right:5px!important;width:204px;">
							<!-- img -->
						<div class="product__info">
							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
									{
							?>

							<div class="img">
								<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res.'/'.$product__title!!}" target="_self"><img  class="product__image" src="<?php echo url('assets/product/').'/'.$product_image[0];?>" alt="" title=""/></a>

							<?php
								}
							?>

							<?php

								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
									{
							?>
									<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res.'/'.$product__title!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>
 							<?php
 								}
 							?>

 							<?php
 								if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
 									{
 							?>

								<a href="{!! url('productview_spesial').'/'.$mcat.'/'.$smcat.'/'.$res.'/'.$product__title!!}" target="_self"><img  class="product__image" alt="" src="<?php echo url('assets/product/').'/'.$product_image[0];?>"></a>

 							<?php
 									}
 							?>
							</div>
							<?php
								if(floatval($product_det->pro_disprice) != 0 )
								{

									$total_diskon = ceil(100 - (floatval($product_det->pro_disprice)/floatval($product_det->pro_price) * 100));
								}
							?>
		<!-- data -->
							<div class="">

								<p class="title product__title tab-title"><?php echo substr($product_det->pro_title,0,25); ?></p>
								<?php if (isset($get_cur)): ?>
								<?php if(floatval($product_det->pro_disprice) != 0 ): ?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;">{{$get_cur}}<?php echo number_format($product_det->pro_disprice);?></p>
											<?php if($product_discount_percentage!=0 && $product_discount_percentage!='') { ?>
										  <i class="tag-dis">
												<div id="dis-val">
													<?php echo round($product_discount_percentage);?>%
												</div>
											</i>
										  <?php } ?>
											<p class="" style="font-weight: bold;margin-top: -10px;"><strike><span style="color: #aaa; font-size: 13;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></span></strike>
								<?php else: ?>
											<p class="" style="font-size: 16px; font-weight: bold; margin-top: -5px; color: red;;margin-bottom:14%;">{{$get_cur}}<?php echo number_format($product_det->pro_price);?></p>
								<?php endif; ?>
								<?php endif; ?>

							<?php
								if($product_det->pro_no_of_purchase >= $product_det->pro_qty)
								{
							?>
                                <h4 style="text-align:center;"><a  class="btn btn-danger">Sold</a>
                            <?php
                            	}
                            	else
                            	{
                            ?>
                            	</h4>

							</div>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat != '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$ssbcat.'/'.$res!!}" target="_self">
									<button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php

								}

							?>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat != '' && $ssbcat == '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$sbcat.'/'.$res!!}" target="_self">
									<button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php
								}
							?>

							<?php
								if($mcat != '' && $smcat != '' && $sbcat == '' && $ssbcat == '')
								{
							?>

								<a href="{!! url('productview').'/'.$mcat.'/'.$smcat.'/'.$res!!}" target="_self">
									<button class="action action--button action--buy font-tab"><i class="fa fa-shopping-cart"></i><span class="action__text">Details</span></button></a>

							<?php
								}
							?>
					</div>

						<label class="action action--compare-add">
							<input class="check-hidden" type="checkbox" /><i class="fa fa-plus"></i><i class="fa fa-check"></i>
								<span class="action__text action__text--invisible">Add to compare</span>
						</label>

						<?php
							}
						?>

					</div>
					<?php
						}
					}
				}

				elseif(count($product_details) == 0)
				{
				?>

				<div class="box jplist-no-results text-shadow align-center jplist-hidden">

					<p style="margin-top:20px;color: rgb(54, 160, 222);margin-top: 20px;font-weight: bold;padding-left: 8px;">No products available</p>
				</div>

				<?php
				}
				?>
				</div>
	<?php
		}
	?>
		</section>
	</div>
		<section class="compare">
			<button class="action action--close"><i class="fa fa-remove"></i><span class="action__text action__text--invisible">Close comparison overlay</span></button>
		</section>
	</div>
	<!-- selesai tampilan product -->
<?php endif; ?>

	<!-- end -->
</div>
</div>
</div>
</div>

<script src="<?php echo url(); ?>/themes/js/compare-products/main.js"></script>
