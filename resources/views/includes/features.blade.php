<?php header("Access-Control-Allow-Origin: *"); ?>
<div class="container">
	<div style="height:25px"></div>
</div>

<div class="container" style="background-color:#ffffff">
	<div class="grid-ytb">
		<div class="ytb-embed">
			<div id="embed-vid">
				<iframe width="352" height="296" src="https://www.youtube.com/embed/yDw5iYkYTYs" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<div class="grid-ytb">
		<div class="feature-icon-hover">
			<div class="span4">
				<span class="icon large">
					<img src="<?php echo url('');?>/themes/images/garansi.png" alt="pengiriman"/>
				</span>
				<div class="wrappin">
					<div class="above-heading">
						<h6>Produk Kami</h6>
					</div>
					<h5 style="color:black;">Jaminan Garansi</h5>
					<p style="text-align:justify;color:black">
						Semua produk Barang Jago bergaransi
						& terjamin kualitasnya. Kukuruyuk.com akan memberikan garansi terbatas untuk service GRATIS!
					</p>
				</div>
			</div>
		</div>
		<div class="feature-icon-hover">
		<div class="span4">
				<span class="icon large">
					<img src="<?php echo url('');?>/themes/images/pengiriman.png" alt="distribusi"/>
				</span>
				<div class="wrappin">
					<div class="above-heading">
						<h6>Distribusi</h6>
					</div>
					<h5 style="color:black;">Dikirim Ke Seluruh Indonesia</h5>
					<p style="text-align:justify;color:black">
						Bekerjasama dengan JNE dan Tim Ekspedisi Terbaik untuk mengirim barang pesanan Anda ke seluruh Indonesia.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="grid-ytb">
		<div class="feature-icon-hover">
			<div class="span4">
				<span class="icon large">
					<img src="<?php echo url('');?>/themes/images/best-prices.png" alt="best-price"/>
				</span>
				<div class="wrappin">
					<div class="above-heading">
						<h6>Harga Terbaik</h6>
					</div>
					<h5 style="color:black;">Paling Hemat</h5>
					<p style="text-align:justify;color:black">
						Hanya Kukuruyuk.com yang menawarkan produk Barang Jago dengan harga paling hemat dan terjangkau.
					</p>
				</div>
			</div>
		</div>
		<div class="feature-icon-hover">
			<div class="span4">
				<span class="icon large">
					<img src="<?php echo url('');?>/themes/images/grade-list.png" alt="kualitas"/>
				</span>
				<div class="wrappin">
					<div class="above-heading">
						<h6>Kualitas Produk</h6>
					</div>
					<h5 style="color:black;">Kategori Produk</h5>
					<p style="text-align:justify;color:black">
						Tim profesional Kukuruyuk.com menilai kategori kualitas Barang Jago berdasarkan kondisi fisiknya, Grade A, Grade B, Grade C dan Grade D. Apa artinya?
					</p>
					<a style="color:black" href="<?php echo url('');?>/kualitas_produk" target="_self" alt="grading"><u>Baca Selengkapnya..</u></a>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>
</div>
