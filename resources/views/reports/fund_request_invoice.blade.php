<html>
    <body>
        <?php if($get){ ?>
            <h1 style="text-align:center">Invoice Fund Request</h1>
            <hr>
            <br>
            <table>
                <tr>
                    <td><h3><b>Faktur Pajak</b></h3></td>
                    <td><h3><b>: {{$get[0]->wd_faktur_pajak}}</b></h3></td>
                </tr>
                <tr>
                    <td><h3><b>Request ID</b></h3></td>
                    <td><h3><b>: {{$get[0]->wd_id}}</b></h3></td>
                </tr>
                <tr>
                    <td><h3><b>Name</b></h3></td>
                    <td><h3><b>: {{$get[0]->mer_fname}} {{$get[0]->mer_lname}}</b></h3></td>
                </tr>
                <tr>
                    <td><h3><b>Request Date</b></h3></td>
                    <td><h3><b>: {{$get[0]->wd_date}}</b></h3></td>
                </tr>
            </table>

            <hr>
            <br>
            <table border="1" style="width:100%;font-size:20px;">
                <tr>
                    <th style="text-align:center;">No. </th>
                    <th style="text-align:center;">Transaction ID</th>
                    <th style="text-align:center;">Product Name</th>
                    <th style="text-align:center;">Quantity</th>
                    <th style="text-align:center;">Total Price - Commission ({{$get[0]->mer_commission}}%) (Rp)</th>
                    <th style="text-align:center;">Shipping Price (Rp)</th>

                </tr>
                <?php $i=1; $total=0; foreach($get as $data){ ?>
                <tr>
                    <td>{{$i}}.</td>
                    <td>{{$data->wdd_transaction_id}}</td>
                    <td>{{$data->pro_title}}</td>
                    <td style="text-align:center;">{{$data->wdd_order_qty}}</td>
                    <td style="text-align:right;">{{number_format($data->wdd_order_amt,0, '.', ',')}}</td>
                    <td style="text-align:right;">{{number_format($data->wdd_order_shipping_price,0,'.',',')}}</td>
                </tr>
                <?php $i++; $total+=$data->wdd_order_amt + $data->wdd_order_shipping_price; } ?>
                <tr>
                    <td colspan="3"></td>
                    <td>Total</td>
                    <td colspan="2" style="text-align:right;">{{number_format($total,0,'.',',')}}</td>
                </tr>
            </table>
        <?php }?>
    </body>
</html>
