<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
.batas{margin-top: 0.2em;}
.line-break{margin-top: 15em;}
.foto{text-align: center;}
</style>
    <body>
            <div class="foto">
                <img src="<?php echo url('');?>/assets/logo/logo-web-baru.png">
            </div>
        <?php if($get_header_transaksi){ ?>
            <h2 style="text-align:center">Invoice Transaksi</h2>
            <hr>
            <div style="">
                <div>
                <table style="width:50%;font-size:15px;float:left;">
                    <tr>
                        <td><u><b>Transaksi</b></u></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td><b>Costumer Name</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->cus_name}}</b></td>
                    </tr>
                    <tr>
                        <td><b>ID Transaksi</b></td>
                        <td><b>: {{$get_header_transaksi->transaction_id}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Metode Pembayaran</b></td>
                        <td><b>: {{$get_header_transaksi->metode_pembayaran}}</b></td>
                    </tr>
                </table>
                </div>

                <div>
                <table style="width:50%;font-size:15px;float:left;">
                    <tr>
                        <td><u><b>Shipping Detail</b></u></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->cus_name}}</b></td>
                    </tr>
                    @if($get_detail_transaksi[0]->order_type != 3)
                    <tr>
                        <td><b>Address Line</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->ship_address1}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Country</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->co_name}}</b></td>
                    </tr>
                    <tr>
                        <td><b>City</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->ci_name}}</b></td>
                    </tr>
                    <tr>
                        <td><b>District</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->dis_name}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Sub District</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->sdis_name}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Email</b></td>
                        <td><b>: {{$get_detail_transaksi[0]->ship_email}}</b></td>
                    </tr>
                    @endif
                    <tr>
                        <td><b>Phone Number</b></td>
                        @if($get_detail_transaksi[0]->order_type != 3)
                        <td><b>: {{$get_detail_transaksi[0]->ship_phone}}</b></td>
                        @else
                        <td><b>: {{$get_detail_transaksi[0]->cus_phone}}</b></td>
                        @endif
                    </tr>
                </table>
                </div>
            </div></br>
            <div class="line-break">
            <hr>
            </div>
            <div class="batas">
                <h3>Detail Transaksi</h3>
            <table border="1" style="width:100%;font-size:15px;">
                <tr>
                    <th style="text-align:center;">No. </th>
                    <th style="text-align:center;">Product Name</th>
                    <th style="text-align:center;">Quantity</th>
                    <th style="text-align:center;">Total Price (Rp)</th>
                    @if($get_detail_transaksi[0]->order_type != 3)
                    <th style="text-align:center;">Shipping Price (Rp)</th>
                    <th style="text-align:center;">Extended Warranty</th>
                    @endif
                </tr>
                <?php $i=1; $total=0; foreach($get_detail_transaksi as $data){
                    if($data->pro_title != 'Jasa Pengiriman' && $data->pro_title != 'Biaya Pengiriman' && $data->pro_title != ''){
                    ?>
                <tr>
                    <td style="text-align:center;">{{$i}}.</td>
                    <td>{{$data->pro_title}}</td>
                    <td style="text-align:center;"><?php if($data->pro_title != "Jasa Pengiriman" && $data->pro_title != "Biaya Pengiriman" && $data->pro_title != "Diskon"){ echo $data->order_qty;} else{ echo "";}?></td>
                    <td style="text-align:right;">{{number_format($data->order_amt,0, '.', ',')}}</td>
                    @if($get_detail_transaksi[0]->order_type != 3)
                    <td style="text-align:right;"><?php if($data->order_shipping_price != 0) { ?>{{number_format($data->order_shipping_price,0,'.',',')}} <?php } else { echo "";} ?></td>
                    <td style="text-align:right;"><?php if($data->order_jumlah_warranty != 0 ){?>{{number_format($data->order_jumlah_warranty,0,'.',',')}}<?php } else{ echo ""; } ?></td>
                    @endif
                    </tr>
                    <?php $i++; $total+=$data->order_amt + $data->order_shipping_price + $data->order_jumlah_warranty; } }?>
                @if($get_detail_transaksi[0]->order_type != 3)
                <tr>
                    <td style="text-align:center;">{{$i}}.</td>
                    <td>Poin</td>
                    <td></td>
                    <td style="text-align:right;">{{number_format($get_header_transaksi->poin_digunakan,0,'.',',')}}</td>
                    <td></td>
                    <td></td>
                </tr>
                @endif
                <?php $total = $total - floatval($get_header_transaksi->poin_digunakan);?>
                <tr>
                    <td colspan="3">Total</td>
                    <td colspan="3" style="text-align:right;">{{number_format($total,0,'.',',')}}</td>
                </tr>
            </table>
            </div>
            <?php
                if($get_header_transaksi->metode_pembayaran == "Columbia")
                {
                    ?>
                        <div style="height: auto;">

                            <h3>Detail Columbia</h3>
                            <table border="1" style="width:100%;font-size:15px;">
                                <tr>
                                    <th style="text-align:center;">Uang Muka</th>
                                    <th style="text-align:center;">Lama Cicilan</th>
                                    <th style="text-align:center;">Admin Fee</th>
                                    <th style="text-align:center;">Cicilan Perbulan</th>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">{{number_format($get_header_transaksi->transaksi_dp,0,'.',',')}}</td>
                                    <td style="text-align:right;">{{number_format($get_header_transaksi->lama_cicilan,0,'.',',')}}</td>
                                    <td style="text-align:right;">{{number_format($get_header_transaksi->adminfee,0,'.',',')}}</td>
                                    <td style="text-align:right;">{{number_format($get_header_transaksi->cicilan_perbulan,0,'.',',')}}</td>
                                </tr>
                            </table>
                        </div>
                    <?php
                }
            ?>
        <?php }?>
    </body>
</html>
