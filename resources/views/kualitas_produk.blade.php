<?php header("Access-Control-Allow-Origin: *"); ?>
@include('includes/headertop')
<body style="height:auto;">

<!-- Navbar Start ================================================== -->
{!! $navbar !!}
<!-- Navbar End ================================================== -->

<!-- Header Start ================================================== -->
{!! $header !!}
<!-- Header End ================================================== -->

<div id="mainBody faq_main">
<div class="container">
<h1 style="color:#ff8400;">Kualitas Produk </h1>
<legend></legend>
<div id="legalNotice">
	<h3>
  <img alt="" src="https://kukuruyuk-telin.akamaized.net/media/wysiwyg/1176x300-px-grade-banner-white.png">
	<br></h3>
	<h3>Apa itu Produk Refurbished?</h3>
	Produk Refurbished adalah produk display, produk model lama atau produk yang sudah di rekondisi oleh pabrik
	dan distributor resmi. Produk di cek ulang oleh Tim Profesional Kukuruyuk.com dan di bagi ke dalam 4 kategori
	produk berdasarkan kondisi fisik, Grade A, B, C dan D. Berikut penjelasannya:
	<h4>Grade A</h4>Fungsi produk berfungsi 100%. Kondisi fisik seperti baru.
    <h6>Contoh gambar</h6>
  <img src="https://kukuruyuk-telin.akamaized.net/media/wysiwyg/contoh_grade_A_3072_of_1_.jpeg"
  alt="ex-a" height="95%" width="90%">
  <div>&nbsp; &nbsp;</div>&nbsp;
	<h4>Grade B</h4>Fungsi produk berfungsi 100%. Kondisi fisik memiliki tanda-tanda seperti sudah pernah di pakai.
  <h6>Contoh gambar</h6>
  <img src="https://kukuruyuk-telin.akamaized.net/media/wysiwyg/0702_contoh_grade_B_882_of_1_.jpeg"
  alt="ex-b" height="95%" width="90%">
	<div>&nbsp;&nbsp;<br>&nbsp;</div>
  <h4>Grade C</h4>Fungsi produk berfungsi 100%. Kondisi fisik memiliki cacat fisik ringan seperti goresan atau penyok namun tidak mengurangi fungsi produk.
  <h6>Contoh gambar</h6>
  <img src="https://kukuruyuk-telin.akamaized.net/media/wysiwyg/contoh_grade_C_855_of_1_.jpeg"
  alt="ex-c" height="95%" width="90%">
	<div>&nbsp;<br></div>&nbsp;
</div>
    <div class="clear">&nbsp;</div>
    <h4>Grade D</h4>Fungsi produk berfungsi 100%. Kondisi fisik memiliki cacat fisik yang cukup
    terlihat seperti goresan atau penyok namun tidak mengurangi fungsi produk.
    (Pilihan produk ini cocok bagi anda yang lebih mementingkan harga produk daripada penampilan produk).
    <h6>Contoh gambar</h6>
    <img src="https://kukuruyuk-telin.akamaized.net/media/wysiwyg/contoh_grade_D_3087_of_1_.jpeg"
    alt="" height="95%" width="90%">
</div>
</div>
</div>
</div>
{!! $footer !!}
</body>
</html>
